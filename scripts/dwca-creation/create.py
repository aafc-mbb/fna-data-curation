import re
import csv
import zlib
import ast

# function to replace the last comma in a list with "and"
def replace_last(string, delimiter, replacement):
    start, _, end = string.rpartition(delimiter)
    return start + replacement + end

# the "pictures" dictionary will associate plates with taxa
plates = open('/Users/joel/plates.csv', mode='r')
reader = csv.reader(plates)
pictures = {rows[0]:rows[1] for rows in reader}
plates.close()

# We build a dictionary of dictionaries.
# famParAuth{family} will give us parent, author, common name, authority, and illustrator for each taxon in the family
famParAuth={}
famFile=open('/Users/joel/src/WFO/families.txt', 'r')
families=famFile.read().splitlines()
for family in families:
  famData=open(f'/Users/joel/src/WFO/parentAuthor/{family}.txt','r')
  reader=csv.reader(famData)
  famParAuth[family]={rows[0]: [rows[1], rows[2], rows[3], rows[4], rows[5]] for rows in reader}
  famData.close()
famFile.close()

# We print out the dictionary of dictionaries for reference and debugging
import pprint
pp = pprint.PrettyPrinter(indent=4, stream=open('dictionary.txt','w'))
pp.pprint(famParAuth)

# The DwC archive will be 5 files, plus meta.txt (built outside of this program)
classification = open('Match21/classification.txt', 'w')
description = open('Match21/description.txt', 'w')
references = open('Match21/references.txt', 'w')
vernacular = open('Match21/vernacular_names.txt', 'w')
images = open('Match21/images.txt', 'w')

with open('match.csv', newline='') as input:
  reader = csv.reader(input, dialect='excel')
  for row in reader:
    wfo_id = row[0]
    fna_id = row[1]
    sciName = row[2]
#    sciAuth = row[3]
    rank = row[4]
    fam = row[5]
    sciAuth = famParAuth[fam][sciName][3]
        illustrator=famParAuth[fam][sciName][4]
    if ',' in illustrator:
      illustrator=illustrator.replace("'","")
      illustrator=illustrator.strip('][')
    m = fna_id.split('_')
    vol=m[0]
    file=m[len(m)-1]
    sciNameURL = sciName.replace(' ', '_')
    if vol in ['V24', 'V25']:
      rights = "Utah State University"
    else:
      rights = "Flora North America Association"
    if vol in ['V19', 'V20', 'V21']:
      target = f'/Users/joel/fna-data-curation/coarse_grained_fna_xml/V19-20-21/{vol}_{file}.xml'
    else:
      target = f'/Users/joel/fna-data-curation/coarse_grained_fna_xml/{vol}/{vol}_{file}.xml'
#    print(target)
    target2 = open(target, 'r')
    treatment=target2.read()
    hash = zlib.adler32(b'treatment')

    parent = famParAuth[fam][sciName][0]
    classification.write(f'{wfo_id},{sciName},{rank},{sciAuth},{parent},{fam}\n')

    if ',' in famParAuth[fam][sciName][1]:
      authors=famParAuth[fam][sciName][1].replace("'","")
      authorString = replace_last(authors, ",", ' and ')
      authorString2=authorString.strip('][')
#    if isinstance(famParAuth[fam][sciName][1], list):
#      print(famParAuth[fam][sciName][1])
#      authors = ",".join(famParAuth[fam][sciName][1])
#      authorString = replace_last(authors, ",", ' and ')
    else:
      authorString2 = famParAuth[fam][sciName][1]

    references.write(f'{wfo_id},{hash},"{sciName} {sciAuth} by {authorString2} in Flora of North America Vol.{vol}",http://floranorthamerica.org/{sciNameURL},{rights},http://creativecommons.org/licenses/by/4.0\n')

    vernaculars = famParAuth[fam][sciName][2].strip('][')
    if vernaculars:
      vernacular.write(f'{wfo_id},"{vernaculars}",{hash}\n')

    if pictures.get(sciName):
      imageURL = pictures.get(sciName).replace(' ', '_')
      images.write(f'{wfo_id},http://beta.floranorthamerica.org/File:{imageURL}.jpeg,{rights},Reuse by request to copyright@floranorthamerica.org,{hash},"{illustrator}"\n')

    for type in ['morphology', 'phenology', 'elevation', 'habitat', 'distribution']:
      type2 = re.search(f'<description type="{type}">(.*)</description>', treatment)
            if type2:
        description.write(f'{wfo_id},{type},"{type2.group(1)}",{hash}\n')


    target2.close()
classification.close()
description.close()