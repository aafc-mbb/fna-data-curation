while read fam; do
  python -m src.run_query --output_file_name "/Users/joel/src/WFO/parentAuthor/$fam.txt" --query_string "[[Taxon family::$fam]]|?Taxon parent|?Author|?Common name|?Authority|?Illustrator"
  echo "DONE $fam"
done </Users/joel/src/WFO/families.txt