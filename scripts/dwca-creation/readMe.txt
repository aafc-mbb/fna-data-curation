These are scripts to produce a DwC-archive for export to World Flora Online.

getInfo.sh uses the fna-query API. It should be run from ~fna-query/python .
It assumes that there is a list of FNA families to read from. (Such a list can 
be created with the API.) 

create.py has the following inputs:
plates.csv is the same file that linkPlatesToTaxa.php uses.
families.txt is a list of families
parentAuthor/ is a directory containing one file per family. (These files are the output of getInfo.sh)
match.csv is an abbreviation of the "name-matching" file that we received from WFO. Columns are:
wfo-id, fna-id, scientific name, scientific name authority, rank, family
 
create.py will generate five files for the dwc-a:
classification, description, references, vernacular_names, images
"meta.xml" needs to be created by hand.