
# coding: utf-8

# In[194]:


import csv
import glob
from lxml import etree
import pandas as pd
from os.path import basename


# In[70]:


def trim_filename(oldstr):
    newstr = basename(oldstr).replace(".xml", "").replace("_refs","")
    return(newstr)


# In[98]:


taxon_files = glob.glob('/Users/jocelynpender/fna/fna-data/fna-data-curation/coarse_grained_fna_xml/*/*.xml')
ref_files = glob.glob('/Users/jocelynpender/fna/fna-data/fna-data-curation/supplemental_data/references/*/*.xml')


# In[75]:


# Clean up the globbed file names
clean_ref_files = ref_files
clean_taxon_files = taxon_files

for i, file in enumerate(clean_ref_files):
    clean_ref_files[i] = trim_filename(file)
    # print(file)

for i, file in enumerate(clean_taxon_files):
    clean_taxon_files[i] = trim_filename(file)


# In[105]:


# Match files up into a dictionary
taxon_file = trim_filename(taxon_file)
file_key = {}

for j, taxon_file in enumerate(clean_taxon_files):
    for i, file in enumerate(clean_ref_files):
        if file == taxon_file:
            file_marker = ref_files[i]
            file_key[taxon_files[j]] = file_marker


# In[170]:


taxon_file = '/Users/jocelynpender/fna/fna-data/fna-data-curation/coarse_grained_fna_xml/V19-20-21/V19_1.xml'
parsed_taxon_file = etree.parse(taxon_file)
root_parsed_ref = etree.parse(file_key[taxon_file]).getroot()


# In[218]:


# Add in parsed references
def replace_refs(parsed_taxon_file, root_parsed_ref):
    if parsed_taxon_file.find("//references"):
        meta_node = parsed_taxon_file.find("//references")
        meta_parent = meta_node.getparent()
        meta_parent.remove(meta_node)
        meta_parent.append(root_parsed_ref)

#parsed_taxon_file.write(taxon_file, encoding= 'utf-8', xml_declaration= True)


# In[215]:


taxon_file = '/Users/jocelynpender/fna/fna-data/fna-data-curation/coarse_grained_fna_xml/V6/V6_450.xml'
parsed_taxon_file = etree.parse(taxon_file)
root_parsed_ref = etree.parse(file_key[taxon_file]).getroot()

if parsed_taxon_file.find("//references"):
    meta_node = parsed_taxon_file.find("//references")
    meta_parent = meta_node.getparent()
    meta_parent.remove(meta_node)
    meta_parent.append(root_parsed_ref)


# In[220]:


for file in taxon_files:#glob.glob('/home/penderj/fna/fna_data/fna-data-curation/coarse_grained_fna_xml/V19-20-21/V19_228.xml'):#
    parsed_taxon_file = etree.parse(file)
    if file in file_key:
        print(file)
        root_parsed_ref = etree.parse(file_key[file]).getroot()
        replace_refs(parsed_taxon_file, root_parsed_ref)
        parsed_taxon_file.write(file, encoding= 'utf-8', xml_declaration= True)

