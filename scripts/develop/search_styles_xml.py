
# coding: utf-8

# In[14]:


from lxml import etree


# In[15]:


tree = etree.parse('../../source_fna/tammy/FNA Published Vol files/FNAVol 12/testV12/document.xml')


# In[16]:


root = tree.getroot()


# In[20]:


body = root.findall('{http://schemas.openxmlformats.org/wordprocessingml/2006/main}body')
body


# In[26]:


NSMAP = {'w': 'http://schemas.openxmlformats.org/wordprocessingml/2006/main'}
p = root.xpath("//w:p", namespaces=NSMAP)
testp = p[0]
testp.xpath("w:pPr", namespaces=NSMAP)


# In[45]:


# use xpath to find all the p's
NSMAP = {'w': 'http://schemas.openxmlformats.org/wordprocessingml/2006/main'}
for p in root.xpath("//w:p", namespaces=NSMAP):
    pPr = p.xpath("w:pPr", namespaces=NSMAP)
    if pPr:
        # print(pPr[0])
    # within each p, look for pPr
        pStylel = pPr[0].xpath("w:pStyle[@w:val]", namespaces=NSMAP) # assume there's only one pPr
        # print(pStylel)
        for pStyle in pStylel:     # within pPr, check pStyle
            print(pStyle.attrib)
    run = p.xpath("//w:r", namespaces=NSMAP)
    for r in run:
        if r:
            textnode = r.xpath("//w:t", namespaces=NSMAP)
            for t in textnode:
                print(t.text) # assume there's only one t
    # back to the p node, grab all the t's eithin the r's.

