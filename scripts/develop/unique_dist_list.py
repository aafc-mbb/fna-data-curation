
# coding: utf-8

# In[ ]:


# Extract unique list of distribution in all volumes
# <description type="distribution">


# In[1]:


from lxml import etree
import os
import re
import csv


# In[19]:


file_path = '/Users/jocelynpender/fna/fna-data/fna-data-curation/coarse_grained_fna_xml/V12/V12_1.xml'
tree = etree.parse(file_path)

dist_node = tree.find("//description[@type='distribution']")
dist_string = dist_node.text

dist_string.split(";")


# In[14]:


def collect_data(file_path):
    # file_path = '/Users/jocelynpender/fna/fna-data/fna-data-curation/coarse_grained_fna_xml/V12/V12_1.xml'
    tree = etree.parse(file_path)

    dist_node = tree.find("//description[@type='distribution']")
    return(dist_node.text)


# In[26]:


dist_collect = set()
for subdir, dirs, files in os.walk("/Users/jocelynpender/fna/fna-data/fna-data-curation/coarse_grained_fna_xml"):
    for file in files:
        print(os.path.join(subdir, file))
        file_path = subdir + os.sep + file

        if (file_path.endswith(".xml")):
            print("Searching" + file_path)
            dist_string = collect_data(file_path)
            clean_dist = dist_string.replace(';',',').split(", ")
            dist_collect.update(clean_dist)


# In[27]:


dist_collect

