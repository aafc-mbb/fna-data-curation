
# coding: utf-8

# In[1]:


from lxml import etree
import os
import re
import csv


# In[2]:


# Format species name from a taxon hierarchy tag

def nameformat(taxonname):
    genus = taxonname.find('genus')
    species = taxonname.find('species')

    # Collect the family name if it exists. Assume it is the first piece of the hierarchy    
    if 'family' in taxonname:
        familyname = taxonname[taxonname.find('family') + 7:taxonname.find(';')]
        familyname = familyname.capitalize()
    else:
        familyname = ''
            
    if 'section' in taxonname:
        section = taxonname.find('section')
        genusname = taxonname[genus + 6:section - 1]
    else:
        genusname = taxonname[genus + 6:species - 1]
    
    binomial = genusname + ' ' + taxonname[species + 8:]
    
    # Search and replace variety with var. and subpsecies with ssp. to match Arb's list!
    if 'variety' in binomial:
        binomial = binomial.replace(';variety', ' var.')
    elif 'subspecies' in binomial:
        binomial = binomial.replace(';subspecies', ' ssp.')
    
    if binomial[-1] == ';': # Trim off trailing ;
        binomial = binomial[0:-1]
    
    binomial = binomial.capitalize()
    genusname = genusname.capitalize()
    
    return([familyname, genusname, binomial])


# In[3]:


for subdir, dirs, files in os.walk("../../coarse_grained_fna_xml/V3"):
    for file in files:
        # print(os.path.join(subdir, file))
        filepath = subdir + os.sep + file
        tree = etree.parse(filepath)
        # taxon = tree.find("//taxon_hierarchy").text
        acceptednode = tree.find("//taxon_identification[@status='ACCEPTED']")
        taxon = nameformat(acceptednode.find("./taxon_hierarchy").text)
        print([file, taxon[2]])

