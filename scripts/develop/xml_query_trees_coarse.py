
# coding: utf-8

# # Query XML files for treeness, taxonomic names (i.e., synonyms) etc.

# ## Load packages

# In[2]:


from lxml import etree
import os
import re
import csv


# ## Utility functions

# In[3]:


# Find introduced species

def findnative(tree, filepath):
    if tree.xpath("//*[text()[contains(.,'Introduced')]]"): # If there is mention anywhere in the text of the species being introduced. This has to be manually cleaned up afterwards because it is not perfect
        distparent = tree.xpath("//*[text()[contains(.,'Introduced')]]")
        nativeness = ['Introduced', distparent[0].text]
    else: # If it is not introduced, we infer here that it is native
        nativeness = ['', '']
    # print(row)
    return(nativeness)


# In[4]:


# Format species name from a taxon hierarchy tag

def nameformat(taxonname):
    genus = taxonname.find('genus')
    species = taxonname.find('species')

    # Collect the family name if it exists. Assume it is the first piece of the hierarchy    
    if 'family' in taxonname:
        familyname = taxonname[taxonname.find('family') + 7:taxonname.find(';')]
        familyname = familyname.capitalize()
    else:
        familyname = ''
            
    if 'section' in taxonname:
        section = taxonname.find('section')
        genusname = taxonname[genus + 6:section - 1]
    else:
        genusname = taxonname[genus + 6:species - 1]
    
    binomial = genusname + ' ' + taxonname[species + 8:]
    
    # Search and replace variety with var. and subpsecies with ssp. to match Arb's list!
    if 'variety' in binomial:
        binomial = binomial.replace(';variety', ' var.')
    elif 'subspecies' in binomial:
        binomial = binomial.replace(';subspecies', ' ssp.')
    
    if binomial[-1] == ';': # Trim off trailing ;
        binomial = binomial[0:-1]
    
    binomial = binomial.capitalize()
    genusname = genusname.capitalize()
    
    return([familyname, genusname, binomial])


# In[5]:


# Print the authorities

def authority(acceptednode):
    if acceptednode.find("./taxon_name[@rank='species']") is not None:
        speciesauth = acceptednode.find("./taxon_name[@rank='species']").attrib['authority']

        # print(speciesauth)
        
        if acceptednode.find("./taxon_name[@rank='variety']") is not None:
            speciesauth = speciesauth + ' ' + acceptednode.find("./taxon_name[@rank='variety']").attrib['authority']
        elif acceptednode.find("./taxon_name[@rank='subspecies']") is not None:
            speciesauth = speciesauth + ' ' + acceptednode.find("./taxon_name[@rank='subspecies']").attrib['authority']

        # print(speciesauth)
        return(speciesauth)


# In[6]:


# filepath = '/Users/jocelynpender/fna/fna-data/fna-data-curation/coarse_grained_fna_xml/V4/V4_271.xml'

#filepath.find('_')
#filepath[56:58]

#print(re.search('\_\d*\.xml', filepath))

def printvol(filepath):
    voltext = re.search('\_\d*\.xml', filepath).start()
    volnum = filepath[voltext-2:voltext]
    volreg = re.search('\d{1,}', volnum)
    return(volreg.group())


# In[133]:


filepath = '/Users/jocelynpender/fna/fna-data/fna-data-curation/coarse_grained_fna_xml/V20_10_20/V20_15.xml'

voltext = re.search('\_\d*\.xml', filepath).start()
print(filepath[voltext])
volnum = filepath[voltext-2:voltext]
print(volnum)
volreg = re.search('\d{1,}', volnum)
print(volreg)
print(volreg.group())


# ## Testing ground

# In[ ]:


tree.findall("//description type='tree']")
# Basically instead of looking for type=tree using parsed content, just search the description type=morphology to find mentions of tree or treelet.
# Manually review this later to ensure that you grab the right stuff.


# In[15]:


filepath = '/Users/jocelynpender/fna/fna-data/fna-data-curation/coarse_grained_fna_xml/V9/V9_882.xml'
tree = etree.parse(filepath)
tree.findall("//*[@name='tree']")

treeness = tree.xpath("//description[@type='morphology']")[0].text
index = treeness.find('tree')
# print(treeness[index - 50:index + 50])

# tree.xpath("//*[text()[contains(.,'introduced')]]") # If there is mention anywhere in the text of the species being introduced. This has to be manually cleaned up afterwards because it is not perfect

#         distparent = tree.xpath("//*[text()[contains(.,'introduced')]]")

print('Tree like' + str(treeness.find('tree-like')))
print('Tree habit ' +  str(treeness.find('tree habit')))



taxon = None
print("working")
if (filepath.endswith(".xml")):
    print("Searching" + filepath)
    query = None


    tree = etree.parse(filepath)

    if tree.find("//description[@type='morphology']") is not None:
        treeness = tree.xpath("//description[@type='morphology']")[0].text.lower()
        if tree.find("//taxon_name[@rank='species']") is not None and treeness.find('tree') != -1 and treeness.find('tree-like') == -1 and treeness.find('tree habit') == -1:
     #   query = "//*[@name='tree']..."

            print("found")
            index = treeness.find('tree')
            print(index)
            textgrab = treeness[index:index + 50]
            print(textgrab)
            print(treeness[index])
            print(treeness[index:index + 50])
            parent = treeness[index:index + 50]

            query = treeness[index:index + 50]
    if query:
        # taxon = tree.find("//taxon_hierarchy").text
        acceptednode = tree.find("//taxon_identification[@status='ACCEPTED']")
        nativeness = findnative(tree, filepath)
        taxon = nameformat(acceptednode.find("./taxon_hierarchy").text)
        synonyms = tree.findall("//taxon_identification[@status='SYNONYM']/taxon_hierarchy")
        synonyms_simple = tree.findall("//taxon_identification[@status='SYNONYM']")

        basionyms = tree.findall("//taxon_identification[@status='BASIONYM']/taxon_hierarchy")
        basionyms_simple = tree.findall("//taxon_identification[@status='BASIONYM']")

        for i in range(len(synonyms)): # I tried to use the other way of looping over synonyms. It didn't work well with findall variable structure
            synonyms[i] = nameformat(synonyms[i].text)[2] + ' ' + authority(synonyms_simple[i])
        synonyms = ';'.join(synonyms)

        for i in range(len(basionyms)): # I tried to use the other way of looping over synonyms. It didn't work well with findall variable structure
            print(basionyms[i])
            print(basionyms[i].text)
            print(nameformat(basionyms[i].text)[2])
            print(authority(basionyms_simple[i]))
            if authority(basionyms_simple[i]) is not None:
                basionyms[i] = nameformat(basionyms[i].text)[2] + ' ' + authority(basionyms_simple[i])
            else:
                basionyms[i] = nameformat(basionyms[i].text)[2]
        basionyms = ';'.join(basionyms)

        row = [filepath, printvol(filepath), taxon[0], taxon[1], taxon[2], authority(acceptednode), basionyms, synonyms, parent, nativeness[0], nativeness[1]]
        print(row)


# ## Run the script

# In[23]:


import os
cwd = os.getcwd()
print(cwd)


# In[19]:


# Read in Output documents and run query

fields = ['File', 'Volume', 'AcceptedFamily', 'AcceptedGenus', 'AcceptedTaxonName', 'AcceptedAuthority', 'BasionymTaxonNames', 'SynonymTaxonNames', 'FNATreeText', 'Introduced', 'IntroducedText']
filename = "tree_taxa_full_all_vols_v7.csv"

with open(filename, 'w') as csvfile:
    # Creating a csv writer object
    csvwriter = csv.writer(csvfile)
     
    # Writing the fields
    csvwriter.writerow(fields)
    for subdir, dirs, files in os.walk("/Users/jocelynpender/fna/fna-data/fna-data-curation/coarse_grained_fna_xml"):
        for file in files:
            # print(os.path.join(subdir, file))
            filepath = subdir + os.sep + file
            taxon = None
            print("working")
            if (filepath.endswith(".xml")):
                print("Searching" + filepath)
                query = None
                
                
                tree = etree.parse(filepath)
                
                if tree.find("//description[@type='morphology']") is not None:
                    treeness = tree.xpath("//description[@type='morphology']")[0].text.lower()
                    if tree.find("//taxon_name[@rank='species']") is not None and treeness.find('tree') != -1 and treeness.find('tree-like') == -1 and treeness.find('treelike') == -1 and treeness.find('tree habit') == -1:
                 #   query = "//*[@name='tree']..."
                    
                        print("found")
                        index = treeness.find('tree')
                        print(index)
                        textgrab = treeness[index:index + 50]
                        print(textgrab)
                        print(treeness[index])
                        print(treeness[index:index + 50])
                        parent = treeness[index:index + 50]
                    
                        query = treeness[index:index + 50]
                if query:
                    # taxon = tree.find("//taxon_hierarchy").text
                    acceptednode = tree.find("//taxon_identification[@status='ACCEPTED']")
                    nativeness = findnative(tree, filepath)
                    taxon = nameformat(acceptednode.find("./taxon_hierarchy").text)
                    synonyms = tree.findall("//taxon_identification[@status='SYNONYM']/taxon_hierarchy")
                    synonyms_simple = tree.findall("//taxon_identification[@status='SYNONYM']")
                    
                    basionyms = tree.findall("//taxon_identification[@status='BASIONYM']/taxon_hierarchy")
                    basionyms_simple = tree.findall("//taxon_identification[@status='BASIONYM']")

                    for i in range(len(synonyms)): # I tried to use the other way of looping over synonyms. It didn't work well with findall variable structure
                        if authority(synonyms_simple[i]) is not None:
                            synonyms[i] = nameformat(synonyms[i].text)[2] + ' ' + authority(synonyms_simple[i])
                        else:
                            synonyms[i] = nameformat(synonyms[i].text)[2]
                    synonyms = ';'.join(synonyms)
                    
                    for i in range(len(basionyms)): # I tried to use the other way of looping over synonyms. It didn't work well with findall variable structure
                        if authority(basionyms_simple[i]) is not None:
                            basionyms[i] = nameformat(basionyms[i].text)[2] + ' ' + authority(basionyms_simple[i])
                        else:
                            basionyms[i] = nameformat(basionyms[i].text)[2]
                    basionyms = ';'.join(basionyms)

                    row = [filepath, printvol(filepath), taxon[0], taxon[1], taxon[2], authority(acceptednode), basionyms, synonyms, parent, nativeness[0], nativeness[1]]
                    print(row)
                    csvwriter.writerow(row)

