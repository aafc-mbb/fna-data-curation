
# coding: utf-8

# In[5]:


# requests allows sending of HTTP requests
import requests
# import html from lxml to parse the html as a tree
from lxml import html
import time
import sys

symbols = {}
file = sys.argv[1]
with open(file) as f:
    for line in f:
        line=line.rstrip('\n')
        (symbol, name) = line.split(",")
        symbols[name] = symbol
        
for name in symbols:
    symbol = symbols[name]
      
    # add url pointing to the web page of interest
    url = 'https://plants.sc.egov.usda.gov/java/reference?symbol=' + symbol + '&sort=State'
    # print url
    # exit()
    #print url
    page = requests.get(url)
    # html.fromstring treats the page.content as a string and then writes it to a tree that can be parse
    tree = html.fromstring(page.content) 

    # define the distribution in the table of interest using xpath
    # the xpath from the browser will add 'tbody' nodes; make sure to remove these as they are introduced by the browser and not necessarily part of the original html
    # using tr[position() > 1] specifies that I want all rows after the first row in this table (first row is the header row and not useful here)
    distribution = tree.xpath('//html/body/table/tr[5]/td/table/tr/td[4]/table/tr[3]/td[1]/table/tr[1]/td/table[2]/tr[position() > 1]/td[2]/text()')
    # print to return an array of values for all distributions in table
    #print distribution
    # use list(set(distribution)) to filter out duplicate distributions
    dist_trim = list(set(distribution))
    dist_str = " ".join(str(d) for d in dist_trim)
    print (symbol + ',' + name + ',' + dist_str)
    # time.sleep(2)   

