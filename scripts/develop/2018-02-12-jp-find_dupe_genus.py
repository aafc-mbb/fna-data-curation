
# coding: utf-8

# In[2]:


from lxml import etree
import os
import re
import csv

from xml.sax.handler import ContentHandler
from xml.sax import make_parser
from glob import glob
import sys


# ## Testing

# In[3]:


filepath = '/home/penderj/fna/fna_data/etc_processing_files/Output/asteraceae/asteraceae_fine_6_21_16/V21_612.xml'
tree = etree.parse(filepath)


# In[4]:


genus = tree.findall("//taxon_identification[@status='ACCEPTED']/taxon_name[@rank='genus']")
print(genus)


# In[5]:


len(genus)


# ## Script

# In[10]:


def parsefile(file): # Helper function to parse file to determine if it is a valid XML doc
    parser = make_parser()
    parser.setContentHandler(ContentHandler())
    parser.parse(file)


for subdir, dirs, files in os.walk("../Input"):
    for file in files:
        filepath = subdir + os.sep + file
        if (filepath.endswith(".xml") and re.search('archive', filepath) is None and re.search('V25', filepath) is None and re.search('V24', filepath) is None and re.search('__MACOSX', filepath) is None):
            try:
                parsefile(filepath)
                tree = etree.parse(filepath)
                # print("parsed")
                genus = tree.findall("//taxon_identification[@status='ACCEPTED']/taxon_name[@rank='subspecies']") # Change rank here to see duplicates in diff ranks
                if len(genus) > 1:
                    print(filepath)
            except Exception as e:
                print("failed: " + filepath)

