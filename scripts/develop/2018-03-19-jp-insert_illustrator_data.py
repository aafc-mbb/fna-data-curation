
# coding: utf-8

# In[ ]:


Old php script
<?php
  function readIllustrators($f) {
    $keys = fgetcsv($f);
    while (!feof($f)) {
      $array[] = array_combine($keys, fgetcsv($f));
    }
    return array_filter($array);
  }

  function findTaxa($find, $array) {   # Function to help search the array
      foreach ($array as $row) {
           if (array_intersect_assoc($row, $find) == $find) {
               return $row;
           }
      }
  }

  function fineTaxon($file_contents) {
    $top_treatment = explode("</taxon_hierarchy>",$file_contents);
    $taxon_hierarchy = explode("<taxon_hierarchy>",$top_treatment[0]);
    $last_taxon = explode(";",$taxon_hierarchy[1]);
    if (end($last_taxon) != '') {
      $taxon = end($last_taxon);
    }
      elseif ($last_taxon[key($last_taxon) - 1] != ''){
        $taxon = $last_taxon[key($last_taxon) - 1];
      }
    return $taxon; # We want the last piece in <taxon_hierarchy>. They are separated by ;
  }

  $f = fopen("IllustratorDataVol5.csv","r");
  $array = @readIllustrators($f);
  $files = glob('*.xml');

  foreach($files as $file) {
    $file_contents=file_get_contents("$file");
    $basename = basename($file);
    $taxon = fineTaxon($file_contents);
    $epithet = preg_split('/ /', $taxon);
    $search = ucfirst(mb_strtolower(end($epithet),'UTF-8'));
    $search_array = array("Taxa" => $search);
    $illustrator = findTaxa($search_array, $array);

    if ($illustrator["Person"] != '') {
      $pieces=explode("</bio:treatment>",$file_contents);
      file_put_contents("$basename", $pieces[0] . "  " . "<illustrator>" . $illustrator["Person"] . "</illustrator>" . "\n</bio:treatment>");
    }
  }

get_ipython().magic('pinfo ')


# Step 1: Read illustrator CSV. Also allow for bulk addition of one illustrator (John Meyers)
# 
# Step 2: Match to the taxon using taxon hierarchy
# 
# Step 3: Add as other_info_on_meta # <other_info_on_meta type="illustrator">

# In[125]:


import csv
import glob
from lxml import etree
import pandas as pd


# In[115]:


def read_csv_file(file_obj):
    file_obj = open(file_obj,'r')
    reader = csv.reader(file_obj)
    your_list = list(reader)
    return(your_list)


# In[131]:


illustrator_files = glob.glob('/home/penderj/fna/fna_data/fna-data-curation/supplemental_data/illustrators/*/*.csv')
opened_file = read_csv_file(illustrator_files[0])

opened_file


# In[132]:


illustrators = []
for file in illustrator_files:
    opened_file = read_csv_file(file)
    illustrators.append(opened_file)
    
illustrators    


# In[136]:


illustrators = []
for file in illustrator_files:
    df = pd.read_csv(file, delimiter=',')
    illustrators.append(df)
illustrators = pd.concat(illustrators)

illustrators


# In[135]:


taxon_files = glob.glob('/home/penderj/fna/fna_data/fna-data-curation/coarse_grained_fna_xml/*/*.xml')

taxon_files


# In[77]:


taxon_file = etree.parse(taxon_files[10000])
# print(taxon_files[10000])

taxon_node = taxon_file.find("//taxon_hierarchy")


# In[100]:


def find_epithet(taxon_obj):
    text = taxon_node.text
    last_text = text.split(";")[-1]
    epithet = last_text.split(" ")[-1]
    return epithet

find_epithet(taxon_node)


# In[108]:


for file in taxon_files:
    taxon_file = etree.parse(file)
    taxon_node = taxon_file.find("//taxon_hierarchy")
    print(taxon_node)
    if taxon_node is not None:
        epithet = taxon(taxon_node)
        print(epithet)


# In[106]:





# ## Best code

# In[21]:


import csv
import glob
from lxml import etree
import pandas as pd


# In[22]:


illustrator_file = pd.read_csv('/home/penderj/fna/fna_data/fna-data-curation/supplemental_data/illustrators/full_illustrator_data.csv', delimiter=',')
illustrator_file


# In[23]:


taxon_files = glob.glob('/home/penderj/fna/fna_data/fna-data-curation/coarse_grained_fna_xml/*/*.xml')
taxon_files


# In[32]:


def find_epithet(taxon_node):
    text = taxon_node.text
    last_text = text.split(";")[-1]
    if last_text.split(" ")[-1] == "":
        epithet = text.split(" ")[-1][:-1]
    else:
        epithet = last_text.split(" ")[-1]
    return epithet.title()


# In[33]:


# <other_info_on_meta type="illustrator">

def add_illustrator_tag(illustrator_text, taxon_file):
   meta_node = taxon_file.find("//meta")
   other_info_node = etree.Element("other_info_on_meta", type="illustrator")
   meta_node.append(other_info_node)
   other_info_node.text = illustrator_text
   # print(etree.tostring(meta_node, pretty_print=True))
   return taxon_file


# In[34]:


for file in taxon_files:#glob.glob('/home/penderj/fna/fna_data/fna-data-curation/coarse_grained_fna_xml/V19-20-21/V19_228.xml'):#
    taxon_file = etree.parse(file)
    print(file)
    write_file = False
    taxon_node = taxon_file.find("//taxon_hierarchy")
    print(taxon_node)
    if taxon_node is not None:
        epithet = find_epithet(taxon_node)
        print(epithet)
        match = illustrator_file.Person[illustrator_file.Taxa == epithet]
        print(match)
        if not match.empty:
            write_file = True
            illustrator_text = match.to_string(index=False)
            print(illustrator_text)
            taxon_file = add_illustrator_tag(illustrator_text, taxon_file)
            if write_file == True:
                print(file)
                taxon_file.write(file, encoding= 'utf-8', xml_declaration= True)
                print('Wrote new file')

