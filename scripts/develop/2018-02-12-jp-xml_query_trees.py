
# coding: utf-8

# # Query XML files for treeness, taxonomic names (i.e., synonyms) etc.

# ## Load packages

# In[1]:


from lxml import etree
import os
import re
import csv


# ## Skip to utility functions
# [Utility functions](#Utility-functions)

# ---

# ## Set up test data

# In[2]:


filepath = '/home/penderj/fna/fna_data/etc_processing_files/Output/V2/584.xml'
# filepath = '/home/penderj/fna/fna_data/etc_processing_files/Output/V3/V3_output_by_TC_task_V3sTextsCapture/924.xml'
# filepath = '/home/penderj/fna/fna_data/etc_processing_files/Output/V4/V4_output_by_TC_task_V4sLearn/225.xml'
# filepath = '/home/penderj/fna/fna_data/etc_processing_files/Output/V4/V4_output_by_TC_task_V4sLearn/204.xml'

tree = etree.parse(filepath)


# ## Archived functions for testing xpath queries

# In[3]:


# Find tree taxon and originating parent text
# def treespecies(filepath, tree):
#    taxon = tree.find("//taxon_hierarchy").text
#    print(taxon)
#    row = [filepath, 'family', 'genus', taxon, 'test', 'native', 'NA']
#    return row

taxon = tree.find("//taxon_hierarchy").text
print(taxon)

# tree.find("//taxon_name[@rank='species']")
# tree.findall("//*[@name='tree']")
# tree.findall("//*[@name='treelet']")

row = ['/home/penderj/fna/fna_data/etc_processing_files/Output/V2/584.xml', 'family', 'genus', 'family cupressaceae;genus callitris;species glaucophylla', 'test', 'native', 'NA']


# ---

# ## Utility functions

# In[4]:


# Find introduced species

def findnative(tree, filepath):
    if tree.find("//character[@value='introduced']"): # If the species has been parsed as an introduced species, (i.e., introduced is a character)
        distparent = tree.find("//character[@value='introduced'].....")
        nativeness = ['introduced', distparent[0].text]
    elif tree.xpath("//*[text()[contains(.,'introduced')]]"): # If there is mention anywhere in the text of the species being introduced. This has to be manually cleaned up afterwards because it is not perfect
        distparent = tree.xpath("//*[text()[contains(.,'introduced')]]")
        nativeness = ['introduced', distparent[0].text]
    else: # If it is not introduced, we infer here that it is native
        nativeness = ['native', '']
    # print(row)
    return(nativeness)


# In[5]:


# Format species name from a taxon hierarchy tag

def nameformat(taxonname):
    genus = taxonname.find('genus')
    species = taxonname.find('species')

    # Collect the family name if it exists. Assume it is the first piece of the hierarchy    
    if 'family' in taxonname:
        familyname = taxonname[taxonname.find('family') + 7:taxonname.find(';')]
        familyname = familyname.capitalize()
    else:
        familyname = ''
            
    if 'section' in taxonname:
        section = taxonname.find('section')
        genusname = taxonname[genus + 6:section - 1]
    else:
        genusname = taxonname[genus + 6:species - 1]
    
    binomial = genusname + ' ' + taxonname[species + 8:]
    
    # Search and replace variety with var. and subpsecies with ssp. to match Arb's list!
    if 'variety' in binomial:
        binomial = binomial.replace(';variety', ' var.')
    elif 'subspecies' in binomial:
        binomial = binomial.replace(';subspecies', ' ssp.')
    
    if binomial[-1] == ';': # Trim off trailing ;
        binomial = binomial[0:-1]
    
    binomial = binomial.capitalize()
    genusname = genusname.capitalize()
    
    return([familyname, genusname, binomial])


# In[6]:


# Print the authorities

def authority(acceptednode):
    if acceptednode.find("./taxon_name[@rank='species']") is not None:
        speciesauth = acceptednode.find("./taxon_name[@rank='species']").attrib['authority']

        # print(speciesauth)
        
        if acceptednode.find("./taxon_name[@rank='variety']") is not None:
            speciesauth = speciesauth + ' ' + acceptednode.find("./taxon_name[@rank='variety']").attrib['authority']
        elif acceptednode.find("./taxon_name[@rank='subspecies']") is not None:
            speciesauth = speciesauth + ' ' + acceptednode.find("./taxon_name[@rank='subspecies']").attrib['authority']

        # print(speciesauth)
        return(speciesauth)


# ## Skip to run the script
# [Run the script](#Run-the-script)

# ---

# ## Other testing activites

# In[7]:


# Pull out synonyms and basionyms

tree.find("//taxon_identification[@status='ACCEPTED']/taxon_hierarchy").text

# Taxon hierarchy has to be changed to reflect the fact that I need to narrow in on ACCEPTED taxonomic name


# In[57]:


# Testing...

filepath = '../../Output/V4/V4_output_by_TC_task_V4sLearn/356.xml'

query = None
tree = etree.parse(filepath)
if tree.find("//taxon_name[@rank='species']") is not None and tree.findall("//*[@name='tree']"):
    query = "//*[@name='tree']..."
    print(tree.findall("//*[@name='tree']"))
    print("im here!")
elif tree.find("//taxon_name[@rank='species']") is not None and tree.findall("//*[@name='treelet']"):
    query = "//*[@name='treelet']..."
    # taxon = tree.find("//taxon_hierarchy").text
print(query)
if query:
    acceptednode = tree.find("//taxon_identification[@status='ACCEPTED']")
    parent = tree.find(query) # Change this to allow for treelets
    print(parent)
    nativeness = findnative(tree, filepath)
    taxon = nameformat(acceptednode.find("./taxon_hierarchy").text)
    synonyms = tree.findall("//taxon_identification[@status='SYNONYM']/taxon_hierarchy")
    #print(type(synonyms))

    for i in range(len(synonyms)):
        synonyms[i] = nameformat(synonyms[i].text)[2]
    synonyms = ';'.join(synonyms)

    row = [filepath, taxon[0], taxon[1], taxon[2], authority(acceptednode), synonyms, parent[0].text, nativeness[0], nativeness[1]]
    print(row)


# ---

# ## Run the script

# In[61]:


# Read in Output documents and run query

fields = ['File', 'AcceptedFamily', 'AcceptedGenus', 'AcceptedTaxonName', 'AcceptedAuthority', 'SynonymTaxonNames', 'FNATreeText', 'Native', 'IntroducedText']
filename = "tree_taxa_synonyms.csv"

with open(filename, 'w') as csvfile:
    # Creating a csv writer object
    csvwriter = csv.writer(csvfile)
     
    # Writing the fields
    csvwriter.writerow(fields)
    for subdir, dirs, files in os.walk("../../Output"):
        for file in files:
            # print(os.path.join(subdir, file))
            filepath = subdir + os.sep + file
            taxon = None

            if (filepath.endswith(".xml") and re.search('archive', filepath) is None and re.search('processed', filepath) is None):
                print("Searching" + filepath)
                query = None
                tree = etree.parse(filepath)
                if tree.find("//taxon_name[@rank='species']") is not None and tree.findall("//*[@name='tree']"):
                    query = "//*[@name='tree']..."
                elif tree.find("//taxon_name[@rank='species']") is not None and tree.findall("//*[@name='treelet']"):
                    query = "//*[@name='treelet']..."
                    # taxon = tree.find("//taxon_hierarchy").text
                if query:
                    print(query)
                    acceptednode = tree.find("//taxon_identification[@status='ACCEPTED']")
                    parent = tree.find(query) # Change this to allow for treelets
                    print(parent)
                    nativeness = findnative(tree, filepath)
                    taxon = nameformat(acceptednode.find("./taxon_hierarchy").text)
                    synonyms = tree.findall("//taxon_identification[@status='SYNONYM']/taxon_hierarchy")
                    #print(type(synonyms))

                    for i in range(len(synonyms)):
                        synonyms[i] = nameformat(synonyms[i].text)[2]
                    synonyms = ';'.join(synonyms)

                    row = [filepath, taxon[0], taxon[1], taxon[2], authority(acceptednode), synonyms, parent[0].text, nativeness[0], nativeness[1]]
                    print(row)


# ## To do

# In[ ]:


# Search for other terms relating to "treeness"

