#!/bin/bash
for filename in ./../../../coarse_grained_fna_xml/treatment_lists/*
do
  wc -l $filename | awk '{print $1}' # cat
done > count_files.txt

awk '{s+=$1} END {print s}' count_files.txt
# https://stackoverflow.com/questions/450799/shell-command-to-sum-integers-one-per-line?noredirect=1&lq=1

#April 22 2020: 22217 files