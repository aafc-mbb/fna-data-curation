#!/usr/bin/perl
use strict;
use warnings;
use feature 'say';

################ D E S C R I P T I O N ####################################
#this script gets the taxon_hierarchy from a group of files specifed by piping an 
#ls or head command, the taxons/file list will be in taxons.csv. The taxon_hierarchy
#is obtained by using the grep bellow
########################################################################### 

################ U S A G E ################################################
##Print usage if there is no pipe
my $usage="\nUSAGE: ls *.xml | perl $0"."\nor\nhead file.txt | perl $0"; 

unless (-p STDIN) {
	print $usage;
	print "\n\nYou are getting this message because you didn't use a pipe, use CTRL+C and run again using a pipe\n";}
########################################################################### 

open my $output, '>', "taxon.csv" #output file 
		or die "Could not open file";
while (<>){
	chomp;
	#say "here is what I see $_";
	my $grep_output= `grep -m 1 "<taxon_hierarchy" $_`;
	chomp $grep_output;
	$grep_output=~ s/\s+\<taxon_hierarchy\>//;
	$grep_output=~ s/\<\/taxon_hierarchy\>//;
	say $output "$_\t$grep_output";
}
