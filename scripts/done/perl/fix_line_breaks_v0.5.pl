#!/usr/bin/perl
use strict;
use warnings;
use feature 'say';

################ D E S C R I P T I O N ####################################
#v0.5 
#avoid getting two line breaks together
#v0.4
#add log 
#v0.2 
#add a check so that it only edits the files that have line breaks
#
#this script is to remove line breaks in .xml FNA files
#for this it will look into the file and check for each line if the next
#line does not start with "<" (excluding whitespace)
#lines without breaks are left the same, but when a break is found 
#the line is put into an array, together with all the next lines until 
#< is found. The content of the array is then merged and writen to the file
#files will have the same name
#clean files are stored in a new directory called ----> clean-files
########################################################################### 

################ U S A G E ################################################

my $usage="\n\nUSAGE:\n\n\tTo run on all xml files in a directory:\n\n\t\tls *.xml | perl $0\n\tor\n\t\tEnter the full name of the file now.\n"; 

unless (-p STDIN) {
	print $usage;
	print "\n\nYou are getting this message because you didn't use a pipe, use CTRL+C and run again using a pipe\n";}
########################################################################### 

## GLOBAL #################################################################
my @allfiles; 
my @broken_files; 
chomp(my $date = localtime);
###########################################################################

### GET THE RESULT FROM LS ################################################
#put files in an array all the files that need to be checked, these come from the ls command that is piped when you invoke this script
while (<STDIN>) {
	chomp;
	push @allfiles, $_; 
}
###########################################################################

### WHICH FILES NEED TO BE CORRECTED? #####################################
foreach	my $file (@allfiles) {
	my $grep_output;
	$grep_output=`grep -v "[[:space:]*]<" $file | grep -v "^<"`; #this grep looks for lines that DO NOT (-v) start (^) with whitespace ([:space:]) followed by a "<" and lines that DO NOT start with "<"
	if ($grep_output ne "") {  #if the result from the prev grep is NOT empty
		push @broken_files, $file; #put this file into an array
	}
}

### LOG WITH FILES THAT WILL BE CORRECTED ###
if ((scalar@broken_files) > (0)) {
	`mkdir clean-files`; #output directory
	open my $log, '>>', "clean-files/log.txt" 
		or die "Could not open file";
	say $log $date;
	say $log "files corrected for line breaks using $0";
	foreach my $file (@broken_files) {
		say $log $file; }
}
##############################################

###########################################################################


### DELETE LINE BREAKS IN FILES ###########################################
my $line_number=0 ; #line counter

foreach my $file (@broken_files) {
	open my $input_xml, '<', $file 
		or die "Could not open file";
	#my $output_xml= fileparse($file_name); #remove file path keep only file name and extension 
	my $output_name= "clean-files/$file"; 
	#$output_name=~ s/.xml/_clean.xml/;
	open my $output_xml, '>', $output_name #output file 
		or die "Could not open file";
	my @all_lines;
	while (<$input_xml>){
		chomp;
		my $text= $_;
		push @all_lines, $text; #put the whole file in an array
	}
	my @broken_lines;
	foreach my $line (@all_lines){
		my $next_line;		
		#say $line_number;
		if ($line =~ /^<\/bio:treatment>/) { #last line	
#			say "this is the last line $line";	
			say $output_xml $line;
		}else {
			$next_line= $all_lines[($line_number+1)]; #look at the next line 
			if ($next_line =~ /^\s*</){		  #if the next line start with <
				if ((scalar@broken_lines) > (0)) {	#and if there is something in my array for broken lines
					push @broken_lines, $line;	#push the current line at the end of the array
					#say "these lines will have to be fixed @broken_lines";
					my $broken_line_counter=0 ;	#start a counter to process the next lines
					my @cleaning_line;		
					my @combined_lines;
					foreach my $broken (@broken_lines){
						if (($broken_line_counter) == 0){
							push @combined_lines, $broken;	#we dont want to get rid of anything from the first line 					
						} else {
							@cleaning_line= split /\s+/, $broken; #this helps to get rid of groups of spaces, keep only one
#							say "this is what the cleaning line looks like"; #test
#							foreach my $line_test (@cleaning_line) { #test
#								say $line_test; } #test
#							say "this is the first word >$cleaning_line[0]<";
							if ($cleaning_line[0] eq "" ) {
								shift @cleaning_line;
								#say "there is no space at the start here@cleaning_line" ;
							}	
							my $clean_string = join " ", @cleaning_line;
							#print $clean_string;
							push @combined_lines, $clean_string;
						}
						$broken_line_counter ++;			
					} 
						say $output_xml "@combined_lines";
#						print "cleaning up @combined_lines\n";
						@broken_lines = (); #empty the array
				} else{
					#say "this line is good $line";
					say $output_xml $line;
				}
			}else {
				push @broken_lines, $line;
			}
		}
		$line_number ++;	
	}
	$line_number=0 ; #reset the counter
	close $input_xml; 
	close $output_xml; 
}
###########################################################################
