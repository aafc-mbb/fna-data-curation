#!/usr/bin/perl
use strict;
use warnings;
use feature 'say';

### DESCRIPTION
#The following script takes a hardcoded path and input file name, to a the xml that needs to be split into families
#The script uses grep to find the boundaries between families and splits the files
#It also removes the w: namespace from the style tag which is required for the next python script to works

#Declare the input xml that contains all the families (from the doc file)
my $path = "../FNA/V17";
my $input_xml =  "$path/big/V17_treatments_strip.xml";

#find the boundaries for the families
my $grep_out = `grep -n "pStyle.*FamName" $input_xml | cut -d ":" -f 1`; 
my @fam_boundaries = split(/\n/, $grep_out); 
my @line_numbers = @fam_boundaries ;

#output file names
my @fam_numbers = (1..100);

for my $start (@fam_boundaries){
    #get the start of the family
    shift @line_numbers;
    $start = $start - 2;
    my $end = $line_numbers[0] -3;
    if (@line_numbers == 0) {
        $end = `wc -l $input_xml | cut -d " " -f 1`;
        chomp $end;
    } 
    say $start, "-", $end;

    #write file 
    my $fam_number = shift @fam_numbers;
    my $output_xml = "$path/V17_F$fam_number.xml" ;
    say $output_xml; 
    `echo "<?xml version='1.0' encoding='UTF-8'?>" > $output_xml`;
    `echo '<document xmlns:mc="http://schemas.openxmlformats.org/markup-compatibility/2006" xmlns:o="urn:schemas-microsoft-com:office:office" xmlns:r="http://schemas.openxmlformats.org/officeDocument/2006/relationships" xmlns:w="http://schemas.openxmlformats.org/wordprocessingml/2006/main" mc:Ignorable="w14 w15 w16se wp14">' >> $output_xml`;
    `awk 'NR >= $start && NR <= $end' $input_xml >> $output_xml`;
    `echo "</document>" >> $output_xml`;

    open my $family_xml, '<', $output_xml 
		or die "Could not open file";
	my $clean_output = $output_xml; 
	$clean_output =~ s/.xml/_clean.xml/;
	open my $clean_output_fh, '>', $clean_output #read the grep output 
		or die "Could not open file";
	my @all_lines;

	while (<$family_xml>){
		#chomp;
		my $text= $_;
        $text =~ s/pStyle w:val/pStyle val/g;
#		push @all_lines, $text;
        print $clean_output_fh $text;
	}
    close $input_xml; 
	close $output_xml; 

}



