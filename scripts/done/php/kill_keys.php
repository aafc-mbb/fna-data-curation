<?php
function deleteNode($node) {
    deleteChildren($node);
    $parent = $node->parentNode;
    $oldnode = $parent->removeChild($node);
}

function deleteChildren($node) {
    while (isset($node->firstChild)) {
        deleteChildren($node->firstChild);
        $node->removeChild($node->firstChild);
    }
}

$cwd=getcwd();
$files=glob('*.xml');
foreach($files as $file) {
  $treatment=simplexml_load_file($file);
  foreach($treatment->xpath('//key') as $key) {
    $node=dom_import_simplexml($key);
    deleteNode($node);
  }
  $treatment->asXML('no_keys/'.$file);
}
?>
