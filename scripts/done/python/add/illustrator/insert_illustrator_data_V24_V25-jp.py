#!/usr/bin/env python
# coding: utf-8

# In[ ]:


The illustrators_V24_V25.csv file contains the illustrators of  all the species in the grass volumes (V24, V25). 

In order to be added to the xml files the csv will need some processing: 
    
    1. Remove rows that dont have an illustrator 
    2. Replace x by hybrid symbol
    3. Split the file in two, one per volume (based on the tribe)
    4. Remove undesired columns 

This csv also allows an opportunity to check all the taxa that are currently accounted for in the csv and what we have.


# In[1]:


V24_tribes = ["PHAREAE", "BAMBUSEAE","OLYREAE", "EHRHARTEAE", "ORYZEAE", "BRACHYELYTREAE",
              "NARDEAE", "DIARRHENEAE", "MELICEAE", "STIPEAE", "BRACHYPODIEAE", "BROMEAE",
              "TRITICEAE", "POEAE"]
V24_tribes = [x.title() for x in V24_tribes]

V25_tribes = ["ARUNDINEAE", "CYNODONTEAE", "PAPPOPHOREAE", "ORCUTTIEAE", "DANTHONIEAE", "ARISTIDEAE", 
              "CENTOTHECEAE", "THYSANOLAENEAE", "GYNERIEAE", "PANICEAE", "ANDROPOGONEAE"]

V25_tribes = [x.title() for x in V25_tribes]


# Clean up csv and write files:

# In[ ]:


import csv
import glob
from lxml import etree
import pandas as pd

illustrator_raw = pd.read_csv('/home/lujantorob/fna-data-curation/supplemental_data/illustrators/V24-25/Illustrators_V24_V25.csv', delimiter=',')
# Remove blanks, not illustrated and replace x 
illustrator_clean = illustrator_raw [illustrator_raw.ILLUSTRATOR.isnull() == False]
illustrator_clean = illustrator_clean [illustrator_clean.ILLUSTRATOR != "Not illustrated"]
illustrator_clean['NAME'] = illustrator_clean['NAME'].str.replace("x ", "×")

# Rename columns
illustrator_clean.rename(columns={'ILLUSTRATOR':'Person'}, inplace=True)
illustrator_clean.rename(columns={'NAME':'Taxa'}, inplace=True)

# Split dataframes and write csvs
illustrator_V24 = illustrator_clean [illustrator_clean['TRIBE'].isin(V24_tribes)]
illustrator_V24 = illustrator_V24[['Person', 'Taxa']]
illustrator_V24.to_csv('/home/lujantorob/fna-data-curation/supplemental_data/illustrators/V24-25/V24_illustrator_data.csv', index=False)

illustrator_V25 = illustrator_clean [illustrator_clean['TRIBE'].isin(V25_tribes)]
illustrator_V25 = illustrator_V25[['Person', 'Taxa']]
illustrator_V25.to_csv('/home/lujantorob/fna-data-curation/supplemental_data/illustrators/V24-25/V25_illustrator_data.csv', index=False)


# Add illustrators, repeat for other volume

# In[7]:


import csv
import glob
from lxml import etree
import pandas as pd

V24_list = pd.read_csv('/Users/jocelynpender/fna/fna-data/fna-data-curation/coarse_grained_fna_xml/treatment_lists/V24_treatment_list.txt',
                        delimiter=',', names = ["file", "taxon_name", "rank"], encoding = "UTF-8")
illustrator_V24 = pd.read_csv('/Users/jocelynpender/fna/fna-data/fna-data-curation/supplemental_data/illustrators/V24-25/V24_illustrator_data.csv', delimiter=',', encoding= "UTF-8")


def add_illustrator_tag(illustrator_text, taxon_file):
    meta_node = taxon_file.find("//meta")
    illustrator_node = taxon_file.find("//other_info_on_meta[@type='illustrator']")
    # Jocelyn inserted this piece so that she could quickly remove all of the illustrator nodes - Nov. 1 2019
    #if illustrator_node is not None:
     #   for remove_illustrator_node in taxon_file.xpath("//other_info_on_meta[@type='illustrator']"):
     #       remove_illustrator_node.getparent().remove(remove_illustrator_node)
     #   added = "yes"
     #   return added
    if illustrator_node is None:
        other_info_node = etree.Element("other_info_on_meta", type="illustrator")
        meta_node.append(other_info_node)
        other_info_node.text = illustrator_text
        added = "yes"
        return added

illustrators = illustrator_V24["Person"].tolist()
for index, taxon in enumerate(illustrator_V24['Taxa']):
    file = V24_list[V24_list.taxon_name.str.lower() == taxon.lower()]['file']
    illustrator = illustrators[index]
    if file.empty:
        test = 1
        #print(taxon+ " - "+illustrator)
        #print(illustrator)
    else:
        file_name = "/Users/jocelynpender/fna/fna-data/fna-fine-grained-xml/V24/" + file.item()
        print(file_name)
        parser = etree.XMLParser(remove_blank_text=True)
        parsed_xml = etree.parse (file_name, parser)
        response = add_illustrator_tag(illustrator, parsed_xml)
        if response == "yes":
            parsed_xml.write(file_name, encoding= 'utf-8', xml_declaration= True, pretty_print= True)
            print(file_name)
print("Finished")


# Notes:
# <p>    
# <p>V24_illustrator_data.csv - 917 taxa illustrated
# <p>V25_illustrator_data.csv - 933 taxa illustrated
# <p>
# <p>Bases on the plates.csv: 
# <p>
# <p>FNA24 - 820
# <p>FNA25 - 854 entries
# <p>
# <p>Need to investigate why these are different...    
# <p>Bouteloua aristidoides var. aristidoides no illustration, check plates.csv
# <p>Check names that have been noted as revised since print on the csv 
# <p>Poa ×gaspensis - Sandy Long is not linked to any plate, check plates.csv

# In[ ]:




