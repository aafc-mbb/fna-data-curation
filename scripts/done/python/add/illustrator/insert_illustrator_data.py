#!/usr/bin/env python
# coding: utf-8

# In[2]:


import csv
import glob
from lxml import etree
import pandas as pd


# In[3]:


illustrator_file = pd.read_csv('/Users/jocelynpender/fna/fna-data/fna-data-curation/supplemental_data/illustrators/V12/V12_illustrator_data.csv', delimiter=',')
illustrator_file


# In[8]:


taxon_files = glob.glob('/Users/jocelynpender/fna/fna-data/fna-fine-grained-xml/V12/*.xml')
taxon_files


# In[5]:


def find_epithet(taxon_node):
    text = taxon_node.text
    last_text = text.split(";")[-1]
    if last_text.split(" ")[-1] == "":
        epithet = text.split(" ")[-1][:-1]
    else:
        epithet = last_text.split(" ")[-1]
    return epithet.title()


# In[6]:


# <other_info_on_meta type="illustrator">

def add_illustrator_tag(illustrator_text, taxon_file):
   meta_node = taxon_file.find("//meta")
   other_info_node = etree.Element("other_info_on_meta", type="illustrator")
   meta_node.append(other_info_node)
   other_info_node.text = illustrator_text
   # print(etree.tostring(meta_node, pretty_print=True))
   return taxon_file


# In[9]:


for file in taxon_files:#glob.glob('/home/penderj/fna/fna_data/fna-data-curation/coarse_grained_fna_xml/V19-20-21/V19_228.xml'):#
    taxon_file = etree.parse(file)
   # print(file)
    write_file = False
    taxon_node = taxon_file.find("//taxon_hierarchy")
  #  print(taxon_node)
    if taxon_node is not None:
        epithet = find_epithet(taxon_node)
      #  print(epithet)
        match = illustrator_file.Person[illustrator_file.Taxa == epithet]
      #  print(match)
        if not match.empty:
            write_file = True
#            print(match)
            illustrator_text = match.to_string(index=False).strip()
            print(illustrator_text)
            taxon_file = add_illustrator_tag(illustrator_text, taxon_file)
            if write_file == True:
            #    print(file)
                taxon_file.write(file, encoding= 'utf-8', xml_declaration= True)
            #    print('Wrote new file')


# In[ ]:




