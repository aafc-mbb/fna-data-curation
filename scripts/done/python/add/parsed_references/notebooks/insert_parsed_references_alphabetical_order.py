
# coding: utf-8

# In[15]:


import glob
from lxml import etree
import os

def trim_filename(oldstr):
    newstr = basename(oldstr).replace(".xml", "").replace("_refs","")
    return(newstr)


def replace_refs(parsed_taxon_file, root_parsed_ref):
    if parsed_taxon_file.find("//references"):
        meta_node = parsed_taxon_file.find("//references")
        meta_parent = meta_node.getparent()
        meta_parent.remove(meta_node)
        meta_parent.append(root_parsed_ref)
        
        
def sort_children(parent, attr):
    parent[:] = sorted(parent, key=lambda child: child.get(attr))

    
def pick(file_type):
    if file_type == "coarse":
        folder = "fna-data-curation/coarse_grained_fna_xml"
    elif file_type == "fine":
        folder = "fna-fine-grained-fna-xml"
    return(folder)


def read_file_write_refs(ref_file, file_type):
    folder = pick(file_type)
    file = ref_file.replace("_refs","").replace("fna-data-curation/supplemental_data/references", folder)
    parsed_taxon_file = etree.parse(file)
    print(file)
    root_parsed_ref = etree.parse(ref_file).getroot()
    replace_refs(parsed_taxon_file, root_parsed_ref)
    references = parsed_taxon_file.find("//references")
    sort_children(references, 'id')
    #for child in references:
     #   sortchildrenby(references, 'id')
    parsed_taxon_file.write(file, encoding= 'utf-8', xml_declaration= True)


# In[16]:


ref_files = glob.iglob('/Users/jocelynpender/fna/fna-data/fna-data-curation/supplemental_data/references/**', recursive=True)
for ref_file in ref_files:#taxon_files:#glob.glob('/home/penderj/fna/fna_data/fna-data-curation/coarse_grained_fna_xml/V19-20-21/V19_228.xml'):
    if os.path.isfile(ref_file) and ref_file.endswith(".xml"):
        read_file_write_refs(ref_file, "coarse")

