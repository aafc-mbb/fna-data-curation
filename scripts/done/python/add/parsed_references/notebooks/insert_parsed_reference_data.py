
# coding: utf-8

# In[2]:


import glob
from lxml import etree
from os.path import basename


# In[3]:


def trim_filename(oldstr):
    newstr = basename(oldstr).replace(".xml", "").replace("_refs","")
    return(newstr)


# In[17]:


# Add in parsed references
def replace_refs(parsed_taxon_file, root_parsed_ref):
    if parsed_taxon_file.find("//references"):
        meta_node = parsed_taxon_file.find("//references")
        meta_parent = meta_node.getparent()
        meta_parent.remove(meta_node)
        meta_parent.append(root_parsed_ref)


# In[21]:


ref_files = glob.glob('/Users/jocelynpender/fna/fna-data/fna-data-curation/supplemental_data/references/V2/*.xml')
for ref_file in ref_files:#taxon_files:#glob.glob('/home/penderj/fna/fna_data/fna-data-curation/coarse_grained_fna_xml/V19-20-21/V19_228.xml'):
    file = ref_file.replace("_refs","").replace("supplemental_data/references","coarse_grained_fna_xml")
    parsed_taxon_file = etree.parse(file)
    print(file)
    root_parsed_ref = etree.parse(ref_file).getroot()
    replace_refs(parsed_taxon_file, root_parsed_ref)
    parsed_taxon_file.write(file, encoding= 'utf-8', xml_declaration= True)

