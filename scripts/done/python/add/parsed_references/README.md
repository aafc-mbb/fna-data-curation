To parse references, do this in BASH:

cd scripts/done/python/add/parsed_references
python add_parsed_refs.py fine /full/path/to/fna-data-curation/supplemental_data/references/**
python add_parsed_refs.py coarse /full/path/to/fna-data/fna-data-curation/supplemental_data/references/**

From Jocelyn's computer:
python add_parsed_refs.py coarse /Users/jocelynpender/fna/fna-data/fna-data-curation/supplemental_data/references/**