#!/usr/bin/env python3

import glob
from lxml import etree
import os
import sys


def trim_filename(oldstr):
    newstr = os.path.basename(oldstr).replace(".xml", "").replace("_refs", "")
    return (newstr)


def replace_refs(parsed_taxon_file, root_parsed_ref):
    if parsed_taxon_file.find("//references"):
        meta_node = parsed_taxon_file.find("//references")
        meta_parent = meta_node.getparent()
        meta_parent.remove(meta_node)
        meta_parent.append(root_parsed_ref)


def sort_children(parent, attr):
    parent[:] = sorted(parent, key=lambda child: child.get(attr))


def pick(file_type):
    print(file_type)
    if file_type == "coarse":
        folder = "fna-data-curation/coarse_grained_fna_xml"
    elif file_type == "fine":
        folder = "fna-fine-grained-xml"
    return (folder)


def read_file_write_refs(ref_file, file_type):
    folder = pick(file_type)
    file = ref_file.replace("_refs", "") \
        .replace("fna-data-curation/supplemental_data/references", folder)
    parsed_taxon_file = etree.parse(file)
    print(file)
    root_parsed_ref = etree.parse(ref_file).getroot()
    replace_refs(parsed_taxon_file, root_parsed_ref)
    references = parsed_taxon_file.find("//references")
    sort_children(references, 'id')
    parsed_taxon_file.write(file, encoding='utf-8', xml_declaration=True)


def main(file_type, path_to_refs):
    ref_files = glob.iglob(path_to_refs, recursive=True)
    for ref_file in ref_files:
        if os.path.isfile(ref_file) and ref_file.endswith(".xml"):
            read_file_write_refs(ref_file, file_type)


if __name__ == '__main__':
    file_type = sys.argv[1]
    print(len(sys.argv))
    if len(sys.argv) == 3:
        path_to_refs = sys.argv[2]
    else:
        path_to_refs = "/Users/jocelynpender/fc/fna/fna-data/fna-data-curation/supplemental_data/references/**"
    main(file_type, path_to_refs)
