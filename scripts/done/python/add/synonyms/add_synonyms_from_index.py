
# coding: utf-8

# This script was writen to add the synonyms to the treatments based on the index provided at the end of V24, which includes the synonyms of both V24 and V25. The index has two types of lines:
# 
# 1. Synonyms which are: synonym = accepted_name volume page#
#  
# 2. Full treatment name: accepted_name authority(if known) volume page# 
# 
# Both names are taken by the script and passed through the function assignRank which returns a diccionary name : rank . For example:
# 
# dictionary = assignRank(Camelina microcarpa) 
# 
# dictionary = {"Camelina" : "genus", "microcarpa" : "species"}
# 
# The dictionary is used to then find the treatment by looping through all the xml files and build the taxon name and hierarchy for the new synonym tag. The script also adds the page numbers provided in the index. 
# 
# When the line cant be parsed by the script, the line is added to a new csv called "manual_fixes.csv". 
# 
# <i>Note: only the treatments where a synonym was added would get the page number, ie the lines with just the accepted name were ignored by the script (lines of the second type described above).
# 
# Note2: when the synonym had the form: </i>
# 
# Agropyron caninum subsp./var. majus = Elymus trachycaulus subsp. trachycaulus	24	322
# 
# <i>This was split into two lines in the csv as:</i>
# 
# Agropyron caninum subsp. majus = Elymus trachycaulus subsp. trachycaulus	24	322
# 
# Agropyron caninum var. majus = Elymus trachycaulus subsp. trachycaulus	24	322
# 
# 

# In[1]:


import re
import csv
import pandas as pd
import glob
from lxml import etree

#function to assign rank to each part of the name using regex
def assignRank (name):
    rank = None
    all_words = name.split()
    dicc = {}
    species_regex = re.search (r'([A-Z]{1}[a-z]+) ([a-z]+)', name)
    rank_list = ["genus", "species"]
    match = False
    if len(all_words) == 1:
        match = True
        rank_list.remove("species")
    if len(all_words) == 2 and species_regex:
        match = True
    if len(all_words) == 4 and species_regex:
        #print("1")
        if "subsp." in name:
            match = True
            rank_list.append("subspecies")
            all_words.remove("subsp.")
        if "var." in name:
            match = True
            if "subvar." in name:
                all_words.remove("subvar.")
                rank_list.append("subvariety")
            else:
                rank_list.append("variety")
                all_words.remove("var.")
        if " forma " in name:
            match = True
            rank_list.append("forma")
            all_words.remove("forma")
    if match == True:
        #print(name)
        for word in all_words:
            dicc[word] = rank_list.pop(0)
    return dicc

# READ CSV and filter synonyms only 
index_csv = pd.read_csv('/home/lujantorob/bibilujan/index_synonyms/Names_synonyms_full_clean.csv')

#FILTER, use a filter to keep lines with synonyms only 
synonyms_filter = index_csv.name.str.contains("=") 
manual_fixes = []

#LOOP over all synonyms
for name in index_csv[synonyms_filter].name:
    two_names = re.split(r' = ', name)
    synonym_name = two_names[0]
    accepted_name = two_names[1]
        
    #REPLACE ABBREVIATED GENUS FULL GENUS 
    abbreviation_pattern = r'^[A-Z]\.'
    abbreviation_regex = re.search (abbreviation_pattern, accepted_name)
    if abbreviation_regex:
        synonym_list = synonym_name.split()
        accepted_name = re.sub(abbreviation_pattern, synonym_list[0], accepted_name)
    
    #PASS accepted name and synonym through function 
    rank_dicc_accepted = assignRank(accepted_name)
    if not rank_dicc_accepted :
        manual_fixes.append(name)
        continue
    accepted_name = accepted_name.replace('subsp.', '')
    accepted_name = accepted_name.replace("var.", "")
    accepted_name = accepted_name.replace("forma", "")
    all_accepted_names = accepted_name.split()
    #print(all_accepted_names)
    
    rank_dicc_synonym = assignRank(synonym_name)
    if not rank_dicc_synonym:
        manual_fixes.append(name)
        continue
    synonym_name = synonym_name.replace('subsp.', '')
    synonym_name = synonym_name.replace("subvar.", "")
    synonym_name = synonym_name.replace("var.", "")
    synonym_name = synonym_name.replace("forma", "")
    all_synonym_name = synonym_name.split()
        
    #FIND file that needs to be edited
    volume = index_csv [index_csv.name == name]['volume'].item()
    directory = "/home/lujantorob/fna-data-curation/coarse_grained_fna_xml/V"+ str(volume).rstrip('0').rstrip('.') +"/*.xml"
    corrected = False    
    for file_name in glob.glob(directory):
        parser = etree.XMLParser(remove_blank_text=True)
        parsed_xml = etree.parse (file_name, parser)
        taxon_accepted_all = parsed_xml.findall('/taxon_identification[@status="ACCEPTED"]/taxon_name')
        last_match = False
        all_match = False 
        for index, accepted_name in enumerate(reversed(all_accepted_names)):
            #print (accepted_name)
            if index == 0:
                if rank_dicc_accepted[accepted_name] == taxon_accepted_all[-1].attrib['rank'] and accepted_name == taxon_accepted_all[-1].text:
                    last_match = True
                if len(all_accepted_names) == 1:
                    all_match = True 
            else:
                file_taxon = parsed_xml.findall('//taxon_name[@rank="' + rank_dicc_accepted[accepted_name] +'"]')
                if file_taxon:
                    if accepted_name.lower() == file_taxon[0].text.lower():
                        all_match = True                          
                    else:
                        all_match = False
                    
        if last_match == True and all_match == True:  
            corrected = "Yes"
            #print (file_name)
            parent = taxon_accepted_all[0].getparent()

            taxon_name_element = etree.Element("taxon_identification", status="SYNONYM")
            parent.addnext(taxon_name_element)

            #ADD new elements
            taxon_hierarchy_text = ""
            for index, synonym in enumerate(all_synonym_name):
                element = etree.SubElement(taxon_name_element,"taxon_name", rank=""+ rank_dicc_synonym[synonym] +"", date="unknown", authority="unknown")
                element.text = synonym
                taxon_hierarchy_text += rank_dicc_synonym[synonym] + " " + synonym.lower() + ";"
            hierarchy_element = etree.SubElement(taxon_name_element,"taxon_hierarchy")    
            hierarchy_element.text = taxon_hierarchy_text.rstrip(';') 
                
            #ADD PAGE number     
            page_num = index_csv [index_csv.name == name]['page'].item()
            if page_num == "0" or page_num == "added post-publication":
                print ("FIX page MANUALLY" + name)
            else:
                page = parsed_xml.findall('/meta/other_info_on_meta[@type="treatment_page"]')
                if not page:
                    meta = parsed_xml.findall('/meta/source')[0].getparent()
                    page_element =  etree.SubElement(meta, "other_info_on_meta", type="treatment_page")
                    page_element.text = str(page_num)

            #WRITE xml file
            parsed_xml.write(file_name, encoding= 'utf-8', xml_declaration= True, pretty_print= True)
    if corrected == "No":
        manual_fixes.append(name)

#CREATE csv file for manual changes
if manual_fixes:
    print ("Needs to be fixed manually")
    df = pd.DataFrame(columns=['name', 'volume', 'page'])
    for name in manual_fixes:
        row = index_csv[index_csv.name == name]
        df = df.append(row)
    df.to_csv('/home/lujantorob/bibilujan/index_synonyms/manual_fixes.csv', sep=',', index=False)

print ("Done!")

    

