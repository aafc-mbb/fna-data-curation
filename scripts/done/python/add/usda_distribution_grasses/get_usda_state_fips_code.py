
# coding: utf-8

# In[46]:


# requests allows sending of HTTP requests
import requests
# import html from lxml to parse the html as a tree
from lxml import html
import time
import sys

# add url pointing to the web page of interest
usda_url = 'https://plants.usda.gov/about_adv_search.html'
# access url with requests
page = requests.get(usda_url)
# html.fromstring treats the page.content as a string and then writes it to a tree that can be parse
tree = html.fromstring(page.content) 

# define the FIPS code and full name in the table of interest using xpath
# the xpath from the browser will add 'tbody' nodes; make sure to remove these as they are introduced by the browser and not necessarily part of the original html
# using tr[position() > 1] specifies that I want all rows after the first row in this table (first row is the header row and not useful here)
all_fips = []
fips_code = tree.xpath('/html/body/table/tr[5]/td/table/tr/td[4]/table/tr[3]/td[1]/table/tr[5]/td[2]/table/tr[position() > 2]/td[1]/text()')
for f_code in fips_code:
    f_code = f_code.strip()
    #print(f_code)
    all_fips.append(f_code)
#print(all_fips)
#print(len(all_fips))

all_states = []
state_name = tree.xpath('/html/body/table/tr[5]/td/table/tr/td[4]/table/tr[3]/td[1]/table/tr[5]/td[2]/table/tr[position() > 2]/td[2]/text()')
for s_name in state_name:
    s_name = s_name.encode('utf-8')
    s_name = s_name.strip()
    if "(the sole Caribbean member of the U.S. Minor Outlying Islands)" in s_name:
        s_name = s_name.replace("(the sole Caribbean member of the U.S. Minor Outlying Islands)", "")
    all_states.append(s_name)
#print(all_states)
#print(len(all_states))

for i in range(0, len(all_fips)):
    print(all_fips[i] + ',' + all_states[i])

