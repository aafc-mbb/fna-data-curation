
# coding: utf-8

# In[ ]:


import re
import glob
from lxml import etree
import pandas as pd

all_vol = ["24", "25"]

for volume in all_vol:
    directory = "/home/lujantorob/fna-data-curation/coarse_grained_fna_xml/V"+ volume +"/*.xml"
    for file_name in glob.glob(directory):
        locations_norepeats = []
        parser = etree.XMLParser(remove_blank_text=True)
        parsed_xml = etree.parse (file_name, parser)
        distribution =  parsed_xml.find('//description[@type="distribution"]')
        taxon_name_all = parsed_xml.findall('/taxon_identification[@status="ACCEPTED"]/taxon_name')
        rank = taxon_name_all[-1].attrib['rank']
        name = taxon_name_all[-1].text
        
        if distribution is not None:
            all_locations = distribution.text.split(';')
            for location in all_locations:
                if location not in locations_norepeats:
                    locations_norepeats.append(location)
            if len(all_locations) > len(locations_norepeats):
                new_distribution = ';'.join(locations_norepeats)
                print(distribution.text)
                distribution.text = new_distribution
                print(new_distribution)
                
        parsed_xml.write(file_name, encoding= 'utf-8', xml_declaration= True, pretty_print= True)

print("DONE!")

