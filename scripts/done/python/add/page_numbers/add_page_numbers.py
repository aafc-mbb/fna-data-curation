
# coding: utf-8

# This script was done to add page numbers to all families based on the index found at the end of V6

# In[ ]:


# First check if the volumes that do have the page numbers have treatment pages for all the families
# vol_w_page_num = ["4", "7", "8", "19-21", "23", "26", "27", "28"]
# vol_wo_page_num = ["2", "3", "5", "6", "9", "22", ]
# all_volumes = vol_w_page_num + vol_wo_page_num


# In[42]:


import glob
import re
import pandas as pd
from lxml import etree
fam_index = pd.read_csv('/home/lujantorob/Documents/Etc/Family_index.csv')

def addPageNumber(family):
    volume = parsed_xml.findall('/meta/other_info_on_meta[@type="volume"]')
    treatment_element = etree.Element("other_info_on_meta", type="treatment_page")
    volume[0].addnext(treatment_element)
    page_num = str(fam_index[fam_index.taxon.str.lower() == family]['page'].item())
    treatment_element.text = page_num 
    
    
def getTreatmentPages():
    treatment_page = parsed_xml.findall('/meta/other_info_on_meta[@type="treatment_page"]')
    if not treatment_page:
        return None 
    else:
        return treatment_page

for file_name in glob.glob("/home/lujantorob/fna-data-curation/coarse_grained_fna_xml/V2/*.xml"):
    parser = etree.XMLParser(remove_blank_text=True)
    parsed_xml = etree.parse (file_name, parser)
    write_file = False
    mention_page = parsed_xml.findall('/meta/other_info_on_meta[@type="mention_page"]')
    taxon_accepted_all = parsed_xml.findall('/taxon_identification[@status="ACCEPTED"]/taxon_name')

    if len(taxon_accepted_all) == 1 and getTreatmentPages() is None:
        family_name = taxon_accepted_all[0].text.lower()
        print (file_name + family_name)
        page_num = fam_index[fam_index.taxon.str.lower() == family_name]['page'].item()
        for page in mention_page:
            page_mention = int(page.text)
            if page_mention == page_num:
                write_file = True
                print ("Treatment page is the same as mention, fixing")
                page.attrib['type'] = 'treatment_page'
                break

        if getTreatmentPages() is None:
            write_file = True
            addPageNumber(family_name)
            #treatment_page = getTreatmentPages()

    if write_file == True:
        parsed_xml.write(file_name, encoding= 'utf-8', xml_declaration= True, pretty_print= True)

print ("Done!")

