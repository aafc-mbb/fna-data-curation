
# coding: utf-8

# In[1]:


from __future__ import print_function
import sys
# import ElementTree
#((import xml.etree.ElementTree as etree))
#import etree using lxml; using lxml instead of ElementTree because lxml allows for pretty-printing of output xml file
from lxml import etree
# import file of interest as a tree
#file = sys.argv[1]

import os
import re
import csv


# In[2]:


for subdir, dirs, files in os.walk("/Users/jocelynpender/fna/fna-data/fna-data-curation/coarse_grained_fna_xml/V6"):
    for file in files:
        print(file)
        filepath = subdir + os.sep + file
        # file = '/Users/jocelynpender/fna/fna-data/fna-data-curation/coarse_grained_fna_xml/V9/V9_364.xml'
        if (filepath.endswith(".xml")):
            tree = etree.parse(filepath)
            # define root
            root = tree.getroot()
            # find all taxon_identification with status=ACCEPTED
            for taxon_identification in root.findall("./taxon_identification/[@status='SYNONYM']"):
            # define empty arrays for rank and name
                rank=[]
                name=[]
            #loop through taxon_name to get rank and name
                for taxon_name in taxon_identification.findall('taxon_name'):

                    rank.append(taxon_name.get('rank'))
                    name.append(taxon_name.text)
            #define empty string for hierarchy
                hierarchy=""
            #print string 'name rank' for each <taxon_name> node, separated by ';'
                for i in range(0,len(rank)):
                    hierarchy+=(rank[i]+' '+name[i]+';')
            #	print(hierarchy)
            #create subelement called taxon_hierarchy
                if taxon_identification.find('taxon_hierarchy') and taxon_identification.find('taxon_hierarchy').text != '':
                    taxon_hierarchy=taxon_identification.find('taxon_hierarchy')
                else:
                    taxon_hierarchy=etree.SubElement(taxon_identification,'taxon_hierarchy')
            #append text from hierarchy to new subelement <taxon_hierarchy> but strip rightmost ';'
                taxon_hierarchy.text=hierarchy.rstrip(';')
            #write the output as a tree to subdirectory hierarchy, but keep file name as original
            tree.write(filepath, encoding= 'utf-8', xml_declaration=True, pretty_print=True)
            print(filepath)


# In[5]:


file = '/Users/jocelynpender/fna/fna-data/fna-data-curation/coarse_grained_fna_xml/V9/V6_242.xml'
if (filepath.endswith(".xml")):
    tree = etree.parse(filepath)
    # define root
    root = tree.getroot()
    # find all taxon_identification with status=ACCEPTED
    for taxon_identification in root.findall("./taxon_identification/[@status='BASIONYM']"):
        print(taxon_identification.find('taxon_hierarchy'))
        if taxon_identification.find('taxon_hierarchy') and taxon_identification.find('taxon_hierarchy').text != '':
            taxon_hierarchy=taxon_identification.find('taxon_hierarchy')

