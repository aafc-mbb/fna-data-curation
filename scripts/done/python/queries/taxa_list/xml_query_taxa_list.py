
# coding: utf-8

# # Query XML files for treeness, taxonomic names (i.e., synonyms) etc.

# ## Load packages

# In[1]:


from lxml import etree
import os
import re
import csv


# ## Utility functions

# In[2]:


def clean_semicolon(taxon_name):
    if taxon_name.endswith(";"):
        taxon_name = taxon_name[:-1]
    return(taxon_name)


# In[3]:


def build_rank_dict(taxon_identification):
    rank_dict = {
        "family" : "",
        "subfamily" : "",
        "tribe" : "",
        "subtribe" : "",
        "genus" : "",
        "subgenus" : "",
        "section" : "",
        "subsection" : "",
        "series" : "",
        "subseries" : "",
        "species" : "",
        "subspecies" : "",
        "variety" : ""
    }
    
    for taxon_name in taxon_identification.findall('./taxon_name'):
        for rank_name in rank_dict:
            if taxon_name.get('rank') == rank_name:
                rank_dict[rank_name] = taxon_name.text
    return(rank_dict)


# In[4]:


def format_name(taxon_identification):
    rank_dict = build_rank_dict(taxon_identification)
    abbrev = {
        "subfamily" : "subfam.",
        "tribe" : "tribe",
        "subtribe" : "subtribe",
        "subgenus" : "subg.",
        "section" : "sect.",
        "subsection" : "subsect.",
        "series" : "ser.",
        "subseries" : "subser.",
        "subspecies" : "subsp.",
        "variety" : "var."
    }
    treat_name = ''
    
    for taxon_name in taxon_identification.xpath('taxon_name[last()]'):
        rank_name = taxon_name.get('rank')
        if rank_name == 'family' or rank_name == 'genus':
            treat_name = rank_dict[rank_name]
        elif rank_name == 'subfamily' or rank_name == 'tribe':
            treat_name = (rank_dict['family'] + ' ' + abbrev[rank_name] + ' ' + rank_dict[rank_name])
        elif rank_name == 'subtribe':
            treat_name = (rank_dict['family'] + ' ' + abbrev['tribe'] + ' ' + rank_dict['tribe']
                          + abbrev['subtribe'] + ' ' + rank_dict[rank_name])        
        elif rank_name == 'subgenus' or rank_name == 'section':
            treat_name = (rank_dict['genus'] + ' ' + abbrev[rank_name] + ' ' + rank_dict[rank_name])
        elif rank_name == 'subsection':
            treat_name = (rank_dict['genus'] + ' ' + rank_dict['section'] 
                          + abbrev[rank_name] + ' ' + rank_dict[rank_name])
        elif rank_name == 'series':
            treat_name = (rank_dict['genus'] + ' ' 
                          + '(' + abbrev['section'] + ' ' + rank_dict['section'] + ')' 
                          + ' ' + abbrev[rank_name] +' '+ rank_dict[rank_name])
        elif rank_name == 'subseries':
            treat_name = (rank_dict['genus'] + ' ' + '(' + abbrev['section'] + ' ' + rank_dict['section'] + ')'
                         + ' ' + abbrev['series'] + ' ' + rank_dict['series']
                         + ' ' + abbrev['subseries'] + ' ' + rank_dict[rank_name])
        elif rank_name == 'species':
            treat_name = rank_dict['genus'] + ' ' + rank_dict[rank_name]
        elif rank_name == 'subspecies' or rank_name == 'variety':
            treat_name = rank_dict['genus'] + ' ' + rank_dict['species'] + ' ' + abbrev[rank_name] + ' ' + rank_dict[rank_name]
    return(treat_name)


# In[5]:


def authority(acceptednode):
    #if acceptednode.find("./taxon_name[@rank='species']") is not None:
     #   speciesauth = acceptednode.find("./taxon_name[@rank='species']").attrib['authority']

        # print(speciesauth)
        
      #  if acceptednode.find("./taxon_name[@rank='variety']") is not None:
       #     speciesauth = speciesauth + ' ' + acceptednode.find("./taxon_name[@rank='variety']").attrib['authority']
        #elif acceptednode.find("./taxon_name[@rank='subspecies']") is not None:
         #   speciesauth = speciesauth + ' ' + acceptednode.find("./taxon_name[@rank='subspecies']").attrib['authority']

        # print(speciesauth)
    #else:
    #print('test' + acceptednode.xpath('taxon_name[last()]')[0].text)
    speciesauth = acceptednode.xpath('taxon_name[last()]')[0].attrib['authority']
    return(speciesauth)


# In[6]:


def printvol(file_path):
    vol_text = re.search('\_\d*\.xml', file_path).start()
    vol_num = file_path[vol_text-2:vol_text]
    vol_reg = re.search('\d{1,}', vol_num)
    return(vol_reg.group())


# In[7]:


def clean_other_names(names):
    formatted = []
    for name in names: # I tried to use the other way of looping over synonyms. It didn't work well with findall variable structure
        if authority(name) is not None:
            formatted.append(format_name(name) + ' ' + authority(name))
        else:
            formatted.append(format_name(name))
    # print(formatted)
    formatted = ';'.join(formatted)
    return(formatted)


# In[8]:


def format_distribution(tree):
    distribution = tree.find("//description[@type='distribution']")
    if distribution is not None:
        distribution_text = distribution.text
    else:
        distribution_text = ''
    return(distribution_text)


# In[9]:


def collect_data(file_path):
#    file_path = '/Users/jocelynpender/fna/fna-data/fna-data-curation/coarse_grained_fna_xml/V6/V6_242.xml'
    tree = etree.parse(file_path)

    accepted_node = tree.find("//taxon_identification[@status='ACCEPTED']")
    ranks = build_rank_dict(accepted_node)
    taxon = format_name(accepted_node)
    # print(taxon)

    synonyms = tree.findall("//taxon_identification[@status='SYNONYM']")
    basionyms = tree.findall("//taxon_identification[@status='BASIONYM']")
    distribution = format_distribution(tree)
    
    taxon_hierarchy = accepted_node.find('./taxon_hierarchy').text

    row = ([file_path, printvol(file_path), ranks['family'], ranks['genus'], taxon_hierarchy,
            taxon, authority(accepted_node), 
            clean_other_names(basionyms), clean_other_names(synonyms), distribution])
    return(row)


# ## Run the script

# In[10]:


# Read in Output documents and run query

fields = ['File', 'Volume', 'AcceptedFamily', 'AcceptedGenus', 'AcceptedTaxonHierarchy', 'AcceptedTaxonName', 'AcceptedAuthority', 'BasionymTaxonNames', 'SynonymTaxonNames', 'Distribution']
file_name = "taxa_names_for_cam.csv"

with open(file_name, 'w') as csv_file:
    # Creating a csv writer object
    csvwriter = csv.writer(csv_file)
     
    # Writing the fields
    csvwriter.writerow(fields)
    for subdir, dirs, files in os.walk("../../../coarse_grained_fna_xml"):
        for file in files:
            #print(os.path.join(subdir, file))
            file_path = subdir + os.sep + file
            taxon = None

            if (file_path.endswith(".xml")):
                print("Searching" + file_path)
                row = collect_data(file_path)
                csvwriter.writerow(row)

