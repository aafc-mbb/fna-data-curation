
# coding: utf-8

# # Query XML files for treeness, taxonomic names (i.e., synonyms) etc.

# ## Load packages

# In[1]:


from lxml import etree
import os
import re
import csv


# ## Utility functions

# In[2]:


# Find introduced species

def findnative(tree, filepath):
    if tree.find("//character[@value='introduced']"): # If the species has been parsed as an introduced species, (i.e., introduced is a character)
        distparent = tree.find("//character[@value='introduced'].....")
        nativeness = ['introduced', distparent[0].text]
    elif tree.xpath("//*[text()[contains(.,'introduced')]]"): # If there is mention anywhere in the text of the species being introduced. This has to be manually cleaned up afterwards because it is not perfect
        distparent = tree.xpath("//*[text()[contains(.,'introduced')]]")
        nativeness = ['introduced', distparent[0].text]
    else: # If it is not introduced, we infer here that it is native
        nativeness = ['native', '']
    # print(row)
    return(nativeness)


# In[3]:


# Format species name from a taxon hierarchy tag

def nameformat(taxonname):
    genus = taxonname.find('genus')
    species = taxonname.find('species')

    # Collect the family name if it exists. Assume it is the first piece of the hierarchy    
    if 'family' in taxonname:
        familyname = taxonname[taxonname.find('family') + 7:taxonname.find(';')]
        familyname = familyname.capitalize()
    else:
        familyname = ''
            
    if 'section' in taxonname:
        section = taxonname.find('section')
        genusname = taxonname[genus + 6:section - 1]
    else:
        genusname = taxonname[genus + 6:species - 1]
    
    binomial = genusname + ' ' + taxonname[species + 8:]
    
    # Search and replace variety with var. and subpsecies with ssp. to match Arb's list!
    if 'variety' in binomial:
        binomial = binomial.replace(';variety', ' var.')
    elif 'subspecies' in binomial:
        binomial = binomial.replace(';subspecies', ' ssp.')
    
    if binomial[-1] == ';': # Trim off trailing ;
        binomial = binomial[0:-1]
    
    binomial = binomial.capitalize()
    genusname = genusname.capitalize()
    
    return([familyname, genusname, binomial])


# In[4]:


# Print the authorities

def authority(acceptednode):
    if acceptednode.find("./taxon_name[@rank='species']") is not None:
        speciesauth = acceptednode.find("./taxon_name[@rank='species']").attrib['authority']

        # print(speciesauth)
        
        if acceptednode.find("./taxon_name[@rank='variety']") is not None:
            speciesauth = speciesauth + ' ' + acceptednode.find("./taxon_name[@rank='variety']").attrib['authority']
        elif acceptednode.find("./taxon_name[@rank='subspecies']") is not None:
            speciesauth = speciesauth + ' ' + acceptednode.find("./taxon_name[@rank='subspecies']").attrib['authority']

        # print(speciesauth)
        return(speciesauth)


# ## Run the script

# In[9]:


# Read in Output documents and run query

fields = ['File', 'AcceptedFamily', 'AcceptedGenus', 'AcceptedTaxonName', 'AcceptedAuthority', 'SynonymTaxonNames', 'FNATreeText', 'Native', 'IntroducedText']
filename = "tree_taxa_synonyms.csv"

with open(filename, 'w') as csvfile:
    # Creating a csv writer object
    csvwriter = csv.writer(csvfile)
     
    # Writing the fields
    csvwriter.writerow(fields)
    for subdir, dirs, files in os.walk("../../Output"):
        for file in files:
            # print(os.path.join(subdir, file))
            filepath = subdir + os.sep + file
            taxon = None

            if (filepath.endswith(".xml") and re.search('archive', filepath) is None and re.search('processed', filepath) is None):
                # print("Searching" + filepath)
                query = None
                
                tree = etree.parse(filepath)
                if tree.find("//taxon_name[@rank='species']") is not None and tree.findall("//*[@name='tree']"):
                    query = "//*[@name='tree']..."
                elif tree.find("//taxon_name[@rank='species']") is not None and tree.findall("//*[@name='treelet']"):
                    query = "//*[@name='treelet']..."
                    # taxon = tree.find("//taxon_hierarchy").text
                if query:
                    # taxon = tree.find("//taxon_hierarchy").text
                    acceptednode = tree.find("//taxon_identification[@status='ACCEPTED']")
                    parent = tree.find(query) # Change this to allow for treelets
                    nativeness = findnative(tree, filepath)
                    taxon = nameformat(acceptednode.find("./taxon_hierarchy").text)
                    synonyms = tree.findall("//taxon_identification[@status='SYNONYM']/taxon_hierarchy")

                    for i in range(len(synonyms)): # I tried to use the other way of looping over synonyms. It didn't work well with findall variable structure
                        synonyms[i] = nameformat(synonyms[i].text)[2]
                    synonyms = ';'.join(synonyms)

                    row = [filepath, taxon[0], taxon[1], taxon[2], authority(acceptednode), synonyms, parent[0].text, nativeness[0], nativeness[1]]
                    print(row)
                    # csvwriter.writerow(row)

