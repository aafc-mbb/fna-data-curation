
# coding: utf-8

# This script was used to move the boxed codes to the "other_info_on_name" tag from "discussion". The boxed codes are described in the notebook "Fix_other_info_on_pub"

# In[6]:


import glob
import re
from lxml import etree

codes_dict = {"endemic":"E", "illustrated":"F", "weedy":"W", "introduced":"I", "of conservation concern":"C"}


for file_name in glob.glob("/home/lujantorob/fna-data-curation/coarse_grained_fna_xml/V3/*xml"):
    parsed_xml = etree.parse (file_name)
    tag_discussion = parsed_xml.findall('//discussion')
    tag_description = parsed_xml.findall('//description')
    boxed_code_list = []
    line_avoid = []
    for discussion in tag_discussion:
        if discussion.text in codes_dict:
            code = codes_dict[discussion.text]
            boxed_code_line = "    <other_info_on_name type=\"special_status\">"                                         + code                                         + "</other_info_on_name>" 
            boxed_code_list.append(boxed_code_line)
            if discussion.text != 'of conservation concern':
                line_avoid.append("<"+discussion.tag + ">" + discussion.text)
                
    for description in tag_description: 
        if description.attrib and description.attrib['type'] == 'distribution':
            distribution = description.text.split(";")
            for word in distribution:
                if word in codes_dict:
                    code = codes_dict[word]
                    boxed_code_line = "    <other_info_on_name type=\"special_status\">"                                         + code                                         + "</other_info_on_name>"
                    boxed_code_list.append(boxed_code_line)
                    
    file_input = open(file_name) 
    lines = file_input.readlines()
    file_input.close()
    file_output = open(file_name,"w") # Output has same name as input
    taxon_id_line = 0
    for line in lines:
        line = line.rstrip() # Remove trailing white space

        if ("</taxon_identification>" in line) and (taxon_id_line == 0): 
            if boxed_code_list:
                for code in boxed_code_list:
                    print (code, file=file_output)
            print(line, file=file_output)
            taxon_id_line += 1
            
        else:
            match = re.search (r">endemic|>illustrated|>weedy", line)
            if match is None:
                print (line, file=file_output)
    file_output.close()
                        

