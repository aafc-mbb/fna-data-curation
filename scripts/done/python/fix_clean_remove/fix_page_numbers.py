
# coding: utf-8

# Used this script to flag/remove/fix anything that wasn't numbers from the page number text.

# In[19]:


import glob
import re
from lxml import etree

for file_name in glob.glob("/home/lujantorob/fna-data-curation/coarse_grained_fna_xml/V28/*.xml"):
    parser = etree.XMLParser(remove_blank_text=True)
    parsed_xml = etree.parse (file_name, parser)
    write_file = False
    page_mention_list =  parsed_xml.findall('//other_info_on_meta[@type="mention_page"]')
    page_treatment_list =  parsed_xml.findall('//other_info_on_meta[@type="treatment_page"]')
    page_illustration_list =  parsed_xml.findall('//other_info_on_meta[@type="illustration_page"]')
    page_all_list = page_mention_list + page_treatment_list + page_illustration_list
    for page in page_all_list:
        symbol = re.search ( r'.+[^\d]', page.text )
        italics = re.findall ( r'i', page.text )
        line_break = re.search ( r'br', page.text )

        if symbol:
            write_file = True

            if line_break:
                page.text = page.text.replace('<br> ', '')

            elif len(italics) == 2:
                page.text = page.text.replace('<i>', '')
                page.text = page.text.replace('</i>', '')
                illustration_element = etree.Element("other_info_on_meta", type="illustration_page")
                illustration_element.text = page.text.replace('<i>', '')
                illustration_element.text = illustration_element.text.replace('</i>', '')
                page.addnext(illustration_element)

            else:
                write_file = False
                print ("the file" + file_name + " - " + page.text)
    
    if write_file == True:
        parsed_xml.write(file_name, encoding= 'utf-8', xml_declaration= True, pretty_print= True)

print ("Done!")


