
# coding: utf-8

# This script was used to parse the treatment name of the grasses, which was a mix of the name and authority. I decided to work on it by ranks, so it would be a shorter code and I could work more carefully. The cells that follow are a collection of very similar scripts but small changes depending on the rank.  
# 
# I got help from the list in V24 (Numerical Listing of the Family, Subfamilies, Tribes, and Genera, Volume 24) which has the family, subfamily, tribes and genera from which the hierarchy (of those ranks) can be inferred. I created a csv file from this index, and added the parent column manually by interpreting the orginal index. 
# 
# /home/lujantorob/bibilujan/index_synonyms/numerical_listing_v24-25.csv
# (previously /home/lujantorob/Documents/Etc/IndexFile_v24-25_2.csv)

# ### SUBFAMILY
# 

# In[13]:


import glob
from lxml import etree
from os.path import basename
import re
import os
import pandas as pd


## INPUT DESCRIPTION
volume = "V25"
directory = "/home/lujantorob/fna-data-curation/coarse_grained_fna_xml/"+ volume +"/*.xml"
#directory = "/home/lujantorob/Test/Vtest/files_keep/*.xml"

# GET ELEMENT THAT WILL BE ADDED 
parser = etree.XMLParser(remove_blank_text=True)
parsed_xml = etree.parse ("/home/lujantorob/fna-data-curation/coarse_grained_fna_xml/V24/V24_1.xml", parser)
taxon_accepted_all = parsed_xml.findall('/taxon_identification[@status="ACCEPTED"]/taxon_name')
family_element = taxon_accepted_all[0]

# LOAD INDEX FILE 
index_csv = pd.read_csv('/home/lujantorob/bibilujan/index_synonyms/numerical_listing_v24-25.csv')

for file_name in glob.glob(directory):
    #print (file_name)
    parser = etree.XMLParser(remove_blank_text=True)
    parsed_xml = etree.parse (file_name, parser)
    taxon_accepted_all = parsed_xml.findall('/taxon_identification[@status="ACCEPTED"]/taxon_name')
    #print (taxon_accepted_all[0].text)
    treatement_name = taxon_accepted_all[0].text
    treatement_rank = taxon_accepted_all[0].attrib['rank']
    if treatement_rank == "subfamily":
        print (treatement_name)
        
## ADD FAMILY taxon_name
        parent = taxon_accepted_all[0].getparent()
        parent.insert(0, family_element)
        
## FIX SUBFAMILY taxon_name   
        taxon_accepted_all[0].attrib['date'] = "unknown"
        #write_file = True
        subfamily_regex = re.search (r'([A-Z]+OIDEAE) (.*)', treatement_name)
        no_authority = re.search (r'([A-Z]+OIDEAE)', treatement_name)
        if subfamily_regex:
            subfamily_name = subfamily_regex.group(1)
            taxon_accepted_all[0].text = subfamily_name
            taxon_accepted_all[0].attrib['authority'] = subfamily_regex.group(2)
        else:
            if no_authority:
                subfamily_name = no_authority.group(0)
                taxon_accepted_all[0].text = subfamily_name
                taxon_accepted_all[0].attrib['authority'] = "unknown"
            else:
                print ("Wrong format" + treatement_name + filen_name)
                continue
                
## ADD taxon_hierarchy                
        hierarchy_element = etree.SubElement(parent, "taxon_hierarchy")
        taxon_accepted_all = parsed_xml.findall('/taxon_identification[@status="ACCEPTED"]/taxon_name')

        hierarchy = ''
        for taxon in taxon_accepted_all:
            rank_forhierachy = taxon.attrib['rank']
            name = taxon.text
            hierarchy = hierarchy + rank_forhierachy + " " + name.lower() + ";"
            hierarchy_element.text= hierarchy.rstrip(';')   
    
## ADD other_info_on_meta page # and volume#, remove <title>
        #subfamily_name = treatement_name.lower()
        #print (subfamily_name.lower())
        page_num = index_csv [index_csv.name.str.lower() == subfamily_name.lower()]['page'].item()
        volume = index_csv [index_csv.name.str.lower() == subfamily_name.lower()]['volume'].item()
        meta = parsed_xml.findall('/meta/source')[0].getparent()
        title = parsed_xml.findall('/meta/source/title')
        title[0].getparent().remove(title[0])
        #print(type(volume))        
        volume_element = etree.SubElement(meta, "other_info_on_meta", type="volume")
        volume_element.text = str(volume)
        page_element =  etree.SubElement(meta, "other_info_on_meta", type="treatment_page")
        page_element.text = str(page_num)

#PRINT file    
        parsed_xml.write(file_name, encoding= 'utf-8', xml_declaration= True, pretty_print= True)
    
print ("Done!")


# ### TRIBES 

# In[18]:


import glob
from lxml import etree
from os.path import basename
import re
import os
import pandas as pd


## INPUT DESCRIPTION
volume = "V25"
directory = "/home/lujantorob/fna-data-curation/coarse_grained_fna_xml/"+ volume +"/*.xml"
#directory = "/home/lujantorob/Test/Vtest/files_keep/*.xml"

# GET ELEMENT THAT WILL BE ADDED 
parser = etree.XMLParser(remove_blank_text=True)
parsed_xml = etree.parse ("/home/lujantorob/fna-data-curation/coarse_grained_fna_xml/V24/V24_1.xml", parser)
taxon_accepted_all = parsed_xml.findall('/taxon_identification[@status="ACCEPTED"]/taxon_name')
family_element = taxon_accepted_all[0]

# LOAD INDEX FILE 
index_csv = pd.read_csv('/home/lujantorob/bibilujan/index_synonyms/numerical_listing_v24-25.csv')

for file_name in glob.glob(directory):
    #print (file_name)
    parser = etree.XMLParser(remove_blank_text=True)
    parsed_xml = etree.parse (file_name, parser)
    taxon_accepted_all = parsed_xml.findall('/taxon_identification[@status="ACCEPTED"]/taxon_name')
    #print (taxon_accepted_all[0].text)
    treatement_name = taxon_accepted_all[0].text
    treatement_rank = taxon_accepted_all[0].attrib['rank']
    if treatement_rank == "tribe":
        if taxon_accepted_all > 1:
            continue
        print (treatement_name)
        
## ADD FAMILY taxon_name
        parent = taxon_accepted_all[0].getparent()
        parent.insert(0, family_element)
        
## FIX TRIBE taxon_name   
        taxon_accepted_all[0].attrib['date'] = "unknown"
        tribe_regex = re.search (r'([A-Z]+EAE) (.*)', treatement_name)
        no_authority = re.search (r'([A-Z]+EAE)', treatement_name)
        if tribe_regex:
            tribe_name = tribe_regex.group(1)
            taxon_accepted_all[0].text = tribe_name
            taxon_accepted_all[0].attrib['authority'] = tribe_regex.group(2)
        else:
            if no_authority:
                tribe_name = no_authority.group(0)
                taxon_accepted_all[0].text = tribe_name
                taxon_accepted_all[0].attrib['authority'] = "unknown"
            else:
                print ("Wrong format" + treatement_name + file_name)
                continue                
                
## ADD taxon_hierarchy                
        hierarchy_element = etree.SubElement(parent, "taxon_hierarchy")
        taxon_accepted_all = parsed_xml.findall('/taxon_identification[@status="ACCEPTED"]/taxon_name')
        hierarchy = ''
        for taxon in taxon_accepted_all:
            rank_forhierachy = taxon.attrib['rank']
            name = taxon.text
            hierarchy = hierarchy + rank_forhierachy + " " + name.lower() + ";"
            hierarchy_element.text= hierarchy.rstrip(';')   
    
## ADD other_info_on_meta page # and volume#, remove <title>
        page_num = index_csv [index_csv.name.str.lower() == tribe_name.lower()]['page'].item()
        volume = index_csv [index_csv.name.str.lower() == tribe_name.lower()]['volume'].item()
        meta = parsed_xml.findall('/meta/source')[0].getparent()
        title = parsed_xml.findall('/meta/source/title')
        title[0].getparent().remove(title[0])
        volume_element = etree.SubElement(meta, "other_info_on_meta", type="volume")
        volume_element.text = str(volume)
        page_element =  etree.SubElement(meta, "other_info_on_meta", type="treatment_page")
        page_element.text = str(page_num)

#PRINT file    
        parsed_xml.write(file_name, encoding= 'utf-8', xml_declaration= True, pretty_print= True)
    
print ("Done!")


# In[5]:


#ADD SUBFAMILY TO TRIBE

import glob
from lxml import etree
from os.path import basename
import re
import os
import pandas as pd


## INPUT DESCRIPTION
volume = "V25"
directory = "/home/lujantorob/fna-data-curation/coarse_grained_fna_xml/"+ volume +"/*.xml"

# LOAD INDEX FILE 
index_csv = pd.read_csv('/home/lujantorob/bibilujan/index_synonyms/numerical_listing_v24-25.csv')

for file_name in glob.glob(directory):
    #print (file_name)
    parser = etree.XMLParser(remove_blank_text=True)
    parsed_xml = etree.parse (file_name, parser)
    taxon_accepted_all = parsed_xml.findall('/taxon_identification[@status="ACCEPTED"]/taxon_name')
    #print (taxon_accepted_all[0].text)
    treatement_name = taxon_accepted_all[-1].text
    treatement_rank = taxon_accepted_all[-1].attrib['rank']
    if treatement_rank == "tribe":
        print (treatement_name)
        parent_name = index_csv [index_csv.name.str.lower() == treatement_name.lower()]['parent'].item()
        print (treatement_name + "'s parent is " + parent_name)

# GET ELEMENT THAT WILL BE ADDED 
        for file_name2 in glob.glob(directory):
            #parser = etree.XMLParser(remove_blank_text=True)
            parsed_xml2 = etree.parse (file_name2, parser)
            parent_taxon_accepted_all = parsed_xml2.findall('/taxon_identification[@status="ACCEPTED"]/taxon_name')
            #parent__accepted_name = parent_taxon_accepted_all[-1].text
            parent_rank = parent_taxon_accepted_all[-1].attrib['rank']
            if parent_rank == "subfamily":
                if parent_taxon_accepted_all[-1].text.lower() == parent_name.lower():
                    parent_element = parent_taxon_accepted_all[-1]
                    print (parent_element.text)
                    #break
                
## ADD PARENT taxon_name
        
        parent = taxon_accepted_all[0].getparent()
        parent.insert(1, parent_element)
        taxon_accepted_all = parsed_xml.findall('/taxon_identification[@status="ACCEPTED"]/taxon_name')
        print (len(taxon_accepted_all))
        
        taxon_accepted_all = parsed_xml.findall('/taxon_identification[@status="ACCEPTED"]/taxon_name')
        taxon_hierarchy = parsed_xml.findall('//taxon_hierarchy')
        hierarchy = ''
        for taxon in taxon_accepted_all:
            rank_forhierachy = taxon.attrib['rank']
            name = taxon.text
            hierarchy = hierarchy + rank_forhierachy + " " + name.lower() + ";"
            taxon_hierarchy[0].text= hierarchy.rstrip(';')             

        
        parsed_xml.write(file_name, encoding= 'utf-8', xml_declaration= True, pretty_print= True)
    
print ("Done!")


# In[1]:


#ADD SUBFAMILY and TRIBE to genera

import glob
from lxml import etree
from os.path import basename
import re
import os
import pandas as pd


## INPUT DESCRIPTION
volume = "V25"
directory = "/home/lujantorob/fna-data-curation/coarse_grained_fna_xml/"+ volume +"/*.xml"

# LOAD INDEX FILE 
index_csv = pd.read_csv('/home/lujantorob/bibilujan/index_synonyms/numerical_listing_v24-25.csv')

for file_name in glob.glob(directory):
    #print (file_name)
    parser = etree.XMLParser(remove_blank_text=True)
    parsed_xml = etree.parse (file_name, parser)
    taxon_accepted_all = parsed_xml.findall('/taxon_identification[@status="ACCEPTED"]/taxon_name')
    #print (taxon_accepted_all[0].text)
    treatement_name = taxon_accepted_all[-1].text
    treatement_rank = taxon_accepted_all[-1].attrib['rank']
    if treatement_rank == "genus":
        print (treatement_name)
        parent_name = index_csv [index_csv.name.str.lower() == treatement_name.lower()]['parent'].item()
        print (treatement_name + "'s parent is " + parent_name)

# GET ELEMENTs THAT WILL BE ADDED 
        for file_name2 in glob.glob(directory):
            #parser = etree.XMLParser(remove_blank_text=True)
            parsed_xml2 = etree.parse (file_name2, parser)
            parent_taxon_accepted_all = parsed_xml2.findall('/taxon_identification[@status="ACCEPTED"]/taxon_name')
            #parent__accepted_name = parent_taxon_accepted_all[-1].text
            parent_rank = parent_taxon_accepted_all[-1].attrib['rank']
            if parent_rank == "tribe":
                if parent_taxon_accepted_all[-1].text.lower() == parent_name.lower():
                    tribe_element = parent_taxon_accepted_all[-1]
                    subfamily_element = parent_taxon_accepted_all[-2]
                    print (subfamily_element.text)
                    #break
                
## ADD PARENT taxon_name
        
        parent = taxon_accepted_all[0].getparent()
        parent.insert(1, subfamily_element)
        parent.insert(2, tribe_element)

## FIX TAXON HIERARCHY
        taxon_accepted_all = parsed_xml.findall('/taxon_identification[@status="ACCEPTED"]/taxon_name')
        taxon_hierarchy = parsed_xml.findall('//taxon_hierarchy')
        hierarchy = ''
        for taxon in taxon_accepted_all:
            rank_forhierachy = taxon.attrib['rank']
            name = taxon.text
            hierarchy = hierarchy + rank_forhierachy + " " + name.lower() + ";"
            taxon_hierarchy[0].text= hierarchy.rstrip(';')             

        
        parsed_xml.write(file_name, encoding= 'utf-8', xml_declaration= True, pretty_print= True)
    
print ("Done!")


# ### GENUS

# In[7]:


import glob
from lxml import etree
from os.path import basename
import re
import os
import pandas as pd

## INPUT DESCRIPTION
volume = "V25"
directory = "/home/lujantorob/fna-data-curation/coarse_grained_fna_xml/"+ volume +"/*.xml"
#directory = "/home/lujantorob/Test/Vtest/files_keep/*.xml"

# GET ELEMENT THAT WILL BE ADDED 
parser = etree.XMLParser(remove_blank_text=True)
parsed_xml = etree.parse ("/home/lujantorob/fna-data-curation/coarse_grained_fna_xml/V24/V24_1.xml", parser)
taxon_accepted_all = parsed_xml.findall('/taxon_identification[@status="ACCEPTED"]/taxon_name')
family_element = taxon_accepted_all[0]

# LOAD INDEX FILE 
index_csv = pd.read_csv('/home/lujantorob/bibilujan/index_synonyms/numerical_listing_v24-25.csv')

for file_name in glob.glob(directory):
    #print (file_name)
    parser = etree.XMLParser(remove_blank_text=True)
    parsed_xml = etree.parse (file_name, parser)
    taxon_accepted_all = parsed_xml.findall('/taxon_identification[@status="ACCEPTED"]/taxon_name')
    #print (taxon_accepted_all[0].text)
    treatement_name = taxon_accepted_all[0].text
    treatement_rank = taxon_accepted_all[0].attrib['rank']
    if treatement_rank == "genus":
        if len(taxon_accepted_all) > 1:
            continue
        print (treatement_name)
        
## ADD FAMILY taxon_name
        parent = taxon_accepted_all[0].getparent()
        parent.insert(0, family_element)
        
## FIX TRIBE taxon_name   
        taxon_accepted_all[0].attrib['date'] = "unknown"
        genus_regex = re.search (r'(x*[A-Z]+) (.*)', treatement_name)
        no_authority = re.search (r'([A-Z]+)', treatement_name)
        if genus_regex:
            genus_name = genus_regex.group(1)
            taxon_accepted_all[0].text = genus_name
            taxon_accepted_all[0].attrib['authority'] = genus_regex.group(2)
        else:
            if no_authority:
                genus_name = no_authority.group(0)
                taxon_accepted_all[0].text = genus_name
                taxon_accepted_all[0].attrib['authority'] = "unknown"
            else:
                print ("Wrong format" + treatement_name + file_name)
                continue 
        
## ADD taxon_hierarchy                
        hierarchy_element = etree.SubElement(parent, "taxon_hierarchy")
        taxon_accepted_all = parsed_xml.findall('/taxon_identification[@status="ACCEPTED"]/taxon_name')
        hierarchy = ''
        for taxon in taxon_accepted_all:
            rank_forhierachy = taxon.attrib['rank']
            name = taxon.text
            hierarchy = hierarchy + rank_forhierachy + " " + name.lower() + ";"
            hierarchy_element.text= hierarchy.rstrip(';')   
    
## ADD other_info_on_meta page # and volume#, remove <title>
        print(genus_name)
        page_num = index_csv [index_csv.name.str.lower() == genus_name.lower()]['page'].item()
        volume = index_csv [index_csv.name.str.lower() == genus_name.lower()]['volume'].item()
        meta = parsed_xml.findall('/meta/source')[0].getparent()
        title = parsed_xml.findall('/meta/source/title')
        title[0].getparent().remove(title[0])
        volume_element = etree.SubElement(meta, "other_info_on_meta", type="volume")
        volume_element.text = str(volume)
        page_element =  etree.SubElement(meta, "other_info_on_meta", type="treatment_page")
        page_element.text = str(page_num)

#PRINT file    
        parsed_xml.write(file_name, encoding= 'utf-8', xml_declaration= True, pretty_print= True)
    
print ("Done!")




# ### SUBGENUS, section and subsection

# In[19]:


import glob
from lxml import etree
from os.path import basename
import re
import os
import pandas as pd

## INPUT DESCRIPTION
volume = "V24"
directory = "/home/lujantorob/fna-data-curation/coarse_grained_fna_xml/"+ volume +"/*.xml"
#directory = "/home/lujantorob/Test/Vtest/files_keep/*.xml"

# LOAD INDEX FILE 
index_csv = pd.read_csv('/home/lujantorob/bibilujan/index_synonyms/numerical_listing_v24-25.csv')

for file_name in glob.glob(directory):
    #print (file_name)
    parser = etree.XMLParser(remove_blank_text=True)
    parsed_xml = etree.parse (file_name, parser)
    taxon_accepted_all = parsed_xml.findall('/taxon_identification[@status="ACCEPTED"]/taxon_name')
    #print (taxon_accepted_all[0].text)
    treatement_name = taxon_accepted_all[0].text
    treatement_rank = taxon_accepted_all[0].attrib['rank']
#    if treatement_rank == "subgenus":
#    if treatement_rank == "section":
    if treatement_rank == "subsection":

        if len(taxon_accepted_all) > 1:
            continue
        #print (treatement_name)
        
## FIX SUBGENUS taxon_name   
        taxon_accepted_all[0].attrib['date'] = "unknown"
#         subgenus_regex = re.search (r'(.+) subg. ([A-Z]{1}[a-z]+) (.*)', treatement_name)
#         no_authority = re.search (r'(.+) subg. ([A-Z]{1}[a-z]+)', treatement_name)
        subgenus_regex = re.search (r'(.+) subsect. ([A-Z]{1}[a-z]+) (.*)', treatement_name)
        no_authority = re.search (r'(.+) subsect. ([A-Z]{1}[a-z]+)', treatement_name)
        if subgenus_regex:
            #print("Here" + treatement_name)
            parent_genus = subgenus_regex.group(1).partition(' ')[0]
            subgenus_name = subgenus_regex.group(2)
            taxon_accepted_all[0].text = subgenus_name
            taxon_accepted_all[0].attrib['authority'] = subgenus_regex.group(3)
            #print (subgenus_regex.group(1))
        else:
            if no_authority:
                #print("or here " + treatement_name)
                parent_genus = no_authority.group(1).partition(' ')[0]
                subgenus_name = no_authority.group(2)
                taxon_accepted_all[0].text = subgenus_name
                taxon_accepted_all[0].attrib['authority'] = "unknown"
            else:
                print ("Wrong format" + treatement_name + file_name)
                continue 
                
# GET ELEMENTs THAT WILL BE ADDED 
        for file_name2 in glob.glob(directory):
            #parser = etree.XMLParser(remove_blank_text=True)
            parsed_xml2 = etree.parse (file_name2, parser)
            parent_taxon_accepted_all = parsed_xml2.findall('/taxon_identification[@status="ACCEPTED"]/taxon_name')
            #parent__accepted_name = parent_taxon_accepted_all[-1].text
            parent_rank = parent_taxon_accepted_all[-1].attrib['rank']
            if parent_rank == "genus":
                if parent_taxon_accepted_all[-1].text.lower() == parent_genus.lower():
                    family_element = parent_taxon_accepted_all[0]                   
                    subfamily_element = parent_taxon_accepted_all[1]
                    tribe_element = parent_taxon_accepted_all[2]                    
                    genus_element = parent_taxon_accepted_all[3]
                    print (subfamily_element.text)
                    #break
                
## ADD PARENTS taxon_name
        
        parent = taxon_accepted_all[0].getparent()
        parent.insert(0, family_element)
        parent.insert(1, subfamily_element)        
        parent.insert(2, tribe_element)
        parent.insert(3, genus_element)


                
## ADD taxon_hierarchy                
        hierarchy_element = etree.SubElement(parent, "taxon_hierarchy")
        taxon_accepted_all = parsed_xml.findall('/taxon_identification[@status="ACCEPTED"]/taxon_name')
        hierarchy = ''
        for taxon in taxon_accepted_all:
            rank_forhierachy = taxon.attrib['rank']
            name = taxon.text
            hierarchy = hierarchy + rank_forhierachy + " " + name.lower() + ";"
            hierarchy_element.text= hierarchy.rstrip(';')   

## ADD other_info_on_meta page # and volume#, remove <title>
        meta = parsed_xml.findall('/meta/source')[0].getparent()
        title = parsed_xml.findall('/meta/source/title')
        title[0].getparent().remove(title[0])
        volume_element = etree.SubElement(meta, "other_info_on_meta", type="volume")
        volume_element.text = str(volume[1:])

#PRINT file    
        parsed_xml.write(file_name, encoding= 'utf-8', xml_declaration= True, pretty_print= True)
    
print ("Done!")





# ### SPECIES 

# In[9]:


import glob
from lxml import etree
from os.path import basename
import re
import os
import pandas as pd

## INPUT DESCRIPTION
volume = "V25"
directory = "/home/lujantorob/fna-data-curation/coarse_grained_fna_xml/"+ volume +"/*.xml"
#directory = "/home/lujantorob/Test/Vtest/files_keep/*.xml"

# LOAD INDEX FILE 
index_csv = pd.read_csv('/home/lujantorob/bibilujan/index_synonyms/numerical_listing_v24-25.csv')

for file_name in glob.glob(directory):
    #print (file_name)
    parser = etree.XMLParser(remove_blank_text=True)
    parsed_xml = etree.parse (file_name, parser)
    taxon_accepted_all = parsed_xml.findall('/taxon_identification[@status="ACCEPTED"]/taxon_name')
    #print (taxon_accepted_all[0].text)
    treatement_name = taxon_accepted_all[0].text
    treatement_rank = taxon_accepted_all[0].attrib['rank']
    if treatement_rank == "species":

        if len(taxon_accepted_all) > 1:
            continue
        #print (treatement_name)
        
## FIX SPECIES taxon_name   
        taxon_accepted_all[0].attrib['date'] = "unknown"
        species_regex = re.search (r'([A-Z]{1}[a-z]+) ([a-z]+) (.*)', treatement_name)
        no_authority = re.search (r'([A-Z]{1}[a-z]+) ([a-z]+)', treatement_name)
        if species_regex:
            print("Here" + treatement_name)
            parent_genus = species_regex.group(1)
            species_name = species_regex.group(2)
            taxon_accepted_all[0].text = species_name
            taxon_accepted_all[0].attrib['authority'] = species_regex.group(3)
            print (species_regex.group(1))
        else:
            if no_authority:
                #print("or here " + treatement_name)
                parent_genus = no_authority.group(1)
                species_name = no_authority.group(2)
                taxon_accepted_all[0].text = species_name
                taxon_accepted_all[0].attrib['authority'] = "unknown"
            else:
                print ("Wrong format" + treatement_name + file_name)
                continue 
                
# GET ELEMENTs THAT WILL BE ADDED 
        for file_name2 in glob.glob(directory):
            #parser = etree.XMLParser(remove_blank_text=True)
            parsed_xml2 = etree.parse (file_name2, parser)
            parent_taxon_accepted_all = parsed_xml2.findall('/taxon_identification[@status="ACCEPTED"]/taxon_name')
            #parent__accepted_name = parent_taxon_accepted_all[-1].text
            parent_rank = parent_taxon_accepted_all[-1].attrib['rank']
            if parent_rank == "genus":
                if parent_taxon_accepted_all[-1].text.lower() == parent_genus.lower():
                    #print (parent_taxon_accepted_all[-1])
                    family_element = parent_taxon_accepted_all[0]                   
                    subfamily_element = parent_taxon_accepted_all[1]
                    tribe_element = parent_taxon_accepted_all[2]                    
                    genus_element = parent_taxon_accepted_all[3]
                    #break
                
## ADD PARENTS taxon_name
        
        parent = taxon_accepted_all[0].getparent()
        parent.insert(0, family_element)
        parent.insert(1, subfamily_element)        
        parent.insert(2, tribe_element)
        parent.insert(3, genus_element)


                
## ADD taxon_hierarchy                
        hierarchy_element = etree.SubElement(parent, "taxon_hierarchy")
        taxon_accepted_all = parsed_xml.findall('/taxon_identification[@status="ACCEPTED"]/taxon_name')
        hierarchy = ''
        for taxon in taxon_accepted_all:
            rank_forhierachy = taxon.attrib['rank']
            name = taxon.text
            hierarchy = hierarchy + rank_forhierachy + " " + name.lower() + ";"
            hierarchy_element.text= hierarchy.rstrip(';')   

## ADD other_info_on_meta page # and volume#, remove <title>
        meta = parsed_xml.findall('/meta/source')[0].getparent()
        title = parsed_xml.findall('/meta/source/title')
        title[0].getparent().remove(title[0])
        volume_element = etree.SubElement(meta, "other_info_on_meta", type="volume")
        volume_element.text = str(volume[1:])

#PRINT file    
        parsed_xml.write(file_name, encoding= 'utf-8', xml_declaration= True, pretty_print= True)
    
print ("Done!")





# ### SUBSPECIES and variety

# In[15]:


import glob
from lxml import etree
from os.path import basename
import re
import os
import pandas as pd

## INPUT DESCRIPTION
volume = "V25"
directory = "/home/lujantorob/fna-data-curation/coarse_grained_fna_xml/"+ volume +"/*.xml"
#directory = "/home/lujantorob/Test/Vtest/files_keep/*.xml"

# LOAD INDEX FILE 
index_csv = pd.read_csv('/home/lujantorob/bibilujan/index_synonyms/numerical_listing_v24-25.csv')

for file_name in glob.glob(directory):
    #print (file_name)
    parser = etree.XMLParser(remove_blank_text=True)
    parsed_xml = etree.parse (file_name, parser)
    taxon_accepted_all = parsed_xml.findall('/taxon_identification[@status="ACCEPTED"]/taxon_name')
    #print (taxon_accepted_all[0].text)
    treatement_name = taxon_accepted_all[0].text
    treatement_rank = taxon_accepted_all[0].attrib['rank']
#     if treatement_rank == "subspecies":
    if treatement_rank == "variety":


        if len(taxon_accepted_all) > 1:
            continue
        #print (treatement_name)
        
## FIX SPECIES taxon_name   
        taxon_accepted_all[0].attrib['date'] = "unknown"
#         subspecies_regex = re.search (r'([A-Z]{1}[a-z]+) ([a-z]+).* subsp. ([a-z]+) (.*)', treatement_name)
#         no_authority = re.search (r'([A-Z]{1}[a-z]+) ([a-z]+).* subsp. ([a-z]+)', treatement_name)
        subspecies_regex = re.search (r'([A-Z]{1}[a-z]+) ([a-z]+).* var. ([a-z]+) (.*)', treatement_name)
        no_authority = re.search (r'([A-Z]{1}[a-z]+) ([a-z]+).* var. ([a-z]+)', treatement_name)

        if subspecies_regex:
            #print("Here" + treatement_name)
            parent_genus = subspecies_regex.group(1)
            parent_species = subspecies_regex.group(2)
            subspecies_name = subspecies_regex.group(3)

            taxon_accepted_all[0].text = subspecies_name
            taxon_accepted_all[0].attrib['authority'] = subspecies_regex.group(4)
            #print (subspecies_regex.group(1))
        else:
            if no_authority:
                #print("or here " + treatement_name)
                parent_genus = no_authority.group(1)  
                parent_species = no_authority.group(2)
                subspecies_name = no_authority.group(3)
                taxon_accepted_all[0].text = subspecies_name
                taxon_accepted_all[0].attrib['authority'] = "unknown"
            else:
                print ("Wrong format" + treatement_name + file_name)
                continue 
                
# GET ELEMENTs THAT WILL BE ADDED 
        for file_name2 in glob.glob(directory):
            #parser = etree.XMLParser(remove_blank_text=True)
            parsed_xml2 = etree.parse (file_name2, parser)
            parent_taxon_accepted_all = parsed_xml2.findall('/taxon_identification[@status="ACCEPTED"]/taxon_name')
            #parent__accepted_name = parent_taxon_accepted_all[-1].text
            parent_rank = parent_taxon_accepted_all[-1].attrib['rank']
            if parent_rank == "species":
                if parent_taxon_accepted_all[-1].text.lower() == parent_species.lower()                 and parent_taxon_accepted_all[-2].text.lower() == parent_genus.lower():
                    #print (parent_taxon_accepted_all[-1])
                    family_element = parent_taxon_accepted_all[0]                   
                    subfamily_element = parent_taxon_accepted_all[1]
                    tribe_element = parent_taxon_accepted_all[2]                    
                    genus_element = parent_taxon_accepted_all[3]
                    species_element = parent_taxon_accepted_all[4]
                    #break
                
## ADD PARENTS taxon_name
        
        parent = taxon_accepted_all[0].getparent()
        parent.insert(0, family_element)
        parent.insert(1, subfamily_element)        
        parent.insert(2, tribe_element)
        parent.insert(3, genus_element)
        parent.insert(4, species_element)


                
## ADD taxon_hierarchy                
        hierarchy_element = etree.SubElement(parent, "taxon_hierarchy")
        taxon_accepted_all = parsed_xml.findall('/taxon_identification[@status="ACCEPTED"]/taxon_name')
        hierarchy = ''
        for taxon in taxon_accepted_all:
            rank_forhierachy = taxon.attrib['rank']
            name = taxon.text
            hierarchy = hierarchy + rank_forhierachy + " " + name.lower() + ";"
            hierarchy_element.text= hierarchy.rstrip(';')   

## ADD other_info_on_meta page # and volume#, remove <title>
        meta = parsed_xml.findall('/meta/source')[0].getparent()
        title = parsed_xml.findall('/meta/source/title')
        title[0].getparent().remove(title[0])
        volume_element = etree.SubElement(meta, "other_info_on_meta", type="volume")
        volume_element.text = str(volume[1:])

#PRINT file    
        parsed_xml.write(file_name, encoding= 'utf-8', xml_declaration= True, pretty_print= True)
    
print ("Done!")





# Old format of names

# In[ ]:


#family - add fammily taxon identification to all 

number_of_words[subfamily] = 1 #CENTOTHECOIDEAE Soderstr.
# you can know the subfamily from the spreadsheet
number_of_words[tribe] = 1 #ARISTIDEAE C.E. Hubb.
#All should have a subfamily ? and a tribe?
number_of_words[genus] = 1 #TRAGUS Haller


number_of_words[subgenus] = 3 #care about 3rd # Setaria subg. Reverchoniae W.E. Fox
number_of_words[section] = 3 #care about 3rd #Dichanthelium sect. Pedicellata (Hitchc. &amp; Chase) Freckmann &amp; Lelong
number_of_words[subsection] = 3 #Poa subsect. Secundae Soreng

number_of_words[species] = 2 #Rytidosperma penicillatum (Labill.) Connor &amp; Edgar
number_of_words[variety] = 4 #care about the 4th #Sporobolus compositus var. drummondii (Trin.) Kartesz &amp; Gandhi
number_of_words[subspecies] = 4 #as above #Setaria pumila subsp. pallidefusca (Schumach.) B.K. Simon


