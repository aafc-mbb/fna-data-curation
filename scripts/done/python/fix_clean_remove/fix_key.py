
# coding: utf-8

# This script uses regular expressions to split the key statements and determination which looked like this:
#   <br>  <description type="morphology">Calyptrae not plicate; setae cygneous16 – 1. Campylostelium , v. 27, p. 307</description> 
#   <br> to 
#   <br> Calyptrae not plicate; setae cygneous.
#   <br>Determination: 
#   <br>16 – 1. Campylostelium

# In[ ]:


import glob
import re
from lxml import etree

for file_name in glob.glob("/home/lujantorob/fna-data-curation/coarse_grained_fna_xml/V28/V28_1004.xml"):
    parsed_xml = etree.parse (file_name)
    write_file = False
    description_list =  parsed_xml.findall('//description[@type="morphology"]')
    for description in description_list:
        has_determination = re.search ( r'(\d+ – \d+\..+) , v\..+$', description.text)
        has_subkey = re.search ( r'(Subkey [A-Z]).+$', description.text)
        has_inpart= re.search ( r'(\d+ – \d+\..+)\(in part\).+$', description.text)
        regex_list = (has_determination, has_subkey, has_inpart)

        for regex in regex_list:
             if regex is not None: 
                write_file = True
                match = regex.group()
                description.text = description.text.replace(regex.group(), '') + "."
                next_statment = description.getnext()
                if next_statment is not None:
                    next_statment.tag = "determination"
                    next_statment.text = regex.group(1).rstrip()

    if write_file == True:
        parsed_xml.write(file_name, encoding= 'utf-8', xml_declaration= True)

