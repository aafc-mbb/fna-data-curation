from __future__ import print_function
import sys
# import ElementTree
#((import xml.etree.ElementTree as etree))
#import etree using lxml; using lxml instead of ElementTree because lxml allows for pretty-printing of output xml file
from lxml import etree
# import file of interest as a tree
file = sys.argv[1]
tree = etree.parse(file)
# define root
root = tree.getroot()
# find all <references>
for references in root.findall("./references"):
	for reference in references.findall("./reference"):
# define empty array for ref_split
	#ref_split = []
# define text in <references> as a string
    		refs = reference.text
# remove old block of all_references (original <reference>) from <references>
		references.remove(reference)
# split ref string into list, delimiter is ;
		ref_split = refs.split(";")
#write each split-reference to new <reference> tag
		for i in range(0, len(ref_split)):
# create new subelement called <reference>
			reference = etree.SubElement(references, "reference")
# write content of split-reference to <reference>
			reference.text = ref_split[i]
		#print reference
#write the output as a tree to subdirectory parsed_refs, but keep file name as original
tree.write('parsed_refs/'+file, xml_declaration=True, pretty_print=True)

