
# coding: utf-8

# Used the keys from the efloras and copied them to a csv, and used the csv to parse the keys.

# In[ ]:


import re
from difflib import SequenceMatcher
import csv
import pandas as pd
import collections
import glob
from lxml import etree
from os.path import basename

parser = etree.XMLParser(remove_blank_text=True)
parsed_xml = etree.parse ("/home/lujantorob/fna-data-curation/coarse_grained_fna_xml/V5/V5_1245.xml", parser)
references_all = parsed_xml.findall('/references')
key_element = etree.Element("key")
references_all[0].addnext(key_element)

plates_csv = pd.read_csv('/home/lujantorob/Documents/Etc/caryophyllaceae_keys.csv', names = ["statement_number", "text", "determination"])
#print (plates_csv.head())
counter = 0

with open('/home/lujantorob/Documents/Etc/caryophyllaceae_keys.csv', 'rt') as keys_file:
    reader = csv.reader(keys_file, delimiter=',')
    statement_id_next = None
    for index, row in enumerate(reader):
        #print (row[0])
        key_statement_element = etree.Element("key_statement")
        key_element.append(key_statement_element)
        statement_id_element = etree.Element("statement_id")
        key_statement_element.append(statement_id_element)
        if row[0] != "+":
            id_text = re.search ( r'(\d+).*', row[0])
            statement_id_element.text = id_text.group(1)
            statement_id_next = id_text.group(1)

        else:
            statement_id_element.text = statement_id_next
            #print (statement_id_element.text)
        description_element = etree.Element("description", type="morphology")
        description_element.text = row[1]
        statement_id_element.addnext(description_element)
        if row[2].startswith('-'):
            #print ("HEY")
            next_statement_element = etree.Element("next_statement_id")
            row[2] = row[2].replace('-', '')
            next_statement_element.text = row[2]
            description_element.addnext(next_statement_element)
        else:
            determination_element = etree.Element("determination")
            row[2] = row[2].replace('(in part)', '')
            determination_element.text = row[2]
            description_element.addnext(determination_element)

        
print ("Done")

parsed_xml.write("/home/lujantorob/fna-data-curation/coarse_grained_fna_xml/V5/V5_1245.xml", encoding= 'utf-8', xml_declaration= True, pretty_print= True)

