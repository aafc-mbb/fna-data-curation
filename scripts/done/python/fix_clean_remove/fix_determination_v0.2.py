
# coding: utf-8

# In[ ]:


#used this to remove page numbers, (in part) and add species name to subsp. and var. determinations: 
#"subsp. something" to "genus species subsp. something"

import glob
import re
from lxml import etree

for file_name in glob.glob("/home/lujantorob/fna-data-curation/coarse_grained_fna_xml/V25/*xml"):
    parsed_xml = etree.parse (file_name)
    write_file = False
    determination_list = parsed_xml.findall('//determination')
    for determination in determination_list:
        #while has_page_in_part is not None or has_page is not None or has_no_species is not None:
        has_page_in_part = re.search (r'(\d+[a-z]*\s.*)\(in part.*', determination.text)
        has_page = re.search ( r'(\d+[a-z]*.*)\,.*', determination.text)
        has_no_species = re.search (r'(\d+[a-z]*\s)((?:subsp|var)\.\s.*)', determination.text)
            
        if has_page_in_part is not None:
            write_file = True
            determination.text = has_page_in_part.group(1).rstrip()
        elif has_page is not None:
            write_file = True
            determination.text = has_page.group(1)
        elif has_no_species:
            write_file = True
            genus_accepted_all = parsed_xml.findall('/taxon_identification[@status="ACCEPTED"]/taxon_name[@rank="genus"]')
            genus = genus_accepted_all[0].text.title() #title to capitalize the first letter
            species_accepted_all = parsed_xml.findall('/taxon_identification[@status="ACCEPTED"]/taxon_name[@rank="species"]')
            if not species_accepted_all:
                write_file = False
                print ("No species in file " + file_name + " fix manually")
                break 
            species= species_accepted_all[0].text
            determination.text = has_no_species.group(1) + genus + " " + species + " " + has_no_species.group(2)
            determination.text = re.sub (r'<', '', determination.text)

    if write_file == True:
        parsed_xml.write(file_name, encoding= 'utf-8', xml_declaration= True)
                
                   
                


# Be careful with Group XX, might delete the word Group

# In[ ]:


#used this to remove volume numbers after I had run the previous cell 

import glob
import re
from lxml import etree

for file_name in glob.glob("/home/lujantorob/fna-data-curation/coarse_grained_fna_xml/V19-20-21/*xml"):
    parsed_xml = etree.parse (file_name)
    write_file = False
    determination_list = parsed_xml.findall('//determination')
    for determination in determination_list:
        has_page = re.search ( r'(\d+[a-z]*.*)\, v\..*', determination.text)
        if has_page is not None:
            write_file = True
            determination.text = has_page.group(1)

    if write_file == True:
        parsed_xml.write(file_name, encoding= 'utf-8', xml_declaration= True)
                
                   
                

