
# coding: utf-8

# This tag "other_info_on_pub" is incorrectly used in volumes 6, 9, and 28. They have the common name, boxed code and sometimes the etymology. <br>
# 
# ### <font color= green>Key to codes</font>
# 
# C = of conservation concern <br>
# E = endemic to the flora area <br>
# F = illustrated <br>
# I = introduced to the flora area <br>
# W = weedy, based mostly on <br>
# 
# “Of conservation concern” applies to species that are globally rare or threatened based on NatureServe’s 
# G1 or G2 designations or infraspecific taxa designa
# ted as T1 or T2 (http://natureserve.org) or other 
# compelling sources.  
# 
# “Introduced” is defined broadly to mean the plant was 
# released accidentally or deliberately into the flora 
# area and now persists without cultivation (i.e., is naturalized).  
# 
# “Weedy” applies to taxa listed by the State and Federal Composite List of All U.S. Noxious Weeds (http://plants.usda.gov/java/noxiousDriver) or the Invader Database System (http://invader.dbs.umt.edu).  
# 
# 

# In[ ]:


# Regular expressions used in this code

# \b([A-Z]+)(?:$|\s)

# match 
#     [A-Z]:  capital letters 
#     +: 1 or more  
#     /b: delimited by a word boundary to the left
#     and 
#     \s or $: a whitespace or the end of the string to the right 
#     In regular expressions, the parenthesis indicate capture groups that can be retrieved  
#     (:?): this indicates non-capture group therefore the space wont be capturable, while the letters A-Z are.

# \[.+\]  

# match 
#     \[: square brackets
#     .+ any character once or more
#     \]: closing square bracket


# In[24]:


import glob
import re
from lxml import etree

for file_name in glob.glob("/home/lujantorob/etc_processing_files/coarse_grained_fna_xml/V6/*xml"):
    parsed_xml = etree.parse (file_name)
    dirty_tag = parsed_xml.findall('//other_info_on_pub')
    boxed_code_list = []
    common_names = []
    etymology = None
    if dirty_tag:
        match = re.search (r'^(\(not|not|based on|as hybrid|as species)', dirty_tag[0].text)
        if match is None:

#1       # Extract boxed codes, etymology and common names
            avoid_line = "<other_info_on_pub>" + dirty_tag[0].text + "</other_info_on_pub>"
            tag_content = dirty_tag[0].text # Evaluate only the first tag
            tag_content= re.sub(r"\* ", '', tag_content)
            all_codes = re.finditer(r'\b([A-Z]+)(?:$|\s)', tag_content) # See above
            if all_codes:
                for boxed_code in all_codes:
                    tag_content= tag_content[:boxed_code.start()].rstrip()
                    boxed_code_list += list(boxed_code.group(1)) # Add each character (list()) into the main boxed code list, use group (1) to get the capture group      
            etymology = re.search (r"\[.+\]", tag_content) # See above
            if etymology:
                tag_content= tag_content[:etymology.start()].rstrip()
            if tag_content:
                common_names = tag_content.split(",")
                   
#2      # Rewrite the file with the correct info
        input_file = open(file_name) 
        lines = input_file.readlines()
        input_file.close()
        output = open(file_name,"w") # Output has same name as input
        taxon_id_line = 0
        for line in lines:
            line = line.rstrip() # Remove trailing white space
            
            if ("</taxon_identification>" in line) and (taxon_id_line == 0): 
                if etymology:
                    etymology_line = "    <other_info_on_name type=\"etymology\">"                                     + etymology.group()                                     + "</other_info_on_name>"
                    print (etymology_line, file=output)
                if boxed_code_list:
                    for code in boxed_code_list:
                        boxed_code_line = "    <other_info_on_name type=\"special_status\">"                                         + code                                         + "</other_info_on_name>" 
                        print (boxed_code_line, file=output)                        
                print (line, file=output)
                taxon_id_line += 1
                
            elif "<number>" in line:
                print(line, file=output)
                if common_names:
                    for name in common_names:
                        common_name_line = "  <other_name type=\"common_name\">"                                             + name.strip()                                             + "</other_name>" 
                        print (common_name_line, file=output)
            else:
                if avoid_line not in line: 
                    print(line, file=output) 
        output.close()

