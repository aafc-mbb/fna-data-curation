
# coding: utf-8

# Fix csv based on file names and the output from the import log. Take the files that could not find and compare to file list and use a sequence match to find the most similar string with at least 70% similarity
# 
# 1. Create a file with the names of all the plates in a volume by:
# ls > V(volume#)-platenames.txt
# 2. Replace variable volume with the volume you are looking at 
# 3. Downloads/output.txt is the log from the link plates to csv output that contains the files that were not found based on plates.csv
# 
# -Note: it only works when the plates names have the volume name in the file, ie different approach was taken with V27 and V28. (script follows)

# In[14]:


import re
from difflib import SequenceMatcher
import csv
import pandas as pd
import collections

volume = "9"
f = open ('/home/lujantorob/fna-plates/plates/FNA0'+volume+'/jpeg/V'+volume+'-platenames.txt')
all_files_invol = f.readlines()

f = open ('/home/lujantorob/Downloads/output.txt')
log = f.readlines()

plates_csv = pd.read_csv('/home/lujantorob/bibilujan/plates.csv', names = ["taxon", "file_name"])

good_files = []
bad_files = []
no_fix_files = []

for index, log_line in enumerate(log):
    fna = re.search (r'(?:fna|FNA)('+volume+')[^\d]', log_line)
    if fna:
        if log_line != log[-1] and "Couldn't find file page" in log[index + 1]:
            bad_files.append(log_line.rstrip())
            print(log_line)
        
        
        elif log_line != log[-1] and "Done!" in log[index + 1]:
            good_files.append(log_line.rstrip())
            
print (bad_files)

for bad_file in bad_files:
    
    match_dic = {}
    all_matches = []
    print ("Start" + bad_file)
    if bad_file in all_files_invol:
        print ("Careful!"+ file)
    else:
        for file in all_files_invol:
            file = file.replace('.jpeg', '')
            match = SequenceMatcher(None, file, bad_file)
            if match.ratio() > 0.7:
                match_dic[match.ratio()]=file.rstrip()
                all_matches.append(match.ratio())
                print (match_dic)
                
        if match_dic:        
            ordered_match = sorted(all_matches)
            greatest_hit = match_dic[ordered_match[-1]]
            print ("Found a match" + greatest_hit)
            print (bad_file + greatest_hit)
            plates_csv['file_name'] = plates_csv['file_name'].replace(bad_file, greatest_hit)
        
        else:
            no_fix_files.append(bad_file)

print (no_fix_files)
print (len(no_fix_files))

plates_csv.to_csv('/home/lujantorob/bibilujan/plates.csv', sep=',', header=False, index=False)




# Since V27/V28 dont have the volume number in the file name I had to manually put the log lines that belonged to each volume in a separate file. This is read, to then find the matching name in the platenames.txt (created by ls > V#-platenames.text). 

# In[ ]:


import re
from difflib import SequenceMatcher
import csv
import pandas as pd
import collections

volume = "28"
f = open ('/home/lujantorob/fna-plates/plates/FNA'+volume+'/jpeg/V'+volume+'-platenames.txt')
all_files_invol = f.readlines()

f = open ('/home/lujantorob/Downloads/log_V28.txt')
log = f.readlines()

plates_csv = pd.read_csv('/home/lujantorob/bibilujan/plates.csv', names = ["taxon", "file_name"])

good_files = []
bad_files = []
no_fix_files = []

for index, log_line in enumerate(log):
    #fna = re.search (r'(?:fna|FNA)('+volume+')[^\d]', log_line)
    #if fna:
    if log_line != log[-1] and "Couldn't find file page" in log[index + 1]:
        bad_files.append(log_line.rstrip())  


    elif log_line != log[-1] and "Done!" in log[index + 1]:
        good_files.append(log_line.rstrip())
print (len(bad_files))            
print (bad_files)


for bad_file in bad_files:
    
    match_dic = {}
    all_matches = []
    print ("Start" + bad_file)
    if bad_file in all_files_invol:
        print ("Careful!"+ file)
    else:
        for file in all_files_invol:
            file = file.replace('.jpeg', '')
            match = SequenceMatcher(None, file, bad_file)
            if match.ratio() > 0.7:
                match_dic[match.ratio()]=file.rstrip()
                all_matches.append(match.ratio())
                
        if match_dic:        
            ordered_match = sorted(all_matches)
            greatest_hit = match_dic[ordered_match[-1]]
            print ("Found a match" + greatest_hit)
            print (bad_file + greatest_hit)
            plates_csv['file_name'] = plates_csv['file_name'].replace(bad_file, greatest_hit)
        
        else:
            no_fix_files.append(bad_file)

print (no_fix_files)
print (len(no_fix_files))
print (len(good_files))
plates_csv.to_csv('/home/lujantorob/bibilujan/plates.csv', sep=',', header=False, index=False)



