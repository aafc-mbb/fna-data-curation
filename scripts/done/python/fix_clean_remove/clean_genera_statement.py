
# coding: utf-8

# This script finds the "Genera statement", which is the statement that has the count of lower taxa that exist. When there is a duplicate, it deletes one of them. If the two are not the same, it is printed here, so that they can be reviewed manually. It also removes ":" at the end and ads "." at the end of the statement

# In[ ]:


import glob
import re
from lxml import etree
from os.path import basename


# #all_volumes = ["2", "3", "4", "5", "6", "7", "8", "9", "19-21", "22", "23", "26", "27", "28" ]
# all_volumes = ["19-21"]
# #for vol in all_volumes:

for file_name in glob.glob("/home/lujantorob/fna-data-curation/coarse_grained_fna_xml/V28/*.xml"):
    counter = 0
    parser = etree.XMLParser(remove_blank_text=True)
    parsed_xml = etree.parse (file_name, parser)
    write_file = False
    discussion_all = parsed_xml.findall('/discussion')
    genera_elements = []
    
    for discussion in discussion_all:
        genera_text = re.search ( r'^(?:Genera|Species|Subspecies|Varieties)(?: ca\.)* \d+(,*).+', discussion.text)
        if genera_text:
            if genera_text.group().endswith(':'):
                write_file = True
                discussion.text = discussion.text[:-1]
            if discussion.text.endswith('.'):
                discussion.text = discussion.text[:-1]
            elif not discussion.text.endswith('.'):
                write_file = True
            if len(genera_elements) == 1:
                if genera_elements[0].text != discussion.text:
                    print ("Review " + basename(file_name))
                    print (genera_elements[0].text +" " + discussion.text)
                discussion.getparent().remove(discussion)
            
 
            genera_elements.append(discussion)

    if write_file == True:
        genera_elements[0].text = genera_elements[0].text + "."
        parsed_xml.write(file_name, encoding= 'utf-8', xml_declaration= True, pretty_print= True)
        
print ("Done!")
    


# I wrote this after, to check if there were any other "genera statements" that are in "description" tags instead of "discussion". I deleted these manually, so that I could check if there were other issues in the file. 

# In[ ]:


import glob
import re
from lxml import etree
from os.path import basename


for file_name in glob.glob("/home/lujantorob/fna-data-curation/coarse_grained_fna_xml/V23/*.xml"):
    counter = 0
    parser = etree.XMLParser(remove_blank_text=True)
    parsed_xml = etree.parse (file_name, parser)
    write_file = False
    discussion_all = parsed_xml.findall('/discussion')
    description_all = parsed_xml.findall('/description')
    full_list = discussion_all + description_all 
    for discussion in full_list:
        genera_text = re.search ( r'^(?:Genera|Species|Subspecies|Varieties)(?: ca\.)* \d+(,*).+', discussion.text)
        if genera_text:
            counter += 1

    if counter > 1:
        print ("Dupe statement in " + basename(file_name))
        
print ("Done!")

