import os
from os import path
import glob
import pandas as pd


def prepare_target_file_names(treatment_list_file):
    treatment_list = pd.read_csv(treatment_list_file, header=None,
                                 names=['file_name', 'taxon_name', 'taxon_rank'])  # read V17_treatment_list.txt
    treatment_list['target_legacy_file_name'] = treatment_list.file_name.apply(
        lambda x: x.strip('.xml') + '-distribution-map.eps')
    # desired style of file name: V12_1004-distribution-map.jpg
    treatment_list.taxon_name = treatment_list.taxon_name.apply(
        lambda x: x.lower().replace('.', ''))  # clean up taxon names for easier
    # comparison (i.e., lowercase and remove periods)
    return treatment_list


def prepare_legacy_file_names_taxon_names(distribution_map_directory):
    legacy_file_list = glob.glob(distribution_map_directory)  # get the filenames
    legacy_file_taxon_names = [file_name.split('/')[-1].strip('eps').strip('.').lower()
                               for file_name in legacy_file_list]  # transform file names into taxon names
    return legacy_file_list, legacy_file_taxon_names


def match_taxon_name_to_file_name(treatment_list, taxon_name):  # TODO: speed up matching process
    match = treatment_list.apply(lambda x: x.target_legacy_file_name if taxon_name == x.taxon_name else None, axis=1)
    file_name = match[match.notna()].to_list()  # there should be a better way to structure this result
    return file_name


def build_new_file_names(treatment_list, legacy_file_taxon_names, legacy_file_list, csv_file_name):
    print("Matching file names to treatment list...")
    file_names = [match_taxon_name_to_file_name(treatment_list, name) for name in legacy_file_taxon_names]
    file_names_lookup = pd.DataFrame([legacy_file_list, file_names]).transpose()
    file_names_lookup = file_names_lookup.rename(columns={0: "legacy_file_name", 1: "target_file_name"})
    file_names_lookup.to_csv(csv_file_name)
    return file_names_lookup


def prepare_data():
    treatment_list = prepare_target_file_names(treatment_list_file="../../../../../fna-data-curation/coarse_grained_fna_xml"
                                                                   "/treatment_lists/V17_treatment_list.txt")
    legacy_file_list, legacy_file_taxon_names = prepare_legacy_file_names_taxon_names(distribution_map_directory=
                                                                                      "../../../../supplemental_data"
                                                                                      "/distribution_maps/V17/AA_Maps/*.eps")
    file_names_lookup = build_new_file_names(treatment_list, legacy_file_taxon_names, legacy_file_list,
                                             csv_file_name="verify_distribution_file_names.csv")


def write_new_file_names():  # rename them
    print("Writing new file names...")
    count_issues = 0
    file_names_lookup = pd.read_csv("verify_distribution_file_names.csv", index_col=0)
    for index, row in file_names_lookup.iterrows():
        if path.exists(row.legacy_file_name):
            src = path.dirname(row.legacy_file_name) # get the path to the file in the current directory
            if row.target_file_name != '[]':
                target_file_name = src + '/' + row.target_file_name.split(sep="'")[1]  # for some reason file names
                # are interpreted as strings of lists. Taking the first hit here!! TODO: fix this
                os.rename(row['legacy_file_name'], target_file_name)  # rename the original file
            else:
                count_issues = count_issues + 1
    return count_issues


def main():
    prepare_data()
    count_issues = write_new_file_names()
    print("Done!")
    print("There are {} name matching issues to address.".format(count_issues))


if __name__ == "__main__":
    main()