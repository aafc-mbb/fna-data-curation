
# coding: utf-8

# This script was created to remove duplicate files from the grasses FNA volumes (24, 25). We noticed that the files had duplicates, and the one with the largest file number had corrections. IE the file with a greater number was the correct one, when there are duplicates. 
# 
# This script uses the taxon name and stores it with the file number in a diccionary, when the loop finds a repeated taxon name, if the file number is greater it replaces the previous one. I create a "files_keep" directory manually in the directory I am working on, and the script will move there the files to keep. 

# In[59]:


import glob
from lxml import etree
from os.path import basename
import re
import os

volume = "V24"
directory = "/home/lujantorob/fna-data-curation/coarse_grained_fna_xml/"+ volume +"/*.xml"
files_keep = {}

for file_name in glob.glob(directory):
    #print (file_name)
    parser = etree.XMLParser(remove_blank_text=True)
    parsed_xml = etree.parse (file_name, parser)
    taxon_accepted_all = parsed_xml.findall('/taxon_identification[@status="ACCEPTED"]/taxon_name')
    #print (taxon_accepted_all[0].text)
    taxon_name = taxon_accepted_all[0].text.lower()
    file_number = file_name.replace('.xml', '')
    path = "/home/lujantorob/fna-data-curation/coarse_grained_fna_xml/" + volume + "/" + volume + "_"
    file_number = file_number.replace(path, '')

    if (taxon_name not in files_keep) or (int(file_number) > int(files_keep[taxon_name])):
                files_keep[taxon_name] = file_number
print (len(files_keep))

for file in files_keep:
    current_file = "/home/lujantorob/fna-data-curation/coarse_grained_fna_xml/"     +volume + "/" + volume +"_" + files_keep[file] + ".xml"
    destination = "/home/lujantorob/fna-data-curation/coarse_grained_fna_xml/"     +volume + "/files_keep/" + volume +"_" +files_keep[file] + ".xml"
    #print (destination)
    os.rename (current_file, destination)

