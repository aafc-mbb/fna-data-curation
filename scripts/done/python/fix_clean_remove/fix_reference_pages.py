
# coding: utf-8

# In[126]:


import glob
import os


# In[117]:


dic = {"<mediawiki xmlns=\"http://www.mediawiki.org/xml/export-0.8/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.mediawiki.org/xml/export-0.8/ http://www.mediawiki.org/xml/export-0.8.xsd\" version=\"0.8\" xml:lang=\"en\">":"<mediawiki xmlns=\"http://www.mediawiki.org/xml/export-0.10/\" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xsi:schemaLocation=\"http://www.mediawiki.org/xml/export-0.10/ http://www.mediawiki.org/xml/export-0.10.xsd\" version=\"0.10\" xml:lang=\"en\">","<base>http://fna.biowikifarm.net/wiki/Main_Page</base>\n    <generator>MediaWiki 1.23.6</generator>\n    <case>case-sensitive</case>":"<dbname>mediawiki</dbname>\n    <base>http://208.113.131.13/wiki/Main_Page</base>\n    <generator>MediaWiki 1.30.0</generator>\n    <case>first-letter</case>","<namespace key=\"-2\" case=\"case-sensitive\">Media</namespace><namespace key=\"-1\" case=\"first-letter\">Special</namespace><namespace key=\"0\" case=\"case-sensitive\"/><namespace key=\"1\" case=\"case-sensitive\">Talk</namespace><namespace key=\"2\" case=\"first-letter\">User</namespace><namespace key=\"3\" case=\"first-letter\">User talk</namespace><namespace key=\"4\" case=\"case-sensitive\">Project</namespace><namespace key=\"5\" case=\"case-sensitive\">Project talk</namespace><namespace key=\"6\" case=\"case-sensitive\">File</namespace><namespace key=\"7\" case=\"case-sensitive\">File talk</namespace><namespace key=\"8\" case=\"first-letter\">MediaWiki</namespace><namespace key=\"9\" case=\"first-letter\">MediaWiki talk</namespace><namespace key=\"10\" case=\"case-sensitive\">Template</namespace><namespace key=\"11\" case=\"case-sensitive\">Template talk</namespace><namespace key=\"12\" case=\"case-sensitive\">Help</namespace><namespace key=\"13\" case=\"case-sensitive\">Help talk</namespace><namespace key=\"14\" case=\"case-sensitive\">Category</namespace><namespace key=\"15\" case=\"case-sensitive\">Category talk</namespace><namespace key=\"102\" case=\"case-sensitive\">Property</namespace><namespace key=\"103\" case=\"case-sensitive\">Property talk</namespace><namespace key=\"106\" case=\"case-sensitive\">Form</namespace><namespace key=\"107\" case=\"case-sensitive\">Form talk</namespace><namespace key=\"108\" case=\"case-sensitive\">Concept</namespace><namespace key=\"109\" case=\"case-sensitive\">Concept talk</namespace><namespace key=\"170\" case=\"case-sensitive\">Filter</namespace><namespace key=\"171\" case=\"case-sensitive\">Filter talk</namespace><namespace key=\"900\" case=\"case-sensitive\">Reference</namespace><namespace key=\"901\" case=\"case-sensitive\">Reference talk</namespace><namespace key=\"902\" case=\"case-sensitive\">Author</namespace><namespace key=\"903\" case=\"case-sensitive\">Author talk</namespace><namespace key=\"904\" case=\"case-sensitive\">Tree</namespace><namespace key=\"905\" case=\"case-sensitive\">Tree talk</namespace>\n":"<namespace key=\"-2\" case=\"first-letter\">Media</namespace>\n      <namespace key=\"-1\" case=\"first-letter\">Special</namespace>\n      <namespace key=\"0\" case=\"first-letter\" />\n      <namespace key=\"1\" case=\"first-letter\">Talk</namespace>\n      <namespace key=\"2\" case=\"first-letter\">User</namespace>\n      <namespace key=\"3\" case=\"first-letter\">User talk</namespace>\n      <namespace key=\"4\" case=\"first-letter\">FNA</namespace>\n      <namespace key=\"5\" case=\"first-letter\">FNA talk</namespace>\n      <namespace key=\"6\" case=\"first-letter\">File</namespace>\n      <namespace key=\"7\" case=\"first-letter\">File talk</namespace>\n      <namespace key=\"8\" case=\"first-letter\">MediaWiki</namespace>\n      <namespace key=\"9\" case=\"first-letter\">MediaWiki talk</namespace>\n      <namespace key=\"10\" case=\"first-letter\">Template</namespace>\n      <namespace key=\"11\" case=\"first-letter\">Template talk</namespace>\n      <namespace key=\"12\" case=\"first-letter\">Help</namespace>\n      <namespace key=\"13\" case=\"first-letter\">Help talk</namespace>\n      <namespace key=\"14\" case=\"first-letter\">Category</namespace>\n      <namespace key=\"15\" case=\"first-letter\">Category talk</namespace>\n      <namespace key=\"102\" case=\"first-letter\">Property</namespace>\n      <namespace key=\"103\" case=\"first-letter\">Property talk</namespace>\n      <namespace key=\"106\" case=\"first-letter\">Form</namespace>\n      <namespace key=\"107\" case=\"first-letter\">Form talk</namespace>\n      <namespace key=\"108\" case=\"first-letter\">Concept</namespace>\n      <namespace key=\"109\" case=\"first-letter\">Concept talk</namespace>\n      <namespace key=\"170\" case=\"first-letter\">Filter</namespace>\n      <namespace key=\"171\" case=\"first-letter\">Filter talk</namespace>\n      <namespace key=\"900\" case=\"first-letter\">Reference</namespace>\n      <namespace key=\"901\" case=\"first-letter\">Reference talk</namespace>\n      <namespace key=\"902\" case=\"first-letter\">Author</namespace>\n      <namespace key=\"903\" case=\"first-letter\">Author talk</namespace>\n      <namespace key=\"904\" case=\"first-letter\">Tree</namespace>\n      <namespace key=\"905\" case=\"first-letter\">Tree talk</namespace>","<title xmlns=\"\">": "<title>Reference:", "<ns xmlns=\"\">900</ns>": "<ns>900</ns>","<revision xmlns=\"\">": "<revision>","<model>wikitext</model>\n      <format>text/x-wiki</format>": "<sha1/>","<text xml:space=\"preserve\">": "<contributor>\n        <username>Admin</username>\n        <id>1</id>\n      </contributor>\n      <model>wikitext</model>\n      <format>text/x-wiki</format>\n      <text xml:space=\"preserve\">"}
# dic


# In[132]:


def rename(ref_file):
    split_name = os.path.splitext(ref_file)
    new_name = split_name[0] + '_reformat' + split_name[1]
    return(new_name)


# In[135]:


def replace_all(ref_file, dic):
    out_file = rename(ref_file)
    with open(ref_file, "r") as infile, open(out_file, "w") as outfile:
        data = infile.read()
        # print(data)
        for i, j in dic.items():
            data = data.replace(i, j)
        infile.close()
        outfile.write(data)
        # print(data)


# In[136]:


ref_file = '/Users/jocelynpender/fna/fna-data/fna-smw-ready-xml-pages/reference_xml/V4_reference_pages.xml'
replace_all(ref_file,dic)


# In[139]:


for filename in glob.glob('/Users/jocelynpender/fna/fna-data/fna-smw-ready-xml-pages/reference_xml/*.xml'):    
    replace_all(filename, dic)

