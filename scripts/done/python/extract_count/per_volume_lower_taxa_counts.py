
# coding: utf-8

# Get the counts for all the ranks except sp, genus, variety, subsp, and family 
# <br> and the text from the genera count to compare. It produces a csv. file
# <br> it also gives all the counts at the end

# In[10]:


import glob
import re
import pandas as pd
from lxml import etree
from collections import Counter
from os.path import basename
import csv

rank_list = []
dont_care_list = [ "species", "genus", "variety", "subspecies", "family"]
volume = "23"
directory = "/home/lujantorob/fna-data-curation/coarse_grained_fna_xml/V"+ volume +"/*.xml"

def findTaxonTotal(taxon_name, rank):
    counter = 0
    for file_name in glob.glob(directory):
        parser = etree.XMLParser(remove_blank_text=True)
        parsed_xml = etree.parse (file_name, parser)
        taxon_accepted_all = parsed_xml.findall('/taxon_identification[@status="ACCEPTED"]/taxon_name')
        treatement_rank = taxon_accepted_all[-1].attrib['rank']
        if rank == "tribe" or rank == "subfamily":
            if treatement_rank == "species":
                for taxon in taxon_accepted_all:
                    if taxon.attrib['rank'] == rank:
                        if taxon.text.lower() == taxon_name.lower():
                            counter += 1

        else: 
            if treatement_rank == "species":
                for taxon in taxon_accepted_all:
                    if taxon.attrib['rank'] == rank:
                        if taxon.text.lower() == taxon_name.lower():
                            counter += 1
    return counter            
with open('/home/lujantorob/Test/taxon_totals_'+volume+'.csv', 'w') as csvfile:
    fieldnames = ['name', 'rank', 'parent_taxon', 'current_total',                   'total', 'text', 'file_name']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()

    for file_name in glob.glob(directory):
        match = 'NA'
        count = 'NA'
        parser = etree.XMLParser(remove_blank_text=True)
        parsed_xml = etree.parse (file_name, parser)
        write_file = False
        taxon_accepted_all = parsed_xml.findall('/taxon_identification[@status="ACCEPTED"]/taxon_name')
        taxon_accepted_all = parsed_xml.findall('/taxon_identification[@status="ACCEPTED"]/taxon_name')
        treatement_rank = taxon_accepted_all[-1].attrib['rank']
        treatement_name = taxon_accepted_all[-1].text
        rank_list.append(treatement_rank)                  

        if treatement_rank not in dont_care_list:
            discussion_all = parsed_xml.findall('/discussion')
            for discussion in discussion_all:
                genera_text = re.search ( r'^(:?Genera|Species|Subspecies|Varieties|Genus)(?: ca\.)* \d+(:?,*).+',                                          discussion.text)
                if genera_text:
                    match = genera_text.group()
                    genera_count = re.search ( r'\((\d*) in the flora', genera_text.group())
                    
                    if genera_count:
                        count = genera_count.group(1)
                    else:
                        count = 'UNDET'

            parent_taxon = taxon_accepted_all[-2].text
            current_count = findTaxonTotal(treatement_name.lower(), treatement_rank)
            writer.writerow({'name':treatement_name.lower(), 'rank':treatement_rank,                              'parent_taxon':parent_taxon, 'current_total':current_count,                              'total':count, 'text':match, 'file_name':basename(file_name)})
                       
                    
    counter_dicc = Counter(rank_list)
    print (counter_dicc)


# Get page number from a file

# In[9]:


from lxml import etree
from os.path import basename

volume = "23/"
file_list = [ "V23_875.xml" ]

for file in file_list: 
    file_name = "/home/lujantorob/fna-data-curation/coarse_grained_fna_xml/V"                           + volume + file
    parser = etree.XMLParser(remove_blank_text=True)
    parsed_xml = etree.parse (file_name, parser)
    #volume = parsed_xml.findall('/meta/other_info_on_meta[@type="volume"]')
    treatment_page = parsed_xml.findall('/meta/other_info_on_meta[@type="treatment_page"]')
    print (treatment_page[0].text)
    
    
    
    


# How to use counter

# In[ ]:


rank = "subgenus"
string = "botrychium"
all_taxa = []
for file_name in glob.glob("/home/lujantorob/fna-data-curation/coarse_grained_fna_xml/V2/*xml"):
    parser = etree.XMLParser(remove_blank_text=True)
    parsed_xml = etree.parse (file_name, parser)
    write_file = False
    taxon_accepted_all = parsed_xml.findall('/taxon_identification[@status="ACCEPTED"]/taxon_name')
    for taxon in taxon_accepted_all:
        if taxon.attrib['rank'] == rank:
            #print (taxon.text.lower())
            all_taxa.append(taxon.text.lower())
        
counter_dicc = Counter(all_taxa) 
#print (counter_dicc)
print (counter_dicc[string])


# In[ ]:


import glob
import re
import pandas as pd
from lxml import etree
from collections import Counter
from os.path import basename
import csv

rank = "tribe"
taxon = "amphiachaenia"
volume = "4"


def findTaxonTotal(taxon_name, rank):
    counter = 0
    for file_name in glob.glob("/home/lujantorob/fna-data-curation/coarse_grained_fna_xml/V" + volume + "/*xml"):
        parser = etree.XMLParser(remove_blank_text=True)
        parsed_xml = etree.parse (file_name, parser)
        taxon_accepted_all = parsed_xml.findall('/taxon_identification[@status="ACCEPTED"]/taxon_name')
        treatement_rank = taxon_accepted_all[-1].attrib['rank']
        for taxon in taxon_accepted_all:
            if taxon.attrib['rank'] == rank:
                if taxon.text.lower() == taxon_name.lower():
                    #counter += 1
                    #print (taxon.text.lower())
                    #print (treatement_rank)
                    if treatement_rank == 'species':
                        counter += 1
                        #print (basename(file_name))
    return counter 


# for taxon in ["cochlearieae", "lunarieae", "idahoa", "aphragmeae", "erysimeae", "chorisporeae"]:
#     print (findTaxonTotal(taxon, rank))
            
print (findTaxonTotal("cactoideae", "subfamily"))        


# How to use csv

# In[ ]:


import csv

treatment_name = 'Beatriz'
rank = 'rank1'
count = 3

with open('/home/lujantorob/Test/test.csv', 'w') as csvfile:
    fieldnames = ['taxon_name', 'treatment_rank', 'total']
    writer = csv.DictWriter(csvfile, fieldnames=fieldnames)
    writer.writeheader()
    writer.writerow({'taxon_name':treatment_name, 'treatment_rank':rank, 'total':count})
    #   writer.writerow({'first_name': 'Baked', 'last_name': 'Beans'})

    
    
    
    


# I moved this script to the fix_hierarchy script

# In[ ]:


import glob
from lxml import etree
from os.path import basename
import re

volume = "7/"
directory = "/home/lujantorob/fna-data-curation/coarse_grained_fna_xml/V"+ volume +"/*.xml"
file_list = [ "V7_913.xml", "V7_220.xml", "V7_210.xml" ]
#file_list = ["V7_220.xml"]


def fixLowerTaxa(genus, species, element_to_add):
    #counter = 0
    for file_name in glob.glob(directory):
        write_file = False

        parser = etree.XMLParser(remove_blank_text=True)
        parsed_xml = etree.parse (file_name, parser)
        taxon_accepted_all = parsed_xml.findall('/taxon_identification[@status="ACCEPTED"]/taxon_name')
        if element_to_add not in taxon_accepted_all:
            for index, taxon in enumerate(taxon_accepted_all):
                rank = taxon.attrib['rank']
                if taxon.text.lower() == genus.lower() and rank == 'genus':
                    if taxon_accepted_all[index] != taxon_accepted_all[-1]:
                        #print (taxon_accepted_all[index+1].attrib['rank'])
                        if taxon_accepted_all[index+1].text.lower() == species.lower()                          and taxon_accepted_all[index+1].attrib['rank'] == 'species':
                            write_file = True
                            #print (taxon.text)
                            #print (element_to_add.text)
                            taxon.addnext(element_to_add)

        if write_file == True:
            taxon_accepted_all = parsed_xml.findall('/taxon_identification[@status="ACCEPTED"]/taxon_name')
            taxon_hierarchy = parsed_xml.findall('//taxon_hierarchy')
            hierarchy = ''
            for taxon in taxon_accepted_all:
                rank_forhierachy = taxon.attrib['rank']
                name = taxon.text
                hierarchy = hierarchy + rank_forhierachy + " " + name.lower() + ";"
                taxon_hierarchy[0].text= hierarchy             
            parsed_xml.write(file_name, encoding= 'utf-8', xml_declaration= True, pretty_print= True)
    print ("Done")
        
                            
                
for file in file_list: 
    file_name = "/home/lujantorob/fna-data-curation/coarse_grained_fna_xml/V"                           + volume + file
    parser = etree.XMLParser(remove_blank_text=True)
    parsed_xml = etree.parse (file_name, parser)
    determination_list = parsed_xml.findall('//determination')
    if determination_list:
        taxon_accepted_all = parsed_xml.findall('/taxon_identification[@status="ACCEPTED"]/taxon_name')
        missing_element = taxon_accepted_all[-1]
        
        for determination in determination_list:
            print (determination.text)
            lower_taxon = re.search (r'\d* (\w+) (\w+)', determination.text)     
            lower_taxon_genus = lower_taxon.group(1)
            lower_taxon_species = lower_taxon.group(2)
            #print (missing_element)
            fixLowerTaxa(lower_taxon_genus, lower_taxon_species, missing_element)
            
    
    
    


# How to use enumerate

# In[4]:


greetings_list = ["Hi", "Hello", "Hola"]

for index, greeting in enumerate(greetings_list):
    print(index, greeting)
    for greeting in greetings_list[index:]:
        print (greeting)


# How to make the taxon hierarchy again

# In[ ]:


write_file = False

parser = etree.XMLParser(remove_blank_text=True)
parsed_xml = etree.parse ("/home/lujantorob/fna-data-curation/coarse_grained_fna_xml/V7/V7_220.xml", parser)
taxon_accepted_all = parsed_xml.findall('/taxon_identification[@status="ACCEPTED"]/taxon_name')
taxon_hierarchy = parsed_xml.findall('//taxon_hierarchy')

hierarchy = ''
print (taxon_hierarchy[0].text)
for index, taxon in enumerate(taxon_accepted_all):
    rank = taxon.attrib['rank']
    rank_name = taxon.text
    hierarchy = hierarchy + rank + " " + rank_name + ";"
    #print (hierarchy)
taxon_hierarchy[0].text= hierarchy
print (taxon_hierarchy[0].text)
    
    
    
print ("Done")

