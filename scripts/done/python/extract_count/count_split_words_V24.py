
# coding: utf-8

# In[4]:


# glob module allows script to be run over multiple files with matching pattern in the same directory (path specified)
import glob
# re is regualar expression module
import re
# need lxml for built-in parser
from lxml import etree
# need csv to write output into csv file
import csv
# need counter to count how many times each unique split-word appears
from collections import Counter

# create empty list to store all text that matches regex pattern
split_text = []
# glob to run script over all files in fna volume x folder
for file in glob.glob("/home/laoyea/Documents/fna-data-curation/coarse_grained_fna_xml/V24/*.xml"):
    # call lxml to parse file as a tree; define root of parsed_xml
    parser = etree.XMLParser(remove_blank_text=True)
    parsed_xml = etree.parse (file, parser)
    root = parsed_xml.getroot()
    # define regex pattern to search for
    split_word = r"\b[a-z]+-\w+\b"
    # findall matches for regex pattern in <description>
    for description in root.findall("./description"):
        #or use description = root.findall("./description")
        match = re.findall(split_word, description.text)
        for word in match:
            print (word)
            split_text.append(word)
    # findall matches for regex pattern in <discussion>
    for discussion in root.findall("./discussion"):
        match = re.findall(split_word, discussion.text)
        for word in match:
            print (word)
            split_text.append(word)
    # findall matches for regex pattern in <reference>
    for reference in root.findall("./references/reference"):
        match = re.findall(split_word, reference.text)
        for word in match:
            print (word)
            split_text.append(word)
print ("Done")

# define split_dictionary; run Counter over split_text (from above) to get counts for each unique split-word
split_dictionary = Counter(split_text)
#print (split_dictionary)

# write results of split_text to csv file
#with open('split_word.csv', 'w') as myfile:
    #wr = csv.writer(myfile, delimiter="\n", quoting=csv.QUOTE_ALL)
    #wr.writerow(split_text)

# write results of Counter(split_text) to csv file    
with open('split_count.csv', 'w') as mytest:
    wr = csv.writer(mytest)
    # define fieldnames (headers) for csv file
    fieldnames = ['word', 'count']
    # write the headers into a row
    wr.writerow(fieldnames)
    # write each (key, value) pair in split_dictionary into a new row in the csv output file
    for word, count in split_dictionary.iteritems():
        wr.writerow([word,count])

