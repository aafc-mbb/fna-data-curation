
# coding: utf-8

# In[2]:


from lxml import etree
import os
import re
import csv

from xml.sax.handler import ContentHandler
from xml.sax import make_parser
from glob import glob # I don't think this is necessary
import sys # I don't think this is necessary


# ## Script

# In[10]:


def parsefile(file): # Helper function to parse file to determine if it is a valid XML doc
    parser = make_parser()
    parser.setContentHandler(ContentHandler())
    parser.parse(file)


for subdir, dirs, files in os.walk("../Input"):
    for file in files:
        filepath = subdir + os.sep + file
        if (filepath.endswith(".xml") and re.search('archive', filepath) is None and re.search('V25', filepath) is None and re.search('V24', filepath) is None and re.search('__MACOSX', filepath) is None):
            try:
                parsefile(filepath)
                tree = etree.parse(filepath)
                # print("parsed")
                genus = tree.findall("//taxon_identification[@status='ACCEPTED']/taxon_name[@rank='subspecies']") # Change rank here to see duplicates in diff ranks
                if len(genus) > 1:
                    print(filepath)
            except Exception as e:
                print("failed: " + filepath)

