
# coding: utf-8

# In[54]:


from __future__ import print_function
import sys
# import ElementTree
#((import xml.etree.ElementTree as etree))
#import etree using lxml; using lxml instead of ElementTree because lxml allows for pretty-printing of output xml file
from lxml import etree
# import file of interest as a tree
file = sys.argv[1]
tree = etree.parse(file)
# define root
root = tree.getroot()
# find ACCEPTED taxon_indentification:
for taxon_identification in root.findall("./taxon_identification/[@status='ACCEPTED']"):
    # define empty string to hold treatment name
    treat_name = ""
    #define empty string to hold the last taxon rank (rank of interest)
    rank_name = ""
    # define empty strings to hold names of different taxon ranks
    fam_name = ""
    subfam_name = ""
    tribe_name = ""
    subtribe_name = ""
    genus_name = ""
    subgenus_name = ""
    sect_name = ""
    subsect_name = ""
    ser_name = ""
    subser_name = ""
    species_name = ""
    subspecies_name = ""
    variety_name = ""
    # find all taxon_name under ACCEPTED taxon_identification
    for taxon_name in taxon_identification.findall('./taxon_name'):
        # use if/elif statements to append taxon_name values to appropriate empty strings above
        if taxon_name.get('rank') == 'family':
            fam_name+=(taxon_name.text)
        elif taxon_name.get('rank') == 'subfamily':
            subfam_name+=(taxon_name.text)
        elif taxon_name.get('rank') == 'tribe':
            tribe_name+=(taxon_name.text)
        elif taxon_name.get('rank') == 'subtribe':
            subtribe_name+=(taxon_name.text)
        elif taxon_name.get('rank') == 'genus':
            genus_name+=(taxon_name.text)
        elif taxon_name.get('rank') == 'subgenus':
            subgenus_name+=(taxon_name.text)
        elif taxon_name.get('rank') == 'section':
            sect_name+=(taxon_name.text)
        elif taxon_name.get('rank') == 'subsection':
            subsect_name+=(taxon_name.text)
        elif taxon_name.get('rank') == 'series':
            ser_name+=(taxon_name.text)
        elif taxon_name.get('rank') == 'subseries':
            subser_name+=(taxon_name.text)
        elif taxon_name.get('rank') == 'species':
            species_name+=(taxon_name.text)
        elif taxon_name.get('rank') == 'subspecies':
            subspecies_name+=(taxon_name.text)
        elif taxon_name.get('rank') == 'variety':
            variety_name+=(taxon_name.text)
    # find the last taxon_name using xpath
    # the value of the last taxon_name in the hierarchy determines the concatenation pattern for...
    # ...writing the treatment name
    for taxon_name in taxon_identification.xpath('taxon_name[last()]'):
        #grab the rank that matches the last taxon_name in taxon_identification
        rank_name = taxon_name.get('rank')
        if taxon_name.get('rank') == 'family':
            treat_name = (fam_name)
        elif taxon_name.get('rank') == 'subfamily':
            treat_name = (fam_name+' '+'subfam.'+' '+subfam_name)
        elif taxon_name.get('rank') == 'tribe':
            treat_name = (fam_name+' '+'tribe'+' '+tribe_name)
        elif taxon_name.get('rank') == 'subtribe':
            treat_name = (fam_name+' '+'tribe '+tribe_name+' '+'subtribe'+' '+subtribe_name)
        elif taxon_name.get('rank') == 'genus':
            treat_name = (genus_name)
        elif taxon_name.get('rank') == 'subgenus':
            treat_name = (genus_name+' '+'subg.'+' '+subgenus_name)
        elif taxon_name.get('rank') == 'section':
            treat_name = (genus_name+' '+'sect.'+' '+sect_name)
        elif taxon_name.get('rank') == 'subsection':
            treat_name = (genus_name+' '+sect_name+' '+'subsect.'+' '+subsect_name)
        elif taxon_name.get('rank') == 'series':
            treat_name = (genus_name+' '+'('+'sect.'+' '+sect_name+')'+' '+'ser.'+' '+ser_name)
        elif taxon_name.get('rank') == 'subseries':
            treat_name = (genus_name+' '+'('+'sect.'+' '+sect_name+')'+' '+'ser.'+' '+ser_name+' '+'subser'+' '+subser_name)
        elif taxon_name.get('rank') == 'species':
            treat_name = (genus_name+' '+species_name)
        elif taxon_name.get('rank') == 'subspecies':
            treat_name = (genus_name+' '+species_name+' '+'subsp.'+' '+subspecies_name)
        elif taxon_name.get('rank') == 'variety':
            treat_name = (genus_name+' '+species_name+' '+'var.'+' '+variety_name)
# print "file name, treatment name"
# use the encode function to set encoding to UTF-8, since ASCII fails to encode certain special characters
print (file+','+treat_name.encode('UTF-8')+','+rank_name) 

