<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Hoffmannsegg &amp; Link" date="unknown">POLYGALACEAE</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">POLYGALA</taxon_name>
    <taxon_name rank="species" authority="(Michaux) de Candolle in A. P. de Candolle and A. L. P. P. de Candolle" date="1824">nana</taxon_name>
    <place_of_publication>
      <publication_title>Prodr.</publication_title>
      <place_in_publication>1: 328.  1824</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygalaceae;genus polygala;species nana</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Polygala</taxon_name>
    <taxon_name rank="species" authority="Linnaeus" date="unknown">lutea</taxon_name>
    <taxon_name rank="variety" authority="Michaux" date="1803">nana</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>2: 54.  1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus polygala;species lutea;variety nana</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Pilostaxis</taxon_name>
    <taxon_name rank="species" authority="(Michaux) Rafinesque" date="unknown">nana</taxon_name>
    <taxon_hierarchy>genus pilostaxis;species nana</taxon_hierarchy>
  </taxon_identification>
  <number>19.</number>
  <other_name type="common_name">Dwarf milkwort</other_name>
  <description type="morphology">Herbs annual or biennial, single- or multi-stemmed, 0.3–1.8 dm, unbranched; from taproot (or rarely fibrous root cluster).  Stems erect, glabrous.  Leaves mostly basal, rarely cauline, with persistent rosette; alternate; usually with narrow petiolelike region to 15 mm, rarely (sub-)sessile; basal blade spatulate, usually oblanceolate or obovate, rarely elliptic, 11–55 × (1.5–)5–20 mm, succulent, base cuneate or acute, apex rounded to acute, occasionally apiculate, rarely acuminate, surfaces glabrous.  Racemes capitate, 1–3.8 × 1–1.7 cm; peduncle 2.3–7.5 cm; bracts deciduous, often tardily so, or sometimes persistent, linear-subulate.  Pedicels winged, 0.4–0.8(–1) mm, glabrous.  Flowers lemon-yellow to greenish yellow, drying green or yellowish green, 5.5–8.2 mm; sepals decurrent on pedicel, lanceolate, 3–5.3 mm, sometimes ciliolate; wings elliptic to oblong-lanceolate, 5.5–8 × 1.2–2(–2.6) mm, apex long-acuminate to cuspidate, involute, tip 0.7–1.6 mm; keel 3.5–5.8 mm, crest 2-parted, with 3 entire or 2-fid, linear lobes on each side; stamens 6(–8).  Capsules broadly ellipsoid to subglobose, 1.6–2 × 1.2–1.6 mm, margins not winged.  Seeds 0.8–1.8 mm, pubescent; aril 0.7–1.1 mm, lobes 1/3 to equal length of seed.  2n = 64, 68.</description>
  <discussion>Individuals of Polygala nana in scattered populations (especially in southern Florida) approach the habit of P. smallii, with the inflorescences scarcely exceeding the leaves; they can be distinguished using the differences discussed under 28. P. smallii.  Herbarium specimens of robust individuals are sometimes confused with P. lutea and small, rosulate plants of P. lutea may be confused with P. nana.  If fresh flower color (orange in P. lutea, yellow in P. nana) is not available, then the taxa can be distinguished readily by the pedicel length, 1.5–2.8 mm in P. lutea and less than 1 mm in P. nana, as well as the involute apical cusp on the sepal wings of P. nana usually ca. 1 mm (0.7–1.6 mm), versus sepal wings only partially involute apically and cusps (if present) less than 0.5 mm in P. lutea.</discussion>
  <description type="phenology">Flowering spring–fall (year-round).</description>
  <description type="habitat">Savannas, sandy pine woods, low wet woods, seepage slopes, wet depressions, flatwoods, bogs, coastal swales.</description>
  <description type="elevation">0–300 m.</description>
  <description type="distribution">Ala., Ark., Fla., Ga., La., Miss., N.C., S.C., Tenn., Tex.</description>
</bio:treatment>
