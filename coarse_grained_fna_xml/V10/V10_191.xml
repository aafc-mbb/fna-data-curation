<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="R. Brown" date="unknown">HALORAGACEAE</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">MYRIOPHYLLUM</taxon_name>
    <taxon_name rank="species" authority="Komarov" date="1914">sibiricum</taxon_name>
    <place_of_publication>
      <publication_title>Repert. Spec. Nov. Regni Veg.</publication_title>
      <place_in_publication>13: 168.  1914</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family haloragaceae;genus myriophyllum;species sibiricum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Myriophyllum</taxon_name>
    <taxon_name rank="species" authority="Fernald" date="unknown">exalbescens</taxon_name>
    <taxon_hierarchy>genus myriophyllum;species exalbescens</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">M.</taxon_name>
    <taxon_name rank="species" authority="Fernald" date="unknown">magdalenense</taxon_name>
    <taxon_hierarchy>genus m.;species magdalenense</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">M.</taxon_name>
    <taxon_name rank="species" authority="Linnaeus" date="unknown">spicatum</taxon_name>
    <taxon_name rank="variety" authority="Lange" date="unknown">capillaceum</taxon_name>
    <taxon_hierarchy>genus m.;species spicatum;variety capillaceum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">M.</taxon_name>
    <taxon_name rank="species" authority="unknown" date="unknown">spicatum</taxon_name>
    <taxon_name rank="subspecies" authority="(Fernald) Hultén" date="unknown">exalbescens</taxon_name>
    <taxon_hierarchy>genus m.;species spicatum;subspecies exalbescens</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">M.</taxon_name>
    <taxon_name rank="species" authority="unknown" date="unknown">spicatum</taxon_name>
    <taxon_name rank="variety" authority="(Fernald) Jepson" date="unknown">exalbescens</taxon_name>
    <taxon_hierarchy>genus m.;species spicatum;variety exalbescens</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">M.</taxon_name>
    <taxon_name rank="species" authority="unknown" date="unknown">spicatum</taxon_name>
    <taxon_name rank="variety" authority="Maximovicz" date="unknown">muricatum</taxon_name>
    <taxon_hierarchy>genus m.;species spicatum;variety muricatum</taxon_hierarchy>
  </taxon_identification>
  <number>9.</number>
  <other_name type="common_name">Northern or short-spike water-milfoil</other_name>
  <other_name type="common_name">myriophylle de Sibérie</other_name>
  <description type="morphology">Herbs monoecious, aquatic, often forming dense stands.  Stems usually unbranched, to 6 m.  Turions present, ± dark green, cylindrical, with gradual transition from foliage leaves to reduced turion leaves, 12–40(–45) × (3–)5–12(–15) mm, apex ± rounded; leaves pectinate, stiff, strongly appressed to axis distally, not proximally, elliptic in outline, 5–15 × 1.4–5 mm, with clusters of brown, conical trichomes between leaf bases; segments 13–15(–17), elongate botuliform, longest segment 1.8–5.2(–6) mm, basal segment usually less than or equal to 1/2 central axis of leaf, apex apiculate, with single, brown, conical trichome in each axil.  Leaves in whorls of (3 or)4, heteromorphic; petiole 0–4 mm; submersed leaves pectinate, usually obovate in outline, (2.8–)13–32(–44) × (2.1–)16–35 mm, segments 6–18(–24), linear-filiform, often perpendicular to central axis, basal segments often as long as leaf axis, segments often irregular in orientation, not parallel and not in same plane, longest segment 2–20(–26) mm; emersed leaves, basal sometimes pectinate to pinnatifid proximally, with abrupt transition to obovate, elliptic, sometimes distally spatulate, in outline, margins of distal leaves entire to serrate to shallowly lobed, 1–2.3 × 0.6–1(–1.5) mm.  Inflorescences to 15 cm; flowers proximally pistillate, medially bisexual, distally staminate;  bracteoles cream to stramineous or purple with distinct, reddish or brown margin, usually ovate to depressed-ovate, sometimes elliptic to triangular, (0.4–)0.6–1.3 × 0.3–0.7 mm, margins entire or serrate, sometimes with distal, irregular, membranous fringe.  Staminate flowers: sepals cream to stramineous, usually depressed-ovate, sometimes ovate to triangular, 0.2–0.4 × 0.2–0.5 mm; petals caducous, cream to red or dark purple, oblong to elliptic or obovate, 1.7–2.3(–3) × 1–2 mm; stamens 8, filaments to 1.5 mm, anthers greenish cream to yellow or purple, 1–2.2 × 0.3–0.7 mm.  Pistillate flowers: sepals cream to green to purple, lanceolate to deltate or ovate, 0.1–0.3 × 0.1–0.2 mm; petals often persistent, cream, widely ovate, 0.3–0.5 × 0.2–0.5 mm; pistils 1–2 mm, stigmas white to red or ± purple, ± pulvinate, 0.2–0.4 mm.  Fruits globose, 4-lobed.  Mericarps olive-green to brown, cylindric to narrowly ovoid, 1.5–2.7 × 1.2–1.6 mm, transversely widely obovate, abaxial surface broadly rounded, sparsely and irregularly tuberculate, margins smooth or tuberculate, sometimes with 2 shallow, partial, longitudinal ridges, wings and ribs absent.  2n = 42.</description>
  <discussion>Myriophyllum exalbescens (M. sibiricum) was considered to be a North American endemic until the discovery of European specimens (S. G. Aiken and J. McNeill 1980).  Since the taxonomic name of Russian material pre-dated that for North American specimens, all material of M. exalbescens was synonymized under the name M. sibiricum (A. Ceska and O. Ceska 1986; Aiken and A. Cronquist 1988).  Myriophyllum sibiricum is widely recognized as circumpolar with an affinity for colder climates and is rarely found south of the 0°C January isotherm (Aiken 1981).  Myriophyllum sibiricum is distinctive when growing with low leaf segment numbers.  Hybridization with M. spicatum and subsequent introgression has apparently blurred the boundaries between these two taxa to the point that some specimens are not assignable to either species without molecular analysis (see 8. M. spicatum).  There is concern that M. sibiricum is being rapidly outcompeted in lakes by either M. spicatum or its hybrid (M. L. Moody and D. H. Les 2007b; A. P. Sturtevant et al. 2009; E. A. LaRue et al. 2013).  The dark green cylindric turions in M. sibiricum, which have reduced and thickened storage leaves, are useful for identification.  These reduced leaves are often blackened and visible at the base of new shoots in the next growing season, which can aid in the identification of vegetative material.</discussion>
  <description type="phenology">Flowering and fruiting May–Oct.</description>
  <description type="habitat">Oligotrophic to eutrophic waters, lakes.</description>
  <description type="elevation">0–3300 m.</description>
  <description type="distribution">Greenland; Alta., B.C., Man., N.B., Nfld. and Labr., N.W.T., N.S., Nunavut, Ont., P.E.I., Que., Sask., Yukon; Alaska, Ariz., Calif., Colo., Conn., Del., Idaho, Ill., Ind., Iowa, Kans., Maine, Md., Mass., Mich., Minn., Mo., Mont., Nebr., Nev., N.H., N.J., N.Mex., N.Y., N.Dak., Ohio, Oreg., Pa., R.I., S.Dak., Tex., Utah, Vt., Wash., Wis., Wyo.; Eurasia.</description>
</bio:treatment>
