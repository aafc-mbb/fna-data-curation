<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Hoffmannsegg &amp; Link" date="unknown">POLYGALACEAE</taxon_name>
    <taxon_name rank="genus" authority="(Chodat) J. R. Abbott" date="2011">HEBECARPA</taxon_name>
    <taxon_name rank="species" authority="(Bentham) J. R. Abbott" date="2011">obscura</taxon_name>
    <place_of_publication>
      <publication_title>J. Bot. Res. Inst. Texas</publication_title>
      <place_in_publication>5: 134.  2011</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family polygalaceae;genus hebecarpa;species obscura</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Polygala</taxon_name>
    <taxon_name rank="species" authority="Bentham" date="1840">obscura</taxon_name>
    <place_of_publication>
      <publication_title>Pl. Hartw.,</publication_title>
      <place_in_publication>58.  1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus polygala;species obscura</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">P.</taxon_name>
    <taxon_name rank="species" authority="S. F. Blake" date="unknown">orthotricha</taxon_name>
    <taxon_hierarchy>genus p.;species orthotricha</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">P.</taxon_name>
    <taxon_name rank="species" authority="S. F. Blake" date="unknown">piliophora</taxon_name>
    <taxon_hierarchy>genus p.;species piliophora</taxon_hierarchy>
  </taxon_identification>
  <number>3.</number>
  <other_name type="common_name">Velvet-seed milkwort</other_name>
  <description type="morphology">Herbs or subshrubs, (0.7–)1.2–4(–7.5) dm.  Stems erect, sometimes laxly so, hairs incurved or spreading.  Leaves: blade oblong to broadly elliptic proximally, to elliptic-ovate, lanceolate, or linear distally, often longer than proximal ones, (5–)12–42(–80) × 1.5–12 mm, base obtuse or acute, apex usually obtuse to acuminate, sometimes bluntly rounded, surfaces not translucent-punctate, hairs incurved or spreading.  Inflorescences terminal or leaf-opposed, racemes, loose, 1.5–15 × 1.5–3 cm; peduncle 0.7–1.5 cm; bracts narrowly ovate or lanceolate-ovate.  Pedicels 2–6 mm, pubescent.  Flowers purplish, 4.5–7 mm; sepals lanceolate, 2.2–3.5 mm, pubescent; wings elliptic-obovate, elliptic, or suborbiculate, 4.5–5.8 × 2–3.5 mm, sparsely pubescent along middle and apically; keel 4.8–5 mm, subglabrous.  Capsules broadly ellipsoid to subglobose, 7–10(–11) × 5–8 mm, not translucent-punctate, densely pubescent to glabrate.  Seeds 4–5 mm; aril 1.3–3(–4) mm, covering less than 1/20 to 4/5 lengthof seed.</description>
  <discussion>The key feature provided by S. F. Blake for diagnosing Polygala piliophora [known only from the type collection (US) in the Huachuca Mountains of Cochise County, Arizona] as distinct from Hebecarpa obscura was having aril lobes subequal or smaller than the apical umbo portion of the aril (versus longer than the umbo).  The veil-like distal portion of the aril (below the umbo region) is without distinct lobes in H. obscura, even when it covers nearly 4/5 the seed body.  Collections of H. obscura from scattered localities can have a reduced helmetlike aril, with the lobe region subequal or smaller than the apical umbo portion.  The length of the aril lobe is not only variable within H. obscura but is a feature that varies within many species of Hebecarpa.  Minor variations in pubescence and leaf and aril morphometrics within H. obscura do not seem worthy of taxonomic recognition, especially as the variants do not appear to be correlated with ecology or geography.  Hebecarpa obscura is sometimes morphologically quite similar to H. barbeyana, despite their differences in ovary and fruit pubescence.</discussion>
  <description type="phenology">Flowering spring–fall.</description>
  <description type="habitat">Sandy or rocky soils, mostly igneous, grasslands, open woodlands.</description>
  <description type="elevation">1200–2400 m.</description>
  <description type="distribution">Ariz., Nev., N.Mex., Tex.; Mexico (Chihuahua, Guanajuato, Oaxaca, Puebla, Sonora).</description>
</bio:treatment>
