<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">ONAGRACEAE</taxon_name>
    <taxon_name rank="subfamily" authority="W. L. Wagner &amp; Hoch" date="2007">Onagroideae</taxon_name>
    <taxon_name rank="tribe" authority="Endlicher" date="1830">Epilobieae</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">EPILOBIUM</taxon_name>
    <taxon_name rank="section" authority="unknown" date="unknown">Epilobium</taxon_name>
    <taxon_name rank="species" authority="Trelease" date="1906">mirabile</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>11: 404.  1906</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe epilobieae;genus epilobium;section epilobium;species mirabile</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>35.</number>
  <other_name type="common_name">Olympic Mountain willowherb</other_name>
  <description type="morphology">Herbs with sessile, compact, fleshy turions that leave dark basal scales.  Stems erect, loosely or not clumped, terete, 7–30 cm, usually simple, rarely branched, subglabrous with raised strig­illose lines decurrent from margins of petioles, or densely strigillose and without raised lines.  Leaves opposite proximal to inflorescence, alter­nate distally, petioles 1–3 mm proximally, subsessile distally; blade ovate to narrowly ovate, coriaceous, 1.5–3 × 0.7–1.2 cm, base rounded to cuneate, margins denticulate, 8–12 teeth per side, veins indistinct, 4–9 per side, apex obtuse proximally to acute distally, surfaces sparsely strigillose, mainly on margins and midrib; bracts not much reduced.  Inflorescences erect racemes, rarely branched, densely strigillose.  Flowers erect; buds 3–4 × 1.5–2.2 mm; pedicel 4–5 mm; floral tube 1.5–2 × 1.6–2.2 mm, sparsely glandular puberulent, sometimes mixed strigillose; sepals often purplish red, 2–3.2 ×1.5–2.4 mm; petals white, often red-tinged at apex, 3.8–5 × 2–3 mm, apical notch 0.4–0.8 mm; filaments cream, those of longer stamens 1.4–2.3 mm, those of shorter ones 0.8–1.4 mm; anthers 0.4–0.6 × 0.3–0.5 mm; ovary 10–18 mm, densely strigillose and glan­dular puberulent; style yellow or light pink, 2–2.3 mm, stigma broadly clavate, 0.8–1 × 0.6–0.7 mm, surrounded by longer anthers.  Capsules 30–45 mm, relatively thick (2–3 mm), surfaces ± sparsely glandular puberulent and mixed strigillose; pedicel 5–16 mm.  Seeds narrowly obovoid, 1.7–2.2 × 0.6–0.8 mm, chalazal collar inconspicuous, gray to light brown, surface low papil­lose or reticulate; coma readily detached, white, very full, 10–15 mm.  2n = 36.</description>
  <discussion>Epilobium mirabile also has the CC chromosomal arrangement and is one of the least common species of Epilobium in North America; fewer than 20 collections are known, even though its range is quite large.  Most collections are from the Olympic Peninsula in Washington or Waterton-Glacier International Peace Park in Alberta and adjacent Montana.  However, one collection is known from Powell County in central Montana, and one from Manning Provincial Park in British Columbia.  The species may be more widespread but under-collected due to its restricted habitat, mainly on subalpine south-facing scree slopes.</discussion>
  <discussion>Specimens of Epilobium mirabile from the northern Rocky Mountains (Alberta and Montana) have subglabrous stems with strong, raised, strigillose lines and seeds with low papillose surfaces, whereas specimens from the northern Cascades (British Columbia) and Olympic Mountains (Washington) have densely strigillose stems with no raised lines and seeds with reticulate surfaces.  The plants otherwise have very similar and distinctive morphology and ecology.</discussion>
  <description type="phenology">Flowering Jul–Aug.</description>
  <description type="habitat">Subalpine scree slopes, gravelly tussock meadows.</description>
  <description type="elevation">1500–2600 m.</description>
  <description type="distribution">Alta., B.C.; Mont., Wash.</description>
</bio:treatment>
