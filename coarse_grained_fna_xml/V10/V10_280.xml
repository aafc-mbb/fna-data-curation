<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">ONAGRACEAE</taxon_name>
    <taxon_name rank="subfamily" authority="W. L. Wagner &amp; Hoch" date="2007">Onagroideae</taxon_name>
    <taxon_name rank="tribe" authority="Endlicher" date="1830">Epilobieae</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">EPILOBIUM</taxon_name>
    <taxon_name rank="section" authority="Hoch &amp; W. L. Wagner" date="2007">Macrocarpa</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot. Monogr.</publication_title>
      <place_in_publication>83: 82. 2007</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe epilobieae;genus epilobium;section macrocarpa</taxon_hierarchy>
  </taxon_identification>
  <number>5a.</number>
  <description type="morphology">Herbs perennial, suffrutescent, with basal shoots from caudex.  Stems ± woody at base, epidermis peeling proximally.  Leaves opposite proximal to inflorescence, alternate distally.  Flowers actinomorphic; floral tube extremely short for size of flower, without bulbous base and scales inside; petals pink to rose-purple; pollen shed in tetrads; stigma deeply 4-lobed.  Capsules narrowly fusiform to cylindrical, splitting to base, central column persistent, pedicellate.  Seeds numerous, in 1 row per locule, narrowly obovoid, prominently constricted near micropylar end, coma present.</description>
  <discussion>Species 1.</discussion>
  <description type="distribution">w United States.</description>
  <discussion>Section Macrocarpa consists of only Epilobium rigidum, an uncommon species endemic to the Klamath-Siskiyou region along the California-Oregon border in western United States.  Since its publication in the worldwide monograph of Epilobium by C. Haussknecht (1884; see also P. C. Hoch and P. H. Raven 1990), this species has been included consistently in sect. Epilobium in the strict sense (Raven 1976), despite having a number of unusual morphological features.  It has the largest seeds in the genus (2.5–3.4 × 0.9–1.4 mm), with a prominent constriction near the micropylar end (S. R. Seavey et al. 1977), in both features resembling seeds of sects. Cordylophorum, Crossostigma (only E. ravenii), Xerolobium, and Zauschneria but unlike any taxa in sect. Epilobium.  Despite having some of the largest petals in the genus (16–20 mm), E. rigidum has an extremely short floral tube (1–1.8 mm), and this combination of character states is unique.  It shares with sect. Epilobium the chromosome number n = 18, and can form hybrids with some species in that section; however, all hybrids are completely sterile, and no bivalents form at meiotic metaphase I (Seavey and Raven 1977b, 1978).  Analysis of ITS sequence data (D. A. Baum et al. 1994) placed E. rigidum as the sister branch to sect. Epilobium but with essentially no support, even considering the sparse sampling of the section (five species).  R. A. Levin et al. (2004), using both ITS and trnL-F sequence data, found very strong support for both a sparsely sampled sect. Epilobium and for a clade of all sections with chromosome numbers other than n = 18; E. rigidum is weakly supported as the basally diverging branch on the non-sect. Epilobium clade.  The current best molecular evidence places E. rigidum near the base of the genus, possibly on a branch with all other species having n = 18 (sect. Epilobium), or more likely (with weak support) on the branch with all non-n = 18 species, with which it shares similarities in seed and pollen morphology.  This apparent basal position and the unique short floral tube (which might even be seen as transitional to the character state in the tubeless sister genus, Chamaenerion) suggest that E. rigidum is best treated as a separate section, positioned near the base of the two well-supported clades in Epilobium.</discussion>
</bio:treatment>
