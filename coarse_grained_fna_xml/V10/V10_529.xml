<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>Shirley A. Graham</author>
    </source>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="J. Saint-Hilaire" date="unknown">LYTHRACEAE</taxon_name>
    <taxon_name rank="genus" authority="J. F. Gmelin" date="1791">DECODON</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Nat.</publication_title>
      <place_in_publication>1: 656, 677.  1791</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family lythraceae;genus decodon</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="etymology">Greek dekas, ten, and odon, tooth, alluding to combination of five sepals and five alternating epicalyx segments</other_info_on_name>
  </taxon_identification>
  <number>3.</number>
  <other_name type="common_name">Swamp loosestrife</other_name>
  <other_name type="common_name">water willow</other_name>
  <other_name type="common_name">décodon verticillé</other_name>
  <description type="morphology">Shrubs or subshrubs, aquatic, (5–)10–30 dm, glabrous or velutinous.  Stems erect or arching, branched or unbranched, spreading vegetatively by rooting from submerged tips of arching stems, submerged stems often thickened by external layers of spongy aerenchyma.  Leaves opposite or 3-whorled; petiolate; blade lanceolate, base attenuate.  Inflorescences indeterminate, axillary, simple or compound cymes, 3+-flowered.  Flowers pedicellate, actinomorphic, tristylous; floral tube perigynous, campanulate; epicalyx segments ca. 2 times longer than sepals; sepals (4 or)5(–7), to 1/3 floral tube length; petals caducous or deciduous, (4 or)5(–7), rose purple; nectariferous tissue present in lower ovary wall; stamens (8–)10; ovary 3(–5)-locular; placenta elongate; style slender; stigma capitate.  Fruits capsules, walls moderately thick and dry, dehiscence loculicidal.  Seeds 20–30, obpyramidal; cotyledons ± complanate.</description>
  <discussion>Species 1.</discussion>
  <description type="distribution">c, e North America.</description>
  <discussion>Although Decodon is now monospecific and endemic to the central and eastern United States, it formerly had a nearly global distribution in the northern latitudes.  Fossil seeds of Decodon from the Late Cretaceous (73.5 Ma) of northern Mexico represent the second oldest known occurrence of Lythraceae in the world, second only to pollen of Lythrum from the Late Cretaceous (82–81 Ma) of Wyoming.  Decodon was widespread and diverse during the Miocene in both hemispheres; it became severely reduced as climates cooled in the Pliocene and thereafter, ultimately becoming restricted to D. verticillatus in eastern North America (S. A. Graham 2013).</discussion>
  <references>
    <reference>Dorken, M. E. and C. G. Eckert.  2001.  Severely reduced sexual reproduction in northern populations of a clonal plant, Decodon verticillatus (Lythraceae).  J. Ecol. 89: 339–350.</reference>
    <reference>Eckert, C. G. and S. C. H. Barrett.  1994.  Post-pollination mechanisms and the maintenance of outcrossing in self-compatible, tristylous, Decodon verticillatus (Lythraceae).  Heredity 72: 396–411.</reference>
  </references>
</bio:treatment>
