<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>Robin W. Scribailo, Mitchell S. Alix</author>
    </source>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="R. Brown" date="unknown">HALORAGACEAE</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">MYRIOPHYLLUM</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 992.  1753</place_in_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 429.  1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family haloragaceae;genus myriophyllum</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek myrios, countless, and phyllon, leaf, alluding to capillary segments of lower and/or submersed leaves</other_info_on_name>
  </taxon_identification>
  <number>2.</number>
  <other_name type="common_name">Water-milfoil</other_name>
  <other_name type="common_name">myriophylle</other_name>
  <description type="morphology">Herbs, usually monoecious (dioecious in M. aquaticum and M. ussuriense), aquatic to semiaquatic [semiterrestrial or terrestrial]; with fibrous and adventitious nodal roots.  Rhizomes usually present, (absent in M. farwellii).  Stems erect [prostrate], terete, glabrous, hydropoten (aggregates of transfer cells) sometimes present as scattered yellow, red, or brown splotches.  Turions present or absent, lateral and/or terminal.  Leaves submersed or emersed (not emersed in M. farwellii), usually whorled, sometimes alternate, opposite, subopposite, subverticillate, or irregular [scattered], usually heteromorphic (homomorphic in M. aquaticum, M. farwellii, and M. tenellum), emersed leaves usually becoming reduced floral bracts distally; sessile or petiolate; blade unlobed or lobed, pinnatifid, or pectinate, margins usually entire, sometimes serrate, surfaces glabrous, trichomes, when present, usually in axils of leaves and leaf segments, sometimes scattered.  Inflorescences racemes, simple, 80+-flowered, or flowers borne singly (rarely dichasia in M. humile) in axils of emersed leaves (submersed in M. farwellii); bracteoles paired, alternate, opposite subtending leaf, apex often fringed with glandular, red trichomes, sometimes aristate; flowers unisexual or bisexual, proximally pistillate, distally staminate, often with intermediate transitional zone of bisexual flowers.  Flowers 4-merous, staminate petals persistent or caducous, pistillate petals caducous or absent; stamens usually 4 or 8, sometimes 5–7 in bisexual flowers; ovary 4-locular.  Fruit a schizocarp, light green, tan, olive-brown or -green, brown, red-brown, or purple, cylindric, ± ovoid to oblong, or ± globose, cruciate to ± 4-lobed, splitting into (2–)4 mericarps; mericarps compressed to ± flattened, concave, or ± rounded, adaxially rounded, sometimes with 1–4 abaxial ridges, ridges with or without wings, surfaces smooth to ± papillate to ± tuberculate, sometimes with red punctate glands.  x = 7.</description>
  <discussion>Species ca. 68 (14 in the flora).</discussion>
  <description type="distribution">North America, Mexico, Central America, South America, Eurasia, n, nw Africa, Indian Ocean Islands, Pacific Islands, Australia.</description>
  <discussion>M. L. Moody and D. H. Les (2010) realigned some species of Myriophyllum to conform to the results of their molecular analyses, formally recognizing three subgenera, five sections, and five subsections.  Because of strong levels of independent convergence, the subgenera almost completely overlap in vegetative characters.  Although we recognize the phylogenetic merits of the work of Moody and Les, use of their classification here would serve only to confuse and not clarify our attempts to present the most straightforward taxonomic arrangement for Myriophyllum.</discussion>
  <discussion>Although Myriophyllum is easily recognized in the field, positive identification at the species level has been problematic.  Much of this difficulty can be attributed to over-reliance on submersed vegetative material for identification.  As noted by some authors (for example, C. D. Sculthorpe 1967; G. E. Hutchinson 1975), phenotypic plasticity can greatly alter leaf characters such as size and segment number of aquatic plants in different environments.  In some cases, vegetative character states used in taxonomic treatments have been based largely on information repeated from regional floras.  As a result, circumscriptions of species have often not encompassed the full extent of variability observed in some taxonomic characters.  Reliance on these older treatments for identification, coupled with plasticity in vegetative characters, has led to misidentifications, particularly when flowering and fruiting materials are absent.  Most misidentified herbarium specimens of Myriophyllum examined for this flora did not possess reproductive characters.  Currently, the most effective method for identifying vegetative specimens of Myriophyllum is by analysis of ITS and cpDNA sequences (see discussion under 8. M. spicatum).</discussion>
  <discussion>Floral structures and fruits offer good characters to distinguish Myriophyllum species.  The inflorescences in Myriophyllum are described here as racemes because most flowers are short-pedicellate.  The term spike often has been used in Myriophyllum implying that the flowers are sessile.  Admittedly, the distinction is largely semantic, where the pedicels are so short that the flowers appear sessile.  It is important to note that mericarp characters used to distinguish these species are not fully expressed morphologically until late in their development.  As a result, mericarps that are farthest from the shoot apex typically best display diagnostic characters.</discussion>
  <discussion>Many authors have mentioned that bisexual flowers often occur on the raceme where there is a transition from pistillate to staminate flowers.  Bisexual flowers are more common in Myriophyllum than realized, and we found that many staminate flowers possess pistillodes at greater or lesser stages of development.  This condition is most pronounced in M. humile, in which staminate flowers often have complete pistils rather than pistillodes.</discussion>
  <discussion>Leaf characters in Myriophyllum often have been described using terms for compound leaves, such as pinnate in form, with a central rachis and leaf divisions referred to as pinnae.  Because the leaves of Myriophyllum are simple, we use pectinate, central axis, and segments as descriptors.</discussion>
  <discussion>Many Myriophyllum species will occasionally produce a small emergent form when plants become stranded along shorelines by wave action or when water levels decrease.  Only those species that produce an emersed form as part of their normal life history are discussed in some detail.</discussion>
  <discussion>Small ascidiate (flask-shaped) trichomes are found on the stems and in the axils, or at the bases of leaves and leaf segments.  They have often been referred to as hydathodes (A. E. Orchard 1979; S. G. Aiken 1981), but they have been referred to also as enations, myriophyllin glands, pseudostipules, or scales (Orchard).  Because the function of these structures is unknown and they do not appear to be secretory, the best approach would seem to be to refer to them as trichomes.</discussion>
  <discussion>Correct identification of Myriophyllum species in North America is critical in the conservation and management of aquatic habitats because some species are introduced and highly invasive (for example, M. aquaticum and M. spicatum).  This issue is further complicated by hybridization of M. spicatum with native M. sibiricum (see 8. M. spicatum discussion).  The native M. heterophyllum is considered invasive in the northeastern United States and Pacific Northwest, where there is evidence of hybridization with other Myriophyllum species (see 13. M. heterophyllum discussion).</discussion>
  <references>
    <reference>Aiken, S. G.  1978.  Pollen morphology in the genus Myriophyllum (Haloragaceae).  Canad. J. Bot. 56: 976–982.</reference>
    <reference>Aiken, S. G.  1981.  A conspectus of Myriophyllum (Haloragaceae) in North America.  Brittonia 33: 57–69.</reference>
    <reference>Orchard, A. E.  1981.  A revision of South American Myriophyllum (Haloragaceae), and its repercussions on some Australian and North American species.  Brunonia 4: 27–65.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves homomorphic, scalelike, submersed leaves 0.3–1(–1.5) mm.</description>
      <determination>1. Myriophyllum tenellum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves usually heteromorphic (homomorphic in M. aquaticum and M. farwellii), submersed leaves usually pectinate, rarely entire or lobed, 1.5+ mm.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Flowers from axils of submersed leaves.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Mericarps (1–)1.5–2.5 mm, abaxial surface 4-angled, smooth to sparsely tuberculate, winged, transversely hexagonal.</description>
      <determination>2. Myriophyllum farwellii</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Mericarps (0.6–)0.8–1.2 mm, abaxial surface rounded, sparsely to densely tuberculate, not winged, transversely elliptic to ovate.</description>
      <determination>11. Myriophyllum humile</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Flowers from axils of emersed leaves.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Herbs dioecious.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Emersed leaves in whorls of 4–6(–8), pectinate, (20–)25–70(–75) mm, with (14–)16–36(–40) filiform segments; turions absent.</description>
      <determination>3. Myriophyllum aquaticum</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Emersed leaves opposite or in whorls of 3(or 4), usually linear, spatulate, or 2- or 3-lobed, sometimes pectinate,  (1.7–)2.5–9(–10.5) mm, with (0–)2–8(–12) lobed to linear-filiform segments; turions present.</description>
      <determination>4. Myriophyllum ussuriense</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Herbs monoecious.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Stamens 8.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Emersed leaves all pectinate; turions present.</description>
      <determination>5. Myriophyllum verticillatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Emersed leaves transitional, proximally pectinate, distally pinnatifid to lobed, serrate, or entire; turions present or absent.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Distal flowers in axils of alternate leaves; mericarps 1.3–1.6 × 0.3–0.4 mm.</description>
      <determination>6. Myriophyllum alterniflorum</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Distal flowers in axils of whorled leaves; mericarps 1.5–2.7 × 0.6–1.6 mm.</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Bracteoles deltate, margins dentate to serrate; emersed leaves pinnatisect to lobed or entire, ovate to oblong in outline, 2–9 × 1–6 mm, margins dentate to serrulate.</description>
      <determination>7. Myriophyllum quitense</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Bracteoles usually ovate to depressed-ovate or obovate, sometimes elliptic to triangular or rhombic, margins entire or serrate; emersed leaves proximally pectinate to pinnatifid, distally elliptic or obovate, sometimes spatulate in outline, 1–2.3 × 0.6–1(–1.5) mm, margins serrate to shallowly lobed or entire.</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Submersed leaves with (20–)24–36(–42) segments, segments usually parallel and in one plane, forming angles less than 45° with central axis.</description>
      <determination>8. Myriophyllum spicatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Submersed leaves with 6–18(–24) segments, segments often irregular in orientation, not parallel or in one plane, forming angles greater than 45° with central axis.</description>
      <determination>9. Myriophyllum sibiricum</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Stamens 4.</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Emersed leaves (0.6–)0.7–2.3(–2.7) mm; mericarps densely tuberculate proximal to midpoint, tubercles crowded, large, often obscuring wings.</description>
      <determination>10. Myriophyllum laxum</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Emersed leaves 3–17(–31) mm; mericarps papillate and/or sparsely to densely tuberculate, tubercles relatively small, shallow, not obscuring wings.</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Submersed leaves usually alternate or opposite, rarely in whorls of 3(or 4).</description>
      <determination>11. Myriophyllum humile</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Submersed leaves usually in whorls of (3 or)4–6, sometimes subverticillate, rarely alternate.</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Mericarps: abaxial surface sharply 2-angled, with 2 ridges, ridges with prominent membranous, undulating wings, wings erect to reflexed with 6–12 perpendicular ribs.</description>
      <determination>12. Myriophyllum pinnatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Mericarps: abaxial surface bluntly 2- or 4-angled, with 2 or 4 ridges, ridges sometimes with inconspicuous wings proximally, ribs absent.</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Anthers 1.3–2.2 mm; staminate sepals 0.5–0.8(–0.9) mm; pistillate petals 1.5–3 mm; mericarps transversely orbiculate to widely elliptic, abaxial surface inconspicuously 4-angled.</description>
      <determination>13. Myriophyllum heterophyllum</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Anthers 0.5–0.9 mm; staminate sepals (0.1–)0.2–0.4(–0.5) mm; pistillate petals 0.7–1.3 mm; mericarps transversely elliptic to narrowly ovate, abaxial surface usually bluntly 2-angled, sometimes 4-angled.</description>
      <determination>14. Myriophyllum hippuroides</determination>
    </key_statement>
  </key>
</bio:treatment>
