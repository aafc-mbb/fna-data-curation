<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">ONAGRACEAE</taxon_name>
    <taxon_name rank="subfamily" authority="W. L. Wagner &amp; Hoch" date="2007">Onagroideae</taxon_name>
    <taxon_name rank="tribe" authority="Endlicher" date="1830">Epilobieae</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Poson.,</publication_title>
      <place_in_publication>366.  1830</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe epilobieae</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Epilobioideae</taxon_name>
    <taxon_name rank="species" authority="Wood" date="unknown">Alph.</taxon_name>
    <taxon_hierarchy>genus epilobioideae;species alph.</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Boisduvaliinae</taxon_name>
    <taxon_name rank="species" authority="unknown" date="unknown">Raimann</taxon_name>
    <taxon_hierarchy>genus boisduvaliinae;species raimann</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Epilobiinae</taxon_name>
    <taxon_name rank="species" authority="&amp; A. Gray" date="unknown">Torrey</taxon_name>
    <taxon_hierarchy>genus epilobiinae;species torrey</taxon_hierarchy>
  </taxon_identification>
  <number>b2.</number>
  <description type="morphology">Herbs, annual or perennial, sometimes suffrutescent.  Leaves opposite at least near base or throughout, alternate distally, or sometimes alternate throughout; stipules absent.  Flowers actinomorphic or slightly zygomorphic, 4-merous; sepals erect or spreading; stamens 2 times as many as sepals; pollen shed in tetrads or monads.  Fruit a slender, cylindrical, loculicidal capsule.  Seeds (1 or) many per locule, with tuft of hairs (coma) at chalazal end, sometimes without coma.</description>
  <discussion>Genera 2, species 173 (2 genera, 43 species in the flora).</discussion>
  <description type="distribution">North America, Mexico, West Indies, Central America, South America, Eurasia, Africa, Atlantic Islands, Australasia; introduced in Pacific Islands.</description>
  <discussion>Epilobium and its close relatives have been recognized historically either as part of Onagreae (A. P. de Candolle 1828b), sometimes as a subtribe (É. Spach 1834–1848, vol. 4; J. Torrey and A. Gray 1838–1843, vol. 1), or as the distinct Epilobieae (R. Raimann 1893; P. A. Munz 1965; P. H. Raven 1976).  The synapomorphies for Epilobieae as currently delimited include its highly condensed, heteropycnotic chromosomes with a base chromosome number of x = 18, sepals held erect or spreading (not reflexed) throughout anthesis, and the presence of a coma of hairs on the seeds (secondarily lost in some species).  Molecular support for the tribe is strong (97–100% BOOTSTRAP support; D. A. Baum et al. 1994; R. A. Levin et al. 2004).</discussion>
  <discussion>Endlicher established Epilobieae and included Oenothera within it; it is unclear how or whether his concept differed from Onagreae of A. P. de Candolle (1828b).  É. Spach (1834–1848, vol. 4) recognized these genera as Onagreae and differentiated so-called sect. Oenotherinae from sect. Epilobieae, placing in the latter not only Epilobium and related groups, but also Clarkia and its segregates.  J. Torrey and A. Gray (1840) excluded Clarkia from their Epilobiinae and also excluded Boisduvalia, a delimitation also followed by R. Raimann (1893) for his Epilobieae.  Epilobieae did not assume its current delimitation, including only Epilobium and its close relatives, until the works of P. A. Munz (1941) and P. H. Raven (1964).</discussion>
  <discussion>G. L. Stebbins (1971) and P. H. Raven (1976) considered the diverse chromosome numbers in Epilobieae and proposed that the species of Boisduvalia (now a section of Epilobium) with n = 9 or 10 represented the original base chromosome number for the tribe, and that these numbers were derived from x = 11 which is found in Circaeeae, Gongylocarpeae, and Lopezieae (W. L. Wagner et al. 2007).  They proposed a series of aneuploid reductions from n = 9 or 10 to n = 6, followed by polyploidy to produce the array of numbers in Epilobium (n = 12, 13, 15, 16, 18, 30) and Boisduvalia (n = 9, 10, 15, 19).  D. A. Baum et al. (1994) demonstrated that molecular data did not support that hypothesis and found that a monophyletic Chamaenerion (n = 18, 36, 54) forms a strongly distinct sister branch to Epilobium, within which sect. Epilobium (n = 18) is monophyletic and sister to the rest of Epilobium, including the former segregates Boisduvalia and Zauschneria.  The data from Baum et al. suggested that Epilobieae are primitively polyploid, with numbers based on x = 18, which is unique in the family.  Using comparable sampling and some additional genes, R. A. Levin et al. (2004) found strong support for the phylogeny proposed by Baum et al.</discussion>
  <references>
    <reference>Raven, P. H.  1976.  Generic and sectional delimitation in Onagraceae, tribe Epilobieae.  Ann. Missouri Bot. Gard. 63: 326–340.</reference>
    <reference>Seavey, S. R. et al.  1977.  Evolution of seed size, shape, and surface architecture in the tribe Epilobieae (Onagraceae).  Ann. Missouri Bot. Gard. 64: 18–47.</reference>
  </references>
</bio:treatment>
