<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>Bruce K. Holst</author>
    </source>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">MYRTACEAE</taxon_name>
    <taxon_name rank="genus" authority="Swartz" date="1788">CALYPTRANTHES</taxon_name>
    <place_of_publication>
      <publication_title>Prodr.,</publication_title>
      <place_in_publication>5, 79.  1788</place_in_publication>
      <other_info_on_pub>name conserved</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family myrtaceae;genus calyptranthes</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek kalyptra, cap or cover, and anthos, flower, alluding to calyx covering stamens in flower bud</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="P. Browne" date="unknown">Chytraculia</taxon_name>
    <other_info_on_name>name rejected</other_info_on_name>
    <taxon_hierarchy>genus chytraculia</taxon_hierarchy>
  </taxon_identification>
  <number>5.</number>
  <other_name type="common_name">Spicewood</other_name>
  <other_name type="common_name">lid-flower</other_name>
  <description type="morphology">Shrubs or trees, branching predominantly bifurcate, glabrous or pubescent, hairs simple or dibrachiate.  Young stems terete, compressed, or quadrangular, often narrowly 2–keeled to 2–winged, the sharp keels or wings terminating distally between base of petioles.  Leaves opposite; blade ± leathery, venation brochidodromous.  Inflorescences 9–50+-flowered, sub­terminal, panicles [rarely spikes], often paired, opposite leaf axils at proximal node of normal or abortive branch, sometimes subtended by conspicuous, foliaceous bracts, axis usually compressed.  Flowers 0–3(–5)-merous, usually sessile; hypanthium obconic to bowl-shaped, prolonged beyond summit of ovary; calyx closed in bud, forming calyptra, circumscissile, calyptra attached at 1 side in anthesis, usually deciduous; petals 0, 2, or 3(–5), inconspicuous, white, usually attached to calyptra and falling with it; stamens to ca. 200, filaments white; ovary 2-locular; style linear; stigma punctate; ovules 2 per locule.  Fruits berries, bluish black or purplish black, spheroid to oblate, crowned by tubular portion of hypanthium or a circular scar, calyptra remnant occasionally persistent.  Seeds 1 or 2, subglobose to oblate; seed coat papery; cotyledons foliaceous, thin, folded into bundle; hypocotyl elongate, as long as cotyledons and encircling them.</description>
  <discussion>Species ca. 200 (2 in the flora).</discussion>
  <description type="distribution">Florida, Mexico, West Indies, Bermuda, Central America, South America.</description>
  <discussion>Calyptranthes is an easily identified genus throughout most of its range, though it tends to grade into Marlierea Cambessèdes and Myrcia de Candolle ex Guillemin in Amazonia.  The combination of predominantly bifurcate branching and the calyx completely closed in bud and opening circumscissily characterize the genus.  Some species have distinctive, narrowly winged or keeled twigs, with the wing/keel in the plane perpendicular to the insertion of the leaves.  The inflorescence is usually of paired panicles, but these can be reduced to a few sessile flowers or a spike in some South American species.</discussion>
  <discussion>Both species of Calyptranthes in Florida are handsome landscape shrubs or small trees for subtropical areas and provide abundant fruit for wildlife.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Inflorescences tomentulose; calyptrae obtuse to rounded; young stems narrowly 2-winged; midvein of leaves sulcate adaxially.</description>
      <determination>1. Calyptranthes pallens</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Inflorescences glabrous; calyptrae apiculate; young stems terete to compressed (not winged); midvein of leaves convex adaxially.</description>
      <determination>2. Calyptranthes zuzygium</determination>
    </key_statement>
  </key>
</bio:treatment>
