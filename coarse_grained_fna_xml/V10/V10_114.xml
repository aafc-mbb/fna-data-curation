<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">ONAGRACEAE</taxon_name>
    <taxon_name rank="subfamily" authority="W. L. Wagner &amp; Hoch" date="2007">Onagroideae</taxon_name>
    <taxon_name rank="tribe" authority="Dumortier" date="1827">Onagreae</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">OENOTHERA</taxon_name>
    <taxon_name rank="section" authority="Munz in N. L. Britton et al." date="1965">Kleinia</taxon_name>
    <taxon_name rank="species" authority="Pursh" date="1813">albicaulis</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Amer. Sept.</publication_title>
      <place_in_publication>2: 733.  1813</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus oenothera;section kleinia;species albicaulis</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Anogra</taxon_name>
    <taxon_name rank="species" authority="(Pursh) Britton" date="unknown">albicaulis</taxon_name>
    <taxon_hierarchy>genus anogra;species albicaulis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">A.</taxon_name>
    <taxon_name rank="species" authority="Rydberg" date="unknown">bradburiana</taxon_name>
    <taxon_hierarchy>genus a.;species bradburiana</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">A.</taxon_name>
    <taxon_name rank="species" authority="A. Nelson" date="unknown">buffumii</taxon_name>
    <taxon_hierarchy>genus a.;species buffumii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">A.</taxon_name>
    <taxon_name rank="species" authority="Rickett" date="unknown">confusa</taxon_name>
    <taxon_hierarchy>genus a.;species confusa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">A.</taxon_name>
    <taxon_name rank="species" authority="Wooton &amp; Standley" date="unknown">ctenophylla</taxon_name>
    <taxon_hierarchy>genus a.;species ctenophylla</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">A.</taxon_name>
    <taxon_name rank="species" authority="Rydberg" date="unknown">perplexa</taxon_name>
    <taxon_hierarchy>genus a.;species perplexa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Oenothera</taxon_name>
    <taxon_name rank="species" authority="unknown" date="unknown">albicaulis</taxon_name>
    <taxon_name rank="variety" authority="H. Léveillé" date="unknown">xanthosperma</taxon_name>
    <taxon_hierarchy>genus oenothera;species albicaulis;variety xanthosperma</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">O.</taxon_name>
    <taxon_name rank="species" authority="(Wooton &amp; Standley) Tidestrom" date="unknown">ctenophylla</taxon_name>
    <taxon_hierarchy>genus o.;species ctenophylla</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">O.</taxon_name>
    <taxon_name rank="species" authority="Linnaeus" date="unknown">sinuata</taxon_name>
    <taxon_name rank="variety" authority="H. Léveillé" date="unknown">bicolor</taxon_name>
    <taxon_hierarchy>genus o.;species sinuata;variety bicolor</taxon_hierarchy>
  </taxon_identification>
  <number>59.</number>
  <description type="morphology">Herbs winter-annual, densely strigillose, also sparsely villous; from a taproot.  Stems ascending to decumbent, 1–several from base, sometimes unbranched, erect or ascending, 5–30 cm.  Leaves in a basal rosette and cauline, 1.5–10 × 0.3–2.5 cm; blade oblanceolate to oblong, margins subentire or coarsely dentate or pinnatifid.  Flowers 1–3 opening per day near sunset; buds nodding, weakly quadrangular, without free tips; floral tube 15–30 mm, mouth glabrous; sepals 15–30 mm; petals white, fading pink, usually obcordate, sometimes obovate, (15–)20–35(–40) mm; filaments 11–17 mm, anthers 6–10 mm; style 25–50 mm, stigma exserted beyond anthers at anthesis.  Capsules ascending to erect, usually straight, sometimes curved, cylindrical, weakly 4-angled, 20–40 × 3–4 mm, dehiscent 1/2 their length; sessile.  Seeds in 2 rows per locule, ellipsoid to subglobose, 0.8–1.5 × 0.5–0.9 mm, surface regularly pitted, pits in longitudinal lines.  2n = 14.</description>
  <discussion>Oenothera albicaulis is self-incompatible (W. L. Wagner et al. 2007; K. E. Theiss et al. 2010).  Oenothera albicaulis has been reported from southern Nevada, but documentation is needed of its occurrence there.</discussion>
  <discussion>Anogra pinnatifida Spach, Baumannia pinnatifida Spach, Oenothera pinnatifida Nuttall, O. purshiana Steudel, and O. purshii G. Don are illegitimate names that pertain here.</discussion>
  <description type="phenology">Flowering (Feb–)Mar–Jun(–Dec).</description>
  <description type="habitat">Dry, usually sandy flats and slopes.</description>
  <description type="elevation">1000–2300 m.</description>
  <description type="distribution">Ariz., Colo., Idaho, Kans., Mont., Nebr., N.Mex., N.Dak., Okla., S.Dak., Tex., Utah, Wyo.; Mexico (Chihuahua, Sonora).</description>
</bio:treatment>
