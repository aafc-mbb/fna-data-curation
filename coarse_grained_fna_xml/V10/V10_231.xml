<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">ONAGRACEAE</taxon_name>
    <taxon_name rank="subfamily" authority="W. L. Wagner &amp; Hoch" date="2007">Ludwigioideae</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">LUDWIGIA</taxon_name>
    <taxon_name rank="section" authority="(Linnaeus) Baillon" date="1876">Jussiaea</taxon_name>
    <taxon_name rank="species" authority="(Michaux) Greuter &amp; Burdet" date="1987">grandiflora</taxon_name>
    <place_of_publication>
      <publication_title>Willdenowia</publication_title>
      <place_in_publication>16: 448.  1987</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily ludwigioideae;genus ludwigia;section jussiaea;species grandiflora</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Jussiaea</taxon_name>
    <taxon_name rank="species" authority="Michaux" date="1803">grandiflora</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor. Amer.</publication_title>
      <place_in_publication>1: 267.  1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus jussiaea;species grandiflora</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">J.</taxon_name>
    <taxon_name rank="species" authority="unknown" date="unknown">repens</taxon_name>
    <taxon_name rank="variety" authority="(Michaux) Micheli" date="unknown">grandiflora</taxon_name>
    <taxon_hierarchy>genus j.;species repens;variety grandiflora</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">J.</taxon_name>
    <taxon_name rank="species" authority="unknown" date="unknown">repens</taxon_name>
    <taxon_name rank="variety" authority="Hauman" date="unknown">hispida</taxon_name>
    <taxon_hierarchy>genus j.;species repens;variety hispida</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">J.</taxon_name>
    <taxon_name rank="species" authority="Gillies ex Hooker &amp; Arnott" date="unknown">stenophylla</taxon_name>
    <taxon_hierarchy>genus j.;species stenophylla</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">J.</taxon_name>
    <taxon_name rank="species" authority="H. Léveillé" date="unknown">stuckertii</taxon_name>
    <taxon_hierarchy>genus j.;species stuckertii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">J.</taxon_name>
    <taxon_name rank="species" authority="Cambessèdes" date="unknown">uruguayensis</taxon_name>
    <taxon_hierarchy>genus j.;species uruguayensis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Ludwigia</taxon_name>
    <taxon_name rank="species" authority="M. Gómez &amp; Molinet" date="unknown">clavellina</taxon_name>
    <taxon_name rank="variety" authority="(Michaux) M. Gómez" date="unknown">grandiflora</taxon_name>
    <taxon_hierarchy>genus ludwigia;species clavellina;variety grandiflora</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">L.</taxon_name>
    <taxon_name rank="species" authority="(Cambessèdes) H. Hara" date="unknown">uruguayensis</taxon_name>
    <taxon_hierarchy>genus l.;species uruguayensis</taxon_hierarchy>
  </taxon_identification>
  <number>8.</number>
  <other_name type="common_name">Large-flower or Uruguayan primrose-willow</other_name>
  <description type="morphology">Herbs, subshrubs, or emergent aquatics, rooting at lower nodes, sometimes woody at base, white pneu­matophores 8–10 cm often on submerged stems.  Stems erect or ascending to creeping or floating, terete or sometimes angled distally, 20–300(–450) cm, usually densely branched, sometimes simple, glabrous if float­ing, or densely villous and viscid throughout, or rarely just on inflorescence.  Leaves: stipules (rarely in clusters of 3), ovate-deltate, 0.6–2 × 0.6–1.5 mm, fleshy, apex subacute, often mucronate; petiole 0.1–1.1 cm; blade usually lanceolate to (narrowly) elliptic or oblance­olate, rarely narrowly obovate, (1.7–)3.1–8(–10.5) × 0.5–2(–2.5) cm, chartaceous, viscid, base cuneate or attenuate, margins entire, apex obtuse or acute, always glandular-mucronate, surfaces densely villous, some­times less dense adaxially, distal leaves more pubescent than proximal ones; bracts scarcely reduced.  Inflorescences on emergent stems sometimes in leafy racemes, flowers solitary in leaf axils; bracteoles narrowly to broadly obovate, 1–1.2 × 0.7–0.8 mm, succulent, apex acute, oppositely attached at ovary base.  Flowers: se­pals usually deciduous, not persistent on capsule, lanceolate, 6–12(–16) × 2–4 mm, chartaceous, apex acute, sur­faces densely villous; petals yellow, fan-shaped, (12–)16–20(–26) × 11–16(–21) mm, apex rounded, usually emarginate, rarely mucronate; stamens 10(or 12), in 2 unequal series, yellow, filaments reflexed, shorter ones (2.8–)3.8–5.3 mm, longer ones (3.7–)6–6.5 mm, anthers oblong, 1–2.5 × (0.6–)0.8–1.2 mm; ovary subcylindric, terete, 6–12 × 1.5–2.5 mm, apex thick­ened, densely villous; nectary disc slightly raised on ovary apex, yellow, 1.5–2.5 mm diam., lobed, ringed with villous hairs; style yellow, 4.7–6.7(–8) mm, glabrous or sparsely pubescent near base, stigma subcapitate-globose, 1–1.3 × 1.6–2.5 mm, usually exserted beyond anthers.  Capsules subcylindric, terete, straight or curved, (11–)14–25 × 3–4 mm, with thick woody walls, irregularly and tardily dehiscent, villous-viscid, pedicel 13–25(–27) mm.  Seeds embedded in wedge-shaped piece of endocarp, 0.8–1 × 0.8–0.9 mm.  2n = 48.</description>
  <discussion>Ludwigia grandiflora occurs in two disjunct areas: the southeastern United States on the coastal plain of southern South Carolina, Georgia, northern Florida, Louisiana, west to central Texas, and recently in southern California (P. C. Hoch and B. J. Grewell 2012) and Oregon; and central South America from south of the Amazon basin of Brazil and Bolivia where it is very scattered, to Uruguay, northeastern Argentina, and Paraguay where it is very frequent.  It has been collected three times in Guatemala and twice in Missouri, although it is not clearly established in either region.  It usually grows below 200 m elevation, but in Guatemala and in Santa Catarina, Brazil (Smith &amp; , MO), it has been collected as high as Klein 133831200 m elevation.  Populations of L. grandiflora in the United States are fairly variable, although not as much as in South American populations.</discussion>
  <discussion>As noted by Greuter and Burdet, the publication of Jussiaea grandiflora Ruíz &amp; Pavon, which was a synonym of J. peruviana, occurred in 1830, not in 1802 as reported (P. A. Munz 1942; P. H. Raven 1963[1964]).  Therefore, J. grandiflora Michaux in 1803 is legitimate, and J. grandiflora Ruíz &amp; Pavon is an illegitimate homonym.</discussion>
  <description type="phenology">Flowering summer.</description>
  <description type="habitat">Wet places, along slow-moving rivers, streams, canals, ditches, often growing into main channel as aquatic weed.</description>
  <description type="elevation">0–200[–1200] m.</description>
  <description type="distribution">Ala., Ark., Calif., Fla., Ga., Ky., La., Miss., Mo., N.J., N.Y., N.C., Okla., Oreg., Pa., S.C., Tenn., Tex., Va., Wash., W.Va.; Central America (Guatemala); South America (Argentina, Bolivia, Brazil, Paraguay, Uruguay).</description>
</bio:treatment>
