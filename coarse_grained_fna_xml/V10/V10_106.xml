<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">ONAGRACEAE</taxon_name>
    <taxon_name rank="subfamily" authority="W. L. Wagner &amp; Hoch" date="2007">Onagroideae</taxon_name>
    <taxon_name rank="tribe" authority="Dumortier" date="1827">Onagreae</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">OENOTHERA</taxon_name>
    <taxon_name rank="section" authority="(Linnaeus) W. L. Wagner &amp; Hoch" date="2007">Gaura</taxon_name>
    <taxon_name rank="subsection" authority="(Linnaeus) W. L. Wagner &amp; Hoch" date="2007">Gaura</taxon_name>
    <taxon_name rank="species" authority="Krakos &amp; W. L. Wagner" date="2013">dodgeniana</taxon_name>
    <place_of_publication>
      <publication_title>PhytoKeys</publication_title>
      <place_in_publication>28: 66.  2013</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus oenothera;section gaura;subsection gaura;species dodgeniana</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Gaura</taxon_name>
    <taxon_name rank="species" authority="Wooton" date="1898">neomexicana</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>25: 307.  1898</place_in_publication>
      <other_info_on_pub>(as neo-mexicana), not Oenothera neomexicana (Small) Munz 1931</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus gaura;species neomexicana</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">O.</taxon_name>
    <taxon_name rank="species" authority="(Rydberg) W. L. Wagner &amp; Hoch" date="unknown">coloradensis</taxon_name>
    <taxon_name rank="subspecies" authority="(Wooton) W. L. Wagner &amp; Hoch" date="unknown">neomexicana</taxon_name>
    <taxon_hierarchy>genus o.;species coloradensis;subspecies neomexicana</taxon_hierarchy>
  </taxon_identification>
  <number>54.</number>
  <description type="morphology">Herbs biennial, villous and strigillose proximally, leaves glabrate or strigillose, also glandular puberulent distally, sometimes also sparsely villous; from stout, fleshy taproot.  Stems 1 or few-branched from base, 50–120 cm.  Leaves in a basal rosette and cauline, basal 6–20 × 1–3 cm, blade lanceolate to narrowly elliptic; cauline 5–10 × 1–2.5 cm, blade lanceolate to narrowly elliptic, margins subentire or repand-denticulate.  Flowers 4-merous, zygomorphic, opening at sunset; floral tube 10–11 mm; sepals 11–15 mm; petals white, fading pink, rhombic-obovate, 11–14 mm; filaments 6.5–9 mm, anthers 2.5–4 mm, pollen 90–100% fertile; style 22–28 mm, stigma exserted beyond anthers.  Capsules ellipsoid or ovoid, sharply 4-angled, with deep furrows alternating with angles for 2–3 mm from apex, ribbed from base of furrow to base of fruit, 9–11 × 3–5 mm; sessile.  Seeds 2–4, yellowish to light brown, 2–3 mm.  2n = 14.</description>
  <discussion>Oenothera dodgeniana occurs in two disjunct areas: the western foothills of the San Juan Mountains in Archuleta and Huerfano counties, Colorado, and Rio Arriba County, New Mexico; and Sierra Blanca and Sacramento Mountains in Lincoln and Otero counties, south-central New Mexico.  The species was collected once at Durango, La Plata County, Colorado (P. H. Raven and D. P. Gregory 1972[1973]), but has not since been recollected.  Oenothera dodgeniana and O. coloradensis were considered by Raven and Gregory to represent a relict species along the eastern flank of the Rocky Mountains that arose from more widespread species farther to the east, such as O. filiformis.  Oenothera dodgeniana belongs to a subclade which is sister to that containing O. coloradensis, and within that subclade is sister to O. demareei and O. lindheimeri (W. L. Wagner et al. 2013).  Although O. dodgeniana is fairly closely related to O. coloradensis, the two taxa seem to have had independent origins that have led to distributions along the eastern flank of the Rocky Mountains.  Oenothera dodgeniana is self-compatible (Raven and Gregory).</discussion>
  <description type="phenology">Flowering Jun–Sep.</description>
  <description type="habitat">Mountain meadow openings in coniferous forests.</description>
  <description type="elevation">1800–2700 m.</description>
  <description type="distribution">Colo., N.Mex.</description>
</bio:treatment>
