<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">ONAGRACEAE</taxon_name>
    <taxon_name rank="subfamily" authority="W. L. Wagner &amp; Hoch" date="2007">Onagroideae</taxon_name>
    <taxon_name rank="tribe" authority="Dumortier" date="1827">Onagreae</taxon_name>
    <taxon_name rank="genus" authority="A. Jussieu" date="1832">GAYOPHYTUM</taxon_name>
    <taxon_name rank="species" authority="Torrey &amp; A. Gray" date="1840">diffusum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>1: 513.  1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus gayophytum;species diffusum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
  </taxon_identification>
  <number>8.</number>
  <description type="morphology">Herbs usually glabrous to strigillose, sometimes villous.  Stems erect, branched or unbranched near base, much branched distally, usually with 1 or 2 nodes between branches, distal branching dichotomous or lateral branches shortened, 5–60 cm.  Leaves reduced distally, 10–60 × 1–5 mm; petiole 0–10 mm; blade very narrowly lanceolate.  Inflorescences with flowers arising usually as proximally as first 1–20 nodes from base.  Flowers: sepals 0.9–3(–5) mm, reflexed singly or in pairs; petals 1.2–5(–7) mm; pollen 90–100% fertile; stigma hemispheric to subglobose, exserted beyond anthers of longer stamens or surrounded by them at anthesis.  Capsules ascending to reflexed, subterete, 3–15 × 1–1.5 mm, with inconspicuous or conspicuous constrictions between seeds, valve margins somewhat undulate, all valves free from septum after dehiscence, septum straight or sinuous; pedicel 2–10(–15) mm, usually shorter than capsule.  Seeds (3–)6–18, all or most developing, arranged ± parallel to septum and in alternating pattern between locules, crowded, overlapping, often appearing to form 2 irregular rows in each locule, or well spaced, forming a single row in capsule, brown, sometimes mottled with gray, 1–1.6 × 0.5–0.8 mm, glabrous or puberulent.  2n = 28.</description>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <description type="distribution">w North America, n Mexico.</description>
  <discussion>Gayophytumdiffusum consists of a diverse assemblage of tetraploid populations, some of which are similar to every known diploid species except G. humile.  The combination of characteristics of at least five diploid species in various ways suggests that the complex is derived from several independently formed allopolyploids that subsequently hybridized and segregated to produce the observed diversity.</discussion>
  <discussion>Populations of Gayophytum diffusum differ in breeding behavior.  Populations with relatively large flowers and stigmas that extend beyond the anthers are obviously outcrossing, whereas most populations are small-flowered and modally self-pollinated.  It is among the latter that the greatest morphological diversity is found.  Often two or more morphologically different, apparently true-breeding strains can be found growing together.  In such a variable complex, recognition of infraspecific taxa becomes arbitrary.  In this treatment the striking morphological differences associated with breeding behavior have been used as a basis for subspecies recognition.  At some localities the two subspecies intergrade.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals 3–5(–7) mm; sepals 2–3(–5) mm; stigma usually exserted beyond anthers of longer stamens at anthesis.</description>
      <determination>8a. Gayophytum diffusum subsp. diffusum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals 1.2–3 mm; sepals 0.9–2 mm; stigma surrounded by anthers at anthesis.</description>
      <determination>8b. Gayophytum diffusum subsp. parviflorum</determination>
    </key_statement>
  </key>
</bio:treatment>
