<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">ONAGRACEAE</taxon_name>
    <taxon_name rank="subfamily" authority="W. L. Wagner &amp; Hoch" date="2007">Onagroideae</taxon_name>
    <taxon_name rank="tribe" authority="Dumortier" date="1827">Onagreae</taxon_name>
    <taxon_name rank="genus" authority="(Torrey &amp; A. Gray) Nuttall ex Raimann in H. G. A. Engler and K. Prantl" date="1893">CHYLISMIA</taxon_name>
    <taxon_name rank="section" authority="unknown" date="unknown">Chylismia</taxon_name>
    <taxon_name rank="species" authority="(S. Watson) Small" date="1896">parryi</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>23: 193.  1896</place_in_publication>
      <other_info_on_pub>(as Chylisma)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus chylismia;section chylismia;species parryi</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Oenothera</taxon_name>
    <taxon_name rank="species" authority="S. Watson" date="1875">parryi</taxon_name>
    <place_of_publication>
      <publication_title>Amer. Naturalist</publication_title>
      <place_in_publication>9: 270.  1875</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus oenothera;species parryi</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Camissonia</taxon_name>
    <taxon_name rank="species" authority="(S. Watson) P. H. Raven" date="unknown">parryi</taxon_name>
    <taxon_hierarchy>genus camissonia;species parryi</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Chylismia</taxon_name>
    <taxon_name rank="species" authority="(M. E. Jones) Rydberg" date="unknown">tenuissima</taxon_name>
    <taxon_hierarchy>genus chylismia;species tenuissima</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">O.</taxon_name>
    <taxon_name rank="species" authority="Torrey &amp; A. Gray" date="unknown">scapoidea</taxon_name>
    <taxon_name rank="variety" authority="(S. Watson) M. E. Jones" date="unknown">parryi</taxon_name>
    <taxon_hierarchy>genus o.;species scapoidea;variety parryi</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">O.</taxon_name>
    <taxon_name rank="species" authority="M. E. Jones" date="unknown">tenuissima</taxon_name>
    <taxon_hierarchy>genus o.;species tenuissima</taxon_hierarchy>
  </taxon_identification>
  <number>10.</number>
  <description type="morphology">Herbs annual, sparsely to densely villous throughout or, sometimes, glabrate distally.  Stems often intricately branched, 5–80 cm.  Leaves in poorly defined basal rosette and also cauline; petiole 0.3–3.8 cm; blade usually unlobed, very rarely pinnately lobed with few, small lateral lobes, ovate to elliptic, margins sparsely denticulate to subentire, pale or dark brown oil cells lining veins abaxially.  Racemes nodding, with intricate, filiform branches, elongating in fruit.  Flowers opening at sunrise; buds without free tips; floral tube 0.5–2 mm, glabrous or villous inside; sepals 1.5–4 mm; petals bright yellow, often with red dots near base, fading pale yellow or yellowish orange, 2–7 mm; stamens unequal, filaments of antisepalous stamens 1.7–3.5 mm, those of antipetalous ones 1.2–2.5 mm, anthers 0.9–1.2 mm, glabrous; style 4–9 mm, stigma exserted beyond anthers at anthesis.  Capsules erect or ascending, clavate, 4–10 mm; pedicel 4–20 mm.  Seeds 0.7–1.2 mm.  2n = 14.</description>
  <discussion>Chylismia parryi is known from northwestern Arizona (Coconino to Mohave counties) and southwestern Utah (Beaver to Washington counties), and is apparently disjunct to San Juan County, Utah.  It is outcrossing and, perhaps, self-incompatible (P. H. Raven 1962, 1969).  There are two morphological forms of this species.  Raven (1962) noted that a later flowering form has narrower, smaller leaves, and less overall pubescence.  It is not clear what these represent, but Raven (1962) made the combination Oenothera parryi forma tenuissima (M. E. Jones) P. H. Raven for it.  He later (Raven 1969) noted that these plants did not seem to merit formal recognition, without any discussion.</discussion>
  <description type="phenology">Flowering May–Jun(–Sep).</description>
  <description type="habitat">Red clay and sand slopes weathered from red (freshwater-deposited) sandstone cliffs, with Juniperus or Larrea tridentata.</description>
  <description type="elevation">800–1300 m.</description>
  <description type="distribution">Ariz., Utah.</description>
</bio:treatment>
