<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="R. Brown" date="unknown">HALORAGACEAE</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">MYRIOPHYLLUM</taxon_name>
    <taxon_name rank="species" authority="Michaux" date="1803">heterophyllum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>2: 191.  1803</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family haloragaceae;genus myriophyllum;species heterophyllum</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>13.</number>
  <other_name type="common_name">Two-leaf or various-leaved water-milfoil</other_name>
  <other_name type="common_name">myriophylle à feuilles variées</other_name>
  <description type="morphology">Herbs monoecious, aquatic, often forming dense stands.  Stems often branched, to 2.5 m.  Turions absent.  Leaves usually in whorls of 4(–6), often subverticillate, sometimes alter­nate, heteromorphic; petiole to 5 mm; submersed leaves pec­tinate, ovate to obovate in outline, (6–)12–29(–65) × (12–)14–18(–50) mm, segments (10–)12–20(–28), linear-filiform, longest segment (7–)9–25(–29) mm; emersed leaves pectinate to pinnatifid proximally, lanceolate to ovate or elliptic distally, 3–14(–31) × 1–5(–7) mm, margins serrate to lobed.  Inflorescences to 60 cm; flowers proximally pistillate, medially bisexual, distally staminate; bract­eoles cream, ovate to triangular or deltate, 0.6–1.1 ×0.3–0.9 mm, margins serrate to irregularly lobed.  Staminate flowers: sepals cream, lanceolate to nar­rowly triangular, 0.5–0.8(–0.9) × 0.1–0.2 mm; petals per­sistent, cream, elliptic to obovate, 1.4–3 × 0.7–1.3 mm; stamens 4, filaments to 1.6 mm, anthers 1.3–2.2 × 0.3–0.7 mm.  Pistillate flowers: sepals cream, triangular, (0.1–)0.2–0.6 × 0.1–0.3(–0.4) mm; petals caducous, cream, elliptic to obovate, 1.5–2(–3) × 0.8–1 mm; pistils 0.8–1.7 mm, stigmas red to ± purple, to 0.4 mm.  Fruits ovoid to subglobose, deeply 4-lobed.  Mericarps tan to red-brown, cylindric to narrowly ovoid, 1–1.5 ×0.5–0.8 mm, transversely orbiculate to widely elliptic, abaxial surface bluntly 4-angled, rounded to slightly flattened, densely papillate, with 4 shallow, longitudinal ridges, ridges sometimes with inconspicuous, shallow wings proximally, ribs absent.</description>
  <discussion>In flower, Myriophyllum heterophyllum is one of the more distinctive water-milfoils; it has relatively large, wide, ovate bracts and elongate spikes, on which leaves transition from pectinate to entire and often trail along the water surface.  Plants are often very robust and bushy, with thickened red stems and highly crowded leaf whorls.</discussion>
  <discussion>D. H. Les and L. J. Mehrhoff (1999) suggested that Myriophyllum heterophyllum is invasive in New England and progressively spread northward from a more southern native range.  It is known to be introduced in British Columbia.  R. A. Thum et al. (2011) provided genetic evidence that invasive populations of M. heterophyllum from New England, the Pacific Northwest, and California represent multiple introductions from the native Atlantic coastal plain and interior continental range.</discussion>
  <discussion>S. G. Aiken (1981) suggested that Myriophyllum heterophyllum produces turions; we have seen no evidence of this, and it is likely that new shoots produced along rhizomes late in the growing season have been mistaken for turions.</discussion>
  <description type="phenology">Flowering and fruiting May–Oct.</description>
  <description type="habitat">Oligotrophic to eutrophic waters, lakes, ponds.</description>
  <description type="elevation">0–600 m.</description>
  <description type="distribution">B.C., N.B., Ont., P.E.I., Que.; Ala., Ark., Conn., Del., D.C., Fla., Ga., Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mass., Mich., Minn., Miss., Mo., N.H., N.J., N.Y., N.C., Ohio, Okla., Pa., R.I., S.C., Tenn., Tex., Vt., Va., Wash., W.Va., Wis.</description>
</bio:treatment>
