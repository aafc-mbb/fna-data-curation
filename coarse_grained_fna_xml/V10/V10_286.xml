<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">ONAGRACEAE</taxon_name>
    <taxon_name rank="subfamily" authority="W. L. Wagner &amp; Hoch" date="2007">Onagroideae</taxon_name>
    <taxon_name rank="tribe" authority="Endlicher" date="1830">Epilobieae</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">EPILOBIUM</taxon_name>
    <taxon_name rank="section" authority="(Spach) P. H. Raven" date="1977">Crossostigma</taxon_name>
    <taxon_name rank="species" authority="Lindley in W. J. Hooker" date="1832">minutum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 207.  1832</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe epilobieae;genus epilobium;section crossostigma;species minutum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Epilobium</taxon_name>
    <taxon_name rank="species" authority="unknown" date="unknown">minutum</taxon_name>
    <taxon_name rank="variety" authority="Suksdorf" date="unknown">canescens</taxon_name>
    <taxon_hierarchy>genus epilobium;species minutum;variety canescens</taxon_hierarchy>
  </taxon_identification>
  <number>4.</number>
  <other_name type="common_name">Chaparral willowherb</other_name>
  <description type="morphology">Herbs slender.  Stems strict, erect, sometimes reddish green, terete, 3.5–40 cm, simple or freely branched, subglabrous proximally to strigillose and glandular puberulent distally.  Leaves alternate distally, not fasciculate, petiole 0–2 mm, blade subspatulate proximally to lanceolate, oblanceolate, or narrowly elliptical distally, not folded along midrib, 0.9–2.5 × 0.2–0.6 cm, shorter than internodes, base tapered, margins entire or scarcely denticulate, 1–4 teeth per side, lateral veins obscure, apex subacute or often blunt proximally, surfaces subglabrous or with scattered hairs along margins; bracts much reduced, sometimes attached to pedicel.  Inflorescences erect racemes or open panicles, relatively loose and uncrowded, branches thin, mixed strigillose and glandular puberulent.  Flowers erect or, sometimes, nodding in bud; buds broadly ovoid, 1.2–2.5 × 1–1.5 mm; floral tube 1.1–1.5 × 1–1.4 mm, usually with ring of spreading hairs at mouth inside; sepals 0.5–2.5 × 0.4–1.3 mm, apex acute, abaxial surface strigillose, sometimes mixed glandular puberulent; petals white to pink, 2–5 × 1.5–3 mm, apical notch 0.2–1.9 mm; filaments white, those of longer stamens 0.5–3 mm, those of shorter ones 0.3–2 mm; anthers 0.6–1 × 0.5–0.8 mm; ovary 4–9 mm, mixed strigillose and glandular puberulent; style light pink, 1–3.5 mm, stigma subclavate to obscurely 4-lobed, 0.4–0.6 × 0.4–0.5 mm, surrounded by longer anthers.  Capsules 9–28 mm, surfaces strigillose and glandular puberulent; pedicel 3–10 mm.  Seeds obovoid, without constriction, 0.9–1.2 × 0.4–0.6 mm, low chalazal collar 0.1–0.2 mm wide, brown, surface reticulate; coma easily detached, white, 2.5–3 mm.  2n = 26.</description>
  <discussion>Epilobium minutum, like the similar E. ravenii, also occasionally produces cleistogamous flowers, and is modally autogamous in any event.  S. R. Seavey et al. (1977b) observed that E. minutum is less common than E. ravenii in the southern part of their overlapping ranges and more common in the north.  Several sheets (for example, Lawler 3276, California, Butte Co. [MO]; Nelson &amp; Gordon 5573, California, Trinity Co. [MO]) mention that the plants were growing on serpentine soil.  The earliest collection of this species appears to be one made by Archibald Menzies in 1792–1794 under the name E. palustre (BM).</discussion>
  <discussion>Crossostigma lindleyi Spach (a substitute name for Epilobium minutum) and E. lindleyi (Spach) Rydberg are illegitimate names that pertain here.</discussion>
  <description type="phenology">Flowering Apr–Sep.</description>
  <description type="habitat">Open, dry places, along roads, disturbed areas.</description>
  <description type="elevation">90–1900 m.</description>
  <description type="distribution">B.C.; Calif., Idaho, Mont., Nev., Oreg., Wash.</description>
</bio:treatment>
