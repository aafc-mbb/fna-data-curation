<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">ONAGRACEAE</taxon_name>
    <taxon_name rank="subfamily" authority="W. L. Wagner &amp; Hoch" date="2007">Onagroideae</taxon_name>
    <taxon_name rank="tribe" authority="Dumortier" date="1827">Onagreae</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">OENOTHERA</taxon_name>
    <taxon_name rank="section" authority="(Spach) W. L. Wagner &amp; Hoch" date="2007">Calylophus</taxon_name>
    <taxon_name rank="subsection" authority="(Torrey &amp; A. Gray) W. L. Wagner &amp; Hoch" date="2007">Salpingia</taxon_name>
    <taxon_name rank="species" authority="Bentham" date="1839">hartwegii</taxon_name>
    <place_of_publication>
      <publication_title>Pl. Hartw.,</publication_title>
      <place_in_publication>5.  1839</place_in_publication>
      <other_info_on_pub>(as hartwegi)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus oenothera;section calylophus;subsection salpingia;species hartwegii</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Calylophus</taxon_name>
    <taxon_name rank="species" authority="(Bentham) P. H. Raven" date="unknown">hartwegii</taxon_name>
    <taxon_hierarchy>genus calylophus;species hartwegii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Galpinsia</taxon_name>
    <taxon_name rank="species" authority="(Bentham) Britton" date="unknown">hartwegii</taxon_name>
    <taxon_hierarchy>genus galpinsia;species hartwegii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Salpingia</taxon_name>
    <taxon_name rank="species" authority="(Bentham) Raimann" date="unknown">hartwegii</taxon_name>
    <taxon_hierarchy>genus salpingia;species hartwegii</taxon_hierarchy>
  </taxon_identification>
  <number>8.</number>
  <description type="morphology">Herbs perennial, sometimes suff­rutescent, strigillose, glan­dular puberulent, glabrous, hirtellous, or short-pilose; from a stout taproot.  Stems 1–many, erect to ascending, unbranched to densely branched, 4–60 cm.  Leaves 0.3–6.5 × 0.04–1.2 cm, sometimes fascicles of small leaves 0.1–1.5 cm present in non-flowering axils; petiole 0–0.2 cm; blade elliptic, lanceolate, linear, or filiform to ovate or oblanceolate, usually not much reduced distally, proximalmost leaves sometimes obovate to spatulate, base attenuate to obtuse, truncate, or subcordate, sometimes clasping, margins entire or serrate, often undulate, apex acute.  Flowers usually 1 per stem opening per day in afternoon or near sunset; buds with free tips 0.5–6 mm; floral tube 16–50(–60) mm, funnelform in distal 1/2 or less; sepals 7–28 mm; petals yellow, fading pale pinkish or pale purple, 10–35 mm; filaments 4–13 mm, anthers 5–13 mm, pollen 85–100% fertile; style 25–65(–75) mm, stigma yellow, quadrangular, usually exserted beyond anthers.  Capsules 6–40 × 2–4 mm, hard, promptly dehiscent throughout their length.  Seeds obovoid, 1–2.5 mm.  2n = 14, 28.</description>
  <discussion>Subspecies 5 (5 in the flora).</discussion>
  <description type="distribution">sw, c United States, n Mexico.</description>
  <discussion>Oenothera hartwegii consists of five intergrading subspecies, which are generally locally common on rocky, sandy, gypsum, or limestone soil in arid to relatively mesic open areas, in southeastern Colorado, southwestern Kansas, western Oklahoma, Texas (except eastern part), New Mexico, southeastern and east-central Arizona, and in Mexico from Chihuahua, northern Coahuila, and northwestern Tamaulipas south to Aguascalientes.  H. F. Towner (1977) found that O. hartwegii is self-incompatible and usually vespertine; two of the subspecies (filifolia and maccartii) open early in the afternoon and are pollinated both day and evening.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves (except proximalmost): blade base truncate or subcordate and clasping; plants densely pubescent with mixture of hair types, but always short-pilose and usually also hirtellous, sometimes also strigillose, especially on leaves, or glandular puberulent distally.</description>
      <determination>8e. Oenothera hartwegii subsp. pubescens</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves: blade base attenuate or obtuse; plants glabrous, sparsely strigillose, or glandular puberulent.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants usually glabrous throughout, sometimes glandular puberulent on distal parts, especially on ovary.</description>
      <determination>8d. Oenothera hartwegii subsp. fendleri</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants strigillose and/or glandular puberulent, especially on distal parts.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Plants glandular puberulent throughout, more densely so on distal parts, sometimes also sparsely strigillose on ovary and leaves; leaf blades filiform to narrowly lanceolate.</description>
      <determination>8c. Oenothera hartwegii subsp. filifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Plants usually strigillose, rarely glandular puberulent; leaf blade narrowly lanceolate to oblanceolate, sometimes linear.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf blades narrowly lanceolate, sometimes linear, margins entire or shallowly and sparsely serrulate, sometimes undulate; plants sparsely to densely strigillose throughout.</description>
      <determination>8a. Oenothera hartwegii subsp. hartwegii</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf blades usually narrowly lanceolate to lanceolate or oblanceolate, rarely linear, margins subentire or serrulate, usually crinkled-undulate; plants usually sparsely strigillose, sometimes glandular puberulent.</description>
      <determination>8b. Oenothera hartwegii subsp. maccartii</determination>
    </key_statement>
  </key>
</bio:treatment>
