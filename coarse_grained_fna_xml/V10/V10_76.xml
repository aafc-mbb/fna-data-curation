<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">10</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">ONAGRACEAE</taxon_name>
    <taxon_name rank="subfamily" authority="W. L. Wagner &amp; Hoch" date="2007">Onagroideae</taxon_name>
    <taxon_name rank="tribe" authority="Dumortier" date="1827">Onagreae</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">OENOTHERA</taxon_name>
    <taxon_name rank="section" authority="(Linnaeus) W. L. Wagner &amp; Hoch" date="2007">Gaura</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Bot. Monogr.</publication_title>
      <place_in_publication>83: 165. 2007</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family onagraceae;subfamily onagroideae;tribe onagreae;genus oenothera;section gaura</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name rank="genus" authority="Linnaeus" date=" 1753">Gaura</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 347.  1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus gaura</taxon_hierarchy>
  </taxon_identification>
  <number>17k.</number>
  <description type="morphology">Herbs annual, biennial, or perennial, caulescent; from a taproot, sometimes woody or producing rhizomes.  Stems usually erect or ascending, sometimes decumbent, branched or unbranched.  Leaves in a basal rosette and cauline (sometimes not present at flowering), (0.5–)2–8(–13) cm; blade margins sinuate-dentate to denticulate, serrate, lobed, or entire.  Inflorescences solitary flowers in axils of distal leaves, forming a spike, erect or nodding.  Flowers opening near sunset or sunrise; buds erect, terete, without free tips; floral tube 1.5–20[–42] mm, usually lanate in distal 1/2 within; sepals splitting along one suture, remaining coherent and reflexed as a unit at anthesis, or separating in pairs or sometimes individually; petals white [rarely yellow], fading pink to red, purple, or off-white, spatulate to elliptic, rhombic, or, sometimes, oblanceolate, usually clawed; filaments with basal scale 0.3–0.5 mm, these nearly closing mouth of floral tube, or sometimes reduced or absent; stigma deeply divided into (3 or) 4 linear lobes.  Capsules woody and nutlike, ovoid, fusiform, lanceoloid, ellipsoid, obovoid, or pyramidal, (3- or)4-angled, some­times weakly so, or (3- or)4-winged, apex acute to attenuate or, sometimes, rounded, indehiscent, septa fragile, not evident at maturity; sessile, sometimes disarticulating from plant at maturity.  Seeds reduced to 1–4(–8), usually ovoid, rarely oblanceoloid (O. glaucifolia), surface smooth.  2n = 14, 28, 42, 56.</description>
  <discussion>Species 25 (23 in the flora).</discussion>
  <description type="distribution">North America, Mexico, Central America (Guatemala); introduced in South America, Europe, Asia, s Africa, Australia.</description>
  <discussion>Section Gaura consists of 25 species (26 taxa) that are subdivided into eight subsections [seven in the flora area; subsect. Gauridium (Spach) W. L. Wagner &amp; Hoch is is found only in Mexico].  All species have indehiscent capsules, a feature otherwise found in Oenothera only in O. canescens (sect. Gauropsis).  Oenothera havardii (sect. Paradoxus) and O. linifolia (sect. Peniophyllum) have tardily and only partially dehiscent capsules.  Twenty-one species in four subsections (Campogaura, Gaura, Stipogaura, and Xenogaura) have zygomorphic flowers; the other four subsections (Gauridium, Schizocarya, Stenosiphon, and Xerogaura), each with a single species, have actinomorphic, or nearly actinomorphic, flowers.  Since Linnaeus described Gaura, it has been maintained as distinct at the generic level and, at various times, even at the tribal level.  Its distinct status rested on several characteristic features including a scale at the base of the filaments; a peltate indusium at the base of the stigma; indehiscent, nutlike capsules; and seeds reduced to 1–4(–8) (P. H. Raven and D. P. Gregory 1972[1973]; W. L. Wagner et al. 2007).  The most recent molecular studies (G. D. Hoggard et al. 2004; R. A. Levin et al. 2004) place Gaura strongly within the Oenothera clade and equally strongly in a clade with other sections possessing winged/angled capsules that are sometimes indehiscent or nearly so.  Hoggard et al. found strong support for the inclusion of the monotypic Stenosiphon within Gaura; Levin et al. concurred, finding strong support for the monophyly of the Gaura lineage, but placed it unequivocally within Oenothera.  Reconsidering the distinctive features of Gaura, Wagner et al. found that the indusium characterizes the whole genus Oenothera, and the indehiscent fruits seem to characterize a larger clade in the genus.  The reduction in seed number appears to be a strong synapomorphy for the reconstituted Oenothera sect. Gaura.  Wagner et al. recognized eight subsections within sect. Gaura that also includes Stenosiphon.  The subsections are arranged according to the synthesis of morphological characters, crossing analyses, and molecular data (Raven and Gregory; Wagner et al.).  Oenothera anomala Curtis and O. hexandra (Ortega) W. L. Wagner &amp; Hoch are Mexican species that occur well south of the flora area.</discussion>
  <references>
    <reference>Hoggard, G. D. et al.  2004.  The phylogeny of Gaura (Onagraceae) based on ITS, ETS and trnL-F sequence data.  Amer. J. Bot. 91: 139–148.</reference>
    <reference>Raven, P. H. and D. P. Gregory.  1972[1973].  A revision of the genus Gaura (Onagraceae).  Mem. Torrey Bot. Club. 23(1): 1–96.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Filaments without basal scales or with minute scale; flowers nearly actinomorphic.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Floral tubes 6–17 mm; herbs probably biennial, glaucous at least in proximal part 17k.</description>
      <determination>1. Oenothera subsect. Stenosiphon</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Floral tubes 1.5–5 mm; herbs annual, not glaucous	17k.</description>
      <determination>2. Oenothera subsect. Schizocarya</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Filaments with basal scales; flowers zygomorphic or sometimes nearly actinomorphic.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Capsules with a slender stipe (0.5–)2–10 mm	17k.</description>
      <determination>5. Oenothera subsect. Stipogaura</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Capsules usually with a stipe 0.2–2.2 mm.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Capsules pyramidal in distal 1/2, abruptly constricted to a cylindrical proximal part.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Capsules not conspicuously bulging at base of distal pyramidal 1/2; plants not rhizomatous	17k.</description>
      <determination>4. Oenothera subsect. Campogaura</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Capsule conspicuously bulging at base of the distal pyramidal 1/2; plants rhizomatous 17k.</description>
      <determination>6. Oenothera subsect. Xenogaura</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Capsules fusiform, ellipsoid, ovoid, or obovoid and then abruptly constricted or cuneate to base.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Capsules ellipsoid, ovoid, or obovoid	17k.</description>
      <determination>7. Oenothera subsect. Gaura</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Capsules fusiform.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Floral tubes 9–13 mm; flowers nearly actinomorphic	17k.</description>
      <determination>3. Oenothera subsect. Xerogaura</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Floral tubes 3–11(–13) mm; flowers zygomorphic 17k.</description>
      <determination>4. Oenothera subsect. Campogaura</determination>
    </key_statement>
  </key>
</bio:treatment>
