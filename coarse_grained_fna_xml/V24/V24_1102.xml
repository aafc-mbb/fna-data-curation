<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <author>Mary E. Barkworth;</author>
      <date>NA</date>
    </source>
    <other_info_on_meta type="treatment_page">776</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda Ann Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" date="unknown" authority="Barnhart">Poaceae</taxon_name>
    <taxon_name rank="subfamily" date="unknown" authority="Benth.">POOIDEAE</taxon_name>
    <taxon_name rank="tribe" date="unknown" authority="R. Br.">POEAE</taxon_name>
    <taxon_name rank="genus" date="unknown" authority="Host">AMMOPHILA</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus ammophila</taxon_hierarchy>
  </taxon_identification>
  <number>14.64</number>
  <description type="morphology">Plants perennial; strongly rhizomatous. Culms 20-130 cm, erect, glabrous. Leaves mostly basal; sheaths open; auricles absent; ligules membranous, sometimes ciliolate; blades 0.5-8 mm wide, involute or convolute. Inflorescences terminal panicles, dense, cylindrical; branches strongly ascending and overlapping. Spikelets pedicellate, laterally compressed, with 1 floret; rachillas prolonged beyond the florets, glabrous or hairy; disarticulation above the glumes, beneath the florets. Glumes equaling or exceeding the florets, subequal, linear-lanceolate, papery, keeled, acute to acuminate; lower glumes 1-veined; upper glumes 3-veined; calluses short, pilose; lemmas chartaceous, linear-lanceolate, obscurely 3-5-veined, keeled, sometimes slightly rounded at the base, apices entire or minutely bifid, unawned or awned, awns 0.2-0.5 mm, subterminal; paleas equaling the lemmas, often appearing 1-keeled, 2- or 4-veined, central veins close together; lodicules 2, free, membranous, ciliate or glabrous, not toothed; anthers 3, 3-7 mm; ovaries glabrous. Caryopses enclosed by the hardened lemma and palea, ellipsoid, longitudinally grooved; hila about 2/3 as long as the caryopses. x = 7.</description>
  <discussion>Ammophila has two species, one native to the coast of Europe and northern Africa, and one to eastern North America. Both species are effective sand binders and dune stabilizers. They are sometimes mistaken for Leymus arenarius and L. mollis, which grow in the same habitats and have a similar habit, but species of Leymus have more than 1 floret per spikelet.</discussion>
  <references>
    <reference>Cope, E.A. 1994. Further notes on beachgrasses (Ammophila) in northeastern North America. Newsletter New York Fl. Assoc. 5(l):5-7</reference>
    <reference> Reznicek, A.A. and E.J. Judziewicz. 1996. A new hybrid species, xCalammophila don-hensonii (Ammophila breviligulata x Calamagrostis canadensis, Poaceae) from Grand Island, Michigan. Michigan Bot. 35:35-40</reference>
    <reference> Seabloom, E.W. and A.M. Wiedemann. 1994. Distribution and effects of Ammophila breviligulata Fern. (American beachgrass) on the foredunes of the Washington coast. J. Coastal Res. 10:178-188</reference>
    <reference> Stern, R.J. 1983. Morphometric and phenologic variability in Ammophila breviligulata Fernald. Master's thesis, University of Vermont, Burlington, Vermont, U.S.A. 29 pp.</reference>
    <reference> Walker, P.J., C.A. Paris, and D.S. Barrington. 1998. Taxonomy and phylogeography of the North American beachgrasses. Amer. J. Bot. 85, Suppl.:87 [abstract].</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Ligules 1-4.6 mm long, truncate to obtuse</description>
      <determination>1 Ammophila breviligulata</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Ligules 10-35 mm long, acute and bifid or lacerate</description>
      <determination>2 Ammophila arenaria</determination>
    </key_statement>
  </key>
  <description type="distribution">Conn.;N.J.;N.Y.;Mass.;Vt.;Wash.;Va.;Del.;Maine;Wis.;Pacific Islands (Hawaii);N.H.;N.C.;Pa.;R.I.;B.C.;N.B.;Nfld. and Labr.;N.S.;Ont.;P.E.I.;Que.;Calif.;Ill.;Ind.;Md.;Ohio;Minn.;Mich.;S.C.;Oreg.</description>
</bio:treatment>
