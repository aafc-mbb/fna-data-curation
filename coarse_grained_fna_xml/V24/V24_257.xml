<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <author>Michael B. Piep;</author>
      <date>NA</date>
    </source>
    <other_info_on_meta type="treatment_page">187</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" date="unknown" authority="Barnhart">Poaceae</taxon_name>
    <taxon_name rank="subfamily" date="unknown" authority="Benth.">POOIDEAE</taxon_name>
    <taxon_name rank="tribe" date="unknown" authority="Harz">BRACHYPODIEAE</taxon_name>
    <taxon_name rank="genus" date="unknown" authority="P. Beauv.">BRACHYPODIUM</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe brachypodieae;genus brachypodium</taxon_hierarchy>
  </taxon_identification>
  <number>11.01</number>
  <description type="morphology">Plants perennial or annual; rhizomatous or cespitose, rhizomes often extensively branched. Culms 5-200 cm, erect or decumbent, often rooting at the lower nodes, sometimes branched above the base; nodes often pubescent. Leaves not basally concentrated; sheaths open, margins overlapping, not fused; auricles absent; ligules membranous, entire, toothed, or ciliate; blades flat or convolute, often attenuate. Inflorescences spikelike racemes, most or all nodes with 1 spikelet, sometimes some with 2-3, most or all spikelets appressed to strongly ascending; disarticulation above the glumes, beneath the florets. Spikelets 14-80 mm, terete to laterally compressed, with (3)5-24 florets. Glumes unequal, 1/2 as long as to equaling the adjacent lemmas, lanceolate, membranous, apices obtuse to acuminate, lower glumes 3-7-veined, upper glumes 5-9-veined; lemmas usually membranous, sometimes coriaceous at maturity, rounded on the back, (5)7-9-veined, apices obtuse or acute, unawned or terminally awned; paleas shorter than to slightly longer than the lemmas, with 2 well-developed veins, sometimes with minor veins in between, keeled over the well-developed veins, keels strongly ciliate; lodicules 2, oblong, attenuate distally, margins ciliate or apices puberulent; anthers 3; styles 2, free to the base, white. Caryopses oblong, flattened, apices pubescent; hila linear, x = 5,7, 9.</description>
  <discussion>Brachypodium is a genus of about 18 species, with about 15 species in Eurasia, centered on the Mediterranean, and three in the Western Hemisphere, centered in Mexico. All five species in the Flora region are Eurasian. Four of the five species have been used in the western United States in seeding trials for mountain rangeland.</discussion>
  <references>
    <reference>Catalan, P. and R.G. Olmstead. 2000. Phylogenetic reconstruction of the genus Brachypodium P. Beauv. (Poaceae) from combined sequences of chloroplast ndbV gene and nuclear ITS. PI. Syst. Evol. 200:1-19</reference>
    <reference> Catalan, P., Y. Shi, L. Armstrong, and C.A. Stace. 1995. Molecular phylogeny of the grass genus Brachypodium P. Beauv. based on RFLP and RAPD analysis. Bot. J. Linn. Soc. 113:263-280</reference>
    <reference> False-Brome Working Group, [viewed 2006]. Home page. http://www.appliedeco.org/FBWG.htm</reference>
    <reference> Hull, A.C., Jr. 1974. Species for seeding mountain rangelands in southeastern Idaho, northeastern Utah, and western Wyoming. J. Range Managem. 27:150-153</reference>
    <reference> Khan, M.A. and C.A. Stace. 1998. Breeding relationships in the genus Brachypodium (Poaceae: Pooideae). Nordic J. Bot. 19:257-269</reference>
    <reference> Lucchese, F. 1990. Revision and distribution of Brachypodium phoenicoides (L.) Roemer et Schultes in Italy. Ann. Bot. (Rome) 48:163-177</reference>
    <reference> Nevski, S.A. 1963. Genus 193. Brachypodium P.B. Pp. 472-474 in R.Yu. Rozhevits [R.J. Roshevitz] and B.K. Shishkin [Schischkin] (eds.). Flora of the U.S.S.R., vol. 2, trans. N. Landau (series ed. V.L. Komarov). Published for the National Science Foundation and the Smithsonian Institution, Washington, D.C. by the Israel Program for Scientific Translations, Jerusalem, Israel. 622 pp. [English translation of Flora SSSR, vol. II. 1934. Botanicheskii Institut Im. V.L. Komarova, Akademiya Nauk, Leningrad [St. Petersburg], Russia. 778 pp.]</reference>
    <reference> Rivas-Martinez, S., D. Sanchez-Mata, and M. Costa. 1999. North American boreal and western temperate forest vegetation. Itinera Geobot. 12:3-331</reference>
    <reference> Schippmann, U. 1991. Revision der europaischen Arten der Gattung Brachypodium Palisot de Beauvois (Poaceae). Boissiera 45:1-249.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Plants annual; spikelets laterally compressed; anthers 0.5-1.1 mm long</description>
      <determination>1 Brachypodium distachyon</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Plants perennial; spikelets terete or subterete; anthers 2.8-6 mm long.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Lemma awns 7-15 mm long, as long as or longer than the lemmas</description>
      <determination>2 Brachypodium sylvaticum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Lemma awns absent or to 7 mm long, shorter than the lemmas.</description>
      <next_statement_id>3.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Blades with all veins more or less equally prominent on the adaxial surfaces</description>
      <determination>3 Brachypodium phoenicoides</determination>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Blades with the primary veins separated by finer secondary veins on the adaxial surfaces.</description>
      <next_statement_id>4.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4.</statement_id>
      <description type="morphology">Leaf blades flat, dark green, abaxial surfaces scabrous, not shiny; lemmas usually hairy</description>
      <determination>4 Brachypodium pinnatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>4.</statement_id>
      <description type="morphology">Leaf blades involute or flat, light green, abaxial surfaces smooth or almost so, conspicuously shiny; lemmas usually glabrous</description>
      <determination>5 Brachypodium rupestre</determination>
    </key_statement>
  </key>
  <description type="distribution">N.J.;Mass.;Tex.;Calif.;Pacific Islands (Hawaii);Colo.;Oreg.</description>
</bio:treatment>
