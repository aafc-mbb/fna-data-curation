<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <author>Robert I. Lonard;</author>
      <date>NA</date>
    </source>
    <other_info_on_meta type="treatment_page">448</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" date="unknown" authority="Barnhart">Poaceae</taxon_name>
    <taxon_name rank="subfamily" date="unknown" authority="Benth.">POOIDEAE</taxon_name>
    <taxon_name rank="tribe" date="unknown" authority="R. Br.">POEAE</taxon_name>
    <taxon_name rank="genus" date="unknown" authority="C.C. Gmel.">VULPIA</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus vulpia</taxon_hierarchy>
  </taxon_identification>
  <number>14.04</number>
  <description type="morphology">Plants usually annual, rarely perennial. Culms 5-90 cm, erect or ascending from a decumbent base, usually glabrous. Sheaths open, usually glabrous; auricles absent; ligules usually shorter than 1 mm, membranous, usually truncate, ciliate; blades flat or rolled, glabrous or pubescent. Inflorescences panicles or racemes, sometimes spikelike, usually with more than 1 spikelet associated with each node; branches 1-3 per node, appressed or spreading, usually glabrous, scabrous. Spikelets pedicellate, laterally compressed, with 1-11(17) florets, distal florets reduced; disarticulation above the glumes and beneath the florets, occasionally also at the base of the pedicels. Glumes shorter than the adjacent lemmas, subulate to lanceolate, apices acute to acuminate, unawned or awn-tipped; lower glumes much shorter than the upper glumes, 1-veined; upper glumes 3-veined; rachillas terminating in a reduced floret; calluses blunt, glabrous; lemmas membranous, lanceolate, 3-5-veined, veins converging distally, margins involute over the edges of the caryopses, apices entire, acute to acuminate, mucronate or awned; paleas usually slightly shorter than to equaling the lemmas, sometimes longer; anthers usually 1, rarely 3 in chasmogamous specimens. Caryopses shorter than the lemmas, concealed at maturity, elongate, dorsally compressed, curved in cross section, falling with the lemma and palea. x = 7.</description>
  <discussion>Vulpia, a genus of 30 species, is most abundant in Europe and the Mediterranean region (Cotton and Stace 1967). The Flora region has three native and three introduced species. Most species, including ours, are weedy, cleistogamous annuals, usually having one anther per floret. Festuca, in which Vulpia is sometimes included, consists of chasmogamous species having three anthers per floret. The two genera are closely related to each other. Sterile hybrids between Vulpia and Festuca, and Vulpia and Lolium, are known.</discussion>
  <references>
    <reference>Cotton, R. and C.A. Stace. 1967. Taxonomy of the genus Vulpia (Gramineae): I. Chromosome numbers and geographical distribution of the Old World species. Genetica 46:235-255</reference>
    <reference> Lonard, R.I. and F.W. Gould. 1974. The North American species of Vulpia (Gramineae). Madrono 22:217-230</reference>
    <reference> Stace, C.A. 1975. Wild hybrids in the British flora. Pp. 111-125 in S.M. Walters (ed.). European Floristic and Taxonomic Studies. E.W. Classey, Faringdon, England. 144 pp.</reference>
  </references>
  <key>
    <discussion>In the key and descriptions, the spikelet and lemma measurements exclude the awns.</discussion>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Lower glumes less than 1/2 the length of the upper glumes.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Lemmas 5-veined, glabrous except the margins sometimes ciliate; rachilla internodes 0.75-1.9 mm long</description>
      <determination>1 Vulpia myuros</determination>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Lemmas 3(5)-veined, pubescent or glabrous, the margins ciliate; rachilla internodes 0.4-0.9 mm long</description>
      <determination>6 Vulpia ciliata</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Lower glumes 1/2 or more the length of the upper glumes.</description>
      <next_statement_id>3.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Lemmas 2.5-3.5 mm long, the apices more pubescent than the bases; caryopses 1.5-2.5 mm long</description>
      <determination>2 Vulpia sciurea</determination>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Lemmas 2.7-9.5 mm long, if pubescent, the apices no more so than the bases but occasionally ciliate; caryopses 1.7-6.5 mm long.</description>
      <next_statement_id>4.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4.</statement_id>
      <description type="morphology">Panicle branches 1-2 per node; spikelets with 4-17 florets; rachilla internodes 0.5-0.7 mm long; awn of the lowermost lemma in each spikelet 0.3-9 mm long; caryopses 1.7-3.7 mm long</description>
      <determination>3 Vulpia octoflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>4.</statement_id>
      <description type="morphology">Panicle branches solitary; spikelets with 1-8 florets; rachilla internodes 0.6-1.2 mm long; awn of the lowermost lemma in each spikelet 2-20 mm long; caryopses 3.5-6.5 mm long.</description>
      <next_statement_id>5.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5.</statement_id>
      <description type="morphology">Panicle branches appressed to erect at maturity, without axillary pulvini; paleas equal to or shorter than the lemmas</description>
      <determination>4 Vulpia bromoides</determination>
    </key_statement>
    <key_statement>
      <statement_id>5.</statement_id>
      <description type="morphology">Panicle branches spreading to reflexed at maturity, with axillary pulvini; paleas usually slightly longer than the lemmas</description>
      <determination>5 Vulpia microstachys</determination>
    </key_statement>
  </key>
  <description type="distribution">Conn.;N.J.;N.Y.;Fla.;S.Dak.;Wyo.;Wash.;Ariz.;Colo.;Ga.;Iowa;Idaho;Maine;Mich.;Miss.;Nebr.;Nev.;Okla.;Pa.;S.C.;Wis.;W.Va.;Del.;D.C;Pacific Islands (Hawaii);Kans.;Minn.;N.Dak.;Mass.;N.H.;R.I.;Vt.;N.Mex.;Tex.;La.;Oreg.;N.C.;Tenn.;Calif.;Puerto Rico;Alaska;Ala.;Va.;Ark.;Ill.;Ind.;Md.;Ohio;Utah;Mo.;Mont.;Ky.;Alta.;B.C.;Man.;N.W.T.;Ont.;Que.;Sask.;Yukon</description>
</bio:treatment>
