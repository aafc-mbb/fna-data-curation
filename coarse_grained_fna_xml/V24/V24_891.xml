<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <author>Mary E. Barkworth;</author>
      <date>NA</date>
    </source>
    <other_info_on_meta type="treatment_page">624</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" date="unknown" authority="Barnhart">Poaceae</taxon_name>
    <taxon_name rank="subfamily" date="unknown" authority="Benth.">POOIDEAE</taxon_name>
    <taxon_name rank="tribe" date="unknown" authority="R. Br.">POEAE</taxon_name>
    <taxon_name rank="genus" date="unknown" authority="P. Beauv.">DESCHAMPSIA</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe poeae;genus deschampsia</taxon_hierarchy>
  </taxon_identification>
  <number>14.26</number>
  <description type="morphology">Plants usually perennial, sometimes annual; cespitose or tufted. Culms 5-140 cm, hollow, erect. Leaves usually mainly basal, often forming a dense tuft; sheaths open; auricles absent; ligules membranous, decurrent, rounded to acuminate; blades often all or almost all tightly rolled or folded and some flat, sometimes most flat, others rolled or folded. Inflorescences terminal panicles, open or contracted; disarticulation above the glumes, beneath the florets. Spikelets 3-9 mm, with 2(3) florets in all or almost all spikelets, florets usually bisexual, sometimes viviparous; rachillas hairy, usually prolonged more than 0.5 mm beyond the base of the distal floret, sometimes terminating in a highly reduced floret. Glumes subequal to unequal, usually exceeding the adjacent florets, often exceeding all florets, 1- or 3-veined, acute to acuminate; calluses antrorsely strigose; lemmas obscurely (3)5-7-veined, rounded over the back, apices truncate-erose to 2-4-toothed, awned, awns usually attached on the lower 1/2 of the lemmas, occasionally subapical, straight to strongly geniculate, slightly to strongly twisted proximally, straight distally; paleas shorter than the lemmas, 2-keeled, keels often scabrous; lodicules 2, lanceolate to ovate-lanceolate, usually entire; anthers 3; ovaries glabrous; styles 2. Caryopses oblong; embryos about 1/4 the length of the caryopses. x = 7.</description>
  <discussion>Deschampsia includes 20-40 species. It is best represented in the Americas and Eurasia, but it grows in cool, damp habitats throughout the world. Seven species are native to the Flora region; none of the remaining species have been introduced.</discussion>
  <discussion>Deschampsia differs from Vahlodea (p. 691), which it used to include, in having primarily basal, rather than primarily cauline, leaves, and hairy rachillas that extend more than 0.5 mm beyond the base of the distal floret in a spikelet. Trisetum (p. 744) differs from Deschampsia primarily in its more acute, bifid lemmas, and in having awns that are inserted at or above the midpoint of the lemmas. In Deschampsia, the awns are usually inserted near the base.</discussion>
  <discussion>Because the treatments of Deschampsia brevifolia and D. sukatschewii were revised shortly before going to press, the maps are preliminary, particularly with respect to the Canadian distribution of these two species.</discussion>
  <references>
    <reference>Aiken, S.G., L.L. Consaul, and M.J. Dallwitz. 1995 on. Grasses of the Canadian Arctic Archipelago: Descriptions, illustrations, identification and information retrieval, http://www.mun.ca/biology/delta/arcticf/poa/index.htm</reference>
    <reference> Chiapella, J. 2000. The Deschampsia cespitosa complex in central and northern Europe: A morphological analysis. Bot. J. Linn. Soc. 134:495-512</reference>
    <reference> Chiapella, J. and N.S. Probatova. 2003. The Deschampsia cespitosa complex (Poaceae: Aveneae) with special reference to Russia. Bot. J. Linn. Soc. 142:213-228</reference>
    <reference> Clarke, G.C.S. 1980. Deschampsia Beauv. Pp. 225-227 in T.G. Tutin, V.H. Heywood, N.A. Burges, D.M. Moore, D.H. Valentine, S.M. Walters, and DA. Webb (eds.). Flora Europaea, vol. 5. Cambridge University Press, Cambridge, England. 452 pp.</reference>
    <reference> Hulten, E. 1960. Flora of the Aleutian Islands and Westernmost Alaska Peninsula with Notes on the Flora of Commander Islands. J. Cramer, Weinheim, Germany. 376 pp.</reference>
    <reference> Kawano, S. 1966. Biosystematic studies of the Deschampsia caespitosa complex with special reference to the karyology of Icelandic populations. Bot. Mag. (Tokyo) 79:293-307</reference>
    <reference> Lawrence, W.E. 1945. Some ecotypic relations of Deschampsia caespitosa. Amer. J. Bot. 32:298-314</reference>
    <reference> McLachlan, K.I., S.G. Aiken, L.P. Lefkovitch, and S.A. Edlund. 1989. Grasses of the Queen Elizabeth Islands. Canad. J. Bot. 67:2088-2105</reference>
    <reference> Tsvelev, N.N. 1995. Deschampsia. Pp. 150-163 in J.G. Packer (ed., English edition). Flora of the Russian Arctic, vol. 1, trans. G.C.D. Griffiths. University of Alberta Press, Edmonton, Alberta, Canada [English translation of A.I. Tolmachev (ed.). 1964. Arkticheskaya Flora SSSR, vol. 2. Nauka, Leningrad [St. Petersburg], Russia].</reference>
  </references>
  <key>
    <discussion>Lemma length, awn attachment, and awn length should be examined on the lower florets within the spikelets. The upper florets often have shorter lemmas, and shorter awns that are attached higher on the back than those of the lower florets.</discussion>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">All or most spikelets viviparous; panicle branches smooth</description>
      <determination>5 Deschampsia alpina</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">All or most spikelets bisexual or, if viviparous, the panicle branches scabrous.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Plants annual; awns strongly geniculate</description>
      <determination>7 Deschampsia danthonioides</determination>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Plants perennial; awns straight to strongly geniculate.</description>
      <next_statement_id>3.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Lemmas scabridulous or puberulent, dull</description>
      <determination>8 Deschampsia flexuosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Lemmas glabrous, shiny.</description>
      <next_statement_id>4.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4.</statement_id>
      <description type="morphology">Glumes mostly green, apices purple; panicles narrowly elongate, 0.5-1.5(2) cm wide, appearing greenish</description>
      <determination>6 Deschampsia elongata</determination>
    </key_statement>
    <key_statement>
      <statement_id>4.</statement_id>
      <description type="morphology">Glumes purplish proximally, sometimes over more than 1/2 their surface, whitish to golden distally; panicles usually pyramidal or ovate, sometimes narrowly elongate, 0.5-30 cm wide, appearing bronze to dark purple (D. cespitosa complex).</description>
      <next_statement_id>5.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5.</statement_id>
      <description type="morphology">Spikelets 6-7.5 mm long; culms sometimes decumbent and rooting at the lower nodes; plants of sandy areas around lakes in the Northwest Territories and northern Saskatchewan</description>
      <determination>2 Deschampsia mackenzieana</determination>
    </key_statement>
    <key_statement>
      <statement_id>5.</statement_id>
      <description type="morphology">Spikelets 2-7.6 mm; culms erect, not rooting at the lower nodes; plants of gravels, wet meadows, and bogs, widely distributed in cooler regions of North America.</description>
      <next_statement_id>6.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6.</statement_id>
      <description type="morphology">Spikelets strongly imbricate, often rather densely clustered on the ends of the branches, sometimes evenly distributed on the branches; glumes and lemmas dark purple proximally for over more than 1/2 their surface; lemmas 2.2-4- mm long</description>
      <determination>4 Deschampsia brevifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>6.</statement_id>
      <description type="morphology">Spikelets usually not or only moderately imbricate, not in dense clusters at the ends of the branches; glumes usually purple over less than 1/2 their surface, often with a green base, a distal purple band, and pale apices; lemmas 2-5(7)mm long.</description>
      <next_statement_id>7.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7.</statement_id>
      <description type="morphology">Basal blades with 5-11 ribs, usually most or all ribs scabridulous or scabrous, outer ribs often more strongly so, sometimes the ribs only papillose or puberulent, usually at least some blades flat and 1-4 mm wide, the majority folded or rolled and 0.5-1 mm in diameter; lower glumes often scabridulous distally over the midvein; lower panicle branches often scabridulous or scabrous, sometimes smooth</description>
      <determination>1 Deschampsia cespitosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>7.</statement_id>
      <description type="morphology">Basal blades with 3-5 ribs, ribs usually smooth or papillose, sometimes puberulent or the outer ribs scabridulous, all blades of the current year usually strongly involute and hairlike, 0.3-0.5(0.8) in diameter; lower glumes smooth over the midvein; lower panicle branches usually smooth, sometimes sparsely scabridulous</description>
      <determination>3 Deschampsia sukatschewii</determination>
    </key_statement>
  </key>
  <description type="distribution">Conn.;N.J.;N.Y.;N.Mex.;Wash.;Va.;Del.;D.C;Wis.;Alaska;Ala.;Ark.;Calif.;Ga.;Mass.;Md.;Mich.;Minn.;N.C.;N.Dak.;N.H.;Ohio;Okla.;Pa.;R.I.;S.C.;Tenn.;Vt.;W.Va.;Pacific Islands (Hawaii);Maine;Wyo.;Nev.;Colo.;Ill.;Ind.;Ariz.;Idaho;Alta.;B.C.;Greenland;Man.;N.B.;Nfld. and Labr.;N.S.;N.W.T.;Nunavut;Ont.;P.E.I.;Que.;Sask.;Yukon;Utah;S.Dak.;Mont.;Ky.;Oreg.</description>
</bio:treatment>
