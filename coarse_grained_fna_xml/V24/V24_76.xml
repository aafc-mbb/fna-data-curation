<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <author>Mary E. Barkworth; Laurel K. Anderton;</author>
      <date>NA</date>
    </source>
    <other_info_on_meta type="treatment_page">68</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" date="unknown" authority="Barnhart">Poaceae</taxon_name>
    <taxon_name rank="subfamily" date="unknown" authority="Benth.">POOIDEAE</taxon_name>
    <taxon_name rank="tribe" date="unknown" authority="Endl.">MELICEAE</taxon_name>
    <taxon_name rank="genus" date="unknown" authority="R. Br.">GLYCERIA</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe meliceae;genus glyceria</taxon_hierarchy>
  </taxon_identification>
  <number>9.01</number>
  <description type="morphology">Plants usually perennial, rarely annual; rhizomatous. Culms (10)20-250 cm, erect or decumbent, freely rooting at the lower nodes, not cormous based. Sheaths closed for at least 3/4 their length, often almost entirely closed; ligules scarious, erose to lacerate; blades flat or folded. Inflorescences terminal, usually panicles, sometimes racemes in depauperate specimens, branches appressed to divergent or reflexed. Spikelets cylindrical and terete or oval and laterally compressed, with 2-16 florets, terminal floret in each spikelet sterile, reduced; disarticulation above the glumes, below the florets. Glumes much smaller than to equaling the adjacent lemmas, 1-veined, obtuse or acute, often erose; lower glumes 0.3-4.5 mm; upper glumes 0.6-7 mm; calluses glabrous; lemmas membranous to thinly coriaceous, rounded over the back, smooth or scabrous, glabrous or hairy, hairs to about 0.1 mm, 5-11-veined, veins usually evident, often prominent and ridged, not or scarcely converging distally, apical margins hyaline, sometimes with a purplish band below the hyaline portion, apices acute to rounded or truncate, entire, erose, or irregularly lobed, unawned; paleas from shorter than to longer than the lemmas, keeled, keels sometimes winged; lodicules thick, sometimes connate, not winged; anthers (1)2-3; ovaries glabrous; styles 2-branched, branches divergent to recurved, plumose distally. x = 10.</description>
  <discussion>Glyceria includes approximately 35 species, all of which grow in wet areas. All but five species are native to the Northern Hemisphere. The genus is represented in the Flora region by 13 native and 3 introduced species, as well as 3 named hybrids. One additional European species, G. notata, is included in this treatment because it has been reported to be present in the region.</discussion>
  <discussion>All native species of Glyceria are palatable to livestock. They are rarely sufficiently abundant to be important forage species. Some grow in areas that are soon degraded by grazing. Glyceria maxima can cause cyanide poisoning in cattle. Species in sects. Striatae and Hydropoa have potential as ornamentals.</discussion>
  <discussion>Glyceria resembles Puccinellia in the structure of its spikelets and its preference for wet habitats; it differs in its inability to tolerate highly alkaline soils, and its usually more flexuous panicle branches, closed leaf sheaths, and single-veined upper glumes. Some species are apt to be confused with Torreyochloa pallida, another species associated with wet habitats but one that, like Puccinellia, has open leaf sheaths. Glyceria includes several species that appear to intergrade. In some cases, the distinctions between such taxa are more evident in the field, particularly when they are sympatric. Recognition of such taxa at the specific level is merited unless it can be shown that all the distinctions between them are inherited as a group.</discussion>
  <discussion>The three named North American hybrids are Glyceria xgatineauensis Bowden, G. xottawensis Bowden, and G. ×occidentalis (Piper) J.C. Nelson. The first two were named as hybrids; they are not included in the key and are mentioned only briefly in the descriptions. Glyceria ×occidentalis has hitherto been treated as a species. Studies finished shortly before completion of this volume indicate that it, too, consists of hybrids (Whipple et al. [in press]). It is included in the key and provided with a full description.</discussion>
  <discussion>Culm thickness is measured near midlength of the basal internode; it does not include leaf sheaths. Unless otherwise stated, ligule measurements reflect both the basal and upper leaves. Ligules of the basal leaves are usually shorter than, but similar in shape and texture to, those of the upper leaves. The number of spikelets on a branch is counted on the longest primary branches, and includes all the spikelets on the secondary (and higher order) branches of the primary branch. Pedicel lengths are measured for lateral spikelets on a branch, not the terminal spikelet. Lemma characteristics are based on the lowest lemmas of most spikelets in a panicle. There is often, unfortunately, considerable variation within a panicle.</discussion>
  <references>
    <reference>Anderson, J.E. and A.A. Reznicek. 1994. Glyceria maxima (Poaceae) in New England. Rhodora 96:97-101</reference>
    <reference> Borrill, M. 1955. Breeding systems and compatibility in Glyceria. Nature 175:561-563</reference>
    <reference> Bowden, W.M. 1960. Chromosome numbers and taxonomic notes on northern grasses: HI. Festuceae. Canad. J. Bot. 38:117-131</reference>
    <reference> Chester, E.W., B.E. Wofford, H.R. DeSelm, and A.M. Evans. 1993. Atlas of Tennessee Vascular Plants, vol. 1. Austin Peay State University Miscellaneous Publication No. 9. The Center for Field Biology, Austin Peay State University, Clarksville, Tennessee, U.S.A. 118 pp.</reference>
    <reference> Church, G.L. 1949. Cytotaxonomic study of Glyceria and Puccinellia. Amer. J. Bot. 36:155-165</reference>
    <reference> Conert, H.J. 1992. Glyceria. Pp. 440-457 in G. Hegi. Illustrierte Flora von Mitteleuropa, ed. 3. Band I, Teil 3, Lieferung 6 (pp. 401—480). Verlag Paul Parey, Berlin and Hamburg, Germany</reference>
    <reference> Dore, W.G. and J. McNeill. 1980. Grasses of Ontario. Research Branch, Agriculture Canada Monograph No. 26. Canadian Government Publishing Centre, Hull, Quebec, Canada. 568 pp.</reference>
    <reference> Hitchcock, C.L., A. Cronquist, and M. Ownbey. 1969. Vascular Plants of the Pacific Northwest. Part 1: Vascular Cryptogams, Gymnosperms, and Monocotyledons. University of Washington Press, Seattle, Washington, U.S.A. 914 pp.</reference>
    <reference> Komarov, V.L. 1963. Genus 176. Glyceria R. Br. Pp. 356-365 in R.Yu. Rozhevits [R.J. Roshevitz] and B.K. Shishkin [Schischkin] (eds.). Flora of the U.S.S.R., vol. 2, trans. N. Landau (series ed. V.L. Komarov). Published for the National Science Foundation and the Smithsonian Institution, Washington, D.C. by the Israel Program for Scientific Translations, Jerusalem, Israel. 622 pp. [English translation of Flora SSSR, vol. II. 1934. Botanicheskii Institut Im. V.L. Komarova, Akademiya Nauk, Leningrad, Russia. 778 pp.]</reference>
    <reference> Koyama, T. 1987. Grasses of Japan and Its Neighboring Regions: An Identification Manual. Kodansha, Ltd., Tokyo, Japan. 370 pp.</reference>
    <reference> Scoggan, H. 1978. Flora of Canada, part 2: Pteridophyta, Gymnosperms, Monocotyledoneae. National Museum of Natural Sciences Publications in Botany No. T2. National Museums of Canada, Ottawa, Ontario, Canada. 545 pp.</reference>
    <reference> Voss, E.G. 1972. Michigan Flora: A Guide to the Identification and Occurrence of the Native and Naturalized Seed-Plants of the State, part 1. University of Michigan, Ann Arbor, Michigan, U.S.A. 488 pp.</reference>
    <reference> Whipple, I.G., B.S. Bushman, and M.E. Barkworth. [in press]. Glyceria in North America.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Spikelets laterally compressed, lengths 1-4 times widths, oval in side view; paleal keels not winged (sects. Hydropoa and Striatae).</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Upper glumes 2.5-5 mm long, longer than wide.</description>
      <next_statement_id>3.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Blades 3-7 mm wide; culms 2.5-4 mm thick, 60-90 cm tall; anthers 0.7-1.2 mm long</description>
      <determination>2 Glyceria alnasteretum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Blades 6-20 mm wide; culms 6-12 mm thick, 60-250 cm tall; anthers (1)1.2-2 mm long</description>
      <determination>3 Glyceria maxima</determination>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Upper glumes 0.6-3.7 mm long, if longer than 3 mm, then shorter than wide.</description>
      <next_statement_id>4.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4.</statement_id>
      <description type="morphology">Panicles ovoid to linear; panicle branches appressed to strongly ascending; ligules of the upper leaves 0.5-0.9 mm long.</description>
      <next_statement_id>5.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5.</statement_id>
      <description type="morphology">Panicles 5-15 cm long, 2.5-6 cm wide, ovoid, erect</description>
      <determination>4 Glyceria obtusa</determination>
    </key_statement>
    <key_statement>
      <statement_id>5.</statement_id>
      <description type="morphology">Panicles 15-25 cm long, 0.8-1.5 cm wide, linear, nodding</description>
      <determination>5 Glyceria melicaria</determination>
    </key_statement>
    <key_statement>
      <statement_id>4.</statement_id>
      <description type="morphology">Panicles pyramidal; panicle branches strongly divergent or drooping; ligules of the upper leaves 1-7 mm long.</description>
      <next_statement_id>6.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6.</statement_id>
      <description type="morphology">Lemma apices almost flat; anthers 3; veins of 1 or both glumes in each spikelet usually extending to the apices</description>
      <determination>1 Glyceria grandis</determination>
    </key_statement>
    <key_statement>
      <statement_id>6.</statement_id>
      <description type="morphology">Lemma apices prow-shaped; anthers 2; veins of both glumes terminating below the apices.</description>
      <next_statement_id>7.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7.</statement_id>
      <description type="morphology">Glumes tapering from below midlength to the narrowly acute (&lt; 45°) apices; lemma lengths more than twice widths</description>
      <determination>6 Glyceria nubigena</determination>
    </key_statement>
    <key_statement>
      <statement_id>7.</statement_id>
      <description type="morphology">Glumes narrowing from midlength or above to the acute (&gt; 45°) or rounded apices; lemma lengths less than twice widths.</description>
      <next_statement_id>8.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8.</statement_id>
      <description type="morphology">Spikelets (2.5)3-5 mm wide; lemma veins evident but not raised distally; palea lengths 1.5-1.8 times widths</description>
      <determination>10 Glyceria canadensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>8.</statement_id>
      <description type="morphology">Spikelets 1.2-2.9 mm wide; lemma veins distinctly raised throughout; palea lengths 1.5-3.5 times widths.</description>
      <next_statement_id>9.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9.</statement_id>
      <description type="morphology">Lemmas 2.5-3.5 mm long; glume lengths about 3 times widths, glume apices broadly acute; lower glumes 1.5-2 mm long; upper glumes 2-2.6 mm long</description>
      <determination>7 Glyceria pulchella</determination>
    </key_statement>
    <key_statement>
      <statement_id>9.</statement_id>
      <description type="morphology">Lemmas 1.2-2.2 mm long; glume lengths up to twice widths, glume apices rounded or acute; lower glumes 0.5-1.5 mm long; upper glumes 0.6-1.5 mm long.</description>
      <next_statement_id>10.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10.</statement_id>
      <description type="morphology">Blades 2-6 mm wide; anthers 0.2-0.6 mm long; culms 1.5-3.5 mm thick</description>
      <determination>8 Glyceria striata</determination>
    </key_statement>
    <key_statement>
      <statement_id>10.</statement_id>
      <description type="morphology">Blades 6-15 mm wide; anthers 0.5-0.8 mm long; culms 2.5-8 mm thick</description>
      <determination>9 Glyceria elata</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Spikelets cylindrical and terete, except at anthesis when slightly laterally compressed, lengths more than 5 times widths, rectangular in side view; paleal keels usually winged distally (sect. Glyceria).</description>
      <next_statement_id>11.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11.</statement_id>
      <description type="morphology">Lemmas tapering from near midlength to the acuminate or narrowly acute apices; paleas exceeding the lemmas by 0.7-3 mm; palea apices often appearing bifid, the teeth 0.4-1 mm long</description>
      <determination>13 Glyceria acutiflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>11.</statement_id>
      <description type="morphology">Lemmas not tapered or tapering only in the distal 1/4, apices truncate, rounded, or acute; paleas shorter or to 1(1.5) mm longer than the lemmas; palea apices not or shortly bifid, the teeth to 0.5 mm long.</description>
      <next_statement_id>12.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12.</statement_id>
      <description type="morphology">Lemma apices with 1 strongly developed lobe on 1 or both sides, entire to crenulate between the lobes; blades 3-12 cm long; primary panicle branches 1.5-9.5 cm long</description>
      <determination>17 Glyceria declinata</determination>
    </key_statement>
    <key_statement>
      <statement_id>12.</statement_id>
      <description type="morphology">Lemma apices not or more or less evenly lobed; blades 5-30 cm long; primary panicle branches 3-18 cm long.</description>
      <next_statement_id>13.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13.</statement_id>
      <description type="morphology">Lemmas 5-8 mm long.</description>
      <next_statement_id>14.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14.</statement_id>
      <description type="morphology">Anthers 0.6-1.6 mm long; lemma apices usually slightly lobed or irregularly crenate</description>
      <determination>15 Glyceria ×occidentalis</determination>
    </key_statement>
    <key_statement>
      <statement_id>14.</statement_id>
      <description type="morphology">Anthers 1.5-3 mm long; lemma apices usually entire</description>
      <determination>16 Glyceria fluitans</determination>
    </key_statement>
    <key_statement>
      <statement_id>13.</statement_id>
      <description type="morphology">Lemmas 2.4-5 mm long.</description>
      <next_statement_id>15.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>15.</statement_id>
      <description type="morphology">Lemmas usually smooth between the veins, if scabridulous the prickles between the veins smaller than those over the veins.</description>
      <next_statement_id>16.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>16.</statement_id>
      <description type="morphology">Lemmas usually acute, sometimes obtuse, entire or almost so; adaxial surfaces of the midcauline blades usually densely papillose, glabrous</description>
      <determination>11 Glyceria borealis</determination>
    </key_statement>
    <key_statement>
      <statement_id>16.</statement_id>
      <description type="morphology">Lemmas truncate to obtuse, crenate; adaxial surfaces of the midcauline blades rarely densely papillose, sometimes sparsely hairy.</description>
      <next_statement_id>17.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>17.</statement_id>
      <description type="morphology">Culms 73-182 cm tall; pedicels 0.7-1.7 mm</description>
      <determination>12 Glyceria septentrionalis</determination>
    </key_statement>
    <key_statement>
      <statement_id>17.</statement_id>
      <description type="morphology">Culms 25-80 cm tall; pedicels 1-6 mm</description>
      <determination>18 Glyceria notata</determination>
    </key_statement>
    <key_statement>
      <statement_id>15.</statement_id>
      <description type="morphology">Lemmas scabridulous or hispidulous   between the veins,  the prickles between the veins similar in size to those over the veins.</description>
      <next_statement_id>18.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>18.</statement_id>
      <description type="morphology">Lemma apices acute.</description>
      <next_statement_id>19.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>19.</statement_id>
      <description type="morphology">Lemmas 2.4-4.8 mm long; pedicels 0.7-1.7 mm long; plants from east of the Rocky Mountains</description>
      <determination>12 Glyceria septentrionalis</determination>
    </key_statement>
    <key_statement>
      <statement_id>19.</statement_id>
      <description type="morphology">Lemmas 4.5-5.9 mm long; pedicels 1.5-8 mm long; plants from west of the Rocky Mountains</description>
      <determination>15 Glyceria ×occidentalis</determination>
    </key_statement>
    <key_statement>
      <statement_id>18.</statement_id>
      <description type="morphology">Lemma apices truncate to obtuse.</description>
      <next_statement_id>20.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>20.</statement_id>
      <description type="morphology">Pedicels 0.7-1.7 mm long; anthers 0.5-1.8 mm long; plants from east of the Rocky Mountains</description>
      <determination>12 Glyceria septentrionalis</determination>
    </key_statement>
    <key_statement>
      <statement_id>20.</statement_id>
      <description type="morphology">Pedicels 2-5 mm long; anthers 0.3-0.9 mm long; plants from British Columbia and the Pacific states</description>
      <determination>14 Glyceria leptostachya</determination>
    </key_statement>
  </key>
  <description type="distribution">N.C.;Conn.;N.J.;N.Y.;Wash.;W.Va.;Del.;D.C;Wis.;Ariz.;N.Mex.;Pacific Islands (Hawaii);Mass.;Maine;N.H.;R.I.;Vt.;Fla.;Wyo.;Tex.;La.;Iowa;Minn.;Oreg.;Tenn.;S.C.;Pa.;Ill.;Ind.;Md.;Mich.;Miss.;Mont.;N.Dak.;Nebr.;S.Dak.;Utah;Okla.;Calif.;Nev.;Va.;Colo.;Alta.;B.C.;Man.;N.B.;Nfld. and Labr.;N.S.;N.W.T.;Ont.;P.E.I.;Que.;Sask.;Yukon;Ala.;Kans.;Ark.;Ga.;Idaho;Ohio;Mo.;Alaska;Ky.</description>
</bio:treatment>
