<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <other_info_on_meta type="treatment_page">326</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" date="unknown" authority="Barnhart">Poaceae</taxon_name>
    <taxon_name rank="subfamily" date="unknown" authority="Benth.">POOIDEAE</taxon_name>
    <taxon_name rank="tribe" date="unknown" authority="Dumort.">TRITICEAE</taxon_name>
    <taxon_name rank="genus" date="unknown" authority="L.">ELYMUS</taxon_name>
    <taxon_name rank="species" date="unknown" authority="(Scribn. &amp; Merr.) Á.Löve">alaskanus</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe triticeae;genus elymus;species alaskanus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Roegneria</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">villosa</taxon_name>
    <taxon_hierarchy>genus roegneria;species villosa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Roegneria</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">borealis</taxon_name>
    <taxon_hierarchy>genus roegneria;species borealis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Elymus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">alaskanus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subspecies">borealis</taxon_name>
    <taxon_hierarchy>genus elymus;species alaskanus;subspecies borealis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Agropyron</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">alaskanum</taxon_name>
    <taxon_hierarchy>genus agropyron;species alaskanum</taxon_hierarchy>
  </taxon_identification>
  <number>26</number>
  <description type="morphology">Plants cespitose or weakly rhizomatous. Culms 20-90 cm, sometimes decumbent at the base, ascending to erect above; nodes usually pubescent, sometimes glabrous. Leaves sometimes basally concentrated; sheaths smooth or scabrous, glabrous or pilose; auricles absent or to 0.5 mm; ligules 0.2-1 mm, erose, ciliolate; blades 3-7 mm wide, flat, both surfaces smooth, scabrous, or pubescent. Spikes 3.5-14 cm long, 0.5-0.8 cm wide, erect or nodding distally, usually with 1 spikelet per node, occasionally with 2 at the lower nodes; internodes 3-10 mm long, 0.5-0.8 mm wide, mostly glabrous and smooth, edges scabrous or ciliate. Spikelets 9-15(20) mm, 2-5 times longer than the internodes, appressed, with 3-6 florets, rachillas hispidulous; disarticulation above the glumes, beneath each floret. Glumes 4-8 mm long, (1.2)1.5-2 mm wide, 1/3-2/3 as long as the adjacent lemmas, oblanceolate to obovate, flat, usually purplish, glabrous or hairy, hairs 0.3-0.5 mm, margins unequal, the widest margin 0.4-1 mm wide, both margins widest above the middle, apices unawned or awned, awns to 1 mm; lemmas 7-11 mm, glabrous or hairy, sometimes scabridulous, sometimes more densely hairy distally, hairs 0.2-0.6 mm, all alike, apices unawned or awned, awns to 7 mm, straight; paleas subequal to the lemmas, keels straight below the apices; anthers 1-2 mm. 2n = 28.</description>
  <discussion>Elymus alaskanus extends across the high arctic of North America to extreme eastern Russia. This treatment interprets E. alaskanus as having relatively short glumes, in accordance with its treatment by Hulten (1968). Large specimens resemble E. macrourus (see previous), but differ in the shape of their glumes and in their wider glume margins. Elymus alaskanus differs from E. trachycaulus (p. 321) in its greater cold tolerance and the distal widening of its glume margins. There is some intergradation, particularly with E. violaceus (p. 324) and E. trachycaulus, but these species have longer glumes. Moreover, in western North America, E. violaceus is restricted to rocky habitats at or above treeline, whereas E. alaskanus is often associated with valleys and flat areas. Reports of its extending to New Mexico are based on the inclusion of high-elevation forms of E. trachycaulus.</discussion>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Glumes glabrous, scabrous or sparsely hairy, hairs to about 0.2 mm long; lemmas glabrous or with hairs to about 0.2 mm long</description>
      <determination>Elymus alaskanus subsp. alaskanus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Glumes   and  lemmas  densely  hairy,  hairs 0.2-0.5 mm long</description>
      <determination>Elymus alaskanus subsp. hyperarcticus</determination>
    </key_statement>
  </key>
  <description type="distribution">Alta.;B.C.;Greenland;Nfld. and Labr. (Nfld.);Man.;N.S.;N.W.T.;Nunavut;Ont.;Que.;Yukon;Mont.;Wyo.;Colo.;N.Mex.;Wash.;Idaho;Mich.;Alaska;Oreg.;Nev.</description>
</bio:treatment>
