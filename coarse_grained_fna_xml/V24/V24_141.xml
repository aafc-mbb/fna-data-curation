<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <author>Mary E. Barkworth;</author>
      <date>NA</date>
    </source>
    <other_info_on_meta type="treatment_page">109</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" date="unknown" authority="Barnhart">Poaceae</taxon_name>
    <taxon_name rank="subfamily" date="unknown" authority="Benth.">POOIDEAE</taxon_name>
    <taxon_name rank="tribe" date="unknown" authority="Dumort.">STIPEAE</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe stipeae</taxon_hierarchy>
  </taxon_identification>
  <number>10</number>
  <description type="morphology">Plants usually perennial; usually tightly to loosely cespitose, sometimes rhizomatous. Culms annual or perennial, not woody, branches 1 to many at the upper nodes. Leaves basally concentrated to evenly distributed; sheaths open, margins not fused, sometimes ciliate distally, basal sheaths sometimes concealing axillary panicles (cleistogenes), sometimes wider than the blade; collars sometimes with tufts of hair at the sides extending to the top of the sheaths; auricles absent; ligules scarious, often ciliate, cilia usually shorter than the base, ligules of the lower and upper cauline leaves sometimes differing in size and vestiture; pseudopetioles absent; blades linear to narrowly lanceolate, venation parallel, cross venation not evident, cross sections non-Kranz, without arm or fusoid cells; epidermes of adaxial surfaces sometimes with unicellular microhairs, cells not papillate. Inflorescences usually terminal panicles, occasionally reduced to racemes in depauperate plants, sometimes 2-3 panicles developing from the highest cauline node. Spikelets usually with 1 floret, sometimes with 2-6 florets, laterally compressed to terete; rachillas not prolonged beyond the base of the floret in spikelets with 1 floret, prolonged beyond the base of the distal floret in spikelets with 2-6 florets, prolongation hairy, hairs 2-3 mm; disarticulation above the glumes and beneath the florets. Glumes usually exceeding the floret(s), always longer than 1/4 the length of the adjacent floret, 1-10-veined, narrowly lanceolate to ovate, hyaline or membranous, flexible; florets usually terete, sometimes laterally or dorsally compressed; calluses usually well-developed, rounded or blunt to sharply pointed, often antrorsely strigose; lemmas lanceolate, rectangular, or ovate, membranous to coriaceous or indurate, 3-5-veined, veins inconspicuous, apices entire, bilobed, or bifid, awned, lemma-awn junction usually conspicuous, awns 0.3-30 cm, not branched, usually terminal and centric or eccentric, sometimes subterminal, caducous to persistent, not or once- to twice-geniculate, if geniculate, proximal segment(s) twisted, distal segment straight, flexuous, or curled, not or scarcely twisted; lodicules 2 or 3; anthers 1 or 3, sometimes differing in length within a floret; ovaries glabrous throughout or pubescent distally; styles 2(3-4)-branched. Caryopses ovoid to fusiform, not beaked, pericarp thin; hila linear; embryos less than 1/3 the length of the caryopses. x = 7, 8, 10, 11, 12.</description>
  <discussion>The tribe Stipeae includes about 15 genera and approximately 500 species. It grows in Africa, Australia, South and North America, and Eurasia. In Australia, South America, and Asia, it is often the dominant grass tribe over substantial areas. It is not present in southern India, and is represented by only one native species in southern Africa. Most species grow in arid or seasonally arid, temperate regions.</discussion>
  <discussion>Morphological considerations have led to the Stipeae being placed in three different subfamilies (Pooideae, Bambusoideae, and Arundinoideae) in the past, and even to recognition as a subfamily. Molecular data support its treatment as an early diverging lineage within the Pooideae (Soreng and Davis 1998; Grass Phylogeny Working Group 2001) that is more closely related to the Meliceae than the core pooid tribes.</discussion>
  <discussion>Decker (1964) suggested including Ampelodesmos in the Stipeae on the basis of the cross sectional anatomy of its leaf blades. His suggestion is supported, not always strongly, by molecular studies (Soreng and Davis 1998; Grass Phylogeny Working Group 2001; Jacobs et al. 2006). The usual alternative is to treat Ampelodesmos as the only genus of a closely related, monospecific tribe, the Ampelodesmeae (Conert) Tutin, because it is so distinct from other members of the Stipeae, being, for example, the only member of the tribe with more than 1 floret in its spikelets and rachillas that are prolonged beyond the base of the terminal floret in a spikelet.</discussion>
  <discussion>The lowest chromosome number known in the Stipeae is 2n =18 (Prokudin et al. 1977), suggesting that all members of the tribe are ancient polyploids. The wide range of base numbers listed is based on numbers for the various genera. The primary basic chromosome number for the tribe is probably 5 or 6, with higher numbers reflecting ancient euploidy.</discussion>
  <references>
    <reference>Barkworth, M.E. 1993. North American Stipeae (Gramineae): Taxonomic changes and other comments. Phytologia 74:1-25</reference>
    <reference> Barkworth, M.E. and J. Everett. 1987. Evolution in the Stipeae: Identification and relationships of its monophyletic taxa. Pp. 251-264 in T.R. Soderstrom, K.W. Hilu, C.S. Campbell, and M.E. Barkworth (eds.). Grass Systematics and Evolution. Smithsonian Institution Press, Washington, D.C., U.S.A. 473 pp.</reference>
    <reference> Decker, H.F. 1964. Affinities of the grass genus Ampelodesmos. Brittonia 16:76-79</reference>
    <reference> Grass Phylogeny Working Group. 2001. Phylogeny and subfamilial classification of the grasses (Poaceae). Ann. Missouri Bot. Gard. 88:373-457</reference>
    <reference> Hsiao, C, S.W.L. Jacobs, N.J. Chatterton and K.H. Asay. 1999. A molecular phylogeny of the grass family (Poaceae) based on the sequences of nuclear ribosomal DNA (ITS). Austral. Syst. Bot. 11:667-688</reference>
    <reference> Jacobs, S.W.L., R. Bayer, J. Everett, M.O. Arriaga, M.E. Barkworth, A. Sabin-Badereau, M.A. Torres, F. Vazquez, and N. Bagnall. 2006. Systematics of the tribe Stipeae using molecular data. Aliso 23:349-361</reference>
    <reference> Jacobs, S.W.L. and J. Everett. 1996. Austrostipa, a new genus, and new names for Australasian species formerly included in Stipa (Gramineae). Telopea 6:579-595</reference>
    <reference> Johnson, B.L. 1945. Cytotaxonomic studies in Oryzopsis. Bot. Gaz. 107:1-32</reference>
    <reference> Prokudin, Y.N., A.G. Vovk, O.A. Petrova, E.D. Ermolenko, and Y.V. Vernichenklo. 1977. Zlaki Ukrainy. Naukava Dumka, Kiev, Russia. 517 pp.</reference>
    <reference> Soreng, R.J. and J.I. Davis. 1998. Phylogenetics and character evolution in the grass family (Poaceae): Simultaneous analysis of morphological and chloroplast DNA restriction site character sets. Bot. Rev. (Lancaster) 64:1-85.</reference>
  </references>
  <key>
    <discussion>The hybrid genus ×Achnella is not included in the key; it is treated on p. 169.</discussion>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Spikelets with 2-6 florets</description>
      <determination>10.01 Ampelodesmos</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Spikelets with 1 floret.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Paleas sulcate, longer than the lemmas; lemma margins involute, fitting into the paleal groove; lemma apices not lobed</description>
      <determination>10.09 Piptochaetium</determination>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Paleas flat, from shorter than to longer than the lemmas; lemma margins convolute or not overlapping; lemma apices often lobed or bifid.</description>
      <next_statement_id>3.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Prophylls exceeding the leaf sheaths; plants cultivated as ornamentals.</description>
      <next_statement_id>4.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4.</statement_id>
      <description type="morphology">Panicles contracted; lemma awns once-geniculate</description>
      <determination>10.05 Macrochloa</determination>
    </key_statement>
    <key_statement>
      <statement_id>4.</statement_id>
      <description type="morphology">Panicles open; lemma awns twice-geniculate</description>
      <determination>10.06 Celtica</determination>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Prophylls concealed by the leaf sheaths; plants native, introduced, sometimes cultivated as ornamentals.</description>
      <next_statement_id>5.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5.</statement_id>
      <description type="morphology">Flag leaf blades up to 12 mm long; basal leaves overwintering</description>
      <determination>10.10 Oryzopsis</determination>
    </key_statement>
    <key_statement>
      <statement_id>5.</statement_id>
      <description type="morphology">Flag leaf blades more than 10 mm long; basal leaves not overwintering.</description>
      <next_statement_id>6.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6.</statement_id>
      <description type="morphology">Plants with multiple stiff branches from the upper nodes; pedicels sometimes plumose; species cultivated as ornamentals in the Flora region</description>
      <determination>10.15 Austrostipa</determination>
    </key_statement>
    <key_statement>
      <statement_id>6.</statement_id>
      <description type="morphology">Plants not branching at the upper nodes, or with a few, flexible branches; pedicels never plumose;  species native,  established introductions,  or cultivated as ornamentals.</description>
      <next_statement_id>7.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7.</statement_id>
      <description type="morphology">Apices of the leaf blades sharp and stiff; caryopses obovoid, often with 3 smooth ribs at maturity; cleistogenes usually present</description>
      <determination>10.14 Amelichloa</determination>
    </key_statement>
    <key_statement>
      <statement_id>7.</statement_id>
      <description type="morphology">Apices of the leaf blades acute to acuminate, never both sharp and stiff; caryopses fusiform, ovoid or obovoid, without ribs; cleistogenes sometimes present.</description>
      <next_statement_id>8.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8.</statement_id>
      <description type="morphology">Lemma margins strongly overlapping their whole length at maturity, lemma bodies usually rough throughout, apices not lobed; paleas 1/4 - 1/2 the length of the lemmas, without veins, glabrous</description>
      <determination>10.12 Nassella</determination>
    </key_statement>
    <key_statement>
      <statement_id>8.</statement_id>
      <description type="morphology">Lemma margins usually not or only slightly overlapping for some or all of their length at maturity, strongly overlapping in some species with smooth lemmas, lemma bodies usually smooth on the lower portion, apices often 1-2-lobed; paleas from 1/3 as long as to equaling or slightly exceeding the lemmas, 2-veined at least on the lower portion, usually with hairs or both lemmas and paleas glabrous.</description>
      <next_statement_id>9.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9.</statement_id>
      <description type="morphology">Calluses 1.5-6 mm long, sharply pointed; plants perennial or annual, if perennial, awns 65-500 mm long, if annual, awns 50-100 mm long; panicle branches straight.</description>
      <next_statement_id>10.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10.</statement_id>
      <description type="morphology">Lower ligules densely hairy, upper ligules less densely hairy or glabrous; plants perennial</description>
      <determination>10.13 Jarava</determination>
    </key_statement>
    <key_statement>
      <statement_id>10.</statement_id>
      <description type="morphology">Ligules glabrous or inconspicuously pubescent, lower and upper ligules alike in vestiture; plants perennial or annual.</description>
      <next_statement_id>11.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11.</statement_id>
      <description type="morphology">Plants perennial; florets 7-25 mm long; awns scabrous or pilose on the first 2 segments, the terminal segment scabrous, or if pilose, the hairs 1-3 mm long</description>
      <determination>10.08 Hesperostipa</determination>
    </key_statement>
    <key_statement>
      <statement_id>11.</statement_id>
      <description type="morphology">Plants annual or perennial, if annual, the florets 4-7 mm long and the awns not plumose, if perennial, the florets 18-27 mm long and the awns plumose on the terminal segment, the hairs 5-6 mm long</description>
      <determination>10.07 Stipa</determination>
    </key_statement>
    <key_statement>
      <statement_id>9.</statement_id>
      <description type="morphology">Calluses 0.1-2 mm long, blunt to sharply pointed; plants perennial; awns 1-70 mm; panicle branches straight or flexuous.</description>
      <next_statement_id>12.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12.</statement_id>
      <description type="morphology">Florets usually dorsally compressed at maturity, sometimes terete; paleas as long as or longer than the lemmas and similar in texture and pubescence; lemma margins separate for their whole length at maturity</description>
      <determination>10.04 Piptatherum</determination>
    </key_statement>
    <key_statement>
      <statement_id>12.</statement_id>
      <description type="morphology">Florets terete or laterally compressed at maturity; paleas often shorter than the lemmas, sometimes less pubescent, sometimes as long as the lemmas and similar in texture and pubescence; lemma margins often overlapping for part or all of their length at maturity.</description>
      <next_statement_id>13.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13.</statement_id>
      <description type="morphology">Glumes without evident venation, glume apices rounded to acute; plants subalpine to alpine, sometimes growing in bogs</description>
      <determination>10.03 Ptilagrostis</determination>
    </key_statement>
    <key_statement>
      <statement_id>13.</statement_id>
      <description type="morphology">Glumes with 1-3(5) evident veins or the glume apices attenuate; plants growing from near sea level to subalpine or alpine habitats, not growing in bogs.</description>
      <next_statement_id>14.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14.</statement_id>
      <description type="morphology">Lemma bodies with evenly distributed hairs of similar length or completely glabrous, sometimes with longer hairs around the base of the awn; basal segment of the awns sometimes with hairs up to 2 mm long</description>
      <determination>10.02 Achnatherum</determination>
    </key_statement>
    <key_statement>
      <statement_id>14.</statement_id>
      <description type="morphology">Lemma bodies with hairs to 1 mm long over most of their length, with strongly divergent hairs 3-8 mm long on the distal 1/4, or the basal segment of the awns with hairs 3-8 mm long</description>
      <determination>10.13 Jarava</determination>
    </key_statement>
  </key>
</bio:treatment>
