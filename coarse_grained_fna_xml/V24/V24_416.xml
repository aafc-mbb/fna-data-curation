<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <other_info_on_meta type="treatment_page">295</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" date="unknown" authority="Barnhart">Poaceae</taxon_name>
    <taxon_name rank="subfamily" date="unknown" authority="Benth.">POOIDEAE</taxon_name>
    <taxon_name rank="tribe" date="unknown" authority="Dumort.">TRITICEAE</taxon_name>
    <taxon_name rank="genus" date="unknown" authority="L.">ELYMUS</taxon_name>
    <taxon_name rank="species" date="unknown" authority="R. Brooks &amp; J.J.N. Campb.">macgregorii</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe triticeae;genus elymus;species macgregorii</taxon_hierarchy>
  </taxon_identification>
  <number>1</number>
  <other_name type="common_name">Early wildrye</other_name>
  <description type="morphology">Plants cespitose, not rhizomatous, usually glaucous. Culms 40-120 cm, erect or slightly decumbent; nodes 4-8, mostly exposed, glabrous. Leaves even¬ly distributed; sheaths usually glabrous, rarely villous; auricles 2-3 mm, usually purplish black when fresh, sometimes light brown; ligules shorter than 1 mm; blades 7-15 mm wide, lax, dark glossy green under the glaucous bloom, adaxial surfaces usually glabrous, occasionally villous. Spikes 4-12 cm long, (1.7)2.2-3(4)4 cm wide, erect, exserted, with (6)9-16(20) nodes and 2 spikelets at all or most nodes, sometimes with 3 at some nodes; internodes 4-7 mm long, about 0.3 mm thick and 2-angled at the thinnest sections, usually glabrous or scabridulous beneath the spikelets. Spikelets 10-15 mm, strongly divergent, glaucous, maturing to pale yellowish brown, with (2)3-4 florets, lowest florets functional; disarticulation below the glumes and each floret, the lowest floret often falling with the glumes. Glumes subequal, entire, the basal 1-3 mm terete or subterete, indurate, without evident venation, moderately bowed out, glume bodies 8-16 mm long, 1-1.8 mm wide, linear-lanceolate, widening or parallel-sided above the base, (2)4-5(8)-veined, usually glabrous, occasionally hirsute, sometimes scabrous, margins firm, awns (10)15-20(25) mm, straight except the awns of the lowest spikelets occasionally contorted; lemmas 6-12 mm, usually glabrous, sometimes scabrous, occasionally villous, awns (15)20-30 mm, straight; paleas 6-10 mm, apices obtuse; anthers 2-4 mm. Anthesis usually mid-May to mid-June. 2n = 28.</description>
  <discussion>Elymus macgregorii grows in moist, deep, alluvial or residual, calcareous or other base-rich soils in woods and thickets, mostly east of the 100th Meridian in the contiguous United States. It used to be confused with E. glabriflorus (p. 296) or E. virginicus (p. 298), but it reaches anthesis about a month earlier than sympatric populations of these species. In most of its range, E. macgregorii has purplish black auricles; light brown auricles may be locally abundant, particularly in populations at the limits of its range.</discussion>
  <discussion>Elymus macregorii hybridizes with several species, but especially E. virginicus and E. hystrix (p. 316) (Campbell 2000). Western plants often have smaller, more condensed spikes and distinctly villous leaves, suggesting a transition to E. virginicus var. jejunus (p. 300). Transitions to E. virginicus var. jejunus can also be recognized to the north, where the dates of anthesis are delayed, but even in Maine, E. macgregorii reaches anthesis about 10 days earlier than E. virginicus (Campbell and Haines 2002). Plants with villous lemmas grow at scattered locations; they have not been reported in distinct habitats, nor in large enough populations to warrant taxonomic recognition.</discussion>
  <description type="distribution">Ont.;Ky.;Del.;Ala.;Ark.;Conn.;D.C.;Fla.;Ga.;Iowa;Ill.;Ind.;Kans.;La.;Mass.;Md.;Maine;Mo.;Miss.;N.C.;N.Dak.;Nebr.;N.H.;N.S.;N.Y.;Ohio;Okla.;Pa.;R.I.;S.Dak.;Tenn.;Tex.;Va.;Vt.;Wis.;W.Va.</description>
</bio:treatment>
