<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <author>Mary E. Barkworth; Edward E. Terrell;</author>
      <date>NA</date>
    </source>
    <other_info_on_meta type="treatment_page">37</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" date="unknown" authority="Barnhart">Poaceae</taxon_name>
    <taxon_name rank="subfamily" date="unknown" authority="Link">EHRHARTOIDEAE</taxon_name>
    <taxon_name rank="tribe" date="unknown" authority="Dumort.">ORYZEAE</taxon_name>
    <taxon_name rank="genus" date="unknown" authority="L.">ORYZA</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily ehrhartoideae;tribe oryzeae;genus oryza</taxon_hierarchy>
  </taxon_identification>
  <number>5.01</number>
  <description type="morphology">Plants annual or perennial; usually aquatic, rooted and emergent or floating, sometimes terrestrial; rhizomatous and/or cespitose; synoecious. Culms to 3.3(5) m, erect, decumbent, or prostrate, sometimes rooting at the lower nodes, aerenchymatous, emergent or immersed, branched or unbranched. Leaves cauline and basal; sheaths open, lower sheaths often slightly inflated, upper sheaths not inflated; auricles usually present; ligules membranous, often veined; pseudopetioles absent; blades linear to narrowly lanceolate, flat, margins smooth or scabridulous. Inflorescences terminal panicles; disarticulation above the glumes, beneath the sterile florets in wild taxa, spikelets of cultivated taxa not disarticulating. Spikelets bisexual, laterally compressed, with 3 florets, lower 2 florets sterile, terminal floret functional. Glumes absent or reduced to lobes at the pedicel apices; sterile florets glumelike, 1.2-10 mm, 1/8 - 1/2 (9/10) as long as the spikelets, linear or subulate to narrowly ovate, coriaceous, 1-veined, acute to acuminate; functional florets: calluses usually inconspicuous and flat to rounded, sometimes conspicuous and stipelike, glabrous; lemmas coriaceous or indurate, with vertical rows of tubercles separated by longitudinal furrows, 5-veined, keeled, margins clasping the margins of the paleas, apices obtuse or acute to acuminate, awned or unawned; paleas with surfaces similar to the lemmas, 3-veined, unawned; lodicules 2; anthers 6; styles 2, bases fused or not, stigmas laterally exserted, plumose. Caryopses laterally compressed; embryos usually ¼ - 1/3 as long as the caryopses; hila linear, x = 12.</description>
  <discussion>Oryza is a tropical and subtropical genus of about 20 species that grow in shallow water, swamps, and marshes in seasonally inundated areas, or along streams, rivers, or lake edges. Oryza sativa (rice) is one of the three most economically valuable cereals, and constitutes a major portion of the diet for half of the world's population. In the Flora region, O. sativa is cultivated and several weedy forms have become established. These are thought to be derived from introgression between O. sativa and O. rufipogon and O. punctata. The latter two species and O. longistaminata are included here because of the threat they pose to cultivated rice.</discussion>
  <discussion>Spikelets of Oryza have sometimes been interpreted as comprising one functional and two sterile florets with two highly reduced glumes (Duistermat 1987), sometimes as comprising a single floret, subtended by two glumes borne on a bilobed pedicel (Terrell et al. 2001). Molecular developmental studies (Komatsu et al. 2003) show that the former interpretation is correct.</discussion>
  <references>
    <reference>Duistermaat, H. 1987. A revision of Oryza (Gramineae) in Malesia and Australia. Blumea 32:157-193</reference>
    <reference> Komatsu, M., A. Chujo, Y. Nagato, K. Shimamoto, and J. Kyozuka. 2003. FRIZZY PANICLE is required to prevent the formation of axillary meristems and to establish floral meristem identity in rice spikelets. Development 130:3841-3850</reference>
    <reference> Launert, E. 1971. Oryza L. Pp. 31-36 in A. Fernande, E. Launert, and H. Wild (eds.). Flora Zambesiaca, vol. 101. Crown Agents for Oversea Governments and Administrations, London, England. 152 pp.</reference>
    <reference> Londo, J.P., Y.-C. Chiang, K.-H. Hung, T.-Y. Chiang, and B.A. Schaal. 2006. Phylogeography of Asian wild rice, Oryza rufipogon, reveals multiple independent domestications of cultivated rice, Oryza sativa. Proc. Natl. Acad. Sci. U.S.A. [PNAS] 103(25):9578-9583</reference>
    <reference> Lu, B.-R., E.B. Naredo, A.B. Juliano, and M.T. Jackson. 2000. Preliminary studies on taxonomy and biosystematics of the AA genome Oryza species (Poaceae). Pp. 51-58 in S.W.L. Jacobs and J. Everett (eds.). Grasses: Systematics and Evolution. CSIRO Publishing, Collingwood, Victoria, Australia. 406 pp.</reference>
    <reference> Terrell, E.E., P.M. Peterson and W.P. Wergin. 2001. Epidermal features and spikelet micromorphology in Oryza and related genera (Poaceae: Oryzeae). Smithsonian Contr. Bot. 91. 50 pp.</reference>
    <reference> Vaughan, D.A. 1989. The Genus Oryza L.-Current Status of Taxonomy. International Rice Research Institute Research Paper Series 138. International Rice Research Institute, Los Bafios, Laguna, Philippines. 21 pp.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Ligules truncate to rounded, 1.5-10 mm long; sterile florets 1.2-2 mm long; disarticulation scar centric or slightly eccentric</description>
      <determination>3 Oryza punctata</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Ligules acute, 4-45 mm long; sterile florets 1.3-10 mm long; disarticulation scar lateral.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Anthers  1-2.5 mm long; spikelets persistent; lemmas usually unawned, plants not rhizomatous; auricles absent or to 5 mm long; blades 5-20 mm wide</description>
      <determination>4 Oryza sativa</determination>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Anthers 3.5-7.4 mm long; spikelets deciduous; lemmas awned; plants usually rhizomatous; auricles absent or to 15 mm long; blades 7-50 mm wide.</description>
      <next_statement_id>3.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Caryopses 5-7 mm long; lemma-awn junctions purplish, pubescent; lemma awns 4-16 cm long; plants cespitose or rhizomatous; auricles absent or to 7 mm long</description>
      <determination>2 Oryza rufipogon</determination>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Caryopses 7.5-8.5 mm long; lemma-awn junctions similar in color to the lemmas, glabrous; lemma awns 2.6-8 cm long; plants strongly rhizomatous; auricles present, to 15 mm long</description>
      <determination>1 Oryza longistaminata</determination>
    </key_statement>
  </key>
  <description type="distribution">Va.;Puerto Rico;Virgin Islands;Okla.;Miss.;Tex.;La.;Calif.;N.C.;Ala.;Tenn.;Ark.;Ill.;Ga.;S.C.;Fla.;Mo.</description>
</bio:treatment>
