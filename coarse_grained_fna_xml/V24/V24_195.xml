<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <author>Mary E. Barkworth;</author>
      <date>NA</date>
    </source>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="treatment_page">144</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" date="unknown" authority="Barnhart">Poaceae</taxon_name>
    <taxon_name rank="subfamily" date="unknown" authority="Benth.">POOIDEAE</taxon_name>
    <taxon_name rank="tribe" date="unknown" authority="Dumort.">STIPEAE</taxon_name>
    <taxon_name rank="genus" date="unknown" authority="P. Beauv.">PIPTATHERUM</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe stipeae;genus piptatherum</taxon_hierarchy>
  </taxon_identification>
  <number>10.04</number>
  <description type="morphology">Plants perennial; cespitose or soboliferous, sometimes rhizomatous. Culms 10-140(150) cm, erect, usually glabrous, usually smooth; nodes 1-6; branching intra- or extravaginal at the base, not branching above the base; prophylls concealed by the leaf sheaths. Leaves sometimes basally concentrated; cleistogenes not present; sheaths open, glabrous, smooth to scabrous; auricles absent; ligules 0.2-15 mm, membranous to hyaline; blades 0.5-16 mm wide, flat, involute, valvate, or folded, often tapering in the distal 1/3, apices acute to acuminate, not stiff, basal blades not overwintering, sometimes not developed, flag leaf blades well developed, longer than 1 cm. Inflorescences 3-40 cm, terminal panicles, open or contracted; branches straight or flexuous, usually scabrous, rarely smooth; pedicels often appressed to the branches. Spikelets 1.5-7.5 mm, with 1 floret; rachillas not prolonged beyond the floret; disarticulation above the glumes, beneath the floret. Glumes from 1 mm shorter than to exceeding the florets, subequal or the lower glumes longer than the upper glumes, membranous, 1-9-veined, veins evident, apices obtuse to acute or acuminate; florets 1.5-10 mm, usually dorsally compressed, sometimes terete; calluses 0.1-0.6 mm, glabrous or with hairs, blunt; lemmas 1.2-9 mm, smooth, coriaceous or stiffly membranous, tawny or light brown to black at maturity, 3-7-veined, margins flat, separated and parallel for their whole length at maturity, apices not lobed or lobed, glabrous or hairy, hairs about 0.5 mm, not spreading, awned, lemma-awn junction evident; awns 1-18(20) mm, centric, often caducous, almost straight to once- or twice-geniculate, scabrous; paleas as long as or slightly longer than the lemmas, similar in texture and pubescence, 2(3)-veined, not keeled over the veins, flat between the veins, veins terminating near the apices, apices often pinched; anthers 3, 0.6-5 mm, sometimes penicillate; styles 2 and free to their bases, or 1 with 2-3 branches. Caryopses glabrous, ovoid to obovoid; hila 1/2 as long as to equaling the length of the caryopses. x = 11, 12.</description>
  <discussion>Piptatherum has approximately 30 species, most of which are Eurasian. They extend from lowland to alpine regions, and grow in habitats ranging from mesic forests to semideserts.</discussion>
  <discussion>The pistils in Piptatherum exhibit variability in the development of the styles, a feature that can be seen only in florets shortly before or at anthesis. This variability is reported in the descriptions, but the number of specimens examined per species is low, sometimes only one.</discussion>
  <references>
    <reference>Curto, MX. and D.H. Henderson. 1998. A new Stipa (Poaceae: Stipeae) from Idaho and Nevada. Madrono 45:57-65</reference>
    <reference> Freitag, H. 1975. The genus Piptatherum (Gramineae) in southwest Asia. Notes Roy. Bot. Gard. Edinburgh 42:355-489</reference>
    <reference> Jacobs, S.W.L., R. Bayer, J. Everett, M.O. Arriaga, M.E. Barkworth, A. Sabin-Badereau, M.A. Torres, F. Vazquez, and N. Bagnall. 2006. Systematics of the tribe Stipeae using molecular data. Aliso 23:349-361</reference>
    <reference> Johnson, B.L. 1945. Cytotaxonomic studies in Oryzopsis. Bot. Gaz. 107:1-32.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Basal leaf blades 0-2 cm long; cauline leaf blades 8-16 mm wide; florets 4.5-7.5 mm long</description>
      <determination>6 Piptatherum racemosum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Basal leaf blades 4-45 cm long; cauline leaf blades 0.5-10 mm wide; florets 1.5-6 mm long.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Lemmas and calluses usually glabrous, occasionally sparsely pubescent; florets 1.5-2.5 mm long.</description>
      <next_statement_id>3.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Blades 0.5-2.5 mm wide, often involute; panicles 5-20 cm long, the lower nodes with 1-3 branches</description>
      <determination>4 Piptatherum micranthum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Blades 2-10 mm wide, flat; panicles 10-40 cm long, the lower nodes usually with 3-7 branches, sometimes with 15-30+ branches</description>
      <determination>7 Piptatherum miliaceum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Lemmas evenly pubescent; calluses hairy; florets 1.5-6 mm long.</description>
      <next_statement_id>4.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4.</statement_id>
      <description type="morphology">Awns 3.9-15 mm long, persistent, once- or twice-geniculate.</description>
      <next_statement_id>5.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5.</statement_id>
      <description type="morphology">Primary panicle branches straight, appressed; awns 3.9-7 mm long; florets 3-6 mm long</description>
      <determination>1 Piptatherum exiguum</determination>
    </key_statement>
    <key_statement>
      <statement_id>5.</statement_id>
      <description type="morphology">Primary panicle branches somewhat flexuous, often divergent; awns 5-15 mm long; florets 2.2-4.5 mm long</description>
      <determination>2 Piptatherum canadense</determination>
    </key_statement>
    <key_statement>
      <statement_id>4.</statement_id>
      <description type="morphology">Awns 1-8 mm long, caducous, often absent from herbarium specimens, straight or arcuate.</description>
      <next_statement_id>6.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6.</statement_id>
      <description type="morphology">Awns 4-8 mm long; florets 1.5-2.5 mm long</description>
      <determination>4 Piptatherum micranthum</determination>
    </key_statement>
    <key_statement>
      <statement_id>6.</statement_id>
      <description type="morphology">Awns 1-2.5 mm long; florets 2.2-4.5 mm long.</description>
      <next_statement_id>7.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7.</statement_id>
      <description type="morphology">Lower panicle branches straight; ligules 0.5-2.5 mm long</description>
      <determination>3 Piptatherum pungens</determination>
    </key_statement>
    <key_statement>
      <statement_id>7.</statement_id>
      <description type="morphology">Lower panicle branches flexuous; ligules 1.8-5.5 mm long</description>
      <determination>5 Piptatherum shoshoneanum</determination>
    </key_statement>
  </key>
  <description type="distribution">Md.;N.J.;Utah;Calif.;Pa.;Ariz.;Idaho;Nev.</description>
</bio:treatment>
