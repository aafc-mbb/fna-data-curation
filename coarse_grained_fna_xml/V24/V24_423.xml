<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <other_info_on_meta type="treatment_page">300</other_info_on_meta>
    <other_info_on_meta type="volume">24</other_info_on_meta>
    <other_info_on_meta type="illustrator">Cindy Roché and Annaliese Miller</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" date="unknown" authority="Barnhart">Poaceae</taxon_name>
    <taxon_name rank="subfamily" date="unknown" authority="Benth.">POOIDEAE</taxon_name>
    <taxon_name rank="tribe" date="unknown" authority="Dumort.">TRITICEAE</taxon_name>
    <taxon_name rank="genus" date="unknown" authority="L.">ELYMUS</taxon_name>
    <taxon_name rank="species" date="unknown" authority="Piper">curvatus</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily pooideae;tribe triticeae;genus elymus;species curvatus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Elymus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">virginicus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">submuticus</taxon_name>
    <taxon_hierarchy>genus elymus;species virginicus;variety submuticus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Elymus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">virginicus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">jenkinsii</taxon_name>
    <taxon_hierarchy>genus elymus;species virginicus;variety jenkinsii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Elymus</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">submuticus</taxon_name>
    <taxon_hierarchy>genus elymus;species submuticus</taxon_hierarchy>
  </taxon_identification>
  <number>4</number>
  <other_name type="common_name">Awnless wlldrye</other_name>
  <description type="morphology">Plants cespitose, not rhizomatous, often glaucous. Culms 60-110 cm, stiffly erect, or the base sometimes geniculate; nodes 6-9, concealed or exposed, glabrous. Leaves evenly distributed; sheaths glabrous, often reddish brown; auricles to 1 mm, sometimes absent; ligules shorter than 1 mm, ciliolate; blades 5-15 mm wide, the lower blades usually lax, shorter, narrower, and senescing earlier, the upper blades usually ascending and somewhat involute, adaxial surfaces smooth or scabridulous, occasionally scabrous. Spikes 9-15 cm long, (0.5)0.7-1.3 cm wide, erect, exserted or the bases slightly sheathed, with 2 spikelets per node; internodes 2.5-4.5 mm long, about 0.25-5 mm thick at the thinnest sections, smooth or scabrous beneath the spikelets. Spikelets 10-15 mm, appressed, often reddish brown at maturity, with (2)3-4(5) florets, lowest florets functional; disarticulation below the glumes and beneath the florets, or the lowest floret falling with the glumes. Glumes equal or subequal, the basal 2-3 mm terete, indurate, strongly bowed out, without evident venation, glume bodies 7-15 mm long, 1.2-2.1 mm wide, linear-lanceolate, widening above the base, 3-5-veined, usually glabrous or scabrous, occasionally hispidulous, rarely hirsute on the veins, margins firm, awns 0-3(5) mm; lemmas 6-10 mm, glabrous or scabrous, rarely hirsute, awns (0.5)1-3(4) mm, rarely 5-10 mm on the lemmas of the distal spikelets, straight; paleas 6-10 mm, obtuse, often emarginate; anthers 1.5-3 mm. Anthesis late June to mid-August. 2n = 28, 42.</description>
  <discussion>Elymus curvatus grows in moist or damp soils of open forests, thickets, grasslands, ditches, and disturbed ground, especially on bottomland. It is widespread from British Columbia and Washington, through the Intermountain region and northern Rockies, to the northern Great Plains. It is infrequent or rare in the midwest, the Great Lakes region, and the northeast, and is virtually unknown in the southeast. It is similar to E. virginicus (p. 298), and has sometimes been included in that species as E. virginicus var. submuticus Hook., but is more distinct than the varieties of E. virginicus treated above. Although E. virginicus and E. curvatus overlap greatly in range, E. curvatus usually has a distinct growth form, and its anthesis is 1-2 weeks later (Brooks 1974). Its spikes range from being completely exserted, especially west of the Great Plains, to largely sheathed, especially east of the Mississippi River and in more stressed environments. This geographic trend parallels that within E. virginicus, but sheathed plants of E. curvatus can usually be distinguished by their short awns. Clear transitions to E. virginicus, usually var. jejunus, are rare, but, especially from Missouri to Wisconsin, there are occasional plants with 5-10 mm awns on a few lemmas, especially at the spike tips. Rarely, plants from Missouri and Iowa to Quebec have hispid to hirsute spikelets, suggesting introgression with E. virginicus var. intermedius. There are a few records of apparent hybrids with other species.</discussion>
</bio:treatment>
