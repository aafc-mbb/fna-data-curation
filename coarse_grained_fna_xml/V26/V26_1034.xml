<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <author>James D. Ackerman</author>
      <date>unknown</date>
    </source>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">491</other_info_on_meta>
    <other_info_on_meta type="mention_page">494</other_info_on_meta>
    <other_info_on_meta type="mention_page">496</other_info_on_meta>
    <other_info_on_meta type="mention_page">510</other_info_on_meta>
    <other_info_on_meta type="treatment_page">507</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">orchidaceae</taxon_name>
    <taxon_name rank="subfamily" authority="(Lindley) Szlachetko" date="1995">Vanilloideae</taxon_name>
    <taxon_name rank="tribe" authority="Blume" date="1837">Vanilleae</taxon_name>
    <taxon_name rank="subtribe" authority="Lindley" date="1840">Vanillinae</taxon_name>
    <taxon_name rank="genus" authority="Miller" date="1754">VANILLA</taxon_name>
    <place_of_publication>
      <publication_title>Gard. Dict. Abr. ed.</publication_title>
      <place_in_publication>4, vol. 3. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily vanilloideae;tribe vanilleae;subtribe vanillinae;genus vanilla;</taxon_hierarchy>
    <other_info_on_name type="etymology">Spanish vainilla, little pod or capsule, referring to long, podlike fruits</other_info_on_name>
    <other_info_on_name type="fna_id">134375</other_info_on_name>
  </taxon_identification>
  <description type="morphology">Vines, terrestrial or hemiepiphytic, monopodial. Roots at each stem node, gray-green, slender, and glabrous when free, thick and villous on contact with substrate. Stems scandent, branching, naked, terete, thick, succulent, glabrous. Leaves persistent when large or deciduous when scalelike, distichous, articulate, sheathless, fleshy or leathery. Inflorescences on short lateral branches or peduncles, racemes, bracteate, densely flowered. Flowers ephemeral, resupinate, large, showy, opening sequentially; sepals spreading, distinct and free; petals distinct and/or free, keeled; lip adnate to base of column, simple or lobed, margins basally involute; disc variously ornamented; column elongate, semiterete, footless, often pubescent proximally; anther terminal, versatile, incumbent; pollinia 4, soft, mealy, composed of monads, without accessory structures, appearing triangular when removed as a unit; ovaries articulate proximally and distally; stigma lobes confluent; rostellum undeveloped. Fruits berries, elongate, leathery, indehiscent. Seeds small; seed coat hard.</description>
  <description type="distribution">Tropical and subtropical regions, North America, Mexico, West Indies, Central America, South America, Africa, Southeast Asia, w Pacific Islands.</description>
  <number>2.</number>
  <discussion>Species 100 (5 in the flora).</discussion>
  <discussion>Vanilla pompona Schiede has escaped cultivation in Miami-Dade County, Florida (P. M. Brown 2002). It may be distinguished from the other Vanilla species by the following combination of characteristics: persistent leaves that are much longer than the internodes; flat or straight sepal and petal margins; a simple, yellow-green to yellow-orange lip with a tuft of retrorse hairs; and floral bracts that are at least 12 mm. In addition to the characteristics above, V. pompona generally has thicker leaves and stems than the two species for which it may be confused, V. planifolia and V. phaeantha.</discussion>
  <references>
    <reference>  Nielsen, L. R. and H. R. Siegismund. 1999. Interspecific differentiation and hybridization in Vanilla species (Orchidaceae). Heredity 83: 560–567.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves much shorter than internodes, early deciduous or persistent.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves nearly as long as or longer than internodes, persistent.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Lip largely reddish purple, no yellow present (in flora); most leaves early deciduous, a few persistent, margins revolute, apex hooked.</description>
      <determination>1 Vanilla dilloniana</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Lip deep red adaxially, shading to white margins, with broad yellow midrib; all leaves early deciduous, margins not revolute, apex not hooked.</description>
      <determination>2 Vanilla barbellata</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Sepal and petal margins undulate, apex reflexed; lip disc 3-keeled toward apex, tuft of hairs absent; stems less than 5 mm diam.</description>
      <determination>3 Vanilla mexicana</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Sepal and petal margins straight, apex spreading; lip disc without keels, tuft of scalelike hairs present; stems more than 6 mm diam.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Sepals 7–9 cm; fruits less than 11 cm.</description>
      <determination>4 Vanilla phaeantha</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Sepals 3.5–5.5 cm; fruits 15–25 cm.</description>
      <determination>5 Vanilla planifolia</determination>
    </key_statement>
  </key>
</bio:treatment>
