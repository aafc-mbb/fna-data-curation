<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <other_info_on_meta type="volume">26</other_info_on_meta>
    <other_info_on_meta type="mention_page">553</other_info_on_meta>
    <other_info_on_meta type="mention_page">561</other_info_on_meta>
    <other_info_on_meta type="mention_page">563</other_info_on_meta>
    <other_info_on_meta type="treatment_page">562</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">orchidaceae</taxon_name>
    <taxon_name rank="subfamily" authority="unknown" date="unknown">Orchidoideae</taxon_name>
    <taxon_name rank="tribe" authority="unknown" date="unknown">Orchideae</taxon_name>
    <taxon_name rank="subtribe" authority="unknown" date="unknown">Orchidinae</taxon_name>
    <taxon_name rank="genus" authority="Richard" date="unknown">platanthera</taxon_name>
    <taxon_name rank="species" authority="(L. C. Higgins &amp; S. L. Welsh) Kartesz &amp; Gandhi" date="1990">zothecina</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>69: 134. 1990</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orchidaceae;subfamily orchidoideae;tribe orchideae;subtribe orchidinae;genus platanthera;species zothecina;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="fna_id">242101856</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Habenaria</taxon_name>
    <taxon_name rank="species" authority="L. C. Higgins &amp; S. L. Welsh" date="unknown">zothecina</taxon_name>
    <place_of_publication>
      <publication_title>Great Basin Naturalist</publication_title>
      <place_in_publication>46: 259. 1986</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Habenaria;species zothecina;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">Plants 24–38 cm. Leaves few, scattered along stem, sometimes restricted to proximal portion, wide-spreading, gradually to abruptly reduced distally; bracts 1 or absent; blade oblanceolate, narrowly elliptic, oblong, or ovate-elliptic, 7–17 × 1.8–3.5 cm. Spikes rather lax. Flowers resupinate, not showy, green to yellowish green; lateral sepals reflexed to somewhat spreading; petals ovate-falcate, margins entire; lip descending or reflexed, linear-lanceolate to linear-lance-elliptic, 5–12 × 1–3 mm, margins entire, midline scarcely (perhaps variably) thickened toward base; spur slenderly cylindric, 12–17 mm; rostellum lobes divergent, directed forward, rounded-subangular, rather prominent; pollinaria straight; pollinia remaining enclosed in anther sacs; viscidia orbiculate to suborbiculate; ovary rather slender to stout, 9–16 mm.</description>
  <description type="phenology">Flowering May–Aug.</description>
  <description type="habitat">“Hanging garden” communities on moist to wet, dripping sandstone cliffs and ledges, occasionally springy sites, riparian meadows</description>
  <description type="elevation">1200–1700 m</description>
  <description type="distribution">Ariz., Colo., Utah.</description>
  <number>15.</number>
  <discussion type="conservation">Of conservation concern.</discussion>
  <discussion>Only recently described, the original publication separated Platanthera zothecina from P. sparsiflora solely on the basis of the former’s spur/lip length ratio of 1.5 and few-flowered habit. In fact, the range in ratios overlaps, with a range of at least 1.3–2.6 in P. zothecina and 0.7–1.6 in P. sparsiflora. Actual spur lengths are more useful for determination, and perhaps more significant, because they may indicate specialization for different pollinators; the very limited overlap in spur lengths supports recognition of P. zothecina. Other features support the separation. The thickened median ridge toward the base of the lip characteristic of P. sparsiflora seems to be lacking in P. zothecina, which instead seems to display a parallel series of low ridges, but this distinction unfortunately cannot be established with certainty from the limited sample available. The columns should be investigated in detail, for a limited sample suggests differences in orientation of anther sacs and rostellum lobes. Platanthera sparsiflora is often fewer flowered than P. zothecina, but the typically few-flowered inflorescences of the latter distal to the broad, spreading, and commonly succulent leaves of an often peculiar whitish green color, contribute a markedly distinctive appearance to the plant.</discussion>
  <discussion>Because of the restricted distribution of Platanthera zothecina and its recent description, the species is poorly known; the description here is based on few specimens and hence apt to be too restrictive.</discussion>
</bio:treatment>
