<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>Craig C. Freeman, Felipe Zapata</author>
    </source>
    <other_info_on_meta type="volume">13</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="R. Brown ex Dumortier" date="unknown">ESCALLONIACEAE</taxon_name>
    <taxon_hierarchy>family escalloniaceae</taxon_hierarchy>
  </taxon_identification>
  <number/>
  <other_name type="common_name">Escallonia Family</other_name>
  <description type="morphology">Shrubs [trees, annual herbs], synoecious, not aromatic.  Leaves usually persistent, alternate (spirally arranged), simple; stipules absent; petiole absent [present]; blade margins serrate or doubly serrate, sometimes minutely so, surfaces usually glandular and resinous.  Inflorescences terminal [axillary], racemes or panicles [cymes or flowers solitary].  Flowers bisexual; perianth and androecium epigynous [hypogynous]; hypanthium adnate to ovary proximally, free distally [absent]; sepals [4–]5[–9], often connate proximally; petals [4–]5[–9], distinct; disc epigynous [absent]; stamens [4–]5[–9], alternate with petals, distinct; anthers dehiscing by longitudinal slits; pistils 1[2–5]-carpellate, ovary inferior [superior], 1–[2–6]-carpellate, [1–]2[–6]-locular, placentation parietal [axile]; styles 1[–2, ± distinct]; stigmas 1[–2], terminal; ovules 15–100 per locule.  Fruits capsules.  Seeds 15–100.</description>
  <discussion>Genera 7, species ca. 130 (1 species in the flora).</discussion>
  <description type="distribution">introduced; Central America, South America, Asia, Indian Ocean Islands (Réunion), Australia; introduced also in Europe, Pacific Islands (New Zealand).</description>
  <discussion>Many species of Escalloniaceae occur at high elevations in the Andes; some South American species grow at lower elevations in Brazil, Chile, and Uruguay.  In some parts of the southern Andes, the family is ecologically important and includes conspicuous elements of the landscape (K. R. Young 1993).  In Australia and Africa, it is represented by only a few, narrowly distributed species.</discussion>
  <discussion>This family is morphologically heterogeneous, with no obvious morphological synapomorphies.  Historically, most authors included members of this family within Saxifragaceae (H. G. A. Engler 1930) or Grossulariaceae (A. Cronquist 1981); recently, however, these genera consistently have been placed, with strong statistical support, within the campanulids (D. E. Soltis etal. 2000; J. Lundberg 2001; Angiosperm Phylogeny Group 2009; D. C. Tank and M. J. Donoghue 2010).  Although Escalloniaceae are a well-supported clade, relationships within the family, and between the family and other members of the campanulids, remain uncertain (Tank and Donoghue; Li H. T. etal. 2019).</discussion>
  <references>
    <reference>Mori, S. A.  2004.  Escalloniaceae.  In: N. P. Smith etal., eds.  Flowering Plants of the Neotropics.  Princeton.  Pp. 145–146.</reference>
  </references>
</bio:treatment>
