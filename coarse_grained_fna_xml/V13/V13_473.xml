<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>Guy L. Nesom</author>
    </source>
    <other_info_on_meta type="volume">13</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Lindley" date="unknown">APIACEAE</taxon_name>
    <taxon_name rank="genus" authority="Torrey &amp; A. Gray" date="1840">EURYTAENIA</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>1: 633.  1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family apiaceae;genus eurytaenia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="etymology">Greek eurys, wide, and taenia, ribbon or band, alluding to two broad contiguous oil tubes on commissural face in E. texana, the type species</other_info_on_name>
  </taxon_identification>
  <number>33.</number>
  <other_name type="common_name">Spreadwing</other_name>
  <description type="morphology">Herbs annual, odor parsleylike, caulescent, 30–120 cm, scabrous-papillate in inflorescence or glabrate; slender-taprooted.  Leaves: basal 1-pinnate; blade ovate, herbaceous, not septate; leaflets undivided, sometimes 3-lobed, lanceolate to ovate-lanceolate, margins coarsely crenate to serrate; cauline 2(–3)-pinnate; blade ovate, herbaceous; leaflets pinnately dissected, ovate to lanceolate, margins sharply serrate or entire.  Umbels terminal and leaf-opposed, compound, convex, peripheral flowers not different; involucral bracts 3-cleft, herbaceous; involucel bractlets present.  Pedicels present.  Flowers bisexual; sepals well developed; petals white, apex inflexed, with a narrower appendage; stylopodium low-conic.  Schizocarps elliptic to broadly elliptic to suborbiculate, strongly dorsiventrally compressed, splitting; mericarps not beaked, ribs 5, abaxial filiform, lateral broadly winged, surface scabrous-papillate abaxially, commissure smooth or papillate; oil ducts large, flattened, 1 in intervals, 2 on commissure; carpophore 2-fid.  x = 7.</description>
  <discussion>Species 2 (2 in the flora).</discussion>
  <description type="distribution">sc United States.</description>
  <discussion>Leaf morphology of Eurytaenia is distinctive.  The basal leaves are once-compound with the leaflets lanceolate to ovate-lanceolate and coarsely serrate-margined.  The cauline become 2(–3)-pinnate and the leaflets much narrower with entire margins, the medial and distal filiform to linear.  All leaves tend to be persistent, and the transition in morphology usually is evident on a single plant.</discussion>
  <references>
    <reference>Nesom, G. L.  2012b.  Taxonomy of Eurytaenia (Apiaceae).  Phytoneuron 2012-67: 1–7.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Schizocarps broadly elliptic to suborbiculate; mericarps: commissure smooth, wings thickened near body but becoming thinner than body toward margins; oil ducts not covered by pericarp or epidermis, those on commissure depressed-semicircular.</description>
      <determination>1. Eurytaenia texana</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Schizocarps elliptic; mericarps: commissure papillate over oil ducts, wings thickened to abruptly narrowed-rounded margins, thicker than body; oil ducts lightly covered by pericarp or epidermis, those of commissure narrowly lenticular.</description>
      <determination>2. Eurytaenia hinckleyi</determination>
    </key_statement>
  </key>
</bio:treatment>
