<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>Derick B. Poindexter</author>
    </source>
    <other_info_on_meta type="volume">13</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Lindley" date="unknown">APIACEAE</taxon_name>
    <taxon_name rank="genus" authority="(Torrey &amp; A. Gray) Drude in H. G. A. Engler and K. Prantl" date="1898">TAENIDIA</taxon_name>
    <place_of_publication>
      <publication_title>Nat. Pflanzenfam.</publication_title>
      <place_in_publication>175/176[III,8]: 195.  1898</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family apiaceae;genus taenidia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="etymology">Greek tainidion, little band, alluding to low ribs of fruit</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name rank="genus" authority="[unranked] Taenidia Torrey &amp; A. Gray" date=" 1840">Zizia</taxon_name>
    <place_of_publication>
      <publication_title>Fl. N. Amer.</publication_title>
      <place_in_publication>1: 614.  1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus zizia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="Mackenzie" date="unknown">Pseudotaenidia</taxon_name>
    <taxon_hierarchy>genus pseudotaenidia</taxon_hierarchy>
  </taxon_identification>
  <number>67.</number>
  <other_name type="common_name">Pimpernel</other_name>
  <description type="morphology">Herbs perennial, odor anise- or celerylike, caulescent, 30–100 cm, glabrous, often glaucous; roots tuberous-thickened.  Leaves usually 2–3-ternate, distal progressively less compound; blade broadly ovate-deltate, herbaceous; leaflets simple or sometimes 2-lobed, ovate or obovate to oblong-elliptic, margins entire.  Umbels terminal and axillary, compound, convex, widely branching, outer umbellets fertile, central usually sterile, consisting entirely of staminate flowers, marginal flowers of fertile umbellets sometimes entirely pistillate, with staminate inner flowers, peripheral flowers not different; involucral bracts absent; involucel bractlets usually absent.  Pedicels present.  Flowers bisexual or staminate; sepals absent or minute; petals yellow, apex inflexed, with a narrower appendage; stylopodium absent.  Schizocarps subglobose, slightly laterally compressed, or elliptic-oblong, strongly dorsiventrally compressed, splitting; mericarps not beaked, ribs 5, abaxial low, not winged, lateral winged or not winged, surface smooth, gla­brous; oil ducts 3–4 in intervals, 4 on commissure; carpophore 2-fid to base.  x = 11.</description>
  <discussion>Species 2 (2 in the flora).</discussion>
  <description type="distribution">e North America.</description>
  <discussion>Traditional subfamilial classifications, based exclusively on fruit characteristics, originally placed the two members of Taenidia within separate tribes (Apieae Takhtajan ex V. M. Vinogradova [= Ammiineae Link] and Peucedaneae Dumortier), and they were included within the monospecific genera Pseudotaenidia and Taenidia (R. L. Guthrie 1968).  Current molecular systematic studies (G. M. Plunkett and S. R. Downie 1999) have proven that this traditional approach does not reflect phylogenetic relationships.  A. Cronquist’s (1982b) assessment of these two genera seems appropriate; with the exception of minor differences in fruit characteristics and fragrance, these two taxa are essentially identical and are here treated as congeneric.</discussion>
  <references>
    <reference>Selected REFERENCES   Cronquist, A.  1982b.  Reduction of Pseudotaenidia to Taenidia (Apiaceae).  Brittonia 34: 365–367.   Guthrie, R. L.  1968.  A Biosystematic Study of Taenidia and Pseudotaenidia (Umbelliferae).  Ph.D. dissertation.  West Virginia University.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Schizocarps slightly laterally compressed, subglobose; mericarp lateral ribs not winged.</description>
      <determination>1. Taenidia integerrima</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Schizocarps strongly dorsiventrally compressed, elliptic-oblong; mericarp lateral ribs winged.</description>
      <determination>2. Taenidia montana</determination>
    </key_statement>
  </key>
</bio:treatment>
