<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">13</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Lindley" date="unknown">APIACEAE</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">ERYNGIUM</taxon_name>
    <taxon_name rank="species" authority="Linnaeus" date="1753">aquaticum</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 232.  1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family apiaceae;genus eryngium;species aquaticum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>2.</number>
  <other_name type="common_name">Rattlesnake master</other_name>
  <description type="morphology">Herbs biennial or perennial, glabrous; fibrous-rooted.  Stems solitary, erect, branching from above-ground cauline leaf axils, sparsely leafy, 30–180 cm, not rooting at nodes, internodes 3–20 cm.  Leaves: basal usually fugacious; petiole 5–44 cm, septate, smooth; blade linear or linear-triangular to narrowly oblong or narrowly elliptic, sometimes elliptic, 3–45 × 0.5–8 cm, fleshy-coriaceous, unlobed, margins entire, irregularly crenate, or serrate, not callous-thickened or callous-thickened; venation pinnate; cauline similar to basal leaves but distally petiole sheathing, shorter and blade narrower, 4–43 × 0.2–7.5 cm, margins of distals entire, crenate, or serrate, sometimes deeply so.  Heads pedunculate, in terminal and lateral pleiochasia or dichasia, white to light blue, ovoid, ovoid-cylindric or globose, sometimes hemispheric, 9–18 × 8–18 mm; involucral bracts reflexed, sometimes spreading, green, whitish green, to light blue, narrowly ovate or narrowly triangular, 7–32 × 1.5–20 mm, coriaceous, margins entire or sparsely spinose-serrate to deeply so, callous-thickened; floral bractlets obovate, narrowly obovate, or linear, 3–7 mm, margins entire or 3-fid, coma absent.  Flowers: sepals narrowly ovate or ovate, 2–3.5 mm; petals 1–2 mm, apex fimbriate.  Schizocarps globose, cylindric, or obovoid, 2–3 mm, surface densely papillate or scaly, papillae linear.</description>
  <discussion>Varieties 3 (3 in the flora).</discussion>
  <description type="distribution">e, se United States.</description>
  <discussion>M. E. Mathias and L. Constance (1941, 1944–1945) recognized Eryngium ravenellii and E. floridanum as varieties of E. aquaticum.  Later, C. R. Bell (1963) and A. E. Radford et al. (1968) placed E. floridanum in synonymy under E. aquaticum var. ravenellii without any comments but clearly deciding that incision of the floral bractlet margins (entire versus 3-fid) is not a good character to separate these two entities that share ovate, acute sepals with spines less than 0.5 mm and scaly mericarps.  Plants with equally 3-fid floral bractlets (var. ravenellii in the strict sense) consistently have very narrow basal and cauline leaf blades with decurrent bases and differ from var. floridanum not only in the incision of the floral bractlets, but also in the shape of the blade base, shape of the petals, and arrangement of scales on the mericarps.  Therefore, until field, mor­phometric, and molecular studies are undertaken to understand the circumscription of these taxa, three varieties are recognized here.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Floral bractlet margins entire.</description>
      <determination>2b. Eryngium aquaticum var. floridanum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Floral bractlet margins 3-fid.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Floral bractlets: middle lobe longer than laterals; sepals narrowly ovate, 0.3–0.5 mm wide, apex acuminate, spine 1 mm; mericarps papillate.</description>
      <determination>2a. Eryngium aquaticum var. aquaticum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Floral bractlets: lobes of equal length; sepals ovate, 0.8–1 mm wide, apex acute, spine less than 0.5 mm; mericarps scaly.</description>
      <determination>2c. Eryngium aquaticum var. ravenellii</determination>
    </key_statement>
  </key>
</bio:treatment>
