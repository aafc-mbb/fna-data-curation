<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">13</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Lindley" date="unknown">APIACEAE</taxon_name>
    <taxon_name rank="genus" authority="Rafinesque" date="1819">LOMATIUM</taxon_name>
    <taxon_name rank="species" authority="(Pursh) J. M. Coulter &amp; Rose" date="1900">triternatum</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>7: 227.  1900</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family apiaceae;genus lomatium;species triternatum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Seseli</taxon_name>
    <taxon_name rank="species" authority="Pursh" date="1813">triternatum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Amer. Sept.</publication_title>
      <place_in_publication>1: 197.  1813</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus seseli;species triternatum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Lomatium</taxon_name>
    <taxon_name rank="species" authority="(Pursh) J. M. Coulter &amp; Rose" date="unknown">triternatum</taxon_name>
    <taxon_name rank="variety" authority="(J. M. Coulter &amp; Rose) Mathias" date="unknown">macrocarpum</taxon_name>
    <taxon_hierarchy>genus lomatium;species triternatum;variety macrocarpum</taxon_hierarchy>
  </taxon_identification>
  <number>98.</number>
  <other_name type="common_name">Nineleaf or ternate biscuitroot</other_name>
  <description type="morphology">Herbs green or slightly blue-green, acaulous or caulescent, (15–)20–60(–80) cm, glabrous or hirtellous; caudex simple or few-branched, with persistent leaf bases, without persistent peduncles; taproot slender.  Pseudoscapes usually absent, if present subterranean.  Leaves arising at slightly different heights, not forming just 1 rosette, green, 1–2-ternate-1–3-pinnate-0–1-pinnatifid; petiole sheathing basally to entire length; blade ovate to broadly ovate, 4–20 × 2–12 cm, surfaces glabrous or hairy; ultimate segments 12–45, linear to lanceolate, (23–)55–80(–119) × (0.4–)1–3.5(–5) mm, relatively nar­row, length/width ratio 14–30(–40), often chan­neled, margins entire, usually not reflexed, apex acute, generally tapered differently than base, callus tips 0–0.1 mm, terminal segment (23–)55–80(–120) mm, length/width ratio 14–30(–40); cauline leaves 0–3, if present, with more than 5 ultimate segments.  Peduncles 1–3 per plant, 1–2 per stem, ascending or erect, not inflated, 10–35 cm, exceeding leaves, 2–6 mm wide 1 cm below umbel, glabrous or finely hirtellous.  Umbels 2–3 cm wide in flower, 4–15 cm wide in fruit, rays 4–20, ascending to erect, 1.5–8(–10) cm in fruit, unequal, glabrous or hirtellous; involucel bractlets (0–)1–10, distinct, linear, 1.5–8 mm, shorter or longer than flowers, margins scarious, not ciliate, entire, glabrous.  Fruiting pedicels (1–)2–6(–10) mm, shorter than fruit, spreading to erect when fruit is mature.  Flowers: petals bright yellow, glabrous; anthers yellow; ovary and young fruit glabrous or hairy, more sparsely so with age.  Mericarps dorsiventrally compressed, broadly to narrowly elliptic, (7.2–)9.2–11(–14) × (2.4–)3–4.5(–5.1) mm, length/width ratio (2.2–)2.5–3(–3.8); wings (0.4–)0.9–1(–1.4) mm wide, (10–)30–50(–60)% of body width, paler than body; abaxial ribs slightly to prominently raised; apex rounded or broadly rounded at tip; oil ducts 1 in inter­vals, 2 on commissure.</description>
  <discussion>The Lomatium triternatum complex consists of yellow-petaled, moderate-sized to large Lomatium with relatively few, long, usually narrow ultimate leaf seg­ments and without tuberlike root swellings.  For iden­tification, mature mericarps are usually necessary but rarely sufficient.</discussion>
  <discussion>The group is in a state of incipient speciation and is too early in its diversification for clear differences to become established.  Genetic evidence reveals several lineages, each varying in height, hairiness, the length and width of leaf blade ultimate segments, and the size and shape of mericarps.  The best taxonomic treatment for these lineages is unclear.  This treatment recognizes Lomatium andrusianum, L. anomalum, L. brevifolium, L. packardiae, and L. triternatum, but combining them all into a diverse L. triternatum could be justified, as could recognizing many more taxa.  Practical botanists may choose to treat all these taxa as “L. triternatum sensu lato” (“in the broad sense”).</discussion>
  <discussion>Preliminary data suggest that the taxa recognized here have some geographic coherence (M. V. Ottenlips et al. 2021).  Lomatium andrusianum occurs from the Boise Foothills east, perhaps to Wyoming.  Lomatium anomalum occurs from north-central Oregon south.  The range of L. packardiae is unclear but may be restricted to the Succor Creek area of Malheur County, Oregon; it may best be treated as part of L. anomalum.  Lomatium brevifolium occurs west of the Cascade Range and near it to the east.  Lomatium triternatum occurs from southwest Washington and immediately adjacent Oregon eastward across northern Idaho to western Montana; reports of L. triternatum from outside this range are based on other species in this complex or L. simplex.  Additional study of the whole complex is needed.</discussion>
  <discussion>The morphologically similar Lomatium simplex (syn­onym, L. triternatum subsp. platycarpum) is moder­ately distant genetically.  Its proportionately wider mature fruit permit identification if they are collected, which they rarely are.  Preliminary results place L. thompsonii as sister to the L. triternatum clade.</discussion>
  <discussion>Lomatium triternatum in the broad sense contains culturally significant food and medicinal plants for members of the Atsugewi, Niitsitapi (Blackfoot), Paiute, and Okanagan-Colville Native nations, among others (D. E. Moerman 1998).  Native peoples traded seeds and roots, sometimes over long distances, likely contributing to the complicated pattern of variation observed today.</discussion>
  <description type="phenology">Flowering May–Jul; fruiting mid Jun–late Jul.</description>
  <description type="habitat">Open slopes, meadows, rocky hillsides, dry to fairly moist soils.</description>
  <description type="elevation">200–2000 m.</description>
  <description type="distribution">Idaho, Mont., Oreg., Wash.</description>
</bio:treatment>
