<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">13</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="R. Brown" date="unknown">ANACARDIACEAE</taxon_name>
    <taxon_name rank="genus" authority="Miller" date="1754">TOXICODENDRON</taxon_name>
    <place_of_publication>
      <publication_title>Gard. Dict. Abr. ed.</publication_title>
      <place_in_publication>4, vol. 3.  1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family anacardiaceae;genus toxicodendron</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek toxikon, arrow poison or poison, and dendron, tree, alluding to noxious property</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="Linnaeus" date="unknown">Rhus</taxon_name>
    <taxon_name rank="subgenus" authority="(Miller) A. Gray" date="unknown">Toxicodendron</taxon_name>
    <taxon_hierarchy>genus rhus;subgenus toxicodendron</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Rhus</taxon_name>
    <taxon_name rank="section" authority="Engler" date="unknown">Venenatae</taxon_name>
    <taxon_hierarchy>genus rhus;section venenatae</taxon_hierarchy>
  </taxon_identification>
  <number>11.</number>
  <description type="morphology">Shrubs or lianas, sometimes trees, polygamodioecious; thorns absent; exudate clear to white, viscous, turning black with exposure.  Leaves deciduous, imparipinnate, 3(–5)-foliate [1-foliolate]; rachis not winged; leaflets 3–11(–15), opposite to subopposite, petiolule present or absent or nearly absent, blade narrowly lanceolate, elliptic, ovate, orbiculate, oblanceolate, obovate, or deltate, margins entire, serrate, dentate, crenate, or lobed.  Inflorescences axillary, panicles.  Pedicels present.  Flowers bisexual or staminate, perianth (4–)5(–6)-merous, imbricate; petals white to greenish; disc glabrous; stamens 5; anthers dorsifixed; pistil 3-carpellate; styles 3, terminal; stigmas 3, capitate; ovules basal, pendulous.  Drupes globose, often slightly laterally compressed, 1-locular; exocarp yellowish to white or pale gray, glabrous or hairy with nonglandular hairs, separating from mesocarp at maturity; mesocarp thin, white-waxy, striated with resin canals; endocarp bony, smooth to ridged.  Seeds ellipsoid, compressed.  x=15.</description>
  <discussion>Species ca. 25 (4 in the flora).</discussion>
  <description type="distribution">North America, Mexico, West Indies, Bermuda, Central America, South America, Asia, Pacific Islands.</description>
  <discussion>Toxicodendron is perhaps the most notorious North American genus of Anacardiaceae due to the contact dermatitis that it can cause in 85–90% of people who come in contact with its urushiol compounds (W. L. Epstein 1994).</discussion>
  <discussion>Although some botanists still do not recognize Toxicodendron separate from Rhus, there is clear and abundant evidence, both morphological and molecular, that support the naming of these two non-sister lineages as distinct genera (W. T. Gillis 1971; A. J. Miller etal. 2001; S. K. Pell 2004; T. S. Yi etal. 2004, 2007).  Molecular data show that Toxicodendron and Rhus are not sister lineages (Miller etal.; Pell; Yi etal. 2004, 2007; Z. L. Nie etal. 2009; A. Weeks etal. 2014; Jiang Y. etal. 2019), nor do they form a monophyletic group even when all the segregates from Rhus, in the broad sense, are included (Pell, unpubl.).</discussion>
  <discussion>W. T. Gillis (1971) treated two sections within the genus (Simplicifolia Gillis and Toxicodendron) and mentioned another section (Venenata) that has been referenced in numerous subsequent publications but has never been validly published and for which Gillis’s intended composition is unclear.  More recently a fourth section, Griffithii T. L. Ming, was erected to accommodate species with exocarps dehiscing in a star pattern.  Most species in the flora area are members of sect. Toxicodendron, with the exception of T. vernix, which has been recognized in Gillis’s invalid sect. Venenata by authors for over 50 years.  Recently, S. K. Pell and K. N. Gandhi validated the name as T. sect. Venenata (Engler) Gillis ex Pell &amp; Gandhi.  North American Toxicodendron taxa with 3–5 leaflets exhibit a great deal of morphological and ecological plasticity, making it difficult to distinguish evolutionary entities, if indeed they are truly distinct from one another.  The most recognizable taxa are treated here at either the specific or varietal level, depending on the extent of their morphological distinction.  However, it should be noted that all of these Toxicodendron taxa from the flora area could be recognized as a single species, T. radicans, due to the region-wide degree of overlap in the characters used to separate them in more geographically restricted floras.  Hybridization seems to be common among overlapping species and has been reported in the literature (Gillis) and in numerous herbarium specimen labels and notes.</discussion>
  <discussion>Native Americans extracted dye from native species of Toxicodendron for a wide variety of applications, from staining baskets to tattooing skin.  The leaves have been used medicinally for various purposes (D. E. Moerman 1998), and their ingestion is often erroneously suggested as a way to build immunity to the dermatitis caused by contact with the urushiol compounds in these plants.  Two species of Toxicodendron, T. succedaneum (Linnaeus) Kuntze and T. vernicifluum (Stokes) F. A. Barkley, which are used to make lacquer, are planted in the United States and have high potential for invasion.  Toxicodendron succedaneum has naturalized in many countries, including Australia, China, Japan, Korea, and New Zealand.  In Australia, both T. radicans and T. succedaneum are classified as noxious weeds.</discussion>
  <references>
    <reference>Gillis, W. T.  1971.  The systematics and ecology of poison-ivy and the poison-oaks (Toxicodendron, Anacardiaceae).  Rhodora 73: 72–159, 161–237, 370–443, 465–540.</reference>
    <reference>Guin, J. D., W. T. Gillis, and J. H. Beaman.  1981.  Recognizing the toxicodendrons (poison ivy, poison oak, and poison sumac).  J. Amer. Acad. Dermatol. 4: 99–114.</reference>
    <reference>Senchina, D. S.  2006.  Ethnobotany of poison ivy, poison oak, and relatives (Toxicodendron spp., Anacardiaceae) in America: Veracity of historical accounts.  Rhodora 108: 203–227.</reference>
    <reference>Senchina, D. S.  2008.  Fungal and animal associates of Toxicodendron spp. (Anacardiaceae) in North America.  Perspect. Pl. Ecol. Evol. Syst. 10: 197–216.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaflets 7–11(–15).</description>
      <determination>4. Toxicodendron vernix</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaflets 3(–5).</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaflet blades elliptic to ovate to obovate; leaflet apices acute to obtuse to rounded or truncate.</description>
      <determination>1. Toxicodendron diversilobum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaflet blades generally narrowly lanceolate to elliptic, ovate, orbiculate to deltate; leaflet apices rounded, acute to long-acuminate.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Shrubs; leaflet blade margins dentate to lobed.</description>
      <determination>2. Toxicodendron pubescens</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Shrubs or lianas, sometimes trees; leaflet blade margins entire to serrate, dentate, or shallowly lobed.</description>
      <determination>3. Toxicodendron radicans</determination>
    </key_statement>
  </key>
</bio:treatment>
