<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">13</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Lindley" date="unknown">APIACEAE</taxon_name>
    <taxon_name rank="genus" authority="Rafinesque" date="1819">LOMATIUM</taxon_name>
    <taxon_name rank="species" authority="Darrach &amp; D. H. Wagner" date="2011">pastorale</taxon_name>
    <place_of_publication>
      <publication_title>J. Bot. Res. Inst. Texas</publication_title>
      <place_in_publication>5: 428, fig. 1.  2011</place_in_publication>
      <other_info_on_pub>(as pastoralis)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family apiaceae;genus lomatium;species pastorale</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
  </taxon_identification>
  <number>73.</number>
  <other_name type="common_name">Shepherd’s desert-parsley</other_name>
  <other_name type="common_name">Umatilla biscuitroot</other_name>
  <description type="morphology">Herbs green, acaulous, 6–37 cm, glabrous; caudex simple or 2–7-branched, with or without persistent leaf sheaths weather­ing into sparse thatch of chaffy or chartaceous scales at base of pseudoscape, without persistent peduncles; taproot slender to thickened, lacking shallow, tuberlike swellings although roots in narrow cracks in rocks may form slender, deep-seated, fusiform or irregular swellings.  Pseudoscapes subterranean.  Leaves arising at slightly different heights, not forming just 1 rosette, green, ternate-(1–)2-pinnate; petiole sheathing basally to entire length; blade equilaterally triangular to rhombic, 5.6–15 × 4.5–17.9 cm, surfaces glabrous; ultimate segments (5–)15–80, ascending to spreading in life, oblong, linear, narrowly elliptic, 1–60 × 0.8–6(–9.7) mm, relatively narrow, lateral leaf segments often narrowed to base forming petiolule or ultimate segments crowded, confluent at base with adjacent lobe, petiolule absent, margins entire, apex acute, callus tips 0–0.1 mm, terminal segment 22–37 mm; cauline leaves 0.  Peduncles 1–10(–19) per plant, 1–2+ per stem, usually strongly decumbent, becoming ascending to erect dis­tally with age, often purplish when mature, not inflated, 2–20(–33) cm, greatly exceeding leaves, 1.5–3.3 mm wide 1 cm below umbel, glabrous or with minute and blunt papillae.  Umbels 1.1–5 cm wide in flower, 1.5–13.3 cm wide in fruit, rays 3–22, usually spreading less than 180 degrees, 1–10.5 cm in fruit, unequal, glabrous; involucel bractlets (0 or)4–8(–12), present on most umbellets, distinct, linear or lanceolate, 1.5–5 mm, sub­equal to flowers, margins sometimes narrowly scarious, not ciliate, entire, sometimes slightly lobed at base, glabrous.  Fruiting pedicels 0.5–3.5(–5.4) mm, subequal to fruit.  Flowers: petals bright yellow to rarely cream with clear to greenish midvein, glabrous; anthers bright to creamy yellow; ovary and young fruit glabrous.  Mericarps dorsiventrally compressed, narrowly elliptic, rarely oblong, 5–11.5 × 2.2–3.8 mm, length/width ratio (2.3–)2.5–3(–3.6), crowded or not; wings 0.3–0.5(–0.6) mm wide, 10–36% of body width, paler than body; abaxial ribs not raised; apex acute to obtuse; oil ducts 3(–5) in intervals, 1(–3) on commissure, 1 in each wing, obscure.</description>
  <discussion>Lomatium pastorale is usually yellow-petaled, and its stems are usually decumbent.  It is known only from Umatilla and Union counties, where it can be abundant in heavily disturbed sites, such as quarries and areas that were severely overgrazed a century ago, but it is also found at lower density in other open sites that lack well-developed soils. The roots penetrate into cracked basalt bedrock.  This species is similar to the L. triternatum complex but glabrous or nearly so.  Lomatium leptocarpum differs in its upright peduncles and rays, narrower and nearly sessile fruit, and its nearly radial involucel.  See discussion under the very similar 93. L. tarantuloides of Grant and Baker counties, Oregon.</discussion>
  <description type="phenology">Flowering late Apr–early Jun; fruiting early Jun–early Jul.</description>
  <description type="habitat">Disturbed, flat to gently sloping, open, rocky or gravelly sites that are moist or saturated in spring but dry later, scablands, forests, shallow, poorly developed soils derived from loess or weathered basalt.</description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">1500–1800 m.</description>
  <description type="distribution">Oreg.</description>
</bio:treatment>
