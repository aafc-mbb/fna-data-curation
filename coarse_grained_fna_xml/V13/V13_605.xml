<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">13</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Lindley" date="unknown">APIACEAE</taxon_name>
    <taxon_name rank="genus" authority="Rafinesque" date="1819">LOMATIUM</taxon_name>
    <taxon_name rank="species" authority="J. A. Alexander &amp; Whaley" date="2018">papilioniferum</taxon_name>
    <place_of_publication>
      <publication_title>J. Bot. Res. Inst. Texas</publication_title>
      <place_in_publication>12: 411, figs. 1F-H, 10.  2018</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family apiaceae;genus lomatium;species papilioniferum</taxon_hierarchy>
  </taxon_identification>
  <number>70.</number>
  <other_name type="common_name">Butterfly lomatium</other_name>
  <description type="morphology">Herbs blue-green to green, acaulous, short-caulescent, caulescent, 40–80 cm, glabrous; caudex simple to multicipital, with persistent leaf sheaths weathering into dense thatch of fibers and chaffy scales, with a few persistent, gray peduncles; taproot thick, with deep, irregular or ovoid, tuberlike swellings.  Pseudoscapes subterranean.  Leaves arising at slightly different heights, not forming just 1 rosette, green, ternate-3–4+-pinnate; petiole sheathing basally, rarely to 1/2 length; blade triangular, rhombic, or ovate, 4.8–8.2 × 3.5–11 cm, surfaces glabrous; apical 2–3 pinnule pairs of secondary leaflets with naked intercostal region between rachilla and basalmost pinnule lobes, appearing like petiolule, this “petiolule” of penultimate segments 1–5(–6) mm; penultimate segments narrow, usually less than 2 mm wide; ultimate segments 300–4000, narrowly linear, lanceolate, or deltate, dorsiventrally compressed (appearing flat) or subterete in cross section, 1–5(–9 in shade) × 0.1–0.6 mm, diffuse, not obscuring elongate intercostal areas along rachillae (best assessed from at least a foot distant from plant), margins entire, apex acute or acuminate, callus tips 0–0.1 mm, terminal segment 5–6 mm; cauline leaves 0–2.  Peduncles 1–10+ per plant, 1 per stem, ascending or erect, not inflated, 7–60 cm, stout, exceeding leaves, (1–)2–6 mm wide 1 cm below umbel, glabrous.  Umbels 3–6 cm wide in flower, 2.3–9 cm wide in fruit, rays 6–40, ascending to erect, 1–20 cm in fruit, subequal, glabrous; involucel bractlets (0 or)2–10, distinct, linear to lanceolate, 2–8 mm, equal or subequal to flowers, margins narrowly scabrous, not ciliate, entire, glabrous.  Fruiting pedicels 5–11(–20) mm, usually shorter than fruit.  Flowers: petals yellow, glabrous; anthers yellow or ochroleucous; ovary and young fruit glabrous.  Mericarps dorsiventrally compressed, oblong or elliptic, 6–13 × 3.4–5 mm, length/width ratio 1.6–2.5; wings 0.5–1.2 mm wide, 14–50% of body width, paler than body; abaxial ribs prominately raised; apex rounded to truncate; oil ducts 1–2 in intervals, 2–6 on commissure.  2n = 22.</description>
  <discussion>Lomatium papilioniferum is the widespread northern segregate of the L. grayi complex.  It is characterized by short, narrowly linear to deltate ultimate leaf segments that give the leaf a three-dimensional structure and by proportionately short, elliptic fruit.  Ultimate leaf segments of L. papilioniferum are more diffuse and less clustered than those of L grayi, which grows in Utah to the east and south of L. papilioniferum.  For plants from the Columbia River Gorge of Oregon and Washington, see the discussion under 46. L. klickitatense.  As the name suggests, L. papilioniferum is a host plant for larval swallowtail butterflies (genus Papilio).</discussion>
  <description type="phenology">Flowering mid Apr–Jun; fruiting Apr–late Jun.</description>
  <description type="habitat">Rocky hillsides, talus, rock outcrops, cliffs.</description>
  <description type="elevation">90–1600 m.</description>
  <description type="distribution">B.C.; Calif., Idaho, Nev., Oreg., Wash.</description>
</bio:treatment>
