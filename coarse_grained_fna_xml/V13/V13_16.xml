<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">13</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">GERANIACEAE</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">GERANIUM</taxon_name>
    <taxon_name rank="species" authority="(Trelease) A. Heller" date="1898">texanum</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Torrey Bot. Club</publication_title>
      <place_in_publication>25: 198.  1898</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family geraniaceae;genus geranium;species texanum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Geranium</taxon_name>
    <taxon_name rank="species" authority="Linnaeus" date="unknown">carolinianum</taxon_name>
    <taxon_name rank="variety" authority="Trelease" date="1888">texanum</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Boston Soc. Nat. Hist.</publication_title>
      <place_in_publication>4: 76, plate 12, fig. 8.  1888</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus geranium;species carolinianum;variety texanum</taxon_hierarchy>
  </taxon_identification>
  <number>5.</number>
  <description type="morphology">Herbs annual, 10–48 cm.  Leaves: basal ±deciduous, mid cauline alternate, distal opposite; blade polygonal, palmatifid, ratio main-sinus length/middle-segment length 0.7–0.9, 1.7–4.7 × 2.2–6 cm, segments 5(–7), rhombic, 5–9-lobed in distal 1/2.  Inflorescences usually solitary, sometimes scattered, aggregated at top of each branch, cymules 2-flowered; peduncle (1–)10–18(–50) mm, sometimes absent, eglandular-hairy, hairs retrorse, appressed, 0.1–0.5 mm.  Pedicels 4–14.8 mm, eglandular-hairy, hairs retrorse, appressed, 0.1–0.4 mm.  Flowers: sepals accrescent, 3.5–4.9 × 2.2–3.9 mm, mucro 0.6–1.4 mm, abaxial surface eglandular-hairy (mainly on veins), hairs antrorse, 0.1–0.6 mm, adaxial surface glabrous; petals erect-patent, white, 4–5 × 1.5–2 mm, basal margins ciliate, apex rounded, surfaces glabrous; fertile stamens 5, external whorl without anthers, filaments 2.2–4 mm, apex abruptly narrowed, glabrous except sparsely ciliate on proximal 1/2; anthers blue or bluish-tinged, 0.3–0.5 × 0.3–0.4 mm; nectary glabrous.  Schizocarps erect when immature, mature fruits 14–18.3 mm; mericarps ±blackish, 2.9–3.5 × 1.3–2 mm, sparsely eglandular-hairy, hairs antrorse, 0.2–0.7 mm, smooth; rostrum 9.7–13.2 mm, narrowed apex 0.5–1 mm; stigmatic remains 0.5–0.7 mm, ratio fruit length/stigmatic-remains length 7–8, lobes 5, hairy.  Seeds 1.9–2.2 × 1.6–1.8 mm, ±reticulate.</description>
  <discussion>Geranium texanum seems to be related to G.carolinianum, from which it can be distinguished by the type of indumentum on the inflorescence and fruit.  In G.texanum, pedicels have retrorse, appressed, eglandular hairs, while G.carolinianum usually has patent, glandular, and eglandular hairs; however, in some forms of G.carolinianum, the glandular hairs are scattered or even lacking.  In these forms, hairs can be retrorse but never appressed.  Geranium texanum fruits show scattered, short hairs; whereas in G.carolinianum, fruits are densely covered by long hairs.  Sepals, petals, and fruits are longer in G.carolinianum than in G.texanum.  On the other hand, two characters support the close relationship between G.carolinianum and G.texanum: the short narrowed apex of the fruit rostrum and the short pedicels.</discussion>
  <discussion>Since Geranium texanum reaches the Mexican border, it is probably present in Mexico; however, no specimens of this species from Mexico are available at the studied herbaria (C. Aedo 2000). Its presence in California (and in the Azores) probably constitutes an occasional introduction.</discussion>
  <description type="phenology">Flowering Mar–Apr.</description>
  <description type="habitat">Open woods, clearings, disturbed areas.</description>
  <description type="elevation">0–700 m.</description>
  <description type="distribution">Calif., La., Okla., Tex.; introduced in Atlantic Islands (Azores).</description>
</bio:treatment>
