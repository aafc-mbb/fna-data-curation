<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">13</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Lindley" date="unknown">APIACEAE</taxon_name>
    <taxon_name rank="genus" authority="Fischer ex Hoffmann" date="1814">CONIOSELINUM</taxon_name>
    <taxon_name rank="species" authority="J. M. Coulter &amp; Rose" date="1900">mexicanum</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Wash. Acad. Sci.</publication_title>
      <place_in_publication>1: 147.  1900</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family apiaceae;genus conioselinum;species mexicanum</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">Herbs 40–100 cm.  Stems simple, slender.  Leaves: basal (0–)1–2(–5), 2(–3)-pinnate; peti­ole (2–)3–20(–28) cm; blade ovate or deltate, (5–)9–16(–36) cm, terminal leaflets ovate to broadly ovate, rarely deltate or elliptic, (1.1–)1.9–3.5 cm, lateral sessile or short-petiolate, ovate, rarely irregularly elliptic to suborbiculate, (0.7–)1.3–2.7 cm, margins incised to pinnatisect, lobes not crowded, elliptic to narrowly elliptic or lanceolate, margins often toothed, apex acute to rounded; cauline (2–)3–5, proximalmost 1–2 similar to basal; distal subtending axillary umbels 1-pinnate or 1-foliolate, short-petiolate; petiole narrow to slightly or con­spicuously dilated; leaflets or lobes few, widely separated, mostly linear, to 5 cm, margins entire or 1–2-lobed.  Umbels: terminal in fruit shallowly rounded to nearly flat, 3.5–8 cm wide; involucral bracts 0 or less often 1(–3), linear, rarely leaflike, 2–14(–25) mm; rays 5–12(–21), (1.1–)2–3.5(–4) cm; involucel bractlets 0 or 1–3(–5), linear, 2–4(–8) mm; umbellets 10–30-flowered; axillary umbels (0–)1–2(–3), alternate or rarely opposite, never observed to bear maturing fruit, peduncles variable in relative length.  Petals white (perhaps rarely pinkish).  Schizocarps broadly elliptic to elliptic, 2.6–6 mm; mericarp oil ducts 1–3 in intervals, 4–8 on commissure.  2n = 22.</description>
  <discussion>Conioselinum mexicanum is very similar to the more widely distributed C. scopulorum but is distinguished from it by both morphological and ecological character­istics.  The characteristic morphology of the distalmost leaves of C. mexicanum is not seen in C. scopulorum.  The umbel of C. scopulorum is more frequently bracteate and on average has more rays, thereby presenting a denser, less open appearance when pressed; bractlets are almost always present, usually in greater numbers, and the average number of oil ducts on the mericarps is somewhat higher.  The apiculate tips of the leaf lobes are usually brown, whereas in C. mexicanum, they are variable but often pale.  The ranges of C. scopulorum and C. mexicanum overlap in Arizona, but C. mexicanum usually occurs at more southerly latitudes and lower altitudes, and apparently flowers somewhat later.  Although some possibly intermediate specimens are seen, the two entities are best treated as distinct.  A specimen from northern Mexico with large, coarsely laciniate leaflets (to 7.3 cm in the terminal leaflets) is speculated to be a hybrid between C. mexicanum and Ligusticum porteri.</discussion>
  <description type="phenology">Flowering fall; fruiting fall.</description>
  <description type="habitat">Forests, especially pine and oak, steep slopes, moist canyon bottoms.</description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">1300–2200 m.</description>
  <description type="distribution">Ariz.; Mexico (Chihuahua, Durango, Sinaloa, Sonora).</description>
</bio:treatment>
