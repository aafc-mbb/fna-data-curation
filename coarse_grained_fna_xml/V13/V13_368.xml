<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>Kevin M. Mason</author>
    </source>
    <other_info_on_meta type="volume">13</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Lindley" date="unknown">APIACEAE</taxon_name>
    <taxon_name rank="genus" authority="Rafinesque" date="1819">CYMOPTERUS</taxon_name>
    <place_of_publication>
      <publication_title>J. Phys. Chim. Hist. Nat. Arts</publication_title>
      <place_in_publication>89: 100.  1819</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family apiaceae;genus cymopterus</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek kyma, wave, and pteron, wing, alluding to mericarp wing shape in many species</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="J. M. Coulter &amp; Rose" date="unknown">Aulospermum</taxon_name>
    <taxon_hierarchy>genus aulospermum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="J. M. Coulter &amp; Rose" date="unknown">Coloptera</taxon_name>
    <taxon_hierarchy>genus coloptera</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="Rydberg" date="unknown">Coriophyllus</taxon_name>
    <taxon_hierarchy>genus coriophyllus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="Rydberg" date="unknown">Pseudoreoxis</taxon_name>
    <taxon_hierarchy>genus pseudoreoxis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="(Torrey &amp; A. Gray) Nuttall ex J. M. Coulter &amp; Rose" date="unknown">Pteryxia</taxon_name>
    <taxon_hierarchy>genus pteryxia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="J. M. Coulter &amp; Rose" date="unknown">Rhysopterus</taxon_name>
    <taxon_hierarchy>genus rhysopterus</taxon_hierarchy>
  </taxon_identification>
  <number>27.</number>
  <other_name type="common_name">Spring-parsley</other_name>
  <other_name type="common_name">wavewing</other_name>
  <description type="morphology">Herbs perennial, slightly to strongly aromatic, acaulous, short-caulescent, or sometimes caulescent, 1–60 cm, glabrous or scabridulous, scabrous, or rarely villous, sometimes viscid, but not stellate-hairy; taproots slender to thickened, tubers absent; crown surficial or subterranean; caudex usually clothed with marcescent remnants of previous years’ leaves.  Stems: pseudoscapes present or absent.  Leaves: basal 1-ternate or 1–3-pinnate or -pinnatifid, cauline absent or similar to basal but reduced; blade lanceolate, ovate, oblong, or orbiculate, sometimes deltate to triangular, reniform, or cordate, herbaceous; leaflets undivided or ternately or pinnately dissected, narrowly elliptic to orbiculate, margins entire, toothed, or lobed.  Umbels terminal, sometimes also axillary, compound (condensed and appearing headlike in C. cinerarius, C. deserticola, C. globosus, C. megacephalus, and C. ripleyi), usually convex or globose, peripheral flowers not different; involucral bracts absent, sometimes present, linear, rarely connate into sheath, scarious; rays well developed, occasionally reduced; involucel bractlets present, sometimes absent, relatively small, not showy, green or purplish, herbaceous or scarious margined, not enveloping young umbellets.  Pedicels present, rarely absent (if absent, then not just central flower on each umbellet).  Flowers bisexual or staminate; sepals reduced to well developed; petals yellow, greenish yellow, cream, greenish white, white, pinkish, rose, or purple, apex inflexed, with a narrower appendage; stylopodium absent.  Schizocarps ovate, oblong, elliptic, suborbiculate, orbiculate, or obovate, ± dorsiventrally compressed, sometimes not compressed or laterally compressed, splitting (not splitting in C. gilmanii); mericarps not beaked, ribs usually 5, winged, sometimes abaxial or all not winged, surface smooth, granular-roughened, or scabridulous, glabrous or hairy; oil ducts 1–17 in intervals, 2–22 on commissure; carpophore absent or 2-fid.  x = 11 or 30.</description>
  <discussion>Species 37 (37 in the flora).</discussion>
  <description type="distribution">North America, n Mexico.</description>
  <discussion>Cymopterus has been defined traditionally as having fruits that are at least somewhat compressed dorsiventrally, with the abaxial as well as the lateral ribs expanded as wings.  Most members of the perennial endemic North America Apioidieae (PENA group) are placed in Cymopterus or Lomatium, the latter of which has been defined by dorsiventrally flattened fruits with only the lateral ribs expanded as wings.  Various smaller genera have also been recognized within the PENA group, characterized by fruits laterally flattened or subterete, or wingless, or with other combinations of traits not consistent with the two larger genera.  However, development of abaxial wings can vary within a species, as in C. newberryi.  Phylogenetic analyses of DNA sequence data within the PENA group indicate that fruit compression and wings—and all other traits used to classify these plants—have evolved and been lost repeatedly (S. R. Downie et al. 2002; E. E. George et al. 2014).  For example, C. douglassii, C. evertii, C. glomeratus, C. lapidosus, C. newberryi, C. ripleyi, and C. williamsii can each have abaxial wings reduced to ribs less than 0.2 mm high above the fruit surface.  In addition, Lomatium planosum, previously treated as Cymopterus planosus, has fruits with prominent abaxial wings (D. H. Mansfield et. al 2017).  Furthermore, mature fruits of C. douglassii and C. williamsii are subterete, and in C. nivalis and related taxa, they are subterete to somewhat compressed laterally.  Therefore, genera defined by these morphological traits are not natural groups.  In any case, the taxonomic placement of many species has not been adequately examined phylogenetically, and the circumscription of Cymopterus can be considered in flux and can be expected to be modified as new molecular and morphologic data are evaluated.</discussion>
  <discussion>With what is known currently, expanding Cymopterus to include many of the smaller, related genera (for example, A. Cronquist 1997) may be as justifiable as recognizing these genera.  In this treatment, Cymopterus is expanded only partially, and some smaller, related genera are recognized.  The six species with large, showy, mostly scarious, white or purple involucel bractlets that are regularly arranged and tend to form a cup around the umbellet (often treated as Cymopterus sect. Phellopterus) are placed in the genus Vesper (R. L. Hartman and G. L. Nesom 2012).  Plants with wavy or corrugated fruit wings (Pteryxia and Rhysopterus) are included in Cymopterus.  Pseudocymopterus is recognized, and five species (P. beckii, P. davidsonii, P. longiradiatus, P. macdougalii, and P. montanus) often classified in Cymopterus or Pteryxia are placed in it.  Two species from Oreoxis are placed in Cymopterus, but the other two are not, pending further study.  Aletes is held separately because its affinities are not well understood.  Plants that appear to be Cymopterus as traditionally defined but do not key satisfactorily here should be sought in Aletes, Lomatium, Pseudocymopterus, or Vesper.</discussion>
  <discussion>Some morphological characteristics found in Cymopterus that are unusual in the PENA group (but not unique to or consistent within Cymopterus) include: palmate leaves, globose umbels, and pseudoscapes.  The pseudoscape is a leafless section of stem between the top of the caudex and the level where the leaves diverge, which usually is at the surface of the soil.  In a few species, including Cymopterus glaucus, C. ibapensis, C. lapidosus, and C. longipes, the pseudoscape normally extends above ground, a condition that may occasionally be seen in a few other species.  The pseudoscape is an annual structure, whereas the caudex is perennial.  Most leaves arise in a cluster at the top of the pseudoscape, but a few may originate at the top of the caudex.  All these leaves are treated as basal, in contrast to the cauline leaves that arise at stem nodes above the ground.  We treat a stem as a pseudoscape only if more than two leaves arise clustered atop it.</discussion>
  <discussion>Some Cymopterus species are culturally significant plants to Indigenous Peoples of North America. Native Peoples throughout this continent traditionally use the leaves, roots, and fruits of certain Cymopterus species as food, condiments that add flavor or spice to other foods, or medicine for a variety of ailments (D. E. Moerman, http://naeb.brit.org).</discussion>
  <discussion>In the key and descriptions, petal color is when fresh unless noted otherwise.  Schizocarp shapes are described as seen in the plane of the commissure, regardless of the direction and degree of fruit compression.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Primary leaflets 3; leaves 1-ternate or ternate-pinnatifid.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Mericarp abaxial surfaces pubescent, commissural surfaces scaly-tomentose; mericarp oil ducts 2–3 in intervals.</description>
      <determination>31. Cymopterus ripleyi</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Mericarp surfaces glabrous or rarely scabridulous; mericarp oil ducts 1 or 3–9 in intervals.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Pseudoscapes present; leaves in single rosette at pseudoscape apex; petals white.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Involucel bractlets 1–3 mm, shorter than flowers; petiolules present on terminal leaflets; Idaho, Nevada, Oregon.</description>
      <determination>5. Cymopterus corrugatus</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Involucel bractlets 2–8 mm, usually exceeding flowers; petiolules absent, sometimes present, on terminal leaflets; Utah.</description>
      <determination>6. Cymopterus coulteri</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Pseudoscape absent or, if present, then leaves arising at slightly different heights; petals yellow, purple, or purplish white (sometimes white in C. basalticus).</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaf blade surfaces, at least adaxial, green, not glaucous.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Involucel bractlets lanceolate to oblanceolate, 0.7–4 mm wide; leaf blade ultimate segments 1–7 mm, apices usually obtuse or rounded; umbels 1.8–4.5 cm wide in flower, to 6.5 cm wide in fruit.</description>
      <determination>26. Cymopterus newberryi</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Involucel bractlets triangular to linear, 0.3–0.8 mm wide; leaf blade ultimate segments 0.8–2(–3) mm, apices broadly acute to obtuse, rarely rounded; umbels 1–2.5 cm wide in flower, to 3(–5.3) cm wide in fruit.</description>
      <determination>32. Cymopterus rosei</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaf blade surfaces gray-green, glaucous.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaf blades ovate; petals yellow; mericarp wings strongly corrugated; Uinta Basin, ne Utah, nw Colorado.</description>
      <determination>11. Cymopterus duchesnensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaf blades orbiculate-reniform; petals white, purple, or purplish white; mericarp wings planar; California, Nevada, w Utah.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaf blade ultimate segments 0.5–2 × 0.5–2 mm; pedicels absent; leaf blades 1.5–3.5 cm wide; e Nevada (White Pine County), w Utah.</description>
      <determination>3. Cymopterus basalticus</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaf blade ultimate segments (1–)2–4(–8) × (1–)3–4(–5) mm; pedicels 2–3.5(–5) mm in fruit; leaf blades 2–6 cm wide; California, s Nevada (Clark, Nye counties).</description>
      <determination>13. Cymopterus gilmanii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Primary leaflets 5–21(or 23), or if 3 then leaves 2-ternate-pinnatifid, ternate-2–3-pinnatifid, or ternate-2–3-pinnate.</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Umbels globose, dense; rays hidden in the umbel in flower or fruit, or absent; carpophore absent.</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Mericarp surfaces hairy, mericarp oil ducts 8 or 12–17 on commissure.</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Petals purple; leaf blade surfaces gray-green, glaucous; primary leaflets 5–7; mericarp oil ducts 12–17 on commissure; mericarp wings 0.3–1 mm high; California.</description>
      <determination>9. Cymopterus deserticola</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Petals white to cream; leaf blade surfaces green, not glaucous; primary leaflets 7–11; mericarp oil ducts 8 on commissure; mericarp wings 1.2–2 mm high; Arizona.</description>
      <determination>24. Cymopterus megacephalus</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Mericarp surfaces glabrous or scabridulous, mericarp oil ducts 2–13 on commissure.</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Leaf blade surfaces scabrous; primary leaflets 9–11; pseudoscapes absent.</description>
      <determination>4. Cymopterus cinerarius</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Leaf blade surfaces glabrous, sometimes viscid; primary leaflets 3–7; pseudo­scapes usually present, (0–)0.5–10 cm.</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Terminal primary leaflet tissue narrow, confluent portions 1–7(–12) mm wide; leaf blade surfaces green, not glaucous; mericarp oil ducts 3–17 in intervals, 5–13 on commissure.</description>
      <determination>16. Cymopterus glomeratus</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Terminal primary leaflet tissue broad, confluent portions (5–)10–35 mm wide; leaf blade surfaces blue-green or gray-green, glaucous; mericarp oil ducts 1 in intervals, 2–4 on commissure.</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Rays 2–10 mm in fruit; pedicels 2–3 mm in fruit; leaf blade ultimate segment apices rounded and acute on same plant; sepals 0.1 mm; mericarp wings not corky-thickened, appearing flattened, 0.8–1.2 mm high.</description>
      <determination>5. Cymopterus corrugatus</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Rays rudimentary, usually less than 2 mm in fruit; pedicels absent; leaf blade ultimate segment apices usually acute, sometimes rounded; sepals 0.5–1.1 mm; mericarp wings corky-thickened, appearing inflated, 2–2.8 mm high.</description>
      <determination>15. Cymopterus globosus</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Umbels convex, or if globose, open; rays easily visible, rarely hidden in umbel in flower or fruit; carpophore present or absent.</description>
      <next_statement_id>15</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Pseudoscapes flexuous; habitat talus.</description>
      <determination>17. Cymopterus goodrichii</determination>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Pseudoscapes absent or, if present, usually erect and habitat exposed soil, not talus.</description>
      <next_statement_id>16</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Pseudoscapes 2–16(–24) cm, leaves in single rosette of 2–20 leaves at pseudoscape apex.</description>
      <next_statement_id>17</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Carpophores absent; primary leaflets 3–7; leaves usually pinnate-pinnatifid.</description>
      <next_statement_id>18</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Terminal primary leaflet tissue broad, confluent portions (5–)10–35 mm wide; mericarp oil ducts 1 in intervals, 2–4 on commissure.</description>
      <determination>5. Cymopterus corrugatus</determination>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Terminal primary leaflet tissue narrow, confluent portions 1–7(–12) mm wide; mericarp oil ducts 3–17 in intervals, 5–13 on commissure.</description>
      <determination>16. Cymopterus glomeratus</determination>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Carpophores present; primary leaflets 5–13(–15); leaves pinnate-2-pinnatifid or 2-pinnate-pinnatifid.</description>
      <next_statement_id>19</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Mericarp abaxial wings less than 0.2 mm high; mericarp oil ducts 2–3 in intervals.</description>
      <determination>21. Cymopterus lapidosus</determination>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Mericarp abaxial wings 0.3–1.5(–2) mm high; mericarp oil ducts 3–7 in intervals.</description>
      <next_statement_id>20</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Involucral bracts present; schizocarps not compressed; mericarp abaxial wings 0.3–0.4 mm high.</description>
      <determination>14. Cymopterus glaucus</determination>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Involucral bracts absent; schizocarps dorsiventrally compressed; mericarp abaxial wings 0.8–1.5(–2) mm high.</description>
      <next_statement_id>21</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Leaves 2-pinnate-pinnatifid; primary leaflets 9–13(–15); leaf blade surfaces scabrous, ultimate segment apices usually rounded, sometimes acute; anthers purple.</description>
      <determination>19. Cymopterus ibapensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Leaves pinnate-2-pinnatifid; primary leaflets 5–9; leaf blade surfaces gla­brous, ultimate segment apices acute; anthers yellow or white.</description>
      <determination>23. Cymopterus longipes</determination>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Pseudoscapes absent or, if present, then leaves arising at slightly different heights.</description>
      <next_statement_id>22</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>22</statement_id>
      <description type="morphology">Peduncles prostrate or reclining; mericarp wings corrugated; plants 2–8 cm.</description>
      <next_statement_id>23</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>23</statement_id>
      <description type="morphology">Caudices much-branched, clothed throughout with persistent peduncles and leaf bases; leaf blades oblong; involucel bractlets linear-attenuate or linear.</description>
      <determination>1. Cymopterus aboriginum</determination>
    </key_statement>
    <key_statement>
      <statement_id>23</statement_id>
      <description type="morphology">Caudices unbranched or few-branched, clothed with fibrous remains of non­persistent peduncles and leaf bases; leaf blades orbiculate to broadly lanceolate; involucel bractlets broadly lanceolate or absent.</description>
      <determination>4. Cymopterus cinerarius</determination>
    </key_statement>
    <key_statement>
      <statement_id>22</statement_id>
      <description type="morphology">Peduncles erect, spreading, ascending, or decumbent, or if prostrate or reclining (C. evertii, C. nivalis, and C. minimus) then mericarp wings planar; plants 2–60 cm.</description>
      <next_statement_id>24</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>24</statement_id>
      <description type="morphology">Leaf blade ultimate segments (6–)10–40 mm, at least some over 25 mm; peduncles glabrous except sparsely to moderately puberulent at apex with minute peglike hairs, appearing glandular.</description>
      <determination>34. Cymopterus spellenbergii</determination>
    </key_statement>
    <key_statement>
      <statement_id>24</statement_id>
      <description type="morphology">Leaf blade ultimate segments 0.2–20(–25) mm; peduncles glabrous or completely or distally scabridulous, scabrous, or papillate.</description>
      <next_statement_id>25</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>25</statement_id>
      <description type="morphology">Leaf blades ovate or broadly triangular.</description>
      <next_statement_id>26</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>26</statement_id>
      <description type="morphology">Leaves ternate-2–3-pinnate or 2–4-pinnate; primary leaflets 3 or (9 or)11 or 13; leaf blade ultimate segments 0.1–1.2 mm wide.</description>
      <next_statement_id>27</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>27</statement_id>
      <description type="morphology">Leaf blade ultimate segments 1–20 mm, apices spinulose, callus tips 0.3–0.6 mm; sepals 0.2–0.5 mm; mericarp abaxial wings 2–3 mm high.</description>
      <determination>28. Cymopterus panamintensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>27</statement_id>
      <description type="morphology">Leaf blade ultimate segments 1–2(–4) mm, apices mucronate, callus tips 0–0.2(–0.3) mm; sepals 0.5–1.5 mm; mericarp abaxial wings 0.6–1 mm high.</description>
      <determination>35. Cymopterus terebinthinus</determination>
    </key_statement>
    <key_statement>
      <statement_id>26</statement_id>
      <description type="morphology">Leaves ternate-1–3-pinnatifid, 2-ternate-pinnatifid, or pinnate-1–2-pinnatifid; primary leaflets 3–9(–13); leaf blade ultimate segments 0.5–10 mm wide.</description>
      <next_statement_id>28</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>28</statement_id>
      <description type="morphology">Leaf blade surfaces gray-green, conspicuously glaucous; leaf blades 4–12 cm.</description>
      <next_statement_id>29</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>29</statement_id>
      <description type="morphology">Leaves pinnate-pinnatifid; leaf blade ultimate segments (1–)3–15 × (1–)3–10 mm, apices mucronate to narrowly callus-pointed, not spinulose, callus tips 0–0.2 mm; mericarp oil ducts 3–7 in intervals, 5–6 on commissure; Uinta Basin, ne Utah, nw Colorado.</description>
      <determination>11. Cymopterus duchesnensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>29</statement_id>
      <description type="morphology">29. Leaves ternate-2–3-pinnatifid; leaf blade ultimate segments 0.5–5 × 0.5–2 mm, apices spinulose, callus tips 0.1–0.4 mm; mericarp oil ducts 1 in intervals, 2 on commissure; Arizona, Nevada, sw Utah.</description>
      <determination>20. Cymopterus jonesii</determination>
    </key_statement>
    <key_statement>
      <statement_id>28</statement_id>
      <description type="morphology">Leaf blade surfaces green, not glaucous, or if pale green and slightly glaucous (C. minimus), then leaf blades 1–3.5(–4) cm.</description>
      <next_statement_id>30</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>30</statement_id>
      <description type="morphology">Leaf blade ultimate segments narrowly oblong to broadly ovate, 1–11 mm, apices usually obtuse or rounded; carpophores absent; involucel bractlets 0.7–4 mm wide.</description>
      <next_statement_id>31</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>31</statement_id>
      <description type="morphology">Terminal primary leaflet tissue narrow, confluent portions 1–7(–12) mm wide; rays 0–15 mm in fruit; pseudoscape 1.5–7(–10) cm, rarely absent.</description>
      <determination>16. Cymopterus glomeratus</determination>
    </key_statement>
    <key_statement>
      <statement_id>31</statement_id>
      <description type="morphology">Terminal leaflet tissue broad, confluent portions (5–)10–35 mm wide; rays 10–35 mm in fruit; pseudoscape absent.</description>
      <determination>26. Cymopterus newberryi</determination>
    </key_statement>
    <key_statement>
      <statement_id>30</statement_id>
      <description type="morphology">Leaf blade ultimate segments short triangular or short elliptic, 0.5–3 mm, apices broadly acute to obtuse, rarely rounded; carpophores present (sometimes deciduous); involucel bractlets 0.2–1.3 mm wide.</description>
      <next_statement_id>32</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>32</statement_id>
      <description type="morphology">Rays 20–40(–50) mm in fruit; umbels 2–4 cm wide in flower, to 10 cm wide in fruit; leaf blades 2–8(–10) cm wide.</description>
      <determination>30. Cymopterus purpureus</determination>
    </key_statement>
    <key_statement>
      <statement_id>32</statement_id>
      <description type="morphology">Rays 2–20 mm in fruit; umbels 1–2.5 cm wide in flower, to 3(–5.3) cm wide in fruit; leaf blades (1–)1.5–4.5 cm wide.</description>
      <next_statement_id>33</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>33</statement_id>
      <description type="morphology">Petals white to pinkish; pedicels 0–3 mm in fruit; Utah (Garfield, Iron, Kane counties).</description>
      <determination>25. Cymopterus minimus</determination>
    </key_statement>
    <key_statement>
      <statement_id>33</statement_id>
      <description type="morphology">Petals yellow or purple; pedicels 1–5(–7) mm in fruit; Utah (Duchesne, Garfield, Juab, Millard, Piute, Sanpete, Sevier, Wasatch, Washington counties).</description>
      <determination>32. Cymopterus rosei</determination>
    </key_statement>
    <key_statement>
      <statement_id>25</statement_id>
      <description type="morphology">Leaf blades oblong to lanceolate.</description>
      <next_statement_id>34</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>34</statement_id>
      <description type="morphology">Leaf blade ultimate segment apices usually obtuse or rounded; primary leaflets 5–7; involucel bractlets 1–4 mm wide.</description>
      <determination>16. Cymopterus glomeratus</determination>
    </key_statement>
    <key_statement>
      <statement_id>34</statement_id>
      <description type="morphology">Leaf blade ultimate segment apices usually acute; primary leaflets 5–21(–23); involucel bractlets 0.1–1.4 mm wide.</description>
      <next_statement_id>35</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>35</statement_id>
      <description type="morphology">Petals white or pinkish; primary leaflets 9–17.</description>
      <next_statement_id>36</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>36</statement_id>
      <description type="morphology">Mericarp surfaces granular-scabridulous, abaxial wings corky; carpophore absent; plants strongly aromatic with orange peel odor (Wyoming) or not aromatic (Utah).</description>
      <determination>12. Cymopterus evertii</determination>
    </key_statement>
    <key_statement>
      <statement_id>36</statement_id>
      <description type="morphology">Mericarp surfaces glabrous, abaxial wings thin; carpophore present (sometimes deciduous); plants with parsleylike odor or ± aromatic but without orange peel odor.</description>
      <next_statement_id>37</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>37</statement_id>
      <description type="morphology">Mericarps 6–11 × 4–8 mm, wings 2–2.5 mm high, corrugated; leaf blades 2.5–10 × 1–3(–4.5) cm; California, Nevada (Clark, Esmeralda, Lyon, s Nye counties).</description>
      <determination>1. Cymopterus aboriginum</determination>
    </key_statement>
    <key_statement>
      <statement_id>37</statement_id>
      <description type="morphology">Mericarps 4.5–6 × 3–4.5 mm, wings 0.2–1 mm high, planar; leaf blades 1–4(–6) × 0.3–1.5(–2) cm; Idaho, Montana, Nevada (Elko, Nye, White Pine counties), Oregon, Wyoming.</description>
      <determination>27. Cymopterus nivalis</determination>
    </key_statement>
    <key_statement>
      <statement_id>35</statement_id>
      <description type="morphology">Petals yellow, sometimes turning purple, pale yellow, or white in age; primary leaflets 5–21 (or 23).</description>
      <next_statement_id>38</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>38</statement_id>
      <description type="morphology">Primary leaflets 13–21(or 23); leaves pinnate-2-pinnatifid or 2-pinnate-pinnatifid; umbels open; rays (5–)10–80 mm in fruit.</description>
      <next_statement_id>39</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>39</statement_id>
      <description type="morphology">Involucel bractlets 4–12(–20) mm, usually exceeding fruits; leaf blade ultimate segments ovate, 3–6 × 1 mm; mericarp wings thickly membranous to corky, usually planar; plants with strong licoricelike odor.</description>
      <determination>22. Cymopterus longilobus</determination>
    </key_statement>
    <key_statement>
      <statement_id>39</statement_id>
      <description type="morphology">Involucel bractlets 2–5 mm, not exceeding fruits; leaf blade ultimate segments linear to lanceolate, 1–2(–8) × 0.5–1.2 mm; mericarp wings thin, usually corrugated; plants with turpentinelike odor.</description>
      <determination>29. Cymopterus petraeus</determination>
    </key_statement>
    <key_statement>
      <statement_id>38</statement_id>
      <description type="morphology">Primary leaflets 5–11; leaves 1–2-pinnate or pinnate-pinnatifid; umbels dense; rays 0.1–9 mm in fruit.</description>
      <next_statement_id>40</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>40</statement_id>
      <description type="morphology">Leaf blade ultimate segments linear, length/width ratio 6–12; leaves 2-pinnate.</description>
      <next_statement_id>41</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>41</statement_id>
      <description type="morphology">Petioles with thickened basal sheaths; Crawford Mountains, n Utah (Rich County).</description>
      <determination>7. Cymopterus crawfordensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>41</statement_id>
      <description type="morphology">Petioles usually lacking thickened basal sheaths; Arizona, Colorado, New Mexico, s Utah (San Juan County), Wyoming.</description>
      <determination>33. Cymopterus sessiliflorus</determination>
    </key_statement>
    <key_statement>
      <statement_id>40</statement_id>
      <description type="morphology">Leaf blade ultimate segments narrowly to broadly elliptic or ovate, rarely oblanceolate to oblong, length/width ratio 0.2–5; leaves 1-pinnate or pinnate-pinnatifid.</description>
      <next_statement_id>42</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>42</statement_id>
      <description type="morphology">Sepals 0.6–1.2 mm; peduncles scabrous or glabrous distally, always scabrous if sepals less than 0.9 mm.</description>
      <next_statement_id>43</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>43</statement_id>
      <description type="morphology">Caudices clothed with persistent, ± flattened leaf bases; involucel bractlets 0.6–1.3 mm wide; leaf blades 0.2–0.6(–1.8) cm wide; umbels 0.5–1.2 cm wide; proximal primary leaflets 2–9 × 2–5 mm; elevation 2200–3700 m.</description>
      <determination>2. Cymopterus alpinus</determination>
    </key_statement>
    <key_statement>
      <statement_id>43</statement_id>
      <description type="morphology">Caudices clothed with persistent, terete leaf bases; involucel bractlets 0.3–0.5 mm wide; leaf blades 0.7–2.6 cm wide; umbels (0.5–)1–2.8 cm wide; proximal primary leaflets 6–20 × (1–)4–15 mm; elevation 1400–2300 m.</description>
      <next_statement_id>44</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>44</statement_id>
      <description type="morphology">Leaf blade surfaces green, ultimate segments narrowly elliptic, rarely oblanceolate to oblong; involucel bractlets distinct or connate to middle; Arizona, Colorado, New Mexico, Utah (San Juan County), Wyoming.</description>
      <determination>33. Cymopterus sessiliflorus</determination>
    </key_statement>
    <key_statement>
      <statement_id>44</statement_id>
      <description type="morphology">Leaf blade surfaces blue-green, ultimate segments broadly elliptic, sometimes narrowly elliptic; involucel bractlets distinct or nearly so; Utah (Garfield and Grand counties).</description>
      <determination>36. Cymopterus trotteri</determination>
    </key_statement>
    <key_statement>
      <statement_id>42</statement_id>
      <description type="morphology">Sepals 0–0.7 mm; peduncles glabrous.</description>
      <next_statement_id>45</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>45</statement_id>
      <description type="morphology">Leaf blade ultimate segments crowded, ovate, 0.2–4 × 1–2(–3) mm, length/width ratio 0.2–1.3, uniformly sized; Idaho (Custer and Lemhi counties).</description>
      <determination>10. Cymopterus douglassii</determination>
    </key_statement>
    <key_statement>
      <statement_id>45</statement_id>
      <description type="morphology">Leaf blade ultimate segments not crowded, narrowly to broadly elliptic, 3–12 × 1–3 mm, length/width ratio 2–5, variably sized; Idaho (including Custer and Lemhi counties), Montana, Wyoming.</description>
      <next_statement_id>46</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>46</statement_id>
      <description type="morphology">Involucel bractlets connate 1/4–3/4 length, obovate, 0.6–1.2 mm wide; leaf blade ultimate segment callus tips 0.1–0.2 mm; mericarp oil ducts 1 in intervals, 2 on commissure; Wyoming.</description>
      <determination>37. Cymopterus williamsii</determination>
    </key_statement>
    <key_statement>
      <statement_id>46</statement_id>
      <description type="morphology">Involucel bractlets distinct, linear to lanceolate, 0.1–0.4 mm wide; leaf blade ultimate segment callus tips 0–0.1 mm; mericarp oil ducts 3–9 in intervals, 4–10 on commissure; Idaho, Montana.</description>
      <next_statement_id>47</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>47</statement_id>
      <description type="morphology">Leaf blade surfaces blue-green, glaucous; proximal primary leaflets 5–8(–10) mm wide; mericarp surfaces glabrous or minutely granular roughened, wings thin, lateral ones 0.5–1.1 mm high; involucel bractlets 1–3.2(–4), not exceeding fruits; Idaho (Cassia County).</description>
      <determination>8. Cymopterus davisii</determination>
    </key_statement>
    <key_statement>
      <statement_id>47</statement_id>
      <description type="morphology">Leaf blade surfaces green, not glaucous; proximal primary leaflets 1–2 mm wide; mericarp surfaces pubescent, wings corky to indurate, lateral ones 0.4–0.5 mm high; involucel bractlets 2–5 mm, usually exceeding fruits; Idaho (Custer and Lemhi counties), Montana.</description>
      <determination>18. Cymopterus hendersonii</determination>
    </key_statement>
  </key>
</bio:treatment>
