<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">13</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Lindley" date="unknown">APIACEAE</taxon_name>
    <taxon_name rank="genus" authority="Rafinesque" date="1819">LOMATIUM</taxon_name>
    <taxon_name rank="species" authority="Evert" date="1983">attenuatum</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>30: 143, fig. 1.  1983</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family apiaceae;genus lomatium;species attenuatum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>4.</number>
  <other_name type="common_name">Taper-tip desert-parsley</other_name>
  <description type="morphology">Herbs green, usually caulescent, rarely short-caulescent, (5–)10–25 cm, scabrous; caudex simple or several-branched, usually without persistent leaf bases, without persistent peduncles; taproot slender to thickened, lacking a shallow, globose tuber.  Pseudoscapes subterranean.  Leaves arising at slightly different heights, not forming just 1 rosette, green, 3-pinnate or ternately 3-pinnate, not shiny; petiole sheathing basally to entire length, glabrous, scarious; blade triangular to ovate or orbiculate, (2–)3–11 × 1.5–10 cm, surfaces glabrous, rachis glabrous proximally; leaflets not overlapping; penultimate segments narrow, usually less than 2 mm wide, ultimate segments 200–1000, ovate to oblan­ceolate, (0.3–)2–5(–7) × 0.5–1.5 mm, length/width ratio 1–3.5(–5), relatively firm, ± thick to ± thin, margins entire, apex usually rounded to obtuse, sometimes acute, callus tips 0 mm, terminal segment 2–3 mm; cauline leaves 1–2(–3), with more than 5 ultimate segments, similar to basal leaves.  Peduncles 1–3(–4) per plant, 1–3 per stem, erect, not inflated, (3–)10–16 cm, exceeding leaves in fruit, 1–2 mm wide 1 cm below umbel, scaberulous distally.  Umbels 1–2.5 cm wide in flower, 3.4–8(–14) cm wide in fruit, rays 3–12(–20), spreading, (1.5–)3–5 cm in fruit, unequal, glabrous, scabrous, or densely scabrous; involucel bractlets (0–)4–7, distinct, linear-lanceolate, 1–4 mm, shorter than or subequal to flowers, margins broadly scarious, not ciliate, entire, glabrous.  Fruiting pedicels 3–8(–10) mm, shorter than fruit.  Flowers: petals yellow, glabrous; anthers yellow; ovary and fruit glabrous or slightly scabrous on ribs when young, glabrous and glossy when mature.  Meri­carps dorsiventrally compressed, oblong-elliptic, glossy, 5–8 × 3–5 mm, length/width ratio 1.5–3; wings 0.4–0.8 mm wide 15–40% of body width, paler than body; abaxial ribs not raised; apex rounded to truncate; oil ducts 1 in intervals, 2(–3) on commissure, prominent.</description>
  <discussion>Lomatium attenuatum occurs in and along the border of the Absaroka Mountains of Park County, Wyoming, with disjunct populations in Beaverhead and Madison counties, Montana.  This species resembles L. cous in habit, leaf dissection, and fruit size and shape, and the two have overlapping ranges.  However, L. cous has conspicuous, broad involucel bractlets, whereas L. attenuatum bractlets are usually absent or, if present, inconspicuous and slender.  In addition, L. attenuatum has longer pedicels and a different distribution of oil ducts, and is more strongly scabrous.  It resembles L. canbyi and L. vaginatum in these features, but it is allopatric to them.  Leaves of L. attenuatum have a celery odor.  Its habitat dries out early in the year, so L. attenuatum has a brief growing season and then goes dormant.  The greatest threats to it seem to be the few introduced plants that can survive in its habitat.  Past mining has eliminated some of its habitat (J. P. Vanderhorst and B. L. Heidel, 1998).</discussion>
  <description type="phenology">Flowering May–Jun; fruiting Jun–Aug.</description>
  <description type="habitat">Semiforested scree, shrublands, woodlands, grasslands, often exposed sites dominated by bluebunch wheatgrass or spike fescue, sometimes extending into big sagebrush or low sagebrush communities, dry, loose, gravelly soils derived from limestone or igneous rocks.</description>
  <description type="elevation">1600–2800 m.</description>
  <description type="distribution">Mont., Wyo.</description>
</bio:treatment>
