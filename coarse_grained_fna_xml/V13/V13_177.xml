<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">13</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="unknown" date="unknown" rank="family">Escalloniaceae</taxon_name>
    <taxon_name rank="genus" authority="Mutis ex Linnaeus f." date="1782">ESCALLONIA</taxon_name>
    <taxon_name rank="species" authority="(Ruiz &amp; Pavon) Persoon" date="1805">rubra</taxon_name>
    <place_of_publication>
      <publication_title>Syn. Pl.</publication_title>
      <place_in_publication>1: 235.  1805</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family escalloniaceae;genus escallonia;species rubra</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Stereoxylon</taxon_name>
    <taxon_name rank="species" authority="Ruiz &amp; Pavon" date="1802">rubrum</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Peruv.</publication_title>
      <place_in_publication>3: 15, plate 236, fig. b.  1802</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus stereoxylon;species rubrum</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Redclaws</other_name>
  <description type="morphology">Plants (30–)80–100(–150) cm.  Twigs densely hairy and stipitate-glandular, especially distally, stalked glands often 2–3 times as long as nonglandular hairs, sometimes bearing spreading hairs along stalk or, rarely, glabrescent.  Leaf blades 13–75 × 4–34 mm, viscid, base tapering to cuneate, margins serrate to doubly serrate, rarely revolute, apex acute to obtuse, abaxial surface dull, glabrous or hairy along veins, glandular-punctate, adaxial surface shiny, glabrous or hairy along impressed midvein, not glandular-punctate.  Inflorescences 4–40-flowered, 5–10 cm.  Pedicels 3–9(–18) mm; bracteoles absent or 1–3 × 0.1–0.3 mm, margins glandular.  Flowers: hypanthium campanulate, 2.2–5.5(–8) × 2.3–6 mm, somewhat coriaceous, densely stipitate-glandular and hairy proximally, free portion glabrous, sparsely hairy, or sparsely stipitate-glandular and sparsely hairy; calyx lobes 1.5–3.5 × 1–2 mm, margins entire, sparsely glandular, sometimes sparsely ciliate proximally, apex acute; petals pink to pinkish red, sometimes paler adaxially, linear-spatulate, 10–16 mm, claw erect, linear, 7–10 × 1–2 mm, limb spreading, obovate or round, 3–6 × 2.6–3.5 mm, margins obscurely undulate; filaments white, flattened, 8–10 mm; anthers 2–2.8 × 0.5–0.9 mm; style 8–11mm; stigma 0.9–1.7 mm diam.  Seeds brown or reddish brown, 0.6–1 mm.  2n = 24.</description>
  <discussion>H. Sleumer (1968) recognized five varieties within Escallonia rubra.  Horticultural plants from North America are often referred to var. macrantha (Hooker &amp; Arnott) Reiche, with broadly elliptic to obovate leaves, glabrous leaf margins, and glandular-pubescent hypanthia.  Analyses of morphologic data suggest that characters used to distinguish the varieties vary freely and that E. rubra is best treated as a morphologically variable complex (F. Zapata 2010).  The range of morphologic variation in this species overlaps with that in other, morphologically similar species (for example, E. rosea Grisebach).  However, a combination of morphological, molecular, and bioclimatic data suggests that E. rubra is a distinct species (Zapata; but see also S. J. Jacobs etal. 2021).</discussion>
  <discussion>P. F. Zika etal. (2000) were the first to report Escallonia rubra from a clearly naturalized population in North America.  First gathered along a lakeshore in Coos County in 1997, the species has now also been collected in Curry, Lincoln, and Tillamook counties.  It is not clear whether collections from California are from naturalized plants.</discussion>
  <description type="phenology">Flowering Jul–Oct.</description>
  <description type="habitat">Roadsides.</description>
  <description type="elevation">0–20 m.</description>
  <description type="distribution">Introduced; Oreg.; South America (Argentina, Chile)</description>
</bio:treatment>
