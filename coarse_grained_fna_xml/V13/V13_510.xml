<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>Geoffrey A. Levin</author>
    </source>
    <other_info_on_meta type="volume">13</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Lindley" date="unknown">APIACEAE</taxon_name>
    <taxon_name rank="genus" authority="Greene" date="1891">LILAEOPSIS</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>2: 192.  1891</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family apiaceae;genus lilaeopsis</taxon_hierarchy>
    <other_info_on_name type="etymology">Genus Lilaea and Greek -opsis, resembling</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name rank="genus" authority="Nuttall" date=" 1818">Crantzia</taxon_name>
    <place_of_publication>
      <publication_title>Gen. N. Amer. Pl.</publication_title>
      <place_in_publication>1: 177.  1818</place_in_publication>
      <other_info_on_pub>not Scopoli 1777 [Gesneriaceae]</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus crantzia</taxon_hierarchy>
  </taxon_identification>
  <number>43.</number>
  <other_name type="common_name">Grasswort</other_name>
  <description type="morphology">Herbs perennial, not aromatic, acaulous, [0–]0.2–6.5(–9)[–11] cm, glabrous; rhizomes creeping, slender.  Leaves: basal simple (reduced to rachis), cauline absent; blade spatulate, linear, long attenuate, or filiform, herbaceous to subsucculent, hollow, transversely septate, margins entire.  Umbels axillary, simple, ± globose, convex, or flat-topped, peripheral flowers not different; involucral bracts ovate to lanceolate, herbaceous.  Pedicels present.  Flowers bisexual; sepals reduced; petals white, pale green, pink, or maroon, apex spreading, not appendaged; stylopodium rounded to low-conic.  Schizocarps ellipsoid, ovoid, turbinate, globose, or obovoid, slightly laterally compressed, splitting; mericarps not beaked, ribs 5, abaxial prominent to inconspicuous, lateral prominently thickened, surface smooth, glabrous; oil ducts 1 in intervals, sometimes absent in 1 or more intervals, 2–5[–16] on commissure; carpophore absent.  x = 11.</description>
  <discussion>Species 13 (4 in the flora).</discussion>
  <description type="distribution">North America, Mexico, West Indies (Hispaniola), South America, Indian Ocean Islands (Kerguelen, Madagascar, Mauritius), Pacific Islands (New Zealand), Australia; introduced in Europe (Iberian Peninsula).</description>
  <discussion>Lilaeopsis plants are aquatic or grow on wet soil, and are characterized by simple, linear to spatulate, septate leaves and simple, few-flowered umbels.  The lateral, and sometimes the abaxial, ribs are filled with spongy tissue that makes the fruits buoyant, presumably aiding their dispersal by water (J. M. Affolter 1985).  Several species, including L. carolinensis and L. chinensis from the flora area, are grown as aquarium plants.</discussion>
  <references>
    <reference>Affolter, J. M.  1985.  A monograph of the genus Lilaeopsis (Umbelliferae).  Syst. Bot. Monogr. 6: 1–140.</reference>
    <reference>Bone, T. S et al.  2011.  A phylogenetic and biogeographic study of the genus Lilaeopsis (Apiaceae tribe Oenantheae).  Syst. Bot. 36: 789–805.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades elliptic in cross section proximally, strongly flattened distally; leaves borne only at nodes of horizontal rhizomes, rarely at apices of vertical branches; e North America.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Mericarp ribs: abaxial broadly triangular, all 5 ribs filled with spongy tissue; leaf blades (22–)80–300(–520) mm, much longer than peduncles, septa (7–)10–20.</description>
      <determination>1. Lilaeopsis carolinensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Mericarp ribs: abaxial narrowly winged, only 2 lateral ribs filled with spongy tissue; leaf blades 10–85(–100) mm, shorter than to a little longer than peduncles, septa (4–)8–10.</description>
      <determination>2. Lilaeopsis chinensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades round or elliptic in cross section throughout; leaves borne at nodes of horizontal rhizomes and usually at apices of vertical branches; w North America.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Mericarp ribs: lateral broadly triangular, abaxial obscure, sometimes narrowly winged, only 2 lateral ribs filled with spongy tissue; petals white, pale green, or pink; British Columbia, California, Oregon, Washington.</description>
      <determination>3. Lilaeopsis occidentalis</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Mericarp ribs: lateral rounded, abaxial rounded or broadly triangular, all 5 ribs filled with spongy tissue; petals maroon; Arizona.</description>
      <determination>4. Lilaeopsis schaffneriana</determination>
    </key_statement>
  </key>
</bio:treatment>
