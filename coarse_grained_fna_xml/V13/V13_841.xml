<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>Larry J.Toolin†, Thomas F.Daniel</author>
    </source>
    <other_info_on_meta type="volume">13</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">RUTACEAE</taxon_name>
    <taxon_name rank="genus" authority="Kunth in A.von Humboldt etal." date="1823">CHOISYA</taxon_name>
    <place_of_publication>
      <publication_title>Nov. Gen. Sp.</publication_title>
      <place_in_publication>6(fol.): 4; 6(qto.): 4, plate 513.  1823</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rutaceae;genus choisya</taxon_hierarchy>
    <other_info_on_name type="etymology">For Jacques Denis Choisy, 1799–1859, Swiss botanist</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="Torrey &amp; A.Gray" date="unknown">Astrophyllum</taxon_name>
    <taxon_hierarchy>genus astrophyllum</taxon_hierarchy>
  </taxon_identification>
  <number>3.</number>
  <other_name type="common_name">Mexican orange</other_name>
  <other_name type="common_name">starleaf</other_name>
  <description type="morphology">Shrubs evergreen, aromatic, variably glandular, pubescence appressed to spreading to erect.  Stems unarmed.  Leaves opposite or subopposite, compound, palmately 3–12-foliolate or ± pinnately 3-foliolate; petiolate; leaflets 3–12, sessile, linear or nearly so, margins ± prominently punctate-glandular and sinuate-crenate to sinuate-denticulate, thickened, subrevolute to plane, apex glandular-emarginate, surfaces glabrate to appressed or erect hairy.  Inflorescences of solitary flowers (or short panicles) in axils of leaves and/or triangular bracts, panicle branches bracteolate, flower stalks (1–)2(–4)-bracteolate along length.  Flowers bisexual, radially symmetric; sepals caducous to persistent, (4–)5, imbricate, distinct; petals (4–)5, distinct, white, obovate to elliptic, oblanceolate, clawed toward base, glabrous, translucent glands few, obscure; disc inconspicuous; stamens (8–)10, in 2 series, free, inner antipetalous and generally shorter than antisepalous outer series; filaments attached at base of disc, subulate to lanceolate, apex attenuate; anthers dorsifixed, 2-celled, oval to oblong or sagittate by divergence of cells below bases of short connectives, longitudinally and introrsely dehiscent; pistil 1, carpels basally connate, hirsute, each with an apical horn, connivent around style; ovary 5-lobed or with lobes nearly distinct hemispheric to subglobose, lobed; style 1 (at least distally), but sometimes consisting of 5 connivent and often twisted styles proximally, base often persistent; stigmas not persistent, 1, capitate, 5-lobed; ovules 2 per locule.  Fruits schizocarps, greenish sometimes with a brownish red discoloration distally, strigose and punctate-glandular; maturing mericarps 1–3(–5), dehiscing introrsely along a single suture from base to a subapical or dorsal horn.  Seeds dark brown to black, subreniform, inconspicuously longitudinally striate to subreticulate.  x = 7 or 9?</description>
  <discussion>Species 7 (3 in flora).</discussion>
  <description type="distribution">sw, sc United States, Mexico.</description>
  <discussion>J. Torrey and A.Gray described the genus Astrophyllum, based on A.dumosum.  A.Gray (1888) synonymized Astrophyllum within Choisya, where it has been retained in all subsequent taxonomic work on the genus.  C.H. Muller (1940) recognized two subgenera within Choisya based primarily on leaf characters: subg. Choisya (as Euchoisya), comprising two species endemic to Mexico (C. neglecta C.H. Muller and C.ternata Kunth), and subg. Astrophyllum (Torrey) C.H. Muller, comprising five species, among which are the three species occurring in the flora area.  Contemporary phylogenetic studies place Choisya within the subfamily Amyridoideae (C.M.Morton and C.Telmer 2014).</discussion>
  <discussion>L. Benson and R.Darrow (1981) treated Choisya arizonica and C.mollis as varieties of C.dumosa, which L.J. Toolin (2014) rejected.  While C.ternata has been widely cultivated since perhaps the establishment of Spanish rule in Mexico, the hybrid between that species and C.arizonica (C. × dewitteana Geerinck) and other species of Choisya (for example, C.mollis; G.Starr, pers. comm.) are now utilized horticulturally.</discussion>
  <discussion>Potential basic chromosome numbers of 7 and 9 are based on the three published counts of 2n = 56 for C.arizonica, C.dumosa, and C.mollis by J.R. Reeder (2001b), and the indication of 2n = 54 on vouchers at ARIZ for these counts.  Lacking other counts in the genus, this contradiction remains unresolved.</discussion>
  <references>
    <reference>Muller, C.H.  1940.  A revision of Choisya.  Amer. Midl. Naturalist 24: 729–742.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaflets (4–)7–12, 10–35(–40) × 1.5–2.5 mm, linear; petiole lengths (0.3–)0.6–0.9(–1.3) times leaflet lengths, glands very prominent, raised to 0.7 mm, with most higher than 0.3 mm; w Texas, New Mexico.</description>
      <determination>1. Choisya dumosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaflets 3–5, (12–)20–75 × 1–4.2(–5) mm, linear to narrowly elliptic; petiole lengths 0.1–0.5(–0.6) times leaflet lengths, glands not prominent, raised to 0.3 mm; se Arizona.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems with antrorse to antrorsely appressed hairs (strigose); leaflet surfaces sparsely strigose, particularly along adaxial midrib, to glabrate.</description>
      <determination>2. Choisya arizonica</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems with spreading (usually erect) to antrorse hairs (puberulent); leaflet surfaces ± densely puberulent throughout.</description>
      <determination>3. Choisya mollis</determination>
    </key_statement>
  </key>
</bio:treatment>
