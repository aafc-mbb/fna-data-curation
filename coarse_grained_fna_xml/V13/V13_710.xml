<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>Guy L. Nesom</author>
    </source>
    <other_info_on_meta type="volume">13</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Lindley" date="unknown">APIACEAE</taxon_name>
    <taxon_name rank="genus" authority="de Candolle" date="1829">POLYTAENIA</taxon_name>
    <place_of_publication>
      <publication_title>Coll. Mém.</publication_title>
      <place_in_publication>5: 53, plate 13.  1829</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family apiaceae;genus polytaenia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="etymology">Greek polys, many, and taenia, ribbon or band, alluding to oil tubes</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="H. St. John" date="unknown">Phanerotaenia</taxon_name>
    <taxon_hierarchy>genus phanerotaenia</taxon_hierarchy>
  </taxon_identification>
  <number>59.</number>
  <other_name type="common_name">Prairie-parsley</other_name>
  <description type="morphology">Herbs perennial, not aromatic, caulescent, 50–150 cm, glabrous except inflorescence moderately to densely scabrous; taproots thickened.  Leaves: basal 1–2-pinnate, cauline 1-pinnate; blade oblong to ovate, herbaceous and slightly thickened; leaflets dissected, ovate to oblanceolate or oblong, margins toothed.  Umbels terminal and axillary, compound, convex, peripheral flowers not different; involucral bracts usually absent; involucel bractlets present.  Pedicels present.  Flowers bisexual; sepals well developed; petals yellow to yellow-green or white, apex inflexed, with a narrower appendage; stylopodium absent.  Schizocarps broadly ovate to orbiculate or obovate, strongly dorsiventrally compressed, splitting; mericarps not beaked, ribs 5, abaxial filiform to obscure, lateral corky-winged, surface smooth, glabrous; oil ducts 1–3 in intervals, 2 or 4 on commissure; carpophore 2-fid.  x = 11.</description>
  <discussion>Species 3 (3 in the flora).</discussion>
  <description type="distribution">c, e United States.</description>
  <discussion>The name Pleiotaenia J. M. Coulter &amp; Rose 1909 was a superfluous substitute name for Polytaenia de Candolle, based on the supposition that Polytaenia could be mistaken for the slightly earlier Polytaenium Desvaux 1827.</discussion>
  <references>
    <reference>Nesom, G. L.  2012c.  Taxonomy of Polytaenia (Apiaceae): P. nuttallii and P. texana.  Phytoneuron 2012-66: 1–11.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals white; involucel bractlets narrowly lanceolate, sometimes absent, mostly 1–3 mm; mature mericarps (9–)11–15 × (6–)7–9.5 mm.</description>
      <determination>3. Polytaenia albiflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals yellow to yellow-green; involucel bractlets linear to filiform, mostly 3–5 mm; mature mericarps 5–11(–15) × 4–7 mm.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Lateral wings of mature mericarps distinctly thickened, thicker than face; abaxial oil ducts (6–)8–10, covered by epidermis and pericarp and indistinct; commissural oil ducts 4, 2 on each side of midrib.</description>
      <determination>1. Polytaenia nuttallii</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Lateral wings of mature mericarps relatively thin, same thickness as face; abaxial oil ducts usually 4, sometimes 6, raised and distinct; commissural oil ducts 2, 1 on each side of midrib.</description>
      <determination>2. Polytaenia texana</determination>
    </key_statement>
  </key>
</bio:treatment>
