<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">13</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Lindley" date="unknown">APIACEAE</taxon_name>
    <taxon_name rank="genus" authority="Rafinesque" date="1819">LOMATIUM</taxon_name>
    <taxon_name rank="species" authority="(M. E. Jones) J. A. Alexander &amp; Whaley" date="2018">depauperatum</taxon_name>
    <place_of_publication>
      <publication_title>J. Bot. Res. Inst. Texas</publication_title>
      <place_in_publication>12: 406.  2018</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family apiaceae;genus lomatium;species depauperatum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Cogswellia</taxon_name>
    <taxon_name rank="species" authority="M. E. Jones" date="unknown">millefolia</taxon_name>
    <taxon_name rank="variety" authority="M. E. Jones" date="1908">depauperata</taxon_name>
    <place_of_publication>
      <publication_title>Contr. W. Bot.</publication_title>
      <place_in_publication>12: 38.  1908</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus cogswellia;species millefolia;variety depauperata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Lomatium</taxon_name>
    <taxon_name rank="species" authority="(J. M. Coulter &amp; Rose) J. M. Coulter &amp; Rose" date="unknown">grayi</taxon_name>
    <taxon_name rank="variety" authority="(M. E. Jones) Mathias" date="unknown">depauperatum</taxon_name>
    <taxon_hierarchy>genus lomatium;species grayi;variety depauperatum</taxon_hierarchy>
  </taxon_identification>
  <number>23.</number>
  <other_name type="common_name">Milfoil lomatium</other_name>
  <description type="morphology">Herbs green, acaulous, 10–60 cm, glabrous; caudex simple or multicipital, with persistent leaf sheaths forming thatch of a few loose fibers, with many persistent, gray pedun­cles; taproot stout, irregularly thickened, lacking dis­tinct tuberlike swellings.  Pseudoscapes absent.  Leaves arising at slightly different heights, not forming just 1 rosette, green, usually ternate-2-pinnate-pinnatifid; peti­ole sheathing basally to entire length; blade triangular to rhombic, 4.5–14 × 2.3–5.9 cm, surfaces glabrous; apical 2–3 pinnule pairs of secondary leaflets with naked intercostal region between rachilla and basalmost pinnule lobes, appearing like petiolule; ultimate seg­ments 300–1000, very narrowly linear, subterete in cross section, 1–4.5 × 0.3–0.7 mm, well separated, not or little overlapping, diffuse, not obscuring elongate intercostal areas along rachillae (best assessed from at least a foot distant from plant); margins entire, apex acuminate, callus tips 0.1–0.2, terminal segment 1–5 mm; cauline leaves 0.  Peduncles 1–10+ per plant, 1 per stem, ascending to erect, not inflated, 9–33 cm, exceeding leaves, 0.5–2 mm wide 1 cm below umbel, glabrous.  Umbels 0.9–2.5 cm wide in flower, 4–9 cm wide in fruit, rays 5–15, ascending to spreading, 0.5–6 cm in fruit, subequal, glabrous; involucel bractlets (0 or)2–10, connate basally, linear or lanceolate, 2–5 mm, longer than flowers, margins usually scarious, not cili­ate, entire, glabrous.  Fruiting pedicels 4–11(–13 mm, shorter than fruit.  Flowers: petals yellow, glabrous; anthers yellow or ochroleucous; ovary and young fruit glabrous.  Mericarps dorsiventrally compressed, oblong or elliptic, 5–9(–10) × 2.5–5 mm, length/width ratio 1.3–2.5; wings 0.7–1.4 mm wide, 33–75% of body width, paler than body; abaxial ribs slightly raised; apex rounded, truncate; oil ducts 1–2 in intervals, 2–6 on commissure.</description>
  <discussion>Lomatium depauperatum grows in western Utah and eastern Nevada (eastern Elko and White Pine counties); its range somewhat overlaps that of L. grayi in west-central Utah, especially in and near eastern Juab County.  Lomatium depauperatum has fewer, smaller, leaflet clusters than L. grayi.  Where they grow together, L. depauperatum can also be distinguished by its flat, dorsiventrally compressed ultimate leaf segments and by the thick thatch on the caudex, which is formed mainly of persistent peduncles.  The sheaths of the basal leaves are narrow and leathery without the broad, scarious margins typical of L. grayi.  In L. grayi, ultimate leaf segments are subterete in cross section, just slightly dorsiventrally compressed.  The thatch covering the caudex branches is dense and fibrous, formed from the broad, scarious basal leaf sheaths.  Lomatium depauperatum can easily be confused with Cymopterus terebinthinus when in flower.  The latter has narrowly triangular calyx teeth 0.5–1 mm.  Those of L. depauperatum are less than 0.2 mm and broadly triangular.</discussion>
  <description type="phenology">Flowering May–Jun; fruiting late May–Jul.</description>
  <description type="habitat">Desert scrub, pinyon-juniper woodlands, mountain brush scrub, often with sagebrush or shadscale.</description>
  <description type="elevation">1500–2900 m.</description>
  <description type="distribution">Nev., Utah.</description>
</bio:treatment>
