<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">13</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="R. Brown" date="unknown">ANACARDIACEAE</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">RHUS</taxon_name>
    <taxon_name rank="subgenus" authority="(Rafinesque) Torrey A. Gray" date="1838">Lobadium</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Amer.</publication_title>
      <place_in_publication>1: 219. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family anacardiaceae;genus rhus;subgenus lobadium</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name rank="genus" authority="Rafinesque" date=" 1819">Lobadium</taxon_name>
    <place_of_publication>
      <publication_title>Amer. Monthly Mag. &amp; Crit. Rev.</publication_title>
      <place_in_publication>4: 357.  1819</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus lobadium</taxon_hierarchy>
  </taxon_identification>
  <number>6b.</number>
  <description type="morphology">Shrubs or trees, not branching dichotomously, rarely clonal via rhizomes and forming clumps (R. aromatica); thorns usually absent (often present in R. microphylla).  Leaves persistent or deciduous, simple, 1-foliolate, imparipinnate, or 3-foliolate; blade margins entire, crenate, serrate, or lobed.  Inflorescences terminal and/or axillary, erect or pendulous, panicles, not pyramidal, terminal branches pseudospicate.  Pedicels usually (0–)1–3.5[–6] mm.  Drupes globose or lenticular.</description>
  <discussion>Species ca. 23 (6 in the flora).</discussion>
  <description type="distribution">North America, n Mexico, Central America.</description>
  <references>
    <reference>Young, D. A.  1978.  Reevaluation of the sections of Rhus L. subgenus Lobadium (Raf.) T. &amp; G. (Anacardiaceae).  Brittonia 30: 411–415.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves compound.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaflets.</description>
      <determination>35. Rhus aromatica</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaflets (3–)5–9.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Thorns often present; rachises winged; flowers appearing before, sometimes with, leaf flush.</description>
      <determination>8. Rhus microphylla</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Thorns absent; rachises not winged; flowers appearing while leaves are present.</description>
      <determination>10. Rhus virens</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves simple or 1-foliolate (2–3-lobed or 3-foliolate).</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaves deciduous, blade margins 3-lobed, sometimes 5-lobed, or entire proximally and crenate or dentate distally; petals yellow to pinkish; flowers emerging before leaves or with leaf flush.</description>
      <determination>5. Rhus aromatica</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaves persistent to tardily deciduous, blade margins entire or serrate (occasionally R.integrifolia and hybrids 1–3-lobed); petals white to pink; appearing after leaf flush.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaves conduplicate, blade margins entire.</description>
      <determination>9. Rhus ovata</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaves flat to undulate, blade margins entire or serrate.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaf surfaces shedding waxy cuticle in dried specimens, abaxial surfaces glabrous to very sparsely hairy with eglandular hairs, bases rounded to cuneate.</description>
      <determination>6. Rhus integrifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaf surfaces not shedding waxy cuticle in dried specimens, abaxial surfaces sparsely to densely golden- to red-glandular, bases cordate.</description>
      <determination>7. Rhus kearneyi</determination>
    </key_statement>
  </key>
</bio:treatment>
