<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">13</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Lindley" date="unknown">APIACEAE</taxon_name>
    <taxon_name rank="genus" authority="Rafinesque" date="1819">CYMOPTERUS</taxon_name>
    <taxon_name rank="species" authority="S. Watson" date="1871">longipes</taxon_name>
    <place_of_publication>
      <publication_title>Botany (Fortieth Parallel),</publication_title>
      <place_in_publication>124.  1871</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family apiaceae;genus cymopterus;species longipes</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>23.</number>
  <other_name type="common_name">Sprawling spring-parsley</other_name>
  <description type="morphology">Herbs not aromatic, short-caulescent, 5–35 cm; crown sub­terranean; caudex unbranched or few-branched, clothed with persistent leaf bases.  Pseudo­scapes usually erect, 4–16(–24) cm, usually aerial, sometimes partially subterranean.  Leaves in single rosette of 4–20 leaves at pseudoscape apex, pinnate-2-pinnatifid; petiole 1–5 cm; blade ovate to ovate-oblong, 1.5–8 × 1–5(–7.5) cm, surfaces pale gray-green, slightly glaucous, glabrous; primary leaflets 5–9, proximal ones 3–48 × 2–18 mm; terminal leaflet often greatly confluent with the rachis; ultimate segments ovate or elliptic, 0.5–2(–2.5) × 0.8–1.5 mm, margins entire, apex acute, mucronulate, callus tip 0–0.2 mm.  Peduncles erect to spreading, 0.4–2.5 cm in flower, to 26 cm in fruit, glabrous.  Umbels dense in flower, open in fruit, convex, 1.3–1.8 cm wide in flower, to 4.8 cm wide in fruit; involucral bracts absent; rays visible, rarely hidden in umbel in flower or fruit, 3–15(–30) mm in fruit; involucel bractlets usually distinct, linear to lanceolate, 2.2–3.2 × 0.2–0.6 mm, shorter than flowers.  Pedicels 3–8 mm in fruit.  Flowers: sepals rounded, to 0.2 mm; petals yellow or white; anthers yellow or white.  Schizocarps oblong to ovate-oblong, dorsiventrally compressed; mericarps 5–8 × 3–4(–6) mm, surfaces glabrous; lateral wings thin, 0.3–4 mm high, planar to slightly corrugated, abaxial wings thin, 1–1.5(–2) mm high, planar to slightly corrugated; oil ducts 3–7 in intervals, 4–9 on commissure; carpo­phore present.</description>
  <discussion>Cymopterus longipes is unusual in that the pseudoscape is strong and produces a rosette of leaves above ground level; C. corrugatus, C. glaucus, C. glomeratus, and C. ibapensis do this as well.  Due to similarities in leaf morphology, Lomatium planosum can be confused with C. longipes; however, C. longipes differs from L. planosum in having fruiting pedicels 3–8 mm long, yellow or white petals, and lateral wings of the fruit 1–5(–2) mm wide.  In contrast, L. planosum has fruiting pedicels 2–5) mm long, yellow petals (rarely purple), and lateral wings of the fruit 0.4–0.8 mm wide.  The yellow flowers may distinguish L. planosum from similar white- or yellow-flowered C. longipes, but flower color fades in herbarium specimens.</discussion>
  <discussion>Cymopterus longipes is a culturally significant food plant to Indigenous Peoples, including but not limited to the Goshute and Ute nations (D. E. Moerman, http://naeb.brit.org).</discussion>
  <description type="phenology">Flowering Apr–Jun.</description>
  <description type="habitat">Sagebrush scrub, mountain mahogany scrub, pinyon-juniper woodlands, sandy, gravelly, rocky, loam or clay soils.</description>
  <description type="elevation">1300–3200 m.</description>
  <description type="distribution">Idaho, Oreg., Utah, Wyo.</description>
</bio:treatment>
