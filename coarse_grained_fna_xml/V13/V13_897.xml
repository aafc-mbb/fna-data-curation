<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">13</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">RUTACEAE</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">ZANTHOXYLUM</taxon_name>
    <taxon_name rank="species" authority="Shinners" date="1956">parvum</taxon_name>
    <place_of_publication>
      <publication_title>Field &amp; Lab.</publication_title>
      <place_in_publication>24: 19.  1956</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rutaceae;genus zanthoxylum;species parvum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
  </taxon_identification>
  <number>6.</number>
  <other_name type="common_name">Shinners’ tickle-tongue</other_name>
  <description type="morphology">Shrubs, deciduous, to 2 m, often clonal and forming thickets.  Stems armed, often with stout paired prickles at nodes, bark relatively smooth; twigs and buds hairy to glabrate with ferruginous unbranched to dendritic or scalelike and often larger whitish unbranched hairs.  Leaves imparipinnate, clustered along stems or at tips of spur shoots, (2.5–)4.5–14 × (1–)2.5–6.5 cm; petiole and rachis cylindric to slightly canaliculate; petiolules 0–1 mm; leaflets (7–)9–11(–13), elliptic or ovate, (0.6–)1.4–3.7 × (0.4–)1.2–2.1 cm, chartaceous, base cuneate to rounded, equilateral to slightly inequilateral, apex acute to short acuminate or obtuse, abaxial surface hairy to glabrate, adaxial surface essentially glabrous.  Inflorescences axillary, fascicles and ramiflorous at old nodes, often appearing before leaves and seeming somewhat terminal on spur shoots, 2–12-flowered, 0.5–0.6 cm diam., hairy with ferruginous hairs as they emerge.  Pedicels 2–4 mm.  Flowers: tepals (4–)5, yellowish green, 2–3 × 1.5–2 mm, apex with ferruginous hairs; staminate flowers: stamens 4–5; pistillodes 2(–3); pistillate flowers: staminodes 0; pistils 2–4; stigmas usually partially connate.  Follicles: not known.</description>
  <discussion>Zanthoxylum parvum and Z.americanum are similar morphologically (see discussion under 5. Z.americanum), with Z.parvum tending to have smaller leaves and inflorescences that sometimes appear more terminal on spur shoots.</discussion>
  <discussion>Botanists at Sul Ross State University have studied Zanthoxylum parvum in the field and laboratory.  J.C. Zech (unpubl.) documented a total of nine populations, all within Jeff Davis County.  He surveyed seven, which ranged in size from about 40–590 stems.  Zanthoxylum parvum tends to grow under trees in relatively deep leaf litter, where rocks may channel rainfall.  Common woody associates include various Quercus species, Acer grandidentatum, Pinus cembroides, P.ponderosa, and Juniperus deppeana.  Fruit and seed production has not been observed and may be limited by drought.  A.M. Powell (pers. comm.) reported an herbarium specimen at SRSC collected by B.G. Hughes, who took live cuttings on January 24, 1992, and harvested flowers in a greenhouse mist bench nearly three weeks later.  A meiotic chromosome count of n = ca. 17 II (bivalents) is reported on the voucher specimen (Hughes 444) made from the cuttings.  This report is interesting because R.I. Walker (1942) reported a chromosome number of n = 34 from Z.americanum collected in Wisconsin.</discussion>
  <discussion>C. Reynel (2017) synonymized Zanthoxylum parvum with the widespread Z.americanum, which, as recognized traditionally, is not known from Texas (see discussion under 5. Z.americanum).  However, its western disjunct occurrence at higher elevations on igneous substrates (versus mostly calcareous substrates) warrants its recognition as a separate species.</discussion>
  <discussion>Zanthoxylum parvum is in the Center for Plant Conservation’s National Collection of Endangered Plants.</discussion>
  <description type="phenology">Flowering Mar–May(–Nov); fruiting not observed.</description>
  <description type="habitat">Protected areas on steep slopes and at bases of bluffs (often north- or northeast-facing sites) on igneous substrates in mixed pine-hardwoods.</description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">1400–1900 m.</description>
  <description type="distribution">Tex.</description>
</bio:treatment>
