<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>Shannon C. K. Straub, James L. Reveal†, Alan S. Weakley</author>
    </source>
    <other_info_on_meta type="volume">11</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Lindley" date="unknown">FABACEAE</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">Faboideae</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">AMORPHA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 713.  1753</place_in_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 319.  1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fabaceae;subfamily faboideae;genus amorpha</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek a-, without, and morphos, shape, misshapen, alluding to the simplified, non-papilionaceous flowers</other_info_on_name>
  </taxon_identification>
  <number>61.</number>
  <other_name type="common_name">False indigo</other_name>
  <description type="morphology">Shrubs, rarely subshrubs, unarmed, often gland-dotted; sometimes rhizomatous, pungently fragrant when bruised.  Stems usually erect, sometimes spreading, glabrous or pubescent.  Leaves alternate, odd-pinnate; stipules present; petiolate; leaflets (7–)9–45(–63), opposite or subopposite, stipels present, petiolulate, blade margins entire or crenulate, veins usually obscured abaxially, midvein mucronate, surfaces gland-dotted or eglandular, pubescent or glabrous.  Inflorescences 50–120+-flowered, terminal, racemes, usually solitary or clustered, rarely paniculiform (A. paniculata); bracts present, caducous, entire or crenulate, often gland-dotted; bracteoles caducous, entire.  Pedicels spreading to ascending, sometimes obscure, articulate to hypanthium.  Flowers non-papilionaceous, reduced to a single banner petal enveloping androecium and gynoecium; calyx persistent, slightly zygomorphic, greenish to reddish or purplish, sometimes drying blackish, cylindric, funnelform, campanulate, turbinate, conic, or obconic, lobes 5, obscure or distinct, not accrescent; banner petal purple, reddish purple, purplish, blue, lavender, violet, violet-blue, or white, glabrous; stamens 10, monadelphous basally, at least at early anthesis, or distinct, exserted; anthers dorsifixed, relatively small, dehiscing longitudinally; ovary slightly compressed, ovoid; styles slender, often exserted; stigma terminal, capitate.  Fruits legumes, often persistent through next growing season, sessile or shortly stipitate, straight, curved, or bent, falcate or D-shaped, slightly compressed and asymmetrical, oblong to obovoid or claviform, indehiscent, dispersed with calyx, glabrous or pubescent.  Seeds 1(or 2), laterally compressed, ovoid to oblong.  x = 10.</description>
  <discussion>Species 16 (15 in the flora).</discussion>
  <description type="distribution">North America, nw Mexico; introduced in Europe, Asia, Africa.</description>
  <discussion>Amorpha is notable among papilionoid legumes for having a non-papilionaceous corolla consisting solely of a banner petal, although deviation from the normal papilionoid floral form is common throughout Amorpheae.  Even though recognition of the genus has never been in question, due to its easily distinguished floral characters, delimitation of species within the genus has long caused consternation among taxonomists.  The range and intergradation of morphological variation have resulted in a prodigious list of recognized species, varieties, and forms, most often associated with the A. fruticosa complex.  Early taxonomic treatments failed to result in a satisfactory circumscription of the genus (C. K. Schneider 1907; P. A. Rydberg 1919–1920; E. J. Palmer 1931), but the insightful and thorough work of R. L. Wilbur (1975), closely followed by D. Isely (1998) and here, was a marked improvement.</discussion>
  <discussion>The “amorphoid” clade of Amorpheae, to which Amorpha belongs, has been strongly supported as monophyletic in molecular analyses (M. McMahon and L. Hufford 2004), although most relationships among genera in the clade remain unclear.  Analyses of chloroplast, ribosomal DNA, and low-copy nuclear gene sequence data indicated that Amorpha may not be monophyletic because either Errazurizia rotundata or Parryella filifolia, or both, are nested within it (McMahon and Hufford 2004, 2005; McMahon 2005).  Some of these analyses have indicated that E. rotundata and P. filifolia are sister species (combined analyses of chloroplast trnK, matK, ITS/5.8S rDNA, nuclear CNGC4), which may or may not be nested in Amorpha; other analyses indicated that E. rotundata is nested among Amorpha species while P. filifolia may or may not be (trnK, matK analyzed alone, CNGC4 analyzed alone).  These conflicting outcomes leave the relationship of these two species to each other and to Amorpha unclear.  Preliminary analyses of additional nuclear gene data sets have also indicated that E. rotundata and P. filifolia are nested in Amorpha but have not clarified whether or not they are sister species (S. C. K. Straub and J. J. Doyle 2014).  Morphological evidence also supports the close association of E. rotundata (originally described as P. rotundata Wooton), P. filifolia, and Amorpha.  Parryella filifolia lacks a corolla, and the corolla of E. rotundata is either absent or consists of a single petal, which suggests a closer association in terms of floral evolution to Amorpha than to the other 5-petaled genera of Amorpheae, an observation supported by developmental studies (McMahon and Hufford 2005).  W. F. Mahler (1965) hypothesized a close relationship of Amorpha, E. rotundata, and P. filifolia based on shared pollen characteristics.</discussion>
  <discussion>The phylogenetic relationships among Amorpha species remain unclear due to lack of molecu­lar variation in non-coding chloroplast, nuclear rDNA spacer regions, and nuclear gene intron sequence data.  Preliminary results from phylogenetic analyses, including DNA sequence data from most species of the genus, indicate that A. californica is the earliest diverging species in the genus and that A. georgiana and A. nana are closely related; however, the relationships among the other species of the genus are thus far unresolved (S. C. K. Straub and J. J. Doyle 2014).  The lack of molecular variation may be indicative of a rapid radiation in the genus or of continued gene flow and partial genetic homogenization through hybridization and introgression, possibly mediated by widespread species.  Ongoing genetic work has suggested that polyploidy is more common in Amorpha than previously recognized.  Amorpha fruticosa has long been known to be a tetraploid, and new information from nuclear gene DNA sequences and microsatellites now indicates that A. confusa, A. crenulata, and A. roemeriana may also be tetraploids, although this is yet to be confirmed by chromosome counts (Straub et al. 2009; Straub and Doyle).  Clearly, further work is needed to resolve species relationships within Amorpha, to update species concepts within the genus, and to determine whether or not Amorpha is monophyletic as circumscribed or if Errazurizia rotundata and P. filifolia should be added to Amorpha.</discussion>
  <discussion>Amorpha has been a group of interest for many years in the search for biologically active compounds.  In the area of medicinal biochemistry, anti-tumor and anti-inflammatory com­pounds have been isolated from A. fruticosa (L. Li et al. 1993; J. Y. Cho et al. 2000).  Antimi­crobial agents have been identified in both A. fruticosa and A. nana (L. A. Mitscher et al. 1981, 1985).  Amorpha fruticosa has also been investigated for its insecticidal and insect repellant properties (R. C. Roark 1947).  The glands on the fruits of this species have been shown to contain compounds that poison numerous types of insects through ingestion or contact (for example, chinch bug, cotton aphid, pea aphid, spotted cucumber beetle, mosquito larvae) and also insect repellant properties (for example, striped cucumber beetle, flour beetles, dog fleas, and houseflies; C. H. Brett 1946, 1946b).</discussion>
  <discussion>Native Americans of the Great Plains employed several of the more common Amorpha species for a variety of uses.  Amorpha fruticosa was used for bedding, horse feed, and arrow shafts, and stems were arranged on the ground to create a clean place to put butchered meat (M. R. Gilmore 1919; P. A. Vestal and R. E. Schultes 1939; D. J. Rogers 1980).  Amorpha canescens was used to treat stomach pain, intestinal worms, eczema, neuralgia, and rheumatism, and powdered leaves were applied to wounds (W. J. Hoffman 1891; Gilmore 1909, 1919; Hu. H. Smith 1928).  The leaves were also mixed with buffalo fat and smoked or used to make tea (Gilmore 1919).  The dried leaves of A. nana were used to treat catarrh (F. H. Elmore 1944).</discussion>
  <discussion>A few Amorpha species are found regularly in cultivation, the most common being A. fruticosa and A. canescens, while A. californica, A. herbacea, and A. nana are less commonly part of the horticultural trade.  Amorpha fruticosa has also been used in the United States and abroad for soil stabilization, erosion control, and in windbreaks, and has been investigated as a potential forage and biomass production crop (S. Karrenberg et al. 2003; L. R. DeHaan et al. 2006; K. Török et al. 2003).  Cultivation of A. fruticosa has led to its escape and naturalization in many parts of Europe and Asia.</discussion>
  <discussion>Amorpha apiculata Wiggins is known from Baja California, Mexico.</discussion>
  <references>
    <reference>Palmer, E. J.  1931.  Conspectus of the genus Amorpha.  J. Arnold Arbor. 12: 157–197.</reference>
    <reference>Schneider, C. K.  1907.  Conspectus generis Amorphae.  Bot. Gaz. 43: 297–307.</reference>
    <reference>Wilbur, R. L.  1964b.  A revision of the dwarf species of Amorpha (Leguminosae).  J. Elisha Mitchell Sci. Soc. 80: 51–65.</reference>
    <reference>Wilbur, R. L.  1975.  A revision of the North American genus Amorpha (Leguminosae-Psoraleae).  Rhodora 77: 337–409.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petioles and rachises with spinelike glands; Arizona, California, Oregon.</description>
      <determination>1. Amorpha californica</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petioles and rachises without spinelike glands; widespread in United States, southernmost Canada.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Subshrubs or shrubs, rarely perennial herbs, (0.1–)0.3–1.4(–3) m; petioles 0.5–15(–20) mm; leaflet margins usually slightly to conspicuously revolute.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Shrubs mostly canescent, rarely glabrous; eglandular or sparsely gland-dotted; c United States, s, c Canada.</description>
      <determination>7. Amorpha canescens</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Shrubs not canescent; usually densely gland-dotted; widespread in n, c, se United States, s, c Canada.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaflet midveins terminated by a swollen mucro; shrubs mostly densely puberulent to pubescent, when glabrous, then plants of Florida.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaflet blades (6–)8–18(–24) cm, margins entire or inconspicuously crenulate; petioles (0.5–)1–10(–13) mm.</description>
      <determination>5. Amorpha herbacea</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaflet blades (8–)15–25(–30) cm, margins crenulate; petioles (3–)8–15.</description>
      <determination>6. Amorpha crenulata</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaflet midveins terminated by a slender mucro; shrubs usually glabrous or sparsely pubescent and not of Florida.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Petioles (6–)8–15(–20) mm; racemes branched, 10–20(–30) cm; banners bright blue.</description>
      <determination>4. Amorpha confusa</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Petioles 1–8(–10) mm; racemes usually unbranched, (2–)3–20(–30) cm; banners reddish purple, rarely lavender.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaflet blades (3–)6–15(–18) cm; petioles 1–3(–5) mm; filaments distinct; anthers yellow; ovaries pubescent; se United States.</description>
      <determination>2. Amorpha georgiana</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaflet blades (1.5–)3–7(–10) cm; petioles (2–)4–8(–10) mm; filaments connate basally; anthers purplish; ovaries glabrous; n, c United States, sc Canada.</description>
      <determination>3. Amorpha nana</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Shrubs or suffrutescent herbs, 1–3(–4) m; petioles (5–)10–60(–90) mm; leaflet margins flat or slightly revolute.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Calyx lobes as long as or longer than tubes, adaxial calyx lobes usually not rounded.</description>
      <determination>10. Amorpha schwerinii</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Calyx lobes as long as or distinctly shorter than tubes, adaxial calyx lobes usually rounded or leaflets with conspicuous raised venation and paniculiform inflorescence if calyx lobes not rounded.</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Leaflet veins conspicuous, distinctly raised abaxially; racemes paniculiform.</description>
      <determination>9. Amorpha paniculata</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Leaflet veins obscure, not raised abaxially; racemes 1–8(–12)-branched.</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Leaflet midveins terminated by a swollen mucro.</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Leaflets (9–)13–19(or 21), blades 4–10(–12) mm wide, mucro 0.1–0.2 mm, petiolules distinctly warty-glandular; banners bright blue to deep violet-blue; legumes 4.5–6 mm.</description>
      <determination>12. Amorpha laevigata</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Leaflets (7 or)9–13(–17), blades (7–)15–25(–38) mm wide, mucro 0.2–0.8 mm, petiolules gland-dotted, rarely eglandular; banners purple; legumes 6–9 mm.</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Racemes (4–)6–12(–20) cm; calyx obviously gland-dotted on distal 1/3; Texas.</description>
      <determination>8. Amorpha roemeriana</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Racemes (8–)10–20 cm; calyx not obviously gland-dotted on distal 1/4; Arkansas, Oklahoma.</description>
      <determination>13. Amorpha ouachitensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Leaflet midveins terminated by a slender or slightly swollen mucro.</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Calyx lobes  0–0.6(–0.8) mm.</description>
      <determination>11. Amorpha glabra</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Calyx lobes 0.2–1.4 mm.</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Foliage and calyx drying blackish; leaflet blades distinctly shiny adaxially.</description>
      <determination>14. Amorpha nitens</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Foliage and calyx not drying blackish; leaflet blades not shiny adaxially.</description>
      <determination>15. Amorpha fruticosa</determination>
    </key_statement>
  </key>
</bio:treatment>
