<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">11</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Lindley" date="unknown">FABACEAE</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">Faboideae</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">PHASEOLUS</taxon_name>
    <taxon_name rank="species" authority="A. Gray" date="1853">angustissimus</taxon_name>
    <place_of_publication>
      <publication_title>Smithsonian Contr. Knowl.</publication_title>
      <place_in_publication>5(6): 33.  1853</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fabaceae;subfamily faboideae;genus phaseolus;species angustissimus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Phaseolus</taxon_name>
    <taxon_name rank="species" authority="unknown" date="unknown">angustissimus</taxon_name>
    <taxon_name rank="variety" authority="M. E. Jones" date="unknown">latus</taxon_name>
    <taxon_hierarchy>genus phaseolus;species angustissimus;variety latus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">P.</taxon_name>
    <taxon_name rank="species" authority="Wooton &amp; Standley" date="unknown">dilatatus</taxon_name>
    <taxon_hierarchy>genus p.;species dilatatus</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <other_name type="common_name">Slimleaf or narrowleaf bean</other_name>
  <description type="morphology">Vines perennial, often forming dense growth, with thick and elongate tuberous, woody tap­roots.  Stems climbing or trail­ing, to 150 cm.  Leaves: stipules spreading to reflexed, ovate to triangular, often faintly lobed basally, 2.5–4 × 1–2 mm; petiole 2–5 cm; rachis 0.8–1.7 cm; stipels narrowly triangular to lanceolate, 1–2.5 mm; leaflet blades linear, narrowly oblong to lanceolate, or narrowly trullate, often lobed basally, 2.5–6 × 0.2–3 cm, thin to subcoriaceous, base truncate to broadly cuneate, apex usually obtuse or long-attenuate, surfaces covered with minute, uncinate hairs, also with incumbent hairs along veins abaxially and marginally.  Peduncles 3–11 cm.  Inflorescences to 20 cm, rarely with axillary flowers at base; axis covered with uncinate hairs; rachis 2–9 cm, with 2–6 biflorous nodes; primary bracts ovate, 1–1.5 × 0.8 mm, 3–5-veined.  Pedicels 3–5(–7) mm, hairs uncinate; bracteoles persistent, ovate to lanceolate or oblong, 1–1.5 mm, often densely covered with incumbent hairs, often at midpoint of pedicel.  Flowers: calyx campanulate, 2.5–3.5 mm, sparsely to densely covered with uncinate hairs, veins prominent; abaxial lobes triangular to narrowly so, apex acute, 1–1.5 mm; lateral lobes triangular, apex acute, 1 × 1 mm; adaxial lobes connate, apex emarginate or cleft; corolla pink to light purple, 12 mm; banner oblong to orbiculate, 1 mm, apex slightly emarginate; wings obovate, 12 mm; keel 5.5 mm; ovary linear, 3.5 mm, sparsely covered with appressed hairs.  Legumes pendent, compressed, linear-falcate, 23–30 × 4–8 mm, elastically dehiscent, valves thin, sparsely covered with minute, appressed hairs.  Seeds 3 or 4, brown often mottled with black, oblong, 3.5–5 × 3–4 mm, rugose, reticulate-areolate patterns with rounded mounds; hilum ovate, 0.5 mm.  2n = 22.</description>
  <discussion>Phaseolus angustissimus is widespread in Arizona and the western half of New Mexico but is restricted to Brewster, El Paso, Presidio, and Terrell counties in Texas.</discussion>
  <discussion>Phaseolus angustissimus is readily distinguished by its habit, linear leaflets, few floral nodes per inflorescence, and falcate pods with three or four rugosely ornamented seeds.  It is most likely to be confused with P. filiformis by a combination of characters, but differences in root system, stipule length, ovule number, and fruit width are usually sufficient to separate the two.  However, rugose seeds are also found in P. filiformis.</discussion>
  <discussion>Flowers and crushed roots have been reported by the Zuñi people as a health strengthener for children (M. C. Stevenson 1915).</discussion>
  <description type="phenology">Flowering May–Nov.</description>
  <description type="habitat">Semidesert regions, dry river or creek beds and banks, under pines, pinyon-juniper-oak forests, igneous or calcareous rocky soils.</description>
  <description type="elevation">1000–2500 m.</description>
  <description type="distribution">Ariz., N.Mex., Tex.; Mexico (Sonora).</description>
</bio:treatment>
