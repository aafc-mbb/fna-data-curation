<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">11</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Lindley" date="unknown">FABACEAE</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">Faboideae</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">CLITORIA</taxon_name>
    <taxon_name rank="species" authority="Linnaeus" date="1753">mariana</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 753.  1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fabaceae;subfamily faboideae;genus clitoria;species mariana</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Martiusia</taxon_name>
    <taxon_name rank="species" authority="(Linnaeus) Small" date="unknown">mariana</taxon_name>
    <taxon_hierarchy>genus martiusia;species mariana</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">Xylopodium: proximal portion horizontal, cylindric, 4–15 × 3–12 cm; distal portion 1+ m × 1.5–6 mm.  Stems 1–4 from crown, trailing, to 150 cm, or erect to lax, apex sometimes twining, to 60 cm; internodes ± straight to weakly flexuous; branches few, primarily basal.  Leaves: stipules lanceolate to lanceolate-ovate, 4–8 × 2–5 mm, apex acute; petiole subterete, often weakly to strongly canaliculate adaxially, 2–10 cm; stipels linear to subulate, 3–8 mm; petiolules 2–4 mm; rachis 1–2(–2.5) cm; leaflets 3, blades ovate, oblong-ovate, elliptic-oblong, lanceolate, lanceolate-ovate, oblong, or elliptic, 20–90(–115) × 10–40(–65) mm, thick membranous to thick papery, base broadly cuneate to subcordate, primary lateral veins 7–12 pairs, apex usually acute to obtuse, rarely short-acuminate or mucronate, surfaces glaucescent, glabrate or sparsely pubescent abaxially on major veins or moderately to densely piloso-sericeous, glabrate or uncinate-pubescent adaxially.  Peduncles 1–4.5 cm (chasmogamous flowers) or 0.5–3 cm (cleistogamous flowers).  Inflorescences 2(–4)-flowered, 1–5 cm, flowers cleistogamous and/or chasmogamous; bracts lanceolate, 3–4 × 1 mm.  Pedicels 3–7 mm; bracteoles of chasmogamous flowers lance­olate to lanceolate-ovate, 4–9 × 2–3 mm, cleistogamous flowers 3–5 mm, apex acute, uncinate-pubescent.  Chasmogamous flowers: calyx tube greenish, 10–14 × 3–5 mm at base becoming 5–8 mm wide; lobes ovate, 5–9 × 2.5–3.5 mm (at base, abaxial lobe 7–9 mm), apex short-acuminate; corolla blue to pale purplish; banner 40–60 mm, claw 5–8 mm; wing blades 21–24 × 5–10 mm, claw 7–12 mm; keel 8–13 × 3–5 mm, claw 14–21 mm; staminal tube 21–30 mm; filaments distinct, 2–4 mm; anthers lanceolate, 1–2 mm; ovary 7–9 mm, densely uncinate-pubescent; style 16–20 mm.  Cleistogamous flowers: calyx tube 4–5 × 1–2 mm becoming 2 mm wide; lobes 2–3 mm; staminal tube 0.1 mm; ovary 5–6 mm; style 4–6 mm, bent backwards and in contact with anthers.  Legumes brown, convex, 25–55(–70) × 6–9 mm; stipe 12–17 mm (5–10 mm in cleistogamous).  Seeds 1–10, black, cuboidal to globular, 3–5 × 4–6 mm, viscid.</description>
  <discussion>Varieties 3 (2 in the flora).</discussion>
  <description type="distribution">se United States, n Mexico, Asia.</description>
  <discussion>Variety orientalis Fantz occurs in China, eastern India, Myanmar (Burma), and Thailand.  It is character­ized as a vine with inflorescences bearing two to six slightly smaller flowers than those in the flora area and without cleistogamous flowers.</discussion>
  <discussion>Clitoria mariana is cultivated for its showy chas­mogamous flowers.  Plants in the wild grow as isolated individuals or in clusters.</discussion>
  <discussion>Historically, Clitoria mariana has been misidentified as Centrosema virginianum, which is distinguished as a vine with a campanulate calyx bearing lobes that are narrow and longer than the tube, bracteoles that nearly hide the calyx, a spurred banner, wings subequal in length to the keel, and flat fruits with two marginal veins.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaflet blades sparsely pubescent or glabrate abaxially, hairs scattered or confined to major veins; calyces glabrate, macrotrichomes sparse, scattered.</description>
      <determination>2a. Clitoria mariana var. mariana</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaflet blades moderately to densely pilose-sericeous abaxially, hairs suberect to erect, not widely scattered or confined to veins; calyces uncinate-pubescent or glabrate, macrotrichomes moderately dense to scattered.</description>
      <determination>2b. Clitoria mariana var. pubescentia</determination>
    </key_statement>
  </key>
</bio:treatment>
