<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>Stanley L. Welsh</author>
    </source>
    <other_info_on_meta type="volume">11</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Lindley" date="unknown">FABACEAE</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">Faboideae</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">HEDYSARUM</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 745.  1753</place_in_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 332.  1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fabaceae;subfamily faboideae;genus hedysarum</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek hedysma or hedys, sweetness, and aron, ointment or fragrance, probably alluding to ancient use of Trigonella foenum-graecum seed oil, applied to the present taxon by Linnaeus</other_info_on_name>
  </taxon_identification>
  <number>142.</number>
  <other_name type="common_name">Sweetvetch</other_name>
  <other_name type="common_name">sainfoin</other_name>
  <description type="morphology">Herbs, perennial, unarmed; with ligneous taproot.  Stems decumbent to erect or ascending, solid, terete, pubescent, hairs basifixed; from branching subterranean to superficial caudex.  Leaves alternate, odd-pinnate; stipules present, slightly adnate to petiole base, ± connate-sheathing, often suffused with purple, lanceolate, simple or bidentate, scarious; petiolate, petiole much shorter than or subequal to blade; leaflets 5–27, opposite or alternate, petiolulate, blade margins entire, surfaces mostly pubescent, sometimes glabrous adaxially.  Inflorescences 5–60-flowered, axillary, racemes, sometimes subcapitate; bracts present, 1 per flower; bracteoles usually 2.  Flowers papilionaceous; calyx campanulate, lobes 5; corolla usually pink, pink-purple, lavender-pink, red-purple, lilac, lilac-purple, or yellow, rarely white, keel much exceeding wings, somewhat longer than banner, broadly truncate, apex prominent, oblique; stamens 10, diadelphous; anthers dorsifixed; ovary enclosed in staminal sheath; style glabrous.  Fruits loments, stipitate, pendulous to spreading, compressed, straight, narrowly ellipsoid, indehiscent (breaking transversely), constricted into 1–8, 1-seeded segments, glabrous or pubescent, rarely with processes.  Seeds 1 per segment, brown, flattened, reniform-ovoid, glossy.  x = 7.</description>
  <discussion>Species ca. 50 (4 in the flora).</discussion>
  <description type="distribution">North America, Europe, Asia (Asia Minor).</description>
  <references>
    <reference>Northstrom, T. E.  1974.  The Genus Hedysarum in North America.  Ph.D. dissertation.  Brigham Young University.</reference>
    <reference>Northstrom, T. E. and S. L. Welsh.  1970.  Revision of the Hedysarum boreale complex.  Great Basin Naturalist 30: 109–130.</reference>
    <reference>Rollins, R. C.  1940b.  Studies in the genus Hedysarum in North America.  Rhodora 42: 217–239.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaflet blade veins obscure; loment margins moderately or not winged; calyx lobes subequal to markedly unequal in size; wing auricles distinct, blunt, shorter than claw.</description>
      <determination>1. Hedysarum boreale</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaflet blade veins conspicuous; loment margins narrowly or conspicuously winged; calyx lobes equal or nearly so; wing auricles connate, linear, nearly equal or equal to claw.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Flowers 10–19(–22) mm (when greater than 16 mm, then from north of 50th parallel), corollas usually lilac- to pink-purple, rarely white; loment segments 5.5–12 × 3.5–6 mm, margins narrowly winged; leaflet blades lanceolate to oblong, elliptic, or lanceolate-elliptic.</description>
      <determination>2. Hedysarum alpinum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Flowers (14–)16–25 mm, corollas usually yellow to pale yellow, or shades of pink or purple, rarely white; loment segments 7–14.5(–18) × 5.5–10.2(–11) mm, mar­gins conspicuously winged; leaflet blades lanceolate to ovate, elliptic, oblong, or lanceolate-oblong.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Corollas yellow to pale yellow, 14–20 mm; loment segments 5.5–9 mm wide, glabrous.</description>
      <determination>3. Hedysarum sulphurescens</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Corollas usually lavender-pink or lilac- to pink-purple, rarely white, 16–25 mm; loment segments 5.6–10.2(–11) mm wide, pubescent or glabrous.</description>
      <determination>4. Hedysarum occidentale</determination>
    </key_statement>
  </key>
</bio:treatment>
