<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">11</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Lindley" date="unknown">FABACEAE</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">CaesalpinioideaeExcludingMimosoidClade</taxon_name>
    <taxon_name rank="genus" authority="Miller" date="1754">SENNA</taxon_name>
    <taxon_name rank="species" authority="(Linnaeus) H. S. Irwin &amp; Barneby" date="1982">obtusifolia</taxon_name>
    <place_of_publication>
      <publication_title>Mem. New York Bot. Gard.</publication_title>
      <place_in_publication>35: 252.  1982</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fabaceae;subfamily caesalpinioideaeexcludingmimosoidclade;genus senna;species obtusifolia</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Cassia</taxon_name>
    <taxon_name rank="species" authority="Linnaeus" date="1753">obtusifolia</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 377.  1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus cassia;species obtusifolia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">C.</taxon_name>
    <taxon_name rank="species" authority="unknown" date="unknown">tora</taxon_name>
    <taxon_name rank="variety" authority="Persoon" date="unknown">humilis</taxon_name>
    <taxon_hierarchy>genus c.;species tora;variety humilis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">C.</taxon_name>
    <taxon_name rank="species" authority="unknown" date="unknown">tora</taxon_name>
    <taxon_name rank="variety" authority="(Linnaeus) Haines" date="unknown">obtusifolia</taxon_name>
    <taxon_hierarchy>genus c.;species tora;variety obtusifolia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">C.</taxon_name>
    <taxon_name rank="species" authority="Rafinesque" date="unknown">toroides</taxon_name>
    <taxon_hierarchy>genus c.;species toroides</taxon_hierarchy>
  </taxon_identification>
  <number>16.</number>
  <other_name type="common_name">Java-bean</other_name>
  <other_name type="common_name">sicklepod</other_name>
  <description type="morphology">Herbs, annual or biennial, to 1.2(–2.4) m.  Leaves mesophyl­lous, 3.5–17 cm, pallid green, slightly and finely hairy or glabrous; stipules caducous; extrafloral nectary 1, usually between first leaflet pair, rarely also between second, sessile or shortly stipitate; leaflet pairs 3, blades obovate to cuneate-obovate or broadly cuneate-oblanceolate, 17–65 × 10–40 mm.  Racemes usually 1 or 2(or 3)-flowered; bracts caducous.  Pedicels 7–28 mm.  Flowers asymmetric, enantiostylous; calyx pale green; corolla pale yellow, longest petal 9–15 mm, 1 lower petal conspicuously larger; androecium heterantherous, stamens 7, middle stamens 1/2 as long as abaxial or smaller, staminodes 3; anthers of middle stamens 1–2.8 mm, of abaxial stamens 2–5 mm, dehiscing by U-shaped slit, apical appendage inconspicuous, cupped; gynoecium incurved, ovules 16–38; ovary hairy; style incurved.  Legumes erect or curved downward, flat, straight, 60–180 × 2.5–6 mm, faintly corrugated over seeds, indehiscent.  Seeds dark reddish brown, rhomboid or subcylindroid-oblong.  2n = 24, 26, 28.</description>
  <discussion>Senna obtusifolia is one of the most widespread weedy sennas native to the Americas (H. S. Irwin and R. C. Barneby 1982) and is probably naturalized circumtropically.  The species is considered a noxious weed in many countries, posing problems especially for agriculture; it is able to completely invade pastures by dominating grass species, it strongly competes with crops, affecting yields negatively, and, although generally unpalatable to stock, if eaten, it is toxic to cattle.  For these reasons, in Australia S. obtusifolia is designated as potentially of national concern, and authorities estimate that this invader can lead properties to become unproductive (Weeds of Australia, www.environment.gov.au/cgi-bin/biodiversity/invasive/weeds/weedsearch.pl; Land Protection, www.nrw.qld.gov.au/factsheets/pdf/pest/pp18.pdf).  In the flora area, its range of distribution appears to have expanded from five southern and southeastern states in the early 1980s (Florida, Kentucky, Missouri, Texas, and Virginia; Irwin and Barneby) to currently almost half of the states.  Although a few specimens were collected as far north as Nebraska and Wisconsin, it is not known to be established in those states.</discussion>
  <discussion>Senna tora (Linnaeus) Roxburgh (Cassia tora Linnaeus) is sometimes considered synonymous with S. obtusifolia.  However, due to differences in length and curvature of the fruit and in length of the petiole and pedicel, B. R. Randell and B. A. Barlow (1998) considered it distinct.  H. S. Irwin and R. C. Barneby (1982) observed a range rather than distinct categories in the measures and shapes of these traits, and only molecular phylogenetic and biogeographic studies accompanied by morphometric analyses may definitively solve this taxonomic dilemma.</discussion>
  <description type="phenology">Flowering late spring–mid winter.</description>
  <description type="habitat">Lakeshores, riverbanks, river beds, disturbed habitats, pastures, plantations, orchards, roadsides, waste places.</description>
  <description type="elevation">0–1700 m.</description>
  <description type="distribution">Ala., Ark., Calif., Fla., Ga., Ill., Ind., Kans., Ky., La., Md., Mass., Miss., Mo., N.J., N.Y., N.C., Okla., Pa., S.C., Tenn., Tex., Va., W.Va.; introduced in Asia, Africa, Atlantic Islands, Indian Ocean Islands, Pacific Islands, Australia.</description>
</bio:treatment>
