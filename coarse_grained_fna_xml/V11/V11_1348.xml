<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">11</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Lindley" date="unknown">FABACEAE</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">Faboideae</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">ASTRAGALUS</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Collini</taxon_name>
    <taxon_name rank="species" authority="(A. Heller) J. F. Macbride" date="1922">curvicarpus</taxon_name>
    <place_of_publication>
      <publication_title>Contr. Gray Herb.</publication_title>
      <place_in_publication>65: 38.  1922</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fabaceae;subfamily faboideae;genus astragalus;section collini;species curvicarpus</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Homalobus</taxon_name>
    <taxon_name rank="species" authority="A. Heller" date="1905">curvicarpus</taxon_name>
    <place_of_publication>
      <publication_title>Muhlenbergia</publication_title>
      <place_in_publication>2: 86.  1905</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus homalobus;species curvicarpus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Astragalus</taxon_name>
    <taxon_name rank="species" authority="Kellogg" date="unknown">gibbsii</taxon_name>
    <taxon_name rank="variety" authority="(A. Gray) M. E. Jones" date="unknown">falciformis</taxon_name>
    <taxon_hierarchy>genus astragalus;species gibbsii;variety falciformis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">A.</taxon_name>
    <taxon_name rank="species" authority="A. Gray" date="unknown">speirocarpus</taxon_name>
    <taxon_name rank="variety" authority="A. Gray" date="unknown">falciformis</taxon_name>
    <taxon_hierarchy>genus a.;species speirocarpus;variety falciformis</taxon_hierarchy>
  </taxon_identification>
  <number>60.</number>
  <other_name type="common_name">Sickle milkvetch</other_name>
  <description type="morphology">Plants robust or slender, 10–40 cm, strigulose, villosulous, glabrous, or with few scattered hairs; from superficial or slightly subterranean caudex.  Stems decumbent to ascending, strigulose, villosulous, glabrous, or with few scattered hairs.  Leaves 2.5–9 cm; stipules distinct, (1–)1.5–5 mm, scarious at proximal nodes, herbaceous at distal nodes; leaflets (9 or)11–21(–25), blades obovate-cuneate, oblong-obovate, or elliptic to broadly oblanceolate, (7–)9–19(–21) mm, apex retuse or truncate to obtuse, surfaces strigulose or villosulous abaxially, strigose to glabrescent or glabrous adaxially.  Peduncles erect or arcuate-erect, 4–15 cm.  Racemes (5–)10–25(–35)-flowered, flowers retrorsely imbricate, ascending then nodding; axis 2–10(–13) cm in fruit; bracts 0.7–2.5 mm; bracteoles 0.  Pedicels 1–3.5 mm.  Flowers 13.5–21 mm; calyx broadly campanulate to broadly cylindric (gibbous-saccate or gibbous-truncate), 6.1–13.6 mm, strigulose or villosulous, marcescent, tube 5.4–11.9 mm, marcescent, lobes triangular, 0.4–2.3 mm; corolla ochroleucous, white, or lemon yellow; banner recurved through 45°; keel 9.4–15.2 mm.  Legumes pendulous, green or purple-spotted becoming brown-stramineous, hamately or lunately incurved or, rarely, coiled through 1.5 spirals, narrowly oblong, laterally compressed, 14–35 × (2.7)3–4.5(–5.5) mm, stiffly papery or thinly leathery, usually glabrous, villosulous, or strigulose, rarely glabrate; stipe 6–20 mm.  Seeds 14–25(–28).</description>
  <discussion>Varieties 3 (3 in the flora).</discussion>
  <description type="distribution">w United States.</description>
  <discussion>Astragalus curvicarpus and its immediate relatives (A. collinus to the north, A. gibbsii to the south) are geographically exclusive.  Formal classification of these three species is speculative because relationships seem reticulate and the origin of the group polyphyletic (D. Isely 1998).  R. C. Barneby (1964) discussed the evolutionary irrationalities of sect. Collini.</discussion>
  <discussion>Astragalus gibbsii var. curvicarpus (E. Sheldon) M. E. Jones and A. speirocarpus var. curvicarpus E. Sheldon are illegitimate names that pertain here.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Legumes usually villosulous or strigulose, rarely glabrate or glabrous; flowers (15–)16.4–21 mm; leaflet blades sparsely pubescent adaxially; California, Idaho, Nevada, Oregon.</description>
      <determination>60a. Astragalus curvicarpus var. curvicarpus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Legumes glabrous; flowers 13.6–19.5 mm; leaflet blades pubescent or glabrous adaxially; nw transmontane Oregon, in drainage of Deschutes and John Day rivers.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Flowers 13.6–15(–16.8) mm; calyces 6.1–8.5(–9.3) mm; leaflet blade adaxial surface pubescent.</description>
      <determination>60b. Astragalus curvicarpus var. brachycodon</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Flowers 13.5–19.5 mm; calyces (8–)9–13.6 mm; leaflet blade adaxial surface glabrous, sparsely ciliate.</description>
      <determination>60c. Astragalus curvicarpus var. subglaber</determination>
    </key_statement>
  </key>
</bio:treatment>
