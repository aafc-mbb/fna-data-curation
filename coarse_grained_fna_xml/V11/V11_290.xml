<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">11</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Lindley" date="unknown">FABACEAE</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">Faboideae</taxon_name>
    <taxon_name rank="genus" authority="Scheele" date="1848">DERMATOPHYLLUM</taxon_name>
    <taxon_name rank="species" authority="(Ortega) Gandhi &amp; Reveal" date="2011">secundiflorum</taxon_name>
    <place_of_publication>
      <publication_title>Phytoneuron</publication_title>
      <place_in_publication>2011-57: 2.  2011</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fabaceae;subfamily faboideae;genus dermatophyllum;species secundiflorum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Broussonetia</taxon_name>
    <taxon_name rank="species" authority="Ortega" date="1798">secundiflora</taxon_name>
    <place_of_publication>
      <publication_title>Nov. Pl. Descr. Dec.,</publication_title>
      <place_in_publication>61, plate 7.  1798</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus broussonetia;species secundiflora</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Agastianis</taxon_name>
    <taxon_name rank="species" authority="(Ortega) Rafinesque" date="unknown">secundiflora</taxon_name>
    <taxon_hierarchy>genus agastianis;species secundiflora</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Calia</taxon_name>
    <taxon_name rank="species" authority="Terán &amp; Berlandier" date="unknown">erythrosperma</taxon_name>
    <taxon_hierarchy>genus calia;species erythrosperma</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Cladrastis</taxon_name>
    <taxon_name rank="species" authority="(Ortega) Rafinesque" date="unknown">secundiflora</taxon_name>
    <taxon_hierarchy>genus cladrastis;species secundiflora</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Dermatophyllum</taxon_name>
    <taxon_name rank="species" authority="Scheele" date="unknown">speciosum</taxon_name>
    <taxon_hierarchy>genus dermatophyllum;species speciosum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Sophora</taxon_name>
    <taxon_name rank="species" authority="(Ortega) Lagasca ex de Candolle" date="unknown">secundiflora</taxon_name>
    <taxon_hierarchy>genus sophora;species secundiflora</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">S.</taxon_name>
    <taxon_name rank="species" authority="(Scheele) Bentham" date="unknown">speciosa</taxon_name>
    <taxon_hierarchy>genus s.;species speciosa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Virgilia</taxon_name>
    <taxon_name rank="species" authority="(Ortega) Cavanilles" date="unknown">secundiflora</taxon_name>
    <taxon_hierarchy>genus virgilia;species secundiflora</taxon_hierarchy>
  </taxon_identification>
  <number>3.</number>
  <other_name type="common_name">Mescal bean</other_name>
  <other_name type="common_name">mountain laurel</other_name>
  <description type="morphology">Shrubs or trees, 1–5(–6) m, twigs tomentose.  Leaves: rachis 6–15 cm; leaflets (5 or)7–11, blades elliptic-obovate or oblong, (2–)2.5–5(–8) × 0.7–3.6 cm, base cuneate, apex rounded to emarginate.  Racemes 4–15-flowered, dense, 5–10 cm; bracts linear-lanceolate, apex acute.  Pedicels 10–15 mm.  Flowers ascending to spreading, 10–20 mm; calyx broadly obconic or turbinate, asymmetric, 8–11 mm; corolla usually blue-purple, rarely white.  Legumes brown, torulose, subglobose to cylidrical, 2–5(–10) × 1–1.5(–2) cm, woody.  Seeds 1–4(–8), usually red, rarely orange or yellow, 10–15 mm.  2n = 18.</description>
  <discussion>Dermatophyllum secundiflorum is widely distributed across the southern half of Texas and extends into southeastern New Mexico.</discussion>
  <discussion>Flowers of Dermatophyllum secundiflorum have been described as offensively fragrant and have a scent reminiscent of artificial grape flavoring.  Their odor can produce headaches and, sometimes, nausea (E. D. Schulz 1928).  The flowers are a source of honey (E. H. Graham 1941).  The dull red seeds, poisonous to humans and livestock, were used by Amerindian groups for beads (trade items).  Powdered and mixed with mescal, the seeds were employed to produce intoxication, delirium, excitement, and a deep sleep of two to three days (J. M. Kingsbury 1964; R. A. Vines 1960).  Bactericidal and fungicidal activities have been reported from seed extracts of mescal bean (D. Pérez-Laínez et al. 2008).  The slow growing plants are used as ornamentals.</discussion>
  <description type="phenology">Flowering Feb–Apr.</description>
  <description type="habitat">Rocky slopes, canyons, ravines, limestone hills, canyons.</description>
  <description type="elevation">0–1500 m.</description>
  <description type="distribution">N.Mex., Tex.; Mexico (Chihuahua, Coahuila, Durango, Hidalgo, Nuevo León, Puebla, San Luis Potosí, Zacatecas).</description>
</bio:treatment>
