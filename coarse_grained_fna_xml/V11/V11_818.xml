<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">11</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Lindley" date="unknown">FABACEAE</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">Faboideae</taxon_name>
    <taxon_name rank="genus" authority="(de Candolle) Bentham" date="1837">CENTROSEMA</taxon_name>
    <taxon_name rank="species" authority="(Linnaeus) Bentham" date="1837">virginianum</taxon_name>
    <place_of_publication>
      <publication_title>Comm. Legum. Gen.,</publication_title>
      <place_in_publication>56.  1837</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fabaceae;subfamily faboideae;genus centrosema;species virginianum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Clitoria</taxon_name>
    <taxon_name rank="species" authority="Linnaeus" date="1753">virginiana</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 753.  1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus clitoria;species virginiana</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Bradburya</taxon_name>
    <taxon_name rank="species" authority="(Linnaeus) Kuntze" date="unknown">virginiana</taxon_name>
    <taxon_hierarchy>genus bradburya;species virginiana</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Centrosema</taxon_name>
    <taxon_name rank="species" authority="unknown" date="unknown">virginianum</taxon_name>
    <taxon_name rank="variety" authority="(de Candolle) Grisebach" date="unknown">angustifolium</taxon_name>
    <taxon_hierarchy>genus centrosema;species virginianum;variety angustifolium</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">C.</taxon_name>
    <taxon_name rank="species" authority="unknown" date="unknown">virginianum</taxon_name>
    <taxon_name rank="variety" authority="(de Candolle) Fernald" date="unknown">ellipticum</taxon_name>
    <taxon_hierarchy>genus c.;species virginianum;variety ellipticum</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <description type="morphology">Rhizomes 4–8 cm × 5–11 mm proximally, to 30+ cm × 1–2 mm distally.  Stems 1–5, to 1–3 m × 1–2 mm.  Leaves: stipules ovate-deltate, 3–5 mm; petioles angular, canaliculate adaxially, 1–5 cm; stipels persistent, striate, linear, 3–5 mm; petiolules 2 mm; rachis 0.5–1.5 cm; leaflet blades ovate to elliptic, ovate to lanceolate, or oblong to linear, (12–)20–65(–100) × 8–45 mm, thinly leathery, base rotund, primary lateral veins of 6–8 pairs, to 15 pairs in linear leaflets, veins with uncinulate hairs, apex broadly acute.  Inflorescences 1(or 2) per node, 1–3 cm; bracts striate, concave around pedicel, oval-ovate, 4–6 mm; bracteoles striate, lanceolate-ovate, 6–8 × 3–5 mm, surfaces with uncinulate hairs; rachis subflexuous, 1–2(–3) cm, strigose.  Pedicels 5–8 mm, 7–11 mm in fruit, thinly pilose.  Flowers: calyx tube 3–4 × 4–6 mm, lobes subequal, abaxial ones 4–7 mm, lateral ones 5–8 mm, adaxial ones 6–10 mm, hairs uncinulate; corolla lavender to bluish violet or purple; banner 25–35(–40) mm, claw 2–4 mm, spur 1–2 mm; wings 14–19 × 5–9 mm, claw 5–7 mm; keel 16–20 × 7–10 mm, claw 3–5 mm; staminal tube 19–25 mm; apical distinct filaments 1–3 mm; ovary 12–15 mm; style 20–27 mm, apex dilated 9–12 × 1 mm.  Legumes: dehiscence causing valve to twist (1–)3–5 turns, (60–)80–110 × 3–5 mm; beak 9–17 mm.  Seeds 14–18, dark brown, 3–4 × 4–6 × 1 mm.  2n = 14, 18, 22.</description>
  <discussion>Historically, Centrosema virginianum has been misidentified as Clitoria mariana by collectors in the United States.  The latter species is distinguished easily by the non-scandent, erect stems, funnelform calyces with lobes broader and shorter than tubes, spurless banners, wings longer than keel petals, presence of cleistogamy, and turgid, ecostate fruits depressed slightly between the seeds.</discussion>
  <discussion>Two varieties of Centrosema virginianum have been described based upon leaflet width.  Specimens with narrow, linear leaves (var. angustifolium) found in the southern coastal areas appear distinct from those found inland with broader and shorter leaves (var. ellipticum, var. virginianum).  Examined specimens exhibit a gra­da­tion in leaflet width and length, from multiple collec­tions within a population, to variation within one plant, bearing shorter and broader leaves at proximal nodes and narrower and longer leaves at distal nodes.  Varieties are not recognized here.</discussion>
  <description type="phenology">Flowering and fruiting Jun–early Oct.</description>
  <description type="habitat">Sandy soils in pine-oak woodlands, oak-hickory woodlands, shale bluffs, roadsides, railroads, grasslands, old fields, sand dunes, alluvial woods, swamps, pocosins, marshes, bogs, river banks.</description>
  <description type="elevation">0–600 m.</description>
  <description type="distribution">Ala., Ark., Del., Fla., Ga., Ky., La., Md., Miss., Mo., N.J., N.C., Okla., Pa., S.C., Tenn., Tex., Va.; Mexico; West Indies; Central America; South America; introduced in Asia.</description>
</bio:treatment>
