<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>Richard R. Halse</author>
    </source>
    <other_info_on_meta type="volume">11</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Lindley" date="unknown">FABACEAE</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">Faboideae</taxon_name>
    <taxon_name rank="genus" authority="Fabricius" date="1763">CARAGANA</taxon_name>
    <place_of_publication>
      <publication_title>Enum. ed.</publication_title>
      <place_in_publication>2, 421.  1763</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fabaceae;subfamily faboideae;genus caragana</taxon_hierarchy>
    <other_info_on_name type="etymology">Mongolian qaraqan or Turkic karaghan, name for Caragana arborescens, the type species</other_info_on_name>
  </taxon_identification>
  <number>139.</number>
  <other_name type="common_name">Pea tree</other_name>
  <description type="morphology">Shrubs or subshrubs [trees], armed or unarmed, stipules sometimes spine-tipped or spinescent, rachis spine-tipped.  Stems erect or decumbent, glabrous or pubescent.  Leaves alternate, some­times clustered on spurs, even-pinnate, subpinnate, or appearing palmate [digitate]; stipules present, membranous when young, some with thickened midribs becoming bristle- or spinelike, or spinescent, 1.5–9 mm; rachis, when present, usually persistent; petiolate; leaflets 2–12[–20], opposite, blade margins entire, surfaces villous, glabrate, or glabrous.  Inflorescences 1–4(or 5)-flowered, axillary, fasciculate or solitary, each peduncle-pedicel 1-flowered; bracts present, membranous, at base of peduncle, bracteoles present or absent, linear, minute, at base of peduncle and pedicel.  Flowers papilionaceous; calyx persistent, tubular or campanulate, subgibbous, lobes 5, distinct, subequal, much shorter than tube, adaxial 2 usually smaller; corolla yellow or orange-yellow [rarely white or pink]; stamens 10, diadelphous; anthers uniform, dorsifixed; style straight or slightly curved.  Fruits legumes, sessile, flattened, oblong or linear, dehiscent, non-septate, valves twisting in dehiscence, glabrous.  Seeds [2 or]3–8, monochrome or mottled and streaked, oblong, ovoid, 4-angled, or globose [ellipsoid, subglobose, or reniform], smooth.  x = 8.</description>
  <discussion>Species 70–80 (3 in the flora).</discussion>
  <description type="distribution">introduced; e Europe, w, c Asia.</description>
  <discussion>The number and persistence of spines on Caragana species is variable.  The leaf rachises may persist after the leaflets fall, becoming woody and spinescent, and may last a few years.  The stipules also may become spinescent and persist; plants under cultivation seem to produce relatively few or no spines, which may be relatively short or reduced to a bristle (W. J. Bean 1970–1988, vol. 1).</discussion>
  <discussion>Some species of Caragana are cold- and drought-resistant.  They are cultivated extensively in Canada and the northern United States as ornamentals, for hedges, windbreaks, shelterbelts, and erosion control, and may persist.  In addition to the three species treated here, some additional species of Caragana are cultivated in North America, including C. microphylla Lamarck, C. pygmaea de Candolle, C. sinica (Buc’hoz) Rehder, and C. spinosa de Candolle.  Some of these are found only in arboreta or in specialty collections; others are more common.  The three species treated here are the only taxa known to have become naturalized in the flora area.</discussion>
  <discussion>Caragana needs worldwide revision because species boundaries are uncertain in some taxa; names associated with some plants growing in cultivation are doubtful.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves even-pinnate, leaflets 6–12(or 14).</description>
      <determination>1. Caragana arborescens</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves appearing palmate or subpinnate, leaflets 2–4.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaflet blades oblanceolate to linear-oblanceolate, mostly curved or sickle-shaped, 0.1–0.3(–0.4) cm wide.</description>
      <determination>2. Caragana aurantiaca</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaflet blades oblong-obovate to cuneate-obovate, not curved, 0.8–1.4 cm wide.</description>
      <determination>3. Caragana frutex</determination>
    </key_statement>
  </key>
</bio:treatment>
