<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">11</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Lindley" date="unknown">FABACEAE</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">CaesalpinioideaeMimosoidClade</taxon_name>
    <taxon_name rank="genus" authority="Britton &amp; Rose in N. L. Britton et al." date="1928">EBENOPSIS</taxon_name>
    <taxon_name rank="species" authority="(Berlandier) Barneby &amp; J. W. Grimes" date="1996">ebano</taxon_name>
    <place_of_publication>
      <publication_title>Mem. New York Bot. Gard.</publication_title>
      <place_in_publication>74(1): 175.  1996</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fabaceae;subfamily caesalpinioideaemimosoidclade;genus ebenopsis;species ebano</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Mimosa</taxon_name>
    <taxon_name rank="species" authority="Berlandier in P. de La Llave" date="1840">ebano</taxon_name>
    <place_of_publication>
      <publication_title>Mosaico Mex.</publication_title>
      <place_in_publication>4: 418.  1840</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus mimosa;species ebano</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Acacia</taxon_name>
    <taxon_name rank="species" authority="Bentham" date="unknown">flexicaulis</taxon_name>
    <taxon_hierarchy>genus acacia;species flexicaulis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Chloroleucon</taxon_name>
    <taxon_name rank="species" authority="(Berlandier) L. Rico" date="unknown">ebano</taxon_name>
    <taxon_hierarchy>genus chloroleucon;species ebano</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Ebenopsis</taxon_name>
    <taxon_name rank="species" authority="(Bentham) Britton &amp; Rose" date="unknown">flexicaulis</taxon_name>
    <taxon_hierarchy>genus ebenopsis;species flexicaulis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Pithecellobium</taxon_name>
    <taxon_name rank="species" authority="(Berlandier) C. H. Muller" date="unknown">ebano</taxon_name>
    <taxon_hierarchy>genus pithecellobium;species ebano</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">P.</taxon_name>
    <taxon_name rank="species" authority="(Bentham) J. M. Coulter" date="unknown">flexicaule</taxon_name>
    <taxon_hierarchy>genus p.;species flexicaule</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">P.</taxon_name>
    <taxon_name rank="species" authority="J. M. Coulter" date="unknown">texense</taxon_name>
    <taxon_hierarchy>genus p.;species texense</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Samanea</taxon_name>
    <taxon_name rank="species" authority="(Bentham) J. F. Macbride" date="unknown">flexicaulis</taxon_name>
    <taxon_hierarchy>genus samanea;species flexicaulis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Siderocarpos</taxon_name>
    <taxon_name rank="species" authority="(Bentham) Small" date="unknown">flexicaulis</taxon_name>
    <taxon_hierarchy>genus siderocarpos;species flexicaulis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Zygia</taxon_name>
    <taxon_name rank="species" authority="(Bentham) Sudworth" date="unknown">flexcaulis</taxon_name>
    <taxon_hierarchy>genus zygia;species flexcaulis</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Ebony black-seed</other_name>
  <other_name type="common_name">ebano</other_name>
  <description type="morphology">Trees 4–6(–20) m, with rounded crown.  Stems gray-ochre, with few horizontal lenticels; bark with vertical fissures.  Leaves: stipules persistent, to 1 cm; petiole to 0.7–1.5 cm, glabrous; rachis to 1 cm; pinna rachis 2–3.5 cm, with cylindric, elevated gland between pin­nae; leaflet blades oblong to rhombic-oblong, 5–12 × 2.5–8 mm, base oblique to semicordate, apex rounded.  Peduncles 2–6 grouped, ebracteate, pubescent.  Spikes: axis 2–5 cm, pubescent; bract triangular, 0.5 mm, pubescent abaxially.  Flowers: calyx 1 mm; corolla campanulate, 4–5 mm, 4–6-lobate, glabrescent; stamens white or cream, staminal tube to 5 mm; ovary subsessile, to 1.5 mm, glabrous.  Legumes falcate, 11–13.5(–22) × 2.5–3 cm, base rounded, apex cuspidate with recurved beak to 5 mm, valves woody, smooth, surface cracking with age into a mosaic like pattern, sometimes with few verruca; mesocarp well developed, endocarp forming pithy septa between seeds, without visible margin.  Seeds maroon, 17–22.5 × 9–13 mm, pleurogram closed, testa thin.</description>
  <discussion>Ebenopsis ebano is found in southern Texas and is cultivated in the southeastern United States as an ornamental.  The wood of this species is extremely hard.</discussion>
  <description type="phenology">Flowering summer.</description>
  <description type="habitat">Roadside thickets, scrub.</description>
  <description type="elevation">0–150 m.</description>
  <description type="distribution">Tex.; Mexico (Nuevo León, San Luis Potosí, Tamaulipas, Yucatán).</description>
</bio:treatment>
