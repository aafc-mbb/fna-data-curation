<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">11</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Lindley" date="unknown">FABACEAE</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">Faboideae</taxon_name>
    <taxon_name rank="genus" authority="Rydberg in N. L. Britton et al." date="1919">PEDIOMELUM</taxon_name>
    <taxon_name rank="species" authority="S. L. Welsh" date="2010">verdiense</taxon_name>
    <place_of_publication>
      <publication_title>Licher &amp; N. D. Atwood, W. N. Amer. Naturalist</publication_title>
      <place_in_publication>70: 12, fig. 3.  2010</place_in_publication>
      <other_info_on_pub>(as verdiensis)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family fabaceae;subfamily faboideae;genus pediomelum;species verdiense</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Pediomelum</taxon_name>
    <taxon_name rank="species" authority="S. L. Welsh, Licher &amp; N. D. Atwood" date="unknown">pauperitense</taxon_name>
    <taxon_hierarchy>genus pediomelum;species pauperitense</taxon_hierarchy>
  </taxon_identification>
  <number>14.</number>
  <other_name type="common_name">Verde breadroot</other_name>
  <description type="morphology">Herbs acaulescent to short-caulescent, 4.5–13(–15) cm, mostly glandular (with obvious blond to dark brown glands) and pubescent throughout.  Stems erect, rarely with decum­bent laterals, unbranched or with few branches near base, spreading white hairy, leaves appearing clustered or dispersed along short stems; pseudoscapes to 6 cm (when present); cataphylls 0–15 mm, glabrous or pubescent.  Leaves palmately (3–)5(or 6)-foliolate; stipules tardily deciduous or persistent, lanceolate to elliptic, 4–16 × 2–8 mm, scarious, strigose to glabrate; petiole jointed basally, 10–100(–115) mm, hairs appressed-ascending; petio­lules 1.5–3 mm; leaflet blades abaxially gray-green, adaxially green to yellow-green, cuneate-obovate, (0.8–)1.2–3 × 0.7–1.8(–2.2) cm, base cuneate, apex broadly acute to rounded or retuse, surfaces glandular and pubescent, with more hairs abaxially and also along veins adaxially.  Peduncles (0.5–)1–5.5(–6) cm, shorter than subtending petiole, pilose, spreading or spreading-ascending white-hairy, sometimes with longer spreading hairs.  Inflorescences disjointing in age at peduncle base, cymose; rachis 1–3 cm, nodes (1 or)2–4(–6), (2 or)3 flowers per node; bracts tardily deciduous or persis­tent, elliptic, 3.5–8.5(–10) × 2–6 mm, strigose.  Pedicels 2.5–4.5(–6) mm.  Flowers (8–)10–13.5(–15) mm; calyx gibbous-campanulate in fruit, (7–)9–11.5 mm abax­ially, (7–)9–11 mm adaxially, glandular, with blond glands obscured by indument, pubescent; tube (3.5–)4–5 mm; lobes lanceolate to oblong or elliptic, abax­ial (4–)5–9 × (1.5–)2–3.5 mm, adaxial 4–7(–8) × 1–2.5 mm; corolla white to purple, banner white, cream, purple, or suffused with pale purple, wings and keel dark purple, wings sometimes lighter, banner broadly elliptic to ± oblanceolate, (7–)9–12(–14) × 6–8 mm with claw 2–5 mm, wings 10–13 × 2–3 mm with claw 4–5 mm, keel 8–10 × 2–3.5 mm with claw 3–5 mm; filaments 7–8.5 mm; anthers elliptic, 0.3 mm; ovary glabrous or apically pubescent, style glabrous or pubescent proximally.  Legumes round to ovoid, 5–7 × 3.5–5 mm, eglandular, pubescent, beak 1–4 mm, not exserted beyond calyx.  Seed olive to gray-brown, with or without purple mottling, oval to reniform, 3.5–5 × 2.5–3 mm, shiny.</description>
  <discussion>Pediomelum verdiense and P. pauperitense were described as separate taxa by S. L. Welsh and M. H. Licher (2010), with P. verdiense endemic to the silty, white limestone soil of the Verde Formation, Yavapai County, and P. pauperitense endemic to the pink lime­stone soil of Poverty Mountain, Mohave County.  Pediomelum pauperitense was described as having smaller flowers, pedicels, bracts, and seeds.  Morpho­metric analysis showed extensive overlap in all quantitative characters between P. pauperitense and P. verdiense (A. N. Egan 2015).  Beyond range and substrate differences, P. pauperitense is said to differ from P. verdiense by having banner and wings suffused with purple, more upright leaves with leaflets held above the inflorescences, and less silvery vestiture—traits that some would argue merit recognition of P. pauperitense at least at varietal status.  Both peduncle and petiole length overlap between the taxa, and live specimens of both taxa exhibit inflorescences held below the leaves, and vestiture density varies greatly, even within a population.  Banner color may be a character difference, but further research into soil substrate specificity and impacts of soil pH on flower color is needed.  Pediomelum pauperitense may represent a hybrid between P. mephiticum and P. verdiense, as several characters overlap or are intermediate between the two.  Molecular phylogenetic work would help to clarify the relationships of P. verdiense with morphologically similar congeners, including P. californicum and P. mephiticum.</discussion>
  <description type="phenology">Flowering spring–summer.</description>
  <description type="habitat">Limestone soils, desert scrub and pinyon-juniper communities.</description>
  <description type="elevation">1000–1700 m.</description>
  <description type="distribution">Ariz.</description>
</bio:treatment>
