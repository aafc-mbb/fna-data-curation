<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">11</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Lindley" date="unknown">FABACEAE</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">Faboideae</taxon_name>
    <taxon_name rank="genus" authority="Fabricius" date="1759">APIOS</taxon_name>
    <taxon_name rank="species" authority="Medikus" date="1787">americana</taxon_name>
    <place_of_publication>
      <publication_title>Vorles. Churpfälz. Phys.-Öcon. Ges.</publication_title>
      <place_in_publication>2: 355.  1787</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fabaceae;subfamily faboideae;genus apios;species americana</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Glycine</taxon_name>
    <taxon_name rank="species" authority="Linnaeus" date="1753">apios</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 753.  1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus glycine;species apios</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Apios</taxon_name>
    <taxon_name rank="species" authority="unknown" date="unknown">americana</taxon_name>
    <taxon_name rank="variety" authority="Fernald" date="unknown">turrigera</taxon_name>
    <taxon_hierarchy>genus apios;species americana;variety turrigera</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">Vines twining or clambering; tubers 4–12, oblong, oval, or globose, 2–10 cm diam., fleshy.  Stems green to brownish green or brown, glabrous or tomen­tose.  Leaves 10–22 cm; rachis terete, 10–32 mm between lateral and terminal leaf­lets, 16–37 mm between lateral leaflets, glabrous or veluti­nous; stipules 4–6.5 × 0.3–0.6 mm; petiole 20–58 mm, glabrous or velutinous; pulvinus 4.5–7 mm, glabrous or velutinous; stipels linear-triangular, 0.5–1 × 0.1–0.3 mm, hairs scattered or sericeous; petiolule (1.5–)2.8–4 mm, slightly hairy to velutinous; leaflet blades ovate to ovate-lanceolate, 47–70(–90) × 21–42 mm (blades of ramal branches 30–45 × 12–20 mm), base rounded (often asymmetrical), apex acuminate to acute, apiculate, surfaces glabrate to tomentose abaxially, usually denser on major veins, glabrous or puberulous adaxially, usually denser on major veins.  Inflorescences 40–60-flowered, 3–14 cm; bracts usually deciduous, rarely persistent, 2–2.8 mm; bracteoles 2–3 × 0.3–0.5 mm.  Pedicels 2–3 mm, glabrous or velutinous.  Flowers in clusters of 2 or 3 on inflated tubercles; calyx green, red and green, or pink-red, hemispheric to campanulate, 2.8–3.4 mm, glabrous or puberulous, abaxial lobe lanceolate to narrowly triangular, 1.3–1.8 × 0.4–0.6 mm, lateral lobes triangular to shallowly so, 0.2–0.4 × 0.7–0.9 mm, adaxial lobe almost obsolete or broadly rounded with acute, triangular apex, 0.2–0.3 × 0.1–0.2 mm; corolla deep to pale maroon and white; banner oblate, 10.5–12.5 × 14–16 mm, stylobos 1.5–2 mm; wings obovate, falcate, 9.5–10.5 × 4.3–4.8 mm; keel strongly incurved, slit apically after pollination, 12–14 × 2–4 mm; stamens 15.5–17 mm, filaments connate most of their length; pistil disk 0.9–1.2 mm; ovary 5.5–7 × 0.4–0.6 mm, glabrous or slightly hairy along sutures; style 6–7.5 mm, glabrous; ovules 6–11.  Legumes olive green to tannish brown, 6–10(–12) × 0.6–0.7 cm, base acute, apex aristate to acuminate; endocarp white.  Seeds 6–11, olive green becoming brown to reddish brown when dry, ellipsoid to broadly oblong, 5–6 × 3.5–4.5 mm, not glaucous; hilum 0.8–2 × 0.3–0.4 mm.  2n = 22.</description>
  <discussion>Six infraspecific taxa of Apios americana have been described.  The highly variable characters of A. americana are so overlapping that no definite lines of demarcation can adequately separate the infraspecific taxa (M. Woods 2005).</discussion>
  <discussion>Species of Megachile (leaf-cutter bees) are the only visitors reported to trip the flowers of Apios americana in the northern half of its range and are the only likely pollinators there.  Megachile species are the only insects observed tripping the flowers in the southern part of its range; two additional types of bees, honeybees (Apidae) and members of the Halictidae, are frequent visitors there but have not been observed tripping the flowers (A. Bruneau and G. J. Anderson 1988, 1994).</discussion>
  <discussion>Diploid and triploid populations of Apios americana are almost entirely restricted to different sections of the overall geographical range.  Triploid individuals are pri­marily located in the section of eastern North American that was covered by ice during the Wisconsinan glacia­tion (areas north of central Ohio, southern Indiana, central Iowa, Pennsylvania, and central Wisconsin).  Dip­loid individuals also occur in the Wisconsinan gla­ciation area but are more abundant outside of the area in the southern part of the range.  Triploidy is consid­ered to have evolved at least four times.  Clones east of the Appalachian Mountains have light-colored petals and little stem indument; the western clones have dark-colored petals and heavy stem indument (S. Joly and A. Bruneau 2004).</discussion>
  <discussion>Apios tuberosa Moench (1794) is an illegitimate, superfluous name that pertains here.</discussion>
  <description type="phenology">Flowering summer–fall.</description>
  <description type="habitat">Wet soils, along streams and lakes.</description>
  <description type="elevation">0–1600 m.</description>
  <description type="distribution">N.B., N.S., Ont., Que.; Ala., Ark., Colo., Conn., Del., D.C., Fla., Ga., Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mass., Mich., Minn., Miss., Mo., Nebr., N.H., N.J., N.Y., N.C., Ohio, Okla., Pa., R.I., S.C., S.Dak., Tenn., Tex., Vt., Va., W.Va., Wis.</description>
</bio:treatment>
