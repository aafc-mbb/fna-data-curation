<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">11</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Lindley" date="unknown">FABACEAE</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">Faboideae</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">ASTRAGALUS</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Reventi-arrecti</taxon_name>
    <taxon_name rank="species" authority="E. Sheldon" date="1894">eremiticus</taxon_name>
    <place_of_publication>
      <publication_title>Minnesota Bot. Stud.</publication_title>
      <place_in_publication>1: 161.  1894</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fabaceae;subfamily faboideae;genus astragalus;section reventi-arrecti;species eremiticus</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>147.</number>
  <other_name type="common_name">Hermit milkvetch</other_name>
  <description type="morphology">Plants 20–55(–63) cm, sparsely to densely strigulose, hairs basifixed; from branched, superficial caudex.  Stems erect or ascending, sparsely to densely strigulose.  Leaves (3–)4.5–18 cm; stipules 3–11 mm, papery-scarious at proximal nodes, subherbaceous at distal nodes; leaflets 11–25, blades ovate, oblong, elliptic, or narrowly oblong to linear, 5–26 mm, apex obtuse to retuse, surfaces strigose abaxially, usually glabrous adaxially, sometimes glabrate.  Peduncles erect, (1.5–)2.5–17(–35) cm.  Racemes (7–)10–26(–30)-flowered, flowers ascending to declined; axis 4–10(–15) cm in fruit; bracts 1.5–4 mm; bracteoles 0–2.  Pedicels spreading in fruit, 0.7–3.5 mm.  Flowers 12–20 mm; calyx short-cylindric, 5.4–9.5 mm, strigose, tube 4.4–8 mm, lobes triangular to subulate, 0.7–2.6 mm; corolla ochroleucous, tinged pink, purplish, pink-purple, or red-violet, keel maculate or immaculate; banner recurved through 45°; keel 9.4–14.4 mm.  Legumes usually erect, rarely deflexed, green or suffused or speckled with purple becoming brownish stramineous, oblong to ellipsoid, 3-sided compressed, grooved dorsally, keeled ventrally, 12–27(–30) × 3.5–8(–9) mm, base cuneate, ± bilocular, fleshy becoming thinly or stiffly leathery, glabrous; septum 0.5–1.2 mm wide; stipe 6–15(–17) mm.  Seeds 17–32.  2n = 22, 24.</description>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <description type="distribution">w United States.</description>
  <discussion>Astragalus eremiticus displays extensive variation.  R. C. Barneby (1964) discussed five unnamed morpho­logical and ecological races in place of fewer varieties that he earlier recognized (1944, 1949).  D. Isely (1998) recognized two varieties, as presented here.  He con­sidered var. ampullarioides Welsh a vigorous form of the species but did not place it into synonymy in either of the varieties (by his map it would be in var. eremiticus).  That entity is here considered a species (as 163. A. ampullarioides).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Calyces basally symmetric; pedicels medially attached to calyces, ascending; racemes lax, axes 5–10(–15) cm in fruit; nw Arizona, se Nevada, disjunct in sw New Mexico, sw Utah.</description>
      <determination>147a. Astragalus eremiticus var. eremiticus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Calyces basally asymmetric; pedicels attached to calyces on one side, spreading to descending; racemes compact, axes 4–6(–15) cm in fruit; sw Idaho, ec to n Nevada, e, se Oregon.</description>
      <determination>147b. Astragalus eremiticus var. spencianus</determination>
    </key_statement>
  </key>
</bio:treatment>
