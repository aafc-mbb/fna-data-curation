<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>Ralph L. Thompson</author>
    </source>
    <other_info_on_meta type="volume">11</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Lindley" date="unknown">FABACEAE</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">CaesalpinioideaeMimosoidClade</taxon_name>
    <taxon_name rank="genus" authority="Bentham" date="1844">LYSILOMA</taxon_name>
    <place_of_publication>
      <publication_title>London J. Bot.</publication_title>
      <place_in_publication>3: 82.  1844</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fabaceae;subfamily caesalpinioideaemimosoidclade;genus lysiloma</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek lysis, loosening, separation, and loma, border, margin, alluding to separation of continuous, thickened margins from legume valves as they disintegrate</other_info_on_name>
  </taxon_identification>
  <number>42.</number>
  <description type="morphology">Trees or shrubs, unarmed.  Stems erect to ascending; bark smooth to furrowed, scaly, or flaking on older branches or trunk; branchlets glabrous or pubescent.  Leaves alternate, even-bipinnate; stipules present; petiolate, petiole terete, rachis and rachilla terete-trigonous, pulvinate; nectaries present; pinnae [1 or]2–10[–50] pairs, opposite; leaflets 6–56, opposite, blade margins entire, surfaces pilose, glabrescent, or glabrous.  Peduncles 2–7, from leaf axils or in relatively small, axillary racemes.  Inflorescences 28–70-flowered, axillary, spikes or racemes, solitary or fasciculate; bracts present or absent; bracteoles present.  Flowers mimosoid, actinomorphic, buds green-canescent; calyx campanulate or campanulate-tubular, lobes 5, synsepalous, valvate in bud; corolla sympetalous, red, white, greenish white, or yellowish green, valvate in bud; stamens 14–36, exserted, monadelphous, connate basally into slender tube; filaments conspicuous, filiform, white to greenish or yellowish white; anthers versatile, oblong, eglandular; ovary superior, free, glabrous; style subulate-filiform; stigma terminal, concave, glabrous.  Fruits legumes, stipitate, laterally compressed, straight, broadly to narrowly oblong, dehiscent or indehiscent or craspedial, glabrous, strongly compressed over seeds, not septate between seeds; sutures prominent, exocarp dark purple to ripe-nigrescent, exfoliating, endocarp pale stramineous, papery to leathery, not exfoliating, not fleshy or pulpy, margins wiry.  Seeds 7–15, compressed, oval or oblong to ellipsoid, smooth, hard, opaque; pleurogram distinct; endosperm absent.  x = 13.</description>
  <discussion>Species 9 (3 in the flora).</discussion>
  <description type="distribution">s United States, Mexico, West Indies, Central America.</description>
  <discussion>Legumes in Lysiloma are either dehiscent (readily dehiscent at maturity, with undivided margins separating from the valves, and seeds that are released through dehiscence of the valve sutures) or indehiscent (indehiscent at maturity, with valves tardily separating from the persistent margins, and seeds that are released through decay of the valves on the ground).</discussion>
  <references>
    <reference>Thompson, R. L.  1980.  Revision of the Genus Lysiloma (Leguminosae).  Ph.D. dissertation.  Southern Illinois University, Carbondale.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Legumes dehiscent, stipes 0.3–1.3 cm; Arizona.</description>
      <determination>1. Lysiloma watsonii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Legumes indehiscent, stipes 0.8–3.8 cm; Florida.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaflets 6–12, blades elliptic to obovate, apex obtuse to retuse, 12–35 × 7–23 mm, venation palmate-pinnate; pinnae 6–10; stipule base cuneate-flabellate; peduncles ebracteate; pedicels present; legume apex rounded to emarginate; pleurogram elliptic.</description>
      <determination>2. Lysiloma sabicu</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaflets 30–56, blades oblong, apex acute to narrowly obtuse, 8–15 × 3–6 mm, venation pinnate; pinnae 4–12; stipule base auriculate-semicordate; peduncles bracteate; pedicels absent; legume apex acuminate; pleurogram U-shaped.</description>
      <determination>3. Lysiloma latisiliquum</determination>
    </key_statement>
  </key>
</bio:treatment>
