<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">11</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Lindley" date="unknown">FABACEAE</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">Faboideae</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">PHASEOLUS</taxon_name>
    <taxon_name rank="species" authority="(Linnaeus) Britton" date="1888">polystachios</taxon_name>
    <place_of_publication>
      <publication_title>Sterns &amp; Poggenburg, Prelim. Cat.,</publication_title>
      <place_in_publication>15.  1888</place_in_publication>
      <other_info_on_pub>(as polystachyus)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family fabaceae;subfamily faboideae;genus phaseolus;species polystachios</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Dolichos</taxon_name>
    <taxon_name rank="species" authority="Linnaeus" date="1753">polystachios</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 726.  1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus dolichos;species polystachios</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Phaseolus</taxon_name>
    <taxon_name rank="species" authority="Michaux" date="unknown">paniculatus</taxon_name>
    <taxon_hierarchy>genus phaseolus;species paniculatus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">P.</taxon_name>
    <taxon_name rank="species" authority="Walter" date="unknown">perennis</taxon_name>
    <taxon_hierarchy>genus p.;species perennis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">P.</taxon_name>
    <taxon_name rank="species" authority="unknown" date="unknown">polystachios</taxon_name>
    <taxon_name rank="variety" authority="Fernald" date="unknown">aquilonius</taxon_name>
    <taxon_hierarchy>genus p.;species polystachios;variety aquilonius</taxon_hierarchy>
  </taxon_identification>
  <number>7.</number>
  <other_name type="common_name">Bean-vine</other_name>
  <other_name type="common_name">thicket or wild kidney bean</other_name>
  <other_name type="common_name">sacsac</other_name>
  <description type="morphology">Vines perennial, with tuberous taproots.  Stems climbing or trailing, 100–400+ cm.  Leaves: stipules spreading or reflexed, triangular to lanceolate, 1.5–3.5 × 0.5–1.5 mm, often strigillose; petiole (2–)3–6(–9) cm; rachis (0.5–)1–2(–2.5) cm; stipels ovate to lanceolate, 1–2.5 mm; leaflet blades ovate to rhombic-ovate, (1–)3–13 × (1–)3–12 cm, membranous to leathery, venation not reticulate, base rounded to depressed-ovate, apex acute to acuminate or obtuse, surfaces abaxially sparsely to densely covered with ascending and uncinate hairs, adaxially glabrous or sparsely covered with ascending and uncinate hairs.  Peduncles (1–)5–25 cm.  Inflorescences usually with basal and lateral branches developed along main axis (compound raceme), 8–55 cm; axis sparsely to densely covered with uncinate and often ascending hairs; rachis (2.5–)8–27.5 cm, with 5–32 often scattered, biflorous nodes, secondary rachis often developed with 3–5 flowers; primary bracts ovate to lanceolate, 1–4 × 0.3–1.2 mm, 3-veined, glabrous or strigillose; secondary bracts usually caducous, ovate, 0.5–2.5 × 0.4 mm.  Pedicels 4–9 mm, sparsely covered with uncinate, and often, ascending hairs; bracteoles usually persistent, ovate to oblong, 0.5–1.2 mm.  Flowers: calyx campanulate, 3–3.5 mm, glabrous or sparsely covered with ascending hairs; abaxial and lateral lobes triangular; adaxial lobes connate; corolla usually pink to light purple, rarely white, 10–12 mm; banner usually wider than long, 5–8 mm, apex emarginate, glabrous; wings obovate, 10–12 mm; keel 6–7.5 mm; ovary linear, 5 mm; style twisted at apex.  Legumes pendent, compressed, oblong-falcate, (30–)45–78(–86) × 7.5–13 mm, elastically dehiscent, valves papery to slightly leathery, strigillose.  Seeds 4–6, light brown, blackish mottled, oblong to reniform, 6–13 × (4–)5–8.3 mm, smooth; hilum oblanceolate, (1.6–)2.3–3.2 mm.  2n = 22.</description>
  <discussion>Phaseolus polystachios is distinguished from other species of the genus by a combination of characters including its large inflorescences, with the presence of basal and lateral floral branches, floral nodes usually scattered along the axis, and banners without transverse thickenings.</discussion>
  <description type="phenology">Flowering Jul–Oct.</description>
  <description type="habitat">Open or shaded, deciduous woodlands, stream banks, thickets, rocky, sandy, or alluvial soils.</description>
  <description type="elevation">0–900 m.</description>
  <description type="distribution">Ala., Ark., Conn., Del., D.C., Fla., Ga., Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mass., Mich., Minn., Miss., Mo., Nebr., N.H., N.J., N.Y., N.C., Ohio, Okla., Pa., R.I., S.C., Tenn., Tex., Vt., Va., W.Va.</description>
</bio:treatment>
