<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>Billie L. Turner†</author>
    </source>
    <other_info_on_meta type="volume">11</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Lindley" date="unknown">FABACEAE</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">Faboideae</taxon_name>
    <taxon_name rank="genus" authority="Rafinesque" date="1832">ORBEXILUM</taxon_name>
    <place_of_publication>
      <publication_title>Atlantic J.</publication_title>
      <place_in_publication>1: 145.  1832</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fabaceae;subfamily faboideae;genus orbexilum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="etymology">Latin orbis, circular, and exilum, spreading or vexillum, alluding to shape of banner in O. latifolium, the type species</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="Rydberg" date="unknown">Rhytidomene</taxon_name>
    <taxon_hierarchy>genus rhytidomene</taxon_hierarchy>
  </taxon_identification>
  <number>112.</number>
  <other_name type="common_name">Snakeroot or leather-root</other_name>
  <description type="morphology">Herbs, perennial, unarmed; with lignescent rhizome, tuber, or fusiform taproot.  Stems erect to ascending, usually gland-dotted, pubescent or glabrous.  Leaves alternate, unifoliolate, palmate, or odd-pinnate; stipules present, persistent or caducous; petiolate; stipels absent; leaflets 1–7, blade margins entire, surfaces glandular-punctate or eglandular, pubescent or glabrous.  Inflorescences pedunculate, 5–50-flowered, terminal, spicate; bracts present; bracteoles absent.  Flowers papilionaceous; calyx tubular-campanulate, lobes 5, lobes 2–3 times tube length; corolla violet, purple, or purplish blue, banner, wings, and keel well developed, keel connate apically; stamens 10, diadelphous or proximally monadelphous; anthers in 2 series, proximal dorsifixed, distal basifixed, introrse.  Fruits loments, dark brown to black, subsessile, asymmetric or curved, flattened, round-obovate to obovate, 0.4–1.2 cm, indehiscent, thickly leathery, rugose or papillose, glandular-punctate or eglandular, glabrous.  Seeds 1, reniform, obovate, depressed-obovate, or round-obovate.  x = 11.</description>
  <discussion>Species 11 (8 in the flora).</discussion>
  <description type="distribution">c, se United States, Mexico.</description>
  <discussion>As treated by J. W. Grimes (1990), Orbexilum comprises closely related species confined to North America, this largely confirmed by DNA data (A. N. Egan and K. A. Crandall 2008).  B. L. Turner (2008b) proposed three additional species known from Mexico [O. chiapasanum B. L. Turner, O. melanocarpum (Bentham) Rydberg, and O. oliganthum (Brandegee) B. L. Turner], bringing to 11 the number of species recognized for the genus.</discussion>
  <references>
    <reference>Turner, B. L.  2008b.  Revision of the genus Orbexilum (Fabaceae: Psoraleeae).  Lundellia 11: 1–7.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves unifoliolate.</description>
      <determination>1. Orbexilum virgatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves 3–7-foliolate (sometimes unifoliolate in O. simplex).</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves mostly palmately (3–)5–7-foliolate.</description>
      <determination>2. Orbexilum lupinellus</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves pinnately 3-foliolate (sometimes unifoliolate or palmately 5–7-foliolate in O. simplex).</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaflet blades: base cordate.</description>
      <determination>3. Orbexilum macrophyllum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaflet blades: base not cordate.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Herbs eglandular; stipules narrowly oblong, 10–13 mm.</description>
      <determination>4. Orbexilum stipulatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Herbs ± glandular-punctate, at least adaxial leaflet surfaces; stipules long-triangular, lanceolate, or linear, 3–10 mm.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Legumes papillose or warty.</description>
      <determination>5. Orbexilum onobrychis</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Legumes rugose.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Flowers 8–10 mm.</description>
      <determination>8. Orbexilum simplex</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Flowers 4–7 mm.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Bracts, calyces, and legumes markedly glandular-punctate; bracts ovate, 5–8 × 2–4 mm; Atlantic Coastal Plain.</description>
      <determination>6. Orbexilum psoralioides</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Bracts, calyces, and legumes eglandular; bracts lanceolate to narrowly ovate, 5–8 × 1–2.5 mm; sc United States.</description>
      <determination>7. Orbexilum pedunculatum</determination>
    </key_statement>
  </key>
</bio:treatment>
