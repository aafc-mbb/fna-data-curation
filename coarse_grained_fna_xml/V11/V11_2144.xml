<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>Ernest Small</author>
    </source>
    <other_info_on_meta type="volume">11</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Lindley" date="unknown">FABACEAE</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">Faboideae</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">MEDICAGO</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 778.  1753</place_in_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 339.  1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fabaceae;subfamily faboideae;genus medicago</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="etymology">Latin medica, from Medes (Media), and ago, connection, alluding to a kind of cultivated forage clover supposedly native to Media</other_info_on_name>
  </taxon_identification>
  <number>149.</number>
  <other_name type="common_name">Alfalfa</other_name>
  <other_name type="common_name">lucerne</other_name>
  <other_name type="common_name">medic</other_name>
  <description type="morphology">Herbs [shrubs], annual, biennial, or perennial, unarmed, sometimes base woody; roots fibrous or branched taproots; rhizomes sometimes present.  Stems prostrate, procumbent, decumbent, ascending, or erect, branched or unbranched, especially when small or crowded by vegetation, usually slightly to moderately pubescent, hairs eglandular or sometimes gland-tipped, sometimes septate.  Leaves alternate, odd-pinnate; stipules present, margins entire, toothed, or lobed, glabrous or pubescent, hairs eglandular and/or gland-tipped; epulvinate; petiolate; leaflets 3(5 or 7), pulvinate, blade margins partly serrate, sometimes laciniate or incised, surfaces glabrous or pubescent, hairs eglandular and/or gland-tipped, especially abaxially.  Inflorescences 1–50-flowered, axillary or terminal, usually racemes or heads, sometimes umbels or flowers subsolitary; bracts present or absent.  Flowers papilionaceous; calyx funnelform to campanulate, lobes 5, triangular to lanceolate, equal or subequal (adaxial pair longer); corolla usually yellow or orange-yellow, rarely purple, violet, variegated violet-yellow, or white; banner subequal to or longer than wings and keel; wings strongly adherent to keel by wing spur in keel invagination; keel rounded, not strongly incurved, hooding stamens; stamens 10, diadelphous; anthers dorsifixed, uniform, distinct portion of alternating filaments often thick.  Fruits legumes, tan to brown or black, sessile or short-stipitate, terete, compressed, or flat [moniliform, plicate], straight, curved, or coiled, indehiscent or dehiscent at sutures; dorsal sutures prickly, tuberculate, or smooth [winged and fimbriate], faces papery, soft, or thickly leathery, sometimes with proliferative alveolar, spongy tissue, glabrous or pubescent, hairs eglandular and/or glandular, intersuture venation obscure to prominent, often anastomosing in species-specific patterns, sometimes a submarginal vein present, parallel to dorsal suture but somewhat remote from it.  Seeds 1–30, usually reniform, sometimes oval, triangular, or rhomboid-ovoid, smooth, rugose, or tuberculate.  x = 7, 8, 9.</description>
  <discussion>Species ca. 85 (13 in the flora).</discussion>
  <description type="distribution">introduced; Europe, Asia, n Africa; introduced also nearly worldwide in temperate and tropical areas.</description>
  <discussion>Several species of Medicago not treated here have been recorded very rarely in the flora area, particularly as coastal, ballast waifs.  These include Medicago disciformis de Candolle, collected in 1949 from Worcester County, Massachusetts (Johnson s.n., DINH, now transferred to CONN); M. monantha (C. A. Meyer) Trautvetter collected in 1884 from Middlesex County, Massachusetts (Swan s.n., NEBC); and M. sphaerocarpa Bertoloni, collected in 2016 from Santa Catalina Island, California, by C. M. Guilliams (SBBG).  Several annual species are present in parts of the flora area because they escape from experimental or commercial forage cultivation; additional introduced species will likely be discovered, and because the majority are of Mediterranean origin, it will likely be in California and the southern states.  All of the species treated here have shown weedy tendencies in some areas outside of the flora area, while some are weeds in the flora area.  None is significantly troublesome.</discussion>
  <discussion>Medicago sativa is arguably the most important temperate region forage plant in the world.  Several other perennial species, and, more commonly, annual species, which are commonly called medics or medicks, are cultivated for forage, but infrequently in the flora area.</discussion>
  <discussion>Species in sect. Spirocarpos, all with coiled fruits, normally have two rows of prickles on the dorsal suture, but in some plants these are reduced to tubercles or the dorsal sutures are without ornamentation.  Cultivars often have been selected for lack of fruit prickles, as this prevents entanglement in fur and makes the nutritious fruits easier for livestock consumption.  Cultivars have been grown in the flora area, increasing the likelihood that prickleless-fruited forms will be encountered.  The prickles protect the fruit, serve to adhere to fur for distribution, and perhaps also to facilitate burial of the indehiscent fruits in soil.  In their native area, prickly and prickleless forms occur randomly on a geographic basis.  The literature is replete with extensive infraspecific classifications of the species on the basis of presence and length of prickles.</discussion>
  <discussion>In the nineteenth and twentieth centuries, Medicago was controversially separated from Trigonella, less controversially from Melilotus, with given species assigned to these and many other genera depending on taxonomic treatment.  Recent morphological, chemical, and molecular evidence has made it clear that Medicago is well demarcated from its nearest allies, including Trigonella, Melilotus, and Trifolium.  Medicago flowers have an irreversible explosively tripping pollination mechanism, which is degenerate in the annual species, but evident by a syndrome of floral characteristics.  Although fruits of Medicago are usually indispensable for species identification, flowers suffice for distinguishing Medicago from allied genera.  Contrasting characters possessed by Medicago but not related genera include: banner, except when very small, with major basal vein usually with more than three branches near base; horn of wing petal large, about one-third of wing limb; keel very strongly adherent to wing by a wing spur in a keel invagination; androecium with apex of fused portion of anther column arched, alternating distinct portions of filaments often relatively thick; stigma fungiliform; style always short.</discussion>
  <discussion>Several character states are unique to one or just a few species in the flora range.  Medicago arabica is the only species in which the fruit has a dorsal suture with a pattern of four ridges separated by three grooves.  Medicago lupulina is the only species that has one-seeded fruits.  Medicago monspeliaca is the only species that has deflexed fruit and a stellate inflorescence; M. monspeliaca and M. orbicularis are the only species with rough-surfaced seeds.  Medicago orbicularis is the only species with papery pod edges (although M. scutellata fruits are somewhat reminiscent).  Medicago sativa is the only species that has violet or purple flowers.  Medicago scutellata is the only species that has fruit resembling a stack of bowls.  Medicago rigidula, M. truncatula, M. turbinata, and sometimes M. polymorpha have fruits that are extremely hard-walled at maturity, while the remaining species have fruit walls that are flexible or relatively easily bent.</discussion>
  <discussion>Several forage species have a number of widely used common names, some often more or less confined to Europe, Australia, or different areas of the United States.  Only vernacular names that have been used repeatedly in the literature are reported here.</discussion>
  <discussion>Perennial species of Medicago are slightly to substantially outbreeding, while annual species are mostly strongly inbreeding.</discussion>
  <references>
    <reference>Bena, G.  2001.  Molecular phylogeny supports the morphologically based taxonomic transfer of the “medicagoid” Trigonella species to the genus Medicago L.  Pl. Syst. Evol. 229: 217–236.</reference>
    <reference>Heyn, C. C.  1963.  The Annual Species of Medicago.  Jerusalem.</reference>
    <reference>Lesins, K. A. and I. Lesins.  1979.  Genus Medicago (Leguminosae).  A Taxogenetic Study.  The Hague.</reference>
    <reference>Small, E.  1987.  Generic changes in Trifolieae subtribe Trigonellinae.  In: C. H. Stirton, ed.  1987.  Advances Legume Systematics.  Part 3.  Kew.  Pp. 159–181.</reference>
    <reference>Small, E.  2011.  Alfalfa and Relatives: Evolution and Classification of Medicago.  Ottawa and Wallingford.</reference>
    <reference>Small, E. and M. Jomphe.  1989.  A synopsis of the genus Medicago (Leguminosae).  Canad. J. Bot. 67: 3260–3294.</reference>
    <reference>Small, E., P. Lassen, and B. S. Brookes.  1987.  An expanded circumscription of Medicago (Leguminosae, Trifolieae) based on explosive flower tripping.  Willdenowia 16: 415–437.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Corollas usually uniformly purple or variegated yellow-violet, sometimes violet or greenish.</description>
      <determination>1. Medicago sativa</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Corollas yellow, rarely orange-yellow.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Legumes 1-seeded, nutletlike.</description>
      <determination>2. Medicago lupulina</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Legumes 2–30-seeded.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Legumes not coiled (often falcate), prickleless.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Flowers not stellate in inflorescences; legumes not deflexed.</description>
      <determination>1. Medicago sativa</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Flowers stellate in inflorescences; legumes deflexed.</description>
      <determination>13. Medicago monspeliaca</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Legumes coiled, prickly, prickleless, or tuberculate.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Legume coils conspicuously imbricate, concave (convex surface facing fruit base).</description>
      <determination>4. Medicago scutellata</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Legume coils ± flat.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Legume coils papery at margins, prickleless; seeds tuberculate.</description>
      <determination>3. Medicago orbicularis</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Legume coils not papery at margins, prickly or prickleless; seeds smooth.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Mature legumes extremely hard-walled (prickly and prickleless forms occur, coils of prickly forms cannot be separated by hand without probability of injury); prickles, when present, very stocky and difficult to bend; base of prickles often round, 2 roots often apparent at maturity; venation pattern often obscure on coil face of mature fruit because of proliferation of alveolar tissue.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Legumes: coil face with radial veins entering veinless margin in distal 1/4 or 1/3 of coil, prickles absent or reduced to tubercles, no more than 5 mm, sometimes inclined opposite to direction of fruit coiling (giving fruit appearance of rapidly spinning top).</description>
      <determination>5. Medicago turbinata</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Legumes: coil face with slightly branching and/or anastomosing veins, prickles or tubercles usually present, shorter to longer than 5 mm, not inclined opposite to direction of fruit coiling.</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Young legumes with glandular hairs, evident only in young pods, often producing velvety appearance; calyx with eglandular and/or glandular hairs.</description>
      <determination>6. Medicago rigidula</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Young legumes with eglandular hairs, if glandular hairs pres­ent, without velvety appearance; calyx usually with eglandu­lar hairs only.</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Legumes usually ovoid, sometimes discoid, cylindrical, or spherical, prickles, when present, at various angles to plane of coil; coils weakly to strongly adpressed, radial veins of coil face very strongly curved, veins in peripheral 1/3 of coil face slightly to moderately anastomosing, evident only in young pods.</description>
      <determination>6. Medicago rigidula</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Legumes usually cylindrical, prickles usually present, often at 90° to plane of coil, pointed towards ends of pod; coils often strongly adpressed; radial veins of fruit coil face weakly to moderately curved, veins in peripheral 1/3 of coil face weakly if at all anastomosing.</description>
      <determination>7. Medicago truncatula</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Mature legumes soft-walled (sometimes hard in M. polymorpha); prickles usually present, often relatively thin and flexible, base of prickles 2-rooted, one root arising in dorsal suture, other in submarginal vein; venation discernible on coil face of mature fruit.</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Legume coil edges with 4 ridges separated by 3 grooves; leaflet blades often with conspicuous, central, purple-red (anthocyanin) blotch adaxially.</description>
      <determination>8. Medicago arabica</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Legume coil edges with fewer than 4 ridges, if any; leaflet blades very rarely with central purple-red blotch.</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Outer periphery of legume coil faces with distinct veinless margin, venation not extending to dorsal suture.</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Stipule margins entire or minutely dentate at base; legumes: veinless margin from 1/3 of coil face radius away from dorsal suture; leaflet blades not laciniate.</description>
      <determination>9. Medicago minima</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Stipule margins deeply dentate to laciniate; legumes: coil face with veinless area from 1/5 of coil face radius away from dorsal suture; leaflet blades often laciniate.</description>
      <determination>10. Medicago laciniata</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Outer periphery of legume coil faces without distinct veinless margin, venation extending to dorsal suture.</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Flowers 3.5–6 mm; legumes 4–10 mm wide.</description>
      <determination>11. Medicago polymorpha</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Flowers 2–4 mm; legumes 2–3 mm wide.</description>
      <determination>12. Medicago praecox</determination>
    </key_statement>
  </key>
</bio:treatment>
