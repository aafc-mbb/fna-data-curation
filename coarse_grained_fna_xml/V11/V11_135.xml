<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>Debra K. Trock</author>
    </source>
    <other_info_on_meta type="volume">11</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Lindley" date="unknown">FABACEAE</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">CaesalpinioideaeMimosoidClade</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1767">PROSOPIS</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Nat. ed.</publication_title>
      <place_in_publication>12, 2: 282, 293.  1767</place_in_publication>
      <publication_title>Mant. Pl.</publication_title>
      <place_in_publication>1: 10, 68.  1767</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fabaceae;subfamily caesalpinioideaemimosoidclade;genus prosopis</taxon_hierarchy>
    <other_info_on_name type="etymology">Derivation uncertain; perhaps Greek prosopon, mask, alluding to similarity with Tamarindus masking identity of Prosopis</other_info_on_name>
  </taxon_identification>
  <number>22.</number>
  <other_name type="common_name">Mesquite</other_name>
  <description type="morphology">Trees or shrubs, usually armed, rarely unarmed; taprooted, forming underground spreading horizontal runners.  Branches ascending or spreading.  Stems glabrous or pubescent.  Leaves alternate, even-bipinnate; stipules present, inconspicuous and early deciduous or modified spines; petiolate, petiole with sessile, circular glands; pinnae 1 or 2(or 3)[–7] pair(s), opposite, apex with scalelike mucro or spine; leaflets 6–60, alternate or opposite, overlapping or not, blade margins entire, surfaces sometimes glaucous, glabrous or pubescent.  Inflorescences 40–100+-flowered, axillary, spikes or heads [racemes]; bracts absent.  Flowers mimosoid; calyx campanulate, lobes 5, connate proximally, sometimes striate; corolla yellow, cream-yellow, purple-brown, greenish white, or yellow-green, [reddish], petals connate or distinct, linear; stamens 10 (5 + 5), distinct; anthers dorsifixed, introrse, elliptic, apex pedicellate with a deciduous, capitate gland.  Fruits loments, stipitate or sessile, straight or spirally coiled, torulose, linear or cylindric, thickened, indehiscent, pubescent, glabrescent, or glabrous.  Seeds (4 or)5–25+, gray-green, tan, yellow-tan, yellow-brown, or brown, ovoid, reniform-ovoid, ellipsoid, or oblong.  x = 14.</description>
  <discussion>Species ca. 48 (6 in the flora).</discussion>
  <description type="distribution">sw, sc United States, Mexico, South America (Argentina, Bolivia, Chile, Peru), sw Asia, Africa; deserts, dry subtropical regions; introduced in Australia.</description>
  <discussion>Prosopis was treated by G. Bentham (1842, 1846, 1875) as polymorphic and divided into several sections based on fruit types and derivation of the spines.  G. Engelmann and A. Gray (1845), as well as N. L. Britton and J. N. Rose (1928), divided the North American species into two and three genera respectively.  A. Burkart (1976) adopted the position of Bentham, and most North American authors have continued to follow his treatment.</discussion>
  <discussion>In arid countries, species of Prosopis are valued for shade, fuel, food, and forage.  Due to their hardiness and abundance, they are often an important component of the vegetation in these regions.  Some species are invasive and are a problem for ranchers and farmers.  At least 27 species are listed as potentially noxious weeds in the United States.</discussion>
  <references>
    <reference>Burkart, A.  1976.  A monograph of the genus Prosopis (Leguminosae subfam. Mimosoideae).  J. Arnold Arbor. 57: 219–249.</reference>
    <reference>Burkart, A.  1976b.  A monograph of the genus Prosopis (Leguminosae subfam. Mimosoideae): Catalogue of the recognized species of Prosopis.  J. Arnold Arbor. 57: 450–524.</reference>
    <reference>Isely, D.  1972.  Legumes of the U.S.  VI.  Calliandra, Pithecellobium, Prosopis.  Madroño 21: 273–298.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Inflorescences globose heads; loments coiled.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Spines 4–9 cm; leaflets touching or overlapping.</description>
      <determination>4. Prosopis reptans</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Spines 0.1–2 cm; leaflets 3.5–5 mm apart.</description>
      <determination>5. Prosopis strombulifera</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Inflorescences amentlike spikes; loments straight or coiled.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Loments coiled; leaflets 10–18.</description>
      <determination>3. Prosopis pubescens</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Loments straight or curved; leaflets 12–60.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaflets alternate.</description>
      <determination>2. Prosopis laevigata</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaflets opposite.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaflets 5–18 mm apart, blades (15–)20–63 mm, surfaces glabrous.</description>
      <determination>1. Prosopis glandulosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaflets 3–4 mm apart, blades 4–13 mm, surfaces pubescent.</description>
      <determination>6. Prosopis velutina</determination>
    </key_statement>
  </key>
</bio:treatment>
