<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">11</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Lindley" date="unknown">FABACEAE</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">Faboideae</taxon_name>
    <taxon_name rank="genus" authority="Rydberg in N. L. Britton et al." date="1919">PEDIOMELUM</taxon_name>
    <taxon_name rank="species" authority="(Payson) W. A. Weber" date="1983">aromaticum</taxon_name>
    <place_of_publication>
      <publication_title>Phytologia</publication_title>
      <place_in_publication>53: 188.  1983</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fabaceae;subfamily faboideae;genus pediomelum;species aromaticum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Psoralea</taxon_name>
    <taxon_name rank="species" authority="Payson" date="1915">aromatica</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>60: 379.  1915</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus psoralea;species aromatica</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Pediomelum</taxon_name>
    <taxon_name rank="species" authority="unknown" date="unknown">aromaticum</taxon_name>
    <taxon_name rank="variety" authority="S. L. Welsh" date="unknown">ambiguum</taxon_name>
    <taxon_hierarchy>genus pediomelum;species aromaticum;variety ambiguum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">P.</taxon_name>
    <taxon_name rank="species" authority="unknown" date="unknown">aromaticum</taxon_name>
    <taxon_name rank="variety" authority="S. L. Welsh" date="unknown">barnebyi</taxon_name>
    <taxon_hierarchy>genus p.;species aromaticum;variety barnebyi</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">P.</taxon_name>
    <taxon_name rank="species" authority="unknown" date="unknown">aromaticum</taxon_name>
    <taxon_name rank="variety" authority="S. L. Welsh" date="unknown">tuhyi</taxon_name>
    <taxon_hierarchy>genus p.;species aromaticum;variety tuhyi</taxon_hierarchy>
  </taxon_identification>
  <number>17.</number>
  <description type="morphology">Herbs caulescent, to 25 cm, mostly glandular and strigose throughout; spreading by rhi­zomes, often forming patches.  Stems suberect to decum­bent, branched, leaves dispersed along stems; pseudo­scapes 0–12 cm; cataphylls 0–7 mm, prominently veined.  Leaves palmately (3–)5–7-foliolate; stipules per­sistent, triangular, 2–8(–9) × 1–5 mm, glan­dular and stri­gose abaxially; petiole jointed to stem, 12–80 mm, stri­gose; petiolules 1–3 mm; leaflet blades oblanceolate, ovate, or rhombic, (0.6–)1.2–2.6 × (0.3–)1–2 cm, base cuneate, apex rounded to retuse, usually apiculate, surfaces glandular, abaxially uniformly strigose, adaxially strigose to glabrate or with hairs concentrated along veins.  Peduncles 0.2–1.3 cm, shorter than subtending petiole, strigose.  Inflorescences persistent, umbellate or subcapitate, rarely reduced to a single flower; rachis 0–0.8 cm, nodes 1–7, 3 flowers per node, internodes 1–2(–4) mm; bracts tardily deciduous or persistent, lanceolate to narrowly elliptic, sometimes short-cupulate, 2–7 × 1–3 mm, glandular and strigose abaxially.  Pedicels 1–2.5(–4) mm.  Flowers (7–)8–11(–12) mm; calyx gibbous-campanulate in fruit, 5.5–10 mm abaxially, 4.5–8 mm adaxially, glandular, pubescent; tube 3–4 mm; abaxial lobe elliptic to oblan­ceolate, 2.5–7 × 1.5–2 mm, adaxial lobes trian­gular, 2–4(–5) × 1 mm; corolla blue-purple, banner white or paler than other petals, oblanceolate, 9–11 × 4–5.5 mm with claw 2.5–3 mm, wings 9–11 × 2–3 mm with claw 3.5–4 mm, keel 7–8 × 2 mm with claw 3–4 mm; filaments 6–6.5 mm; anthers ovate, 0.3 mm; ovary glabrous, style pubescent basally.  Legumes globose to ovoid, 5–6 × 4 mm, glandular and short-strigose distally, beak broad, slightly arcuate, 5–6 mm, exserted beyond calyx.  Seed gray-green to red-brown, oblong-reniform, 4.5–5 × 3.5–4 mm, shiny.</description>
  <discussion>Pediomelum aromaticum is known from Mohave County in Arizona, Montrose County in Colorado, and southern Utah.</discussion>
  <discussion>In the past, varieties were recognized based on the number of flowers per node, flower and peduncle length, and stem robustness.  These varieties are largely confined to distinct geographical populations or areas.  Variety ambiguum was described from Little Valley in Grand County as having long peduncles (5–28 mm) and an often bidentate abaxial calyx tooth.  Variety barnebyi was described from populations from the Canaan Mountain region in Washington county and adjacent Arizona, an area at the westernmost edge of the P. aromaticum distribution.  These plants are more robust than others, have more flowers per inflorescence, and have longer peduncles, but plants from across the range show similar robustness with number of flowers per inflorescence varying widely.  Variety tuhyi was described as differing from var. aromaticum in having flowers smaller than 9 mm, coupled with decumbent stems.  J. W. Grimes (1990) pointed out that mature flowers on several collections, including the holotype, are longer than 9 mm, and not all plants are decumbent.</discussion>
  <description type="phenology">Flowering spring–summer.</description>
  <description type="habitat">Rocky clay or sandstone soils, barren or open places in pinyon-juniper wood­lands.</description>
  <description type="elevation">1000–2000 m.</description>
  <description type="distribution">Ariz., Colo., Utah.</description>
</bio:treatment>
