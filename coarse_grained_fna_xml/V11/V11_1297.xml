<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">11</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Lindley" date="unknown">FABACEAE</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">Faboideae</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">ASTRAGALUS</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="section">Genistoidei</taxon_name>
    <taxon_name rank="species" authority="Douglas in W. J. Hooker" date="1831">miser</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Bor.-Amer.</publication_title>
      <place_in_publication>1: 153.  1831</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fabaceae;subfamily faboideae;genus astragalus;section genistoidei;species miser</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>30.</number>
  <other_name type="common_name">Weedy milkvetch</other_name>
  <description type="morphology">Plants clump-forming, caules­cent or shortly caulescent, 1–35 cm, strigose, strigulose-pilosulous, pilosulous, strigu­lose, or villous, hairs basifixed or malpighian; from ± super­ficial, branched caudex.  Stems decumbent to erect, sterile branches forming tuft of sub-basal leaves, strigose, strigulose, or villous.  Leaves 1.5–20 cm; stipules connate-sheathing (bidentate) at proximal nodes, shortly connate or distinct at dis­tal nodes, 1.5–9 mm, papery-membranous through­out or, sometimes, herbaceous at distal nodes; leaflets 3–21, blades linear, oblong, elliptic, linear-elliptic, linear-oblanceolate, lanceolate, oblong-elliptic, elliptic-lanceolate, or, rarely, filiform-subulate (distally), 2–30(–42) × 0.5–7 mm, apex acute, obtuse, acuminate, apiculate, or, rarely, retuse, surfaces strigose, glabres­cent, or glabrous abaxially, glabrous, glabrescent, or sparsely hairy adaxially; terminal leaflet sometimes decur­rent distally, not jointed to rachis.  Peduncles usually incurved-ascending, 2–14 cm.  Racemes 3–19(–24)-flowered, flowers spreading-declined; axis 1–14 cm in fruit; bracts 0.6–4 mm; bracteoles usually 0. Ped­icels 0.8–3 mm.  Flowers 5.3–10.6 mm; calyx (2.3–)2.4–6 mm, strigose, tube 1.7–4.2 mm, lobes subulate, 0.5–2.6 mm; corolla lilac, pink-purple, ochroleucous, or whitish, often suffused, lined, or veined with purple; banner recurved through 40–90°; keel 5.9–10.7(–11.4) mm, apex beaklike.  Legumes declined-pendulous; green, sometimes with purple speckles, becoming brown or stramineous, straight or nearly so, linear, linear-oblong, linear-ellipsoid, or oblanceoloid, laterally compressed, bicarinate by sutures, 11–25 × (1.2–)2–4 mm, papery, usually strigulose, villosulous, or glabrous, rarely with few hairs; stipe 0–1 mm.  Seeds 6–19.</description>
  <discussion>Varieties 8 (8 in the flora).</discussion>
  <description type="distribution">w North America.</description>
  <discussion>Astragalus miser, widespread in the American West, contains miserotoxin, a nitrogenous compound that is poisonous to cattle and sheep.  In the key to varieties the presence of malpighian hairs is important.  The point of attachment may be so near the base as to be almost indistinguishable from a basifixed trichome.  The key is tentative and will not always reliably separate varieties; nevertheless, morphological trends exist and most plants are identifiable.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Herbage hairs obscurely malpighian; leaflet blades pubescent.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Herbage villosulous or villous and pilosulous, hairs gray or silvery, mostly twisted and loose; legumes minutely villosulous, hairs twisted; ec Idaho, adjoining Montana.</description>
      <determination>30h. Astragalus miser var. crispatus</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Herbage strigulose, hairs ± straight; legumes strigulose; Idaho, Montana, Wyoming.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaflet blades linear to linear-elliptic or oblong-elliptic; corollas whitish, ochro­leu­cous, or stramineous, sometimes brownish-veined, keel apex maculate; seeds 7–11; sw Montana, ec Idaho, nw Wyoming.</description>
      <determination>30f. Astragalus miser var. praeteritus</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaflet blades narrowly elliptic to elliptic-lanceolate (leaves subtending racemes sometimes broadly elliptic or oblanceolate); corollas usually pink-purple, purplish, bluish, or dull purple, sometimes pallid or whitish, except maculate keel; seeds 12–18; Idaho and s Montana to c Wyoming.</description>
      <determination>30g. Astragalus miser var. decumbens</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Herbage hairs basifixed; leaflet blades abaxially pubescent, adaxially pubescent or glabrous.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaflet blades with pubescent surfaces, hairs silvery or cinereous; calyces (4.2–)4.6–6 mm; corollas lilac or pink-purple, banners (9.5–)9.8–12 mm, keel (7.8–)8.6–10.7 mm; legumes densely strigulose; ne Washington to w Montana and adjacent Canada.</description>
      <determination>30a. Astragalus miser var. miser</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaflet blades with surfaces pubescent or glabrous abaxially, usually glabrous or gla­brate adaxially, sometimes pubescent; calyces 2.4–5.6 mm; corollas usually whitish, rarely ochroleucous, sometimes suffused or lined or veined with purple, banners (5.2–)6–13 mm, keel 5.9–10 mm; legumes strigulose or gla­brous; British Columbia and Alberta south­ward to Washington, Arizona, New Mexico, and South Dakota.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaflets (3–)7–11; stems 1–11(–15) cm; banners 6–8 mm; legumes linear (in profile), strigulose; seeds 8–12; se Idaho to sw Montana, ne Nevada, n Utah, and w Wyoming.</description>
      <determination>30c. Astragalus miser var. tenuifolius</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaflets (9 or)11–21; stems 1–35 cm; banners (5.2–)6.5–13 mm; legumes oblanceolate, linear, linear-oblong, -elliptic, or -oblanceolate (in pro­file), glabrous or strigulose; seeds (6 or)7–19; British Columbia and Alberta southward to Washington, Arizona, New Mexico, and South Dakota.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Legumes strigulose; seeds 13–19; Montana to New Mexico, west to Nevada and Arizona.</description>
      <determination>30b. Astragalus miser var. oblongifolius</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Legumes glabrous, strigulose, or few hairs present; seeds (6 or)7–11; South Dakota west to British Columbia and Washington.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Stems 10–35 cm; banners 7–9.5 mm, keel 6–7.8(–8.4) mm; leaflet blades narrowly elliptic to linear or linear-oblanceolate; legumes glabrous or strigulose; Alberta and British Columbia southward to Montana, Idaho, and Washington.</description>
      <determination>30d. Astragalus miser var. serotinus</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Stems 1–15 cm; banners (5.2–)6.5–13 mm, keel (7.1–)8–10(–11.4) mm; leaflet blades narrowly to broadly elliptic, lanceolate, or lanceolate-oblong; legumes usually glabrous, rarely with few, scattered hairs; w Wyoming and Montana, adjoining Idaho, and Black Hills of South Dakota.</description>
      <determination>30e. Astragalus miser var. hylophilus</determination>
    </key_statement>
  </key>
</bio:treatment>
