<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>David S. Seigler, John E. Ebinger</author>
    </source>
    <other_info_on_meta type="volume">11</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Lindley" date="unknown">FABACEAE</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">CaesalpinioideaeMimosoidClade</taxon_name>
    <taxon_name rank="genus" authority="Rafinesque" date="1838">SENEGALIA</taxon_name>
    <place_of_publication>
      <publication_title>Sylva Tellur.,</publication_title>
      <place_in_publication>119.  1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fabaceae;subfamily caesalpinioideaemimosoidclade;genus senegalia</taxon_hierarchy>
    <other_info_on_name type="etymology">From species name Mimosa senegal Linnaeus, alluding to nativity</other_info_on_name>
  </taxon_identification>
  <number>30.</number>
  <other_name type="common_name">Acacia</other_name>
  <description type="morphology">Shrubs or trees [lianas], armed, rarely unarmed.  Stems usually erect [spreading or climbing], pubescent or glabrous; twigs terete to angulate, usually straight, short shoots present or absent.  Leaves alternate, even-bipinnate; stipules present, caducous, herbaceous; petiolate, petiole channeled, gland present [absent] in channel; pinnae 1–30(–43) pairs, mostly opposite; leaflets 3–55 pairs per pinna, opposite, sessile or subsessile, blade margins entire, surfaces glabrous or pubescent.  Inflorescences 6–250+-flowered, terminal or axillary, heads or spikes, usually in pseudoracemes or pseudopanicles; bracts present.  Flowers mimosoid; calyx campanulate, lobes 5; corolla white, creamy white, or yellow, lobes 5; stamens 40–160, distinct, exserted, creamy or yellow, fading reddish brown; anthers dorsifixed, dehiscing longitudinally, sometimes with a small stalked gland; ovary stipitate or sessile with nectiferous disc at base, usually pubescent; style and stigma filiform.  Fruits legumes, stipitate or sessile, mostly flattened, straight to falcate, oblong [linear], usually dehiscent along sutures, glabrous or pubescent.  Seeds 3–16[–25], uniseriate, sometimes flattened, ovoid to ellipsoid, not surrounded by pulp; pleurogram usually U-shaped.  x = 13.</description>
  <discussion>Species ca. 235 (6, including 2 hybrids, in the flora).</discussion>
  <description type="distribution">sw, sc United States, Mexico, West Indies, Central America, South America, Asia, Africa, Pacific Islands, Australia; nearly worldwide in tropical to warm temperate areas.</description>
  <discussion>Senegalia is separated from related genera by the presence of prickles, the absence of stipular spines, a nectiferous disk at the base of the stipitate ovary, and portate pollen.  Based on mor­phological evidence, L. Pedley (1978, 1986) separated Senegalia from other members of Acacia in the broad sense.  The separation was further supported by phylogenetic analysis of chloroplast matK and trnl sequence datasets, in which species of Senegalia formed a monophyletic clade (D. S. Seigler et al. 2006b).</discussion>
  <references>
    <reference>Seigler, D. S., J. E. Ebinger, and J. T. Miller.  2006b.  The genus Senegalia (Fabaceae: Mimosoideae) from the New World.  Phytologia 88: 38–93.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaflets 25–55 pairs per pinna.</description>
      <determination>1. Senegalia berlandieri</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaflets 3–21 pairs per pinna.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Inflorescences cylindric spikes, lengths 3+ times widths.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaflet blades usually 2.8–5.5 mm; flowers sessile or short-pedicellate, pedicels.</description>
      <next_statement_id>0–0</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>0–0</statement_id>
      <description type="morphology">6 mm.</description>
      <determination>3. Senegalia greggii</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaflet blades usually 5.5–9.2 mm; flowers pedicellate, pedicels 0.5–2.1 mm.</description>
      <determination>6. Senegalia wrightii</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Inflorescences globose or subglobose heads, lengths less than 2 times widths.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaflets 3–10 pairs per pinna, blades usually obovate, rarely oblong.</description>
      <determination>4. Senegalia roemeriana</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaflets 13–21 pairs per pinna, blades oblong.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Mature leaflet blades mostly less than 5.4 mm (3–7 mm).</description>
      <determination>2. Senegalia × emoryana</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Mature leaflet blades mostly more than 5.4 mm (5.1–9.3 mm).</description>
      <determination>5. Senegalia × turneri</determination>
    </key_statement>
  </key>
</bio:treatment>
