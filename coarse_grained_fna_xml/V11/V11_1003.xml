<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">11</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Lindley" date="unknown">FABACEAE</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">Faboideae</taxon_name>
    <taxon_name rank="genus" authority="Rydberg in N. L. Britton et al." date="1919">PEDIOMELUM</taxon_name>
    <taxon_name rank="species" authority="(Pursh) Rydberg in N. L. Britton et al." date="1919">esculentum</taxon_name>
    <place_of_publication>
      <publication_title>N. Amer. Fl.</publication_title>
      <place_in_publication>24: 20.  1919</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fabaceae;subfamily faboideae;genus pediomelum;species esculentum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Psoralea</taxon_name>
    <taxon_name rank="species" authority="Pursh" date="1813">esculenta</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Amer. Sept.</publication_title>
      <place_in_publication>2: 475, plate 22.  1813</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus psoralea;species esculenta</taxon_hierarchy>
  </taxon_identification>
  <number>6.</number>
  <other_name type="common_name">Prairie turnip or potato</other_name>
  <other_name type="common_name">Indian breadroot</other_name>
  <description type="morphology">Herbs usually caulescent, rarely subacaulescent to acaulescent, to 50 cm, eglandular and pubes­cent throughout.  Stems erect, usually unbranched, some­times branched basally, leaves dis­persed along stem and arising nearly perpendicular to it; pseudoscapes 0.5–2 cm; cataphylls 0.5–15 mm, striate.  Leaves palmately (3–)5-foliolate; stipules persistent, broadly lanceolate proximally to linear-lanceolate distally, 10–20 × 2–8 mm, stramineous basally, eglandular, glabrate to sparsely pubescent, hairs semi-erect; petiole not jointed basally, (2–)30–100(–150) mm; petiolules 1.5–4 mm; leaflet blades elliptic to oblanceolate, 2–4(–6) × 0.7–2.3 cm, base attenuate to cuneate, apex broadly acute to rounded or retuse, surfaces abaxially pubescent, adaxially glabrate except on midvein.  Peduncles (0.5–)5–12(–15) cm, shorter than subtending petiole, pilose.  Inflorescences persistent (not disjointing at base of peduncle in fruit), elliptic to oblong; rachis 1.6–7 cm, elongating slightly in fruit, nodes (6–)8–15, (2 or)3 flowers per node; bracts persistent, oblanceolate to elliptic, 5–15 × (0.5–)4–9 mm, glabrate to sparsely pubescent, hairs semi-erect.  Pedicels 1–3 mm.  Flowers 12–20 mm; calyx strongly gibbous-campanulate in fruit, 13–16 mm abaxially, 12–14 mm adaxially, eglandular, pubescent; tube 5–6 mm; lobes linear or linear-lanceolate to elliptic, abaxial 7.5–10 × 2–2.5 mm, adaxial 4–7 × 1–1.5 mm; corolla violet to blue-purple, banner sometimes paler, oblanceolate, 17–18 × 6 mm with claw 7–8 mm, wings 15–16.5 × 3–3.5 mm with claw 6–6.5 mm, keel 12–12.5 × 3 mm with claw 6–6.5 mm; filaments 11–14 mm; anthers elliptic, 0.5 mm; ovary pubescent apically, style glabrous apically.  Legumes oblong, 4–6 × 2.5–3.5 mm, eglandular, pubescent, beak 9–13(–16) mm, exserted beyond calyx.  Seed brown, reniform, 4 × 3 mm, somewhat rugose.  2n = 22.</description>
  <discussion>Pediomelum esculentum was once one of the main sources of starch for Native American tribes of the Great Plains, eaten fresh, boiled, dried, or ground into flour and used as a thickening agent.  Use of the root for food and barter was documented by Lewis and Clark on their historic expedition across the United States (Mer. Lewis and W. Clark 2003).</discussion>
  <discussion>Pediomelum esculentum ranges in morphology from strongly caulescent to acaulescent with no apparent geo­graphical structuring in this most widespread species.  J. W. Grimes (1990) placed this species in subg. Pediomelum due to its persistent inflorescences.  Molecular phylogenetic and network analyses suggest a split affinity for P. esculentum between both subgenera, suggesting that this may be an intermediate form and bridge between his subgenera or the groupings suggested by D. J. Ockendon (1965) based on habit—groupings somewhat supported by molecular phylogenies (A. N. Egan and K. A. Crandall 2008, 2008b).</discussion>
  <description type="phenology">Flowering late spring–summer.</description>
  <description type="habitat">Prairies, grasslands, open pine woodlands.</description>
  <description type="elevation">500–2000 m.</description>
  <description type="distribution">Alta., Man., Sask.; Ark., Colo., Ill., Iowa, Kans., Minn., Mo., Mont., Nebr., N.Mex., N.Dak., Okla., S.Dak., Tex., Wis., Wyo.</description>
</bio:treatment>
