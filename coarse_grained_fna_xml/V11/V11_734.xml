<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>Alan W. Lievens, Michael A. Vincent</author>
    </source>
    <other_info_on_meta type="volume">11</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Lindley" date="unknown">FABACEAE</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="subfamily">Faboideae</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">INDIGOFERA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 751.  1753</place_in_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 333.  1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family fabaceae;subfamily faboideae;genus indigofera</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin indicum, indigo, and fero, to bear, alluding to blue dye obtained from the plants</other_info_on_name>
  </taxon_identification>
  <number>76.</number>
  <other_name type="common_name">Indigo</other_name>
  <description type="morphology">Herbs, annual or perennial, subshrubs, or shrubs, unarmed.  Stems erect, ascending, spreading, procumbent, or prostrate, usually pubescent (except glabrous in I. decora, I. pilosa pilose with biramous hairs, hairs dolabriform in part or throughout).  Leaves alternate, odd-pinnate [unifoliolate], not glandular-punctate [or glandular-punctate]; stipules present; petiolate; leaflets (1 or)3–17(or 19)[–23], opposite or alternate, stipels absent or evanescent, blade margins entire, surfaces glabrous or pubescent.  Inflorescences 1–60+-flowered, axillary, racemes, often appearing spicate; bracts present, caducous; bracteoles absent.  Flowers papilionaceous; calyx campanulate, lobes 5; corolla usually pink to red, salmon to maroon, orange-mauve to orange, or greenish yellow to ochroleucous, rarely white, 2.5–12(–14) mm; petals caducous; keel shorter than wings and banner; wings auriculate; keel with a pouch or spur extending outward from lateral surface; stamens 10, diadelphous; anthers uniform, basifixed, apiculate and initially gland-tipped; ovary usually sessile.  Fruits legumes, sessile or stipitate, terete, straight or curved, cylindric, ovoid, oblong, or ellipsoidal, 3–70 mm, ± dehiscent, usually septate, not constricted between seeds, margins smooth, often mottled inside, glabrous or pubescent.  Seeds 1–12, ± cuboid to ellipsoidal.  x = 6, 7, 8.</description>
  <discussion>Species ca. 750 (14 in the flora).</discussion>
  <description type="distribution">North America, Mexico, West Indies, Central America, South America, Asia, Africa, Pacific Islands, Australia; tropical and temperate regions.</description>
  <discussion>Indigofera is easily recognized since all species have malpighian hairs that may be appressed or ascending and may have unequal branching; other types of hairs may also be present.  In addition, the corollas range in color from shades of pink to red, salmon pink, orange, purplish red, or greenish yellow; petals are caducous; anthers are mucronate distally; seeds are cuboid or ellipsoid, and usually mottled.  Another feature characteristic of Indigofera species is a floral-tripping mechanism in the flowers that results in a sudden release of pollen.  If the base of the banner petal is touched, the claw splits and detaches from the calyx instantly, causing other petals to collapse and the pollen to be thrown from the anthers (F. H. G. Hildebrand 1866; G. Henslow 1867; A. W. Lievens 1992).</discussion>
  <discussion>Indigofera includes species from which the blue dye indigo is produced.  Some Indigofera species are grown as ornamental plants, especially woody species from southeast Asia and Australia, such as I. australis Willdenow, I. decora, I. heterantha Wallich ex Brandis, I. kirilowii, I. pendula Franchet, and I. pseudotinctoria Matsumura, some of which have escaped cultivation in the flora area.</discussion>
  <discussion>Chromosome numbers reported in Indigofera are 2n = 8, 12, 14, 16, 32, to 48 (J. A. Frahm-Leliveld 1966; P. K. Gupta and K. Agarwal 1982; B. L. Turner 1956b).</discussion>
  <discussion>Indigofera parviflora F. Heyne ex Wight &amp; Arnott was collected by C. Mohr (US) as a waif on ballast spoils in Mobile, Alabama; I. trifoliata Linnaeus was found as a waif on chrome-ore piles in Maryland by Clyde F. Reed (MO).  A specimen of I. heterantha Wallich ex Brandis (as I. gerardiana Graham ex Baker) was collected in 1979 in Los Angeles County, California.</discussion>
  <references>
    <reference>Lievens, A. W.  1992.  Taxonomic Treatment of Indigofera L. (Fabaceae: Faboideae) in the New World.  Ph.D. dissertation.  Louisiana State University.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Flowers 12–18 mm; legumes glabrous, 25–70(–80) mm.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems glabrous; racemes 8–15-flowered; stipules 1–2 mm; leaves 8–25 cm; leaflets 5–15[–23], 20–75(–100) mm.</description>
      <determination>13. Indigofera decora</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems sparsely pubescent; racemes 40–60+-flowered; stipules 4–6 mm; leaves 6–15 cm; leaflets (5 or)7–11, 15–40(–50) mm.</description>
      <determination>14. Indigofera kirilowii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Flowers 2.5–12 mm; legumes pubescent, 3–35(–40) mm.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stems procumbent or prostrate; leaflets 3–11(–17), usually alternate, rarely opposite.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Stems procumbent; leaflets similar in size within a leaf, blades oblanceolate, obovate, or narrowly elliptic, apex acute or truncate, surfaces glabrate to densely pubescent adaxially; peduncles 1.5–8 cm.</description>
      <determination>1. Indigofera miniata</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Stems prostrate; terminal leaflet usually larger than laterals, blades obovate to broadly oblanceolate, apex rounded to truncate, surfaces glabrous adaxially; peduncles 0.5–1 cm.</description>
      <determination>2. Indigofera spicata</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stems usually erect, ascending, spreading, prostrate, or procumbent, sometimes scrambling or sprawling; leaflets 1–19, usually opposite (at least distally).</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Herbs; stems erect to ascending-spreading; leaflets 1 or 3, if 3, terminal leaflet 2–3 times larger than laterals; racemes 1–3-flowered.</description>
      <determination>3. Indigofera pilosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Herbs, subshrubs, or shrubs; stems usually erect, ascending, spreading, procumbent, or prostrate, sometimes scrambling or sprawling; leaflets 3–19, similar in size within a leaf; racemes 3–40+-flowered.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Annual, biennial, or short-lived perennial herbs; stems, petioles, and legumes glandular, or brownish hirsute, or pilose, hairs long-spreading, or long-spreading intermixed with appressed.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Herbs brownish hirsute or pilose, hairs long-spreading; racemes 10–20+-flowered, dense; flowers 6–7 mm.</description>
      <determination>4. Indigofera hirsuta</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Herbs glandular and pubescent, hairs intermixed long-spreading and appressed; racemes 3–10-flowered, lax; flowers 2.5–3 mm.</description>
      <determination>5. Indigofera colutea</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Perennial herbs, shrubs, or subshrubs; glands and brownish hairs absent, hairs appressed or ascending, or hairs crisped, curling.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Perennial herbs; stems erect to procumbent; stems and petioles with crisped, curling hairs; c Texas.</description>
      <determination>6. Indigofera texana</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Perennial herbs, shrubs, or subshrubs; stems usually erect or ascending (usually procumbent, sometimes scrambling in I. oxycarpa); stems and petioles with appressed or ascending hairs; not confined to c Texas.</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Legumes ovoid, oblong, or ellipsoidal, straight, 3–9 mm; seeds 1–3.</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Herbs; leaflets (7 or)9–13; flowers 6–9 mm; legumes 7–9 mm, woody; seeds 2 or 3; se United States.</description>
      <determination>7. Indigofera caroliniana</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Shrubs or subshrubs, woody; leaflets 13–19; flowers 4.5–5.2 mm; legumes 3–3.5 mm, leathery; seed 1; Arizona, California, New Mexico.</description>
      <determination>8. Indigofera sphaerocarpa</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Legumes cylindric, straight or curved, 15–40 mm; seeds 4–12.</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Shrubs; stems usually procumbent, sometimes scrambling; leaflets 3–7, blade surfaces glabrate to appressed-pubescent adaxially; racemes 8–20 cm, lax, 20–40+-flowered.</description>
      <determination>9. Indigofera oxycarpa</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Herbs or subshrubs; stems erect or ascending; leaflets 7–17, blade surfaces glabrous, densely pubescent, or strigose adaxially; racemes 0.5–12 cm, lax or dense, 5–30+-flowered.</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Leaflet surfaces glabrous adaxially; legumes straight or slightly curved, or abruptly upturned distally.</description>
      <determination>10. Indigofera tinctoria</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Leaflet surfaces densely pubescent or strigose adaxially; legumes straight or slightly curved, or falcate.</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Legumes 18–25 mm, straight or slightly curved to falcate, cinereous-pubescent, base bulbous and reddish; racemes 5–12 cm, lax; flowers 6–8 mm.</description>
      <determination>11. Indigofera lindheimeriana</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Legumes 15–20 mm, strongly curved, strigose to glabrate, base not bulbous or reddish; racemes 3.5–5.5 cm, dense; flowers 5–6 mm.</description>
      <determination>12. Indigofera suffruticosa</determination>
    </key_statement>
  </key>
</bio:treatment>
