<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <author>William J. Crins</author>
      <date>unknown</date>
    </source>
    <other_info_on_meta type="volume">23</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">cyperaceae</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">carex</taxon_name>
    <taxon_name rank="section" authority="(Kükenthal) Mackenzie" date="1935">Firmiculmes</taxon_name>
    <place_of_publication>
      <publication_title>in N. L. Britton et al., N. Amer. Fl.</publication_title>
      <place_in_publication>18: 221. 1935</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cyperaceae;genus carex;section Firmiculmes</taxon_hierarchy>
    <other_info_on_name type="fna_id">302694</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Carex</taxon_name>
    <taxon_name rank="subsection" authority="Kükenthal" date="unknown">Firmiculmes</taxon_name>
    <place_of_publication>
      <publication_title>in H. G. A. Engler, Pflanzenr.</publication_title>
      <place_in_publication>20[IV,38]: 93. 1909</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Carex;subsection Firmiculmes;</taxon_hierarchy>
  </taxon_identification>
  <description type="morphology">Plants cespitose or not, short to long rhizomatous, sometimes inconspicuously rhizomatous. Culms brown or red brown at base. Leaves: basal sheaths fibrous; sheath fronts membranous; blades V-shaped in cross section when young, glabrous. Inflorescence 1 spike; bractless; spike androgynous, with not more than 15 perigynia, lax. Proximal pistillate scales less than 10 mm, apex short- or long-awned. Perigynia ascending, veinless or weakly veined with 2 strong marginal veins, stipitate, narrowly obovate, trigonous in cross section, 4.7–8.4 mm, 2–3 times as long as wide, base tapering, with spongy tissue, apex rounded to beak, glabrous; beak truncate. Stigmas 3. Achenes trigonous, 4–5 mm, almost as large as bodies of perigynia; style deciduous.</description>
  <description type="distribution">w North America.</description>
  <number>26lll.</number>
  <discussion>Species 3 (3 in the flora).</discussion>
  <discussion>The relationships of Carex sect. Firmiculmes are unclear. The absence of bracts and presence of foliaceous pistillate scales are reminiscent of sect. Phyllostachyae Tuckerman ex Kükenthal. The groups differ in numerous other characters. The presence of rachillae suggests that the section is primitive; its phylogenetic position remains unresolved.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants cespitose, inconspicuously rhizomatous; culms terete, smooth distally; leaves involute, 0.8–1.5 mm wide, shorter than culms; perigynia with 2 strong marginal veins and several additional faint veins.</description>
      <determination>470 Carex multicaulis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants not cespitose, short to long rhizomatous; culms triangular, scabrous distally; leaves plane, 1.1–3.5 mm wide, equaling or exceeding culms; perigynia with 2 strong marginal veins, otherwise veinless.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Rhizomes prolonged; pistillate portions of spike unbranched; proximal pistillate scales chartaceous, cuspidate to short-awned.</description>
      <determination>468 Carex geyeri</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Rhizomes very short; pistillate portions of spike 0–2 branched; proximal pistillate scales foliaceous, long-awned.</description>
      <determination>469 Carex tompkinsii</determination>
    </key_statement>
  </key>
</bio:treatment>
