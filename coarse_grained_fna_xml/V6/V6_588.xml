<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <other_info_on_meta type="treatment_page">325</other_info_on_meta>
    <other_info_on_meta type="mention_page">324</other_info_on_meta>
    <other_info_on_meta type="mention_page">329</other_info_on_meta>
    <other_info_on_meta type="mention_page">332</other_info_on_meta>
    <other_info_on_meta type="mention_page">333</other_info_on_meta>
    <other_info_on_meta type="mention_page">339</other_info_on_meta>
    <other_info_on_meta type="mention_page">354</other_info_on_meta>
    <other_info_on_meta type="mention_page">356</other_info_on_meta>
    <other_info_on_meta type="illustration_page">316</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">malvaceae</taxon_name>
    <taxon_name rank="subfamily" authority="Burnett" date="unknown">Malvoideae</taxon_name>
    <taxon_name rank="genus" authority="A. Gray" date="1849">sidalcea</taxon_name>
    <taxon_name rank="species" authority="Greene" date="unknown">asprella</taxon_name>
    <place_of_publication>
      <publication_title>Bull. Calif. Acad. Sci.</publication_title>
      <place_in_publication>1: 78. 1885</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus sidalcea;species asprella;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250101110</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Sidalcea</taxon_name>
    <taxon_name rank="species" authority="(de Candolle) A. Gray" date="unknown">malviflora</taxon_name>
    <taxon_name rank="subspecies" authority="(Greene) C. L. Hitchcock" date="unknown">asprella</taxon_name>
    <taxon_hierarchy>genus Sidalcea;species malviflora;subspecies asprella</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <description type="morphology">Herbs, perennial, 0.1–1(–1.2) m, infrequently ± glaucous, with caudex or not, usually with freely-rooting fibrous rootstocks or rhizomes (5–)10–30 cm × 2–4 mm, matted or not. Stems usually single, erect and sometimes supported by adjacent plants (sprawling), base prostrate or decumbent-ascending to erect, often rooting, solid, not brittle, sometimes ± glaucous distally, proximally stellate-hairy, glabrate, hairs minute or larger and coarse (never simple only), usually 4-rayed, 0.5–1 mm. Leaves basal and/or cauline, similar in size and shape; stipules linear to lanceolate, 2–3 × 1.1 mm; petiole (1–)5–10(–15) cm, longest on proximal leaves, 1–4 times longer on proximal leaves to 1/2 times to as long as blade on distal leaves; blade usually shallowly to deeply palmately 3–7-lobed usually halfway to base, proximal and distal cauline blades rounded to reniform, 2–3 × 2–5 cm, usually wider than long, base cordate to truncate, margins crenate, apex blunt or rounded, lobes narrowest at base, margins usually apically coarsely toothed, rarely entire, surfaces stellate-puberulent. Inflorescences ascending or erect, often spiciform, open, calyces not overlapping in flower or fruit, unbranched or branched, 2–15(–30)-flowered, elongate in both flower and fruit, usually 1-sided, 6–11(–30) cm; bracts leaflike to linear, usually 2-fid, (2–)3–5(–15) mm. Pedicels 2–5(–10) mm; involucellar bractlets absent. Flowers bisexual or unisexual and pistillate, plants gynodioecious; calyx 5–12 mm, uniformly densely stellate-puberulent; petals pink to pale purple, pale-veined, (5–)10–28 mm, pistillate flowers darker, 5–15 mm; staminal column 4–5 mm, stellate-puberulent; anthers white; stigmas (6 or)7 or 8. Schizocarps 6–8 mm diam.; mericarps (6 or)7 or 8, 3–4 mm, usually glandular-puberulent to stellate-puberulent, sometimes glabrous, roughened, strongly reticulate-veined, sides and back pitted, mucro 0.5–1 mm. Seeds 1.5–2.8 mm.</description>
  <description type="distribution">Calif., Oreg.</description>
  <discussion>Subspecies 2 (2 in the flora).</discussion>
  <discussion>Sidalcea asprella is variable and occurs from the central Sierra Nevada to southwestern Oregon. Typical plants in the central Sierra Nevada have weak, elongated stems that are often supported by neighboring vegetation; they lack simple recurved hairs at the stem base and may have either elongated rhizomes or a caudex. It has been confused with S. celata, S. elegans, S. gigantea, and S. glaucescens; formerly it was included within S. malviflora; molecular study has shown that it is different from S. malviflora. It belongs to a group including S. celata, S. elegans, S. gigantea, and S. hirtipes (K. Andreasen and B. G. Baldwin 2003).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants (0.3–)0.5–1(–1.2) m, with caudex or usually compact rootstocks or rhizomes to 10(–30) cm × 4 mm; leaves mostly cauline; inflorescences 8–15 (–30)-flowered, erect; stems erect, sometimes weak and supported by other vegetation, sometimes proximally decumbent.</description>
      <determination>1a Sidalcea asprella subsp. asprella</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Plants 0.1–0.3(–0.4) m, with rhizomes 5–20 cm × 2(–3) mm; leaves mostly basal; inflorescences usually 2–10(–19)-flowered, ascending; stems decumbent-ascending to erect, sometimes proximally prostrate.</description>
      <determination>1b Sidalcea asprella subsp. nana</determination>
    </key_statement>
  </key>
</bio:treatment>
