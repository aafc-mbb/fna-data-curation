<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <author>Paul A. Fryxell†,Steven R. Hill</author>
      <date>unknown</date>
    </source>
    <other_info_on_meta type="treatment_page">310</other_info_on_meta>
    <other_info_on_meta type="mention_page">215</other_info_on_meta>
    <other_info_on_meta type="mention_page">217</other_info_on_meta>
    <other_info_on_meta type="mention_page">239</other_info_on_meta>
    <other_info_on_meta type="mention_page">311</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">malvaceae</taxon_name>
    <taxon_name rank="subfamily" authority="Burnett" date="unknown">Malvoideae</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">SIDA</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 683. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 306. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus sida;</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek side, name used by Theophrastos for plants now called Nymphaea alba Linnaeus</other_info_on_name>
    <other_info_on_name type="fna_id">130307</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="Wight" date="unknown">Dictyocarpus</taxon_name>
    <taxon_hierarchy>genus Dictyocarpus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="Boehmer" date="unknown">Malvinda</taxon_name>
    <taxon_hierarchy>genus Malvinda</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="(K. Schumann) Monteiro" date="unknown">Pseudomalachra</taxon_name>
    <taxon_hierarchy>genus Pseudomalachra</taxon_hierarchy>
  </taxon_identification>
  <number>45.</number>
  <other_name type="common_name">Fanpetals</other_name>
  <other_name type="common_name">wireweed</other_name>
  <description type="morphology">Herbs, annual or perennial, subshrubs, or shrubs. Stems erect, ascending, or reclining to procumbent, glabrous or hairy, sometimes viscid (S. glabra). Leaves spirally arranged (distichous in S. planicaulis and S. ulmifolia), petiolate or subsessile; stipules persistent, usually linear to lanceolate or falcate; blade usually unlobed (lobed with maplelike leaves in S. hermaphrodita), base cuneate, cordate, subcordate, truncate, or rounded, margins crenate, dentate, serrate, or entire. Inflorescences axillary solitary (sometimes paired or clustered) often plicate in bud, usually 1/2 divided, often 10-ribbed at base (unribbed in S. hermaphrodita) or angulate, lobes acute or acuminate to triangular or ovate; corolla white, cream, yellow, yellow-orange, salmon-pink, red-orange, or reddish [purplish], sometimes with dark-red center; staminal column included; style 5–14-branched; stigmas capitate. Fruits developed or muticous, reticulate, glabrous or hairy, lateral walls usually persistent, indehiscent below with well-differentiated dorsal wall, indehiscent or partially dehiscent apically. Seeds 1 per mericarp, glabrous. x = 7, 8.</description>
  <description type="distribution">North America, Mexico, West Indies, Central America, South America, Asia, Africa, Australia; warm-temperate and tropical areas.</description>
  <discussion>Species ca. 150 (19 in the flora).</discussion>
  <discussion>In the flora area, Sida linifolia Cavanilles, flax-leaved sida, is known from a single collection (Alabama, Mobile, introduced from West Indies on ballast, Sep 1886, Mohr s.n., F) and treated here as a waif; it is distinguished from other sidas in North America by its entire leaf margins. Sida cordata (Burman f.) Borssum Waalkes has been reported in Maryland (Baltimore City); it is a generally prostrate herb with cordate leaves and filiform pedicels that are nearly the same length as the leaves; no vouchers have been found; if it was present, it can be regarded as a waif. Reports of S. aggregata K. Presl, a variable and rather common Neotropical species, have not been verified; no vouchers have been located. Sida acuta Burman f. and S. carpinifolia Linnaeus f. are names often used for ballast specimens of plants found in temperate seaports that have not persisted.</discussion>
  <discussion>Most sidas have apical spines on the fruits that adhere to fur, wool, and clothing, and therefore it may be difficult to pinpoint their native ranges versus the areas to which they have been introduced. Some are considered to be pan-tropical roadside weeds.</discussion>
  <references>
    <reference>Fryxell, P. A. 1985. Sidus sidarum V. The North and Central American species of Sida. Sida 11: 62–91.</reference>
    <reference>Kearney, T. H. 1954. A tentative key to the North American species of Sida L. Leafl. W. Bot. 7: 138–150.</reference>
    <reference>Krapovickas, A. 2003b. Sida sección Distichifolia (Monteiro) Krapov. comb. nov., stat. nov. (Malvaceae–Malveae). Bonplandia (Corrientes) 12: 83–121.</reference>
    <reference>Krapovickas, A. 2007. Las especies de Sida secc. Malachroideae del Cono Sur de Sudamérica. Bonplandia (Corrientes) 16: 209–225.</reference>
    <reference>Siedo, S. J. 1999. A taxonomic treatment of Sida sect. Ellipticifolia (Malvaceae). Lundellia 2: 100–127.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades palmately 5–7-lobed, maplelike, to 24 cm; calyces not ribbed or angled.</description>
      <determination>7 Sida hermaphrodita</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades unlobed, 1–9 cm; calyces ribbed or angled</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems procumbent</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems erect, sometimes ascending to reclining but not procumbent</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Petals white; leaves distributed evenly along stems, blade margins crenate to base; stems usually with 1–2 mm simple hairs in addition to multirayed stellate hairs; mericarps slightly rugose.</description>
      <determination>1 Sida abutilifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Petals salmon-pink, red-orange, or yellowish; leaves crowded distally on stems, blade margins dentate only at apex; stems with appressed, normally 4-rayed stellate hairs; mericarps prominently muricate.</description>
      <determination>3 Sida ciliaris</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Styles 5-branched; mericarps 5; leaf blades cordate or subcordate at base</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Styles 7–14-branched; mericarps 7–14; leaf blades usually cuneate or truncate at base, sometimes rounded or subcordate to cordate</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Stems and petioles minutely stellate-hairy, hairs to 0.5 mm; petioles usually with small spinelike tubercle on stem just below its attachment.</description>
      <determination>16 Sida spinosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Stems and petioles glandular-viscid and/or with simple hairs 1–3 mm; petioles without spinelike tubercle just below attachment to stem</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Stems usually glandular-viscid; petals white or yellow-orange without darker base, not fading rose-pink; calyces usually glandular, not setose, lobes triangular, acute to short-acuminate (not beaked in bud).</description>
      <determination>6 Sida glabra</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Stems not glandular-viscid; petals yellowish to orange or salmon usually with dark orange or reddish base, fading rose-pink; calyces setose, not glandular, lobes trullate, attenuate-aristate (beaked in bud).</description>
      <determination>19 Sida urens</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaves and branches distichous</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaves spirally arranged</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Mericarps 7 or 8, spines 2 mm; inflorescences usually axillary glomerules, sometimes flowers solitary or paired; calyces 5–6 mm; staminal columns glabrous.</description>
      <determination>12 Sida planicaulis</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Mericarps 8–12, spines 0.5–1 mm; inflorescences axillary, flowers solitary or paired; calyces 6–8 mm; staminal columns hairy, sometimes glabrous.</description>
      <determination>18 Sida ulmifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Leaf blades ± ovate or ovate-oblong with broadly cordate base, infrequently ovate-lanceolate to lanceolate; petioles (5–)10–25 mm</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Leaf blades rhombic to subrhombic or elliptic to oblong, sometimes oblong-lanceolate, lanceolate-elliptic, round, lanceolate, or linear; petioles 2–10(–40) mm</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Leaf blades velvety-tomentose, 1–2 times longer than wide; calyces prominently ribbed, densely stellate-tomentose; inflorescences axillary, usually corymbs or panicles, sometimes solitary flowers; fruits 6–7 mm diam.; mericarp spines to 2 mm; Alabama, Florida, Texas.</description>
      <determination>4 Sida cordifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Leaf blades not velvety, usually 5+ times as long as wide; calyces obscurely ribbed, stellate-hairy and with long simple hairs; inflorescences axillary solitary flowers; fruits 5–6 mm diam.; mericarp spines to 1 mm; Texas.</description>
      <determination>17 Sida tragiifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Leaf blade margins usually entire basally, distally dentate or serrate</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Leaf blade margins crenulate-serrate to dentate to base</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Pedicels 0.4–1.2 cm; leaf blades lanceolate-elliptic to round, 1.5–5 cm.</description>
      <determination>2 Sida antillensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Pedicels 0.5–4(–16) cm; leaf blades rhombic, subrhombic, or elliptic, 2.5–9 cm</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Petals yellow; stem hairs to 0.1 mm; pedicels (1–)3–4 cm.</description>
      <determination>13 Sida rhombifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Petals cream or pale yellow with reddish spot at base; stem hairs to 0.5 mm; pedicels to 2 cm.</description>
      <determination>15 Sida santaremensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Pedicels 8–12(–16) cm.</description>
      <determination>10 Sida longipes</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Pedicels 0.5–6 cm</description>
      <next_statement_id>15</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Leaf blades narrowly oblong-lanceolate or elliptic to linear, 4–20 times as long as wide</description>
      <next_statement_id>16</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Leaf blades narrowly lanceolate or elliptic to subrhombic, 2.5–10 times as long as wide</description>
      <next_statement_id>17</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Plants usually little-branched from base; flowers little, if at all, apically congested; petals yellow-orange.</description>
      <determination>5 Sida elliottii</determination>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Plants freely branching from base; flowers apically congested; petals yellow-orange to reddish, sometimes drying lavender.</description>
      <determination>11 Sida neomexicana</determination>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Pedicels 2–6 cm, sometimes equaling subtending leaf; mericarps 8–10.</description>
      <determination>8 Sida lindheimeri</determination>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Pedicels 0.5–3 cm, shorter than subtending leaf; mericarps 9–12</description>
      <next_statement_id>18</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Pedicels unarticulated; stipules subequal to corresponding petioles.</description>
      <determination>9 Sida littoralis</determination>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Pedicels articulated; stipules 2 times length of corresponding petioles.</description>
      <determination>14 Sida rubromarginata</determination>
    </key_statement>
  </key>
</bio:treatment>
