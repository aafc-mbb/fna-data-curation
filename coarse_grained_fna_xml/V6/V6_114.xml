<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <author>Norman K. B. Robson</author>
      <date>unknown</date>
    </source>
    <other_info_on_meta type="treatment_page">72</other_info_on_meta>
    <other_info_on_meta type="mention_page">71</other_info_on_meta>
    <other_info_on_meta type="mention_page">86</other_info_on_meta>
    <other_info_on_meta type="mention_page">91</other_info_on_meta>
    <other_info_on_meta type="mention_page">102</other_info_on_meta>
    <other_info_on_meta type="mention_page">103</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">hypericaceae</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">HYPERICUM</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 783. 1753</place_in_publication>
    </place_of_publication>
    <place_of_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 341. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family hypericaceae;genus HYPERICUM</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek hyper, above, and eikon, image, alluding to ancient Greek custom of decorating religious figures with Hypericum species to ward off evil spirits</other_info_on_name>
    <other_info_on_name type="fna_id">116180</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="Linnaeus" date="unknown">Ascyrum</taxon_name>
    <taxon_hierarchy>genus Ascyrum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="Small" date="unknown">Crookea</taxon_name>
    <taxon_hierarchy>genus Crookea</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="Small" date="unknown">Sanidophyllum</taxon_name>
    <taxon_hierarchy>genus Sanidophyllum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="Linnaeus" date="unknown">Sarothra</taxon_name>
    <taxon_hierarchy>genus Sarothra</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">St. John’s wort</other_name>
  <other_name type="common_name">St. Andrew’s cross</other_name>
  <other_name type="common_name">millepertuis</other_name>
  <description type="morphology">Herbs, annual or perennial, subshrubs, or shrubs [trees], sometimes rhizomatous, glabrous or hairy, with glandular canals, lacunae, or dots containing resins or waxes (amber), essential oils (pale, translucent), and/or, sometimes, hypericin and pseudohypericin (black or red) in various parts. Stems: internodes terete (not lined) or 2-, 4-, or 6-lined at first (lines usually raised), then sometimes becoming angled, terete, or winged; bark smooth or striate, sometimes corky, punctiform. Inflorescences terminal, cymose, 2+-flowered, or flowers solitary, branching stellate [cupulate]; sepals persistent or deciduous, (3–)4–5, distinct or ± connate, margins sometimes glandular-ciliate; petals persistent or deciduous, (3–)4–5[–6], contorted, yellow to orange, sometimes red-tinged; stamens persistent or deciduous, (5–)10–300(–650), in continuous or interrupted ring or in (3–)4–5 fascicles, fascicles distinct or connate, each with 1–60+ stamens; filaments distinct or basally connate; anthers yellow to orange, oblong to ellipsoid, almost isodiametric, sometimes with amber or black gland on connective; staminode fascicles 0 [3]; ovary 2–5-merous; placentation axile to parietal; ovules 2+ on each placenta; styles distinct or ± connate basally, spreading to ± appressed. Capsules 2–5-valved, sometimes with glandular vittae or vesicles. Seeds narrowly cylindric to ellipsoid, sometimes carinate; testa foveolate or reticulate to scalariform [papillose]. x = 12, 9–7, 6 (dihaploid).</description>
  <description type="distribution">Nearly worldwide.</description>
  <discussion>Species ca. 490 (54 in the flora).</discussion>
  <discussion>Shrubs with deciduous leaves, petals, and stamens belong to either Hypericum sect. Ascyreia Choisy (with five stamen fascicles and five styles) or sect. Androsaemum (Duhamel) Godron (with five stamen fascicles and three or four styles). These are all introductions, mostly garden escapes. Those in sect. Ascyreia include: Hypericum calycinum Linnaeus, a low shrub with creeping stolons and flowers 50–95 mm diam. that has been found in California, Oregon, and Washington; H. ×moserianum Luquet ex André, its hybrid with H. patulum Thunberg, a low (sterile?) branching shrub with red anthers; and H. hookerianum Wight &amp; Arnott, a shrub to 2 m tall with narrow leaves and a dense ring of relatively short stamens, recorded from California (its identity requires confirmation). In sect. Androsaemum, H. androsaemum is a deciduous shrub with relatively small flowers and baccate fruits that ripen from cherry-red to black; it has been found in British Columbia and in California and Washington.</discussion>
  <discussion>Introduced herbaceous species with three stamen fascicles and three styles include: Hypericum hirsutum, with hairy stems and leaves (Ontario); H. tetrapterum, with four-winged internodes and lanceolate sepals (British Columbia and Washington); H. pulchrum with cordate leaves and red-tinged petals (Newfoundland, St. Pierre and Miquelon); and H. humifusum Linnaeus, a procumbent herb with unequal sepals (British Columbia).</discussion>
  <references>
    <reference>Adams, W. P. 1957. A revision of the genus Ascyrum (Hypericaceae). Rhodora 59: 73–95.</reference>
    <reference>Adams, W. P. 1962b. Studies in the Guttiferae. II. Taxonomic and distributional observations in North American taxa. Rhodora 64: 231–242.</reference>
    <reference>Adams, W. P. and N. K. B. Robson. 1961. A re-evaluation of the generic status of Ascyrum and Crookea (Guttiferae). Rhodora 63: 10–16.</reference>
    <reference>Crockett, S. L. 2003. Phytochemical and Biosystematic Investigations of New and Old World Hypericum Species (Clusiaceae). Ph.D. dissertation. University of Mississippi.</reference>
    <reference>Robson, N. K. B. 1981. Studies in the genus Hypericum L. (Guttiferae) 2. Characters of the genus. Bull. Brit. Mus. (Nat. Hist.), Bot. 8: 55–226.</reference>
    <reference>Robson, N. K. B. 1985. Studies in the genus Hypericum L. (Guttiferae) 8. Sections 29. Brathys (part 2) and 30. Trigynobrathys. Bull. Brit. Mus. (Nat. Hist.), Bot. 20: 1–151.</reference>
    <reference>Robson, N. K. B. 1994. Studies in the genus Hypericum L. (Guttiferae) 6. Sections 20. Myriandra to 28. Elodes. Bull. Nat. Hist. Mus. London, Bot. 26: 75–217.</reference>
    <reference>Robson, N. K. B. 2001. Studies in the genus Hypericum L. (Guttiferae) 4(1). Sections 7. Roscyna to 9. Hypericum sensu lato (part 1). Bull. Nat. Hist. Mus. London, Bot. 31: 37–88.</reference>
    <reference>Robson, N. K. B. 2002. Studies in the genus Hypericum L. (Guttiferae) 4(2). Section 9. Hypericum sensu lato (part 2): subsection 1. Hypericum series 1. Hypericum. Bull. Nat. Hist. Mus. London, Bot. 32: 61–123.</reference>
    <reference>Robson, N. K. B. 2006. Studies in the genus Hypericum (Guttiferae) 4(3). Section 9. Hypericum sensu lato (part 3): subsection 1. Hypericum series 2. Senanensia, subsection 2. Erecta and section 9b. Graveolentia. Syst. Biodivers. 4: 19–98.</reference>
    <reference>Robson, N. K. B. 2012. Studies in the genus Hypericum L. (Hypericaceae) 9. Addenda, corrigenda, keys, lists and general discussion. Phytotaxa 72: 1–111.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Herbs, subshrubs, or shrubs, black glands absent; stamens in continuous or interrupted ring or in 4 or 5 barely discernable fascicles, each of 1 or 2 stamens</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Herbs or shrubs, black and/or red glands usually present throughout, sometimes absent; stamens in 5 fascicles, each of 2+ stamens</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Herbs (perennial), subshrubs, or shrubs; petals deciduous; stamens usually persistent, sometimes deciduous, 30–650, in continuous ring or in 4–5 barely discernable fascicles; styles ± appressed, bases distinct.</description>
      <determination>1a Hypericum sect. Myriandra</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Herbs (annual or perennial); petals persistent; stamens persistent, (5–)10–80, usually in continuous or interrupted ring, sometimes in 5 barely discernable fascicles; styles ± spreading, bases distinct.</description>
      <determination>1b Hypericum informal sect. group Brathys</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Shrubs; leaves deciduous (base articulated); style bases distinct.</description>
      <determination>1c Hypericum sect. Webbia</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Herbs (perennial); leaves persistent or tardily deciduous (base not articulated) or; style bases ± connate or distinct</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Herbs, black glands absent; flowers 40–70 mm diam.; stamens 150, in 5 fascicles, fascicles usually distinct, rarely 1 pair connate; styles ± appressed, bases ± connate or distinct.</description>
      <determination>1d Hypericum sect. Roscyna</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Herbs, black glands usually on leaves, sepals, and petals and, sometimes, on stems and anthers; flowers 6–35 mm diam.; stamens 20–109, in 5 fascicles, fascicles connate (as 2 + 2 + 1); styles spreading, bases distinct.</description>
      <determination>1e Hypericum informal sect. group Hypericum</determination>
    </key_statement>
  </key>
</bio:treatment>
