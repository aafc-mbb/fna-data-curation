<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <other_info_on_meta type="treatment_page">334</other_info_on_meta>
    <other_info_on_meta type="mention_page">321</other_info_on_meta>
    <other_info_on_meta type="mention_page">354</other_info_on_meta>
    <other_info_on_meta type="mention_page">335</other_info_on_meta>
    <other_info_on_meta type="mention_page">345</other_info_on_meta>
    <other_info_on_meta type="illustration_page">331</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">malvaceae</taxon_name>
    <taxon_name rank="subfamily" authority="Burnett" date="unknown">Malvoideae</taxon_name>
    <taxon_name rank="genus" authority="A. Gray" date="1849">sidalcea</taxon_name>
    <taxon_name rank="species" authority="Greene" date="unknown">hickmanii</taxon_name>
    <place_of_publication>
      <publication_title>Pittonia</publication_title>
      <place_in_publication>1: 139. 1887</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus sidalcea;species hickmanii;</taxon_hierarchy>
    <other_info_on_name type="special_status">C</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250101127</other_info_on_name>
  </taxon_identification>
  <number>14.</number>
  <description type="morphology">Herbs, perennial, 0.1–0.8 m, not glaucous, with thick, woody taproot or caudex, without rhizomes. Stems several to many (ca. 3–20+), clustered, erect to ascending, branched or unbranched, solid, usually densely stellate-canescent. Leaves cauline, evenly arrayed on stem, usually similar in size, shape; stipules linear-lanceolate to ovate, 2–9 × 1–3 mm, widest above base, width sometimes exceeding stem diam.; petiole 0.6–3(–9) cm, 1/2–3 times as long as blade, apex often with pulvinus; blade orbiculate or reniform to flabelliform, unlobed and margins coarsely crenate to shallowly or deeply lobed, 1–7 × 1–7 cm, usually wider than long, base truncate or cordate, apex rounded, surfaces stellate-hairy. Inflorescences erect, infrequently ascending, usually spiciform, dense or open, calyces overlapping or not, branched or unbranched, 2–20+-flowered, proximal flowers scattered, usually more congested distally, not notably elongate in flower, not 1-sided, (1.5–)3–25 cm, usually longer in fruit; bracts linear to ovate-lanceolate or oblong, undivided, 2-fid, or divided, 2–8(–12) mm, not involucrelike, distal entire to 2-fid, stipulelike, proximalmost not involucrelike, divided to base, much shorter than to nearly equaling calyx. Pedicels 1–4(–5) mm; involucellar bractlets (2 or)3, 2–10 mm, shorter to slightly longer than calyx. Flowers usually bisexual, infrequently unisexual and pistillate; calyx 4–12 mm, densely to sparsely stellate-puberulent to long-bristly; petals usually pale pink to pink-lavender, rarely white, veins not conspicuously whitened, 5–17 mm; staminal column 4–7 mm, hairy; anthers white to pale pinkish or pale yellow; stigmas (4–)6 or 7(–10). Schizocarps 4–7 mm diam.; mericarps usually (4–)6 or 7(–10), (1.5–)2–2.5 mm, glabrous, sides usually smooth, thin, margins and back usually lightly reticulate-veined, transversely corrugated, back usually with medial, raised line, not pitted, mucro absent. Seeds 1–2 mm.</description>
  <description type="distribution">Calif., Oreg.</description>
  <other_name type="past_name">hickmani</other_name>
  <discussion>Sidalcea hickmanii is found in isolated populations from southern California to southwestern Oregon and appears to have a relict distribution. K. Andreasen and B. G. Baldwin (2001, 2003) suggested that it is basal within Sidalcea. It is distinctive in having three (normally two in subsp. petraea) involucellar bractlets attached to the calyx, no mucro on the mericarps, and leaves that are almost the same size and shape throughout the stem. Each subspecies apparently represents a distinct relictual colony; the sexuality of these is not well known because of the paucity of specimens. As in many sidalceas, this species in particular appears to be fire-dependent.</discussion>
  <discussion>Subspecies 7 (7 in the flora).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades usually lobed, incised ± to base; California</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades unlobed, incised to 1/2 length; California, Oregon</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Bracts (7–)10–12 mm, equaling or shorter than calyx; c San Luis Obispo County.</description>
      <determination>14b Sidalcea hickmanii subsp. anomala</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Bracts 5.5–7 mm, shorter than calyx; Napa County.</description>
      <determination>14c Sidalcea hickmanii subsp. napensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Involucellar bractlets 2(–3); petals white to pale pink; flowers bisexual or pistillate; sw Oregon.</description>
      <determination>14e Sidalcea hickmanii subsp. petraea</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Involucellar bractlets 3; petals pink, pale pink, pinkish lavender, or pale lavender; flowers bisexual; California</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Bracts broadly lanceolate, 5–7(–10) × 2.5–4 mm, slightly shorter than calyx; involucellar bractlets equaling or slightly shorter than calyx; leaf blades: distal unlobed or incised to 1/4 length; Santa Barbara and San Bernardino counties.</description>
      <determination>14d Sidalcea hickmanii subsp. parishii</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Plants 0.4–0.8 m; stems brick red, greenish, or grayish; calyces stellate-puberulent, hairs longest at margins; involucellar bractlets 2–7 mm; largest leaf blades deeply cordate, 2.5–7 cm wide; inflorescences dense; Monterey County.</description>
      <determination>14a Sidalcea hickmanii subsp. hickmanii</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Bracts of distal flowers 1, cupped; leaf blades 0.6–1.5 × 0.7–2.2 cm; stems distally stellate-hairy, hairs appressed, 0.2–0.5 mm; plants 0.1–0.4 m; inflorescences not spiciform, to 10-flowered; calyces 4–5.5 mm; n Lake County.</description>
      <determination>14f Sidalcea hickmanii subsp. pillsburiensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Bracts of distal flowers usually 2, flat or cupped; leaf blades (1–)2–4 × (1–)2.7 cm; stems distally hairy, hairs tufted, 0.5–1.2 mm; plants (0.2–)0.3(–0.4) m; inflorescences spiciform in age, 10+-flowered; calyces 6–7 mm; Marin County.</description>
      <determination>14g Sidalcea hickmanii subsp. viridis</determination>
    </key_statement>
  </key>
</bio:treatment>
