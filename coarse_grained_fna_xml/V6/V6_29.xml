<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <other_info_on_meta type="treatment_page">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">6</other_info_on_meta>
    <other_info_on_meta type="mention_page">22</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">cucurbitaceae</taxon_name>
    <taxon_name rank="genus" authority="Kellogg" date="1854">MARAH</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Calif. Acad. Sci.</publication_title>
      <place_in_publication>1: 38. 1854</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cucurbitaceae;genus MARAH</taxon_hierarchy>
    <other_info_on_name type="fna_id">119660</other_info_on_name>
  </taxon_identification>
  <number>10.</number>
  <description type="morphology">Plants perennial, monoecious, sometimes temporarily dioecious, trailing or climbing; stems annual, usually glabrous, sometimes scabrous; roots tuberous, globose to fusiform; tendrils unbranched or 2–3-branched. Leaves: bracts absent; blade suborbiculate, shallowly to deeply palmately 5(–7)-lobed, lobes deltate to oblong-ovate or ovate, margins entire or remotely and coarsely dentate-lobulate, surfaces eglandular. Inflorescences: staminate flowers 5–100 in axillary panicles or racemoid panicles; pistillate flowers solitary, in same axils as staminate; peduncles erect at apex; bracts absent. Flowers: hypanthium campanulate to cupulate; sepals 5, linear or filiform to subulate or deltate, sometimes vestigial; petals 5, connate 1/2 length, usually white, cream-yellow, or greenish yellow, rarely greenish, triangular to ovate or oblong-ovate, (1–)2.5–10(–12) mm, glandular-villous adaxially, corolla campanulate, cupulate, or rotate. Staminate flowers: stamens 3(–4); filaments inserted near hypanthium base, connate, sometimes vestigial; thecae not fused into ring, forming a head, arched-flexuous, connective broad; pistillodes absent. Pistillate flowers: ovary (2–)4(–8)-locular, ovoid to globose; ovules 1–4 per locule; style 1, short to nearly vestigial; stigmas 1, discoid to subglobose head; staminodes present or absent. Fruits capsular, yellowish green to orange-yellow, often green-striped, globose to subglobose or depressed-globose, short-ellipsoid, ovoid, or oblong, symmetric, often short-beaked, dry, thin-walled, moderately to sparsely or densely echinate (smooth to sparsely echinate or muriculate in M. watsonii), irregularly dehiscent by splitting or dropping of beak. Seeds 1–16(–24), orbicular to suborbicular, ellipsoid, oblong, ovoid, obovoid, or oblanceoloid, slightly compressed, or globose to subglobose, not compressed, not arillate, margins usually not differentiated, slightly grooved in several species, surface smooth. x = 16.</description>
  <description type="distribution">w North America, nw Mexico.</description>
  <discussion>Species 8 (7 in the flora).</discussion>
  <discussion>Marah micrantha Dunn, endemic to Cedros Island of Baja California, is the only species of the genus without at least part of its range in the United States. See comments below under 6. M. macrocarpa.</discussion>
  <discussion>The only consistent distinctions of Marah from Echinocystis and Echinopepon noted by K. M. Stocking (1955) are the hypogeous (versus epigeous) germination of Marah seeds and its massively tuberous (versus mostly fibrous) roots. The common name manroot alludes to the large tubers, which in M. macrocarpa can weigh more than 160 kg.</discussion>
  <discussion>R. A. Schlising (1993) noted without documentation that “presumed hybrids occur where species overlap,” but K. M. Stocking (1955b) found no evidence of hybridization between species of Marah in nature or in experimental gardens.</discussion>
  <discussion>A remarkable adaptation in seedling establishment of Marah to the Mediterranean climate of California was described by R. A. Schlising (1969). In seed germination, in the cooler and wetter months of November and December, the radicle and epicotyl are carried 5–25 cm down into the soil by the elongating fused bases (the petioles) of the fleshy hypogeal cotyledons. As they elongate, the fused cotyledon bases form a tube that carries the embryonic axis at its tip. From that point, already deep in the soil, the radicle grows downward while the epicotyl grows upward through the petiole tube. The epicotyl reaches the soil surface by early spring, completes the first season’s growth, and dries up by the beginning of the arid summer season. Concurrently with epicotyl growth, the hypocotyl begins to enlarge, forming a tuber. The subterranean cotyledons transfer nutrients to the tuber, which produces shoots in following seasons.</discussion>
  <references>
    <reference>Stocking, K. M. 1955b. Some taxonomic and ecological considerations of the genus Marah. Madroño 13: 113–137.</reference>
  </references>
  <other_name type="common_name">Manroot [Hebrew marah, bitter, alluding to taste of all parts]</other_name>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Capsules globose to subglobose or depressed-globose to short-ellipsoid</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Capsules usually oblong, ellipsoid, or ovoid</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Corollas deeply cupulate to campanulate; leaf blades glaucous abaxially; capsules smooth to sparsely echinate or muriculate, spinules weak, 1–2 mm.</description>
      <determination>3 Marah watsonii</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Corollas rotate; leaf blades not glaucous; capsules sparsely to densely echinate, spinules rigid or flexuous, 2–12 mm</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Corollas yellowish green to cream-yellow or (especially inland) white; capsules sparsely to densely echinate, spinules 4–12 mm.</description>
      <determination>1 Marah fabacea</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Corollas white; capsules densely echinate, spinules 2–3(–5) mm.</description>
      <determination>2 Marah gilensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Sepals (pistillate) 6–7 mm, linear; seeds 2–4.</description>
      <determination>4 Marah guadalupensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Sepals (pistillate) 0.4–1 mm, filiform to subulate or deltate, sometimes vestigial; seeds 3–6 or 4–20(–24)</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Capsules 4–6.5(–8) cm, surface sparsely to moderately echinate (usually smooth distally), spinules weak, flexible, 3–6 mm; seeds 3–6.</description>
      <determination>5 Marah oregana</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Capsules (5–)8–15(–20) cm, surface densely echinate, spinules rigid, 5–35 mm; seeds 4–20(–24)</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Corollas shallowly cupulate to rotate; seeds not flat at one end.</description>
      <determination>6 Marah macrocarpa</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Corollas deeply cupulate to campanulate or campanulate-rotate; seeds flat at one end.</description>
      <determination>7 Marah horrida</determination>
    </key_statement>
  </key>
</bio:treatment>
