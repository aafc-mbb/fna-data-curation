<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <other_info_on_meta type="treatment_page">127</other_info_on_meta>
    <other_info_on_meta type="mention_page">118</other_info_on_meta>
    <other_info_on_meta type="mention_page">128</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Batsch" date="unknown">violaceae</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">viola</taxon_name>
    <taxon_name rank="species" authority="M. S. Baker &amp; J. C. Clausen" date="unknown">charlestonensis</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>8: 58. 1945</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family violaceae;genus viola;species charlestonensis</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250100908</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Viola</taxon_name>
    <taxon_name rank="species" authority="Kellogg" date="unknown">purpurea</taxon_name>
    <taxon_name rank="variety" authority="(M. S. Baker &amp; J. C. Clausen) S. L. Welsh &amp; Reveal" date="unknown">charlestonensis</taxon_name>
    <taxon_hierarchy>genus Viola;species purpurea;variety charlestonensis</taxon_hierarchy>
  </taxon_identification>
  <number>12.</number>
  <other_name type="common_name">Charleston Mountain or Charleston violet</other_name>
  <description type="morphology">Plants perennial, caulescent, not stoloniferous, 6–15 cm. Stems 1–3, prostrate, decumbent, or erect, leafy proximally and distally, 1/2–2/3 subterranean, glabrous or puberulent, on caudex from usually vertical, subligneous rhizome. Leaves basal and cauline; basal: 1–3; stipules adnate to petiole, forming 2 linear-lanceolate wings, margins entire or sparingly lacerate, apex of each wing free, acuminate; petiole 3.5–13.5 cm, densely short-puberulent; blade purplish abaxially (often dark purple-veined), grayish adaxially with prominent whitish veins (from dense hairs), usually orbiculate to broadly ovate, sometimes reniform, thick, 1–3.5 × 1.1–3.3 cm, base attenuate or truncate, margins entire, ciliate mostly on proximal half of blade, apex acute to obtuse, mucronulate, surfaces densely short-puberulent; cauline similar to basal except: stipules deltate to lanceolate, apex acute; petiole 1.9–3.4 cm; blade ovate or elliptic to deltate, 0.7–3.1 × 0.6–2.2 cm, length 0.7–1.9 times width, base usually attenuate, sometimes subcordate, margins entire, ciliate. Peduncles 1.7–6.6 cm, pubescent. Flowers: sepals linear-lanceolate, margins ciliate or eciliate, auricles 0.5–1 mm; petals deep lemon-yellow adaxially, upper 2 usually conspicuously reddish brown to brownish purple abaxially, lateral 2 streaked or solid reddish brown, lower 3 and sometimes upper 2 dark brown-veined proximally, lateral 2 bearded, lowest 8–13 mm, spur usually reddish brown, sometimes yellowish, gibbous, 0.4–2 mm, glabrous or scabrous abaxially; style head bearded; cleistogamous flowers axillary. Capsules spherical, 4.5–9 mm, puberulent. Seeds black, 3.4–3.5 mm. 2n = 12.</description>
  <description type="phenology">Flowering May–Jun.</description>
  <description type="habitat">On limestone hills, slopes, and dry washes beneath Pinus monophylla, P. ponderosa, Juniperus osteosperma, and/or Cercocarpus sp.</description>
  <description type="elevation">2000–2900 m</description>
  <description type="distribution">Nev., Utah.</description>
  <discussion>Viola charlestonensis is known only from the Spring Mountains (previously called Charleston Mountains) in Nevada and Zion National Park, Utah. M. S. Baker (in I. W. Clokey 1945) stated that E. C. Jaeger reportedly collected it at Jacob’s Pool, Arizona, in July 1926. The location of this single Arizona collection may be an error (R. J. Little 2001).</discussion>
  <discussion>M. S. Baker and J. Clausen (in I. W. Clokey 1945) stated that Viola charlestonensis is the only species in Becker’s Nuttallianae group with the spur pubescent on the exterior. In some populations in the Spring Mountains, Nevada, the spur and the midvein on the abaxial surface of the lowest petal to ± the middle of the lowest petal are covered ± densely with short hairs. Scattered hairs are also present on the abaxial surface of the lateral and upper petals. In other populations in the Spring Mountains, short hairs are mostly absent on the spur, lowest petal, and abaxial surfaces of lateral and upper petals.</discussion>
  <discussion>M. S. Baker (in I. W. Clokey 1945) commented that he observed numerous sterile flowers and relatively few mature capsules and seeds of Viola charlestonensis plants when he visited the Spring Mountains in June 1937. Similar observations were made of V. charlestonensis plants in the Spring Mountains at one location in 2009 and of three other locations in 2010.</discussion>
</bio:treatment>
