<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <other_info_on_meta type="treatment_page">330</other_info_on_meta>
    <other_info_on_meta type="mention_page">320</other_info_on_meta>
    <other_info_on_meta type="mention_page">322</other_info_on_meta>
    <other_info_on_meta type="mention_page">334</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">malvaceae</taxon_name>
    <taxon_name rank="subfamily" authority="Burnett" date="unknown">Malvoideae</taxon_name>
    <taxon_name rank="genus" authority="A. Gray" date="1849">sidalcea</taxon_name>
    <taxon_name rank="species" authority="Piper" date="unknown">cusickii</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Biol. Soc. Wash.</publication_title>
      <place_in_publication>29: 99. 1916</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus sidalcea;species cusickii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101120</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Sidalcea</taxon_name>
    <taxon_name rank="species" authority="(Nuttall ex Torrey &amp; A. Gray) A. Gray" date="unknown">cusickii</taxon_name>
    <taxon_name rank="subspecies" authority="C. L. Hitchcock" date="unknown">purpurea</taxon_name>
    <taxon_hierarchy>genus Sidalcea;species cusickii;subspecies purpurea</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">S.</taxon_name>
    <taxon_name rank="species" authority="unknown" date="unknown">oregana</taxon_name>
    <taxon_name rank="variety" authority="(Piper) Roush" date="unknown">cusickii</taxon_name>
    <taxon_hierarchy>genus S.;species oregana;variety cusickii</taxon_hierarchy>
  </taxon_identification>
  <number>7.</number>
  <other_name type="common_name">Cusick’s checkerbloom</other_name>
  <description type="morphology">Herbs, perennial, 0.4–1.8 m, not glaucous, with thick taproot and freely-rooting rhizomes usually 3–10 mm diam. Stems usually several, clustered, erect, often purplish, often thick and hollow proximally, usually proximally ± scabrous, hairs stellate and distally stellate-hairy, sometimes glabrous. Leaves mostly cauline; stipules persistent or deciduous, lanceolate, 6–8 × 1–1.5 mm; proximal petioles 25–30 cm, 4–5 times as long as blade, reduced to 7–13 cm on midstem leaves and to 2–3 cm on distalmost leaves, these to 1/2 times as long as blades; basal blades wide-ovate to orbiculate, shallowly to deeply, palmately 5–7(–9)-lobed, 6–13 × 6–13 cm, lobe margins toothed, teeth rounded, sometimes Ribes-like, base cordate, apex rounded, surfaces: abaxial densely stellate-pubescent, adaxial glabrous or sparsely stellate-pubescent on veins, cauline blades round, divided nearly to base, 5–9-lobed, lobes again deeply incised to subentire, 10–20 × 10–20 cm, distalmost 5–7-cleft to base. Inflorescences erect, spiciform, dense, calyces usually conspicuously overlapping in flower and sometimes in fruit, usually 20–30-branched per primary stem, 20+-flowered, elongate, not 1-sided, most flowers usually open at same time, branches relatively short, each spike unit 6 cm, longer in fruit; bracts ovate-lanceolate to subulate, undivided, 1–6(–10) mm, subequal or longer than pedicels, much shorter than calyx. Pedicels 1–2(–5) mm; involucellar bractlets absent. Flowers bisexual or unisexual and pistillate, pistillate plants more frequent than bisexual ones, some flowers may be staminate, plants gynodioecious; calyx urceolate with swollen base especially in young fruit, pistillate 6–8 mm, bisexual 6–10 mm, strongly reticulate-veined, usually finely stellate-puberulent, sometimes glabrate, lobes usually purple tinted; petals 7 or 8, pale pink, pinkish rose, or rose-purple, usually neither pale-veined nor white at base, pistillate 8–12(–14) mm, staminate or bisexual (10–)11–19(–23) mm; staminal column 5–6 mm, hairy; anthers white; stigmas 7 or 8. Schizocarps 6 mm diam.; mericarps 7 or 8, 3 mm, apical margins ± rounded, sides smooth or slightly reticulate-veined, not pitted, back essentially smooth, very sparsely glandular-puberulent near tip, mucro 0.5–1 mm. Seeds 2 mm. 2n = 20.</description>
  <description type="phenology">Flowering Jun–Aug.</description>
  <description type="habitat">Moist to wet, mostly black, adobe soil, lowland and mountain meadows, often with Juncus and Camassia</description>
  <description type="elevation">100–200(–500), 1000–1400 m</description>
  <description type="distribution">Oreg.</description>
  <discussion>Sidalcea cusickii is showy and distinctive. It has been confused with S. campestris, S. hendersonii, S. nelsoniana, S. oregana (subsp. spicata), S. setosa, and S. virgata. Molecular data support a close relationship with S. hendersonii and S. virgata (K. Andreasen and B. G. Baldwin 2003b). Hitchcock distinguished subsp. cusickii and subsp. purpurea, the latter with purple, ciliate calyx lobes and stem proximally glabrous and thought to be more closely related to S. hendersonii and S. nelsoniana. The two variations overlap in range and characteristics and are not recognized here. There appear to be two groups of populations at different elevation ranges with little overlap. Overall, S. cusickii can be distinguished by its range and by its spikelike, many-flowered, thick, compounded racemes in which essentially all of the flowers are open at the same time. It is also unusual in the preponderance of pistillate-flowered individuals in a given population and the presence of what appear to be truly staminate, rather than bisexual, flowers on some individuals. It is found in the Willamette and Umpqua valley region in Douglas, Josephine, and Lane counties.</discussion>
</bio:treatment>
