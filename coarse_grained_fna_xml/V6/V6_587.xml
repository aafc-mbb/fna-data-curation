<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <author>Steven R. Hill</author>
      <date>unknown</date>
    </source>
    <other_info_on_meta type="treatment_page">319</other_info_on_meta>
    <other_info_on_meta type="mention_page">216</other_info_on_meta>
    <other_info_on_meta type="mention_page">219</other_info_on_meta>
    <other_info_on_meta type="mention_page">320</other_info_on_meta>
    <other_info_on_meta type="mention_page">327</other_info_on_meta>
    <other_info_on_meta type="mention_page">328</other_info_on_meta>
    <other_info_on_meta type="mention_page">329</other_info_on_meta>
    <other_info_on_meta type="mention_page">330</other_info_on_meta>
    <other_info_on_meta type="mention_page">332</other_info_on_meta>
    <other_info_on_meta type="mention_page">334</other_info_on_meta>
    <other_info_on_meta type="mention_page">335</other_info_on_meta>
    <other_info_on_meta type="mention_page">339</other_info_on_meta>
    <other_info_on_meta type="mention_page">340</other_info_on_meta>
    <other_info_on_meta type="mention_page">345</other_info_on_meta>
    <other_info_on_meta type="mention_page">347</other_info_on_meta>
    <other_info_on_meta type="mention_page">349</other_info_on_meta>
    <other_info_on_meta type="mention_page">351</other_info_on_meta>
    <other_info_on_meta type="mention_page">353</other_info_on_meta>
    <other_info_on_meta type="mention_page">354</other_info_on_meta>
    <other_info_on_meta type="mention_page">355</other_info_on_meta>
    <other_info_on_meta type="volume">6</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">malvaceae</taxon_name>
    <taxon_name rank="subfamily" authority="Burnett" date="unknown">Malvoideae</taxon_name>
    <taxon_name rank="genus" authority="A. Gray" date="1849">SIDALCEA</taxon_name>
    <place_of_publication>
      <publication_title>Mem. Amer. Acad. Arts, n. s.</publication_title>
      <place_in_publication>4: 18. 1849</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family malvaceae;subfamily malvoideae;genus sidalcea;</taxon_hierarchy>
    <other_info_on_name type="etymology">Generic names Sida and Alcea, alluding to resemblances</other_info_on_name>
    <other_info_on_name type="fna_id">130308</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="Greene" date="unknown">Hesperalcea</taxon_name>
    <taxon_hierarchy>genus Hesperalcea</taxon_hierarchy>
  </taxon_identification>
  <number>46.</number>
  <other_name type="common_name">Checkerbloom</other_name>
  <other_name type="common_name">checker mallow</other_name>
  <description type="morphology">Herbs, annual or perennial, or subshrubs, sometimes glaucous, usually hairy, sometimes glabrate, hairs stellate or simple or both, with taproot, clustered fleshy roots, caudex, or adventitious roots, sometimes with shallow, elongated or compact rhizomes often termed rootstocks. Stems erect or ascending, reclining to decumbent at base, these often rooting, stolonlike stems sometimes present.</description>
  <description type="distribution">w North America; n Mexico.</description>
  <discussion>Some species of Sidalcea are cultivated for ornament. The common name checkerbloom comes from the pattern of veins on the petals of some species.</discussion>
  <discussion>Some species of Sidalcea are similar and difficult to distinguish; details of the plant habit and fruits are generally necessary for identification. Most Sidalcea species, especially those in the S. oregana group and S. malviflora group, show great plasticity. The presence of gynodioecy is widespread in Sidalcea, and pistillate and bisexual flowers often have different sizes within a population (the pistillate being smaller). As with most other mallows that have schizocarps, the mericarp features usually are not evident until full dry maturity. The inflorescences are generally rather congested initially and later may elongate; both alternatives in the key should be considered. Inflorescence descriptions in the key are for plants in full flower; staminal column measurements are for bisexual flowers only.</discussion>
  <discussion>There is some difficulty in applying the term rhizome in Sidalcea, although it has widespread traditional use in keys to the species; most structures called rhizomes are at or near the soil surface and may be decumbent rooting stems; rhizomes are best developed in S. calycosa subsp. rhizomata, S. gigantea, and S. hirtipes.</discussion>
  <discussion>The rank subspecies is widely used in this treatment in accordance with C. L. Hitchcock (1957) and S. R. Hill (2012b). Varieties were distinguished within some of the subspecies in past treatments. See Hitchcock for more information on the published varieties.</discussion>
  <discussion>Species 31 (31 in the flora).</discussion>
  <references>
    <reference>Andreasen, K. and B. G. Baldwin. 2001. Unequal evolutionary rates between annual and perennial lineages of checker mallows (Sidalcea, Malvaceae): Evidence from 18S-26S rDNA internal and external transcribed spacers. Molec. Biol. Evol. 18: 936–944.</reference>
    <reference>Andreasen, K. and B. G. Baldwin. 2003. Reexamination of relationships, habital evolution, and phylogeography of checker mallows (Sidalcea, Malvaceae) based on molecular phylogenetic data. Amer. J. Bot. 90: 436–444.</reference>
    <reference>Andreasen, K. and B. G. Baldwin. 2003b. Nuclear ribosomal DNA sequence polymorphism and hybridization in checker mallows (Sidalcea, Malvaceae). Molec. Phylogen. Evol. 29: 563–581.</reference>
    <reference>Halse, R. R., B. A. Rottink, and R. Mishaga. 1989. Studies in Sidalcea taxonomy. NorthW. Sci. 63: 154–161.</reference>
    <reference>Hill, S. R. 1993. Sidalcea. In: J. C. Hickman, ed. 1993. The Jepson Manual. Higher Plants of California. Berkeley, Los Angeles, and London. Pp. 755–760.</reference>
    <reference>Hill, S. R. 2012b. Sidalcea. In: B. G. Baldwin et al., eds. 2012. The Jepson Manual: Vascular Plants of California, ed. 2. Berkeley. Pp. 887–896.</reference>
    <reference>Hitchcock, C. L. 1957. A Study of the Perennial Species of Sidalcea. Seattle. [Univ. Wash. Publ. Biol. 18.]</reference>
    <reference>Roush, E. M. F. 1931. A monograph of the genus Sidalcea. Ann. Missouri Bot. Gard. 18: 117–244.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Herbs, annual, except S. calycosa subsp. rhizomata</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Herbs, perennial, or subshrubs</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Cauline leaf blades: lobes of mid and distal usually apically 2–5-toothed, linear to obovate; stems bristly-hairy, hairs erect; mericarps usually glabrous, mucro absent</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Cauline leaf blades: lobes of mid and distal entire, not apically 2–5-toothed, lobes linear to linear-elliptic or oblanceolate-obtuse; stems usually glabrate at least proximally; mericarps glabrous or sparsely puberulent, mucro present</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stipules at mid and distal nodes divided into 2–5 filiform or linear segments, subequal to or longer than calyx, becoming involucrelike; distal cauline leaves: blades 5–7-lobed, lobes linear, sometimes 3-toothed, mid tooth longer than laterals, or lobe margins entire; calyces seldom glandular; mericarps without bristles.</description>
      <determination>8 Sidalcea diploscypha</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stipules and mid and distal nodes linear-filiform, rarely divided, usually much shorter than calyx, not involucrelike; distal cauline leaves: blades 3-lobed, lobes obovate, apically 2–5-toothed, teeth subequal; calyces usually glandular; mericarps usually with 1–5 minute proximal apical bristles.</description>
      <determination>17 Sidalcea keckii</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Stamens: outer filaments distally distinct, anthers not attached to connate portion of filaments; mericarps reticulate-veined on back, deeply pitted especially on top, glabrous or glandular-puberulent; calyces 8–10(–12) mm.</description>
      <determination>12 Sidalcea hartwegii</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Stamens: outer filaments connate to apex, staminal column funnel-like with continuous rim to which the unstalked anthers attach; mericarps glabrous or puberulent, longitudinally grooved or reticulate-veined; calyces 4–12 mm</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Stems glabrous or sparsely stellate-hairy; plants annual or perennial; bracts ovate to wide-elliptic, 2–5 mm wide; mericarps glabrous, longitudinally grooved; calyces stellate-puberulent and strigose-bristly.</description>
      <determination>22 Sidalcea calycosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Stems usually densely bristly-hairy distally, rarely glabrescent; plants annual; bracts linear, to 2 mm wide; mericarps puberulent, reticulate-veined, pitted; calyces prominently tawny-hirsute and densely stellate-canescent.</description>
      <determination>15 Sidalcea hirsuta</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Mericarps: mucro absent; involucellar bractlets (0 or)1–3; leaves evenly arrayed on stem throughout season, blades all similar in shape</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Mericarps: mucro present; involucellar bractlets absent; leaves not evenly arrayed, often proximally crowded at least early in season or not (S. gigantea), sometimes rosettelike, proximal and distal blades usually dissimilar</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Involucellar bractlets (0–)1–2; petals white or purple-tinged; leaf blades lobed, maplelike; plants (0.4–)0.8–1.5(–2) m.</description>
      <determination>18 Sidalcea malachroides</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Involucellar bractlets (2)3; petals usually pale pink to pink-lavender, rarely white; leaves unlobed or lobed, but not maplelike; plants 0.1–0.5(–0.8) m</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Inflorescences usually open or spiciform, sometimes dense early; bracts: proximalmost not involucrelike; stipule width sometimes, but not obviously, exceeding stem diam.; leaf blades orbiculate to flabelliform, unlobed or lobed, base truncate or cordate; widely scattered, local in California but not in Sierra Nevada, also in sw Oregon.</description>
      <determination>14 Sidalcea hickmanii</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Inflorescences ± dense, capitate; bracts: proximalmost involucrelike; stipule width exceeding stem diam.; leaf blades ovate to elliptic, unlobed, base cordate; local in n Sierra Nevada, Nevada County, California.</description>
      <determination>30 Sidalcea stipularis</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Plants (0.8–)2(–2.5) m, usually in colonies, rhizomes (6–)10 mm diam., with reflexed, appressed bristle hairs; stems erect proximally, hollow, densely bristly, bristles reflexed; high Cascades and high Sierra Nevada, California.</description>
      <determination>10 Sidalcea gigantea</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Plants (0.1–)0.5–1(–2) m, seldom in colonies, without rhizomes or rhizomes usually 3–10 mm diam., hairs usually erect, retrorse or appressed, simple or stellate or glabrescent, sometimes glabrous; stems erect, sprawling, or ascending to decumbent, proximally usually solid, seldom hollow, hairy, hairs sometimes bristlelike; widespread</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Inflorescences dense, subcapitate or spiciform, not 1-sided; calyces usually overlapping others in flower, sometimes in fruit</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Inflorescences usually ± open, sometimes dense, sometimes 1-sided; calyces usually not conspicuously overlapping others in flower or in fruit</description>
      <next_statement_id>19</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Mericarps smooth, slightly reticulate-veined, or pitted (sometimes slightly wrinkled); inflorescences continuous (flowers sometimes clustered but not regularly interrupted), usually dense in flower, sometimes elongating in fruit; rhizomes present or not</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Mericarps usually reticulate-veined, sometimes pitted; inflorescences dense in flower, elongated or interrupted in fruit; rhizomes or rooting stems sometimes present</description>
      <next_statement_id>15</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Petals white to pale pinkish, drying yellowish, 10–20 mm; rhizomes present; herbage glabrous, hirsute, puberulent, or stellate-hairy.</description>
      <determination>4 Sidalcea candida</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Petals pale pink, pinkish rose, pink, or pinkish lavender, dark rose-pink, or magenta, not drying yellowish, (5–)10–15(–23) mm; rhizomes present or not; herbage hairy</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Rhizomes absent; inflorescences unbranched or branched, flowers opening and closing sequentially from base to apex, usually 3–10 open at same time; stems usually solid; calyx lobes not strongly veined, usually green; widespread in w North America.</description>
      <determination>23 Sidalcea oregana</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Rhizomes present; inflorescences branched, most flowers usually open at same time; stems usually hollow proximally; calyx lobes strongly veined, usually purplish; inland w Oregon and immediate coast from Oregon to Alaska</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Mericarps 3 mm, apical margins ± rounded, mucro 0.5–1 mm; calyces usually stellate-puberulent, sometimes glabrate; inland.</description>
      <determination>7 Sidalcea cusickii</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Mericarps 4 mm, apical margins sharp-edged, not winged, mucro 0.8–1.3 mm; calyces stellate-hairy or proximally glabrous; coastal marshes.</description>
      <determination>13 Sidalcea hendersonii</determination>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Plants without rhizomes, stems not proximally rooting; stems single or multiple, clustered, not rooting; petals not notably whitish- or pale-veined</description>
      <next_statement_id>16</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Plants with rhizomes or stems proximally rooting; stems usually scattered, not clustered; petals usually whitish- or pale-veined</description>
      <next_statement_id>17</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Calyces 3.5–10 mm in flower and fruit, usually uniformly stellate-puberulent, sometimes also with bristles; inflorescences spicate, 10–30 cm.</description>
      <determination>23 Sidalcea oregana</determination>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Calyces 5–9 mm, to 10 mm in fruit, stellate-hairy and bristly; inflorescences subcapitate or spicate, 3–7(–10) cm.</description>
      <determination>28 Sidalcea setosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Inflorescences interrupted, rachis exposed between flower clusters; inland mountains in Kern and Tulare counties, California.</description>
      <determination>25 Sidalcea ranunculacea</determination>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Stems erect; inflorescences usually long-pedunculate; plants usually in colonies, with cordlike rhizomes; stems coarsely bristly; calyces 11–16 mm in fruit; nw Oregon, sw Washington.</description>
      <determination>16 Sidalcea hirtipes</determination>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Stems decumbent to suberect; inflorescences not long-pedunculate; plants in colonies or not, stem bases usually rooting or with compact rhizomes but without cordlike rhizomes; stems often soft-bristly; calyces 8–11(–13) mm in fruit; California, sw Oregon.</description>
      <determination>19 Sidalcea malviflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Mericarps usually glabrous or glandular-puberulent; stems erect, proximally stellate-hairy, distally glabrous, glaucous; inflorescences 30–40(–45) cm; Butte County, California.</description>
      <determination>27 Sidalcea robusta</determination>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Mericarps glabrous or puberulent; stems erect, decumbent, sprawling, prostrate, or ascending, glabrous or hairy, glaucous; inflorescences 2–20(–45) cm; not limited to Butte County, California</description>
      <next_statement_id>20</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Mericarps usually not, sometimes slightly, reticulate-veined or pitted, usually glabrous, rarely sparsely glandular-puberulent; without rhizomes</description>
      <next_statement_id>21</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Basal leaves deeply incised, lobes again incised, segments linear to oblong-elliptic; cauline leaves 1–3; stems usually unbranched (plants nearly scapose); inflorescences much elongated in fruit; San Bernardino County, California.</description>
      <determination>24 Sidalcea pedata</determination>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Basal leaves absent or unlobed or shallowly lobed; cauline leaves usually 3+; stems often branched (plants usually not appearing to be scapose); inflorescences usually somewhat elongated in fruit; not restricted to San Bernardino County, California</description>
      <next_statement_id>22</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>22</statement_id>
      <description type="morphology">Roots fleshy; plants usually with nonwoody taproot and fleshy roots; calyces sparsely hirsute, hairs pustulose and sometimes stellate, surface not obscured.</description>
      <determination>22 Sidalcea neomexicana</determination>
    </key_statement>
    <key_statement>
      <statement_id>22</statement_id>
      <description type="morphology">Roots not fleshy; plants with fibrous or woody caudex or taproot; calyces densely stellate-puberulent and sometimes with nonpustulose bristles, surface sometimes obscured.</description>
      <determination>23 Sidalcea oregana</determination>
    </key_statement>
    <key_statement>
      <statement_id>23</statement_id>
      <description type="morphology">Roots not fleshy; rhizomes and caudices present or absent; stems single or clustered or scattered; inflorescences 2–30+-flowered; usually not alkaline flats</description>
      <next_statement_id>25</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>24</statement_id>
      <description type="morphology">Leaves mostly basal; blades of basal leaves glaucous, 5–7-lobed, deeply incised; calyces stellate-puberulent, without pustulose bristles; e of Sierra Nevada, Inyo County, California.</description>
      <determination>6 Sidalcea covillei</determination>
    </key_statement>
    <key_statement>
      <statement_id>24</statement_id>
      <description type="morphology">Leaves basal and cauline; blades of basal leaves glaucous or not, unlobed or 5(–9)-lobed, shallowly incised; calyces stellate-puberulent and with pustulose bristles; widespread.</description>
      <determination>22 Sidalcea neomexicana</determination>
    </key_statement>
    <key_statement>
      <statement_id>25</statement_id>
      <description type="morphology">Inflorescences usually 30+-flowered, 5–20(–30) in S. virgata, loosely or densely spiciform, usually branched, usually not 1-sided; petals of bisexual flowers 5–15(–30) mm; stems usually erect, sometimes proximally decumbent</description>
      <next_statement_id>26</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>25</statement_id>
      <description type="morphology">Inflorescences usually 2–10(–30)-flowered, usually racemose, sometimes spiciform, branched or not, 1-sided or not; petals of bisexual flowers (5–)10–33 mm; stems erect, ascending to decumbent, sprawling, or nearly prostrate</description>
      <next_statement_id>29</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>26</statement_id>
      <description type="morphology">Stems erect, decumbent-ascending, or trailing; plants 0.3–0.6(–0.8) m, with rhizomes and taproot; stems proximally hairy, hairs usually soft, tangled, and stellate; inflorescences: proximalmost 1 or 2 flowers usually leafy-bracted.</description>
      <determination>31 Sidalcea virgata</determination>
    </key_statement>
    <key_statement>
      <statement_id>26</statement_id>
      <description type="morphology">Stems erect; plants (0.3–)1–2 m, with rhizomes or not; stems proximally hairy, hairs simple or stellate, not soft or tangled; inflorescences: proximalmost 1 or 2 flowers not leafy-bracted</description>
      <next_statement_id>27</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>27</statement_id>
      <description type="morphology">Plants 0.5–2 m; petals nearly white to pale pink or pale lavender, usually not overlapping; calyces 8–10 mm in fruit.</description>
      <determination>3 Sidalcea campestris</determination>
    </key_statement>
    <key_statement>
      <statement_id>27</statement_id>
      <description type="morphology">Plants 0.3–1.5 m; petals pink, pinkish purple, pink-purple, purplish rose, or magenta, usually overlapping, sometimes not in S. nelsoniana; calyces 3.5–10 mm in fruit</description>
      <next_statement_id>28</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>28</statement_id>
      <description type="morphology">Calyces 4–6 mm, lobes usually purple-tinged; plants with rhizomes; mericarps finely reticulate to faintly rugose or nearly smooth; stems glabrous or sparsely hirtellous; primarily Willamette Valley of Oregon, and sw Washington.</description>
      <determination>21 Sidalcea nelsoniana</determination>
    </key_statement>
    <key_statement>
      <statement_id>28</statement_id>
      <description type="morphology">Calyces 3.5–10 mm, lobes green; plants without rhizomes; mericarps usually slightly reticulate-veined or pitted; stems usually stellate-hairy, sometimes sparsely so; widespread, usually s, e and ne of Willamette Valley.</description>
      <determination>23 Sidalcea oregana</determination>
    </key_statement>
    <key_statement>
      <statement_id>29</statement_id>
      <description type="morphology">Plants glaucous, without rhizomes; stems not freely rooting, proximally glabrous or hairy, hairs stellate; calyces uniformly stellate-puberulent; frequently at elevations above 2000 m (900–3000 m)</description>
      <next_statement_id>30</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>29</statement_id>
      <description type="morphology">Plants seldom glaucous, with or without rhizomes; stems freely rooting or not, proximally hairy, hairs simple and stellate; calyces stellate-puberulent, usually bristly; usually at elevations below 2000 m (0–2700 m)</description>
      <next_statement_id>31</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>30</statement_id>
      <description type="morphology">Stems usually sprawling or decumbent to ascending, rarely erect; basal leaves 9 or fewer or deciduous, blades usually 5(–7)-lobed, lobes dentate or entire; California, n, c high Sierra Nevada, nw to the Cascades and Klamath ranges and e to Washoe County, Nevada.</description>
      <determination>11 Sidalcea glaucescens</determination>
    </key_statement>
    <key_statement>
      <statement_id>30</statement_id>
      <description type="morphology">Stems usually erect or ascending; basal leaves 10+, persistent, blades usually (5–)7–9-lobed, lobes pinnately or ternately lobed; California, high Sierra Nevada, e to Nevada.</description>
      <determination>20 Sidalcea multifida</determination>
    </key_statement>
    <key_statement>
      <statement_id>31</statement_id>
      <description type="morphology">Stems ascending-decumbent or creeping, freely rooting, usually proximally hairy, rarely glabrate, hairs simple, 2–3 mm, distally ± stellate-puberulent; leaf blade surfaces hairy, hairs simple; mericarps densely stellate-puberulent apically, on back, and on mucro.</description>
      <determination>26 Sidalcea reptans</determination>
    </key_statement>
    <key_statement>
      <statement_id>31</statement_id>
      <description type="morphology">Stems usually erect to ascending, sometimes prostrate or sprawling to decumbent and rooting or not, proximally hairy, hairs simple, stellate, or mixed; leaf blade surfaces hairy, hairs stellate or not; mericarps sparsely stellate-puberulent or glandular-puberulent apically, on back, and/or on mucro</description>
      <next_statement_id>32</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>32</statement_id>
      <description type="morphology">Leaves mostly basal, cauline usually to 5 and distally smaller, plants sometimes appearing scapose; inflorescences 10+-flowered, (15–)30–45 cm; mericarps slightly to moderately reticulate-veined and pitted.</description>
      <determination>29 Sidalcea sparsifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>32</statement_id>
      <description type="morphology">Leaves mostly cauline, sometimes clustered at base, distally smaller or not; inflorescences 2–10(–30)-flowered, 2–30(–40) cm; mericarps usually strongly reticulate-veined, roughened, pitted or honeycomb-pitted</description>
      <next_statement_id>33</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>33</statement_id>
      <description type="morphology">Calyces usually both stellate-hairy and bristly-hairy, if bristles absent, some stellate hairs larger; plants not glaucous; leaf blades unlobed or lobed; usually coastal, sometimes inland, seldom on serpentine.</description>
      <determination>19 Sidalcea malviflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>34</statement_id>
      <description type="morphology">Rhizomes absent or present, 4–6 mm diam., freely rooting or not; inflorescences usually erect; stems usually erect</description>
      <next_statement_id>36</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>35</statement_id>
      <description type="morphology">Stems proximally usually hairy, sometimes glabrate, hairs coarse, stellate; stems not especially brittle; n high Sierra Nevada, Cascade Ranges, high North Coast Ranges, Klamath Ranges, California to sw Oregon.</description>
      <determination>1 Sidalcea asprella</determination>
    </key_statement>
    <key_statement>
      <statement_id>35</statement_id>
      <description type="morphology">Stems proximally hairy or glabrate, hairs soft, simple and stellate; stems distally brittle; Klamath Ranges, nw California, sw Oregon.</description>
      <determination>9 Sidalcea elegans</determination>
    </key_statement>
    <key_statement>
      <statement_id>36</statement_id>
      <description type="morphology">Plants with or without caudex, usually with rooting rootstocks or rhizomes; stems sometimes sprawling, supported by other plants, proximally hairy, hairs stellate; leaf blades usually lobed, all similar in shape, lobe margins usually crenate; c, n High Sierra Nevada, Cascades, California, sw Oregon.</description>
      <determination>1 Sidalcea asprella</determination>
    </key_statement>
    <key_statement>
      <statement_id>36</statement_id>
      <description type="morphology">Plants with caudex and rootstocks, not freely rooting, without rhizomes; stems usually erect, free-standing, proximally hairy, hairs reflexed, stiff, simple, sometimes also coarse, 2–3-rayed, stellate; leaf blades lobed, proximals shallowly incised, lobe margins crenate-dentate, distals deeply incised, lobe margins entire or 1–5-toothed; n inner North Coast Ranges, California.</description>
      <determination>5 Sidalcea celata</determination>
    </key_statement>
  </key>
</bio:treatment>
