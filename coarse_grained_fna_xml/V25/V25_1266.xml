<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <other_info_on_meta type="treatment_page">474</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" date="unknown" authority="Barnhart">Poaceae</taxon_name>
    <taxon_name rank="subfamily" date="unknown" authority="Link">PANICOIDEAE</taxon_name>
    <taxon_name rank="tribe" date="unknown" authority="R. Br.">PANICEAE</taxon_name>
    <taxon_name rank="genus" date="unknown" authority="L.">PANICUM</taxon_name>
    <taxon_name rank="subgenus" date="unknown" authority="unknown">Panicum</taxon_name>
    <taxon_name rank="section" date="unknown" authority="Stapf">Repentia</taxon_name>
    <taxon_name rank="species" date="unknown" authority="L.">virgatum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus panicum;subgenus panicum;section repentia;species virgatum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Panicum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">virgatum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">spissum</taxon_name>
    <taxon_hierarchy>genus panicum;species virgatum;variety spissum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Panicum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">virgatum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">cubense</taxon_name>
    <taxon_hierarchy>genus panicum;species virgatum;variety cubense</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Panicum</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">bavardii</taxon_name>
    <taxon_hierarchy>genus panicum;species bavardii</taxon_hierarchy>
  </taxon_identification>
  <number>20</number>
  <other_name type="common_name">Switchgrass</other_name>
  <other_name type="common_name">Panic raide</other_name>
  <description type="morphology">Plants perennial; rhizomatous, rhizomes often loosely interwoven, hard, with closely overlapping scales, sometimes short or forming a knotty crown. Culms 40-300 cm tall, 3-5 mm thick, solitary or forming dense clumps, erect or decumbent, usually simple; nodes glabrous; internodes hard, glabrous or glaucous, green or purplish. Sheaths longer than the lower internodes, shorter than those above, glabrous or pilose, especially on the throat, margins usually ciliate; ligules 2-6 mm; blades 10-60 cm long, 2-15 mm wide, flat, erect, ascending or spreading, glabrous or pubescent, adaxial surfaces sometimes densely pubescent, particularly basally, bases rounded to slightly narrowed, margins scabrous. Panicles 10-55 cm long,  4-20 cm wide, exserted, open; primary branches thin, straight, solitary to whorled or fascicled, ascending to spreading, scabrous, usually rebranching once; pedicels 0.5-20 mm, appressed to spreading. Spikelets 2.5-8 mm long, 1.2-2.5 mm wide, narrowly lanceoloid, turgid to slightly laterally compressed, glabrous, acuminate. Lower glumes 1.8-3.2 mm, 1/2 - 4/5 as long as the spikelets, glabrous, 5-9-veined, acuminate; upper glumes and lower lemmas extending 0.4-3 mm beyond the upper florets, 7-11-veined, strongly gaping at the apices; lower florets staminate; lower paleas 3-3.5 mm, ovate-hastate, lateral lobes folded over the anthers before anthesis; upper florets 2.3-3 mm long, 0.8-1.1 mm wide, narrowly ovoid, smooth, glabrous, shiny; upper lemmas clasping the paleas only at the base. 2n = 18, 21, 25, 30, 32, 35, 36, 54-60, 67-72, 74, 77, 90, 108.</description>
  <discussion>Panicum virgatum grows in tallgrass prairies, especially mesic to wet types where it is a major component of the vegetation, and on dry slopes, sand, open oak or pine woodlands, shores, river banks, and brackish marshes. Its range extends, primarily on the eastern side of the Rocky Mountains, from southern Canada through the United States to Mexico, Cuba, Bermuda, and Costa Rica, and, possibly as an introduction, in Argentina. It has also been introduced as a forage grass to other parts of the world.</discussion>
  <discussion>Panicum virgatum is an important and palatable forage grass, but its abundance in native grasslands decreases with grazing. Several types are planted for range and wildlife habitat improvement. Plants from eastern New Mexico, western Texas, and northern Mexico tend to have larger spikelets (6-8 mm versus 2.5-5.5 mm) and are sometimes called P. havardii Vasey Tetraploids appear to be the most common ploidy level, especially in the upper midwest and northern plains, with higher ploidy levels being more common southwards, but plants in a small area can range from diploid through duodecaploid, with dysploid derivatives. If morphological markers matched chromosome numbers and ecotypic characters, the species could be considered an aggregate of numerous microspecies. In the absence of such correlations, it must be regarded as simply a wide-ranging, highly variable taxon. Plants identified as Panicum virgatum var. cubense Griseb. and P. virgatum var. spissum Linder represent end points of geographic clines.</discussion>
  <discussion>Panicum virgatum is not always readily separable from P. amarum, particularly P. amarum subsp. amarulum; future work may support their treatment as conspecific taxa.</discussion>
  <description type="distribution">Conn.;N.J.;N.Y.;Del.;D.C.;Wis.;W.Va.;Pacific Islands (Hawaii);Mass.;Maine;N.H.;R.I.;Vt.;Fla.;Wyo.;N.Mex.;Tex.;La.;Ala.;Ariz.;Colo.;Idaho;Ill.;Ind.;Minn.;Mo.;Mont.;N.C.;N.Dak.;Nebr.;Nev.;Okla.;Pa.;S.C.;S.Dak.;Tenn.;Utah;Va.;Man.;N.S.;Ont.;Que.;Sask.;Kans.;Ark.;Ga.;Iowa;Md.;Ohio;Mich.;Miss.;Ky.</description>
</bio:treatment>
