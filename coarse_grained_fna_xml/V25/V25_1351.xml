<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <author>J.K. Wipff;</author>
      <date>NA</date>
    </source>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="treatment_page">515</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" date="unknown" authority="Barnhart">Poaceae</taxon_name>
    <taxon_name rank="subfamily" date="unknown" authority="Link">PANICOIDEAE</taxon_name>
    <taxon_name rank="tribe" date="unknown" authority="R. Br.">PANICEAE</taxon_name>
    <taxon_name rank="genus" date="unknown" authority="Rich.">PENNISETUM</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus pennisetum</taxon_hierarchy>
  </taxon_identification>
  <number>25.15</number>
  <description type="morphology">Plants annual or perennial; habit various. Culms 3-800 cm, not woody, sometimes branching above the base; internodes solid or hollow. Ligules membranous and ciliate, or of hairs, rarely completely membranous; blades sometimes pseudopetiolate. Inflorescences spicate panicles with highly reduced branches termed fascicles; panicles 1-many per plant, terminal on the culms or on both the culms and the secondary branches, or terminal and axillary, or only axillary, usually completely exposed at maturity; rachises usually terete, with (1)5-many fascicles; fascicle axes 0.2-7.5(28) mm, with (1)3-130+ bristles and 1-12 spikelets. Bristles free or fused at the base, disarticulating with the spikelets at maturity; of 3 kinds, outer, inner, and primary, in some species with all 3 kinds present below each spikelet, in others 1 or more kinds missing from some or all of the spikelets; outer (lower) bristles antrorsely scabrous, terete; inner (upper) bristles antrorsely scabrous or long-ciliate, usually flatter and wider than the outer bristles; primary (terminal) bristles located immediately below the spikelets, solitary, antrorsely scabrous or long-ciliate, often longer than the other bristles associated with the spikelet; disarticulation usually at the base of the fascicles, sometimes also beneath the upper florets. Spikelets with 2 florets; lower glumes absent or present, 0-5-veined; upper glumes longer, 0-11-veined; lower florets sterile or staminate; lower lemmas usually as long as the spikelets, membranous, 3-15-veined, margins usually glabrous; lower paleas present or absent; upper lemmas membranous to coriaceous, 5-12-veined; upper paleas shorter than the lemmas but similar in texture; lodicules 0 or 2, glabrous; anthers 3, if present, x = 5,7, 8,9 (usually 9).</description>
  <discussion>Pennisetum has 80-130 species, most of which grow in the tropics and subtropics, and occupy a wide range of habitats. Twenty-five species are native to the Western Hemisphere, but none to the Flora region. Most of the species treated here are cultivated for food, forage, or as ornamental plants. Many species, including several cultivated species, are weedy. Four are classified as noxious weeds by the U.S. Department of Agriculture. Records known to be based on cultivated plants are not included in the distribution maps but, in many cases, it is not possible to determine whether a record is based on a cultivated plant or an escape.</discussion>
  <discussion>The placement of the boundary between Pennisetum and Cenchrus is contentious. As treated here, Pennisetum has antrorsely scabrous bristles that are not spiny, fascicle axes that terminate in a bristle, and chromosome base numbers of 5, 7, 8, and 9. Cenchrus has retrorsely (rarely antrorsely) scabrous, spiny bristles, fascicle axes that are terminated by a spikelet, and a chromosome base number of 17 (Wipff 2001). In both genera, the bristles are reduced branches (Goebel 1882; Sohns 1955).</discussion>
  <references>
    <reference>Brunken, J.N. 1977. A systematic study of Fennisetum sect. Fennisetum (Gramineae). Amer. J. Bot. 64:161-176.</reference>
    <reference>Brunken, J.N. 1979. Morphometric variation and the classification of Pennisetum section Brevivalvula (Gramineae) in tropical Africa. Bot. J Linn. Soc. 79:51-64</reference>
    <reference> Chase, A. 1921. The North American species of Fennisetum. Contr. U.S. Natl. Herb. 224:209-234</reference>
    <reference> Goebel, K.I. 1882 Beitrage zur Entwickelungsgeschichte einiger Inflorescenzen. Jahrb. Uiss. Bot. 14:1-39</reference>
    <reference> Hignight, K.W., E.C. Basha, and M.A. Hussey. 1991 Cytological and morphological diversity of native apomictic buffelgrass, Fennisetum ciliare (L.) Link. Bot. Gaz. 152:214-218</reference>
    <reference> Schmelzer, G.H 1997. Review of Fennisetum section Brevivalvula (Poaceae). Euphytica 97:1-20</reference>
    <reference> Sohns, E.R. 1955. Cenchrus and Fennisetum: Fascicle morphology. J. Wash. Acad. Sci. 45:135-143</reference>
    <reference> Wipff, J.K. 1995. A biosystematic study of selected facultative apomictic species of Fennisetum (Poaceae: Paniceae) and their hybrids. Ph.D. dissertation, Texas A&amp;M University, College Station, Texas, U.S.A. 183 pp.</reference>
    <reference> Wipff, J.K. 2001. Nomenclatural changes in Fennisetum (Poaceae: Paniceae). Sida:19:523-530.</reference>
  </references>
  <key>
    <discussion>NOTE: Pedicel length is the distance from the base of the primary bristles to the base of the terminal spikelets. Fascicle axis lengths and fascicle densities are measured in the middle of the panicle; spikelet measurements refer to the largest spikelets in the fascicles.</discussion>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Plants stoloniferous; panicles axillary, partially or wholly hidden in the leaf sheaths at maturity, the rachises flattened in cross section, with 1-6 fascicles; spikelets 10-22 mm long, bristles mostly shorter than the spikelet</description>
      <determination>1 Pennisetum clandestinum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Plants not stoloniferous; panicles terminal or terminal and axillary, fully exserted at maturity, the rachises terete, with 10-many fascicles; spikelets 2.5-12 mm long, the majority of the bristles as long as or longer than the spikelets.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Fascicles with only 1 bristle and 1 spikelet</description>
      <determination>18 Pennisetum petiolare</determination>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Fascicles with 6 or more bristles and 1-12 spikelets.</description>
      <next_statement_id>3.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Most or all bristles scabrous, the primary bristles sometimes sparsely and inconspicuously long-ciliate.</description>
      <next_statement_id>4.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4.</statement_id>
      <description type="morphology">Primary bristles not noticeably longer than all the other bristles in the fascicles.</description>
      <next_statement_id>5.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5.</statement_id>
      <description type="morphology">Terminal panicle erect; fascicles with a stipelike base 1.5-5.6 mm long</description>
      <determination>7 Pennisetum alopecuroides</determination>
    </key_statement>
    <key_statement>
      <statement_id>5.</statement_id>
      <description type="morphology">Terminal panicle drooping; fascicles subsessile, the bases 0.4-0.7 mm long.</description>
      <next_statement_id>6.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6.</statement_id>
      <description type="morphology">Plants green; most of the bristles only slightly longer than the spikelets; upper glumes (7)9-veined, about as long as the spikelets</description>
      <determination>4 Pennisetum nervosum</determination>
    </key_statement>
    <key_statement>
      <statement_id>6.</statement_id>
      <description type="morphology">Plants purplish; bristles at least twice as long as the spikelets; upper glumes 1-3-veined, usually about 1/2 as long as the spikelets</description>
      <determination>5 Pennisetum macrostachys</determination>
    </key_statement>
    <key_statement>
      <statement_id>4.</statement_id>
      <description type="morphology">Primary bristles noticeably longer than all the other bristles in the fascicles.</description>
      <next_statement_id>7.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7.</statement_id>
      <description type="morphology">Panicles dense; rachises with 21-40 fascicles per cm.</description>
      <next_statement_id>8.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8.</statement_id>
      <description type="morphology">Rachises pubescent; bristles yellow or purple; leaf blades (4)12-40 mm wide; paleas of lower florets present</description>
      <determination>2 Pennisetum purpureum</determination>
    </key_statement>
    <key_statement>
      <statement_id>8.</statement_id>
      <description type="morphology">Rachises scabrous; bristles white to stramineous; leaf blades 4-12 mm wide; paleas of lower florets absent</description>
      <determination>6 Pennisetum macrourum</determination>
    </key_statement>
    <key_statement>
      <statement_id>7.</statement_id>
      <description type="morphology">Panicles less dense; rachises with 5-16 fascicles per cm.</description>
      <next_statement_id>9.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9.</statement_id>
      <description type="morphology">Panicles drooping, terminal and axillary; leaf blades 19-45 mm wide</description>
      <determination>8 Pennisetum latifolium</determination>
    </key_statement>
    <key_statement>
      <statement_id>9.</statement_id>
      <description type="morphology">Panicles erect, all terminal; leaf blades 2-12 mm wide.</description>
      <next_statement_id>10.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10.</statement_id>
      <description type="morphology">Plants not rhizomatous; lower part of rachises pubescent</description>
      <determination>7 Pennisetum alopecuroides</determination>
    </key_statement>
    <key_statement>
      <statement_id>10.</statement_id>
      <description type="morphology">Plants rhizomatous; lower part of rachises scabrous</description>
      <determination>14 Pennisetum flaccidum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Bristles, at least the primary bristles, conspicuously long-ciliate.</description>
      <next_statement_id>11.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11.</statement_id>
      <description type="morphology">Spikelets 9-12 mm long</description>
      <determination>11 Pennisetum villosum</determination>
    </key_statement>
    <key_statement>
      <statement_id>11.</statement_id>
      <description type="morphology">Spikelets 2.5-7 mm long.</description>
      <next_statement_id>12.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12.</statement_id>
      <description type="morphology">Fascicles not disarticulating from the rachises; panicles 4-200 cm long; upper lemmas with pubescent margins; caryopses protruding from the florets at maturity</description>
      <determination>3 Pennisetum glaucum</determination>
    </key_statement>
    <key_statement>
      <statement_id>12.</statement_id>
      <description type="morphology">Fascicles disarticulating from the rachises at maturity; panicles 2-37.5 cm long; upper lemmas with glabrous margins; caryopses concealed by the lemmas and paleas at maturity.</description>
      <next_statement_id>13.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13.</statement_id>
      <description type="morphology">Upper florets readily disarticulating at maturity; upper lemmas smooth and shiny, conspicuously different in texture from the lower lemmas.</description>
      <next_statement_id>14.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14.</statement_id>
      <description type="morphology">Fascicles with 6-14 long-ciliate inner bristles and 13-30 scabrous outer bristles; fascicle axes 0.2-0.5 mm long; spikelets sessile</description>
      <determination>9 Pennisetum polystachion</determination>
    </key_statement>
    <key_statement>
      <statement_id>14.</statement_id>
      <description type="morphology">Fascicles with 40-90 long-ciliate inner bristles and 10-20 scabrous outer bristles; fascicle axes 1.5-2.5 mm long; spikelets pedicellate, the pedicels 1-3.5 mm long</description>
      <determination>10 Pennisetum pedicellatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>13.</statement_id>
      <description type="morphology">Upper florets not disarticulating at maturity; lower and upper lemmas similar in texture.</description>
      <next_statement_id>15.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>15.</statement_id>
      <description type="morphology">Lower portion of the rachises glabrous, sometimes scabrous.</description>
      <next_statement_id>16.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>16.</statement_id>
      <description type="morphology">Inner bristles neither grooved nor fused, even at the base; spikelets 5.2-6.7 mm long, pedicellate, the pedicels 0.1-0.5 mm long</description>
      <determination>14 Pennisetum flaccidum</determination>
    </key_statement>
    <key_statement>
      <statement_id>16.</statement_id>
      <description type="morphology">Inner bristles grooved and fused, at least at the base; spikelets 2.5-5.6 mm long, sessile.</description>
      <next_statement_id>17.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>17.</statement_id>
      <description type="morphology">Inner bristles fused for up to 1/4 their length; many outer bristles exceeding the spikelets; terminal bristles 10.5-23 mm, noticeably longer than the other bristles in the fascicles</description>
      <determination>12 Pennisetum ciliare</determination>
    </key_statement>
    <key_statement>
      <statement_id>17.</statement_id>
      <description type="morphology">Inner bristles fused for 1/3 – 1/2 their length; outer bristles not exceeding the spikelets; terminal bristles 2.9-6.5 mm, usually not noticeably longer than other bristles in the fascicles</description>
      <determination>13 Pennisetum setigerum</determination>
    </key_statement>
    <key_statement>
      <statement_id>15.</statement_id>
      <description type="morphology">Lower portion of the rachises pubescent.</description>
      <next_statement_id>18.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>18.</statement_id>
      <description type="morphology">Plants 200-800 cm tall; midculm leaves (4)12-40 mm wide; panicles golden-yellow or dark purple; rachises with 30-40 fascicles per cm</description>
      <determination>2 Pennisetum purpureum</determination>
    </key_statement>
    <key_statement>
      <statement_id>18.</statement_id>
      <description type="morphology">Plants 50-200 cm tall; midculm leaves 2-11 mm wide; panicles white, burgundy, light purple, or pink; rachises with 5-17 fascicles per cm.</description>
      <next_statement_id>19.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>19.</statement_id>
      <description type="morphology">Midculm leaves 2-3.5 mm wide, convolute or folded, green, the midvein noticeably thickened; lower florets of the spikelets usually sterile, sometimes staminate</description>
      <determination>15 Pennisetum setaceum</determination>
    </key_statement>
    <key_statement>
      <statement_id>19.</statement_id>
      <description type="morphology">Midculm leaves 3-11 mm wide, flat, green or burgundy, the midvein not noticeably thickened; lower florets of the spikelets staminate.</description>
      <next_statement_id>20.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>20.</statement_id>
      <description type="morphology">Plants shortly rhizomatous; nodes pubescent; panicles erect to slightly arching, white or purple-tinged; leaves green; ligules 1-1.7 mm long; fascicles with 0-24 terete, scabrous outer bristles</description>
      <determination>16 Pennisetum orientale</determination>
    </key_statement>
    <key_statement>
      <statement_id>20.</statement_id>
      <description type="morphology">Plants not rhizomatous; nodes glabrous; panicles conspicuously drooping, burgundy (rarely whitish-green); leaves burgundy (rarely green); ligules 0.5-0.8 mm long; fascicles with 43-68 terete, scabrous outer bristles</description>
      <determination>17 Pennisetum advena</determination>
    </key_statement>
  </key>
  <description type="distribution">Wash.;W.Va.;Calif.;Fla.;Pacific Islands (Hawaii);Tex.;Del.;D.C;Wis.;Ariz.;N.Mex.;Conn.;Mass.;Maine;N.H.;R.I.;Vt.;La.;Okla.;Wyo.;Puerto Rico;N.J.;N.C.;N.Dak.;Tenn.;S.C.;Pa.;S.Dak.;N.Y.;Nev.;Va.;Ala.;Ga.;Iowa;Idaho;Kans.;Mich.;Minn.;Mont.;Nebr.;Oreg.;Colo.;Virgin Islands;Ark.;Ill.;Ind.;Md.;Ohio;Utah;Mo.;Miss.;Ky.</description>
</bio:treatment>
