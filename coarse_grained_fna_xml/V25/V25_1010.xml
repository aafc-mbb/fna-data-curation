<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <other_info_on_meta type="treatment_page">330</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Andy Sudkamp</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" date="unknown" authority="Barnhart">Poaceae</taxon_name>
    <taxon_name rank="subfamily" date="unknown" authority="Caro">ARISTIDOIDEAE</taxon_name>
    <taxon_name rank="tribe" date="unknown" authority="C.E. Hubb.">ARISTIDEAE</taxon_name>
    <taxon_name rank="genus" date="unknown" authority="L.">ARISTIDA</taxon_name>
    <taxon_name rank="species" date="unknown" authority="Nutt.">purpurea</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily aristidoideae;tribe aristideae;genus aristida;species purpurea</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aristida</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">roemeriana</taxon_name>
    <taxon_hierarchy>genus aristida;species roemeriana</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name authority="unknown" date="unknown" rank="genus">Aristida</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="species">purpurea</taxon_name>
    <taxon_name authority="unknown" date="unknown" rank="variety">laxiflora</taxon_name>
    <taxon_hierarchy>genus aristida;species purpurea;variety laxiflora</taxon_hierarchy>
  </taxon_identification>
  <number>19</number>
  <description type="morphology">Plants perennial; densely cespitose, without rhizomes. Culms 10-100 cm, erect to ascending, usually unbranched. Leaves mostly basal or mostly cauline; sheaths shorter or longer than the internodes, glabrous, not disintegrating into threadlike fibers at maturity; collars glabrous, or sparsely pilose at the sides with straight hairs; ligules less than 0.5 mm; blades 4-25 cm long, 1-1.5 mm wide, tightly involute to flat, usually glabrous, sometimes scabridulous abaxially, gray-green, lax to curled at maturity. Inflorescences usually sparingly branched panicles, occasionally racemes, 3-30 cm long, 2-12 cm wide, with 2 or more spikelets per node; nodes glabrous or with straight, about 0.5 mm hairs; primary branches 3-6 cm, appressed to divaricate, varying sometimes within a panicle, stiff to flexible, bases appressed or abruptly spreading, usually without axillary pulvini. Spikelets divergent or appressed, with or without axillary pulvini. Glumes usually unequal, lower glumes shorter than the upper glumes, sometimes subequal, light to dark brown or purplish, glabrous, smooth or scabridulous, 1(2)-veined, acuminate, unawned or awned, awns to 1 mm; lower glumes 4-12 mm; upper glumes 7-25 mm; calluses 0.5-1.8 mm; lemmas 6-16 mm, glabrous, scabridulous, or tuberculate, whitish to purplish, apices 0.1-0.8 mm wide, not beaked or the beak less than 3 mm, junction with the awns not conspicuous; awns (8)13-140 mm, ascending to divaricate, not disarticulating at maturity; central awns thicker than the lateral awns; lateral awns (8)13-140 mm, usually subequal to the central awns, occasionally less than 1/3 as long as the central awns; anthers 3, 0.7-2 mm. Caryopses 6-14 mm, tan to chestnut. 2n = 22, 44, 66, 88.</description>
  <discussion>Aristida purpurea is composed of several intergrading varieties.</discussion>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Lower or all primary panicle branches stiff, divergent to divaricate from the base, with axillary pulvini; awns 13-30 mm.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Lower glumes 1/2 - 2/3 as long as the upper glumes</description>
      <determination>Aristida purpurea var. perplexa</determination>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Lower glumes from 3/4 as long as to equaling the upper glumes</description>
      <determination>Aristida purpurea var. parishii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Primary panicle branches appressed or ascending at the base, sometimes drooping distally, without axillary pulvini; awns 8-140 mm.</description>
      <next_statement_id>3.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Awns 35-140 mm long.</description>
      <next_statement_id>4.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4.</statement_id>
      <description type="morphology">Lemmas apices 0.1-0.3 mm wide; awns 0.1-0.2(0.3) mm wide at the base, 35-60 mm long; upper glumes usually shorter than 16 mm long</description>
      <determination>Aristida purpurea var. purpurea</determination>
    </key_statement>
    <key_statement>
      <statement_id>4.</statement_id>
      <description type="morphology">Lemma apices 0.3-0.8 mm wide; awns 0.2-0.5 mm wide at the base, 40-140 cm long; upper glumes 14-25 mm long</description>
      <determination>Aristida purpurea var. longiseta</determination>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Awns 8-35 mm long.</description>
      <next_statement_id>5.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5.</statement_id>
      <description type="morphology">Lemma apices 0.1-0.3 mm wide distally; awns 0.1-0.3 mm wide at the base.</description>
      <next_statement_id>6.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6.</statement_id>
      <description type="morphology">All or most of the panicle branches straight (lower branches sometimes lax); pedicels straight, appressed to ascending</description>
      <determination>Aristida purpurea var. nealleyi</determination>
    </key_statement>
    <key_statement>
      <statement_id>6.</statement_id>
      <description type="morphology">All or most of the panicle branches and  pedicels  drooping  to  sinuous distally</description>
      <determination>Aristida purpurea var. purpurea</determination>
    </key_statement>
    <key_statement>
      <statement_id>5.</statement_id>
      <description type="morphology">Lemma apices 0.2-0.3 mm wide; awns stout, 0.2-0.3 mm wide at the base.</description>
      <next_statement_id>7.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7.</statement_id>
      <description type="morphology">Mature panicle branches and pedicels flexible, lax or drooping distally</description>
      <determination>Aristida purpurea var. purpurea</determination>
    </key_statement>
    <key_statement>
      <statement_id>7.</statement_id>
      <description type="morphology">Mature panicle branches and pedicels usually stiff, straight.</description>
      <next_statement_id>8.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8.</statement_id>
      <description type="morphology">Panicles  usually  3-15  cm  long; blades 4-10 cm long</description>
      <determination>Aristida purpurea var. fendleriana</determination>
    </key_statement>
    <key_statement>
      <statement_id>8.</statement_id>
      <description type="morphology">Panicles usually 15-30 cm long; blades 10-25 cm long.</description>
      <next_statement_id>9.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9.</statement_id>
      <description type="morphology">Glumes and lemmas reddish or dark-colored at anthesis or earlier (fading to stramineous), usually in marked contrast with the current foliage; panicles dense, the lower nodes with 8-18 spikelets; flowering March to May, after winter rains</description>
      <determination>Aristida purpurea var. parishii</determination>
    </key_statement>
    <key_statement>
      <statement_id>9.</statement_id>
      <description type="morphology">Glumes and lemmas tan to brown (also fading to stramineous), giving the panicle a brownish appearance; old growth gray-green, not in marked contrast with the current foliage; panicles less dense, the lower nodes with 2-10 spikelets</description>
      <determination>Aristida purpurea var. wrightii</determination>
    </key_statement>
  </key>
  <description type="distribution">Wash.;Wyo.;N.Mex.;Tex.;La.;N.C.;S.C.;Colo.;Kans.;N.Dak.;Nebr.;Okla.;S.Dak.;Nev.;Calif.;Utah;Ark.;Vt.;Ill.;Iowa;Ariz.;Idaho;Mont.;Minn.;Oreg.;Alta.;B.C.;Man.;Sask.</description>
</bio:treatment>
