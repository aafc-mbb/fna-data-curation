<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <author>Stephen J. Darbyshire; Henry E. Connor;</author>
      <date>NA</date>
    </source>
    <other_info_on_meta type="treatment_page">309</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Hana Pazdírková</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" date="unknown" authority="Barnhart">Poaceae</taxon_name>
    <taxon_name rank="subfamily" date="unknown" authority="N.P. Barker &amp; H.P. Under">DANTHONIOIDEAE</taxon_name>
    <taxon_name rank="tribe" date="unknown" authority="Zotov">DANTHONIEAE</taxon_name>
    <taxon_name rank="genus" date="unknown" authority="Steud.">RYTIDOSPERMA</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily danthonioideae;tribe danthonieae;genus rytidosperma</taxon_hierarchy>
  </taxon_identification>
  <number>20.05</number>
  <description type="morphology">Plants perennial; cespitose to somewhat spreading, sometimes shortly rhizomatous. Culms (1.5)30-90(140) cm. Sheaths open, glabrous or hairy, apices with tufts of hair, these sometimes extending across the collar; ligules of hairs; blades persistent or disarticulating. Inflorescences terminal, racemes or panicles. Spikelets with 3-10 florets; florets bisexual, terminal florets reduced; disarticulation above the glumes and between the florets. Glumes (2)8-20 mm, subequal or equal, usually exceeding the florets, stiffly membranous; calluses with lateral tufts of stiff hairs; lemmas ovate to lanceolate, with 2 complete or incomplete transverse rows of tufts of hairs, sometimes reduced to marginal tufts, 5-9-veined, apices bilobed, lobes usually at least as long as the body, acute, acuminate, or aristate, awned from between the lobes, awns longer than the lobes, twisted, usually geniculate; lodicules 2, fleshy, with hairs or glabrous. Caryopses 1.2-3 mm, obovate to elliptic. Cleistogenes absent, x = 12.</description>
  <discussion>Rytidosperma, as interpreted here and by Edgar and Connor (2000), is a genus of about 45 species that are native to south and southeastern Asia, Australia, New Zealand, and South America. Linder and Verboom (1996) advocated a narrower interpretation of the genus than Edgar and Connor, but acknowledged that "there is an almost equally strong case for recognizing a single, large genus, Rytidosperma" (p. 607). According to their interpretation, all three species treated here would be placed in Austrodanthonia H.P. Linder (Linder 1997).</discussion>
  <discussion>Several species of Rytidosperma have been cultivated in research plots or forage trials in North America. The three species treated here have been tried in several states but have escaped cultivation and persisted only in California and Oregon (Weintraub 1953). They have been included in commercial seed mixtures for forage planting in Australia and New Zealand. Other species that have been grown experimentally in both the United States and Canada include R. caespitosum (Gaudich.) Connor &amp; Edgar, R. setaceum (R. Br.) Connor &amp; Edgar, and R. tenuius (Steud.) A. Hansen &amp; P. Sunding. They are not known to have escaped or persisted in North America.</discussion>
  <discussion>H.E. Connor identified two additional species of Rytidosperma among specimens that have been found as escapes in Alameda and San Mateo counties, California. They are Rytidosperma caespitosa (Gaudich.) Connor &amp; Edgar, and R. richardsonii (Cashmore) Connor &amp; Edgar. Both are native to Australia. Rytidosperma caespitosa differs from the three species included in volume 25 in having two rows of tufts of hair in which the upper row of hairs greatly exceeds the lemma body. Like R. biannulare, it has intravaginal branching. Rytidosperma richardsonii has lemma lobes that are shorter than the lemma body, and obovate paleas that are 2-2.5 mm wide.</discussion>
  <references>
    <reference>Blumler, M. 2001. Notes and comments. Fremontia 29:36</reference>
    <reference> Connor, H.E. and E. Edgar. 1979. Rytidosperma Steudel (Nothodantbonia Zotov) in New Zealand. New Zealand J. Bot. 17:311-337</reference>
    <reference> Edgar, E. and H.E. Connor. 2000. Flora of New Zealand, vol. 5. Manaaki Whenua Press, Lincoln, New Zealand. 650 pp.</reference>
    <reference> Linder, H.P. 1997. Nomenclatural corrections in the Rytidosperma complex (Danthonieae, Poaceae). Telopea 7:269-274</reference>
    <reference> Linder, H.P. and G.A. Verboom. 1996. Generic limits in the Rytidosperma (Danthonieae, Poaceae) complex. Telopea 6:597-627</reference>
    <reference> Murphy, A.H. and R.M. Love. 1950. Hairy oatgrass, Danthonia pilosa R. Br., as a weedy range grass. Bull. Calif. Dep. Agric. 39:118-124</reference>
    <reference> Myers, W.M. 1947. Cytology and genetics of forage grasses (concluded). Bot. Re.-. 7:369-419</reference>
    <reference> Vickery, J.W. 1956. A revision of the Australian species of Danthonia DC. Contr. New South Wales Natl. Herb. 2:249-325</reference>
    <reference> Weintraub, EC. 1953. Grasses Introduced into the United States. Agricultural Handbook No. 58. Forest Service, U.S. Department of Agriculture, Washington, D.C., U.S.A. 79 pp.</reference>
    <reference> Zotov, V.D. 1963. Synopsis of the grass subfamily Arundinoideae in New Zealand. New Zealand J. Bot. 1:78-136.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Upper row of lemma hairs in a more or less continuous row of tufts, the hairs much exceeding the base of the awn; shoots intravaginal, without scaly cataphylls</description>
      <determination>2 Rytidosperma biannulare</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Upper row of lemma hairs in isolated tufts or only at the margins, the hairs not or only just exceeding the base of the awn; some or most shoots extravaginal and with scaly cataphylls.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Callus hairs usually overlapping the lower row of lemma hairs; upper row of lemma hairs often reduced to marginal tufts, sometimes scanty medial tufts also present</description>
      <determination>1 Rytidosperma penicillatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Callus hairs short, rarely reaching the lower row of lemma hairs; upper row of lemma hairs usually with scanty medial tufts</description>
      <determination>3 Rytidosperma racemosum</determination>
    </key_statement>
  </key>
  <description type="distribution">Calif.;Pacific Islands (Hawaii);Oreg.</description>
</bio:treatment>
