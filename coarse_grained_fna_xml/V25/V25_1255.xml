<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <author>unknown</author>
      <date>NA</date>
    </source>
    <other_info_on_meta type="treatment_page">469</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
    <other_info_on_meta type="illustrator">Linda A. Vorobik and Cindy Roché</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" date="unknown" authority="Barnhart">Poaceae</taxon_name>
    <taxon_name rank="subfamily" date="unknown" authority="Link">PANICOIDEAE</taxon_name>
    <taxon_name rank="tribe" date="unknown" authority="R. Br.">PANICEAE</taxon_name>
    <taxon_name rank="genus" date="unknown" authority="L.">PANICUM</taxon_name>
    <taxon_name rank="subgenus" date="unknown" authority="unknown">Panicum</taxon_name>
    <taxon_name rank="section" date="unknown" authority="(Hitchc.) Honda">Dichotomiflora</taxon_name>
    <taxon_name rank="species" date="unknown" authority="Michx.">dichotomiflorum</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily panicoideae;tribe paniceae;genus panicum;subgenus panicum;section dichotomiflora;species dichotomiflorum</taxon_hierarchy>
  </taxon_identification>
  <number>15</number>
  <other_name type="common_name">Fall panicum</other_name>
  <other_name type="common_name">Panic d'automne</other_name>
  <description type="morphology">Plants annual or short-lived perennials in the Flora region, perennial in the tropics; usually terrestrial, sometimes aquatic but not floating. Culms 5-200 cm tall, 0.4-3 mm thick, decumbent to erect, commonly geniculate to ascending, rooting at the lower nodes when in water, simple to divergently branched from the lower and middle nodes, usually succulent, slightly compressed, glabrous; nodes usually swollen, sometimes constricted on robust plants, glabrous; internodes glabrous, shiny, pale green to purplish. Sheaths compressed, inflated, sparsely pubescent near the base, elsewhere mostly glabrous, sparsely pilose, or hispid, hairs sometimes papillose-based, margins or throat ciliate, with papillose-based hairs; ligules 0.5-2 mm; blades 10-65 cm long, 3-25 mm wide, glabrous or sparsely pilose, often scabrous near the margins, midribs stout, whitish. Panicles 4-40 cm, diffuse, lax, with a few spikelets; branches to 15 cm, alternate or opposite, occasionally verticillate, ascending to spreading, stiff, scabrous; pedicels 1-6 mm, sharply 3-angled, scabrous, expanded to cuplike apices, appressed mostly to the abaxial side of the branches. Spikelets 1.8-3.8 mm long, 0.7-1.2 mm wide, ellipsoid to narrowly ovoid, light green to red-purple, glabrous, acute to acuminate. Lower glumes 0.6-1.2 mm, 1/4 - 1/3 as long as the spikelets, 0-3-veined, obtuse to acute; upper glumes and lower lemmas similar, exceeding the upper florets by 0.3-0.6 mm, 7-9-veined; lower paleas vestigial to almost as long as the lower lemmas; lower florets sterile; upper florets 1.4-2.5 mm long, 0.7-1.1 mm wide, narrowly ellipsoid, smooth, shiny, stramineous to nigrescent, with pale veins. 2n = 36, 54.</description>
  <discussion>Panicum dichotomiflorum grows in open, often wet, disturbed areas such as cultivated and fallow fields, roadsides, ditches, open stream banks, receding shores, clearings in flood plain woods, and sometimes in shallow water. It is probably native throughout the eastern United States and adjacent Canada, but introduced elsewhere, including in the western United States. Its size and habit may be partly under genetic control, but these features also seem to be strongly affected by moisture levels, soil richness, competition, and the time of germination.</discussion>
  <key>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Spikelets 1.8-2.2 mm long, widest at the middle, acute; upper glumes and lower lemmas submembranaceous; pedicels often over 3 mm long</description>
      <determination>Panicum dichotomiflorum subsp. puritanorum</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Spikelets 2.2-3.8 mm long, widest below the middle, acuminate; upper glumes and lower lemmas subcoriaceous; most pedicels less than 3 mm long.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Sheaths glabrous or sparsely pilose, hairs not papillose-based</description>
      <determination>Panicum dichotomiflorum subsp. dichotomiflorum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Sheaths hispid, hairs papillose-based</description>
      <determination>Panicum dichotomiflorum subsp. bartowense</determination>
    </key_statement>
  </key>
  <description type="distribution">Conn.;N.J.;N.Y.;B.C.;N.B.;N.S.;Ont.;Que.;Wash.;Va.;Mich.;Del.;Wis.;W.Va.;Pacific Islands (Hawaii);Mass.;Maine;N.H.;R.I.;Vt.;Fla.;N.Mex.;Tex.;La.;D.C.;Ind.;Tenn.;N.C.;S.C.;Pa.;Kans.;Nebr.;Okla.;S.Dak.;Nev.;Puerto Rico;Colo.;Calif.;Ala.;Ark.;Ariz.;Ga.;Minn.;Mo.;Mont.;Ohio;Oreg.;Utah;Ill.;Iowa;Idaho;Md.;Miss.;Ky.</description>
</bio:treatment>
