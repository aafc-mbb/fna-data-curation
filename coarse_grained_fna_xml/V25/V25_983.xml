<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <author>Kelly W. Allred;</author>
      <date>NA</date>
    </source>
    <other_info_on_meta type="treatment_page">315</other_info_on_meta>
    <other_info_on_meta type="volume">25</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" date="unknown" authority="Barnhart">Poaceae</taxon_name>
    <taxon_name rank="subfamily" date="unknown" authority="Caro">ARISTIDOIDEAE</taxon_name>
    <taxon_name rank="tribe" date="unknown" authority="C.E. Hubb.">ARISTIDEAE</taxon_name>
    <taxon_name rank="genus" date="unknown" authority="L.">ARISTIDA</taxon_name>
    <taxon_hierarchy>family poaceae;subfamily aristidoideae;tribe aristideae;genus aristida</taxon_hierarchy>
  </taxon_identification>
  <number>21.01</number>
  <description type="morphology">Plants usually perennial; herbaceous, usually cespitose, occasionally rhizomatous. Culms 10-150 cm, not woody, sometimes branched above the base; internodes usually pith-filled, sometimes hollow. Leaves sometimes predominantly basal, sometimes predominantly cauline; sheaths open; auricles lacking; ligules of hairs or very shortly membranous and long-ciliate, the 2 types generally indistinguishable. Inflorescences terminal, usually panicles, sometimes racemes, occasionally spikes; primary branches without axillary pulvini and usually appressed to ascending, or with axillary pulvini and ascending to strongly divergent or divaricate. Spikelets with 1 floret; rachillas not prolonged beyond the florets; disarticulation above the glumes. Glumes often longer than the florets, thin, usually 1-3-veined, acute to acuminate; florets terete or weakly laterally compressed; calluses well-developed, hirsute; lemmas fusiform, 3-veined, convolute, usually glabrous or scabridulous, usually enclosing the palea at maturity, usually with 3 terminal awns, lateral awns reduced or obsolete in some species, lemma apices sometimes narrowed to a straight or twisted beak below the awns; awns ascending to spreading, usually straight, bases sometimes twisted together into a column or the bases of the individual awns coiled, twisted, or otherwise contorted, occasionally disarticulating at maturity; paleas shorter than the lemmas, 2-veined, occasionally absent; anthers 1 or 3. Caryopses fusiform; hila linear, x = 11, 12.</description>
  <discussion>Aristida is a tropical to warm-temperate genus of 250-300 species. It grows throughout the world in dry grasslands and savannahs, sandy woodlands, arid deserts, and open, weedy habitats and on rocky slopes and mesas. All 29 species in this treatment are native to the Flora region.</discussion>
  <discussion>The divergent awns aid in wind and animal transportation of the florets and, by holding the florets and the caryopses they contain at an angle to the ground, in establishment. The presence of Aristida frequently indicates soil disturbance or abuse. Although generally poor forage grasses and, because of the calluses, potentially harmful to grazing animals, some species of Aristida are an important source of spring forage on western rangelands. Quail and small mammals eat small amounts of the seed.</discussion>
  <references>
    <reference>Allred, K.W. 1984. Morphologic variation and classification of the North American Aristida purpurea complex (Graraineae). Brittonia 36:382-395</reference>
    <reference> Allred, K.W. 1984, 1985, 1986. Studies in the genus Aristida (Gramineae) of the southeastern United States. Rhodora 86:73-77, 87:137-145, 145-155, 88:367-387</reference>
    <reference> Henrard, J.T. 1926, 1927, 1928, 1933. A critical revision of the genus Aristida. Meded. Rijks-Herb. 54:1-701; 55G703-747</reference>
    <reference> Henrard, J.T. 1929, 1933. A monograph of the genus Aristida. Meded. Rijks-Herb. 58:1-325</reference>
    <reference> Kesler, T.R. 2000. A taxonomic reevaluation of Aristida striata (Poaceae) using anatomy and morphology. Master's thesis. Florida State University, Tallahassee, Florida, U.S.A. 33 pp.</reference>
    <reference> Peet, R.K. 1993. A taxonomic study of Aristida striata and A. beyricbiana. Rhodora 95:25-37</reference>
    <reference> Reeder, J.R. and R.S. Felger. 1989. The Aristida californica-glabrata complex (Gramineae). Madrono 36:187-197</reference>
    <reference> Trent, J.S. and K.W. Allred. 1990. A taxonomic comparison of Aristida ternipes and Aristida hamulosa (Gramineae). Sida 14:251-261</reference>
    <reference> Vaughn, J.M. 1981. Systematics of Aristida dichotoma, basiramea, and curtissii (Poaceae). Master's thesis. University of Oklahoma, Norman, Oklahoma, U.S.A. 38 pp.</reference>
    <reference> Walters, T.E., D.S. Decker-Walters, and D.R. Gordon. 1994. Restoration considerations for wiregrass (Aristida striata): Allozymic diversity of populations. Conservation Biol. 8:581-585.</reference>
  </references>
  <key>
    <discussion>NOTE: Lemma lengths are measured from the base of the callus to the divergence of the awns.</discussion>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Lower glumes 3-7-veined.</description>
      <next_statement_id>2.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Awns nearly equal, the lateral awns 8-66 mm long and at least 3/4 as long as the central awns.</description>
      <determination>13 Aristida oligantha</determination>
    </key_statement>
    <key_statement>
      <statement_id>2.</statement_id>
      <description type="morphology">Awns markedly unequal, the lateral awns 1-4 mm long, no more than 1/2 as long as the central awns, sometimes absent.</description>
      <next_statement_id>3.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Plants annual; inflorescences 5-12 cm long, 2-4 cm wide</description>
      <determination>12 Aristida ramosissima</determination>
    </key_statement>
    <key_statement>
      <statement_id>3.</statement_id>
      <description type="morphology">Plants perennial; inflorescences 10-30 cm long, 4-26 cm wide</description>
      <determination>7 Aristida schiedeana</determination>
    </key_statement>
    <key_statement>
      <statement_id>1.</statement_id>
      <description type="morphology">Lower glumes 1-2(3)-veined.</description>
      <next_statement_id>4.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4.</statement_id>
      <description type="morphology">Central awns spirally coiled at the base.</description>
      <next_statement_id>5.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5.</statement_id>
      <description type="morphology">Lateral awns 1-4 mm long, erect</description>
      <determination>15 Aristida dichotoma</determination>
    </key_statement>
    <key_statement>
      <statement_id>5.</statement_id>
      <description type="morphology">Lateral awns 5-13 mm long, spreading</description>
      <determination>14 Aristida basiramea</determination>
    </key_statement>
    <key_statement>
      <statement_id>4.</statement_id>
      <description type="morphology">Central awns straight to curved, sometimes loosely contorted but not spirally coiled, at the base.</description>
      <next_statement_id>6.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6.</statement_id>
      <description type="morphology">Lateral awns markedly reduced, usually 1/3 or less as long as the central awns.</description>
      <next_statement_id>7.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7.</statement_id>
      <description type="morphology">Panicles 1-6 cm wide, the branches erect-appressed to strongly ascending, without axillary pulvini or the pulvini only weakly developed.</description>
      <next_statement_id>8.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8.</statement_id>
      <description type="morphology">Plants annual; culms often highly branched above the base.</description>
      <next_statement_id>9.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9.</statement_id>
      <description type="morphology">Awns flattened at the base</description>
      <determination>17 Aristida adscensionis</determination>
    </key_statement>
    <key_statement>
      <statement_id>9.</statement_id>
      <description type="morphology">Awns terete at the base.</description>
      <next_statement_id>10.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10.</statement_id>
      <description type="morphology">Lemmas 2.5-10 mm long; central awns curving up to 100° at the base</description>
      <determination>16 Aristida longespica</determination>
    </key_statement>
    <key_statement>
      <statement_id>10.</statement_id>
      <description type="morphology">Lemmas 8-22 mm long; central awns with a semicircular bend at the base</description>
      <determination>12 Aristida ramosissima</determination>
    </key_statement>
    <key_statement>
      <statement_id>8.</statement_id>
      <description type="morphology">Plants perennial; culms rarely branched above the base.</description>
      <next_statement_id>11.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11.</statement_id>
      <description type="morphology">Collars  densely  pilose,  the hairs 1-3 mm, often densely tangled and deflexed; blades usually tightly involute, about 0.5 mm in diameter</description>
      <determination>11 Aristida gypsophila</determination>
    </key_statement>
    <key_statement>
      <statement_id>11.</statement_id>
      <description type="morphology">Collars mostly glabrous or with straight hairs, often with long hairs at the sides; blades usually flat to loosely involute, sometimes tightly involute.</description>
      <next_statement_id>12.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12.</statement_id>
      <description type="morphology">Lateral awns absent; panicle branches spikelet-bearing to the base; plants of the Florida keys</description>
      <determination>5 Aristida floridana</determination>
    </key_statement>
    <key_statement>
      <statement_id>12.</statement_id>
      <description type="morphology">Lateral awns usually present, varying from much shorter than to equaling the central awns; panicle branches sometimes naked near the base; plants rarely found east of the Mississippi and not known at all from Florida.</description>
      <next_statement_id>13.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13.</statement_id>
      <description type="morphology">Primary panicle branches 3-6 cm long; lateral awns (1)8-140 mm long</description>
      <determination>19 Aristida purpurea</determination>
    </key_statement>
    <key_statement>
      <statement_id>13.</statement_id>
      <description type="morphology">Primary panicle branches 6-16 cm long; lateral awns absent or to 1(3) mm long</description>
      <determination>7 Aristida schiedeana</determination>
    </key_statement>
    <key_statement>
      <statement_id>7.</statement_id>
      <description type="morphology">Panicles 6-45 cm wide, at least the lower branches spreading and having well-developed axillary pulvini.</description>
      <next_statement_id>14.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14.</statement_id>
      <description type="morphology">Lateral awns absent or no more than 3 mm long.</description>
      <next_statement_id>15.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>15.</statement_id>
      <description type="morphology">Central awns often deflexed at a sharp angle when mature; lemma apices often twisted at maturity.</description>
      <next_statement_id>16.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>16.</statement_id>
      <description type="morphology">Blades usually flat, sometimes folded, 1-2 mm wide; plants of juniper, oak, or pine woodlands</description>
      <determination>7 Aristida schiedeana</determination>
    </key_statement>
    <key_statement>
      <statement_id>16.</statement_id>
      <description type="morphology">Blades usually tightly involute, about 0.5 mm in diameter; plants of thorn-scrub deserts</description>
      <determination>11 Aristida gypsophila</determination>
    </key_statement>
    <key_statement>
      <statement_id>15.</statement_id>
      <description type="morphology">Central awns usually straight or arcuate; lemma apices not twisted.</description>
      <next_statement_id>17.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>17.</statement_id>
      <description type="morphology">Panicle branches spikelet-bearing from the base; lower glumes longer than the upper glumes</description>
      <determination>5 Aristida floridana</determination>
    </key_statement>
    <key_statement>
      <statement_id>17.</statement_id>
      <description type="morphology">Panicle branches usually naked at the base; lower glumes about equal to the upper glumes</description>
      <determination>6 Aristida ternipes</determination>
    </key_statement>
    <key_statement>
      <statement_id>14.</statement_id>
      <description type="morphology">Lateral awns 3-23 mm long.</description>
      <next_statement_id>18.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>18.</statement_id>
      <description type="morphology">Anthers 0.8-1 mm long.</description>
      <next_statement_id>19.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>19.</statement_id>
      <description type="morphology">Spikelets usually divergent and the pedicels with axillary pulvini; secondary branches usually absent; primary branches 2-6 cm long; lemma apices with 0-2 twists when mature</description>
      <determination>9 Aristida havardii</determination>
    </key_statement>
    <key_statement>
      <statement_id>19.</statement_id>
      <description type="morphology">Spikelets usually appressed and the pedicels without axillary pulvini; secondary branches usually well-developed; primary branches 5-13 cm long; lemma apices with 4 or more twists when mature</description>
      <determination>8 Aristida divaricata</determination>
    </key_statement>
    <key_statement>
      <statement_id>18.</statement_id>
      <description type="morphology">Anthers 1.2-3 mm long.</description>
      <next_statement_id>20.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>20.</statement_id>
      <description type="morphology">Collars glabrous or strigillose; blades with scattered hairs 1.5-3 mm long above the ligule on the adaxial surface; lower glumes about equal to or slightly shorter than the upper glumes</description>
      <determination>6 Aristida ternipes</determination>
    </key_statement>
    <key_statement>
      <statement_id>20.</statement_id>
      <description type="morphology">Collars pubescent, with hairs 0.2-0.8 mm long; blades glabrous, sometimes scabridulous, above the ligule on the adaxial surface; lower glumes slightly longer than the upper glumes</description>
      <determination>4 Aristida patula</determination>
    </key_statement>
    <key_statement>
      <statement_id>6.</statement_id>
      <description type="morphology">Lateral awns well-developed, usually at least 1/2 as long as the central awns.</description>
      <next_statement_id>21.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>21.</statement_id>
      <description type="morphology">Blades tightly involute, the adaxial surfaces densely scabrous or densely short-pubescent</description>
      <determination>21 Aristida stricta</determination>
    </key_statement>
    <key_statement>
      <statement_id>21.</statement_id>
      <description type="morphology">Blades flat or folded and lax, or, if involute, the adaxial surfaces neither densely scabrous nor densely short pubescent.</description>
      <next_statement_id>22.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>22.</statement_id>
      <description type="morphology">Rachis nodes and leaf sheaths usually lanose or floccose, sheaths occasionally glabrous</description>
      <determination>25 Aristida lanosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>22.</statement_id>
      <description type="morphology">Rachis nodes glabrous, scabrous, or with straight hairs; leaf sheaths glabrous, pilose, or floccose.</description>
      <next_statement_id>23.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>23.</statement_id>
      <description type="morphology">Junction of the lemma and awns evident; awns disarticulating at maturity.</description>
      <next_statement_id>24.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>24.</statement_id>
      <description type="morphology">Plants perennial.</description>
      <next_statement_id>25.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>25.</statement_id>
      <description type="morphology">Culms 45-100 cm tall; culms unbranched or sparingly branched; blades 12-28 cm long</description>
      <determination>18 Aristida spiciformis</determination>
    </key_statement>
    <key_statement>
      <statement_id>25.</statement_id>
      <description type="morphology">Culms 10-40 cm tall; culms much branched; blades usually less than 6 cm long</description>
      <determination>3 Aristida californica</determination>
    </key_statement>
    <key_statement>
      <statement_id>24.</statement_id>
      <description type="morphology">Plants annual.</description>
      <next_statement_id>26.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>26.</statement_id>
      <description type="morphology">Awns divergent but not arcuate or entwined above the column; cauline internodes pubescent or glabrous</description>
      <determination>3 Aristida californica</determination>
    </key_statement>
    <key_statement>
      <statement_id>26.</statement_id>
      <description type="morphology">Awns strongly arcuate, often entwined above the column or no column present; cauline internodes glabrous.</description>
      <next_statement_id>27.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>27.</statement_id>
      <description type="morphology">Glumes 10-17 mm long; lemmas beaked, the beak 2-7 mm long; awns not forming a column; calluses 1-2.5 mm long</description>
      <determination>1 Aristida desmantha</determination>
    </key_statement>
    <key_statement>
      <statement_id>27.</statement_id>
      <description type="morphology">Glumes 20-30 mm long; lemmas not beaked; awns forming a column 8-15 mm long; calluses 3-4 mm long</description>
      <determination>2 Aristida tuberculosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>23.</statement_id>
      <description type="morphology">Junction of the lemma and awns not evident; awns not disarticulating at maturity. 28. Lemmas terminating in a beak 7-30 mm long; upper glumes awned, the awns 10-12 mm long</description>
      <determination>18 Aristida spiciformis</determination>
    </key_statement>
    <key_statement>
      <statement_id>28.</statement_id>
      <description type="morphology">Lemmas not beaked or with a beak less than 7 mm long; upper glumes unawned or with an awn to 6 mm long.</description>
      <next_statement_id>29.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>29.</statement_id>
      <description type="morphology">At least the lower primary panicle branches divergent and with axillary pulvini.</description>
      <next_statement_id>30.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>30.</statement_id>
      <description type="morphology">Lateral awns about 1/2 as thick as the central awns</description>
      <determination>4 Aristida patula</determination>
    </key_statement>
    <key_statement>
      <statement_id>30.</statement_id>
      <description type="morphology">Lateral awns nearly as thick as the central awns.</description>
      <next_statement_id>31.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>31.</statement_id>
      <description type="morphology">Panicles narrow and contracted above, usually only the lower 1-2 branches spreading and with a pulvinus; lemma apices 0.2-0.3 mm wide</description>
      <determination>19 Aristida purpurea</determination>
    </key_statement>
    <key_statement>
      <statement_id>31.</statement_id>
      <description type="morphology">Almost all panicle branches spreading and with axillary pulvini; lemma apices 0.1-0.2 mm wide.</description>
      <next_statement_id>32.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>32.</statement_id>
      <description type="morphology">Anthers 0.8-1 mm long.</description>
      <next_statement_id>33.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>33.</statement_id>
      <description type="morphology">Spikelets usually divergent and the pedicels with axillary pulvini; secondary branches absent or nearly so; primary branches 2-6 cm long; lemma apices straight or with 1 or 2 twists</description>
      <determination>9 Aristida havardii</determination>
    </key_statement>
    <key_statement>
      <statement_id>33.</statement_id>
      <description type="morphology">Spikelets usually appressed and the pedicels without axillary pulvini; secondary branches usually well-developed; primary branches 5-13 cm long; lemma apices with 4 or more twists at maturity</description>
      <determination>8 Aristida divaricata</determination>
    </key_statement>
    <key_statement>
      <statement_id>32.</statement_id>
      <description type="morphology">Anthers 1-3 mm long.</description>
      <next_statement_id>34.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>34.</statement_id>
      <description type="morphology">Base of the blades with scattered hairs 1.5-3 mm long on the adaxial surfaces</description>
      <determination>6 Aristida ternipes</determination>
    </key_statement>
    <key_statement>
      <statement_id>34.</statement_id>
      <description type="morphology">Base of the blades glabrous or puberulent on the adaxial surface, the hairs, if present, less than 0.5 mm long.</description>
      <next_statement_id>35.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>35.</statement_id>
      <description type="morphology">Glumes reddish, the lower glumes often shorter than the upper glumes; awns ascending to divaricate, (8)13-140 mm long; terminal spikelets usually appressed and without axillary pulvini</description>
      <determination>19 Aristida purpurea</determination>
    </key_statement>
    <key_statement>
      <statement_id>35.</statement_id>
      <description type="morphology">Glumes brownish, equal or unequal; awns spreading to horizontal, 5-15 mm long; terminal spikelets often spreading from axillary pulvini</description>
      <determination>10 Aristida pansa</determination>
    </key_statement>
    <key_statement>
      <statement_id>29.</statement_id>
      <description type="morphology">Lower primary panicle branches (pedicels in racemose species) appressed, without axillary pulvini.</description>
      <next_statement_id>36.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>36.</statement_id>
      <description type="morphology">Plants with well-developed rhizomes; basal sheaths shredding into threadlike segments at maturity</description>
      <determination>22 Aristida rhizomopbora</determination>
    </key_statement>
    <key_statement>
      <statement_id>36.</statement_id>
      <description type="morphology">Plants tufted, without rhizomes; basal sheaths not fibrous, not shredding into threadlike segments even when old.</description>
      <next_statement_id>37.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>37.</statement_id>
      <description type="morphology">Lower inflorescence nodes with only 1 spikelet; inflorescences spicate or racemose</description>
      <determination>23 Aristida mobrii</determination>
    </key_statement>
    <key_statement>
      <statement_id>37.</statement_id>
      <description type="morphology">Lower inflorescence nodes with 2 or more spikelets; inflorescences racemose or paniculate.</description>
      <next_statement_id>38.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>38.</statement_id>
      <description type="morphology">Lower glumes usually 1/3– 3/4 as long as the upper glumes.</description>
      <next_statement_id>39.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>39.</statement_id>
      <description type="morphology">Plants annual</description>
      <determination>17 Aristida adscensionis</determination>
    </key_statement>
    <key_statement>
      <statement_id>39.</statement_id>
      <description type="morphology">Plants perennial.</description>
      <next_statement_id>40.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>40.</statement_id>
      <description type="morphology">Lemma awns 8-15 mm long; lemmas 5-7 mm long</description>
      <determination>29 Aristida gyrans</determination>
    </key_statement>
    <key_statement>
      <statement_id>40.</statement_id>
      <description type="morphology">Lemma awns (8)15-140 mm long; lemmas 6-16 mm long</description>
      <determination>19 Aristida purpurea</determination>
    </key_statement>
    <key_statement>
      <statement_id>38.</statement_id>
      <description type="morphology">Lower glumes usually more than 3/4 as long as the upper glumes.</description>
      <next_statement_id>41.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>41.</statement_id>
      <description type="morphology">Plants annual.</description>
      <next_statement_id>42.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>42.</statement_id>
      <description type="morphology">Awns flat at the base</description>
      <determination>17 Aristida adscensionis</determination>
    </key_statement>
    <key_statement>
      <statement_id>42.</statement_id>
      <description type="morphology">Awns terete at the base</description>
      <determination>16 Aristida longespica</determination>
    </key_statement>
    <key_statement>
      <statement_id>41.</statement_id>
      <description type="morphology">Plants perennial.</description>
      <next_statement_id>43.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>43.</statement_id>
      <description type="morphology">Lemma apices prominently twisted for 3-6 mm; blades usually curled at maturity; leaves forming a basal tuft</description>
      <determination>20 Aristida arizonica</determination>
    </key_statement>
    <key_statement>
      <statement_id>43.</statement_id>
      <description type="morphology">Lemma apices straight or only slightly twisted; blades usually not curled at maturity; leaves variously distributed.</description>
      <next_statement_id>44.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>44.</statement_id>
      <description type="morphology">Lower glumes prominently 2-keeled, (7.5)9-13 mm long; central awns 15-40 mm long</description>
      <determination>26 Aristida palustris</determination>
    </key_statement>
    <key_statement>
      <statement_id>44.</statement_id>
      <description type="morphology">Lower glumes usually 1-keeled, if 2-keeled, 5-9 mm long; central awns 8-25 mm long, [revert to left]</description>
      <next_statement_id>45.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>45.</statement_id>
      <description type="morphology">Central awns about twice as thick as the lateral awns, divergent to arcuate-reflexed.</description>
      <next_statement_id>46.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>46.</statement_id>
      <description type="morphology">All 3 awns divergent to reflexed and contorted at the base; lower rachis nodes usually associated with 2 spikelets (occasionally 1 or 3), 1 pedicellate and 1 sessile</description>
      <determination>24 Aristida simpliciflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>46.</statement_id>
      <description type="morphology">Lateral awns usually erect to ascending and not contorted at the base; lower rachis nodes usually associated with more than 2 spikelets, pedicellate to subsessile</description>
      <determination>27 Aristida purpurascens</determination>
    </key_statement>
    <key_statement>
      <statement_id>45.</statement_id>
      <description type="morphology">Central awns about the same thickness as the lateral awns, erect to spreading.</description>
      <next_statement_id>47.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>47.</statement_id>
      <description type="morphology">Lower glumes 1-4 mm longer than the upper glumes</description>
      <determination>27 Aristida purpurascens</determination>
    </key_statement>
    <key_statement>
      <statement_id>47.</statement_id>
      <description type="morphology">Lower glumes from shorter than to 1 mm longer than the upper glumes.</description>
      <next_statement_id>48.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>48.</statement_id>
      <description type="morphology">Culms usually 3-6 mm thick at the base; primary panicle branches 4-20 cm long; lower glumes 1-veined</description>
      <determination>28 Aristida condensata</determination>
    </key_statement>
    <key_statement>
      <statement_id>48.</statement_id>
      <description type="morphology">Culms usually 1-4 mm thick at the base; primary panicle branches 1-5 cm long; lower glumes 1-2-veined.</description>
      <next_statement_id>49.</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>49.</statement_id>
      <description type="morphology">Calluses 0.4-0.8 mm long</description>
      <determination>27 Aristida purpurascens</determination>
    </key_statement>
    <key_statement>
      <statement_id>49.</statement_id>
      <description type="morphology">Calluses 1-2 mm long</description>
      <determination>29 Aristida gyrans</determination>
    </key_statement>
  </key>
  <description type="distribution">Conn.;D.C;Del.;Ill.;Ind.;Mass.;Md.;Mo.;Mont.;Nebr.;N.H.;N.J.;N.Y.;Ohio;Okla.;Pa.;R.I.;Tex.;Utah;Vt.;Wyo.;Ga.;Wash.;Va.;W.Va.;Mich.;Wis.;Ariz.;Kans.;Minn.;Maine;Miss.;Tenn.;Iowa;Pacific Islands (Hawaii);Fla.;N.Mex.;Ala.;Ark.;N.C.;S.C.;La.;S.Dak.;Nev.;Puerto Rico;Colo.;Virgin Islands;Calif.;Idaho;Alta.;B.C.;Man.;Ont.;Que.;Sask.;N.Dak.;Ky.;Oreg.</description>
</bio:treatment>
