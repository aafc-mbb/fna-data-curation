<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <other_info_on_meta type="volume">27</other_info_on_meta>
    <other_info_on_meta type="mention_page">226</other_info_on_meta>
    <other_info_on_meta type="mention_page">231</other_info_on_meta>
    <other_info_on_meta type="mention_page">233</other_info_on_meta>
    <other_info_on_meta type="mention_page">247</other_info_on_meta>
    <other_info_on_meta type="treatment_page">232</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Arnott" date="unknown">grimmiaceae</taxon_name>
    <taxon_name rank="subfamily" authority="Brotherus" date="1902">grimmioideae</taxon_name>
    <taxon_name rank="genus" authority="Hedwig" date="1801">grimmia</taxon_name>
    <taxon_name rank="subgenus" authority="Schimper" date="1856">Grimmia</taxon_name>
    <taxon_name rank="species" authority="Cardot" date="1890">crinitoleucophaea</taxon_name>
    <place_of_publication>
      <publication_title>Rev. Bryol.</publication_title>
      <place_in_publication>17: 18. 1890,</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family grimmiaceae;subfamily grimmioideae;genus grimmia;subgenus Grimmia;species crinitoleucophaea</taxon_hierarchy>
    <other_info_on_name type="fna_id">250075417</other_info_on_name>
  </taxon_identification>
  <description type="morphology">Plants in loose tufts, olivaceous to black. Stems 0.5–0.9 cm. Leaves oblong-ovate to oblong-lanceolate, 1.6–2 × 0.3–0.6 mm, concave, awn 0.3–0.6 mm; basal juxtacostal laminal cells quadrate to long-rectangular, straight, thin-walled; basal marginal laminal cells quadrate to short-rectangular, straight, thin-walled; medial laminal cells rounded, straight, thick-walled; distal laminal cells 2-stratose, marginal cells 2-stratose. Sexual condition dioicous. Seta sigmoid, 0.3–0.5 mm. Capsule usually present, exothecial cells thin-walled, annulus of 2–3 rows, rectangular, thick-walled, revoluble, operculum obliquely rostrate, peristome present, rudimentary, teeth composed of only a few rows of cells, perforated, papillose.</description>
  <description type="habitat">Basalt, granite, schist and limestone</description>
  <description type="elevation">moderate to high elevations (500-2100 m)</description>
  <description type="distribution">B.C., N.W.T., Yukon; Ariz., Calif., Colo., Nev., N.Mex., Utah, Wash.; Eurasia.</description>
  <number>4.</number>
  <discussion>In North America, Grimmia crinitoleucophaea is known from only scattered localities in the American west and in three extremely disjunct sites in Canada: in southern British Columbia, near the Keele River of the Northwest Territories, and along the Dempster Highway in the Yukon. It is found on a broad range of both basic and acidic rock types. Its subgeneric placement is problematic. Gametophytically the species is inseparable from G. tergestina, of subg. Litoneuron. Indeed, based on areolation and leaf shape, L. Loeske (1913–1914, part 1) placed it as a subspecies of the latter. This close similarity may account for reports by J. Muñoz and F. Pando (2000) and D. H. Norris and J. R. Shevock (2004) of G. tergestina from North America. We have seen all cited specimens in these papers and they all represent G. crinitoleucophaea or G. ovalis. Therefore, we reject G. tergestina being in North America. Sporophytic characters of G. crinitoleucophaea (a short, arcuate to sigmoid seta that is eccentrically attached to the capsule, an immersed ventricose capsule with a small, mitrate calyptra, and 3–4 large stomata) clearly indicate membership in subg. Grimmia. Further, G. crinitoleucophaea and G. tergestina have never been collected together, suggesting that the two are also ecologically distinct. Despite its dioicous sexuality, G. crinitoleucophaea is usually fertile; its rudimentary peristome and large annulus are thus readily evident. Its 2-stratose laminal stratification separates it from specimens of G. plagiopodia that may have broken peristome teeth.</discussion>
</bio:treatment>
