<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>Kamal I. Mohamed, Lytton J. Musselman</author>
    </source>
    <other_info_on_meta type="treatment_page">508</other_info_on_meta>
    <other_info_on_meta type="mention_page">457</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta></meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Ventenat" date="unknown">OROBANCHACEAE</taxon_name>
    <taxon_name rank="genus" authority="Loureiro" date="1790">STRIGA</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Cochinch.</publication_title>
      <place_in_publication>1: 22. 1790</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus striga</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin strigosus, slender, alluding to habit</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
  </taxon_identification>
  <number>14.</number>
  <other_name type="common_name">Witchweed</other_name>
  <description type="morphology">Herbs, annual; chlorophyllous or achlorophyllous, hemiparasitic or holoparasitic, haustoria either single and relatively large, or multiple, smaller, and formed on secondary roots. Stems erect, sometimes fleshy, hispid, puberulent, or glabrous. Leaves cauline, opposite, subopposite, or alternate; petiole absent; blade not fleshy, not leathery, margins entire. Inflorescences terminal, racemes or spikes; bracts present. Pedicels present or absent; bracteoles present. Flowers: sepals 5(–8), calyx radially or bilaterally symmetric, tubular, lobes lanceolate or subulate; petals 5, corolla red, brownish red, or purple, rarely white or yellow, bilabiate, salverform, abaxial lobes 3, adaxial 2; stamens 4, didynamous, filaments glabrous; staminode 0; ovary 2-locular, placentation axile; stigma capitate. Capsules: dehiscence loculicidal. Seeds 400–600, brown or black, ovoid, wings absent.</description>
  <discussion>Species ca. 40 (2 in the flora).</discussion>
  <description type="distribution">introduced; s Asia, Africa, Australia.</description>
  <discussion>Striga produces leaves of different sizes; typical proximal leaves are scalelike, and mid-stem leaves are larger. Striga is distinguished from its close relative Buchnera by its bilabiate corolla with an abruptly bent tube, one pollen sac, and glabrous filaments. Buchnera has a bilabiate corolla with a straight or slightly curved tube, two pollen sacs, and pilose filaments. Striga has been divided into three sections based on the number of ribs on the calyx tube (R. Wettstein 1891–1893): sect. Pentapleurae Wettstein with five, sect. Polypleurae Wettstein with ten, and sect. Tetrasepalum Engler with 15.</discussion>
  <discussion>Thirty-four species and subspecies of witchweeds occur in Africa; 22 are endemic (K. I. Mohamed et al. 2001). All Striga species parasitize hosts in the Poaceae except S. gesnerioides, which grows on hosts in Acanthaceae, Convolvulaceae, Euphorbiaceae, Fabaceae, and Solanaceae. Striga asiatica, S. aspera Bentham, S. forbesii Bentham, S. gesnerioides, and S. hermonthica (Delile) Bentham are of economic importance. Crops most affected by Striga include Digitaria exilis (fonio), Oryza subspp. (upland rice), Pennisetum glaucum (bulrush millet), Sorghum vulgare (sorghum), and Zea mays (maize). Striga gesnerioides is a serious pest on Vigna unguiculata (cowpea, Fabaceae) and a minor pest on other dicot crops. All species of witchweed are listed as noxious weeds by the United States Department of Agriculture and 11 state governments. New infestations of quarantine pests in the United States, such as witchweeds, should be reported to the State Plant Health Director in the appropriate state (http://www.aphis.usda.gov/services/report_pest_disease/report_pest_disease.shtml).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Calyx ribs 10; mid-stem leaf blades ascending or spreading, linear or narrowly elliptic, 20–50 mm; bracts linear, longer than calyces; corollas red, rarely yellow, with yellow throats; parasitic on Poaceae.</description>
      <determination>1. Striga asiatica</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Calyx ribs 5; mid-stem leaf blades appressed, lanceolate, 3–7 mm; bracts lanceolate, shorter than calyces; corollas brownish red or purple, rarely white; parasitic on dicots.</description>
      <determination>2. Striga gesnerioides</determination>
    </key_statement>
  </key>
</bio:treatment>