<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>Wayne J. Elisens</author>
    </source>
    <other_info_on_meta type="treatment_page">35</other_info_on_meta>
    <other_info_on_meta type="mention_page">14</other_info_on_meta>
    <other_info_on_meta type="mention_page">20</other_info_on_meta>
    <other_info_on_meta type="mention_page">21</other_info_on_meta>
    <other_info_on_meta type="mention_page">36</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">PLANTAGINACEAE</taxon_name>
    <taxon_name rank="genus" authority="(A. Gray) Rothmaler" date="1943">MAURANDELLA</taxon_name>
    <place_of_publication>
      <publication_title>Feddes Repert. Spec. Nov. Regni Veg.</publication_title>
      <place_in_publication>52: 26. 1943</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus maurandella</taxon_hierarchy>
    <other_info_on_name type="etymology">Genus Maurandya and Latin -ella, diminutive, alluding to presence of personate corolla in Maurandella</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name rank="genus" authority="Linnaeus [unranked] Maurandella A. Gray" date=" 1868">Antirrhinum</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>7: 375. 1868</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus antirrhinum</taxon_hierarchy>
  </taxon_identification>
  <number>12.</number>
  <other_name type="common_name">Snapdragon vine</other_name>
  <description type="morphology">Herbs, perennial; caudex woody. Stems climbing, glabrous. Leaves cauline, alternate; petiole twining; blade fleshy, not leathery, margins entire. Inflorescences axillary, flowers solitary; bracts absent. Pedicels present; bracteoles absent. Flowers bisexual; sepals 5, distinct, lanceolate, calyx bilaterally symmetric, campanulate; corolla blue to violet, pink, or red, bilaterally symmetric, bilabiate and personate, tubular, tube base not spurred or gibbous, lobes 5, abaxial 3, adaxial 2; stamens 4, basally adnate to corolla, didynamous, filaments glandular-hairy; staminode 1, filamentous; ovary 2-locular, placentation axile; stigma 2-lobed. Fruits capsules, dehiscence loculicidal. Seeds 80–150, dark brown to black, ovoid to oblong-polygonal, wings absent. x = 12.</description>
  <discussion>Species 2 (1 in the flora).</discussion>
  <description type="distribution">sw, sc United States, Mexico.</description>
  <discussion>Maurandella most commonly has been recognized as a genus segregated from Maurandya Ortega or as a section within Maurandya, based on distinctive characteristics: personate corolla, globular capsule, V-shaped septum, and marked asymmetry of the locules. The V-shaped septum results in unequal locule volumes, which is unique in Antirrhineae. Based on this unique combination of characteristics, Maurandella is recognized here as a genus distinct from Epixiphium and Maurandya.</discussion>
  <discussion>Initial molecular phylogenetic studies indicated a sister group relationship between species of Maurandella and Maurandya (M. Ghebrehiwet et al. 2000); they lacked the taxon sampling needed to resolve relationships among closely related species in Epixiphium, Maurandella, and Maurandya (C. E. Freeman and R. Scogin 1999; P. Vargas et al. 2004). The more complete ITS sampling by M. Fernández-Mazuecos et al. (2013) placed both genera in the Cymbalaria clade, but not as sister taxa; Maurandya was sister to Lophospermum, whereas Maurandella was nested in a more basal position within a seven-taxon subclade comprising genera in subtribe Maurandyinae.</discussion>
  <discussion>Maurandella hederifolia Rothmaler is known from northern Mexico.</discussion>
</bio:treatment>
