<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="treatment_page">466</other_info_on_meta>
    <other_info_on_meta type="mention_page">465</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Ventenat" date="unknown">OROBANCHACEAE</taxon_name>
    <taxon_name rank="genus" authority="(Beck) Beck" date="1930">KOPSIOPSIS</taxon_name>
    <taxon_name rank="species" authority="(Walpers) Govaerts" date="1996">hookeri</taxon_name>
    <place_of_publication>
      <publication_title>World Checkl. Seed Pl.</publication_title>
      <place_in_publication>2: 14. 1996</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus kopsiopsis;species hookeri</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Boschniakia</taxon_name>
    <taxon_name rank="species" authority="Walpers" date="1844">hookeri</taxon_name>
    <place_of_publication>
      <publication_title>Repert. Bot. Syst.</publication_title>
      <place_in_publication>3: 479. 1844</place_in_publication>
      <other_info_on_pub>based on Orobanche tuberosa Hooker, Fl. Bor.-Amer. 2: 92, plate 168. 1838, not Vellozo 1829</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus boschniakia;species hookeri</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <other_name type="common_name">Small or Hooker’s groundcone</other_name>
  <description type="morphology">Tuberlike bases 20–50 mm diam., surface finely tessellate. Stems dark red-brown, purple, or yellow, 55–160 mm, slender, 5–17 mm diam. Leaves tightly to loosely imbricate; blade triangular-obovate or rhombic, 6–12 × 5–12 mm, margins finely erose, apex obtuse or ± acute. Inflorescences compact racemes, 6–10 cm diam.; bracts erect, sometimes spreading, purple or yellow, narrowly spatulate or lanceolate, 7–11(–14) × 6–10 mm, margins erose, frequently white-translucent, apex obtuse or acute, slightly rolled adaxially. Pedicels 0–1.5 mm; bracteoles 2. Flowers dark purple, pink, or frequently yellow, 10–15 mm; calyx cup 1–3 mm deep, lobes persistent, 2–4, attenuate or linear-subulate, equal to cup; corolla lobe margins white, lavender, wine red, pink, or yellow, minutely ciliate, abaxial lip shorter than adaxial, lobes 3–5 mm, apex apiculate, adaxial lip margins inrolled; filaments with tuft of hair at base, glabrous distally, anthers glabrous or sparsely villous after anthesis, included, base rounded, sterile appendage (connective) short; style equal to or longer than stamens; stigma obscurely 2- or 3-lobed; carpels 3; placentae 3. Capsules 3-valved. Seeds 1.5–2 mm.</description>
  <discussion>Kopsiopsis hookeri is a coastal species parasitic on Gaultheria shallon (Ericaceae) and confined to the range of the host species. It has been reported rarely on Alnus, Arbutus, and Arctostaphylos. Reports on Pinus are clearly spurious. It is often confused with K. strobilacea because of their close morphological similarity, but K. hookeri is a much smaller plant.</discussion>
  <discussion>The range of Kopsiopsis hookeri is from southern Oregon and northwestern California to the Queen Charlotte Islands (Haida Gwaii) in British Columbia, with apparently disjunct populations in isolated locations in California. A few historic collections at CAS and UC from southern California are morphologically indistinguishable from typical K. hookeri, indicating that this taxon may have once occurred south of its present distributional boundary. Populations represented by specimen records from Marin (1925, 1944, 1958), Mendocino (1966), and Monterey (1957) counties may still be extant and indicate the need for additional studies to determine the current range of the species. A single specimen from Los Angeles County may be mislabeled, as this area is outside the range of the host taxon.</discussion>
  <discussion>No morphological evidence was observed in the disjunct populations to suggest genetic influence from Kopsiopsis strobilacea; refer to 1. K. strobilacea for further discussion.</discussion>
  <description type="phenology">Flowering Apr–Jul.</description>
  <description type="habitat">Sandy coastal areas in thickets.</description>
  <description type="elevation">0–700 m.</description>
  <description type="distribution">B.C.; Calif., Oreg., Wash.</description>
</bio:treatment>
