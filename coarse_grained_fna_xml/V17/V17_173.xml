<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="treatment_page">59</other_info_on_meta>
    <other_info_on_meta type="mention_page">52</other_info_on_meta>
    <other_info_on_meta type="mention_page">57</other_info_on_meta>
    <other_info_on_meta type="mention_page">58</other_info_on_meta>
    <other_info_on_meta type="mention_page">60</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">PLANTAGINACEAE</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">CHELONE</taxon_name>
    <taxon_name rank="species" authority="Linnaeus" date="1753">glabra</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>2: 611. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus chelone;species glabra</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Chelone</taxon_name>
    <taxon_name rank="species" authority="Pennell &amp; Wherry" date="unknown">chlorantha</taxon_name>
    <taxon_hierarchy>genus chelone;species chlorantha</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">C.</taxon_name>
    <taxon_name rank="species" authority="unknown" date="unknown">glabra</taxon_name>
    <taxon_name rank="variety" authority="Fernald &amp; Wiegand" date="unknown">dilatata</taxon_name>
    <taxon_hierarchy>genus c.;species glabra;variety dilatata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">C.</taxon_name>
    <taxon_name rank="species" authority="unknown" date="unknown">glabra</taxon_name>
    <taxon_name rank="variety" authority="Rafinesque" date="unknown">elatior</taxon_name>
    <taxon_hierarchy>genus c.;species glabra;variety elatior</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">C.</taxon_name>
    <taxon_name rank="species" authority="unknown" date="unknown">glabra</taxon_name>
    <taxon_name rank="subspecies" authority="(N. Coleman) Pennell" date="unknown">linifolia</taxon_name>
    <taxon_hierarchy>genus c.;species glabra;subspecies linifolia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">C.</taxon_name>
    <taxon_name rank="species" authority="unknown" date="unknown">glabra</taxon_name>
    <taxon_name rank="variety" authority="N. Coleman" date="unknown">linifolia</taxon_name>
    <taxon_hierarchy>genus c.;species glabra;variety linifolia</taxon_hierarchy>
  </taxon_identification>
  <number>3.</number>
  <other_name type="common_name">White turtlehead</other_name>
  <other_name type="common_name">galane glabre</other_name>
  <description type="morphology">Stems 20–230 cm. Leaves: petiole (0–)2–10(–20) mm; blade broadly elliptic to narrowly elliptic, 17–230 × 6–54 mm, base cuneate, margins once-serrate, teeth 1–6 per cm, abaxial surface glabrous or pilose, rarely tomentose, adaxial glabrate or mostly glabrous. Cymes 30–115 mm; bracts 4–23 × 3–10 mm, ape× acute to acuminate, rarely obtuse. Flowers: calyx lobes 5–11 × 3–8 mm, margins not or sparsely, rarely densely, ciliate; corolla completely white to white in tube and distally green-white, pink, red, or purple, tube 13–20 mm, abaxial lobes 6–16 × 5–15 mm, adaxial slightly keeled; palate white-bearded, rarely green-yellow-bearded; adaxial filaments (10–)15–24 mm; staminode 4–12(–16) mm, ape× green; style 15–30 mm. 2n = 28.</description>
  <discussion>A tetraploid population reported for Chelone glabra (T. S. Cooperrider 1970) likely is based on a misidentified specimen of C. obliqua var. erwiniae.</discussion>
  <discussion>Chelone glabra has been proposed as a diploid progenitor for the allopolyploid C. obliqua (A. D. Nelson 1995; Nelson and W. J. Elisens 1999).</discussion>
  <discussion>Infraspecific variants of Chelone glabra as proposed by F. W. Pennell (1935) are not recognized here because morphological variation within and among populations appears to be independent of geography. Qualitative and quantitative characters used to distinguish varieties have been shown to be highly variable; recognition of varieties is unwarranted (A. D. Nelson 1995; Nelson and W. J. Elisens 1999).</discussion>
  <discussion>Chelone glabra in central Maine is visited exclusively by two species of bumblebees (Bombus fervidus and B. vagans) (B. Heinrich 1975). American Indians and pioneers used C. glabra as a tonic, laxative, and treatment for jaundice and internal parasites, and as an ointment to relieve itching and inflammation (C. S. Rafinesque 1828[–1830]); it is also used as an ornamental in bog gardens.</discussion>
  <description type="phenology">Flowering Jul–Nov.</description>
  <description type="habitat">Bogs, fens, marshes, swamps, seeps, stream banks, wet meadows and woods, margins of ponds and lakes.</description>
  <description type="elevation">0–2000 m.</description>
  <description type="distribution">St. Pierre and Miquelon; Man., N.B., Nfld. and Labr. (Nfld.), N.S., Ont., P.E.I., Que.; Ala., Ark., Conn., Del., D.C., Ga., Ill., Ind., Iowa, Ky., Maine, Md., Mass., Mich., Minn., Miss., Mo., N.H., N.J., N.Y., N.C., Ohio, Pa., R.I., S.C., Tenn., Vt., Va., W.Va., Wis.</description>
</bio:treatment>
