<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="treatment_page">217</other_info_on_meta>
    <other_info_on_meta type="mention_page">216</other_info_on_meta>
    <other_info_on_meta type="mention_page">218</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">PLANTAGINACEAE</taxon_name>
    <taxon_name rank="genus" authority="Schmidel" date="1763">PENSTEMON</taxon_name>
    <taxon_name rank="subgenus" authority="unknown" date="unknown">Penstemon</taxon_name>
    <taxon_name rank="section" authority="unknown" date="unknown">Penstemon</taxon_name>
    <taxon_name rank="species" authority="Douglas ex Graham" date="1829">procerus</taxon_name>
    <taxon_name rank="variety" authority="(D. D. Keck) Cronquist" date="1959">formosus</taxon_name>
    <place_of_publication>
      <publication_title>in C. L. Hitchcock et al., Vasc. Pl. Pacif. N.W.</publication_title>
      <place_in_publication>4: 398. 1959</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section penstemon;species procerus;variety formosus;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Penstemon</taxon_name>
    <taxon_name rank="species" authority="Hooker" date="unknown">tolmiei</taxon_name>
    <taxon_name rank="subspecies" authority="D. D. Keck" date="1945">formosus</taxon_name>
    <place_of_publication>
      <publication_title>Amer. Midl. Naturalist</publication_title>
      <place_in_publication>33: 147. 1945</place_in_publication>
      <other_info_on_pub>based on P. formosus A. Nelson, Proc. Biol. Soc. Wash. 17: 100. 1904 (as Pentstemon), based on P. pulchellus Greene, Pittonia 3: 310. 1898 (as Pentstemon), not Lindley 1828</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus penstemon;species tolmiei;subspecies formosus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">P.</taxon_name>
    <taxon_name rank="species" authority="unknown" date="unknown">procerus</taxon_name>
    <taxon_name rank="subspecies" authority="(D. D. Keck) D. D. Keck" date="unknown">formosus</taxon_name>
    <taxon_hierarchy>genus p.;species procerus;subspecies formosus</taxon_hierarchy>
  </taxon_identification>
  <number>181d.</number>
  <description type="morphology">Stems 3–15(–20) cm. Leaves: basal and proximal cauline 9–30(–48) × 2–7(–12) mm; cauline 6–20(–45) × 1–4(–8) mm, blade lanceolate to linear, proximals sometimes spatulate. Thyrses 0.5–3(–12) cm, verticillasters 1 or 2(–5). Flowers: calyx lobes 1.4–3 × 0.5–1.5 mm, apex acute or short-caudate, rarely truncate, obtuse, or mucronate; corolla 7–10 mm; pollen sacs 0.4–0.6 mm; staminode glabrous or distal 0.5 mm sparsely pilose, hairs yellow, to 0.4 mm.</description>
  <discussion>D. D. Keck (1945) mapped populations of var. formosus in three widely separated areas: eastern Oregon (Blue Mountains, Steens Mountain, and Wallowa Mountains), northern California in the Klamath Range, and in the southern High Sierra Nevada and ranges in the western Great Basin in east-central California and western Nevada. Populations in the northern part of the range are typical var. formosus, which have spatulate, oblanceolate, or lanceolate basal and proximal cauline leaves. Populations in the southern part of the range usually have linear-oblanceolate basal and proximal cauline leaves. Tall plants can be mistaken for Penstemon cinicola, but var. formosus has calyx lobes that are consistently ovate to lanceolate with apices acute to short-caudate. Calyx lobes of P. cinicola are obovate to ovate with apices truncate or cuspidate.</discussion>
  <discussion>Specimens from the Toquima Range in Nye County, Nevada, have been referred to var. formosus. They have the short stems and capitate thyrses of that variety but bear short (1–1.5 mm), truncate or mucronate calyx lobes that are typical of var. modestus.</discussion>
  <discussion>Variety formosus has been reported from Custer County, Idaho (S. Caicco et al. 1983). Specimens cited in that paper have not been seen, but a collection from the same area by one of the coauthors and identified as var. formosus (D. Henderson 6439, RM) appears to be representative of the cited populations. That sheet bears three short (6–8 cm) flowering stems, each with two or three pairs of elliptic leaves and a more or less capitate inflorescence, calyx lobes 2–2.7 mm with acute apices, and corollas 10–12 mm, features consistent with var. formosus. However, the thyrses, calyces, and corollas are glandular-pubescent, and the corolla throats are 3–4 mm in diameter, suggesting possible contact with Penstemon humilis; consequently, Idaho is excluded from the distribution of var. formosus.</discussion>
  <description type="phenology">Flowering Jun–Aug.</description>
  <description type="habitat">Alpine talus slopes, alpine meadows.</description>
  <description type="elevation">2200–3600 m.</description>
  <description type="distribution">Calif., Nev., Oreg.</description>
</bio:treatment>
