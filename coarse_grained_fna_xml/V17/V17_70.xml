<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="treatment_page">355</other_info_on_meta>
    <other_info_on_meta type="mention_page">354</other_info_on_meta>
    <other_info_on_meta type="mention_page">356</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Borsch, Kai Müller &amp; Eb. Fischer" date="unknown">LINDERNIACEAE</taxon_name>
    <taxon_name rank="genus" authority="Allioni" date="1766">LINDERNIA</taxon_name>
    <taxon_name rank="species" authority="(Linnaeus) Pennell" date="1935">dubia</taxon_name>
    <place_of_publication>
      <publication_title>Monogr. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>1: 141. 1935</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family linderniaceae;genus lindernia;species dubia</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Gratiola</taxon_name>
    <taxon_name rank="species" authority="Linnaeus" date="1753">dubia</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 17. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus gratiola;species dubia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Ilysanthes</taxon_name>
    <taxon_name rank="species" authority="(Linnaeus) Barnhart" date="unknown">dubia</taxon_name>
    <taxon_hierarchy>genus ilysanthes;species dubia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">I.</taxon_name>
    <taxon_name rank="species" authority="(Walter) Pennell" date="unknown">inaequalis</taxon_name>
    <taxon_hierarchy>genus i.;species inaequalis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">I.</taxon_name>
    <taxon_name rank="species" authority="Rafinesque" date="unknown">riparia</taxon_name>
    <taxon_hierarchy>genus i.;species riparia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Lindernia</taxon_name>
    <taxon_name rank="species" authority="(Michaux) Pennell" date="unknown">anagallidea</taxon_name>
    <taxon_hierarchy>genus lindernia;species anagallidea</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">L.</taxon_name>
    <taxon_name rank="species" authority="unknown" date="unknown">dubia</taxon_name>
    <taxon_name rank="variety" authority="(Michaux) Cooperrider" date="unknown">anagallidea</taxon_name>
    <taxon_hierarchy>genus l.;species dubia;variety anagallidea</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">L.</taxon_name>
    <taxon_name rank="species" authority="unknown" date="unknown">dubia</taxon_name>
    <taxon_name rank="variety" authority="(Pennell) Pennell" date="unknown">inundata</taxon_name>
    <taxon_hierarchy>genus l.;species dubia;variety inundata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">L.</taxon_name>
    <taxon_name rank="species" authority="unknown" date="unknown">dubia</taxon_name>
    <taxon_name rank="variety" authority="(Rafinesque) Fernald" date="unknown">riparia</taxon_name>
    <taxon_hierarchy>genus l.;species dubia;variety riparia</taxon_hierarchy>
  </taxon_identification>
  <number>3.</number>
  <other_name type="common_name">Yellowseed false-pimpernel</other_name>
  <other_name type="common_name">lindernie douteuse</other_name>
  <description type="morphology">Annuals. Stems erect, ascending, or prostrate, not matted, usually rooting at proximal nodes, 1.5–27(–38) cm. Leaves cauline, thin; petiole absent; blade spatulate, lanceolate, oblanceolate, elliptic, ovate, or obovate, (1–)5–37 × (0.5–)3–18 mm, palmately 3–5-veined or 1-nerved, not leathery, margins entire or remotely, sometimes coarsely, toothed; distal well developed or much reduced. Pedicels 0.5–31 mm, 1/2–5 times subtending leaves. Flowers chasmogamous or cleistogamous; chasmogamous: sepals 0.7–6.1 mm, connate to 1/8 lengths; corolla tube and adaxial lip lavender or blue to white, abaxial lobes white with purple to blue markings, tube 2.5–8 mm, adaxial lip 1/2 abaxial; stamens 2; staminodes each with appendage and distal segment. Capsules ellipsoid, often obliquely, sometimes ovoid or subglobular, 1.4–6.3(–7.5) × 1.2–3.3 mm. Seeds usually 6-angled, usually ribbed. 2n = 18.</description>
  <discussion>In Asia, Lindernia dubia is considered a noxious weed in rice paddies, where it has become resistant to some commonly used herbicides.</discussion>
  <discussion>Lindernia dubia shows extreme morphological plasticity, especially in vegetative characters. This has led to the naming of species and varieties that have been accepted or not in recent treatments (for example, D. Q. Lewis 2000). B. A. Berger (2005) examined variation within the L. dubia complex and concluded that the recognition of these taxa is unwarranted.</discussion>
  <discussion>Variety inundata, an estuarine form from the intertidal zone along the Atlantic Coast, continues to be recognized in several databases. However, W. R. Ferren Jr. and A. E. Schuyler (1980) described the clinal variation in these intertidal populations, ranging from typical Lindernia dubia to this form, with such variation sometimes evident on submerged and emergent parts of the same plant.</discussion>
  <description type="phenology">Flowering year-round.</description>
  <description type="habitat">Wet ditches, meadows, borders of ponds, lakes, streams, moist to wet disturbed habitats.</description>
  <description type="elevation">0–2600 m.</description>
  <description type="distribution">B.C., N.B., N.S., Ont., P.E.I., Que.; Ala., Ariz., Ark., Calif., Colo., Conn., Del., D.C., Fla., Ga., Idaho, Ill., Ind., Iowa, Kans., Ky., La., Maine, Md., Mass., Mich., Minn., Miss., Mo., Nebr., Nev., N.H., N.J., N.Mex., N.Y., N.C., N.Dak., Ohio, Okla., Oreg., Pa., R.I., S.C., S.Dak., Tenn., Tex., Vt., Va., Wash., W.Va., Wis.; Mexico; West Indies; Central America; South America; introduced in Europe, Asia, Africa, Australia.</description>
</bio:treatment>
