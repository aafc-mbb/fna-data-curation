<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="treatment_page">125</other_info_on_meta>
    <other_info_on_meta type="mention_page">93</other_info_on_meta>
    <other_info_on_meta type="mention_page">94</other_info_on_meta>
    <other_info_on_meta type="mention_page">130</other_info_on_meta>
    <other_info_on_meta type="mention_page">138</other_info_on_meta>
    <other_info_on_meta type="mention_page">142</other_info_on_meta>
    <other_info_on_meta type="mention_page">207</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">PLANTAGINACEAE</taxon_name>
    <taxon_name rank="genus" authority="Schmidel" date="1763">PENSTEMON</taxon_name>
    <taxon_name rank="subgenus" authority="unknown" date="unknown">Penstemon</taxon_name>
    <taxon_name rank="section" authority="(Rydberg) Pennell" date="1920">Cristati</taxon_name>
    <place_of_publication>
      <publication_title>Contr. U.S. Natl. Herb.</publication_title>
      <place_in_publication>20: 325, 328. 1920</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section cristati;</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name rank="genus" authority="[unranked] Cristati Rydberg" date=" 1906">Penstemon</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Colorado,</publication_title>
      <place_in_publication>306. 1906</place_in_publication>
      <other_info_on_pub>(as Pentstemon)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus penstemon</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Penstemon</taxon_name>
    <taxon_name rank="section" authority="Pennell" date="unknown">Albidi</taxon_name>
    <taxon_hierarchy>genus penstemon;section albidi</taxon_hierarchy>
  </taxon_identification>
  <number>26b7.</number>
  <description type="morphology">Herbs, caulescent or essentially acaulescent (P. acaulis, P. yampaënsis). Stems retrorsely hairy, sometimes glandular-pubescent or glandular-villous distally or wholly, rarely glabrous, glabrate, or scabrous proximally or wholly, not glaucous. Leaves basal and cauline, sometimes ± cauline or ± basal, opposite, rarely whorled (P. albidus, P. auriberbis, P. jamesii), leathery or not, glabrous or glabrate, scabrous, retrorsely hairy, glandular-pubescent, pubescent, villous, or glandular-villous, not glaucous; basal and proximal cauline petiolate, sometimes sessile, rarely short-petiolate (P. pumilus); cauline sessile or short-petiolate, blade ovate, deltate-ovate, oblanceolate, lanceolate, elliptic, oblong, or linear, rarely pandurate, margins entire or toothed. Thyrses continuous or interrupted, cylindric, rarely ± secund to secund, axis glandular-pubescent, glandular-villous, or retrorsely hairy, rarely puberulent or glabrous, cymes 1 or 2 per node; peduncles and pedicels ascending to erect. Flowers: calyx lobes: margins entire, sometimes erose (P. atwoodii, P. laricifolius, P. triflorus), herbaceous or ± scarious, glandular-pubescent or glandular-villous, sometimes also scabrous, rarely entirely retrorsely hairy, puberulent, or glabrous; corolla white to lilac, lavender, blue, violet, purple, red, pink, or magenta, bilaterally symmetric, rarely nearly radially symmetric (P. goodrichii), weakly to strongly bilabiate, not personate, funnelform, tubular-funnelform, ventricose, or ampliate, glandular-pubescent externally, rarely glabrous (P. laricifolius), hairy internally abaxially, rarely glabrous or glandular-pubescent, throat gradually to abruptly inflated, rarely slightly inflated, constricted or not at orifice, rounded abaxially, sometimes slightly 2-ridged; stamens included, reaching orifice, or longer pair exserted, filaments glabrous, pollen sacs parallel to divergent or opposite, navicular to subexplanate or explanate, rarely navicular-sigmoid, dehiscing completely, rarely incompletely, proximal 1/5–1/2 indehiscent, connective splitting, rarely not, sides glabrous, sutures smooth or papillate; staminode included to exserted, flattened distally, 0.2–1.1 mm diam., tip straight to recurved or coiled, distal (40–)50–100% hairy, hairs to 4 mm; style glabrous, sometimes proximally glandular-pubescent (P. eriantherus). Capsules glabrous, rarely glandular-pubescent distally. Seeds brown, dark brown, or black, angled to reniform or slightly rounded, 1.5–4.8 mm.</description>
  <discussion>Species 31 (31 in the flora).</discussion>
  <description type="distribution">w North America, n Mexico.</description>
  <discussion>Members of sect. Cristati are mostly calciphiles on soils derived from limestone, dolomite, or other calcareous substrates. Species in sect. Cristati generally have hairy herbage, glandular-pubescent inflorescences and flowers, and staminodes prominently bearded more than half their lengths. Two species often assigned elsewhere (Penstemon acaulis, P. laricifolius) are included in this section based on molecular data (A. D. Wolfe et al. 2006). D. D. Keck’s (1938) sect. Aurator, which appears widely in the literature, is invalid, published without a Latin diagnosis.</discussion>
  <references>
    <reference>Keck, D. D. 1938. Studies in Penstemon VI. The section Aurator. Bull. Torrey Bot. Club 65: 233–255.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems to 1 cm.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Basal and proximal cauline leaves 6–15(–22) × 0.6–1.3(–1.5) mm, blades linear.</description>
      <determination>49. Penstemon acaulis</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Basal and proximal cauline leaves 15–25(–35) × 1.5–2.6(–4.5) mm, blades oblanceolate to linear.</description>
      <determination>79. Penstemon yampaënsis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems 2–65(–100) cm.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Corollas glabrous externally; calyx lobes glabrous.</description>
      <determination>69. Penstemon laricifolius</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Corollas glandular-pubescent externally; calyx lobes hairy.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Pollen sacs parallel, navicular.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Staminodes prominently exserted; basal and proximal cauline leaf margins sharply serrate-dentate, rarely entire; corollas ventricose.</description>
      <determination>76. Penstemon pinorum</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Staminodes included to barely exserted; basal and proximal cauline leaf margins entire or ± dentate; corollas funnelform or tubular-funnelform.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Thyrses 8–30 cm, interrupted; stems (25–)30–60 cm; Arizona.</description>
      <determination>58. Penstemon distans</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Thyrses 2–7(–8) cm, continuous or interrupted; stems 2–25 cm; Utah.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaves glabrous abaxially, retrorsely hairy adaxially, usually only along midveins on distal caulines; pollen sacs 1.3–1.6 mm.</description>
      <determination>62. Penstemon franklinii</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaves densely retrorsely hairy; pollen sacs 1.7–2 mm.</description>
      <determination>74. Penstemon nanus</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Pollen sacs opposite or divergent, explanate or subexplanate to navicular or navicular-sigmoid.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Thyrse axes puberulent or retrorsely hairy.</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Leaves: basal and proximal cauline blades oblanceolate to linear, 1–4 mm wide, caulines usually arcuate; Idaho.</description>
      <determination>77. Penstemon pumilus</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Leaves: basal and proximal cauline blades spatulate to oblanceolate, 2–14 mm wide, caulines not arcuate; Nevada, Utah.</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Cauline leaves 12–38 × 3–7 mm; stems 2–14(–20) cm, prostrate, decumbent, ascending, or erect; corollas with, rarely without, faint blue or reddish purple nectar guides; ec Nevada, wc Utah.</description>
      <determination>59. Penstemon dolius</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Cauline leaves 14–23 × 1–4 mm; stems 2–6(–8) cm, ascending to erect; corollas without nectar guides; ne Utah.</description>
      <determination>60. Penstemon duchesnensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Thyrse axes glandular-pubescent or glandular-villous, sometimes retrorsely hairy.</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Capsules sparsely glandular-pubescent distally; styles sparsely glandular-pubescent proximally or glabrous.</description>
      <determination>61. Penstemon eriantherus</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Capsules glabrous; styles glabrous.</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Pollen sacs navicular or navicular-sigmoid.</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Corollas ventricose, 19–30 mm; staminodes prominently exserted.</description>
      <determination>64. Penstemon gormanii</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Corollas tubular-funnelform, 10–22(–24) mm; staminodes included to exserted.</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Stems puberulent, hairs usually white, scalelike; leaves densely retrorsely hairy, hairs white, scalelike; California.</description>
      <determination>73. Penstemon monoensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Stems glabrous or retrorsely hairy, sometimes sparsely glandular-pubescent distally, hairs not white or scalelike; leaves glabrous, puberulent, or retrorsely hairy, hairs not white or scalelike; Colorado, New Mexico, Utah.</description>
      <next_statement_id>15</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Pollen sacs divergent, dehiscing incompletely; corollas weakly bilabiate; Colorado, New Mexico.</description>
      <determination>52. Penstemon auriberbis</determination>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Pollen sacs opposite, dehiscing completely; corollas bilabiate; Colorado, Utah.</description>
      <next_statement_id>16</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Basal and proximal cauline leaf blades oblanceolate to lanceolate; corollas 10–13 mm; styles 6–8 mm; Utah.</description>
      <determination>51. Penstemon atwoodii</determination>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Basal and proximal cauline leaf blades spatulate to oblanceolate or ovate; corollas 15–20(–22) mm; styles 10–12 mm; Colorado, Utah.</description>
      <determination>72. Penstemon moffatii</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Pollen sacs explanate, rarely subexplanate.</description>
      <next_statement_id>17</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Corollas tubular-funnelform to funnelform.</description>
      <next_statement_id>18</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Corollas white, sometimes tinged pink or lavender.</description>
      <next_statement_id>19</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Leaves glabrate or puberulent to scabrous, basals and proximals (4–)7–18(–20) mm wide, blades oblanceolate or obovate to lanceolate; thyrse axes densely glandular-pubescent; Alberta, Manitoba, Ontario, Saskatchewan, Colorado, Iowa, Kansas, Minnesota, Montana, Nebraska, New Mexico, North Dakota, Oklahoma, South Dakota, Texas, Wyoming.</description>
      <determination>50. Penstemon albidus</determination>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Leaves glabrous or glabrate, rarely basals retrorsely hairy along midveins, basals and proximals 1–6 mm wide, blades linear, rarely lanceolate; thyrse axes glandular-pubescent; Texas.</description>
      <determination>66. Penstemon guadalupensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Corollas blue, bluish lavender, lavender, pink, rose red, rose purple, violet, or purple.</description>
      <next_statement_id>20</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Corollas pink to rose red or rose purple; California, sw Nevada.</description>
      <determination>55. Penstemon calcareus</determination>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Corollas lavender, violet, purple, bluish lavender, or blue; e Nevada, Utah.</description>
      <next_statement_id>21</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Staminodes exserted; corollas bilabiate, tubular-funnelform.</description>
      <determination>57. Penstemon concinnus</determination>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Staminodes included; corollas weakly bilabiate, funnelform.</description>
      <next_statement_id>22</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>22</statement_id>
      <description type="morphology">Stems glabrous or slightly retrorsely hairy proximally, glandular-pubescent distally; basal and proximal cauline leaf blades lanceolate to linear; staminodes: distal 5–7 mm densely pilose, hairs yellow, to 0.8 mm.</description>
      <determination>63. Penstemon goodrichii</determination>
    </key_statement>
    <key_statement>
      <statement_id>22</statement_id>
      <description type="morphology">Stems retrorsely hairy; basal and proximal cauline leaf blades oblanceolate to lanceolate; staminodes: distal 3–5 mm sparsely to densely pubescent, hairs yellow, to 1 mm.</description>
      <determination>70. Penstemon marcusii</determination>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Corollas ventricose, ventricose-ampliate, or ampliate.</description>
      <next_statement_id>23</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>23</statement_id>
      <description type="morphology">Corollas 35–55 mm; staminodes 23–30 mm; styles 18–34 mm.</description>
      <determination>56. Penstemon cobaea</determination>
    </key_statement>
    <key_statement>
      <statement_id>23</statement_id>
      <description type="morphology">Corollas 10–35 mm; staminodes 7–20 mm; styles 7–24 mm.</description>
      <next_statement_id>24</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>24</statement_id>
      <description type="morphology">Corollas glandular-pubescent internally abaxially, sometimes also pilose.</description>
      <next_statement_id>25</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>25</statement_id>
      <description type="morphology">Corollas 14–20(–22) mm; styles 11–13 mm; capsules 6–11 mm.</description>
      <determination>75. Penstemon ophianthus</determination>
    </key_statement>
    <key_statement>
      <statement_id>25</statement_id>
      <description type="morphology">Corollas 22–35 mm; styles 17–24 mm; capsules (7–)10–16 mm.</description>
      <next_statement_id>26</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>26</statement_id>
      <description type="morphology">Staminodes exserted, distal 10–14 mm lanate, hairs to 3.5 mm; corolla throats abruptly inflated.</description>
      <determination>67. Penstemon jamesii</determination>
    </key_statement>
    <key_statement>
      <statement_id>26</statement_id>
      <description type="morphology">Staminodes included or reaching orifice, distal 8–12 mm sparsely to moderately pilose, hairs to 1 mm; corolla throats gradually inflated.</description>
      <determination>78. Penstemon triflorus</determination>
    </key_statement>
    <key_statement>
      <statement_id>24</statement_id>
      <description type="morphology">Corollas lanate or villous internally abaxially.</description>
      <next_statement_id>27</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>27</statement_id>
      <description type="morphology">Styles 15–24 mm; corollas 18–35 mm, tubes (6–)8–13 mm; staminodes 16–20 mm.</description>
      <next_statement_id>28</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>28</statement_id>
      <description type="morphology">Corollas 28–35 mm, ventricose-ampliate, sparsely white-lanate internally abaxially; Colorado, Utah.</description>
      <determination>65. Penstemon grahamii</determination>
    </key_statement>
    <key_statement>
      <statement_id>28</statement_id>
      <description type="morphology">Corollas 18–28 mm, ventricose, densely white-lanate internally abaxially; California, Idaho, Nevada, Oregon.</description>
      <determination>68. Penstemon janishiae</determination>
    </key_statement>
    <key_statement>
      <statement_id>27</statement_id>
      <description type="morphology">Styles 7–11 mm; corollas 10–22 mm, tubes 4–7 mm; staminodes 7–10 mm.</description>
      <next_statement_id>29</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>29</statement_id>
      <description type="morphology">Staminodes reaching orifices or exserted; corollas ampliate; leaves retrorsely hairy, sometimes glabrate; Arizona, Colorado, New Mexico, Utah.</description>
      <determination>54. Penstemon breviculus</determination>
    </key_statement>
    <key_statement>
      <statement_id>29</statement_id>
      <description type="morphology">Staminodes exserted or prominently exserted; corollas ventricose; leaves retrorsely hairy, glabrate, or glabrous; California, Idaho, Nevada, Oregon.</description>
      <next_statement_id>30</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>30</statement_id>
      <description type="morphology">Corollas 10–14 mm; styles 7–9 mm; capsules 3–6 mm; California, Nevada.</description>
      <determination>53. Penstemon barnebyi</determination>
    </key_statement>
    <key_statement>
      <statement_id>30</statement_id>
      <description type="morphology">Corollas 14–22 mm; styles 9–10 mm; capsules (6–)8–11 mm; Idaho, Oregon.</description>
      <determination>71. Penstemon miser</determination>
    </key_statement>
  </key>
</bio:treatment>
