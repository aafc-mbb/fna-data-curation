<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>Craig C. Freeman</author>
    </source>
    <other_info_on_meta type="treatment_page">270</other_info_on_meta>
    <other_info_on_meta type="mention_page">15</other_info_on_meta>
    <other_info_on_meta type="mention_page">271</other_info_on_meta>
    <other_info_on_meta type="mention_page">275</other_info_on_meta>
    <other_info_on_meta type="mention_page">279</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
    <other_info_on_meta type="illustrator">John Myers</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">PLANTAGINACEAE</taxon_name>
    <taxon_name rank="genus" authority="Nuttall" date="1834">LEUCOSPORA</taxon_name>
    <place_of_publication>
      <publication_title>J. Acad. Nat. Sci. Philadelphia</publication_title>
      <place_in_publication>7: 87. 1834</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus leucospora</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek leucos, white or clear, and spora, seed, alluding to transparency of matured seeds</other_info_on_name>
  </taxon_identification>
  <number>32.</number>
  <other_name type="common_name">Paleseed</other_name>
  <description type="morphology">Herbs, annual. Stems erect or ascending, glandular-villosulous. Leaves cauline, opposite or whorled; petiole present; blade not fleshy, not leathery, margins pinnatifid to bipinnatifid. Inflorescences axillary, flowers solitary; bracts absent. Pedicels present; bracteoles absent. Flowers bisexual; sepals 5, distinct, lanceolate to linear-lanceolate, calyx obscurely bilaterally symmetric, tubular; corolla pale blue to pale lavender, bilaterally symmetric, bilabiate, tubular, tube base not spurred or gibbous, throat not densely pilose internally, lobes 5, abaxial 3, adaxial 2; stamens 4, medially adnate to corolla, didynamous, filaments glabrous; staminode 0; ovary 2-locular, placentation axile; stigma cuneate. Fruits capsules, dehiscence septicidal. Seeds 50–100, white, parallel-ridged, ovoid, wings absent.</description>
  <discussion>Species 2 (1 in the flora).</discussion>
  <description type="distribution">c, e North America, n Mexico.</description>
  <discussion>The second species of Leucospora, as circumscribed here, is L. coahuilensis Henrickson, which is known from Chihuahua, Coahuila, Durango, and Zacatecas, Mexico. It differs from L. multifida in its longer corollas, shorter pedicels, longer and more slender capsules, and smaller seeds (J. Henrickson 1989).</discussion>
  <discussion>Based on a molecular phylogenetic study, D. Estes and R. L. Small (2008) suggested that Leucospora belongs to a clade that includes Bacopa, Gratiola, Mecardonia, Scoparia, and Stemodia, genera traditionally placed in Gratioleae. Relationships of some species, and the circumscriptions of some genera and the tribe itself, remain uncertain due to limited sampling. Leucospora was found to be sister to Stemodia verticillata (Miller) Hassler; Stemodia was paraphyletic, with the type species of that genus, S. maritima Linnaeus, included in a second, separate clade. B. L. Turner and C. C. Cowan (1993) included Leucospora in Stemodia based on morphological evidence.</discussion>
  <discussion>Schistophragma, another genus possibly allied with Leucospora, has not been examined molecularly; it shares some morphological similarities with Leucospora. On the basis of morphology, J. Henrickson (1989) argued that Schistophragma should be submerged in Leucospora. Pending more complete taxon sampling for Gratioleae, Leucospora is retained here in its traditional sense.</discussion>
</bio:treatment>
