<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="treatment_page">509</other_info_on_meta>
    <other_info_on_meta type="mention_page">457</other_info_on_meta>
    <other_info_on_meta type="mention_page">508</other_info_on_meta>
    <other_info_on_meta type="mention_page">510</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Ventenat" date="unknown">OROBANCHACEAE</taxon_name>
    <taxon_name rank="genus" authority="Loureiro" date="1790">STRIGA</taxon_name>
    <taxon_name rank="species" authority="(Willdenow) Vatke" date="1875">gesnerioides</taxon_name>
    <place_of_publication>
      <publication_title>Oesterr. Bot. Z.</publication_title>
      <place_in_publication>25: 11. 1875</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family orobanchaceae;genus striga;species gesnerioides</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Buchnera</taxon_name>
    <taxon_name rank="species" authority="Willdenow" date="1800">gesnerioides</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>3: 338. 1800</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus buchnera;species gesnerioides</taxon_hierarchy>
  </taxon_identification>
  <number>2.</number>
  <other_name type="common_name">Cowpea or tobacco witchweed</other_name>
  <description type="morphology">Annuals 15–30 cm; holoparasitic. Taproots stout, fleshy; secondary roots absent; haustoria single, large, globular. Flowering stems drying black or brown, simple or branched from base, obtusely square or terete, fleshy, puberulent, pilose, or glabrous. Leaves: blade opposite, appressed, lanceolate, scalelike, 3–7 x 2 mm, surfaces puberulent. Inflorescences spikes, lax, sometimes congested; flowers opposite, rarely alternate; bracts lanceolate, 5–7 x 1–2 mm, shorter than calyx, surfaces glabrous or puberulent. Flowers: sepals 5, 4–7 mm; calyx: tube 3–5 mm, ribs 5, scarious between ribs, teeth 5, unequal, subulate, 1–2 mm, adaxial shorter; corolla brownish red or purple, rarely white, sparsely pubescent or glabrous, tube bent, dilated distally above calyx, 8–12 mm, abaxial lobes 6 mm wide, adaxial 3–4 mm wide; style 5 mm, curved out, glabrous. Capsules oblong or ovoid, 4–5 x 2–3 mm. 2n = 40 (Nigeria).</description>
  <discussion>Worldwide, host-specific strains of Striga gesnerioides occur on Lepidagathis Willdenow (Acanthaceae), Ipomoea and Merremia (Convolvulaceae), Euphorbia (Euphorbiaceae), Indigofera, Tephrosia, and Vigna (Fabaceae), and Nicotiana (Solanaceae) (K. I. Mohamed et al. 2001); some of these may be potential hosts for this species in the flora area.</discussion>
  <discussion>Striga gesnerioides was discovered in Florida in 1979 as a parasite on Indigofera hirsuta (hairy indigo), an African species planted for phosphate mine reclamation. Alysicarpus ovalifolius (alyce clover) is also attacked in the field but with much less frequency (L. Herbaugh et al. 1980; L. J. Musselman et al. 1980). Striga gesnerioides is known from Citrus, Hillsborough, Lake, Orange, Polk, Seminole, and Volusia counties (http://www.plantatlas.usf.edu/). Greenhouse experiments conducted in Florida on over 125 known potential hosts of S. gesnerioides (Herbaugh et al.) showed that it poses little threat to American agriculture (Musselman et al.). In addition to hairy indigo and alyce clover, the only other hosts reported in the Florida study were Jacquemontia tamnifolia, Helianthus annuus (sunflower), and Ipomoea batatas (sweet potato) (Herbaugh et al.). In an experiment conducted by Musselman and C. Parker (1981) on more than 30 potential hosts, the American strain of S. gesnerioides failed to grow on any of them except hairy indigo, showing the strict specificity of this strain.</discussion>
  <description type="phenology">Flowering Aug–Oct.</description>
  <description type="habitat">heavily disturbed phosphate mines, mine reclamation sites.</description>
  <description type="elevation">0–50 m.</description>
  <description type="distribution">Introduced; Fla.; Asia (Arabian Peninsula); Africa.</description>
</bio:treatment>
