<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="treatment_page">119</other_info_on_meta>
    <other_info_on_meta type="mention_page">111</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">PLANTAGINACEAE</taxon_name>
    <taxon_name rank="genus" authority="Schmidel" date="1763">PENSTEMON</taxon_name>
    <taxon_name rank="subgenus" authority="unknown" date="unknown">Penstemon</taxon_name>
    <taxon_name rank="section" authority="Pennell" date="1920">Coerulei</taxon_name>
    <taxon_name rank="species" authority="S. Watson" date="1891">haydenii</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Gaz.</publication_title>
      <place_in_publication>16: 311. 1891</place_in_publication>
      <other_info_on_pub>(as Pentstemon haydeni)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family plantaginaceae;genus penstemon;subgenus penstemon;section coerulei;species haydenii;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
  </taxon_identification>
  <number>40.</number>
  <other_name type="common_name">Blowout beardtongue</other_name>
  <description type="morphology">Plants: caudex rhizomelike. Stems decumbent to ascending, (15–)20–48 cm, glabrous. Leaves essentially cauline, basal absent or reduced, glabrous; basal and proximal cauline (25–)55–130(–175) × 3–25 mm, blade linear to lanceolate, base tapered, apex acute to acuminate; cauline 2–10 pairs, sessile, 60–110(–120) × 7–30 mm, blade lanceolate to linear, base clasping, apex acuminate to long-acuminate. Thyrses continuous, cylindric, (2–)6–21(–34) cm, axis glabrous, verticillasters (2–)6–10(–17), cymes 1–8-flowered; proximal bracts ovate, 58–120 × 20–45 mm; peduncles and pedicels glabrous. Flowers: calyx lobes lanceolate to linear, 8–13 × 1–3 mm, margins entire or erose, herbaceous or scarious, glabrous; corolla lavender to bluish, usually with magenta nectar guides, ampliate, 21–28 mm, glabrous externally, glabrous internally, tube 7–9 mm, throat abruptly inflated, 9–11 mm diam., rounded abaxially; stamens included, pollen sacs opposite, 1.1–2 mm, sutures papillate; staminode 13–16 mm, included, 1.2–3 mm diam., tip recurved, distal 0.1–5 mm sparsely to densely villous, hairs golden yellow, to 1 mm, rarely glabrous; style 13–23 mm. Capsules 8–12 × 5–9 mm. 2n = 16.</description>
  <discussion>Penstemon haydenii is known from the Nebraska Sandhills, where extant populations occur in Box Butte, Cherry, Garden, Hooker, Morrill, and Sheridan counties. Historic populations occurred in Thomas County (D. M. Sutherland 1988).</discussion>
  <discussion>Penstemon haydenii was discovered in northern Carbon County, Wyoming, in the late 1990s some 300 km west of the nearest Nebraska populations; it might have been collected in Wyoming as early as 1877 (W. Fertig 2001). A morphometric analysis of Nebraska and Wyoming plants revealed differences that could justify recognition of Wyoming populations as a distinct variety (C. C. Freeman 2015). It is listed as endangered by the U.S. Department of the Interior.</discussion>
  <references>
    <reference>Fertig, W. 2001. Survey for Blowout Penstemon (Penstemon haydenii) in Wyoming. Laramie.</reference>
    <reference>Freeman, C. C. 2015. Final Report on an Assessment of the Status of Blowout Beardtongue (Penstemon haydenii S. Watson, Plantaginaceae) Using Molecular and Morphometric Approaches. Lawrence, Kans.</reference>
    <reference>Sutherland, D. M. 1988. Historical notes on collections and taxonomy of Penstemon haydenii S. Wats. (blowout penstemon), Nebraska’s only endemic plant species. Trans. Nebraska Acad. Sci. 16: 191–194.</reference>
  </references>
  <description type="phenology">Flowering May–Jul(–Sep).</description>
  <description type="habitat">Blowouts in sand dunes.</description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">900–2300 m.</description>
  <description type="distribution">Nebr., Wyo.</description>
</bio:treatment>
