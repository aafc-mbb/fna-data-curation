<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="treatment_page">395</other_info_on_meta>
    <other_info_on_meta type="mention_page">379</other_info_on_meta>
    <other_info_on_meta type="mention_page">396</other_info_on_meta>
    <other_info_on_meta type="volume">17</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Schauer" date="unknown">PHRYMACEAE</taxon_name>
    <taxon_name rank="genus" authority="Spach" date="1840">ERYTHRANTHE</taxon_name>
    <taxon_name rank="species" authority="(W. A. Weber) G. L. Nesom &amp; N. S. Fraga" date="2012">gemmipara</taxon_name>
    <place_of_publication>
      <publication_title>Phytoneuron</publication_title>
      <place_in_publication>2012-39: 37. 2012</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family phrymaceae;genus erythranthe;species gemmipara</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">C</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Mimulus</taxon_name>
    <taxon_name rank="species" authority="W. A. Weber" date="1972">gemmiparus</taxon_name>
    <place_of_publication>
      <publication_title>Madroño</publication_title>
      <place_in_publication>21: 423, fig. 1. 1972</place_in_publication>
      <other_info_on_pub>(as Minulus)</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus mimulus;species gemmiparus</taxon_hierarchy>
  </taxon_identification>
  <number>33.</number>
  <other_name type="common_name">Rocky Mountain or petiole-purse monkeyflower</other_name>
  <description type="morphology">Annuals, taprooted. Stems erect, straight at nodes, simple, 1–10 cm, glabrous. Leaves cauline; petiole 2–3 mm, laterally compressed, base deeply saccate, usually containing a lenticular propagule; blade emerging from bulbils, palmately veined, elliptic-ovate to ovate, 2–8(–10) × 2–5(–7) mm, base truncate to shallowly cordate, margins entire or remotely denticulate, apex obtuse to rounded, surfaces glabrous. Flowers herkogamous, (1 or)2–12, from medial or medial to distal nodes. Fruiting pedicels 4–6 mm, slightly longer than calyx, glabrous. Fruiting calyces strongly angled, subcampanulate, weakly inflated, 3–4 mm, margins distinctly toothed or lobed, glabrous, lobes pronounced, erect, incurved-triangular. Corollas yellow, palate yellow, not spotted or striped, bilaterally symmetric, weakly bilabiate; tube-throat broadly cylindric-funnelform, 3–4 mm, exserted beyond calyx margin; lobes subequal, oblong-obovate, throat open, palate puberulent, abaxial ridges low. Styles glabrous. Anthers included, glabrous. Capsules unknown. 2n = 16.</description>
  <discussion>Erythranthe gemmipara is known only from Grand, Jefferson, Larimer, and Park counties in north-central Colorado. Flowers in this species are uncommon, and seed set has not been observed in natural populations; reproduction in nature appears to be solely asexual via overwintering propagules (bulbils) formed in leaf axils. Two meristems are initiated in each axil. The proximal meristem produces a pair of starch-thickened storage leaves, a rudimentary axis, and a distal pair of preformed leaf primordia that enclose the shoot apical meristem. Root primordia are present within the first node of the bulbil. The petiole of the subtending leaf expands laterally and folds adaxially to enclose the developing bulbil, and entangled trichomes along the petiole margins secure it following leaf abscission and dispersal. The leaf blades commonly are deciduous, leaving the bulbil still attached (M. R. Beardsley 1997).</discussion>
  <references>
    <reference>Beardsley, M. R. 1997. Colorado’s Rare Endemic Plant, Mimulus gemmiparus, and Its Unique Mode of Reproduction. M.S. thesis. Colorado State University.</reference>
    <reference>Moody, A., P. K. Diggle, and D. A. Steingraeber. 1999. Developmental analysis of the evolutionary origin of vegetative propagules in Mimulus gemmiparus. Amer. J. Bot. 86: 1512–1522.</reference>
  </references>
  <description type="phenology">Flowering Jul–Aug(–Sep).</description>
  <description type="habitat">Granitic seeps, thin soils over bedrock cliff bases, crevices, ledges, talus, among rocks and boulders, Douglas fir, spruce-fir, and aspen forests.</description>
  <description type="conservation">Of conservation concern.</description>
  <description type="elevation">2600–3700 m.</description>
  <description type="distribution">Colo.</description>
</bio:treatment>
