<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <other_info_on_meta type="treatment_page">621</other_info_on_meta>
    <other_info_on_meta type="mention_page">619</other_info_on_meta>
    <other_info_on_meta type="mention_page">620</other_info_on_meta>
    <other_info_on_meta type="mention_page">622</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Brotherus" date="unknown">lembophyllaceae</taxon_name>
    <taxon_name rank="genus" authority="Bridel" date="unknown">isothecium</taxon_name>
    <taxon_name rank="species" authority="Bridel" date="1827">stoloniferum</taxon_name>
    <place_of_publication>
      <publication_title>Bryol. Univ.</publication_title>
      <place_in_publication>2: 371. 1827</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family lembophyllaceae;genus isothecium;species stoloniferum</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250099199</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Hypnum</taxon_name>
    <taxon_name rank="species" authority="Hooker" date="unknown">stoloniferum</taxon_name>
    <place_of_publication>
      <publication_title>Musci Exot.</publication_title>
      <place_in_publication>1: plate 74. 1818,</place_in_publication>
      <other_info_on_pub>not P. Beauvois 1805</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>genus hypnum;species stoloniferum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Isothecium</taxon_name>
    <taxon_name rank="species" authority="(Mitten) Macoun &amp; Kindberg" date="unknown">acuticuspis</taxon_name>
    <taxon_hierarchy>genus isothecium;species acuticuspis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">I.</taxon_name>
    <taxon_name rank="species" authority="Kindberg" date="unknown">brachycladon</taxon_name>
    <taxon_hierarchy>genus i.;species brachycladon</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">I.</taxon_name>
    <taxon_name rank="species" authority="Kindberg" date="unknown">myurellum</taxon_name>
    <taxon_hierarchy>genus i.;species myurellum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">I.</taxon_name>
    <taxon_name rank="species" authority="Kindberg" date="unknown">myuroides</taxon_name>
    <taxon_hierarchy>genus i.;species myuroides</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">I.</taxon_name>
    <taxon_name rank="species" authority="Kindberg" date="unknown">obtusatulum</taxon_name>
    <taxon_hierarchy>genus i.;species obtusatulum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">I.</taxon_name>
    <taxon_name rank="species" authority="(Mitten) Macoun &amp; Kindberg" date="unknown">spiculiferum</taxon_name>
    <taxon_hierarchy>genus i.;species spiculiferum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">I.</taxon_name>
    <taxon_name rank="species" authority="Kindberg" date="unknown">thamnioides</taxon_name>
    <taxon_hierarchy>genus i.;species thamnioides</taxon_hierarchy>
  </taxon_identification>
  <number>5.</number>
  <description type="morphology">Plants medium-sized, brownish green, dark green, pale green to golden green, occasionally bronze brown, dull to extremely glossy. Stems with stipe short, secondary stems 2–9 cm, julaceous, often acutely pinnate, sometimes irregularly branched, branches curving downward, branches of secondary system blunt, flagelliform branches sometimes present, to 5 cm, with smaller, more distant leaves with alar cells weakly differentiated; pseudoparaphyllia foliose, blunt or deltoid, blunt or toothed. Primary stem leaves broadly triangular; margins nearly entire; apex attenuate; ecostate or costa present; alar cells differentiated, region small, to 1/10 leaf length. Branchlet leaves lanceolate, ovate-lanceolate, or ovate; margins coarsely toothed throughout; apex attenuate; costa single, often ending in spine; alar cells isodiametric, except at insertion where rectangular, region often excavate, confined to base near margin just distal to insertion. Seta 1–1.5 cm. Capsule 1.5–2 mm.</description>
  <description type="habitat">Epiphytic on tree trunks, branches, shrubs, exposed or shaded rock cliffs, boulder slopes, forests, open sites</description>
  <description type="elevation">low to moderate elevations</description>
  <description type="distribution">Alta., B.C.; Alaska, Calif., Idaho, Mont., Oreg.</description>
  <discussion>Isothecium stoloniferum is an extremely variable species strongly related to the less variable I. myosuroides. Isothecium stoloniferum differs in its genetic composition (K. Ryall et al. 2005). There is a morphotype of I. stoloniferum that resembles I. alopecuroides, but the leaves are not cucullate and the apex is acute and strongly toothed. This morphotype appears to be most frequent on boulders or outcrops in and beside streams, often in well-drained but humid sites.</discussion>
  <discussion>The predominant morphotype of Isothecium stoloniferum has pinnate, acute-branched plants, sometimes with a stipe emerging from the primary system or proliferating from the secondary system, and can appear dendroid, especially when humid. The shoots are not julaceous, and flagelliform branches are infrequent. This morphotype tends to produce short stolons adventitiously usually near the base of the stipe. Another morphotype mimics I. myosuroides and is often impossible to distinguish; it is commonly epiphytic and tends to produce flagelliform branches; some populations can be composed entirely of flagelliform branches. Although the alar cells are less pronounced in this morphotype, the long-acuminate, strongly toothed leaves are characteristic.</discussion>
</bio:treatment>
