<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <author>Wilfred B. Schofield†,William R. Buck,Robert R. Ireland Jr.</author>
      <date>unknown</date>
    </source>
    <other_info_on_meta type="treatment_page">515</other_info_on_meta>
    <other_info_on_meta type="mention_page">282</other_info_on_meta>
    <other_info_on_meta type="mention_page">338</other_info_on_meta>
    <other_info_on_meta type="mention_page">484</other_info_on_meta>
    <other_info_on_meta type="mention_page">516</other_info_on_meta>
    <other_info_on_meta type="mention_page">522</other_info_on_meta>
    <other_info_on_meta type="mention_page">524</other_info_on_meta>
    <other_info_on_meta type="mention_page">554</other_info_on_meta>
    <other_info_on_meta type="mention_page">574</other_info_on_meta>
    <other_info_on_meta type="mention_page">578</other_info_on_meta>
    <other_info_on_meta type="mention_page">581</other_info_on_meta>
    <other_info_on_meta type="mention_page">611</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
    <other_info_on_meta type="illustrator">Patricia M. Eckel</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Schimper" date="unknown">HYPNACEAE</taxon_name>
    <taxon_hierarchy>family HYPNACEAE</taxon_hierarchy>
    <other_info_on_name type="fna_id">10437</other_info_on_name>
  </taxon_identification>
  <number>72.</number>
  <description type="morphology">Plants small to large, lax to dense, in tufts or mats, dark green, yellow-green, golden green, or orange, sometimes reddish brown or nearly black, often lustrous. Stems creeping, suberect, or erect, occasionally complanate-foliate or julaceous, irregularly branched to regularly 1- or 2-pinnate; hyalodermis 1-stratose or absent, central strand present or absent; pseudoparaphyllia filamentous, foliose, or absent; rhizoids often in clusters proximal to juncture of leaves on adaxial surface of stems and branches, smooth or papillose; axillary hair basal cells 1–2(–4), short, brown, distal cells 1–several, elongate, hyaline. Stem and branch leaves similar or less commonly differentiated, straight to homomallous, often falcate-secund, usually ovate-lanceolate, often asymmetric, sometimes linear, lanceolate, or triangular, occasionally plicate; base sometimes decurrent; margins often plane, occasionally recurved proximally or throughout, entire or toothed; apex obtuse to acuminate; costa double, short, to obscure or ecostate; alar cells usually differentiated, often quadrate to short-rectangular, sometimes enlarged and inflated, pigmented or similar in color to other cells, excavate or plane; medial and distal laminal cells linear, hexagonal, or elongate-sinuate, smooth, sometimes prorulose at distal ends on abaxial surface. Specialized asexual reproduction sometimes by leafy propagula or filamentous gemmae clustered in leaf axils. Sexual condition autoicous, dioicous, or phyllodioicous. Seta elongate, smooth (occasionally scabrous near capsule in Ctenidium). Capsule inclined, horizontal, or sometimes erect, cylindric or ovoid, usually smooth, often constricted below mouth when dry and empty; operculum conic or rostrate; peristome usually double; exostome teeth 16, external surface cross striate basally, papillose distally, sometimes bordered, internal surface often trabeculate; endostome usually free, sometimes fused to exostome, basal membrane high or rarely low, segments 16, cilia 1–3, nodose, rarely rudimentary or absent. Calyptra cucullate, smooth (weakly prorulose distally in Ctenidium), naked or rarely hairy. Spores spheric to ovoid, usually finely papillose, rarely smooth.</description>
  <description type="distribution">Nearly worldwide; most diverse in subtropics and tropics.</description>
  <discussion>Genera 60, species ca. 600 (19 genera, 62 species in the flora).</discussion>
  <discussion>Hypnaceae are taxonomically problematic; the family once held a high proportion of pleurocarpous species, but as genera are monographed, they are often placed in other families. The distinctive hypnoid peristome has been used as a significant feature, but this is not entirely reliable.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems with hyalodermis present, sometimes indistinct</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stems with hyalodermis absent</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Laminal cells minutely granular papillose; plants large; leaves undulate.</description>
      <determination>2 Buckiella</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Laminal cells smooth or rarely minutely prorulose; plants small to medium-sized; leaves not undulate</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Pseudoparaphyllia present, filamentous to foliose.</description>
      <determination>10 Hypnum</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Pseudoparaphyllia absent (present in Herzogiella adscendens)</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf margins serrulate to serrate (serrulate to entire in H. adscendens); asexual propagula absent.</description>
      <determination>8 Herzogiella</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf margins entire or minutely serrulate; asexual propagula sometimes present.</description>
      <determination>11 Isopterygiopsis</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Laminal cells with abaxial surface prominently prorulose</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Laminal cells smooth or prorulose</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Laminal cells prorulose at distal and sometimes proximal ends; leaves straight.</description>
      <determination>4 Chryso-hypnum</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Laminal cells prorulose at distal ends; leaves falcate.</description>
      <determination>5 Ctenidium</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Basal row of laminal cells with 1 large prorula at proximal end on abaxial surface.</description>
      <determination>6 Dacryophyllum</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Basal row of laminal cells smooth</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaf apices bluntly obtuse to broadly acute.</description>
      <determination>1 Bryocrumia</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaf apices acute to acuminate, rarely subobtuse</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Lateral and dorsal leaf shape strongly differentiated</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Lateral and dorsal leaf shape not strongly differentiated</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Laminal cell walls thick; medial cells linear.</description>
      <determination>7 Gollania</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Laminal cell walls thin; medial cells at least of lateral leaves relatively short.</description>
      <determination>19 Vesicularia</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Brood branchlets present at branch apices.</description>
      <determination>14 Platygyrium</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Brood branchlets absent at branch apices</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Pseudoparaphyllia filamentous, 1 (or 2)-seriate at base.</description>
      <determination>12 Isopterygium</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Pseudoparaphyllia foliose or absent</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Distal laminal cells ca. 4-6:1.</description>
      <determination>9 Homomallium</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Distal laminal cells usually longer than 6:1</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Leaves lanceolate to ovate-lanceolate, usually widest at base; alar cells not or poorly differentiated.</description>
      <determination>13 Orthothecium</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Leaves lanceolate to ovate, widest beyond base; alar cells mostly differentiated</description>
      <next_statement_id>15</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Plants large; stems suberect to ascending, pinnate, forming fronds; leaves strongly plicate.</description>
      <determination>16 Ptilium</determination>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Plants usually small or medium-sized, if large, not forming fronds; stems creeping, irregularly branched or sometimes regularly pinnate; leaves not to weakly plicate</description>
      <next_statement_id>16</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Asexual reproductive bodies present in leaf axils, occasionally stem apices.</description>
      <determination>15 Pseudotaxiphyllum</determination>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Asexual reproductive bodies absent</description>
      <next_statement_id>17</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Branches usually strongly curled when dry; capsules erect to suberect, straight; exostome teeth smooth basally.</description>
      <determination>17 Pylaisia</determination>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Branches not curled when dry; capsules erect to cernuous, often curved; exostome teeth usually cross striolate basally</description>
      <next_statement_id>18</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Stems somewhat complanate-foliate; leaf margins entire; capsules suberect or somewhat inclined, not or weakly curved.</description>
      <determination>3 Callicladium</determination>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Stems not complanate-foliate or if so, leaf margins serrulate to serrate; capsules inclined, usually curved</description>
      <next_statement_id>19</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Stems pinnate or irregularly branched, rarely subjulaceous; leaves usually spreading.</description>
      <determination>10 Hypnum</determination>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Stems simple or sparingly and irregularly branched, sometimes julaceous or subjulaceous; leaves complanate.</description>
      <determination>18 Taxiphyllum</determination>
    </key_statement>
  </key>
</bio:treatment>
