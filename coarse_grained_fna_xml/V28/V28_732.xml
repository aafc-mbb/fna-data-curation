<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <author>William R. Buck,Robert R. Ireland Jr.</author>
      <date>unknown</date>
    </source>
    <other_info_on_meta type="treatment_page">469</other_info_on_meta>
    <other_info_on_meta type="mention_page">473</other_info_on_meta>
    <other_info_on_meta type="volume">28</other_info_on_meta>
    <other_info_on_meta type="illustrator">Patricia M. Eckel</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="W. R. Buck &amp; Ireland" date="unknown">STEREOPHYLLACEAE</taxon_name>
    <taxon_hierarchy>family STEREOPHYLLACEAE</taxon_hierarchy>
    <other_info_on_name type="fna_id">20417</other_info_on_name>
  </taxon_identification>
  <number>63.</number>
  <description type="morphology">Plants medium-sized [to moderately large], in thin to dense, flat mats, often glossy. Stems creeping, simple or sparingly and irregularly branched, complanate-foliate to julaceous; cortical cells small, walls thick; pseudoparaphyllia filamentous [sometimes foliose]; rhizoids in clusters below leaf insertion on abaxial stem surface, smooth [or papillose]; axillary hair basal cell 1, brown, apical cells 3–6, hyaline. Leaves erect to wide-spreading, usually imbricate when dry, lanceolate to ovate, symmetric or rarely asymmetric, flat to somewhat concave; base not decurrent; margins plane or incurved, entire to serrulate distally; apex obtuse to long-acuminate; costa single, 1/3–3/4 leaf length, or rarely ecostate; alar cells differentiated, quadrate, rectangular, or oblate, collenchymatous, regions usually large, often unequally distributed on either side of costa, always covering adaxial costa surface; distal laminal cells linear to rhomboidal, smooth, rarely 1-papillose over lumina or prorulose at distal ends on abaxial surface, walls thin or thick, not porose. Specialized asexual reproduction unknown. Sexual condition autoicous [rarely dioicous], often producing sporophytes; perigonia scattered along stem, leaves ovate. Seta single, long, straight to somewhat flexuose, smooth. Capsule cernuous or erect, straight or slightly curved, contracted below mouth when dry, smooth; exothecial cell walls thin or thick; annulus differentiated and deciduous, or undifferentiated; operculum conic to short-rostrate; peristome double; exostome teeth bordered, external surface cross striolate proximally, papillose distally, internal surface ± projecting; endostome basal membrane high or low, segments keeled, perforate, shorter than or almost as long as exostome teeth, cilia present or absent. Calyptra cucullate, smooth, naked. Spores spheric to ovoid, minutely papillose.</description>
  <description type="distribution">s United States, Mexico, West Indies, Central America, South America, Asia, Africa, Pacific Islands, Australia.</description>
  <discussion>Genera 8, species ca. 28 (3 genera, 3 species in the flora).</discussion>
  <discussion>There are two subfamilies recognized for Stereophyllaceae. Plants of subfam. Stereophylloideae M. Fleischer have similar stem and branch leaves; strong costae to 1/3–3/4 leaf length on all leaves; cernuous or erect capsules; exothecial cells not or scarcely collenchymatous; and spores 9–27 µm. Plants of subfam. Pilosioideae W. R. Buck &amp; Ireland have differentiated stem and branch leaves; weak costae, rarely to 1/3 leaf length, only on lateral leaves; cernuous capsules; exothecial cells collenchymatous; and spores 7–9 µm.</discussion>
  <references>
    <reference>Buck, W. R. and R. R. Ireland. 1985. A reclassification of the Plagiotheciaceae. Nova Hedwigia 41: 89–125.</reference>
    <reference>Grout, A. J. 1945. A revision of the North American species of Stereophyllum and Pilosium, with descriptions of some South American species. Bryologist 48: 60–70.</reference>
    <reference>Ireland, R. R. and W. R. Buck. 1994. Stereophyllaceae. In: Organization for Flora Neotropica. 1968+. Flora Neotropica. 109+ nos. New York. No. 65.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stem and branch leaves differentiated; costae rarely to 1/3 leaf length, only on lateral leaves; capsules cernuous; exothecial cell walls collenchymatous; spores 7-9 µm.</description>
      <determination>3 Pilosium</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stem and branch leaves similar; costae 1/3 -3/4 leaf length, on all leaves; capsules cernuous or erect; exothecial cell walls thin; spores 12-27 µm</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf apices abruptly acute to obtuse; distal laminal cells quadrate to rhomboidal, 1-papillose over lumina or sometimes smooth.</description>
      <determination>1 Stereophyllum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf apices acuminate, rarely acute or obtuse; distal laminal cells broadly fusiform, smooth or occasionally prorulose on abaxial surface.</description>
      <determination>2 Entodontopsis</determination>
    </key_statement>
  </key>
</bio:treatment>
