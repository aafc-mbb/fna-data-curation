<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="treatment_page">446</other_info_on_meta>
    <other_info_on_meta type="mention_page">444</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Berchtold &amp; J. Presl" date="unknown">CORNACEAE</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">CORNUS</taxon_name>
    <taxon_name rank="subgenus" authority="(Endlicher) Reichenbach" date="1841">Arctocrania</taxon_name>
    <place_of_publication>
      <publication_title>Deut. Bot. Herb.-Buch.,</publication_title>
      <place_in_publication>143. 1841</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cornaceae;genus cornus;subgenus arctocrania</taxon_hierarchy>
    <other_info_on_name type="fna_id">316357</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name rank="unranked" authority="Arctocrania Endlicher" date=" 1839">Cornus</taxon_name>
    <place_of_publication>
      <publication_title>Gen. Pl.</publication_title>
      <place_in_publication>10: 798. 1839</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus cornus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="(Endlicher) Nakai" date="unknown">Arctocrania</taxon_name>
    <taxon_hierarchy>genus arctocrania</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="Hill" date="unknown">Chamaepericlymenum</taxon_name>
    <taxon_hierarchy>genus chamaepericlymenum</taxon_hierarchy>
  </taxon_identification>
  <number>1c.</number>
  <description type="morphology">Herbs, perennial; rhizomes present. Branches and leaves opposite, leaves at distalmost node appearing whorled in some species. Inflorescences congested cymes; bracts 4, well developed, petaloid, subtending inflorescence. Pedicels present. Drupes distinct; stone apex rounded.</description>
  <discussion>Species definitions within subg. Arctocrania have been controversial, reflecting an apparently complicated history of post-glacial range shifts, hybridization, and polyploidization in these long-lived clonal plants (Z. E. Murrell 1994). Many treatments (for example, E. Hultén 1937; J. A. Calder and R. L. Taylor 1965) had broad concepts of Cornus suecica and especially C. canadensis, and considered the intermediates to be hybrids, sometimes calling them C. ×intermedia (Farr) Calder &amp; Roy L. Taylor. J. F. Bain and K. E. Denford (1979) recognized C. unalaschkensis as a tetraploid species derived from hybridization between C. canadensis and C. suecica and the remaining intermediates as C. canadensis ×C. suecica. C. Gervais and M. Blondeau (2003) applied the name C. ×lapagei Gervais &amp; Blondeau to these hybrids. A morphometric analysis by Murrell showed five morphological groups: the morphological extremes, C. canadensis and C. suecica; the tetraploid intermediate species, C. unalaschkensis; and two groups of intermediates that were considered introgressive hybrids between C. canadensis and C. suecica (or perhaps rarely between one of these species and C. unalaschkensis); that treatment is followed here.</discussion>
  <discussion>Species 3 (3 in the flora).</discussion>
  <description type="distribution">North America, Eurasia.</description>
  <discussion>Identification of many specimens may be difficult because of the relatively high frequency of introgressants (approximately half of specimens examined by Z. E. Murrell 1994). Some intermediates are found in places where Cornus canadensis and C. suecica currently occur somewhat close together in Alaska, British Colombia, Greenland, Newfoundland and Labrador, Northwest Territories, Nova Scotia, Quebec, and St. Pierre and Miquelon. However, plants that mostly resemble C. canadensis but have petals that are purple on their distal third or chlorophyllous leaves at the second node from the apex (and non-chlorophyllous scale leaves at the third node), and sometimes only opposite leaves, are found at scattered locations throughout the range of C. canadensis but far from the current range of C. suecica, apparently reflecting the presence of the latter in the past (Murrell).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals cream; leaves at 2d node from apex non-chlorophyllous, scalelike; sepals cream, membranous, apices rounded.</description>
      <determination>4. Cornus canadensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals purple or cream proximally, purple distally; leaves at 2d node from apex chlorophyllous or non-chlorophyllous proximally and chlorophyllous distally, well developed; sepals purple or mottled purple and cream, thick, apices rounded or acute.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Petals purple; hypanthium very sparsely hairy; distalmost leaves similar size to those at 2 more proximal nodes.</description>
      <determination>5. Cornus suecica</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Petals cream proximally, purple distally; hypanthium densely hairy; distalmost leaves much larger than those at 2 more proximal nodes.</description>
      <determination>6. Cornus unalaschkensis</determination>
    </key_statement>
  </key>
</bio:treatment>
