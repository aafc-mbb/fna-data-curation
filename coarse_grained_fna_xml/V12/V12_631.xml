<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="treatment_page">452</other_info_on_meta>
    <other_info_on_meta type="mention_page">451</other_info_on_meta>
    <other_info_on_meta type="mention_page">453</other_info_on_meta>
    <other_info_on_meta type="mention_page">456</other_info_on_meta>
    <other_info_on_meta type="mention_page">457</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Berchtold &amp; J. Presl" date="unknown">CORNACEAE</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">CORNUS</taxon_name>
    <taxon_name rank="subgenus" authority="(Dumortier) C. K. Schneider" date="1909">Thelycrania</taxon_name>
    <taxon_name rank="species" authority="Rafinesque" date="1819">obliqua</taxon_name>
    <place_of_publication>
      <publication_title>W. Rev. &amp; Misc. Mag.</publication_title>
      <place_in_publication>1: 229. 1819</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family cornaceae;genus cornus;subgenus thelycrania;species obliqua;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250084382</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Cornus</taxon_name>
    <taxon_name rank="species" authority="Miller" date="unknown">amomum</taxon_name>
    <taxon_name rank="subspecies" authority="(Rafinesque) J. S. Wilson" date="unknown">obliqua</taxon_name>
    <taxon_hierarchy>genus cornus;species amomum;subspecies obliqua</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">C.</taxon_name>
    <taxon_name rank="species" authority="unknown" date="unknown">amomum</taxon_name>
    <taxon_name rank="variety" authority="(C. A. Meyer) Rickett" date="unknown">schuetzeana</taxon_name>
    <taxon_hierarchy>genus c.;species amomum;variety schuetzeana</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">C.</taxon_name>
    <taxon_name rank="species" authority="Koehne" date="unknown">purpusii</taxon_name>
    <taxon_hierarchy>genus c.;species purpusii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Swida</taxon_name>
    <taxon_name rank="species" authority="(Koehne) A. Heller" date="unknown">purpusii</taxon_name>
    <taxon_hierarchy>genus swida;species purpusii</taxon_hierarchy>
  </taxon_identification>
  <number>10.</number>
  <other_name type="common_name">Pale dogwood</other_name>
  <other_name type="common_name">cornouiller oblique</other_name>
  <description type="morphology">Shrubs, to 5 m, flowering at 1.5 m; rhizomes absent. Stems clustered, branches occasionally arching to ground and rooting at nodes; bark green-tan or maroon-tan, not corky, appearing braided, splitting longitudinally; branchlets green abaxially, maroon to green adaxially, turning red-maroon in fall, densely erect-hairy when young; lenticels not protruding on 2d year branches, area surrounding them not suffused with purple on older branches; pith tan or brown. Leaves: petiole 6–20 mm; blade lanceolate to narrowly elliptic, 4–12 × 1–5 cm, base cuneate, apex acuminate, abaxial surface pale whitish yellow, hairs white, all appressed and rigid, tufts of hairs absent in axils of secondary veins, midvein and secondary veins densely tomentose, adaxial surface dark green, hairs appressed; secondary veins (4–)5–6 per side, evenly spaced, tertiary veins not prominent. Inflorescences flat-topped or convex, 2–7 cm diam., peduncle 20–70 mm; branches and pedicels green or greenish yellow, turning maroon in fruit. Flowers: hypanthium densely appressed-hairy, especially at base; sepals 1–2.3 mm; petals cream, 3.8–5 mm. Drupes blue, portion in direct sunlight bleached white, globose, 5–9 mm diam.; stone globose, 4–6 mm diam., irregularly longitudinally ridged, apex pointed. 2n = 22</description>
  <discussion>H. W. Rickett (1934) argued that the description of Cornus obliqua by Rafinesque is inadequate to associate that name with a species; that assessment is not accepted here. The description by Rafinesque of the plants having reddish brown, slightly rugose bark, narrowly elliptic to lanceolate discs, and whitish yellow abaxial leaf surfaces, along with the cited locality of the Kentucky River, clearly delineates this species. Rafinesque was the first to divide the blue-fruited dogwood of L. Plukenet (1691–1705, part 4) into two species.</discussion>
  <discussion>Cornus obliqua and C. amomum can be distinguished not only by the differences included in the key above, but also by their abaxial leaf cuticle, which is coronulate in C. obliqua but not in C. amomum, but seeing this character requires high magnification. However, in much of the area where they are sympatric (Connecticut, District of Columbia, Maine, Maryland, Massachusetts, New Hampshire, New York, Tennessee, and Vermont), many individuals show intermediate leaf blade abaxial surface and hair morphology. They are detected by having both the whitish leaf blade abaxial surface and appressed hairs of C. obliqua with occasional scattered erect, often tan to brown hairs, similar to those typical for C. amomum. J. S. Wilson (1964) concluded that differences in leaf surface and hair morphology are environmentally based (sun versus shade), whereas work by Z. E. Murrell (1992) documented a geographical basis for the differences. The geographical zone of intermediacy needs greater scrutiny to determine its full extent and whether the intermediacy of many plants represent hybridization or incomplete speciation.</discussion>
  <discussion>A putative hybrid between Cornus obliqua and C. racemosa, reported from Massachusetts, Ohio, and Pennsylvania, has been called C. ×arnoldiana Rehder [= Swida arnoldiana (Rehder) Soják].</discussion>
  <description type="phenology">Flowering May–Aug; fruiting Aug–Oct.</description>
  <description type="habitat">Alluvial woods, river and stream banks, wet meadows, marshes, ditches.</description>
  <description type="elevation">0–1500 m.</description>
  <description type="distribution">N.B., Ont., Que.; Ark., Conn., D.C., Ill., Ind., Iowa, Kans., Ky., Maine, Md., Mass., Mich., Minn., Mo., Nebr., N.H., N.J., N.Y., N.Dak., Ohio, Okla., Pa., R.I., S.Dak., Tenn., Vt., Va., W.Va., Wis.</description>
</bio:treatment>
