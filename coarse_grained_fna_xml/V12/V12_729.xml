<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>Guy L. Nesom</author>
    </source>
    <other_info_on_meta type="treatment_page">70</other_info_on_meta>
    <other_info_on_meta type="mention_page">43</other_info_on_meta>
    <other_info_on_meta type="mention_page">44</other_info_on_meta>
    <other_info_on_meta type="mention_page">45</other_info_on_meta>
    <other_info_on_meta type="mention_page">62</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">RHAMNACEAE</taxon_name>
    <taxon_name rank="genus" authority="Miller" date="1754">ZIZIPHUS</taxon_name>
    <place_of_publication>
      <publication_title>Gard. Dict. Abr. ed.</publication_title>
      <place_in_publication>4, vol. 3. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rhamnaceae;genus ziziphus</taxon_hierarchy>
    <other_info_on_name type="etymology">Latinized Arabic vernacular name zizouf for common jujube, Z. jujuba</other_info_on_name>
    <other_info_on_name type="fna_id">135333</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="Cavanilles" date="unknown">Condalia</taxon_name>
    <taxon_name rank="subgenus" authority="Weberbauer" date="unknown">Condaliopsis</taxon_name>
    <taxon_hierarchy>genus condalia;subgenus condaliopsis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="(Weberbauer) Suessenguth" date="unknown">Condaliopsis</taxon_name>
    <taxon_hierarchy>genus condaliopsis</taxon_hierarchy>
  </taxon_identification>
  <number>11.</number>
  <other_name type="common_name">Jujube</other_name>
  <description type="morphology">Shrubs or small trees, armed with thorns or stipular spines [unarmed]; bud scales present or absent. Leaves deciduous or persistent, alternate, sometimes fascicled on short shoots; blade not gland-dotted; pinnately veined (obscurely, appearing 1-veined) or 3[–5]-veined from base (acrodromous), secondary veins absent or poorly developed distal to basal veins, except sometimes near apex. Inflorescences axillary [terminal], within foliage, corymblike cymes, thyrses, or fascicles, or rarely flowers solitary; peduncles and pedicels not fleshy in fruit. Pedicels present. Flowers bisexual; hypanthium shallowly cupulate to hemispheric, 1–3 mm wide; sepals 5, spreading, greenish, greenish white, yellowish, yellow-green, orangish, or purplish, ovate-triangular to triangular, keeled adaxially; petals [0–]5, yellow to pale yellow or white, ± flat, obovate or spatulate, clawed; nectary fleshy, filling hypanthium, 5–10-lobed; stamens 5; ovary superior, 2–3(–4)-locular; styles (1–)2–3(–4), connate basally to proximally. Fruits drupes; stone 1. x = 12.</description>
  <discussion>Species ca. 170 (6 in the flora).</discussion>
  <description type="distribution">United States, Mexico, Central America, South America, Asia, Africa.</description>
  <discussion>The molecular and morphological study by M. B. Islam and M. P. Simmons (2006) corroborates earlier observations (for example, M. C. Johnston 1963) that Ziziphus comprises two clades, with the Old World species of Ziziphus more closely related to Paliurus than to the New World species. The New World species have paired or unpaired thorns, tangential to diagonal bands of axial parenchyma, and deciduous, non-spinose stipules. Old World Ziziphus and Paliurus do not have thorns tangential to diagonal bands of axial parenchyma but do have stipular spines. The Islam and Simmons study found the Florida endemic Z. celata to be more closely related to Berchemia and Rhamnus than to either group of Ziziphus. Further study by M. B. Islam and R. P. Guralnick (2015) showed Z. celata and Z. parryi to be sister species that are most closely related to the genus Condalia. Maintaining monophyletic genera will require reclassification of Ziziphus as treated here.</discussion>
  <discussion>Ziziphus jujuba and Z. mauritiana are grown commercially for fruits in their native or introduced countries; fruits also are harvested in the native or naturalized regions (O. P. Pareek 2001).</discussion>
  <references>
    <reference>Islam, M. B. and M. P. Simmons. 2006. A thorny dilemma: Testing alternative intrageneric classifications within Ziziphus (Rhamnaceae). Syst. Bot. 31: 826–842.</reference>
    <reference>Islam, M. B. and R. P. Guralnick. 2015. Generic placement of the former Condaliopsis (Rhamnaceae) species. Phytotaxa 236: 25–39.</reference>
    <reference>Johnston, M. C. 1963. The species of Ziziphus indigenous to United States and Mexico. Amer. J. Bot. 50: 1020–1027.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades 3-veined from base; secondary branches not thorn-tipped, axillary thorns absent; stipular spines usually present, sometimes absent.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Secondary branches and leaf blade abaxial surfaces glabrous; stipular spines 15–40 mm.</description>
      <determination>5. Ziziphus jujuba</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Secondary branches and leaf blade abaxial surfaces tomentose; stipular spines 2–3 mm.</description>
      <determination>6. Ziziphus mauritiana</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades 1-veined from base; secondary branches thorn-tipped and with axillary thorns; stipular spines absent.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Inflorescences usually pedunculate thyrses, rarely flowers solitary; secondary branches gray-green to white, pruinose; drupes 5–10 mm.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Secondary branches usually glabrous, sometimes sparsely pilose; leaf blades subcoriaceous, surfaces usually glabrous; hypanthia moderately to densely strigose, hairs loose, curved; inflorescences (1–)2–6-flowered, peduncles (0.5–)1–2 mm, nearly equaling or shorter than pedicels.</description>
      <determination>1. Ziziphus obtusifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Secondary branches minutely hirtellous to short-villous, usually densely so, glabrescent; leaf blades relatively thin-herbaceous, surfaces persistently hirtellous to short-villous; hypanthia densely and persistently hirtellous; inflorescences (5–)10–30-flowered; peduncles 2–4 mm, equaling or longer than pedicels.</description>
      <determination>2. Ziziphus divaricata</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Inflorescences usually fascicles, rarely flowers solitary; secondary branches gray or pale greenish yellow to purplish, not pruinose; drupes 10–20(–25) mm.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaf blades 1–2.5(–3) cm; hypanthia and sepals purplish to greenish; drupes brownish to orange or purplish brown; California.</description>
      <determination>3. Ziziphus parryi</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaf blades 0.5–1 cm; hypanthia and sepals greenish; drupes yellow, orange, or brownish; Florida.</description>
      <determination>4. Ziziphus celata</determination>
    </key_statement>
  </key>
</bio:treatment>
