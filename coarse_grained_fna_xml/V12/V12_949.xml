<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="treatment_page">189</other_info_on_meta>
    <other_info_on_meta type="mention_page">185</other_info_on_meta>
    <other_info_on_meta type="mention_page">186</other_info_on_meta>
    <other_info_on_meta type="mention_page">187</other_info_on_meta>
    <other_info_on_meta type="mention_page">188</other_info_on_meta>
    <other_info_on_meta type="illustration_page">190</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">EUPHORBIACEAE</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">TRAGIA</taxon_name>
    <taxon_name rank="species" authority="Torrey" date="1827">ramosa</taxon_name>
    <place_of_publication>
      <publication_title>Ann. Lyceum Nat. Hist. New York</publication_title>
      <place_in_publication>2: 245. 1827</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus tragia;species ramosa</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242417369</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Tragia</taxon_name>
    <taxon_name rank="species" authority="Nuttall" date="unknown">angustifolia</taxon_name>
    <taxon_hierarchy>genus tragia;species angustifolia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">T.</taxon_name>
    <taxon_name rank="species" authority="Cavanilles" date="unknown">nepetifolia</taxon_name>
    <taxon_name rank="variety" authority="(Müller Arg.) Müller Arg." date="unknown">angustifolia</taxon_name>
    <taxon_hierarchy>genus t.;species nepetifolia;variety angustifolia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">T.</taxon_name>
    <taxon_name rank="species" authority="unknown" date="unknown">nepetifolia</taxon_name>
    <taxon_name rank="variety" authority="(Torrey) Müller Arg." date="unknown">ramosa</taxon_name>
    <taxon_hierarchy>genus t.;species nepetifolia;variety ramosa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">T.</taxon_name>
    <taxon_name rank="species" authority="unknown" date="unknown">ramosa</taxon_name>
    <taxon_name rank="variety" authority="(Müller Arg.) Pax &amp; K. Hoffmann" date="unknown">latifolia</taxon_name>
    <taxon_hierarchy>genus t.;species ramosa;variety latifolia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">T.</taxon_name>
    <taxon_name rank="species" authority="Müller Arg." date="unknown">stylaris</taxon_name>
    <taxon_hierarchy>genus t.;species stylaris</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">T.</taxon_name>
    <taxon_name rank="species" authority="unknown" date="unknown">stylaris</taxon_name>
    <taxon_name rank="variety" authority="Müller Arg." date="unknown">angustifolia</taxon_name>
    <taxon_hierarchy>genus t.;species stylaris;variety angustifolia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">T.</taxon_name>
    <taxon_name rank="species" authority="unknown" date="unknown">stylaris</taxon_name>
    <taxon_name rank="variety" authority="Müller Arg." date="unknown">latifolia</taxon_name>
    <taxon_hierarchy>genus t.;species stylaris;variety latifolia</taxon_hierarchy>
  </taxon_identification>
  <number>11.</number>
  <other_name type="common_name">Branched or desert or common noseburn</other_name>
  <description type="morphology">Subshrubs, 1.2–5 dm. Stems erect to trailing, dark green to light green, apex rarely flexuous. Leaves: petiole 2–20 mm; blade linear-lanceolate to narrowly ovate, 1–4 × 0.5–2 cm, base truncate to weakly cordate, margins serrate, apex acute. Inflorescences terminal (often appearing leaf opposed), glands few, sessile, staminate flowers 2–20 per raceme; staminate bracts 1.5–2 mm. Pedicels: staminate 0.7–2 mm, persistent base 0.4–1.5 mm; pistillate 2–2.5 mm in fruit. Staminate flowers: sepals 3–4, green, 1–2.2 mm; stamens 3–6(–10), filaments 0.3–1 mm. Pistillate flowers: sepals lanceolate, 0.8–2.5 mm; styles connate 1/3–1/2 length, long-exserted; stigmas smooth to undulate. Capsules 6–8 mm wide. Seeds dark brown, 2.5–3.5 mm. 2n = 44.</description>
  <discussion>Tragia ramosa is a variable species showing much environmental plasticity. Collections from the western United States and western Mexico have much broader leaves than those from Texas and Nuevo León, and were previously referred to as T. stylaris. Smooth stigmatic surfaces, three to six (rarely to ten) stamens, and narrow apical leaves are characters consistent with T. ramosa.</discussion>
  <description type="phenology">Flowering spring–fall; fruiting late spring–fall.</description>
  <description type="habitat">Mesquite, desert scrub, pine-juniper, oak woodlands.</description>
  <description type="elevation">200–2800 m.</description>
  <description type="distribution">Ariz., Ark., Calif., Colo., Kans., Mo., Nebr., Nev., N.Mex., Tex., Utah; Mexico (Baja California, Chihuahua, Coahuila, Nuevo León, Sonora, Tamaulipas).</description>
</bio:treatment>
