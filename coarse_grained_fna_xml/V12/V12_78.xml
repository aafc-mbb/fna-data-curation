<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="treatment_page">140</other_info_on_meta>
    <other_info_on_meta type="mention_page">134</other_info_on_meta>
    <other_info_on_meta type="mention_page">137</other_info_on_meta>
    <other_info_on_meta type="mention_page">141</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="R. Brown" date="unknown">OXALIDACEAE</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">OXALIS</taxon_name>
    <taxon_name rank="species" authority="(Small) Fedde" date="1905">texana</taxon_name>
    <place_of_publication>
      <publication_title>Just's Bot. Jahresber.</publication_title>
      <place_in_publication>32(1): 410. 1905</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family oxalidaceae;genus oxalis;species texana</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250101491</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Xanthoxalis</taxon_name>
    <taxon_name rank="species" authority="Small" date="1903">texana</taxon_name>
    <place_of_publication>
      <publication_title>Fl. S.E. U.S.,</publication_title>
      <place_in_publication>667, 1332. 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus xanthoxalis;species texana</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Oxalis</taxon_name>
    <taxon_name rank="species" authority="Small" date="unknown">priceae</taxon_name>
    <taxon_name rank="subspecies" authority="(Small) G. Eiten" date="unknown">texana</taxon_name>
    <taxon_hierarchy>genus oxalis;species priceae;subspecies texana</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">O.</taxon_name>
    <taxon_name rank="species" authority="Elliot" date="unknown">recurva</taxon_name>
    <taxon_name rank="variety" authority="(Small) Wiegand" date="unknown">texana</taxon_name>
    <taxon_hierarchy>genus o.;species recurva;variety texana</taxon_hierarchy>
  </taxon_identification>
  <number>5.</number>
  <other_name type="common_name">Texas wood-sorrel</other_name>
  <description type="morphology">Herbs perennial, caulescent, cespitose, caudex present, rhizomes or stolons present, bulbs absent. Aerial stems usually 2–6 from base, erect to ascending, 5–15 cm, becoming woody proximally, usually evenly strigose to strigillose from base to peduncles and pedicels, hairs straight, antrorsely appressed to closely ascending, (rarely Louisiana plants proximally villous, hair spreading), nonseptate. Leaves basal and cauline; stipules oblong, margins usually very narrowly flanged, apical auricles usually slightly free; petiole 2–6 cm, hairs nonseptate; leaflets 3, green to purple, cordate, (4–)6–12(–18) mm, lobed 1/5–1/3 length, abaxial surface sparsely strigose, adaxial surface glabrous or sparsely strigose, oxalate deposits absent. Inflorescences umbelliform cymes, very rarely irregular cymes, (2–)3–5(–8)-flowered; peduncles 4–10 cm. Flowers distylous; sepal apices without tubercles; petals yellow, with prominent red lines proximally, (10–)12–16(–17) mm (Arkansas, Louisiana, Texas) or 6–12 mm (Alabama). Capsules angular-cylindric, abruptly tapering to apex, 8–15 mm, moderately to densely puberulent to puberulent-villous. Seeds brown, transverse ridges white.</description>
  <discussion>Oxalis texana is similar to O. dillenii in its evenly strigose to strigillose stems but differs primarily in its more numerous flowers per inflorescence and larger, distylous flowers with red-lined corolla throats. Plants of O. dillenii with larger flowers on elevated peduncles might be mistaken for O. texana, yet the two taxa exist sympatrically in the range of O. texana and it is clear that they are separate species.</discussion>
  <discussion>All Alabama plants identified as Oxalis texana (weighting orientation of cauline vestiture in the identifications) are from Dauphin Island and localities in and around Mobile. Compared to those in Louisiana and Texas, the Alabama plants have shorter petals [6–12 mm versus (10–)12–16(–17) mm], a more colonial habit (usually with long, lateral, stolonlike branches versus commonly with short basal offsets), and they grow in disturbed sites (versus mostly undisturbed sites, usually within woods). It is plausible that they may prove to be more closely related to O. colorea than to the western O. texana.</discussion>
  <description type="phenology">Flowering Mar–May(–Jun).</description>
  <description type="habitat">Commonly in undisturbed habitats and usually in deep, loose sand, but also fields, roadsides, edges and openings in pine, pine-oak, and mixed hardwood woods.</description>
  <description type="elevation">10–200 m.</description>
  <description type="distribution">Ala., Ark., La., Tex.</description>
</bio:treatment>
