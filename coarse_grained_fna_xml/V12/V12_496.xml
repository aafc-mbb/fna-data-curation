<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>Jess A. Peirson, Victor W. Steinmann, Jeffery J. Morawetz</author>
    </source>
    <other_info_on_meta type="treatment_page">240</other_info_on_meta>
    <other_info_on_meta type="mention_page">239</other_info_on_meta>
    <other_info_on_meta type="mention_page">317</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">EUPHORBIACEAE</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">EUPHORBIA</taxon_name>
    <taxon_name rank="section" authority="(Schlechtendal) Baillon" date="1858">Alectoroctonum</taxon_name>
    <place_of_publication>
      <publication_title>Étude Euphorb.,</publication_title>
      <place_in_publication>284. 1858</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family euphorbiaceae;genus euphorbia;section alectoroctonum</taxon_hierarchy>
    <other_info_on_name type="fna_id">318112</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name rank="genus" authority="Schlechtendal" date=" 1846">Alectoroctonum</taxon_name>
    <place_of_publication>
      <publication_title>Linnaea</publication_title>
      <place_in_publication>19: 252. 1846</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus alectoroctonum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="Rafinesque" date="unknown">Agaloma</taxon_name>
    <taxon_hierarchy>genus agaloma</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="Klotzsch &amp; Garcke" date="unknown">Tithymalopsis</taxon_name>
    <taxon_hierarchy>genus tithymalopsis</taxon_hierarchy>
  </taxon_identification>
  <number>24a.</number>
  <description type="morphology">Herbs or shrubs [trees, rarely lianas], annual or perennial, with taproot or thickened or tuberous rootstock. Stems erect, ascending, decumbent, or prostrate, branched or unbranched, terete, glabrous or variously hairy, covered by exfoliating waxy coat in E. antisyphilitica. Leaves alternate or opposite; stipules present (sometimes rudimentary in E. graminea and E. hexagona), at base of petiole; petiole usually present, rarely absent or rudimentary, glabrous or hairy; blade monomorphic (dimorphic in E. curtisii, E. exserta, E. ipecacuanhae, and E. mercurialina), base symmetric, margins entire [rarely toothed], surfaces glabrous or hairy; venation pinnate, occasionally inconspicuous. Cyathial arrangement: solitary or in terminal monochasia, dichasia, or pleiochasia; individual dichasial or pleiochasial branches unbranched or few-branched at one or more successive nodes; bracts subtending dichasia and pleiochasia (pleiochasial bracts) opposite or whorled, green or with white margins, similar in shape and size to distal stem leaves, those on branches (dichasial or subcyathial bracts) opposite (rarely whorled or alternate in E. corollata), distinct; additional cymose branches occasionally present in distal axils, but not subtended by opposite or whorled bracts. Involucre ± actinomorphic, not spurred; glands [0–](2–)5, slightly concave, flat, or slightly convex; appendages usually petaloid, occasionally rudimentary. Staminate flowers (5–)20–25(–70). Pistillate flowers: ovary glabrous or hairy; styles connate basally, 2-fid. Seeds: caruncle present or absent.</description>
  <discussion>Species ca. 120 (21 in the flora).</discussion>
  <description type="distribution">North America, Mexico, West Indies, Central America, South America; introduced in Asia, Pacific Islands.</description>
  <references>
    <reference>Huft, M. J. 1979. A Monograph of Euphorbia Section Tithymalopsis. Ph.D. dissertation. University of Michigan.</reference>
    <reference>Park, K. R. 1998. Monograph of Euphorbia sect. Tithymalopsis (Euphorbiaceae). Edinburgh J. Bot. 55: 161–208.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Shrubs.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems pencil-like, covered with flaky, exfoliating layer of wax; cyathia in axillary congested cymes near branch tips or solitary at distal nodes; Arizona, New Mexico, Texas.</description>
      <determination>2. Euphorbia antisyphilitica</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems gnarled, not waxy; cyathia solitary on short shoots; s California.</description>
      <determination>16. Euphorbia misera</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Herbs.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Annual herbs with taproots (E. graminea rarely perennial).</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaves opposite.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Stems 30–70(–100) cm; leaf blades linear-filiform, linear, or elliptic; cyathia solitary in leaf axils or in terminal cymes or dichasia; involucral gland appendages 0.7–1.7 mm; seeds 3.4 × 2.7 mm; c United States, mostly e of Rocky Mountains.</description>
      <determination>10. Euphorbia hexagona</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Stems 4–25 cm; leaf blades usually linear- to narrowly-elliptic, occasionally ovate to obovate; cyathia solitary at distal bifurcations of stems; involucral gland appendages 0.2–0.5 mm; seeds 2.3–2.6 × 1.3–1.5 mm; Colorado Plateau of Utah, sw Colorado.</description>
      <determination>17. Euphorbia nephradenia</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaves mostly alternate (opposite at proximal nodes in E. bilobata; some opposite in E. graminea).</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Dichasial bracts with conspicuous white margins.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Dichasial bracts linear to narrowly oblanceolate; leaf blades pilose.</description>
      <determination>3. Euphorbia bicolor</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Dichasial bracts narrowly elliptic to oblanceolate; leaf blades glabrous.</description>
      <determination>14. Euphorbia marginata</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Dichasial bracts wholly green or distal ones white.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Stems 10–35 cm; leaves opposite proximally, alternate distally; dichasial bracts wholly green; involucral glands 5; involucral gland appendages usually 2-fid.</description>
      <determination>4. Euphorbia bilobata</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Stems 30–80(–110) cm; leaves usually alternate, sometimes some opposite; distal dichasial bracts often white; involucral glands (1–)2–4; involucral gland appendages undivided.</description>
      <determination>9. Euphorbia graminea</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Perennial herbs with rootstocks, tubers, or taproots.</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Stem leaves usually opposite, occasionally whorled distally, rarely with 1 or 2 alternate leaves; plants with thick, globose to elongated tubers; se Arizona, primarily from Huachuca Mountains.</description>
      <determination>13. Euphorbia macropus</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Stem leaves alternate; plants usually with rootstocks or taproots, rarely with elongated tubers; Arizona to e North America.</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Involucral glands 4, appendages green; leaf blade adaxial surfaces densely pilose, bases cordate; s coastal Texas.</description>
      <determination>11. Euphorbia innocua</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Involucral glands 5, appendages usually white to pink, if greenish then minute and forming rim around gland; leaf blade adaxial surfaces glabrous, rarely villous or strigose (or pilose when young in E. aaron-rossii), bases cuneate to rounded; not s coastal Texas.</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Stems usually densely clumped, previous year's dead stems often persistent; leaf blades filiform to linear or narrowly ovate to lanceolate or oblanceolate, 0.5–6.5 mm wide; Arizona, New Mexico to wc Texas.</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Cyathia in terminal monochasia; petiole 0.2–2.2 mm; stem leaves usually reflexed, occasionally spreading; endemic to banks of Colorado River in n Arizona.</description>
      <determination>1. Euphorbia aaron-rossii</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Cyathia in terminal dichasia (rarely in pleiochasia in E. wrightii); petiole absent; stem leaves spreading or ascending; New Mexico, Texas.</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Leaf blades (2–)4–5 mm wide; involucral gland appendages 0.2 mm, forming narrow rim around distal margin of gland; capsules 3.2–4.5 × 4–6.5 mm, all 3 locules fertile; seeds 3.8 mm; mid and proximal cyathia early deciduous; Texas Panhandle, adjacent New Mexico.</description>
      <determination>20. Euphorbia strictior</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Leaf blades 1–2.5 mm wide; involucral gland appendages 0.5–1 mm, orbiculate; capsules 2.5(–3) × 2.7–3.3(–5) mm, 1 locule usually aborting; seeds 2.2–2.9 mm; cyathia persistent; wc Texas.</description>
      <determination>21. Euphorbia wrightii</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Stems usually solitary or few, if densely clumped then previous year's dead stems not persistent; leaf blades filiform, linear or elliptic to lanceolate, ovate, oblanceolate, obovate or orbiculate, 0.8–26 mm wide (often greater than 5 mm wide); e Texas and Oklahoma to e North America.</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Involucral gland appendages 0–0.2 mm; peduncles 10–50(–70) mm.</description>
      <next_statement_id>15</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Involucres and glands typically dark red; plants with spreading rootstocks; stems erect or ascending.</description>
      <determination>8. Euphorbia exserta</determination>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Involucres and glands yellow or yellow-green; plants with deep, stout taproots; stems decumbent or slightly ascending.</description>
      <determination>12. Euphorbia ipecacuanhae</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Involucral gland appendages 0.3–3.5(–4.5) mm; peduncles 1–17 mm (occasionally peduncle of central cyathium greater than 30 mm; occasionally to 40 mm in early May–Jun flowering E. pubentissima).</description>
      <next_statement_id>16</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Involucral glands red; leaf blades linear to filiform, 10–20 × 0.8–1.5(–4) mm; c, s peninsular Florida.</description>
      <determination>18. Euphorbia polyphylla</determination>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Involucral glands green; leaf blades not linear to filiform, or if linear then 10–55 × 1.5–6 mm; not peninsular Florida.</description>
      <next_statement_id>17</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Involucral gland appendages 0.3–0.6 mm; proximal leaves greatly reduced and often scalelike and appressed.</description>
      <next_statement_id>18</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Leaf blades usually linear, occasionally elliptic, rarely ovate, 1.5–6 mm wide, margins occasionally sparsely ciliate; seeds smooth.</description>
      <determination>6. Euphorbia curtisii</determination>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Leaf blades elliptic to ovate-deltate, 20–26 mm wide, margins densely ciliate; seeds with shallow and coarse depressions.</description>
      <determination>15. Euphorbia mercurialina</determination>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Involucral gland appendages (0.5–)1–3.5 mm; proximal leaves not reduced, neither scalelike nor appressed.</description>
      <next_statement_id>19</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Leaf blades usually linear, rarely ovate, 1.5–4 mm wide, margins revolute; stems usually densely puberulent to sericeous, rarely glabrous; seeds 2 × 1.2–1.3 mm.</description>
      <determination>7. Euphorbia discoidalis</determination>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Leaf blades oblanceolate, obovate, lanceolate, lance-ovate, or elliptic, 5–18 mm wide, margins not revolute or occasionally slightly revolute (E. corollata); stems glabrous, slightly pilose, or rarely villous; seeds 2.2–2.8 × 1.6–2.2 mm.</description>
      <next_statement_id>20</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Involucral gland appendages 2.5–3.5(–4.5) × 2.5–3.2 mm; peduncles (1.5–)5–11(–13) mm (proximal to 70 mm); seeds 2.5–2.8 mm.</description>
      <determination>5. Euphorbia corollata</determination>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Involucral gland appendages 1–2.2 × 1.5 mm; peduncles 1–5 mm (or 15–40 mm in early flowering plants); seeds 2.2–2.4 mm.</description>
      <determination>19. Euphorbia pubentissima</determination>
    </key_statement>
  </key>
</bio:treatment>
