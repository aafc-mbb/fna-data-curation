<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="treatment_page">373</other_info_on_meta>
    <other_info_on_meta type="mention_page">371</other_info_on_meta>
    <other_info_on_meta type="mention_page">372</other_info_on_meta>
    <other_info_on_meta type="mention_page">376</other_info_on_meta>
    <other_info_on_meta type="mention_page">377</other_info_on_meta>
    <other_info_on_meta type="mention_page">382</other_info_on_meta>
    <other_info_on_meta type="mention_page">388</other_info_on_meta>
    <other_info_on_meta type="mention_page">395</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="de Candolle ex Perleb" date="unknown">LINACEAE</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">LINUM</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 277. 1753</place_in_publication>
      <publication_title>Gen Pl. ed.</publication_title>
      <place_in_publication>5, 135. 1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family linaceae;genus linum</taxon_hierarchy>
    <other_info_on_name type="etymology">Latin lin, flax</other_info_on_name>
    <other_info_on_name type="fna_id">118675</other_info_on_name>
  </taxon_identification>
  <number>2.</number>
  <other_name type="common_name">Flax</other_name>
  <description type="morphology">Herbs or subshrubs, annual, biennial, or perennial, glabrous or hairy. Stems usually erect or spreading to ascending, sometimes decumbent or ascending from decumbent base, unbranched or branched at base, throughout, or only in inflorescence. Leaves sometimes falling early, alternate or sometimes partially opposite or whorled; stipular glands present or absent; blade linear, linear-lanceolate, linear-oblanceolate, lanceolate, oblanceolate, elliptic, oblong, obovate, spatulate, or awl-shaped, margins glandular-toothed or entire, sometimes ciliate. Inflorescences usually panicles, racemes, or cymes, rarely thyrses or corymbs. Pedicels articulated or not. Flowers: sepals persistent or deciduous, 5, connate at base, equal or unequal in size, margins scarious, entire, ciliate, or toothed, glandular or not; petals 5, distinct or coherent at base, attached to filament cup at base, midway, or on or proximal to rim, blue, white, yellow, yellowish orange, orange, or salmon, rarely red or maroon, sometimes with darker bands near base, appendages absent or pouches formed on petal margins at base of claw; stamens 5; staminodes 0 or 5, as small deltate projections; pistil 5-carpellate, ovary 5-locular, or 10-locular by intrusion of false septa; styles 5, distinct or connate; stigmas capitate, linear, or clavate, wider than styles. Fruits capsules, usually 5-celled and dehiscing into 5 segments, sometimes each cell partially divided by incomplete or nearly complete false septum and dehiscing into 10 segments. Seeds 10, lenticular. x = 13, [15, 18].</description>
  <discussion>Species ca. 180 (37 in the flora).</discussion>
  <description type="distribution">Nearly worldwide; temperate and subtropical regions.</description>
  <discussion>C. M. Rogers (1963, 1964, 1968, 1982, 1984) published comprehensive studies of Linum in North America and Central America; he also studied Linum in South America (Rogers and R. Mildner 1976), southern Africa (Rogers 1981), and Madagascar (Rogers 1981b). This treatment draws largely on his work and follows his taxonomic arrangement, which is congruent, at least at the level of section, with the results in J. R. McDill et al. (2009). Species of Linum in the flora have been placed in three sections of the genus, out of a total of five sections worldwide.</discussion>
  <references>
    <reference>Harris, B. D. 1968. Chromosome numbers and evolution in North America species of Linum. Amer. J. Bot. 55: 1197–1204.</reference>
    <reference>Rogers, C. M. 1963. Yellow flowered species of Linum in eastern North America. Brittonia 15: 97–122.</reference>
    <reference>Rogers, C. M. 1964. Yellow-flowered Linum (Linaceae) in Texas. Sida 1: 328–336.</reference>
    <reference>Rogers, C. M. 1968. Yellow-flowered species of Linum in Central America and western North America. Brittonia 20: 107–135.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals yellow, sometimes with maroon at base.</description>
      <determination>2c. Linum sect. Linopsis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals red, white, or blue.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Sepal margins not glandular-toothed; petals usually blue or red to maroon, rarely white.</description>
      <determination>2a. Linum sect. Linum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Sepal margins (at least inner) glandular-toothed, petals white.</description>
      <determination>2b. Linum sect. Cathartolinum</determination>
    </key_statement>
  </key>
</bio:treatment>
