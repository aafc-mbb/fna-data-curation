<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>Nancy R. Morin</author>
    </source>
    <other_info_on_meta type="treatment_page">371</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
    <other_info_on_meta type="illustrator">John Myers</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="de Candolle ex Perleb" date="unknown">LINACEAE</taxon_name>
    <taxon_hierarchy>family linaceae</taxon_hierarchy>
    <other_info_on_name type="fna_id">250101529</other_info_on_name>
  </taxon_identification>
  <number>103.</number>
  <other_name type="common_name">Flax Family</other_name>
  <description type="morphology">Herbs or subshrubs [shrubs, trees, vines], annual, biennial, or perennial. Leaves alternate, opposite, or whorled, simple; stipules absent or present as small, dark, spheric glands; petiole usually absent, rarely present; blade margins entire, serrate, or denticulate; venation pinnate. Inflorescences terminal, racemes, panicles, or cymes (rarely thyrses or corymbs in Linum) [spikes]. Flowers bisexual; perianth and androecium hypogynous; hypanthium absent; sepals 4–5, connate basally [distinct]; petals 4–5, distinct or coherent basally, imbricate or convolute, bases sometimes with appendages; nectary extrastaminal; stamens 4–5 [10], connate basally, filament tube and petal bases adherent or adnate [free]; anthers dehiscing by longitudinal slits; pistil 1, 2–5-carpellate, ovary superior, 4–5-locular, placentation axile or apical-axile; ovules 2 per locule, anatropous; styles 2–5, distinct or partly connate; stigmas 2–5. Fruits capsules, dehiscence septicidal, or indehiscent or schizocarps breaking into 4 nutlets (Sclerolinon). Seeds 2 per locule, seed coat often mucilaginous.</description>
  <discussion>Genera 10–14, species ca. 260 (4 genera, 52 species in the flora).</discussion>
  <description type="distribution">North America, Mexico, West Indies, Bermuda, Central America, South America, Eurasia, Africa, Atlantic Islands, Pacific Islands, Australia.</description>
  <discussion>Two subfamilies are generally recognized in Linaceae, the mostly herbaceous, temperate Linoideae Arnott (8 genera, ca. 240 species), in which all the genera in the flora area are placed, and the woody, mostly tropical Hugonoideae Reveal. Based on molecular phylogenetic analysis, J. R. McDill et al. (2009) concluded that Linaceae is a monophyletic group, as is Linoideae.</discussion>
  <discussion>According to J. R. McDill (2009), Cliococca Babington, Hesperolinon, and Sclerolinon are nested within Linum sect. Linopsis, and collectively these are sister to Radiola; Hesperolinon and Sclerolinon are most closely related to Mexican and Central American species of Linum. McDill et al. (2009) noted that the relationships within this clade are not well-enough resolved or supported to warrant nomenclatural changes; McDill (2009) came to the same conclusion based on a much wider sample of species. The current generic circumscriptions are maintained here.</discussion>
  <references>
    <reference>McDill, J. R. 2009. Molecular Phylogenetic Studies in the Linaceae and Linum, with Implications for Their Systematics and Historical Biogeography. Ph.D. dissertation. University of Texas.</reference>
    <reference>McDill, J. R. et al. 2009. The phylogeny of Linum and Linaceae subfamily Linoideae, with implications for their systematics, biogeography, and evolution of heterostyly. Syst. Bot. 34: 386–405.</reference>
    <reference>McDill, J. R. and B. B. Simpson. 2011. Molecular phylogenetics of Linaceae with complete generic sampling and data from two plastid genes. Bot. J. Linn Soc. 165: 64–83.</reference>
    <reference>Rogers, C. M. 1975. Relationships of Hesperolinon and Linum (Linaceae). Madroño 23: 153–159.</reference>
    <reference>Rogers, C. M. 1984. Linaceae. In: N. L. Britton et al., eds. 1905+. North American Flora.... 47+ vols. New York. Ser. 2, part 12, pp. 1–54.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sepals 4; petals 4.</description>
      <determination>1 Radiola</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sepals 5; petals 5.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Styles 5; fruits capsules, dehiscing into 5 or 10 segments.</description>
      <determination>2 Linum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Styles 2–3; fruits capsules dehiscing into 4 or 6 segments, schizocarps breaking into 4 nutlets, or indehiscent.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves: basal and proximal usually whorled, distal alternate or opposite; fruits capsules, dehiscing into 4 or 6 segments; styles 2–3, stigmas ± equal in width to styles; stipular glands present (exudate often red) or absent.</description>
      <determination>3 Hesperolinon</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves: proximal opposite, distal sometimes alternate; fruits schizocarps, breaking into 4 nutlets, or indehiscent; styles 2, stigmas wider than styles; stipular glands absent.</description>
      <determination>4 Sclerolinon</determination>
    </key_statement>
  </key>
</bio:treatment>
