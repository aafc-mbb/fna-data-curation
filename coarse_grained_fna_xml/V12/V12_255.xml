<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="treatment_page">425</other_info_on_meta>
    <other_info_on_meta type="illustration_page">421</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Batsch" date="unknown">VISCACEAE</taxon_name>
    <taxon_name rank="genus" authority="M. Bieberstein" date="1819">ARCEUTHOBIUM</taxon_name>
    <taxon_name rank="species" authority="Nuttall ex Engelmann" date="1850">americanum</taxon_name>
    <place_of_publication>
      <publication_title>Boston J. Nat. Hist.</publication_title>
      <place_in_publication>6. 214. 1850</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family viscaceae;genus arceuthobium;species americanum</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">250063317</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Razoumofskya</taxon_name>
    <taxon_name rank="species" authority="(Nuttall ex Engelmann) Kuntze" date="unknown">americana</taxon_name>
    <taxon_hierarchy>genus razoumofskya;species americana</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Lodgepole pine dwarf mistletoe</other_name>
  <description type="morphology">Plants usually forming systemic witches' brooms, sometimes nonsystemic witches' brooms in secondary hosts. Stems yellowish to olive green; secondary branching whorled, branches 5–9(–30) cm, third internode 6–23 × 1–2 mm, dominant shoot 1–3 mm diam. at base. Staminate pedicels present. Staminate flowers radially symmetric, subglobose in bud, 2.2 mm diam.; petals 3(–4), same color as stems. Berries proximally olive green, distally yellowish to reddish brown, 3.5–4.5 × 1.5–2.5 mm. Seeds ellipsoid, 2.4 × 1.1 mm, endosperm green. 2n = 28.</description>
  <discussion>Meiosis occurs in August, with fruits maturing 16 months after pollination; seeds germinate in May.</discussion>
  <discussion>The principal hosts of Arceuthobium americanum are Pinus contorta var. latifolia in western North America, P. contorta var. murrayana in the Sierra Nevada and Cascade ranges of the western United States, and P. banksiana in western Canada. A study utilizing AFLPs (C. A. Jerome and B. A. Ford 2002) documented that the parasite exists as three genetic races that correspond to these host species. Arceuthobium americanum has the most extensive geographic range of any species of the genus, and can utilize other species as secondary hosts, including P. albicaulis, P. flexilis, P. jeffreyi, and P. ponderosa, as well as a number of rare hosts. Although young infections may be localized, A. americanum eventually forms massive systemic witches’ brooms. Interestingly, when parasitizing some secondary hosts, the brooms may become nonsystemic, possibly indicating partial breakdown of coordinated developmental pathways.</discussion>
  <description type="phenology">Flowering (Mar–)Apr–Jun; fruiting Aug–Sep.</description>
  <description type="habitat">Coniferous forests, especially with jack or lodgepole pine.</description>
  <description type="elevation">200–3400 m.</description>
  <description type="distribution">Alta., B.C., Man., Ont., Sask.; Calif., Colo., Idaho, Mont., Nev., Oreg., Utah, Wash., Wyo.</description>
</bio:treatment>
