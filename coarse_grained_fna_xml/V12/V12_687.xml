<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="treatment_page">94</other_info_on_meta>
    <other_info_on_meta type="mention_page">78</other_info_on_meta>
    <other_info_on_meta type="mention_page">84</other_info_on_meta>
    <other_info_on_meta type="mention_page">93</other_info_on_meta>
    <other_info_on_meta type="mention_page">95</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">RHAMNACEAE</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">CEANOTHUS</taxon_name>
    <taxon_name rank="subgenus" authority="(S. Watson) Weberbauer" date="1896">Cerastes</taxon_name>
    <place_of_publication>
      <publication_title>in H. G. A. Engler and K. Prantl, Nat. Pflanzenfam.</publication_title>
      <place_in_publication>128[III,5]: 414. 1896</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rhamnaceae;genus ceanothus;subgenus cerastes</taxon_hierarchy>
    <other_info_on_name type="fna_id">318092</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Ceanothus</taxon_name>
    <taxon_name rank="section" authority="S. Watson" date="1875">Cerastes</taxon_name>
    <place_of_publication>
      <publication_title>Proc. Amer. Acad. Arts</publication_title>
      <place_in_publication>10: 338. 1875</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus ceanothus;section cerastes</taxon_hierarchy>
  </taxon_identification>
  <number>13b.</number>
  <description type="morphology">Shrubs, evergreen. Branchlets not thorn-tipped. Leaves opposite (alternate in C. megacarpus and C. verrucosus); stipules persistent, thick, wartlike; blade leathery, margins entire or teeth not gland-tipped, often spinulose, stomata on abaxial surface in crypts, (crypts appearing as areolae aligned in rows between secondary veins); pinnately veined. Inflorescences umbel-like (rarely racemelike in C. pauciflorus). Capsules usually horned (horns sometimes minute or weakly developed bulges), sometimes not horned, usually not crested (crested in C. divergens and C. gloriosus); ridges between valves present or absent.</description>
  <discussion>Species of subg. Cerastes not accounted for here are: Ceanothus australis Rose and C. bolensis S. Boyd &amp; J. Keeley, both endemic to Mexico.</discussion>
  <discussion>Species 25 (23 in the flora).</discussion>
  <description type="distribution">w, sc United States, Mexico.</description>
  <discussion>In the following key, references to indumentum do not include the hairs associated with the stomatal crypts, which in all species of subg. Cerastes appear microscopically tomentulose or densely puberulent; these hairs are much shorter than hairs borne on the abaxial surfaces and veins between the crypts.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves alternate.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades widely obovate to suborbiculate, 5–14 mm, apices truncate to retuse; capsules 4–6 mm wide, valves smooth, horns minute or absent.</description>
      <determination>29. Ceanothus verrucosus</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades elliptic to obovate, 10–25(–33) mm, apices obtuse; capsules 7–12 mm wide, valves rugulose to weakly ridged near apex, horns prominent.</description>
      <determination>30. Ceanothus megacarpus</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves opposite.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Shrubs matlike to moundlike, 0.1–1 m; stems spreading to weakly ascending (sometimes erect to ascending in C. maritimus and C. sonomensis), sometimes rooting at nodes.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Shrubs matlike, 0.1–0.3 m; stems prostrate or spreading.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaf blade margins sharply dentate to spinose-dentate, teeth 3–9.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaves not crowded (shorter than internodes); leaf blade adaxial surfaces green, dull; capsules 4–6 mm wide.</description>
      <determination>42. Ceanothus confusus</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaves often crowded (usually longer than internodes); leaf blade adaxial surfaces dark green, shiny; capsules 6–9 mm wide.</description>
      <determination>43. Ceanothus prostratus</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaf blade margins entire or denticulate near apex, teeth 0–3.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaf blade adaxial surfaces dark green, shiny, apices rounded to retuse; capsule horns prominent.</description>
      <determination>36. Ceanothus fresnensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaf blade adaxial surfaces green to grayish green, dull, apices usually truncate, sometimes obtuse; capsule horns minute or weakly developed bulges.</description>
      <determination>44. Ceanothus pumilus</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Shrubs moundlike, 0.1–1 m; stems ± prostrate, spreading, or weakly ascending.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaf blade margins spinose-dentate.</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Leaf blades widely obovate to suborbiculate, 5–12 mm, apices widely notched, marginal teeth 2–4.</description>
      <determination>40. Ceanothus sonomensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Leaf blades elliptic, ± oblong, or obovate, 10–20 mm, apices acute or retuse, with an apical tooth, marginal teeth 3–9(–11).</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Shrubs 0.5–1.5 m; stems ascending to erect.</description>
      <determination>41. Ceanothus divergens</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Shrubs 0.1–0.6 m; stems spreading to weakly ascending.</description>
      <determination>42. Ceanothus confusus</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaf blade margins entire, denticulate, or serrulate.</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Leaf blade abaxial surfaces tomentulose.</description>
      <determination>33. Ceanothus maritimus</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Leaf blade abaxial surfaces strigillose, sometimes only on or between veins, puberulent, or glabrate.</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Leaf blade margins dentate to denticulate or serrulate most of length, teeth 5–31.</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Leaves crowded, blades elliptic to obovate, marginal teeth 5–9.</description>
      <determination>43. Ceanothus prostratus</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Leaves not crowded, blades widely elliptic, obovate, or suborbiculate, marginal teeth 9–31.</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Leaf blades 23–31(–45) mm, marginal teeth 13–31; capsule horns minute, intermediate ridges absent; coastal habitats, 30–200 m.</description>
      <determination>38. Ceanothus gloriosus</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Leaf blades 10–20 mm, marginal teeth 9–15; capsule horns prominent, intermediate ridges present; montane habitats, elevations 1600–2600 m.</description>
      <determination>45. Ceanothus pinetorum</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Leaf blade margins entire, denticulate near apex, or remotely denticulate, teeth 1–5(–7).</description>
      <next_statement_id>15</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Leaf blades narrowly oblanceolate to narrowly oblong-lanceolate, margins thick to ± revolute, apex usually truncate, sometimes obtuse.</description>
      <determination>44. Ceanothus pumilus</determination>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Leaf blades elliptic to oblanceolate, margins sometimes thick but not revolute, apex acute, obtuse, retuse, or rounded, or ± truncate.</description>
      <next_statement_id>16</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Leaf blades folded lengthwise; capsule horns absent or weakly developed bulges; axillary short shoots, if present, erect.</description>
      <determination>37. Ceanothus roderickii</determination>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Leaf blades flat; capsule horns prominent to minute or absent; axillary short shoots, if present, ascending to spreading.</description>
      <next_statement_id>17</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Leaf blades apices rounded to obtuse, adaxial surfaces pale green; capsule horns subapical, erect.</description>
      <determination>35. Ceanothus arcuatus</determination>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Leaf blades apices acute to ± truncate; adaxial surfaces grayish green; capsule horns minute or absent, or lateral, spreading.</description>
      <determination>48. Ceanothus pauciflorus</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Plants not matlike or moundlike, 0.5–6 m; stems usually erect to ascending, sometimes spreading, not rooting at nodes.</description>
      <next_statement_id>18</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Leaf blade margins spinose-dentate.</description>
      <next_statement_id>19</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Stems ± flexible; leaves spreading, abaxial leaf surfaces grayish green.</description>
      <determination>41. Ceanothus divergens</determination>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Stems rigid; leaves spreading to deflexed, abaxial leaf surfaces pale green or pale yellowish green.</description>
      <next_statement_id>20</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Sepals, petals, and stamens 5; leaf blade adaxial surfaces green to dark green; capsule valves smooth, horns slender, intermediate ridges absent; on volcanic soils.</description>
      <determination>46. Ceanothus purpureus</determination>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Sepals, petals, and stamens (5–)6(–8); leaf blade adaxial surfaces pale green; capsule valves rugose, horns thick, intermediate ridges present; on serpentine soils.</description>
      <determination>47. Ceanothus jepsonii</determination>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Leaf blade margins entire, dentate, denticulate, or serrulate.</description>
      <next_statement_id>21</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Leaf blade margins entire or remotely denticulate, teeth if present minute, 1–7 (8–19 in C. crassifolius).</description>
      <next_statement_id>22</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>22</statement_id>
      <description type="morphology">Leaves both fascicled and not fascicled on same plant.</description>
      <next_statement_id>23</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>23</statement_id>
      <description type="morphology">Petioles 1–3 mm; leaf blades elliptic, oblanceolate, obovate, or orbiculate, 6–22(–30) × 3–12(–22) mm; capsules 4–6 mm wide, horns prominent.</description>
      <determination>32. Ceanothus cuneatus</determination>
    </key_statement>
    <key_statement>
      <statement_id>23</statement_id>
      <description type="morphology">Petioles 0–1 mm; leaf blades narrowly oblanceolate to narrowly obovate, 3–7 × 1–3 mm; capsules 3–4 mm wide, horns minute or absent.</description>
      <determination>51. Ceanothus ophiochilus</determination>
    </key_statement>
    <key_statement>
      <statement_id>22</statement_id>
      <description type="morphology">Leaves not fascicled.</description>
      <next_statement_id>24</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>24</statement_id>
      <description type="morphology">Branchlets light gray to ashy gray, puberulent to tomentulose, hairs curly or wavy, glabrescent; leaf blade adaxial surfaces grayish green, puberulent, hairs curly or wavy, glabrescent.</description>
      <determination>48. Ceanothus pauciflorus</determination>
    </key_statement>
    <key_statement>
      <statement_id>24</statement_id>
      <description type="morphology">Branchlets grayish brown to brown, puberulent or tomentulose, hairs straight, sometimes glabrate; leaf blade adaxial surfaces green, glabrous or sparsely tomentulose, hairs straight, glabrescent.</description>
      <next_statement_id>25</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>25</statement_id>
      <description type="morphology">Leaf blades widely oblanceolate to widely obovate, 5–11 × 4–7 mm; sepals, petals, and nectaries usually lavender, sometimes pale blue; capsules 5–6 mm wide.</description>
      <determination>32. Ceanothus cuneatus</determination>
    </key_statement>
    <key_statement>
      <statement_id>25</statement_id>
      <description type="morphology">Leaf blades elliptic to obovate, 10–40 × 5–15(–20) mm; sepals and petals white, nectaries blue or black; capsules 6–12 mm wide.</description>
      <next_statement_id>26</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>26</statement_id>
      <description type="morphology">Leaf blade abaxial surfaces glabrous or strigillose on veins; capsules 7–12 mm wide, horns weakly developed or absent.</description>
      <determination>30. Ceanothus megacarpus</determination>
    </key_statement>
    <key_statement>
      <statement_id>26</statement_id>
      <description type="morphology">Leaf blade abaxial surfaces tomentulose; capsules 6–9 mm wide, horns prominent.</description>
      <determination>31. Ceanothus crassifolius</determination>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Leaf blade margins dentate or denticulate, at least distal to middle, teeth 5–35 (3–5 in C. otayensis).</description>
      <next_statement_id>27</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>27</statement_id>
      <description type="morphology">Sepals and petals white to cream, nectaries tan to brown, yellow to green, blue, purple, or black.</description>
      <next_statement_id>28</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>28</statement_id>
      <description type="morphology">Leaf blade margins revolute or thick, abaxial surfaces tomentulose.</description>
      <next_statement_id>29</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>29</statement_id>
      <description type="morphology">Leaf blade margins with 8–19 teeth most of length, apices obtuse to rounded; capsules 5–9 mm wide, horns prominent.</description>
      <determination>31. Ceanothus crassifolius</determination>
    </key_statement>
    <key_statement>
      <statement_id>29</statement_id>
      <description type="morphology">Leaf blades margins with 3–5 teeth near apex, apices truncate, retuse, or cuspidate; capsules 4–6 mm wide, horns minute or absent.</description>
      <determination>50. Ceanothus otayensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>28</statement_id>
      <description type="morphology">Leaf blade margins thick or not, not revolute, abaxial surfaces sparsely puberulent (hairs curly) or strigillose, sometimes glabrescent.</description>
      <next_statement_id>30</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>30</statement_id>
      <description type="morphology">Leaf blade adaxial surfaces dark green; nectaries dark blue to purple; capsules 7–9 mm wide, horns subapical, prominent; serpentine substrates.</description>
      <determination>34. Ceanothus ferrisiae</determination>
    </key_statement>
    <key_statement>
      <statement_id>30</statement_id>
      <description type="morphology">Leaf blade adaxial surfaces green to yellowish green; nectaries yellow to green; capsules 4–6 mm wide, horns lateral, usually minute, sometimes absent; granitic or metamorphic substrates.</description>
      <determination>49. Ceanothus perplexans</determination>
    </key_statement>
    <key_statement>
      <statement_id>27</statement_id>
      <description type="morphology">Sepals, petals, and nectaries pale blue, blue, or purple.</description>
      <next_statement_id>31</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>31</statement_id>
      <description type="morphology">Leaf blades 4–10 × 4–6 mm, margins denticulate distal to middle, teeth 5–9.</description>
      <determination>32. Ceanothus cuneatus</determination>
    </key_statement>
    <key_statement>
      <statement_id>31</statement_id>
      <description type="morphology">Leaf blades 7–40 × 4–22 mm, denticulate most of length, teeth 9–35.</description>
      <next_statement_id>32</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>32</statement_id>
      <description type="morphology">Sepals, petals, and nectaries pale blue to blue; capsule horns prominent, rugose, intermediate ridges present; montane habitats, 1600–2600 m.</description>
      <determination>45. Ceanothus pinetorum</determination>
    </key_statement>
    <key_statement>
      <statement_id>32</statement_id>
      <description type="morphology">Sepals, petals, and nectaries deep blue to purple; capsule horns minute, not rugose, intermediate ridges absent; coastal habitats, 30–500 m.</description>
      <next_statement_id>33</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>33</statement_id>
      <description type="morphology">Leaf blades widely elliptic to suborbiculate, 3–40 × 17–22 mm, marginal teeth 13–35.</description>
      <determination>38. Ceanothus gloriosus</determination>
    </key_statement>
    <key_statement>
      <statement_id>33</statement_id>
      <description type="morphology">Leaf blades usually elliptic or oval, sometimes suborbiculate, 7–21 × 4–13 mm, marginal teeth 9–17.</description>
      <determination>39. Ceanothus masonii</determination>
    </key_statement>
  </key>
</bio:treatment>
