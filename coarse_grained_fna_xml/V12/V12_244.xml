<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="treatment_page">487</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Dumortier" date="unknown">HYDRANGEACEAE</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">HYDRANGEA</taxon_name>
    <taxon_name rank="species" authority="Linnaeus" date="1753">arborescens</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 397. 1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family hydrangeaceae;genus hydrangea;species arborescens</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">242416667</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Hydrangea</taxon_name>
    <taxon_name rank="species" authority="unknown" date="unknown">arborescens</taxon_name>
    <taxon_name rank="variety" authority="Torrey &amp; A. Gray" date="unknown">oblonga</taxon_name>
    <taxon_hierarchy>genus hydrangea;species arborescens;variety oblonga</taxon_hierarchy>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Wild or smooth hydrangea</other_name>
  <description type="morphology">Shrubs, 10–30 dm. Twigs strigose to hirsute, trichomes white. Leaves opposite; petiole 1.4–8.5(–11.5) cm, glabrous or glabrous abaxially and sparsely tomentose adaxially; blade ovate, elliptic-ovate, or broadly ovate, (2.7–)6–17.8 × (1.4–)2.5–12(–15.5) cm, unlobed, base cordate, truncate, or cuneate, margins dentate to serrate, apex acute to acuminate, abaxial surface green, glabrous or glabrate, or sparsely hirsute along midvein and sometimes along lateral veins, trichomes at 40× conspicuously tuberculate, 0.3–1 mm, adaxial surface green, glabrous or sparsely hirsute. Inflorescences compact, 100–500-flowered, dome-shaped to hemispheric, (3.3–)4–14 × 3.6–12 cm; peduncle 1.5–7.8 cm, sparsely tomentose. Pedicels 1–2.5 mm, glabrous or sparsely hirsute. Sterile flowers absent or present, white, greenish white, or yellowish white, tube 6–16 mm, lobes 3–4(–5), obovate to broadly ovate, round, or elliptic, 3.6–15 × 2.2–14 mm. Bisexual flowers: hypanthium adnate to ovary to near its apex, 0.7–1 × 0.8–1.2 mm, strongly 8–10(–11)-ribbed in fruit, glabrous; sepals deltate to triangular, 0.2–0.5 × 0.2–0.5 mm, margins entire, apex acute to acuminate, abaxial surface glabrous; petals caducous, white to yellowish white, elliptic to narrowly ovate, 1–1.5 × 0.6–1.1 mm; filaments 2–4.5 × 0.1–0.2 mm; anthers 0.3–0.5 mm; pistils 2(–3)-carpellate, ovary completely inferior or nearly so; styles 2(–3), distinct, 0.9–1.2 mm. Capsules hemispheric, 1.2–2.1 × 1.7–2.5 mm. Seeds 0.3–0.6(–0.8) mm. 2n = 36.</description>
  <discussion>Hydrangea arborescens has escaped from cultivation in Connecticut, Massachusetts, New Brunswick, and Nova Scotia; it is not native in those states or provinces.</discussion>
  <discussion>E. McClintock (1957) circumscribed Hydrangea arborescens as comprising three partly sympatric subspecies; subsp. arborescens, subsp. discolor, and subsp. radiata. R. E. Pilatowski (1980, 1982) concluded that these were best treated as three species (H. arborescens, H. cinerea, and H. radiata), citing chemical, morphological, reproductive, and geographic discontinuities among the taxa. Most herbarium specimens are easily referred to one of these species; occasional specimens appear intermediate between H. arborescens and H. cinerea.</discussion>
  <discussion>The Cherokee and Delaware tribes used bark and occasionally leaves from Hydrangea arborescens to prepare infusions or poultices to treat various internal and external ailments. The Cherokee used peeled twigs and branches to make tea or cooked twigs and branches as a vegetable (D. E. Moerman 1998).</discussion>
  <description type="phenology">Flowering (May–)Jun–Jul(–Aug).</description>
  <description type="habitat">Moist to dry deciduous forests and woods, moist slopes, shaded bluffs, ledges, stream banks.</description>
  <description type="elevation">70–2000 m.</description>
  <description type="distribution">N.B., N.S.; Ala., Ark., Conn., Del., D.C., Fla., Ga., Ill., Ind., Ky., La., Md., Mass., Miss., Mo., N.J., N.Y., N.C., Ohio, Okla., Pa., S.C., Tenn., Va., W.Va.</description>
</bio:treatment>
