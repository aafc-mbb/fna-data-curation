<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>Geoffrey A. Levin, Lynn J. Gillespie</author>
    </source>
    <other_info_on_meta type="treatment_page">156</other_info_on_meta>
    <other_info_on_meta type="mention_page">157</other_info_on_meta>
    <other_info_on_meta type="mention_page">193</other_info_on_meta>
    <other_info_on_meta type="mention_page">325</other_info_on_meta>
    <other_info_on_meta type="mention_page">328</other_info_on_meta>
    <other_info_on_meta type="mention_page">368</other_info_on_meta>
    <other_info_on_meta type="mention_page">411</other_info_on_meta>
    <other_info_on_meta type="mention_page">546</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
    <other_info_on_meta type="illustrator">Barbara Alongi</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">EUPHORBIACEAE</taxon_name>
    <taxon_hierarchy>family euphorbiaceae</taxon_hierarchy>
    <other_info_on_name type="fna_id">250101435</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Spurge Family</other_name>
  <description type="morphology">Herbs, subshrubs, shrubs, trees, or vines [lianas], annual, biennial, or perennial, deciduous or evergreen, monoecious or dioecious; latex present or absent. Leaves alternate, opposite, whorled, or fascicled on short shoots, simple (3-foliolate in Tragia laciniata) [palmately compound]; stipules present or absent; petiole present or absent; blade sometimes palmately lobed, margins entire, subentire, repand, crenate, serrate, or dentate; venation pinnate, palmate, or palmate at base and pinnate distally. Inflorescences unisexual or bisexual, axillary, terminal, or leaf-opposed [cauliflorous], racemes, panicles, spikes, thyrses, cymes, fascicles, or pseudanthia, or flowers solitary. Flowers unisexual; perianth hypogynous; hypanthium absent; sepals 0 or 2–12, distinct or connate basally to most of length; petals 0 or (3–)5(–6), distinct or connate; nectary present or absent; stamens 1–35(–1000), distinct or connate, free; anthers dehiscing by longitudinal slits; pistil 1, (1–)3–5(–20)-carpellate, ovary superior, (1–)3–5(–20)-locular, placentation axile; ovules 1 per locule, anatropous; styles 1–5(–9), distinct or connate, unbranched, 2-fid, or multifid; stigmas 1–32+. Fruits usually capsules, dehiscence septicidal, (usually schizocarpic with cocci separating from persistent columnella, coccus usually dehiscent loculicidally), sometimes schizocarps, drupes, or achenes [berries]. Seeds 1 per locule.</description>
  <discussion>Genera ca. 220, species ca. 6500 (24 genera, 259 species in the flora).</discussion>
  <description type="distribution">North America, Mexico, West Indies, Bermuda, Central America, South America, Eurasia, Africa, Atlantic Islands, Indian Ocean Islands, Pacific Islands, Australia; mostly tropical to warm temperate regions.</description>
  <discussion>Molecular phylogenetic studies have shown Euphorbiaceae, as traditionally treated (A. Radcliffe-Smith 2001; G. L. Webster 1994b, 2014), to be polyphyletic, forming several groups in Malpighiales (C. C. Davis et al. 2005; T. Tokuoka and H. Tobe 2006; K. Wurdack and Davis 2009; Z. Xi et al. 2012). The treatment here reflects those findings. Genera formerly placed in subfamilies Oldfieldioideae Eg. Köhler &amp; G. L. Webster and Phyllanthoideae Beilschmied, characterized by two ovules per locule, are treated as Picrodendraceae and Phyllanthaceae respectively, with two genera segregated from the latter as Putranjivaceae; the first two families appear to be sister taxa, possibly near much of the remaining traditional Euphorbiaceae, whereas the third is placed elsewhere in Malpighiales (Xi et al. 2012). The remaining genera, characterized by one ovule per locule, are treated as Euphorbiaceae in the strict sense and Peraceae Klotzsch (not represented in the flora area).</discussion>
  <discussion>Four subfamilies currently are recognized in the narrowly defined Euphorbiaceae (K. Wurdack et al. 2005; G. L. Webster 2014). Traditionally, three subfamilies, Acalyphoideae Beilschmied, Crotonoideae Beilschmied, and Euphorbioideae Beilschmied, were recognized based on laticifer presence or absence and pollen morphology (A. Radcliffe-Smith 2001; Webster 1994b). Phylogenetic analyses of molecular data (Wurdack et al.; T. Tokuoka 2007) support the monophyly of Euphorbioideae, represented in the flora area by Ditrysinia, Euphorbia, Gymnanthes, Hippomane, Hura, Microstachys, Pleradenophora, Stillingia, and Triadica. Most of the traditional Crotonoideae is moderately supported as monophyletic, including Astraea, Cnidoscolus, Croton, Jatropha, Manihot, and Vernicia in the flora area. These studies also strongly support the monophyly of most of Acalyphoideae (including Acalypha, Adelia, Argythamnia, Bernardia, Caperonia, Dalechampia, Mercurialis, Ricinus, and Tragia in the flora area), and segregation of a fourth, small subfamily, Cheilosoideae K. Wurdack &amp; Petra Hoffmann (not represented in the flora area). The remaining genera of Acalyphoideae and Crotonoideae, none of which are in the flora area, were placed in several small clades whose relationships to each other and to the four subfamilies were not well supported.</discussion>
  <discussion>Euphorbiaceae in the strict sense are diverse morphologically, but most are characterized by schizocarpic capsules in which the cocci separate from the persistent columella, often explosively; the seeds are dispersed when the cocci split septicidally and usually also loculicidally. Similar capsules also are found in many Phyllanthaceae and Picrodendraceae but, as noted above, they have two ovules per locule, versus one in Euphorbiaceae. White or whitish latex is found in Euphorbioideae and colored latex in many Crotonoideae, whereas Acalyphoideae lack latex. Pseudanthia evolved independently and are structurally different in Dalechampia and Euphorbia.</discussion>
  <discussion>Euphorbiaceae are most species-rich and ecologically important in tropical and subtropical regions. The same is true of genera found in the flora area, many of which are represented there only by a small proportion of their global diversity.</discussion>
  <discussion>Notable economically important Euphorbiaceae are Hevea Aublet, a major source of rubber; Manihot, cassava or manioc, the starchy tubers of which are a major food source in much of the tropics; Ricinus, the source of castor oil; and Vernicia (and its close relative Aleurites J. R. Forster &amp; G. Forster), sources of tung and other finishing oils. Some species of Euphorbiaceae are used horticulturally, especially members of Euphorbia; probably the best known of these is the poinsettia, E. pulcherrima Willdenow ex Klotzsch. Also well-known is Codiaeum variegatum (Linnaeus) A. Jussieu, the horticultural croton. Some introduced Euphorbiaceae have become problematic invasives in the flora area, particularly in areas with mild climates. Most notable among these invasive species are leafy spurge (Euphorbia virgata, commonly incorrectly called E. esula), Chinese tallowtree (Triadica sebifera), and tung-oil tree (Vernicia fordii).</discussion>
  <discussion>Mallotus japonicus (Linnaeus f.) Müller Arg., food wrapper plant, has escaped locally in Durham and Orange counties, North Carolina. The Orange County population may have been eradicated and the status of the Durham County population is not known. This dioecious shrub or small tree, native to eastern Asia, has stellate hairs and in the key below would come under the first lead of couplet 5 with Astraea, Bernardia, and Croton. It can be distinguished from those three genera by its pale yellow to white glandular scales on the leaves and stems, staminate flowers with 70–100 stamens, unbranched styles, and ovaries and capsules covered with soft spines and reddish orange glandular scales.</discussion>
  <discussion>Sapium haematospermum Müller Arg. from South America was collected on ballast in Pensacola, Florida, in 1901; this collection generally has been incorrectly reported as S. glandulosum (Linnaeus) Morong. Although the species does not appear to have become naturalized in the flora area, it could become adventive in subtropical areas. In the key below, S. haematospermum would come under the second lead of couplet 12 and can be distinguished from the five genera there by its combination of petioles with apical glands, inflorescences with two glands subtending each bract, staminate flowers with two to three sepals that are connate basally, pistillate flowers with three-carpellate pistils bearing three styles, and seeds covered in a red aril.</discussion>
  <discussion>In the key and descriptions below, laminar glands are those borne on the surface of the leaf blade, not those extending from the margins or teeth or borne at the leaf base at the junction with the petiole.</discussion>
  <references>
    <reference>Punt, W. 1962. Pollen morphology of the Euphorbiaceae with special reference to taxonomy. Wentia 7: 1–116.</reference>
    <reference>Radcliffe-Smith, A. 2001. Genera Euphorbiacearum. Kew.</reference>
    <reference>Tokuoka, T. 2007. Molecular phylogenetic analysis of Euphorbiaceae sensu stricto based on plastid and nuclear DNA sequences and ovule and seed character evolution. J. Pl. Res. 120: 511–522.</reference>
    <reference>Webster, G. L. 1967. The genera of Euphorbiaceae in the southeastern United States. J. Arnold Arbor. 48: 303–361, 363–430.</reference>
    <reference>Webster, G. L. 1994. Synopsis of the genera and suprageneric taxa of Euphorbiaceae. Ann. Missouri Bot. Gard. 81: 33–144.</reference>
    <reference>Webster, G. L. 1994b. Classification of the Euphorbiaceae. Ann. Missouri Bot. Gard. 81: 3–32.</reference>
    <reference>Webster, G. L. 2014. Euphorbiaceae. In: K. Kubitzki et al., eds. 1990+. The Families and Genera of Vascular Plants. 10+ vols. Berlin etc. Vol. 11, pp. 51–216.</reference>
    <reference>Wurdack, K., P. Hoffmann, and M. W. Chase. 2005. Molecular phylogenetic analysis of uniovulate Euphorbiaceae (Euphorbiaceae sensu stricto) using plastid rbcL and trnL-F DNA sequences. Amer. J. Bot. 92: 1397–1420.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Inflorescences pseudanthia, each consisting of an involucre enclosing 1 or 3 pistillate flowers and (0–)1–80 staminate flowers.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Vines; pseudanthia with involucre of 2 showy bracts and 3 pistillate flowers; latex absent.</description>
      <determination>9 Dalechampia</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Herbs, subshrubs, or shrubs; pseudanthia with cuplike involucre and 1 pistillate flower; latex white.</description>
      <determination>24 Euphorbia</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Inflorescences thyrses, cymes, racemes, panicles, spikes, or fascicles, or flowers solitary.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stinging hairs present (sometimes inconspicuous in Tragia except on ovaries and capsules).</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Sepals usually green, sometimes reddish green, not petaloid; inflorescences racemes; petioles without glands at apices; latex absent.</description>
      <determination>8 Tragia</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Sepals white, petaloid; inflorescences dichasial cymes; petioles with glands at apices; latex white.</description>
      <determination>11 Cnidoscolus</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stinging hairs absent.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Hairs stellate or scalelike, sometimes also unbranched.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Stamens ± straight in bud; laminar glands abaxial, crateriform; staminate petals 0, nectaries intrastaminal; latex absent; caruncle absent.</description>
      <determination>7 Bernardia</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Stamens inflexed in bud; laminar glands absent; staminate petals 0 or (3–)5(–6), nectaries extrastaminal; latex usually present, colorless to reddish; caruncle present.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaf blades usually palmately lobed, sometimes unlobed; hairs unbranched and stellate; pistillate sepals usually not touching in bud; seeds rectangular-oblong.</description>
      <determination>13 Astraea</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaf blades unlobed; hairs stellate or scalelike; pistillate sepals imbricate or valvate; seeds ellipsoid, oblong, ovoid, globose, or lenticular.</description>
      <determination>14 Croton</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Hairs unbranched, malpighiaceous, 2-fid, or absent.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaves opposite or subopposite.</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Latex absent; plants dioecious; inflorescences axillary; capsules hispid.</description>
      <determination>2 Mercurialis</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Latex white; plants monoecious; inflorescences terminal; capsules glabrous.</description>
      <determination>23 Stillingia</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaves alternate or fascicled.</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Glands subtending each inflorescence bract 2 or 10–14.</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Leaf blades palmately lobed; stamens to 1000; pistillate sepals 5; styles 2-fid.</description>
      <determination>1 Ricinus</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Leaf blades unlobed; stamens 2–3; pistillate sepals 0 or 2–3; styles unbranched.</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Pistils 6–9-carpellate; fruits drupes.</description>
      <determination>21 Hippomane</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Pistils 2–3-carpellate; fruits capsules.</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Glands subtending inflorescence bracts 10–14; pistillate sepals 2; pistils 2-carpellate; styles 2.</description>
      <determination>22 Pleradenophora</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Glands subtending inflorescence bracts 2; pistillate sepals 0 or 3; pistils 3-carpellate; styles 3.</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Petioles with glands at apices; staminate sepals connate most of length; outer seed coat fleshy.</description>
      <determination>16 Triadica</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Petioles without glands at apices; staminate sepals distinct or connate basally; outer seed coat dry.</description>
      <next_statement_id>15</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Plants without hairs; staminate sepals 2; stamens 2; capsule base persisting as 3-lobed gynobase.</description>
      <determination>23 Stillingia</determination>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Plants with hairs; staminate sepals 3; stamens 3; capsule base not persisting.</description>
      <next_statement_id>16</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Herbs, annual; latex white; leaf blade margins serrulate.</description>
      <determination>18 Microstachys</determination>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Shrubs; latex absent; leaf blade margins entire.</description>
      <determination>19 Ditrysinia</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Glands subtending each inflorescence bract 0.</description>
      <next_statement_id>17</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Petals 5(–6) (pistillate sometimes rudimentary or 0 in Argythamnia).</description>
      <next_statement_id>18</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Sepals 2(–3); petioles with glands at apices; petals longer than 24 mm; inflorescences paniclelike thyrses.</description>
      <determination>15 Vernicia</determination>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Sepals 5–8(–10); petioles without glands at apices (sometimes stipitate-glandular or with tack-shaped glands along length); petals less than 18 mm or absent; inflorescences cymes, fascicles, racemes, or spikes, or flowers solitary.</description>
      <next_statement_id>19</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Latex colorless, cloudy-whitish, yellow, or red; inflorescences cymes or fascicles, or flowers solitary; staminate sepals imbricate; leaf blades palmately lobed or unlobed.</description>
      <determination>12 Jatropha</determination>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Latex absent; inflorescences spikes or racemes; staminate sepals valvate; leaf blades unlobed.</description>
      <next_statement_id>20</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Leaf blade secondary veins arcuate, not closely spaced; styles 2-fid, branches 6 per flower; capsules not muricate; flower nectaries 5 glands; hairs usually malpighiaceous, sometimes unbranched, rarely absent.</description>
      <determination>4 Argythamnia</determination>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Leaf blade secondary veins straight, closely spaced; styles deeply multifid, branches 12–21 per flower; capsules muricate; flower nectaries absent; hairs unbranched.</description>
      <determination>5 Caperonia</determination>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Petals 0 (sepals petaloid in Manihot).</description>
      <next_statement_id>21</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Petioles with conspicuous lateral glands at apex; pistils 5–20-carpellate; style 1; stamens 10–80, connate entire length forming thick column; trees, trunks with broad-based conic thorns.</description>
      <determination>20 Hura</determination>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Petioles with inconspicuous adaxial glands at apex or glands absent; pistils (1–)3(–4)-carpellate; styles (1–)3(–4), equal to carpel number; stamens (2–)4–17, distinct or connate basally; herbs, subshrubs, shrubs, or trees, unarmed (branchlets sometimes stiff and thorn-tipped in Adelia).</description>
      <next_statement_id>22</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>22</statement_id>
      <description type="morphology">Inflorescences racemes or panicles; staminate sepals 5, petaloid, 7–20 mm, connate 1/2 length; latex white; leaf blades usually deeply palmately lobed, rarely unlobed.</description>
      <determination>10 Manihot</determination>
    </key_statement>
    <key_statement>
      <statement_id>22</statement_id>
      <description type="morphology">Inflorescences fascicles or racemelike or spikelike thyrses, or flowers solitary; staminate sepals 0 or 4–5, not petaloid, 1–2 mm, distinct; latex colorless or absent; leaf blades unlobed.</description>
      <next_statement_id>23</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>23</statement_id>
      <description type="morphology">Sepals 0; latex colorless; styles unbranched; stamens (2–)4(–5).</description>
      <determination>17 Gymnanthes</determination>
    </key_statement>
    <key_statement>
      <statement_id>23</statement_id>
      <description type="morphology">Staminate sepals 4–5, pistillate sepals 3 or 5(–6); latex absent; styles usually multifid or laciniate, rarely 2-fid or unbranched; stamens 4–8 or 14–17.</description>
      <next_statement_id>24</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>24</statement_id>
      <description type="morphology">Leaves alternate, blade margins serrate or crenate to subentire; inflorescences spikelike thyrses; bracts subtending pistillate flowers enlarging in fruit; stamens 4–8; pistillate sepals 3.</description>
      <determination>3 Acalypha</determination>
    </key_statement>
    <key_statement>
      <statement_id>24</statement_id>
      <description type="morphology">Leaves fascicled on short shoots, blade margins entire; inflorescences fascicles or flowers solitary; bracts subtending pistillate flowers minute, not enlarging in fruit; stamens 14–17; pistillate sepals 5(–6).</description>
      <determination>6 Adelia</determination>
    </key_statement>
  </key>
</bio:treatment>
