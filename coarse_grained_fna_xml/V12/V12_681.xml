<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="treatment_page">375</other_info_on_meta>
    <other_info_on_meta type="mention_page">374</other_info_on_meta>
    <other_info_on_meta type="mention_page">376</other_info_on_meta>
    <other_info_on_meta type="illustration_page">370</other_info_on_meta>
    <other_info_on_meta type="volume">12</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="de Candolle ex Perleb" date="unknown">LINACEAE</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">LINUM</taxon_name>
    <taxon_name rank="section" authority="unknown" date="unknown">Linum</taxon_name>
    <taxon_name rank="species" authority="Pursh" date="1813">lewisii</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Amer. Sept.</publication_title>
      <place_in_publication>1: 210. 1813</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family linaceae;genus linum;section linum;species lewisii;</taxon_hierarchy>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">250101693</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Linum</taxon_name>
    <taxon_name rank="species" authority="Linnaeus" date="unknown">perenne</taxon_name>
    <taxon_name rank="subspecies" authority="(Pursh) Hultén" date="unknown">lewisii</taxon_name>
    <taxon_hierarchy>genus linum;species perenne;subspecies lewisii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">L.</taxon_name>
    <taxon_name rank="species" authority="unknown" date="unknown">perenne</taxon_name>
    <taxon_name rank="variety" authority="(Pursh) Eaton &amp; Wright" date="unknown">lewisii</taxon_name>
    <taxon_hierarchy>genus l.;species perenne;variety lewisii</taxon_hierarchy>
  </taxon_identification>
  <number>3.</number>
  <other_name type="common_name">Lewis's or wild blue flax</other_name>
  <description type="morphology">Herbs, perennial, 5–80 cm, glabrous or glabrate throughout, ± glaucous. Stems erect to spreading or ascending, branched from near base and in inflorescence. Leaves: blade linear to linear-lanceolate or linear-oblanceolate, 5–30 × 0.5–3(–4.5) mm. Inflorescences open panicles or racemes. Pedicels 5–20 mm. Flowers homostylous; sepals elliptic or elliptic-ovate, 3.5–6 mm, margins glabrous, apex acute; petals usually blue, sometimes white, base whitish or yellowish, cuneate-obovate, 6–23 mm; stamens 3–10 mm; anthers 1–2.2 mm; staminodia present; styles distinct, 2–12 mm; stigmas thickened ellipsoid-capitate. Capsules ovoid globose, 4–8 × 5–6 mm, apex acute, segments ± persistent on plant, margins arachnoid-ciliate. Seeds 2.5–5 × 1.5–3 mm. 2n = 18.</description>
  <discussion>Varieties 3 (3 in the flora).</discussion>
  <description type="distribution">Alta., B.C., Man., N.W.T., Nunavut, Ont., Que., Sask., Yukon; Alaska, Ariz., Ark., Calif., Colo., Idaho, Kans., La., Minn., Mo., Mont., N.Dak., N.Mex., Nebr., Nev., Okla., Oreg., S.Dak., Tex., Utah, W.Va., Wash., Wyo.; n Mexico.</description>
  <discussion>Linum lewisii grows in many habitats in western North America from northern Mexico to Alaska east to the Great Plains in the United States and to the west side of Hudson and James bays in Canada; it appears to be less common in the Great Basin. A component of wildflower seed mixes, the species may be expanding its range. Some authors have considered it conspecific with L. perenne, and many collections in herbaria are identified as L. perenne without an indication of variety; they are most likely L. lewisii var. lewisii (D. J. Ockendon 1971; C. M. Rogers 1984). Because of the prevalence of L. bienne, L. perenne, and L. usitatissimum in bird seed and wildflower mixes, it may be that these three non-natives are becoming more common than in the past. Capitate stigmas distinguish L. lewisii from L. bienne and L. usitatissimum, which have linear or clavate stigmas. Distinguishing L. lewisii from L. perenne is more difficult: the size of flower parts in the homostyled L. lewisii varies along elevational and latitudinal gradients, with smaller flowers and flower parts in higher elevations and higher latitudes; except in var. lepagei, the styles are always longer than the stamens. In the heterostyled L. perenne, populations usually include plants in which flowers have stamens much longer than the very short styles (short-styled form) and plants in which flowers have stamens much shorter than the very long styles, up to twice as long as the stamens (long-styled form).</discussion>
  <discussion>C. A. Kearns and D. W. Inouye (1994) reported that Linum lewisii is facultatively autogamous but tends not to set seed in the absence of pollinators; small bees and flies are the most common pollinators. A. Cronquist et al. (1997b) reported unusual populations of L. lewisii on sandy soil in Nye County, Nevada, in the 40-Mile-Canyon drainage, that had persistent, ascending, pale blue petals with darker veins.</discussion>
  <references>
    <reference>Becker, J. D. T. 2010. Taxonomy of the Linum lewisii Complex in Canada Based on Macromorphology, Micromorphology, and Phytogeography. Honors thesis. University of Manitoba.</reference>
    <reference>Kearns, C. A. and D. W. Inouye. 1994. Fly pollination in Linum lewisii (Linaceae). Amer. J. Bot. 81: 1091–1095.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals mostly white; Hudson and James Bay regions.</description>
      <determination>3c. Linum lewisii var. lepagei</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Petals usually blue; w North America.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Petals (8–)12–23 mm; styles 6–12 mm.</description>
      <determination>3a. Linum lewisii var. lewisii</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Petals 6–13 mm; styles 2–6 mm.</description>
      <determination>3b. Linum lewisii var. alpicola</determination>
    </key_statement>
  </key>
</bio:treatment>
