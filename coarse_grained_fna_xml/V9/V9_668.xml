<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <other_info_on_meta type="treatment_page">401</other_info_on_meta>
    <other_info_on_meta type="mention_page">399</other_info_on_meta>
    <other_info_on_meta type="illustration_page">397</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">rosaceae</taxon_name>
    <taxon_name rank="subfamily" authority="Arnott" date="unknown">amygdaloideae</taxon_name>
    <taxon_name rank="tribe" authority="de Candolle" date="unknown">spiraeeae</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">spiraea</taxon_name>
    <taxon_name rank="species" authority="Du Roi" date="unknown">alba</taxon_name>
    <place_of_publication>
      <publication_title>Harbk. Baumz.</publication_title>
      <place_in_publication>2: 430. 1772</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe spiraeeae;genus spiraea;species alba</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="fna_id">242417310</other_info_on_name>
  </taxon_identification>
  <number>1.</number>
  <other_name type="common_name">Narrow-leaf</other_name>
  <other_name type="common_name">eastern</other_name>
  <other_name type="common_name">or white meadowsweet</other_name>
  <other_name type="common_name">spirée blanche</other_name>
  <description type="morphology">Shrubs, 10–20 dm. Stems erect, unbranched. Leaves: petiole 2–8 mm, puberulent or sparsely hairy; blade narrowly lanceolate to oblanceolate or broadly oblanceolate to obovate, 2–9 × 0.5–3 cm, length 3–5 times width, chartaceous or membranous, base cuneate to rounded, margins finely to coarsely, sharply serrate to serrulate (sometimes doubly so on long shoot leaves), number of primary and secondary serrations 0.5–1.1 times number of secondary veins (excluding inter-secondary veins), venation pinnate craspedodromous, secondary veins not prominent, irregularly terminating in primary teeth, inter-secondary veins usually 8–12+ per leaf, apex acute to obtuse, abaxial surface mostly glabrous, adaxial glabrous. Inflorescences mostly terminal, narrowly conic to open, pyramidal panicles, 5–20 × 3–10 cm height 1.4–3.5 times diam.; branches usually in axils of leaves, puberulent to pubescent. Pedicels 1–2(–3) mm, glabrous or glabrate. Flowers 3–8 mm diam.; hypanthia hemispheric, 0.6–0.8 mm, abaxial surface usually puberulent to sparsely strigose, sometimes glabrate or glabrous, adaxial glabrous; sepals triangular, 0.8–1.5 mm; petals usually white, sometimes pink-tinged (in bud), suborbiculate, 1.3–2(–3) mm; staminodes 0–4; stamens 30–50, 1–2 times petal length. Follicles oblanceoloid, 3–4 mm, shiny, glabrous.</description>
  <description type="distribution">Alta., Man., N.B., N.S., Ont., P.E.I., Que., Sask.; Conn., Del., Ill., Ind., Iowa, Ky., Maine, Mass., Md., Mich., Minn., Mo., N.C., N.Dak., N.H., N.J., N.Y., Ohio, Pa., R.I., S.Dak., Tenn., Va., Vt., W.Va., Wis.; introduced in Europe.</description>
  <discussion>Varieties 2 (2 in the flora).</discussion>
  <discussion>Varieties alba and latifolia have regions of hybridization, primarily around the Great Lakes, that produce intermediate forms that may be difficult to key. Recognition of these two taxa as either species or varieties has been problematic because the taxa are quite distinct at the extremes of their range. A. R. Kugel (1958) recognized them as distinct species and her work illustrates the regional zone of hybridization. H. A. Gleason (1952) and Gleason and A. Cronquist (1963) also recognized the two taxa as separate species; later Gleason and Cronquist (1991) recognized them as varieties that frequently intergrade.</discussion>
  <discussion>The European Spiraea salicifolia has a leaf morphology that is similar to that of var. alba and the two taxa have been treated as conspecific. With the interest in Spiraea as an ornamental, S. salicifolia was imported to North America, resulting in this species or hybrids of it becoming naturalized. Specimens that are difficult to identify may be S. salicifolia, as escapes from homesteads and gardens that became established or may have hybridized with native taxa.</discussion>
  <discussion>Spiraea alba has become locally naturalized in western and central Europe; in the British Isles, its hybrids are commonly naturalized as S. ×rosalba Dippel (S. alba × S. salicifolia), or as S. ×billardii Hortus ex K. Koch (S. alba × S. douglasii) (A. J. Silverside 1990).</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves: lengths 3–4 times widths, margins finely serrate to serrulate; inflorescences narrowly conic.</description>
      <determination>1a Spiraea alba var. alba</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves: lengths 2–3 times widths, margins coarsely serrate; inflorescences open, pyramidal.</description>
      <determination>1b Spiraea alba var. latifolia</determination>
    </key_statement>
  </key>
</bio:treatment>
