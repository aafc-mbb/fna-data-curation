<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <other_info_on_meta type="treatment_page">536</other_info_on_meta>
    <other_info_on_meta type="mention_page">516</other_info_on_meta>
    <other_info_on_meta type="mention_page">525</other_info_on_meta>
    <other_info_on_meta type="mention_page">526</other_info_on_meta>
    <other_info_on_meta type="mention_page">531</other_info_on_meta>
    <other_info_on_meta type="mention_page">532</other_info_on_meta>
    <other_info_on_meta type="mention_page">549</other_info_on_meta>
    <other_info_on_meta type="mention_page">606</other_info_on_meta>
    <other_info_on_meta type="mention_page">607</other_info_on_meta>
    <other_info_on_meta type="mention_page">628</other_info_on_meta>
    <other_info_on_meta type="mention_page">629</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">rosaceae</taxon_name>
    <taxon_name rank="subfamily" authority="Arnott" date="unknown">amygdaloideae</taxon_name>
    <taxon_name rank="tribe" authority="Small" date="1933">MALEAE</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">crataegus</taxon_name>
    <taxon_name rank="section" authority="unknown" date="unknown">Coccineae</taxon_name>
    <taxon_name rank="series" authority="(Loudon) Rehder" date="1940">Crus-galli</taxon_name>
    <place_of_publication>
      <publication_title>Man. Cult. Trees ed.</publication_title>
      <place_in_publication>2, 364. 1940</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe maleae;genus crataegus;section coccineae;series crus-galli;</taxon_hierarchy>
    <other_info_on_name type="fna_id">318085</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Crataegus</taxon_name>
    <taxon_name rank="section" authority="Loudon" date="unknown">Crus-galli</taxon_name>
    <place_of_publication>
      <publication_title>Arbor. Frutic. Brit.</publication_title>
      <place_in_publication>2: 820. 1838</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Crataegus;section Crus-galli</taxon_hierarchy>
  </taxon_identification>
  <number>64f.2.</number>
  <description type="morphology">Shrubs or trees, 10–100 dm, main trunk dominant (more commonly in taller plants). Stems: trunk bark older gray-ochre, usually fibrous, checked into longitudinal plates, freshly exposed bark not recorded; compound thorns on trunks (of larger individuals) usually present; twigs: new growth usually glabrous, sometimes pilose, 1-year old dark gray, pale brown, tan, brown, or orange-brown, 2-years old usually medium pale gray; thorns on twigs numerous, straight to recurved, sometimes reflexed, 2-years old dark gray to blackish or chestnut brown, medium to stout, (2–)3–6.5 cm. Leaves: petiole usually short, length 0–18(–31)% blade, usually glabrous, sometimes villous or glabrescent, usually eglandular, sometimes glandular; blade paler green abaxially, bright, glossy, deep green adaxially, narrowly oblanceolate to obovate, sometimes elliptic or ± rhombic, 2–7 cm (usually much longer than wide), usually coriaceous, sometimes thin and firm, base cuneate, tapered, or narrowed, lobes 0 or 1–3 per side, sinuses shallow, lobe apex acute, margins usually finely and evenly serrate, sometimes crenate, except near bases, or toothed only in distal part, venation semicamptodromous to craspedodromous, veins 4–6(–9) per side, apex acute to obtuse, rarely rounded, short-pointed, surfaces glabrous or hairy. Inflorescences 5–20-flowered; branches glabrous or hairy, sparsely glandular-punctate; bracteoles caducous (not seen in C. schizophylla), narrow, membranous, margins sparsely glandular. Flowers (8–)10–20 mm diam.; hypanthium glabrous, pilose, or villous; sepals narrowly triangular, much shorter than petals, margins entire or irregularly glandular-serrate; stamens 10–20, anthers ivory, cream, pink, dark purple, or red; styles 1–5. Pomes rose to deep red, sometimes orange-red, rarely yellow, suborbicular to ± oblong, (7–)8–15 mm diam., glabrous or pubescent; flesh hard, becoming mealy; sepals usually spreading to reflexed; pyrenes 1–5.</description>
  <description type="distribution">North America, Mexico; introduced in Europe.</description>
  <discussion>Species 7 (5 in the flora).</discussion>
  <discussion>Members of ser. Crus-galli are common hawthorns to the east of a line from central Texas to southern Minnesota and have a southeastern limit in north-central Florida. Members of ser. Crus-galli are less shade-tolerant than most American hawthorns of the southeast, rarely succeeding in woodland understory.</discussion>
  <discussion>Members of ser. Crus-galli are usually recognized by their rather narrow leaves more or less coriaceous, shiny, fairly dark green, lobes absent, usually short-petiolate strongly tapered to the base, and rather late anthesis relative to sympatric hawthorns. The caducous bracteoles resemble those of ser. Virides in being especially narrow and often sparsely glandular. Sporadic yellow-fruited variants, some of which have been named, are well known in this series.</discussion>
  <discussion>Series Crus-galli constitutes one of the most distinctive groups of hawthorns, but taxonomy of the species is difficult. A large range of variation is subsumed in Crataegus crus-galli, less in species like C. fecunda. E. J. Palmer (1925) cited well over 100 names for the series (C. persimilis, which keys out in the second couplet below, is an interserial hybrid). Stamen number and anther color in the more common species as interpreted here are rather inconstant compared to those in many Crataegus series. Rare putative hybrids with ser. Punctatae and ser. Virides are known. The distinctive C. ×permixta E. J. Palmer, from Illinois and Missouri, was placed in ser. Crus-galli by Palmer (1952), but its very broad and somewhat lobed leaves suggest hybridity with ser. Virides.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sepal margins subentire or shallowly to deeply glandular-serrate; petiole length 25+% blade</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Sepal margins ± entire; petiole length to 20% blade</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Young leaves adaxially scabrous; leaf apices acute; sepal margins subentire or glandular-serrate; pyrene sides plane; 1-year old twigs orange-brown.</description>
      <determination>38 Crataegus fecunda</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Young leaves adaxially glabrous except for midvein; leaf apices broadly acute; sepal margins deeply glandular-serrate; pyrene sides plane to deeply excavated; 1-year old twigs purplish brown.</description>
      <determination>166 Crataegus persimilis</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves hairy adaxially young, glabrescent or becoming scabrous; inflorescence branches densely pubescent.</description>
      <determination>37 Crataegus berberifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaves glabrous or sparsely hairy adaxially young, except sometimes for hairy midveins; inflorescence branches usually glabrous</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Most short-shoot leaf lobes 1–3 per side.</description>
      <determination>36 Crataegus schizophylla</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Short-shoot leaf lobes 0</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaf blades ± orbiculate to broadly elliptic; styles and pyrenes (1–)3–5.</description>
      <determination>34 Crataegus reverchonii</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaf blades broadly spatulate to narrowly oblanceolate or narrowly elliptic; styles and pyrenes 1 or 2(or 3).</description>
      <determination>35 Crataegus crus-galli</determination>
    </key_statement>
  </key>
</bio:treatment>
