<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <other_info_on_meta type="treatment_page">579</other_info_on_meta>
    <other_info_on_meta type="mention_page">528</other_info_on_meta>
    <other_info_on_meta type="mention_page">529</other_info_on_meta>
    <other_info_on_meta type="mention_page">580</other_info_on_meta>
    <other_info_on_meta type="mention_page">583</other_info_on_meta>
    <other_info_on_meta type="mention_page">584</other_info_on_meta>
    <other_info_on_meta type="mention_page">586</other_info_on_meta>
    <other_info_on_meta type="mention_page">591</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">rosaceae</taxon_name>
    <taxon_name rank="subfamily" authority="Arnott" date="unknown">amygdaloideae</taxon_name>
    <taxon_name rank="tribe" authority="Small" date="1933">MALEAE</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">crataegus</taxon_name>
    <taxon_name rank="section" authority="Loudon" date="1838">coccineae</taxon_name>
    <taxon_name rank="series" authority="unknown" date="1974">Pulcherrimae</taxon_name>
    <place_of_publication>
      <publication_title>J. Arnold Arbor.</publication_title>
      <place_in_publication>55: 628. 1974</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe maleae;genus crataegus;section coccineae;series pulcherrimae;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">318068</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Crataegus</taxon_name>
    <taxon_name rank="unranked" authority="Beadle" date="unknown">Pulcherrimae</taxon_name>
    <place_of_publication>
      <publication_title>in J. K. Small, Fl. S.E. U.S.,</publication_title>
      <place_in_publication>532, 535. 1903</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Crataegus;unranked Pulcherrimae</taxon_hierarchy>
  </taxon_identification>
  <number>64f.13.</number>
  <description type="morphology">Shrubs or trees, 20–70 dm, usually main trunk dominant. Stems: trunk bark gray, often rough or corrugated; branches spreading; twigs ± straight, new growth glabrous, 1-year old reddish brown to blackish brown, often shiny, 2-years old deep gray, sometimes grayish red or gray-brown; thorns on twigs few to numerous, straight to slightly recurved, 2-years old very dark brown to black, ± slender, (1.5–)2–3(–5) cm. Leaves: petiole length 20–50(–55)% blade, glabrous, sparsely to densely sessile-glandular, sometimes stipitate-glandular; blade mid to dark green, narrowly elliptic and lanceolate to trullate or deltate, (2.5–)4–7(–8) cm, thin or chartaceous, base ± cuneate to rounded, lobes 0 or (1–)3 or 4(or 5) per side, sinuses usually shallow, lobe apex obtuse to acute, sometimes ± cuspidate, margins crenate to serrate, venation craspedodromous, veins 4–9 per side, apex acuminate to obtuse, surfaces glabrous. Inflorescences (2–)5–10(or 11)-flowered, slightly domed panicles; branches usually glabrous, sometimes sparsely pilose; bracteoles usually caducous, sometimes ± persistent, usually numerous, linear to narrowly oblong or very narrowly obovate, membranous to subherbaceous, margins stipitate-glandular. Flowers (13–)14–18(–25) mm diam.; hypanthium glabrous; sepals narrowly triangular or triangular, much shorter than petals, margins ± glandular-serrate; stamens 20, anthers usually pink to purple, sometimes ivory to cream; styles 3–5. Pomes yellow, orange, ruddy, red, red-purple, or green, broadly ellipsoid to suborbicular, 5–13(–14) mm diam., glabrous; flesh ± hard to mealy; sepals on collar, usually reflexed, sometimes spreading, or erose, non-accrescent; pyrenes 2–5.</description>
  <description type="distribution">se United States.</description>
  <discussion>Species 10 (10 in the flora).</discussion>
  <discussion>Series Pulcherrimae has a southern limit along the Gulf Coast of eastern Texas to northern Florida. Its species are fairly common over much of Mississippi and Alabama, the panhandle of Florida, and the western part of Georgia. There are also sporadic records east to South Carolina, one record in southern Tennessee, and one locality for North Carolina in Buncombe County. Anthesis is in April through much of Alabama, Georgia, and Mississippi; in the panhandle of Florida, it may start late March, while in North Carolina or Tennessee, it is in May. Fruit is ripe anywhere from late August to early October.</discussion>
  <discussion>Species of ser. Pulcherrimae are mesophytes of woodland edge or woodlands, and if the latter, usually under oak or oak-mixed canopy but not in denser shade. They are recorded mainly on finer-grained soils.</discussion>
  <discussion>The deeply corrugated bark of several species, found in few other series, glabrous or nearly glabrous plant parts, about 20 stamens, more or less glandular-bracteolate inflorescences, usually reflexed, somewhat elevated sepals in fruit, and sometimes large numbers of lateral veins in the leaf make ser. Pulcherrimae distinctive. Series Intricatae differs in usually possessing ten stamens, having some very hairy species, northern limits much farther north and southern limits somewhat to the north. As in ser. Intricatae, yellowish fruited forms are at least as numerous as red-fruited ones. Because of the relative scarcity of many species, taxonomic limits are sometimes somewhat difficult to be certain about and may be resolved differently in the future. Crataegus pearsonii Ashe, a poorly understood member of ser. Lacrimatae, quite often has been used by Louisiana and Texas authors for species of ser. Pulcherrimae from those states.</discussion>
  <references>
    <reference>Phipps, J. B. and K. A. Dvorsky. 2006b. Review of Crataegus series Pulcherrimae (Rosaceae). Sida 22: 973–1007.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades: lobes 0 or obscure, sinuses very shallow, LII 0–10%</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf blades: lobes (1 or)2–4(or 5) per side, sinuses shallow to deep, LII 10–50%</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades ovate-trullate.</description>
      <determination>79 Crataegus sargentii</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blades broadly elliptic to narrowly ovate or broadly lanceolate</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blades narrowly ovate to broadly lanceolate, apices acute to acuminate, veins 5–7(–9) per side, margins serrulate; pomes greenish yellow to yellow.</description>
      <determination>80 Crataegus gilva</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blades broadly oblong to elliptic or narrowly ovate, apices subacute to obtuse, veins 8 or 9 per side, margins crenate or crenate-serrate; pomes yellow-green with pink or salmon blush, or red.</description>
      <determination>81 Crataegus mendosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf blades narrower (length/width = 1.5–2)</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf blades broader (length/width = 1.2–1.5)</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Lobes 3 or 4 per side, sinuses deep (max LII 25–50%), lobe apices usually ± narrowly cuspidate.</description>
      <determination>87 Crataegus incilis</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Lobes 1–4 per side, sinuses shallow (max LII 10–30%), lobe apices subacute to obtuse</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaf blade margins with extremely small teeth; pomes dull red; petioles sometimes densely glandular.</description>
      <determination>83 Crataegus pinetorum</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaf blade margins with average-size to large teeth; pomes greenish yellow to deep yellow, sometimes flushed red or purplish red; petioles of low to average glandularity</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Lobes 1–3 per side, lobe apex acute distally, sides ± straight; pomes greenish yellow to yellow.</description>
      <determination>80 Crataegus gilva</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Lobes 2–4 per side, lobe apex obtuse to subacute, sides ± bowed; pomes deep yellow, sometimes flushed red or purplish red.</description>
      <determination>82 Crataegus pulcherrima</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaf blades ovate-trullate, distal 1/3 often unlobed, terminal 1/2 ± straight-sided across lobe tips, 4–6(–7) cm, lobes 1–3 per side, sinuses shallow, lobe apices acute; anthers pale pink to pale purple; pomes yellow or flushed pink, sometimes reddish.</description>
      <determination>79 Crataegus sargentii</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaf blades of quite different form, if ± trullate-ovate, then terminal 1/2 ± rounded across lobe apices, 2.5–8 cm, lobes 0 or 1–4(or 5) per side, sinuses shallow to deep, lobe apices obtuse to acute; anthers ivory to cream or pink to purple; pomes greenish to deep yellow, sometimes flushed red, purplish red, or red</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Leaf blades: sinuses deep (LII 15–30%)</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Leaf blades: sinuses shallow (LII 10–15%)</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Leaf blades 4.5–8 cm, ovate-rhombic to ovate-trullate, lobes angled; anthers cream to pale or deep purple; pomes red; flowering mid May, flowers 22–25 mm diam.</description>
      <determination>88 Crataegus eximia</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Leaf blades 3–6 cm, ovate to broadly ovate or ± deltate, trullate, or rhombic-ovate, lobes angled or not; anthers cream or pink to pale or deep purple; pomes yellow to green or red, sometimes ivory to cream; flowering early to mid April, flowers 16–19 mm diam</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Lobe apices ± obtuse to subacute.</description>
      <determination>85 Crataegus opima</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Lobe apices ± acute.</description>
      <determination>86 Crataegus tecta</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Lobe apices acute to acuminate young</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Lobe apices obtuse to subacute young</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Leaf blades narrowly ovate, lobe apices acute distally, (straight, ± unchanging with age); angle from widest lobes to tip ca. 60°.</description>
      <determination>80 Crataegus gilva</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Leaf blades broadly oblong to ovate, lobe apices acuminate young, (sometimes reflexed), obtuse later; angle from widest lobe to tip ca. 90°.</description>
      <determination>84 Crataegus venusta</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Leaf blades 5–8 cm, lobes 1–3 per side, sinuses shallow, base cuneate, veins 5–7 per side.</description>
      <determination>Crataegus species (see discussion under 81 Crataegus mendosa)</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Leaf blades to 5 cm, lobes 2–4 per side, sinuses shallow, base cuneate to broadly cuneate, veins 4 or 5(or 6) per side</description>
      <next_statement_id>15</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Leaf blades: length/width = 1.5, bases cuneate.</description>
      <determination>82 Crataegus pulcherrima</determination>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Leaf blades: length/width less than 1.5, bases cuneate to broadly cuneate.</description>
      <determination>85 Crataegus opima</determination>
    </key_statement>
  </key>
</bio:treatment>
