<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <other_info_on_meta type="treatment_page">618</other_info_on_meta>
    <other_info_on_meta type="mention_page">492</other_info_on_meta>
    <other_info_on_meta type="mention_page">493</other_info_on_meta>
    <other_info_on_meta type="mention_page">525</other_info_on_meta>
    <other_info_on_meta type="mention_page">528</other_info_on_meta>
    <other_info_on_meta type="mention_page">529</other_info_on_meta>
    <other_info_on_meta type="mention_page">580</other_info_on_meta>
    <other_info_on_meta type="mention_page">588</other_info_on_meta>
    <other_info_on_meta type="mention_page">605</other_info_on_meta>
    <other_info_on_meta type="mention_page">606</other_info_on_meta>
    <other_info_on_meta type="mention_page">611</other_info_on_meta>
    <other_info_on_meta type="mention_page">613</other_info_on_meta>
    <other_info_on_meta type="mention_page">617</other_info_on_meta>
    <other_info_on_meta type="mention_page">619</other_info_on_meta>
    <other_info_on_meta type="mention_page">622</other_info_on_meta>
    <other_info_on_meta type="mention_page">627</other_info_on_meta>
    <other_info_on_meta type="mention_page">628</other_info_on_meta>
    <other_info_on_meta type="mention_page">629</other_info_on_meta>
    <other_info_on_meta type="mention_page">630</other_info_on_meta>
    <other_info_on_meta type="mention_page">631</other_info_on_meta>
    <other_info_on_meta type="mention_page">632</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">rosaceae</taxon_name>
    <taxon_name rank="subfamily" authority="Arnott" date="unknown">amygdaloideae</taxon_name>
    <taxon_name rank="tribe" authority="Small" date="1933">MALEAE</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">crataegus</taxon_name>
    <taxon_name rank="section" authority="Loudon" date="1838">Coccineae</taxon_name>
    <taxon_name rank="series" authority="J. B. Phipps" date="unknown">Lacrimatae</taxon_name>
    <place_of_publication>
      <publication_title>Taxon</publication_title>
      <place_in_publication>37: 113. 1988</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe maleae;genus crataegus;section coccineae;series lacrimatae;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="fna_id">318086</other_info_on_name>
  </taxon_identification>
  <number>64f.20.</number>
  <description type="morphology">Shrubs or trees, 5–70(–100) dm, usually main trunk dominant. Stems: trunk bark usually dark brown to dark gray or black, sometimes ashy gray, highly corrugated; distal branches usually pendulous (particularly in the slender-twigged species), rarely ± spreading (in dwarf taxa and C. exilis), ± strongly flexuous (slightly so in 140. C. teres and 150. C. exilis); twigs: new growth usually pubescent, sometimes glabrate or glabrous, 1-year old purple-brown, older gray or dark gray, sometimes gray-brown; thorns on twigs absent or sparse to numerous, usually ± straight, sometimes ± recurved, 1-year old very dark, usually slender, 1–5 cm. Leaves: petiole length (0–)10–40(–60)% blade, pubescent (young), sessile-glandular (young); blade light to mid green abaxially, usually elliptic to oblong or narrowly obovate, sometimes obtrullate, ovate, or suborbiculate, (0.6–)1–4(–5) cm, thin to coriaceous, firm to floppy, base narrowly cuneate to rounded, lobes 0 or 1–3 per side, sinuses usually shallow, lobe apex acute to obtuse, margins entire or serrate, very glandular, venation craspedodromous (semicamptodromous in 142. C. attrita), veins 1–6 per side (exiting mainly or wholly in apical part of blade), apex obtuse to acute, surfaces ± woolly-tomentose (young). Inflorescences 1–4(–7)-flowered, usually highly reduced panicles; branches usually appressed-white-pubescent to canescent or tomentose, sometimes pilose or glabrous; bracteoles usually deciduous, rarely ± caducous (in 129. C. lacrimata), sometimes semipersistent, linear or oblong-linear, small, membranous, margins usually glandular, rarely eglandular (in 129. C. lacrimata), adaxially sometimes short-pubescent. Flowers 10–20(–25) mm diam.; hypanthium usually densely hairy, sometimes glabrous; sepals ± narrowly triangular, much shorter than petals, margins entire or glandular-serrate; petals elliptic or ± circular in C. crocea, C. quaesita; stamens 20, anthers white to cream; styles (2 or)3–5. Pomes yellow to orange and copper to red (1 face often colored most brightly), usually suborbicular, sometimes pyriform or ellipsoid, (5–)6–12(–15) mm diam., glabrous or sparsely hairy; flesh texture not recorded; sepals ± sessile, usually reflexed, sometimes circumscissile, non-accrescent; pyrenes (2 or)3–5.</description>
  <description type="distribution">se United States.</description>
  <other_name type="past_name">Larimatae</other_name>
  <discussion>Species 24 (24 in the flora).</discussion>
  <discussion>Series Lacrimatae constitutes a characteristic and often abundant element of the sand plains, scrubby areas, and pinelands of the southeastern United States coastal plain and piedmont from Louisiana east of the Mississippi to central Florida and on both sides of the Appalachians to northern Alabama and southeastern Virginia.</discussion>
  <discussion>Crataegus lacrimata, with conspicuously weeping branches, supplies the basis for the serial name. The widely used vernacular name, summer haw, is particularly apt as in most species the fruit ripens in August. Distinctions from the segregate ser. Apricae are given with that series. Nearly all the 24 species derive from Beadle who recognized 54 species. Those recognized here range from narrowly defined to variable entities lacking clear internal discontinuities. Regional authors after Beadle usually favored a few-species taxonomy, which tended to confuse ser. Lacrimatae with ser. Apricae. The present treatment reflects the substantial variation in stature, habit, leaf size, shape and venosity, indumentum, inflorescence size, and fruit size, color and time of ripening first noticed by Beadle. Series Lacrimatae contains dwarf shrubs as well as larger shrubs and trees, which mostly arrange straightforwardly into slender, very weeping types (subser. Tenues) or more robust, less weeping ones (subser. Robustae) (see J. B. Phipps and K. A. Dvorsky 2008); the bark is highly corrugated and presumably fire-resistant, unlike that of most other Crataegus series.</discussion>
  <discussion>The taxonomy of ser. Lacrimatae and the often subsumed ser. Apricae has been confused by the introduction of the name Crataegus michauxii by Persoon in 1806, first by C. D. Beadle, followed by regional floras. The type is a vegetative fragment at Paris (P) that cannot confidently be aligned with a particular species, or even series, without molecular study, though D. B. Ward (2009) ambitiously tried to equate it to C. condigna.</discussion>
  <references>
    <reference>Phipps, J. B. and K. A. Dvorsky. 2008. A taxonomic revision of Crataegus series Lacrimatae (Rosaceae). J. Bot. Res. Inst. Texas 2: 1101–1162.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Shrubs, dense, twiggy, 5–20(–25) dm, branches not weeping except in larger specimens; leaf blades: lobes 0 or very obscure</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Shrubs or trees, (10–)20–70(–100) dm, branches usually ± weeping; leaf blades: lobes 0 or conspicuous</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves deciduous, thin to subchartaceous, blades narrowly obovate to cuneate or oblanceolate; Mississippi to s Virginia.</description>
      <determination>130 Crataegus munda</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaves deciduous or semipersistent, subcoriaceous or coriaceous, blades broadly obovate to ± isodiametric or obovate-cuneate; c Florida to South Carolina</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blades broadly obovate to ± isodiametric; petiole lengths 15–30% blade; thorns on twigs 0.6–2 cm.</description>
      <determination>127 Crataegus lepida</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blades obovate-cuneate; petiole lengths 5–15% blade; thorns on twigs 3–5 cm.</description>
      <determination>135 Crataegus invicta</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Branches strongly weeping; inflorescence branches and hypanthium glabrous; bracteole margins eglandular or nearly so.</description>
      <determination>129 Crataegus lacrimata</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Branches ± to strongly weeping; inflorescence branches and hypanthium pubescent to tomentose; bracteole margins glandular</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Mature branches strongly weeping (moving in slight wind); habit usually rather slender; twigs slender (1.5 mm or less thick, at 5 cm from tip); thorns on twigs fine, 1–4 cm; leaf blades 1–4 cm, drooping or fluttering in many species; pomes 6–8(–12) mm diam., usually yellow with crimson cheek (subser. Tenues)</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Mature branches ± weeping; habit more robust and stocky; twigs usually relatively stout (ca. 1.5–3 mm diam., at 5 cm from tip); thorns on twigs absent or slender to stout, sometimes fine, (1–)2–5(–7) cm; leaf blades (1.5–)2.5–4.5 cm, rather thick and stiff or coriaceous in most species, others thin or subcoriaceous, mostly not drooping or fluttering (except in C. lassa and C. lanata); pomes (5–)8–15 mm diam., orange-red to red, often copper-colored mature (subser. Robustae, except C. condigna, C. exilis)</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaf blades: lobes 0 or obscure</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaf blades: lobes usually 1 or 2 per side, usually quite evident</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaf blades usually spatulate or narrowly oblong to narrowly oblanceolate or narrowly elliptic, margins entire, sometimes obscurely crenate-serrate distally.</description>
      <determination>131 Crataegus crocea</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaf blades narrowly obovate or ± spatulate to obtrullate, margins shallowly serrate to crenate-serrate except near base.</description>
      <determination>132 Crataegus condigna</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaf blades 1–2(–3) cm, short-obtrullate to broadly obovate (length/width mainly less than 1.4:1).</description>
      <determination>128 Crataegus egens</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaf blades 1.5–4 cm, narrowly to broadly cuneate to obtrullate or spatulate (length/width = 1.4–1.8:1)</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Leaf blades 1.5–3 cm, narrowly to broadly cuneate to obtrullate, ± stiff to ± floppy mature; petiole lengths 15–20% blade.</description>
      <determination>133 Crataegus quaesita</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Leaf blades (1.5–)3–4 cm (usually some shorter on same specimen), spatulate to obtrullate, readily fluttering in wind; petiole lengths 25–50% blade.</description>
      <determination>134 Crataegus floridana</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Shrubs, 15–30 dm; thorns on twigs usually numerous, 2–5(–7) cm; leaf blades 1.5–3 cm</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Shrubs or trees, (10–)20–70(–100) dm; thorns on twigs usually absent or few, sometimes moderately frequent, 1–4(–5) cm; leaf blades 1.5–5 cm</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Leaf blades: lobes 0, margins basally entire or subentire, distally ± denticulate; thorns on twigs 2–4.5 cm.</description>
      <determination>138 Crataegus colonica</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Leaf blades: lobes usually obscure, sinuses shallow, sometimes lobes 0, margins strongly crenate-serrate most of length; thorns on twigs 3–5(–7) cm.</description>
      <determination>139 Crataegus pexa</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Leaf blades: usually distinctly lobed, only shallowly, margins often toothed</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Leaf blades: lobes 0, sometimes present, few, subterminal, obscure, very short-triangular or margins merely sinuous, toothed or not</description>
      <next_statement_id>19</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Leaf blades 2–5 cm mature; most leaves ± narrow, length/width = 1.75+, ± obovate or narrowly to broadly obtrullate, sometimes oblanceolate or oblong-spatulate to cuneate or narrowly obdeltate</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Leaf blades 1.5–4 cm mature; 2/3+ of leaves relatively broad, length/width = 1.4–1.6, broadly obovate to obdeltate or obtrullate, obovate-spatulate, or rhombic-elliptic</description>
      <next_statement_id>16</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Leaf blades 3–5 cm mature, floppy, older remaining pubescent; inflorescences 1–4-flowered; flowers 17–20 mm diam.</description>
      <determination>144 Crataegus lanata</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Leaf blades 2–3 cm mature, ± floppy, older becoming glabrous; inflorescences 3–7-flowered; flowers (10–)14–16(–20) mm diam</description>
      <next_statement_id>15</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Leaf blades usually narrowly obovate to obtrullate, sometimes oblanceolate, lobes 0, or 1 or 2(or 3) per side distally, lobe apices subacute, margins weakly crenate-serrate, veins 1–3 per side.</description>
      <determination>147 Crataegus lancei</determination>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Leaf blades oblong-spatulate to cuneate or narrowly obdeltate, lobes 1 or 2 per side distally, lobe apices acute, margins strongly crenate-serrate almost to bases, veins (2 or)3 or 4(or 5) per side.</description>
      <determination>148 Crataegus senta</determination>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Leaf blades often persistently lanate, lobes 1 or 2 per side, obscure, lobe apices obtuse.</description>
      <determination>145 Crataegus furtiva</determination>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Leaf blades not persistently lanate, lobes 1–3 per side, prominent, lobe apices obtuse to acute</description>
      <next_statement_id>17</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Upper twigs suberect, others ± spreading; inflorescences 3–7-flowered, branches pilose-pubescent; leaves glabrescent; pomes 5–7 mm diam.</description>
      <determination>150 Crataegus exilis</determination>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Most twigs ± pendent; inflorescences 2–4-flowered, branches appressed-canescent or densely appressed-pubescent; leaves remaining pubescent or glabrescent; pomes 8–12 mm diam</description>
      <next_statement_id>18</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Leaf blades narrowly to broadly cuneate to obtrullate, lobes 1–3 per side, margins crenate-serrate; petiole lengths 30–50% blade.</description>
      <determination>146 Crataegus meridiana</determination>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Leaf blades ± obovate-spatulate, lobes 2 per side, margins glandular-serrate; petiole lengths 20–35% blade.</description>
      <determination>149 Crataegus dispar</determination>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Leaf margins glandular-crenate or glandular-denticulate to subentire</description>
      <next_statement_id>20</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Leaf margins crenate, serrate, or glandular-serrate</description>
      <next_statement_id>22</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Leaf blades 2–4 cm (length/width = 1.6–2.1), obovate-cuneate to narrowly obovate, sides evenly tapered to base, apices often cuspidate; petiole lengths 25–33% blade.</description>
      <determination>143 Crataegus lassa</determination>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Leaf blades 1.5–4 cm (length/width = 1–1.7), ± broadly obtrullate to obdeltate or ± obovate, sometimes suborbiculate, alternatively sometimes obscurely sinuously lobed around broadest point, sides variably tapered, apices acute to ± flat or truncate-obtuse, sometimes cuspidate; petiole lengths 10–25% blade</description>
      <next_statement_id>21</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Leaf blades obovate-cuneate to obovate or suborbiculate, apices ± acute to weakly cuspidate, surfaces pubescent young; lobes 0 or obscure.</description>
      <determination>137 Crataegus integra</determination>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Leaf blades broadly obtrullate or obdeltate to obtrullate, apices acute or rounded to subacute, sometimes cuspidate, surfaces densely tomentose young; lobes 1–3 per side, often evident, occasionally obscure.</description>
      <determination>145 Crataegus furtiva</determination>
    </key_statement>
    <key_statement>
      <statement_id>22</statement_id>
      <description type="morphology">Hypanthium and inflorescence branches glabrous, pilose, or glabrate</description>
      <next_statement_id>23</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>22</statement_id>
      <description type="morphology">Hypanthium and inflorescence branches tomentose</description>
      <next_statement_id>25</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>23</statement_id>
      <description type="morphology">Leaf blade margins finely serrate, veins (2 or)3 or 4 per side; twigs slightly flexuous; inflorescence branches glabrate; styles and pyrenes 2 or 3.</description>
      <determination>140 Crataegus teres</determination>
    </key_statement>
    <key_statement>
      <statement_id>23</statement_id>
      <description type="morphology">Leaf blade margins coarsely and/or sharply serrate, veins 1–4 per side; twigs usually flexuous; inflorescence branches ± pilose or glabrous; styles and pyrenes 3–5</description>
      <next_statement_id>24</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>24</statement_id>
      <description type="morphology">Leaf blades 2–4.5 cm, abaxial vein axils glabrous, venation craspedodromous, veins (1 or)2–4 per side; inflorescence branches sparsely to moderately pilose.</description>
      <determination>141 Crataegus florens</determination>
    </key_statement>
    <key_statement>
      <statement_id>24</statement_id>
      <description type="morphology">Leaf blades 1.5–3.5 cm, abaxial vein axils with tufts of hair, venation semicamptodromous, veins 1 or 2 per side; inflorescence branches glabrous or very sparsely pilose.</description>
      <determination>142 Crataegus attrita</determination>
    </key_statement>
    <key_statement>
      <statement_id>25</statement_id>
      <description type="morphology">Leaf blade margins coarsely, irregularly serrate, lobes 0 or 1 per side, sinuses shallow</description>
      <determination>Crataegus pulla (see discussion under C. lanata)</determination>
    </key_statement>
    <key_statement>
      <statement_id>25</statement_id>
      <description type="morphology">Leaf blade margins regularly, finely serrate to crenate-serrate, lobes 0 or obscure</description>
      <next_statement_id>26</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>26</statement_id>
      <description type="morphology">Leaf blades narrowly obovate or ± spatulate to obtrullate, apices acute to subacute, often cuspidate; twigs usually slender, (1.5 mm diam. at 5 cm from tip); flowers 12–16 mm diam.; pomes 8–12 mm diam., yellow-orange to reddish, sometimes with flushed cheeks.</description>
      <determination>132 Crataegus condigna</determination>
    </key_statement>
    <key_statement>
      <statement_id>26</statement_id>
      <description type="morphology">Leaf blades broadly oblong to cuneate, apices ± flattened to slightly cuspidate; twigs ± stout, (2–3 mm diam. at 5 cm from tip); flowers 20–25 mm diam.; pomes 10–15 mm diam., red.</description>
      <determination>136 Crataegus alabamensis</determination>
    </key_statement>
  </key>
</bio:treatment>
