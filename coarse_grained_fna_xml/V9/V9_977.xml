<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <author>unknown</author>
      <date>unknown</date>
    </source>
    <other_info_on_meta type="treatment_page">574</other_info_on_meta>
    <other_info_on_meta type="mention_page">493</other_info_on_meta>
    <other_info_on_meta type="mention_page">522</other_info_on_meta>
    <other_info_on_meta type="mention_page">543</other_info_on_meta>
    <other_info_on_meta type="mention_page">563</other_info_on_meta>
    <other_info_on_meta type="mention_page">571</other_info_on_meta>
    <other_info_on_meta type="mention_page">572</other_info_on_meta>
    <other_info_on_meta type="mention_page">575</other_info_on_meta>
    <other_info_on_meta type="mention_page">577</other_info_on_meta>
    <other_info_on_meta type="mention_page">637</other_info_on_meta>
    <other_info_on_meta type="mention_page">642</other_info_on_meta>
    <other_info_on_meta type="illustration_page">576</other_info_on_meta>
    <other_info_on_meta type="volume">9</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">rosaceae</taxon_name>
    <taxon_name rank="subfamily" authority="Arnott" date="unknown">amygdaloideae</taxon_name>
    <taxon_name rank="tribe" authority="Small" date="1933">MALEAE</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">crataegus</taxon_name>
    <taxon_name rank="section" authority="Loudon" date="1838">Coccineae</taxon_name>
    <taxon_name rank="series" authority="unknown" date="1940">Pruinosae</taxon_name>
    <taxon_name rank="species" authority="(H. L. Wendland) K. Koch" date="1853">pruinosa</taxon_name>
    <place_of_publication>
      <publication_title>Hort. Dendrol.,</publication_title>
      <place_in_publication>168. 1853</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family rosaceae;subfamily amygdaloideae;tribe maleae;genus crataegus;section coccineae;series pruinosae;species pruinosa;</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
    <other_info_on_name type="fna_id">242416351</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Mespilus</taxon_name>
    <taxon_name rank="species" authority="H. L. Wendland" date="unknown">pruinosa</taxon_name>
    <place_of_publication>
      <publication_title>Flora</publication_title>
      <place_in_publication>6: 701. 1823</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus Mespilus;species pruinosa</taxon_hierarchy>
  </taxon_identification>
  <number>75.</number>
  <other_name type="common_name">Frosted hawthorn</other_name>
  <description type="morphology">Shrubs or trees, dense, often suberect, 20–70 dm. Stems: compound thorns on trunks present; twigs: new growth reddish brown, 1-year old dull purple-brown, 2-years old dull gray, older paler; thorns on twigs straight to slightly recurved, 2-years old deep purple to shiny black, usually ± fine, 3–5 cm. Leaves: petiole length 50–66% blade, sparsely glandular; blade trullate to ovate, ovate-oblong, or broadly rhombic, rarely ± deltate, 2–6(–7) cm, subcoriaceous, base broadly cuneate to subtruncate to weakly subcordate, lobes (1–)3 or 4 per side, sinuses shallow to deeper, lobe apex acute, sometimes ± obtuse, margins serrate, veins 5 or 6 per side (except smaller leaves), apex acute, surfaces glabrous (except var. virella). Inflorescences 5–10-flowered; branches glabrous, sometimes sparsely villous; bracteoles caducous, usually few, margins short-stipitate-glandular. Flowers 15–25 mm diam.; sepals narrowly triangular, 5–6 mm, margins usually entire or subentire, rarely glandular-serrate, abaxially glabrous; stamens (10 or)20, anthers pale pink to bright rose or dull purple, sometimes cream, 0.6–0.8 mm; styles 3–5. Pomes greenish with pink or mauve areas, sometimes bright crimson or scarlet, often rather angular, 10–20 mm diam., highly pruinose, not punctate; flesh hard; sepals on collar, spreading; pyrenes 3–5.</description>
  <description type="distribution">Ont., Que.; Ark., Conn., Ga., Ill., Ind., Iowa, Kans., Ky., Maine, Mass., Mich., Mo., N.C., N.H., N.J., N.Y., Ohio, Pa., Tenn., Va., Vt., W.Va., Wis.</description>
  <discussion>Varieties 6 (6 in the flora).</discussion>
  <discussion>Crataegus pruinosa extends from Arkansas to Wisconsin, through the southern Great Lakes to southern New England, and, in the south, mainly in the Appalachians to northern Georgia.</discussion>
  <discussion>In the north of its range, Crataegus pruinosa is mainly a shrub of open successional habitats but in the south may commonly be a taller tree of open or thin woodlands.</discussion>
  <discussion>Many hawthorns have a little waxy bloom on their pomes; it is particularly prominent on Crataegus pruinosa and C. cognata compared to others. Some authors include in C. pruinosa their white-anthered counterparts, here assigned to C. cognata. Whereas C. pruinosa characteristically has entire or subentire sepal margins, some forms in the southwest of the range of the species (for example, C. calliantha Sargent, C. seducta Sargent) may have glandular-serrate sepal margins. They may represent introgression with C. coccinioides. The varieties of C. pruinosa are weakly differentiated from each other, most of them on leaf shape and size characters. The more widespread varieties constitute a range of morphotypes held together by common traits. Crataegus gaudens Sargent is a strikingly distinct form from Pennsylvania that has more or less elliptic leaves with lobes absent; it is clearly related to C. pruinosa. Note that 159. Crataegus ×coleae, a Michigan endemic, will key out here if its laterally scarred pyrenes are missed.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stamens 10.</description>
      <determination>75b Crataegus pruinosa var. dissona</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Stamens 20</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf surfaces sparsely short-hairy adaxially young, usually glabrescent; inflorescence branches sometimes sparsely villous.</description>
      <determination>75e Crataegus pruinosa var. virella</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf surfaces glabrous adaxially young, sometimes sparsely appressed-hairy along veins; inflorescence branches glabrous</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blades 5–7 cm, ovate to ovate-deltate, lobes 0 or 1–4 per side, obscure, lobe apices ± obtuse, bases broadly cuneate to subtruncate; anthers cream.</description>
      <determination>75f Crataegus pruinosa var. magnifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blades 2–7 cm, ovate or ovate-oblong to ± deltate or broadly rhombic, lobes 2–4 per side, distinct, lobe apices acute, bases broadly cuneate to subtruncate or truncate to weakly subcordate; anthers pale pink</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf blades 3–7 cm, broadly ovate to deltate, length/width = 1–1.2, bases very broadly cuneate or truncate to weakly subcordate.</description>
      <determination>75d Crataegus pruinosa var. rugosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf blades 2–6 cm, broadly rhombic, ovate, or ovate-oblong to narrowly deltate, length/width = 1.5, bases broadly cuneate to subtruncate</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaf blades ovate or ovate-oblong to narrowly deltate, 4–6 cm, bases broadly cuneate to subtruncate.</description>
      <determination>75a Crataegus pruinosa var. pruinosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaf blades broadly rhombic, 2–4 cm, bases broadly cuneate.</description>
      <determination>75c Crataegus pruinosa var. parvula</determination>
    </key_statement>
  </key>
</bio:treatment>
