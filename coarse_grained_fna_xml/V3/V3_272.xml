<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <author>Henk van der Werff</author>
      <date>unknown</date>
    </source>
    <other_info_on_meta type="volume">3</other_info_on_meta>
    <other_info_on_meta type="treatment_page">26</other_info_on_meta>
    <other_info_on_meta type="illustrator">John Myers</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">LAURACEAE</taxon_name>
    <taxon_hierarchy>family LAURACEAE;</taxon_hierarchy>
    <other_info_on_name type="fna_id">10479</other_info_on_name>
  </taxon_identification>
  <description type="morphology">Shrubs to tall trees, evergreen or rarely deciduous (Cassytha a parasitic vine with leaves reduced to scales), usually aromatic. Leaves alternate, rarely whorled or opposite, simple, without stipules, petiolate. Leaf blade: unlobed (unlobed or lobed in Sassafras), margins entire, occasionally with domatia (crevices or hollows serving as lodging for mites) in axils of main lateral veins (in Cinnamomum). Inflorescences in axils of leaves or deciduous bracts, panicles (rarely heads), racemes, compound cymes, or pseudoumbels (spikes in Cassytha), sometimes enclosed by decussate bracts. Flowers bisexual or unisexual, bisexual only, or staminate and pistillate on different plants, or staminate and bisexual on some plants, pistillate and bisexual on others; flowers usually yellow to greenish or white, rarely reddish; hypanthium well developed, resembling calyx tube, tepals and stamens perigynous; tepals 6(-9), in 2(-3) whorls of 3, sepaloid, equal or rarely unequal, if unequal then usually outer 3 smaller than inner 3 (occasionally absent in Litsea); stamens (3-)9(-12), in whorls of 3, but 1 or more whorls frequently staminodial or absent; stamens of 3d whorl with 2 glands near base; anthers 2- or 4-locular, locules opening by valves; pistil 1, 1-carpellate; ovary 1-locular; placentation basal; ovule 1; stigma subsessile, discoid or capitate. Fruits drupes, drupe borne on pedicel with or without persistent tepals at base, or seated in ± deeply cup-shaped receptacle (cupule), or enclosed in accrescent floral tube. Seed 1; endosperm absent.</description>
  <description type="distribution">Pantropical, a few species also in subtropical and temperate regions</description>
  <number>5.</number>
  <other_name type="common_name">Laurel Family</other_name>
  <discussion>Genera ca. 50, species 2000-3000 (9 genera, 13 species in the flora).</discussion>
  <discussion>Cassytha is sometimes placed in its own family, Cassythaceae; it is here retained in Lauraceae.</discussion>
  <references>
    <reference>Wood, C. E. Jr. 1958. The genera of the woody Ranales in the southeastern United States. J. Arnold Arbor. 39: 296-346.</reference>
  </references>
  <key>
    <key_head>Key to Genera Based on Flowering Material</key_head>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Parasitic vines, leaves reduced to minute scales; stems pale green to yellow-green or orange, twining.</description>
      <determination>9 Cassytha</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Shrubs or trees, leafy; stem various in color but not orange, not twinning.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants deciduous; flowers appearing before or with new leaves.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Plants evergreen; flowers appearing when leaves mature.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Flowers in racemes or panicles; leaf blade often lobed.</description>
      <determination>3 Sassafras</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Flowers in pseudoumbels; leaf blade always unlobed.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Anthers 2-locular.</description>
      <determination>1 Lindera</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Anthers 4-locular.</description>
      <determination>2 Listea</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Flowers in pseudoumbels.</description>
      <determination>4 Umbellularia</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Flowers in panicles or compound cymes.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Stamens 3, anthers 2-locular.</description>
      <determination>6 Licaria</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Stamens 9, anthers 4-locular.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Outer 3 tepals shorter than inner 3.</description>
      <determination>8 Persea</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Tepals equal.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaf blade pinnately veined, domatia absent; terminal bud not covered by imbricate scales.</description>
      <determination>7 Nectandra</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaf blade with (1-)3 primary veins, pubescent domatia in axils of main lateral veins; terminal bud covered by imbricate scales, young twigs with clusters of scars from fallen scales.</description>
      <determination>5 Cinnamomum</determination>
    </key_statement>
  </key>
  <key>
    <key_head>Key to Genera Based on Fruiting Material</key_head>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Parasitic vines, leaves reduced to minute scales.</description>
      <determination>9 Cassytha</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Shrubs or trees, leafy.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blade usually lobed (often unlobed).</description>
      <determination>3 Sassafras</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blade always unlobed.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Tepals persistent at base of fruit; cupule absent.</description>
      <determination>8 Persea</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Tepals deciduous; small cupule present.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Cupule usuallly double-rimmed.</description>
      <determination>6 Licaria</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Cupule single-rimmed.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Fruit at maturity 2 cm or more in greatest dimension; California, Oregon</description>
      <determination>4 Umbellularia</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Fruit at maturity less than 2 cm in greatest dimension; e of Rocky Mountains.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Infructescences umbellate or not branched, about 1 cm.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Infructescences paniculate, more than 4 cm.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaf blade 4 × 1.5 cm or less.</description>
      <determination>2 Listea</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaf blade 4 × 2 cm or more.</description>
      <determination>1 Lindera</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaf blade pinnately veined, domatia absent; terminal bud not covered by imbricate scales.</description>
      <determination>7 Nectandra</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaf blade with (1-)3 primary veins, pubescent domatia in axils of main lateral veins; terminal bud covered by imbricate scales, young twigs with clusters of scars from fallen scales.</description>
      <determination>5 Cinnamomum</determination>
    </key_statement>
  </key>
</bio:treatment>
