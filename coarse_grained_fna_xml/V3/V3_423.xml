<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <author>Alan T. Whittemore, Bruce D. Parfitt</author>
      <date>unknown</date>
    </source>
    <other_info_on_meta type="volume">3</other_info_on_meta>
    <other_info_on_meta type="treatment_page">85</other_info_on_meta>
    <other_info_on_meta type="illustrator">John Myers</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">RANUNCULACEAE</taxon_name>
    <taxon_hierarchy>family RANUNCULACEAE;</taxon_hierarchy>
    <other_info_on_name type="fna_id">10757</other_info_on_name>
  </taxon_identification>
  <description type="morphology">Herbs, sometimes woody or herbaceous climbers or low shrubs, perennial or annual, often rhizomatous. Stems unarmed. Leaves usually basal and cauline, alternate or sometimes opposite, rarely whorled, simple or variously compound; stipules present or absent; petioles usually present, often proximally sheathing. Leaf blade undivided or more commonly divided or compound, base cordate, sometimes truncate or cuneate, margins entire, toothed, or incised; venation pinnate or palmate. Inflorescences terminal or axillary, racemes, cymes, umbels, panicles, or spikes, or flowers solitary, flowers pedicellate or sessile. Flowers bisexual, sometimes unisexual, inconspicuous or showy, radially or bilaterally symmetric; sepaloid bracteoles absent; perianth hypogynous; sepals usually imbricate, 3-6(-20), distinct, often petaloid and colored, occasionally spurred; petals 0-26, distinct (connate in Consolida), plane, cup-shaped, funnel-shaped, or spurred, conspicuous or greatly reduced; nectary usually present, rarely absent; stamens 5-many, distinct; anthers dehiscing longitudinally; staminodes absent (except in Aquilegia and Clematis); pistils 1-many; styles present or absent, often persistent in fruit as beak. Fruits achenes, follicles, or rarely utricles, capsules, or berries, often aggregated into globose to cylindric heads. Seeds 1-many per ovary, never stalked, not arillate; endosperm abundant; embryo usually small.</description>
  <description type="distribution">Worldwide.</description>
  <number>15.</number>
  <other_name type="common_name">Crowfoot Family</other_name>
  <discussion>Genera ca. 60, species 1700 (22 genera, 284 species in the flora).</discussion>
  <discussion>The flowers of many species of Ranunculaceae begin to open long before anthesis, while the floral organs are just partly expanded. Only mature flowers with open anthers should be used for determination of diagnostic characteristics (especially measurements).</discussion>
  <discussion>The literature is inconsistent about the term for the whorl of organs between sepals and stamens; these may be conspicuous and petaloid, or reduced to stalked nectaries, or intermediate between the two states. They have been called petals, honey-leaves, or (when they are inconspicuous) staminodes or nectaries. We follow M. Tamura (1993) and treat as petals all organs between the sepals and stamens, except in Clematis and Aquilegia where they usually bear rudimentary anthers and clearly represent staminodes.</discussion>
  <references>
    <reference>Brayshaw, T. C. 1989. Buttercups, Waterlilies, and Their Relatives (the Order Ranales) in British Columbia. Victoria. [Roy. Brit. Columbia Mus. Mem. 1.]</reference>
    <reference>Tamura, M. 1963. Morphology, ecology and phylogeny of the Ranunculaceae I. Sci. Rep. Coll. Gen. Educ. Osaka Univ. 11: 115-126.</reference>
    <reference>Tamura, M. 1993. Ranunculaceae. In: K. Kubitzki et al., eds. 1990+. The Families and Genera of Vascular Plants. 2+ vols. Berlin etc. Vol. 2, pp. 563-583.</reference>
    <reference>Ziman, S. N. and C. S. Keener. 1989. A geographical analysis of the family Ranunculaceae. Ann. Missouri Bot. Gard. 76: 1012-1049.</reference>
    <reference>Duncan, T. and C. S. Keener. 1991. A classification of the Ranunculaceae with special reference to the Western Hemisphere. Phytologia 70: 24-27.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Flowers bilaterally symmetric; sepals showy; petals smaller than sepals.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Flowers radially symmetric; sepals showy or not; petals present or absent, smaller to larger than sepals.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Upper (adaxial) sepal (hood) saccate or helmet-shaped; petals completely hidden by sepals.</description>
      <determination>15 Aconitum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Upper (adaxial) sepal spurred; petals at least partly exserted from calyx.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Perennials; pistils 3(-5); petals 4, distinct.</description>
      <determination>16 Delphinium</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Annuals; pistil 1; petals 2, connate.</description>
      <determination>17 Consolida</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Fruits achenes or utricles; ovule 1 per pistil.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Fruits follicles, capsules, or berries; ovules 2 or more per pistil (1 of 2 aborting in Xanthorhiza, leaving 1 seed at maturity).</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Sepals spurred; leaves all basal, blade linear or narrowly oblanceolate.</description>
      <determination>3 Myosurus</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Sepals plane; leaves either not all basal, or blade not linear or narrowly oblanceolate.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaves all cauline and opposite; stems ±woody, at least at base.</description>
      <determination>6 Clematis</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaves cauline and alternate (rarely opposite), or basal, or plants with basal leaves and opposite or whorled involucral bracts; stems herbaceous.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Plants with 1 or more pairs (opposite) or whorls of involucral bracts, these leaflike or calyxlike.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Plants without involucral bracts (inconspicuous, linear-lanceolate involucral bracts in Trautvetteria), cauline leaves if present alternate (rarely a pair of opposite, unlobed leaves in Ranunculus sect. Flammula).</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Achenes with conspicuous veins or ribs on lateral surfaces; style absent.</description>
      <determination>22 Thalictrum thalictroides</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Achenes without veins on lateral surfaces; style present.</description>
      <determination>5 Anemone</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Petals absent; inflorescences panicles, racemes, or corymbs (umbels in Thalictrum thalictroides); filaments filiform or dilated distally.</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Petals present (rarely absent in Ranunculus pedatifidus); inflorescences simple or compound cymes or flowers solitary; filaments filiform.</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Leaves simple, blade lobed; flowers bisexual; inflorescences corymbs.</description>
      <determination>4 Trautvetteria</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Leaves compound; flowers unisexual or bisexual; inflorescences panicles, racemes, corymbs, or umbels.</description>
      <determination>22 Thalictrum</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Petals without nectaries; sepals 5(-8).</description>
      <determination>12 Adonis</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Petals with basal nectaries; sepals 3-5(-6).</description>
      <determination>2 Ranunculus</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Leaves dissected into linear, threadlike segments; pistils compound; fruits capsules.</description>
      <determination>11 Nigella</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Leaves not dissected, if parted or compound the segments not linear; pistils simple; fruits aggregates of follicles or solitary or aggregate berries.</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Shrubs; beak of follicle lateral, strongly incurved against abaxial surface of follicle.</description>
      <determination>19 Xanthorhiza</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Herbs; beak of follicle, if present, terminal or nearly so, straight or slightly curved, sometimes hooked at tip.</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Petals prominent, spurred.</description>
      <determination>21 Aquilegia</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Petals if present inconspicuous, plane or funnel-shaped.</description>
      <next_statement_id>15</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Flowers 12-50, in racemes or racemelike panicles.</description>
      <next_statement_id>16</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Flowers 1-10, in leafy cymes or solitary.</description>
      <next_statement_id>17</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Pistils 1-8; fruits follicles, usually aggregate; petals 2-cleft or absent.</description>
      <determination>8 Cimicifuga</determination>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Pistil 1; fruits berries; petals unlobed.</description>
      <determination>9 Actaea</determination>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Leaves simple, blade often lobed 1/2-3/4 its length, margins entire, crenate, or toothed; petals absent.</description>
      <next_statement_id>18</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Leaves compound or divided to base; petals usually inconspicuous (absent in Enemion).</description>
      <next_statement_id>19</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Leaf blades unlobed, margins entire, dentate, or crenate; fruits follicles.</description>
      <determination>13 Caltha</determination>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Leaf blades lobed, margins serrate; fruits berries.</description>
      <determination>1 Hydrastis</determination>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Leaves ternately 1-2× compound.</description>
      <next_statement_id>20</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Leaves palmately or pedately compound or divided.</description>
      <next_statement_id>21</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Leaves all basal; leaf blade deeply divided, ternately or pinnately 1-2× compound; petals present.</description>
      <determination>18 Coptis</determination>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Leaves basal and cauline; leaf blade ternately 2× compound; petals absent.</description>
      <determination>20 Enemion</determination>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Leaf segments lobed, margins sharply toothed; sepals persistent in fruit.</description>
      <determination>7 Helleborus</determination>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Leaf segments cleft or parted, margins entire or toothed; sepals not persistent in fruit.</description>
      <next_statement_id>22</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>22</statement_id>
      <description type="morphology">Cauline leaves absent except for whorl of 3 involucral bracts immediately subtending flower; follicles stipitate.</description>
      <determination>10 Eranthis</determination>
    </key_statement>
    <key_statement>
      <statement_id>22</statement_id>
      <description type="morphology">Cauline leaves alternate, (0.8-)1 cm or more from flower, involucral whorl absent; follicles sessile.</description>
      <determination>14 Trollius</determination>
    </key_statement>
  </key>
</bio:treatment>
