<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <author>R. David Whetstone, T.A. Atkinson, Daniel D. Spaulding</author>
      <date>unknown</date>
    </source>
    <other_info_on_meta type="volume">3</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">berberidaceae</taxon_name>
    <taxon_name rank="genus" authority="de Candolle" date="1821">ACHLYS</taxon_name>
    <place_of_publication>
      <publication_title>Syst. Nat.</publication_title>
      <place_in_publication>2: 35. 1821</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family berberidaceae;genus ACHLYS</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek Achlus, a god of night</other_info_on_name>
    <other_info_on_name type="fna_id">100201</other_info_on_name>
  </taxon_identification>
  <description type="morphology">Herbs, perennial, deciduous, 2.5-5 dm, glabrous. Rhizomes extensive, branching, producing 1-few foliage leaves or flowering shoots per year. Aerial stems absent. Leaves basal, alternate, 3-foliolate; petiole long, slender. Leaf blade orbiculate in gross outline, 20-40 cm; leaflet blades fan-shaped, entire or lobed, lateral leaflet blades strongly asymmetric, margins entire to coarsely sinuate; venation palmate. Inflorescences terminal, dense scapose-pedunculate spikes of inconspicuous flowers. Flowers 3-merous, white to cream, 6 mm or less; bracteoles absent; sepals absent; petals absent; stamens 8-10(-13); anthers dehiscing by 2 apically hinged flaps; pollen exine striate; ovaries asymmetrically ellipsoid; placentation marginal, placenta developed only near base of ovary. Fruits follicles with transverse dehiscence, purplish red or brown, curved, furrowed. Seeds 1, brown; aril absent. x = 6.</description>
  <description type="distribution">North America, Asia (Japan).</description>
  <number>6.</number>
  <discussion>Species 3 (2 in the flora).</discussion>
  <discussion>Achlys is of particular interest because of its amphi-Pacific disjunction. Despite the 8000 km or more disjunction, the taxa are remarkably similar in morphology, ecology, and karyology. Japanese populations are diploid; American populations are diploid and tetraploid.</discussion>
  <discussion>Two species are recognized in this treatment; some researchers prefer to treat them as varieties. In the Californian portion of the range, some field botanists believe the two taxa are sufficiently morphologically distinct to be called species; farther north these differences are reportedly less apparent.</discussion>
  <references>
    <reference>Fukuda, I. 1967. The biosystematics of Achlys. Taxon 16: 308-316.</reference>
    <reference>Fukuda, I. and H. G. Baker. 1970. Achlys californica --(Berberidaceae)–a new species. Taxon 19: 341-344.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Central leaflet blade 4–11 cm, distal margins (1–)3–4(–8)-lobed; stamens 3–4 mm; follicles red-purple.</description>
      <determination>1 Achlys triphylla</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Central leaflet blade ca. 7–16 cm, distal margins (3–)6–9(–12)-lobed; stamens 3.8–6 mm; follicles brown.</description>
      <determination>2 Achlys californica</determination>
    </key_statement>
  </key>
</bio:treatment>
