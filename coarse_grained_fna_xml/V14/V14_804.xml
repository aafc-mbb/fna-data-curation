<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>James S. Pringle</author>
    </source>
    <other_info_on_meta type="volume">14</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">GENTIANACEAE</taxon_name>
    <taxon_name rank="genus" authority="Muhlenberg ex Willdenow" date="1801">BARTONIA</taxon_name>
    <place_of_publication>
      <publication_title>Ges. Naturf. Freunde Berlin Neue Schriften</publication_title>
      <place_in_publication>3: 444.  1801</place_in_publication>
      <other_info_on_pub>name conserved</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family gentianaceae;genus bartonia</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
    <other_info_on_name type="etymology">For Benjamin Smith Barton, 1766–1815, American botanist</other_info_on_name>
  </taxon_identification>
  <number>12.</number>
  <description type="morphology">Herbs annual or perennial, weakly chlorophyllous, with stems and leaves yellowish green or purplish, glabrous.  Leaves cauline, alternate, subopposite, or opposite, scalelike.  Inflorescences dichasial or racemoid cymes, reduced thyrses, or solitary flowers.  Flowers 4-merous; calyx lobed nearly to base, or some or all lobes proximally connate; corolla white or greenish white to pale yellow, yellowish green, to green, sometimes purple-tinged, narrowly campanulate, glabrous, lobes longer than tube, margins entire or erose, plicae between lobes absent, spurs absent; stamens inserted in corolla sinuses; anthers distinct; ovary sessile or subsessile; style short and stout or absent; stigma 2-lobed or decurrent along distal portion of sutures; nectaries absent.  Capsules compressed-cylindric.  x = 11, 13.</description>
  <discussion>Species 3 (3 in the flora).</discussion>
  <description type="distribution">c, e North America.</description>
  <discussion>Plants cultivated as Bartonia are species of Mentzelia (Loasaceae), to which the homonym Bartonia Sims of 1804 was formerly applied.</discussion>
  <discussion>Bartonia has a reduced root system, minute leaves, and stems that are sometimes weakly chlorophyllous, and is partially mycotrophic (D. D. Cameron and J. F. Bolin 2010).  Bartonia species have been assumed to be annuals, from the appearance of the basal and underground parts, but this has been questioned.</discussion>
  <references>
    <reference>Gillett, J. M.  1959.  A revision of Bartonia and Obolaria (Gentianaceae).  Rhodora 61: 43–62.</reference>
    <reference>Mathews, K. G. et al.  2009.  A phylogenetic analysis and taxonomic revision of Bartonia (Gentianaceae: Gentianeae), based on molecular and morphological evidence.  Syst. Bot. 34: 162–172.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves ± evenly spaced; corollas 4.8–11 mm, lobed nearly to base, lobes narrowly spatulate-obovate to elliptic; flowering winter–spring.</description>
      <determination>3. Bartonia verna</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaves crowded near base of stem, distally widely separated; corollas 1.9–6.2 mm, lobed 0.5–0.8 times their length, lobes oblong or oblong-lanceolate; flowering summer–fall.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Corolla lobe margins distally erose-serrate, apices rounded to abruptly acute, mucronate.</description>
      <determination>1. Bartonia virginica</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Corolla lobe margins entire, apices acute to acuminate, not mucronate.</description>
      <determination>2. Bartonia paniculata</determination>
    </key_statement>
  </key>
</bio:treatment>
