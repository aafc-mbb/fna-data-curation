<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>Daniel F. Austin†</author>
    </source>
    <other_info_on_meta type="volume">14</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">Convolvulaceae</taxon_name>
    <taxon_name rank="genus" authority="J. R. Forster &amp; G. Forster" date="1776">DICHONDRA</taxon_name>
    <place_of_publication>
      <publication_title>Char. Gen. Pl. ed.</publication_title>
      <place_in_publication>2, 39, plate 20.  1776</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family convolvulaceae;genus dichondra</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek dis, double, and chondros, grain, alluding to each flower producing two 1-seeded capsules in D. repens, the type species</other_info_on_name>
  </taxon_identification>
  <number>3.</number>
  <other_name type="common_name">Ponyfoot</other_name>
  <description type="morphology">Perennials.  Stems procumbent to prostrate or trailing, usually rooting at nodes, sometimes mat-forming, glabrous or hairy.  Leaves petiolate; blade ± cordate-orbiculate to ± reniform, 3–51 mm, to 62 mm wide, surfaces glabrate, glabrous, or hairy.  Inflorescences: flowers usually solitary, rarely paired.  Flowers: sepals lanceolate to obovate or spatulate, 1–4(–5.2) mm, basally connate; corolla usually cream, greenish, greenish yellow, or white, rarely purplish or reddish, campanulate to funnelform, 1.5–5 mm, limb 5-lobed; styles 2, insertion on ovary ± basal; stigmas capitate.  Fruits capsular or utricular, subglobose to ± compressed and/or ± incised, 2-lobed, indehiscent, pericarp fragile, shattering irregularly, or dehiscence irregularly valvate.  Seeds 1 or 2(–4), obovoid, pyriform, or subspheric, glabrous, smooth.  x = 15.</description>
  <discussion>Species 15 (8 in the flora).</discussion>
  <description type="distribution">North America, Mexico, West Indies, Central America, South America, Pacific Islands (New Zealand), Australia; introduced also in Europe, Asia, Africa, Pacific Islands (Hawaii).</description>
  <discussion>This treatment is adapted from the revision by B. C. Tharp and M. C. Johnston (1961).</discussion>
  <references>
    <reference>Tharp, B. C. and M. C. Johnston.  1961.  Recharacterization of Dichondra (Convolvulaceae) and a revision of the North American species.  Brittonia 13: 346–360.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Fruits slightly notched to weakly 2-lobed.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Corollas purplish to reddish; abaxial leaf surfaces glabrate.</description>
      <determination>1. Dichondra occidentalis</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Corollas cream; abaxial leaf surfaces sericeous.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blade surfaces: abaxial and adaxial densely sericeous; pedicels 4–6 mm.</description>
      <determination>2. Dichondra argentea</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blade surfaces: abaxial moderately sericeous, adaxial sparsely sericeous to glabrate; pedicels 5–13(–26) mm.</description>
      <determination>3. Dichondra brachypoda</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Fruits notably 2-lobed.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf blade surfaces: abaxial densely sericeous, hairs silvery gray, adaxial sparsely sericeous to glabrate, hairs green; fruit lobes separating, each valvately dehiscent.</description>
      <determination>4. Dichondra sericea</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf blade surfaces: abaxial densely to sparsely sericeous, adaxial sparsely sericeous, hairs sometimes patent, or glabrous; fruit lobes separating, each indehiscent (pericarp fragile, shattering irregularly).</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Pedicels ± straight, seldom notably recurved near tips; sepals longer than fruits.</description>
      <determination>5. Dichondra carolinensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Pedicels recurved near tips; sepals shorter than to slightly longer than fruits.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Sepals 2–2.5 mm in fruit; fruits 2–2.6 × 1.8–2.3 mm.</description>
      <determination>6. Dichondra micrantha</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Sepals 2.5–3.8 mm in fruit; fruits 2.5–4 × 2.5–3.3 mm.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaf blades (5–)10–15(–22) × (8–)15–25(–38) mm; corollas 2–3 mm.</description>
      <determination>7. Dichondra donelliana</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Leaf blades (10–)20–30(–51) × (15–)30–40(–62) mm; corollas 3.1–4 mm.</description>
      <determination>8. Dichondra recurvata</determination>
    </key_statement>
  </key>
</bio:treatment>
