<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">14</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">GENTIANACEAE</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">GENTIANA</taxon_name>
    <taxon_name rank="species" authority="J. S. Pringle &amp; Sharp" date="1964">austromontana</taxon_name>
    <place_of_publication>
      <publication_title>Rhodora</publication_title>
      <place_in_publication>66: 402, fig. 1.  1964</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family gentianaceae;genus gentiana;species austromontana</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>20.</number>
  <other_name type="common_name">Blue Ridge or Roan Mountain or Appalachian gentian</other_name>
  <description type="morphology">Herbs perennial, 0.7–5 dm, puberulent on stems and calyx tubes.  Stems 1–50, terminal from caudex, erect or nearly so.  Leaves cauline, ± evenly spaced; blade lanceolate to ovate or elliptic, 3–12 cm × 10–30 mm, apex acute to acuminate.  Inflo­rescences 1–15-flowered heads, sometimes with additional flowers at 1–3 nodes or on short branches.  Flowers: calyx 8–25 mm, lobes lanceo­late to ovate-triangular, narrowly ovate, or occasionally elliptic, 1.5–12 mm, margins ciliate; corolla violet-blue, usually deeply colored, tubular, completely closed, 30–50 mm, lobes erect, triangular to nearly semicircular, 1.5–3 mm, free portions of plicae ± as long as lobes and ± 2 times as wide, deeply divided into 2 nearly equal, triangular to ± oblong segments, margins minutely erose; anthers connate.  Seeds winged.</description>
  <discussion>Because of the relatively late recognition of Gentiana austromontana as a distinct species, specimens of this species have been identified both as G. clausa and as G. decora.  Gentiana austromontana is distinguished from G. andrewsii, G. clausa, and G. latidens by the combination of puberulent stems and calyx tubes with free portions of the corolla plicae that are about as long as the lobes and about twice as wide, divided into two more or less triangular segments each similar to a true lobe in size and shape.  Because of its puberulence, it has been confused with G. decora, which differs in its more open, generally paler corollas with longer lobes and pli­cae, and usually narrowly linear calyx lobes.</discussion>
  <discussion>Both Gentiana austromontana and G. decora occur in the higher elevations of eastern Tennessee and western North Carolina, although G. decora tends to occur in shadier habitats.  These species are usually distinctly dissimilar, in each case bearing a greater resemblance to other species than to each other, but populations of plants variously intermediate between the two occur relatively frequently, especially in Greene and Unicoi counties, Tennessee.  Plants otherwise typical of G. austromontana but with narrowly open corollas have been found in Mount Jefferson State Natural Area, Ashe County, North Carolina, and may be derived from introgression of genetic material from G. decora.  Gentiana austromontana also hybridizes occasionally with G. clausa.</discussion>
  <description type="phenology">Flowering fall.</description>
  <description type="habitat">Grassy balds, open woods, acid soils.</description>
  <description type="elevation">600–2100 m.</description>
  <description type="distribution">N.C., Tenn., Va., W.Va.</description>
</bio:treatment>
