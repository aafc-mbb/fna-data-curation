<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">14</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="unknown" date="unknown" rank="family">Apocynaceae</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">ASCLEPIAS</taxon_name>
    <taxon_name rank="species" authority="Linnaeus" date="1753">tuberosa</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 217.  1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family apocynaceae;genus asclepias;species tuberosa</taxon_hierarchy>
  </taxon_identification>
  <number>70.</number>
  <other_name type="common_name">Butterflyweed</other_name>
  <other_name type="common_name">butterfly milkweed</other_name>
  <other_name type="common_name">butterfly flower</other_name>
  <other_name type="common_name">pleurisy root</other_name>
  <other_name type="common_name">chigger flower</other_name>
  <other_name type="common_name">tuber root</other_name>
  <other_name type="common_name">orange milkweed</other_name>
  <other_name type="common_name">asclépiade tubéreuse</other_name>
  <description type="morphology">Herbs.  Stems 1–numerous, erect to ascending, branched in inflorescence, 15–90 cm, densely hirsute, not glaucous, rhizomes absent.  Leaves alternate, petio­late, with 1 or 2 stipular colleters on each side of petiole; petiole 1–4 mm, densely hirsute; blade elliptic, oblong, or oblanceolate to lanceolate or linear, 2–12 × 0.3–3 cm, chartaceous, base cuneate or obtuse to rounded, truncate, hastate, or cordate, margins entire, apex acute to attenuate or obtuse to rounded, venation brochidodromous to eucamptodromous, surfaces hirsute, more densely so on midvein abaxially, margins ciliate, 0–4 laminar colleters.  Inflorescences corymbs of extra-axillary umbels on branches, sessile or pedunculate, 5–27-flowered; peduncle 0–4 cm, sometimes branched, hirsute, with 1 caducous bract at the base of each pedicel.  Pedicels 9–24 mm, puberulent with curved trichomes to pilos­ulous.  Flowers erect; calyx lobes narrowly lanceolate, 2–3 mm, apex acute, hirsute to puberulent with curved trichomes; corolla reddish orange (nearly red) to orange or yellow, lobes reflexed with spreading tips, narrowly elliptic, (5–)6–8 mm, apex acute, glabrous abaxially, minutely papillose at base adaxially; gynostegial column 1.2–1.5 mm, fused anthers yellow to yellowish green, cylindric, 2–3 mm, wings right-triangular, closed, apical appendages ovate; corona segments reddish orange (nearly red) to orange or yellow, substipitate, condu­plicate, dorsally flattened, sulcate, 5.5–7 mm, greatly exceeding style apex, apex acute, glabrous, internal appendage subulate, exserted, arching above style apex, glabrous; style apex shallowly depressed, yellow to yellowish green.  Follicles erect on upcurved pedicels, fusiform, 7–14 × 1.2–2 cm, apex long-acuminate or atten­uate, smooth, hirsute.  Seeds ovate, 8–9 × 4–5 mm, margin winged, faces minutely rugulose; coma 3–5 cm.  2n = 22.</description>
  <discussion>Subspecies 3 (3 in the flora).</discussion>
  <description type="distribution">North America, n Mexico.</description>
  <discussion>Asclepias tuberosa is one of the most familiar and beloved North American milkweeds and is a favored element of pollinator gardens because of the cheery orange flowers that attract abundant insect visitors.  The clear latex is unusual in the genus and is often commented upon by collectors.  The subspecies of A. tuberosa are highly intergrading.  It is often difficult to satisfactorily place a given specimen in a particular subspecies; however, the great majority are readily assigned.  It appears that the conspicuous variation in leaf morphology across the subspecies corresponds to genetically structured population variation (R. E. Woodson Jr. 1947).  However, it is unknown to what extent cultivation and other human activities have blurred the distinctions among the geographic variants.  Future recognition of the subspecies should be supported by genetic study with modern techniques.  Although yellow-flowered plants predominate in the western plains, color variation is often pronounced in single populations, and yellow flowers may be encountered anywhere in the range.  Hybridization with A. syriaca is documented, but is exceedingly rare.  Presumed hybrids can be recognized by intermediate floral and vegetative traits.</discussion>
  <references>
    <reference>Woodson, R. E. Jr.  1947.  Some dynamics of leaf variation in Asclepias tuberosa.  Ann. Missouri Bot. Gard. 34: 353–432.</reference>
    <reference>Woodson, R. E. Jr.  1953.  Biometric evidence of natural selection in Asclepias tuberosa.  Proc. Natl. Acad. Sci. U.S.A. 39: 74–79.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf bases hastate, blade margins crisped.</description>
      <determination>70c. Asclepias tuberosa subsp. rolfsii</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Leaf bases cuneate, obtuse, rounded, truncate, or subcordate, blade margins planar.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf bases cuneate to obtuse, rounded, or subcordate, apices rounded to acute, mostly east of the crest of the Appalachian Mountains.</description>
      <determination>70a. Asclepias tuberosa subsp. tuberosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf bases truncate or obtuse to cordate, apices acute to attenuate, mostly west of the crest of the Appalachian Mountains.</description>
      <determination>70b. Asclepias tuberosa subsp. interior</determination>
    </key_statement>
  </key>
</bio:treatment>
