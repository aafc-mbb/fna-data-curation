<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>James S. Pringle</author>
    </source>
    <other_info_on_meta type="volume">14</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">GENTIANACEAE</taxon_name>
    <taxon_name rank="genus" authority="Walter" date="1788">FRASERA</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Carol.,</publication_title>
      <place_in_publication>9, 87.  1788</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family gentianaceae;genus frasera</taxon_hierarchy>
    <other_info_on_name type="etymology">For John Fraser Sr., 1750–1811, Scottish botanical and horticultural explorer</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="Rydberg" date="unknown">Leucocraspedum</taxon_name>
    <taxon_hierarchy>genus leucocraspedum</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="Linnaeus" date="unknown">Swertia</taxon_name>
    <taxon_name rank="section" authority="(Walter) Zuev" date="unknown">Frasera</taxon_name>
    <taxon_hierarchy>genus swertia;section frasera</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="Kellogg" date="unknown">Tesseranthium</taxon_name>
    <taxon_hierarchy>genus tesseranthium</taxon_hierarchy>
  </taxon_identification>
  <number>11.</number>
  <other_name type="common_name">Green-gentian</other_name>
  <description type="morphology">Herbs perennial or long-lived monocarpic, chlorophyllous, glabrous or with stems and leaves puberulent; stems stout and hollow, proximally over 1 cm diam. in F. caroliniensis and F. speciosa, in other species firm and more slender.  Leaves basal and cauline, opposite or whorled in 3s to 5s.  Inflorescences thyrses, verticillasters, or occasionally racemoid cymes.  Flowers 4-merous; calyx lobed nearly to base, lobes lanceolate; corolla violet-blue or pale green or pale yellow to white, usually with violet-blue markings and/or suffusions, nearly rotate or rarely campanulate (distinctly campanulate only in F. tubulosa, somewhat so in F. fastigiata), lobes much longer than tube, margins entire, without plicae between lobes; stamens inserted near base of corolla tube, usually connected by a corona consisting of a low ridge from which, in most species, trichomes or scales arise between the filaments; anthers distinct; ovary sessile; style erect, gradually or abruptly differentiated from ovary, persistent; stigmas 2; nectaries in 1 or 2 foveae on each corolla lobe, rim of fovea openings in some species also surrounding a differentiated area on the adaxial corolla surface distally adjacent to the opening, raised and fringed, the fringe components distinct to the base or nearly so except in F. tubulosa.  Capsules compressed-cylindric to ovoid.  x = 13.</description>
  <discussion>Species 15 (15 in the flora).</discussion>
  <description type="distribution">North America, Mexico.</description>
  <discussion>The division of Swertia in the broad sense, which is sometimes circumscribed so as to include Frasera, is supported by molecular phylogenetic studies by P. Chassot et al. (2001), but as of this writing, the matter of the most appropriate segregation of genera is unsettled.  The acceptance of generic status for Frasera, which has become more frequent in recent years, is provisional in this flora.  Relatively few of the North American species were included in the study by Chassot et al., and its usefulness for generic delimitations is further impaired by the molecular techniques available at the time.  Satisfactory morphological characterizations of Frasera and other genera that might be segregated from Swertia in the broad sense remain elusive.  Some character states that have sometimes been said to distinguish Frasera from Swertia in the narrow sense, including the presence or absence of connate-sheathing leaf bases, four- versus five-merous flowers, and whether or not a distinct, slender style is present, are not consistently applicable in the flora area, and some extralimital species that clustered closely with S. perennis in the phylogenetic study cited above are morphologically similar to species treated as Frasera in the flora area.  Further generic realignments in the complex are to be expected.</discussion>
  <discussion>Characters associated with the nectaries are important in distinguishing among the species and varieties of Frasera, so herbarium specimens should be prepared so that the adaxial surfaces of some corollas are visible.  The nectaries, which may be one or two on each corolla lobe, are located in pits, called foveae, which open adaxially.  Two kinds of foveal morphology are present in North American Frasera.  In species 1 through 5, the opening is immediately adaxial to the nectary, and a raised, fringed rim surrounds the fovea opening only.  This foveal morphology is present in the type species of both generic names Frasera and Swertia.  In species 6 through 15, including all of the species that have been placed in Leucocraspedum, the fovea is pocketlike, with the adaxial opening distal to the nectary or nectaries.  The raised, fringed rim extends beyond the opening of the fovea, so as to surround both the opening and a distally adjacent portion of the adaxial corolla surface differentiated in color and texture.  These structures associated with the nectaries are to be distinguished from the androecial coronas of some species, which consist of fimbriate ridges or entire, serrate, or laciniate scales between the bases of the filaments.</discussion>
  <discussion>In the first group of species distinguished above, the ovary in species 1 and 2 tapers toward the stigmatic lobes with scarcely any differentiation of a style, as also occurs in Swertia perennis, whereas in species 3 through 5, including Frasera caroliniensis, the type of the generic name Frasera, a distinct, slender style is present.  All species in the second group have distinct, slender styles.  White leaf margins prevail in the second group of species but in the first group are present only in F. tubulosa.</discussion>
  <discussion>The inflorescences of Frasera are thyrses or verticillasters, consisting of a central axis along which smaller thyrses (proximally) and/or dichasial or modified cymules are borne in pairs or whorls.  The cymules of the larger plants are usually accompanied by flowers on pedicels that arise directly from the main axis.  In F. coloradensis and F. parryi, the primary branches of the inflorescence may be nearly as long as the central axis.  Small inflorescences, which occur fre­quently in F. ackermaniae and F. gypsicola and occasionally in F. albicaulis, may be racemoid.  Inflorescences are described here as dense if the primary branches are closely spaced, less than 2 cm apart except sometimes at the proximal nodes, and relatively short, so that the inflorescences are usually less than 6 cm wide and/or more than five times as long as wide, with crowded flow­ers; as narrow but not dense in F. gypsicola, in which the inflorescence width is similar but the flowers are few and are not closely spaced; or as diffuse, if the branches are more widely sepa­rated and both the branches and the pedicels are longer and generally strongly divergent, so that the inflorescences are generally more than 6 cm wide and most of the flowers are well separated.</discussion>
  <discussion>Plants of at least some of the monocarpic species of Frasera remain in a rosette stage for sev­eral to many years.  Studies of F. caroliniensis and F. speciosa have shown that, within any one population, after several years in which all or most plants have remained in the rosette stage, all or most of the larger plants are likely to flower in the same year, while the smaller plants remain vegetative.  The environmental factors that induce flowering in such species are not well under­stood.  In some species, the plants are monocarpic with respect to the original crown, which dies after flowering, but prior to flowering may produce new crowns from adventitious buds on the roots.  These may persist as independent plants and flower in later years, after having attained larger size (D. W. Inouye and O. R. Taylor 1980; P. F. Threadgill et al. 1981).</discussion>
  <references>
    <reference>Card, H. H.  1931.  A revision of the genus Frasera.  Ann. Missouri Bot. Gard. 18: 245–280,  plate 14.</reference>
    <reference>St. John, H.  1941.  Revision of the genus Swertia (Gentianaceae) and the reduction of Frasera.  Amer. Midl. Naturalist 26: 1–29.</reference>
    <reference>Shah, J.  1984.  Taxonomic Studies in the Genus Swertia (Gentianaceae).  Ph.D. thesis.  University of Aberdeen.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Openings of foveae directly adaxial to nectaries and surrounded by a raised, fringed rim, with no differentiated areas on the adaxial corolla surface; leaves with or without white margins.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems proximally 1+ cm diam.; calyces 6–25 mm.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Foveae 2 on each corolla lobe, with separate openings; w North America.</description>
      <determination>3. Frasera speciosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Foveae solitary; e, c North America.</description>
      <determination>4. Frasera caroliniensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems to 1 cm diam.; calyces 4–15 mm.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Rim of fovea opening prolonged into a tube 1+ mm, which divides into 2 oblong projections; leaf blades white-margined, proximal blades oblanceolate, distal blades linear-oblong.</description>
      <determination>5. Frasera tubulosa</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Rim of fovea opening fimbriate, scarcely or not prolonged into a tube; leaf blades not white-margined, all or at least basal and mid-cauline blades widely elliptic to ovate.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Corollas light to medium blue or violet-blue with darker veins, often spotted; androecial corona of sparse hairs or absent; Idaho, Washington.</description>
      <determination>1. Frasera fastigiata</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Corollas predominantly white to pale greenish yellow, occasionally with a violet-blue tinge, not spotted; androecial corona of many dense hairs; California, sw Oregon.</description>
      <determination>2. Frasera umpquaensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Openings of foveae distal to the nectaries, usually with a differentiated area on the adaxial corolla surface extending distally from the opening of each fovea (differentiated area absent in F. gypsicola), with a raised rim surrounding the combined foveal opening and differentiated area on the corolla lobe; leaf blades with white margins.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Nectaries 2 per corolla lobe, foveae paired or, if single, 2-lobed at base.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Nectaries in closely paired foveae with separate openings into a single differentiated area shaped ± like the spade on playing cards, rim of differentiated area ± sparsely short- to long-fringed all around, no part scalelike; Arizona, Colorado, New Mexico, Utah.</description>
      <determination>6. Frasera paniculata</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Nectaries each at the base of a 2-lobed fovea with a single opening into an oblong to elliptic differentiated area on the corolla surface, rim proximally projecting as a scale, distally deeply fringed; California, w Nevada.</description>
      <determination>7. Frasera puberulenta</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Nectaries 1 per corolla lobe, foveae not 2-lobed at base.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Raised, completely fringed rim surrounding a round fovea opening only, no differentiated area on corolla surface.</description>
      <determination>12. Frasera gypsicola</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Raised, proximally or completely fringed rim surrounding the fovea opening and a differentiated area on the corolla surface.</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Leaves proximally whorled on main stem, opposite on branches.</description>
      <determination>8. Frasera albomarginata</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">All cauline leaves opposite.</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Inflorescences diffuse, 5–30 cm wide.</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Plants 1.5–2.5 dm; stems and adaxial leaf surfaces puberulent; foveae opening into an orbiculate to elliptic-oblong differentiated area.</description>
      <determination>9. Frasera coloradensis</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Plants 5–16 dm; stems and leaves glabrous; foveae opening into a U-shaped differentiated area on the corolla surface.</description>
      <determination>10. Frasera parryi</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Inflorescences narrow, ± dense, 1.5–4.5(–6) cm wide.</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Only a low, fringed ridge between bases of filaments; mountains of s California.</description>
      <determination>11. Frasera neglecta</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Corona scales 1–6 mm between bases of filaments; widely distributed in California and/or elsewhere.</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Corollas unmarked; foveae opening into an elliptic-obovate to suborbiculate differentiated area, rim ± evenly fringed all around.</description>
      <determination>15. Frasera montana</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Corolla with dark blue or purple dots (except in F. albicaulis var. idahoensis); foveae opening into an oblong, elliptic-oblong, or lance-ovate differentiated area, fringe at distal end of this area absent or distinctly shorter than at base.</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Lowest cauline internode generally longer than all basal leaves; corolla lobes oblong-obovate, widest distal to midlength, tapering ± abruptly to an acute or short-acuminate apex.</description>
      <determination>13. Frasera albicaulis</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Lowest cauline internode generally shorter than some basal leaves; corolla lobes narrowly proximally oblong, widest near midlength, tapering gradually to an acuminate apex.</description>
      <determination>14. Frasera ackermaniae</determination>
    </key_statement>
  </key>
</bio:treatment>
