<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">14</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">GENTIANACEAE</taxon_name>
    <taxon_name rank="genus" authority="Ma" date="1951">GENTIANOPSIS</taxon_name>
    <taxon_name rank="species" authority="(Engelmann) H. H. Iltis" date="1965">barbellata</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>2: 136.  1965</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family gentianaceae;genus gentianopsis;species barbellata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Gentiana</taxon_name>
    <taxon_name rank="species" authority="Engelmann" date="1863">barbellata</taxon_name>
    <place_of_publication>
      <publication_title>Trans. Acad. Sci. St. Louis</publication_title>
      <place_in_publication>2: 216, plate 11, figs. 1–6.  1863</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus gentiana;species barbellata</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Gentianella</taxon_name>
    <taxon_name rank="species" authority="(Engelmann) J. M. Gillett" date="unknown">barbellata</taxon_name>
    <taxon_hierarchy>genus gentianella;species barbellata</taxon_hierarchy>
  </taxon_identification>
  <number>7.</number>
  <other_name type="common_name">Perennial fringed or fragrant fringed or windmill gentian</other_name>
  <description type="morphology">Herbs perennial, 0.5–1.5 dm.  Stems arising from buds on roots, forming clonal colonies; individual stems indeterminate, forming an erect caudex from which 1+ lateral flowering branches arise each year, generally unbranched.  Leaves: basal persistent, blades narrowly spatulate to oblance­olate, 1.5–10 cm × 3–14 mm, apex rounded or obtuse; cauline blades (when present) oblanceolate (proximal) to linear (distal), 1–6 cm × 1–10 mm, apex obtuse to acute.  Peduncles usually absent, occasionally to 0.7 cm.  Flowers usually 1 per primary stem, occasionally paired; calyx 11–25 mm, keels smooth, outer lobes lanceolate, apices acuminate, inner lobes lanceolate to narrowly ovate, apices acute to short-acuminate; corolla bright to deep blue, 25–45(–50) mm, lobes ovate-oblong, 15–25 × 2–6 mm, margins proximally with fringes 1–2 mm, distally dentate; ovary distinctly stipitate.  Seeds papillate, not winged.</description>
  <discussion>The perennating habit of Gentianopsis barbellata is unique within the genus in the flora area.  Stems arise from root buds, forming clonal colonies.  These stems are perennial and indeterminate.  They generally persist as vegetative rosettes for a few years, then each year thereafter produce a flowering branch from an axillary bud.  Especially robust plants may produce more than one flowering branch per year (G. Engelmann 1879; P. A. Groff 1989).</discussion>
  <discussion>The flowers of Gentianopsis barbellata appear to have longer peduncles than they actually do when they are closely subtended by narrow bracts that may be confused with the calyx lobes.  Although a few minute trichomes are sometimes found on the corolla tube near the level of stamen insertion in other species of Gentianopsis in the flora area, these are more numerous and more prominent in G. barbellata, forming distinct rows flanking the adnate portions of the stamens, as in the European G. ciliata (Linnaeus) Ma (G. Engelmann 1863; J. M. Gillett 1957).</discussion>
  <description type="phenology">Flowering late summer–fall.</description>
  <description type="habitat">Rocky slopes, open con­iferous and aspen woods, wet alpine meadows, calcar­eous soils.</description>
  <description type="elevation">1000–3800 m.</description>
  <description type="distribution">Ariz., Colo., N.Mex., Utah, Wyo.</description>
</bio:treatment>
