<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>Casie L. Reed, Alexander Krings</author>
    </source>
    <other_info_on_meta type="volume">14</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="unknown" date="unknown" rank="family">Apocynaceae</taxon_name>
    <taxon_name rank="genus" authority="R. Brown" date="1820">CRYPTOSTEGIA</taxon_name>
    <place_of_publication>
      <publication_title>Bot. Reg.</publication_title>
      <place_in_publication>5: plate 435.  1820</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family apocynaceae;genus cryptostegia</taxon_hierarchy>
    <other_info_on_name type="special_status">I</other_info_on_name>
    <other_info_on_name type="etymology">Greek kryptos, hidden, and stegein, to cover, alluding to enclosure of five-scaled crown within corolla tube</other_info_on_name>
  </taxon_identification>
  <number>24.</number>
  <other_name type="common_name">Rubbervine</other_name>
  <description type="morphology">Lianas or subshrubs; latex white.  Stems climbing or with self-supporting branches, unarmed, glabrous or eglandular-pubescent.  Leaves persistent, opposite, petiolate; stipular colleters inter­petiolar and intrapetiolar; laminar colleters absent.  Inflorescences terminal cymes, pedunculate.  Flowers: calycine colleters present; corolla white, pale pink, or purple-pink, infundibuliform, aestivation dextrorse; corolline corona 2-fid or entire; androecium and gynoecium not united into a gynostegium; stamens inserted at base of corolla tube; anthers connivent, adherent to stigma, connectives apiculate, locules 4; pollen in tetrads, not massed into pollinia, but shed onto translators; nectary absent.  Fruits follicles, paired, deflexed, green to brown, fusiform, strongly 3-angled, striate or smooth, glabrous or minutely pubescent.  Seeds oblong, flattened, not winged, not beaked, comose, not arillate.  x = 11.</description>
  <discussion>Species 2 (2 in the flora).</discussion>
  <description type="distribution">introduced; Indian Ocean Islands (Madagascar); introduced also in Mexico, West Indies, Bermuda, Central America, South America, Asia, Africa, elsewhere in Indian Ocean Islands, Pacific Islands, Australia.</description>
  <discussion>Cryptostegia is endemic to Madagascar, but its two species have been introduced pantropically.  Hybrids between C. grandiflora and C. madagascariensis have been reported in cultivated landscapes in Florida.  There is no evidence that these hybrids have become naturalized.</discussion>
  <references>
    <reference>Klackenberg, J.  2001.  Revision of the genus Cryptostegia R. Br. (Apocynaceae, Periplocoideae).  Adansonia 23: 205–218.</reference>
    <reference>Marohasy, J. and P. I. Forster.  1991.  A taxonomic revision of Cryptostegia R. Br. (Asclepiadaceae: Periplocoideae).  Austral. Syst. Bot. 4: 571–577.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Calyx lobes 13–20 mm, margins reflexed; corolline corona 2-fid; translator spathes orbiculate; follicles (8–)10–15.5 cm.</description>
      <determination>1. Cryptostegia grandiflora</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Calyx lobes 7–13(–14) mm, margins ± flat; corolline corona entire; translator spathes lanceolate to ovate; follicles 5–10 cm.</description>
      <determination>2. Cryptostegia madagascariensis</determination>
    </key_statement>
  </key>
</bio:treatment>
