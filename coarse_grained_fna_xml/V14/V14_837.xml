<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">14</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">GENTIANACEAE</taxon_name>
    <taxon_name rank="genus" authority="Ma" date="1951">GENTIANOPSIS</taxon_name>
    <taxon_name rank="species" authority="(Rafinesque) Holub" date="1967">virgata</taxon_name>
    <taxon_name rank="subspecies" authority="(Holm) J. S. Pringle" date="2004">macounii</taxon_name>
    <place_of_publication>
      <publication_title>Sida</publication_title>
      <place_in_publication>21: 529. 2004</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family gentianaceae;genus gentianopsis;species virgata;subspecies macounii</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Gentiana</taxon_name>
    <taxon_name rank="species" authority="Holm" date="1901">macounii</taxon_name>
    <place_of_publication>
      <publication_title>Ottawa Naturalist</publication_title>
      <place_in_publication>15: 110.  1901</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus gentiana;species macounii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">G.</taxon_name>
    <taxon_name rank="species" authority="Victorin" date="unknown">gaspensis</taxon_name>
    <taxon_hierarchy>genus g.;species gaspensis</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">G.</taxon_name>
    <taxon_name rank="species" authority="(Lunell) Victorin" date="unknown">tonsa</taxon_name>
    <taxon_hierarchy>genus g.;species tonsa</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Gentianella</taxon_name>
    <taxon_name rank="species" authority="(Froelich) Berchtold &amp; J. Presl" date="unknown">crinita</taxon_name>
    <taxon_name rank="subspecies" authority="(Holm) J. M. Gillett" date="unknown">macounii</taxon_name>
    <taxon_hierarchy>genus gentianella;species crinita;subspecies macounii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Gentianopsis</taxon_name>
    <taxon_name rank="species" authority="(Froelich) Ma" date="unknown">crinita</taxon_name>
    <taxon_name rank="subspecies" authority="(Holm) Á. Löve &amp; D. Löve" date="unknown">macounii</taxon_name>
    <taxon_hierarchy>genus gentianopsis;species crinita;subspecies macounii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">G.</taxon_name>
    <taxon_name rank="species" authority="(Holm) H. H. Iltis" date="unknown">macounii</taxon_name>
    <taxon_hierarchy>genus g.;species macounii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">G.</taxon_name>
    <taxon_name rank="species" authority="(Holm) Ma" date="unknown">procera</taxon_name>
    <taxon_name rank="subspecies" authority="(Holm) H. H. Iltis" date="unknown">macounii</taxon_name>
    <taxon_hierarchy>genus g.;species procera;subspecies macounii</taxon_hierarchy>
  </taxon_identification>
  <number>2c.</number>
  <other_name type="common_name">Macoun’s fringed gentian</other_name>
  <other_name type="common_name">gentiane de Macoun</other_name>
  <description type="morphology">Herbs 0.5–4 dm.  Peduncles 5–20 cm.  Flowers often 1 per primary stem, occasionally 2–5; calyx 13–25 mm, keels minutely granular-scabridulous near base or nearly smooth; corolla deep blue or rarely pale blue to white, 18–40 mm, lobes oblong-obovate, 10–20 × (3–)5–13 mm, margins with sparse lateral fringes to 2 mm, merely erose or shallowly dentate, with teeth less than 2 mm, distally and around the apex; ovary at flowering subsessile or with a short, thick gynophore much shorter than body.  2n = 78.</description>
  <discussion>A report of subsp. macounii from Nebraska is erro­neous, having been based on a specimen actually from North Dakota (studies for this flora).  At least one report of this taxon from the southern part of the Northwest Territories is correct, but most specimens from the Northwest Territories so identified have been reidenti­fied as Gentianopsis detonsa subsp. raupii in studies for this flora.</discussion>
  <discussion>A disjunct population in brackish meadows along the estuary of the Bonaventure River, Gaspé Peninsula, Quebec, has been called Gentiana gaspensis but appears to constitute only a relatively uniform population, the morphology of which falls within the range of variation exhibited by plants called Gentianopsis virgata subsp. macounii (H. A. Gleason 1952, as Gentiana tonsa; J. M. Gillett 1957; A. Dutilly et al. 1958).</discussion>
  <discussion>Subspecies macounii and subsp. virgata intergrade in Minnesota, southern Manitoba, and occasionally as far west as southern Saskatchewan.</discussion>
  <description type="phenology">Flowering summer–early fall.</description>
  <description type="habitat">Wet meadows, river­banks, margins of sloughs, fens, beach ridges, lake­shores, brackish meadows.</description>
  <description type="elevation">0–1900 m.</description>
  <description type="distribution">Alta., B.C., Man., N.W.T., Ont., Que., Sask.; Minn., Mont., N.Dak., S.Dak.</description>
</bio:treatment>
