<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>Mark Fishbein</author>
    </source>
    <other_info_on_meta type="volume">14</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="unknown" date="unknown" rank="family">Apocynaceae</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">CYNANCHUM</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 212.  1753</place_in_publication>
      <publication_title>Gen. Pl. ed.</publication_title>
      <place_in_publication>5, 101.  1754</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family apocynaceae;genus cynanchum</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek kyon, dog, and ancho, strangle, alluding to toxicity of some species</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="Rafinesque" date="unknown">Ampelamus</taxon_name>
    <taxon_hierarchy>genus ampelamus</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="A. Gray" date="unknown">Mellichampia</taxon_name>
    <taxon_hierarchy>genus mellichampia</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="Vail" date="unknown">Rouliniella</taxon_name>
    <taxon_hierarchy>genus rouliniella</taxon_hierarchy>
  </taxon_identification>
  <number>27.</number>
  <other_name type="common_name">Strangle-vine</other_name>
  <other_name type="common_name">swallow-wort</other_name>
  <other_name type="common_name">milkweed vine</other_name>
  <description type="morphology">Vines [erect herbs, subshrubs], herbaceous or somewhat woody, but not corky, at base [succulent]; latex white or clear.  Stems twining [not twining], unarmed, puberulent in single line or glabrate [variously pubescent or glabrous].  Leaves persistent [reduced to inconspicuous scales], opposite, petiolate [sessile]; stipular colleters interpetiolar; laminar colleters present [absent].  Inflorescences extra-axillary, racemiform, corymbiform, or paniculiform [umbelliform], sessile or pedunculate.  Flowers: calycine colleters apparently absent [present?]; corolla white, cream, or green [brown, purple, red, pink], campanulate [rotate], aestivation imbricate; corolline corona absent; androecium and gynoecium united into a gynostegium adnate to corolla tube; gynostegial corona 1-[2-]whorled; anthers adnate to style, locules 2; pollen in each theca massed into a rigid, vertically oriented pollinium, pollinia lacrimiform, joined from adjacent anthers by translators to common corpulsculum and together forming a pollinarium.  Fruits follicles, usually solitary, variously oriented, green to brown, terete or subterete, smooth, glabrous.  Seeds winged, not beaked, ovate, flattened, comose, not arillate.  x = 11.</description>
  <discussion>Species ca. 300 (3 in the flora).</discussion>
  <description type="distribution">United States, Mexico, West Indies, Central America, South America, Europe, Asia, Africa, Indian Ocean Islands (Madagascar), Australia.</description>
  <discussion>The influential revision of North American milkweed genera by R. E. Woodson Jr. (1941) adopted a very broad (and evidently polyphyletic) circumscription of Cynanchum, reducing genera recognized here to synonymy (Metastelma, Orthosia, Pattalias).  Similarly, regional floras for Canada and the United States have included the introduced species here treated in Vincetoxicum under Cynanchum.  The polyphyly of this broad concept of Cynanchum has been convincingly demonstrated (for example, by S. Liede and A. Täuber 2002).  Another potential source of confusion is that the species of Funastrum have previously been included in the Old World genus Sarcostemma R. Brown.  Although Sarcostemma has been synonymized with Cynanchum following the phylogenetic results of Liede and Täuber, the species of Funastrum are distantly related to those of Cynanchum.</discussion>
  <references>
    <reference>Liede, S. and A. Täuber.  2002.  Circumscription of the genus Cynanchum (Apocynaceae–Asclepiadoideae).  Syst. Bot. 27: 789–800.</reference>
    <reference>Sundell, E.  1981.  The New World species of Cynanchum subgenus Mellichampia (Asclepiadaceae).  Evol. Monogr. 5: 1–63.</reference>
  </references>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Latex clear; dwarf axillary shoots common, resembling stipules (pseudostipules); corollas lacking inframarginal ridges adaxially; corona segments distinct, divided into 2 subulate lobes; c, e United States, including e Texas.</description>
      <determination>1. Cynanchum laeve</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Latex white; dwarf axillary shoots rare; corollas with inframarginal ridges adaxially; corona segments united at base, shallowly lobed or unlobed; Arizona, Texas.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Corollas deeply campanulate, lobes 7–8 mm, cream (fading yellowish), inframarginal ridges pilose; corona segments unlobed; Arizona.</description>
      <determination>2. Cynanchum ligulatum</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Corollas shallowly campanulate to campanulate-rotate, lobes 3–4 mm, green with white margins, inframarginal ridges glabrous; corona segments 3-lobed at apex; Texas.</description>
      <determination>3. Cynanchum unifarium</determination>
    </key_statement>
  </key>
</bio:treatment>
