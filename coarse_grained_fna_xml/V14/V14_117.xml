<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">14</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="unknown" date="unknown" rank="family">Apocynaceae</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">ASCLEPIAS</taxon_name>
    <taxon_name rank="species" authority="Walter" date="1788">humistrata</taxon_name>
    <place_of_publication>
      <publication_title>Fl. Carol.,</publication_title>
      <place_in_publication>105.  1788</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family apocynaceae;genus asclepias;species humistrata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>19.</number>
  <other_name type="common_name">Sandhill or pinewoods or pink-veined milkweed</other_name>
  <description type="morphology">Herbs.  Stems 1–10, decumbent or ascending to erect, unbranched, 20–50 cm, gla­brous, glaucous, rhizomes absent.  Leaves opposite, sessile, with 0 or 1 stipular colleter on each side of leaf base; blade ovate, 4–11 × 2.5–8 cm, sub­succulent, base cordate, clasp­ing, margins entire, apex obtuse, mucronulate, venation eucamptodromous to brochidodromous, surfaces gla­brous, glaucous, laminar colleters absent.  Inflo­rescences terminal, sometimes branched, and extra-axillary at upper nodes, pedunculate, 7–37-flowered; peduncle 2.6–5.5 cm, glabrous, glaucous, with 1 bract at the base of each pedicel.  Pedicels 18–26 mm, glabrous.  Flowers erect to spreading; calyx lobes lanceolate, 2–3 mm, apex obtuse, glabrous; corolla pink or red to pinkish green or reddish green, lobes reflexed, sometimes with spreading tips, oval, 5.5–6 mm, apex acute to obtuse, glabrous; gyno­stegial column 0.5–1 mm; fused anthers brown, cylindric, 1.2–1.5 mm, wings right-triangular, tips closed, apical appendages broadly ovate; corona seg­ments pink to nearly white at base, white at apex, yel­lowing with age, sessile, conduplicate, dorsally rounded, 3–3.5 mm, slightly exceeding style apex, base subsaccate, apex truncate with a proximal tooth on each side, glabrous, internal appendage ensiform, slightly incurved, slightly exserted, glabrous; style apex shallowly depressed, pink.  Follicles erect on upcurved pedicels, lance-ovoid, 8–12 × 0.8–1.7 cm, apex long-acuminate to attenuate, smooth, glabrous, glaucous.  Seeds ovate to oval, 8–8.5 × 5–6 mm, margin winged, faces papillose-rugulose; coma 3–3.5 cm.</description>
  <discussion>Asclepias humistrata is a distinctive milkweed unlike any other in its range.  Its decumbent habit with vertically oriented leaves, bearing strongly contrasting white or pink venation, is unmatched by any other sandhill species.  It is apparently closely related to the highly disjunct A. cordifolia of the Pacific Northwest, sug­gesting an unusual biogeographic history (M. Fishbein et al. 2011).  It shares with this species bluish, grayish, or purplish glaucous herbage.  Asclepias humistrata often exhibits remarkably high fruit set and, perhaps as a consequence, often grows in large, dense populations.  It is reported as possibly extirpated from Louisiana, where it was documented from Washington Parish.  Asclepias amplexicaulis Michaux, an illegitimate synonym, created confusion between this species and A. amplexicaulis Smith, a similarly glaucous, cordate-leaved species.</discussion>
  <description type="phenology">Flowering (Feb–)Mar–Oct; fruiting (Mar–)Apr–Oct(–Nov).</description>
  <description type="habitat">Dunes, sandhills, ridges, slopes, coastal strand, streamsides, sandy soils, pine flatwoods, pine-oak woods, oak and pine-oak scrub.</description>
  <description type="elevation">0–200 m.</description>
  <description type="distribution">Ala., Fla., Ga., La., Miss., N.C., S.C.</description>
</bio:treatment>
