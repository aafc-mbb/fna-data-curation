<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">14</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="unknown" date="unknown" rank="family">Apocynaceae</taxon_name>
    <taxon_name rank="genus" authority="Walter" date="1788">AMSONIA</taxon_name>
    <taxon_name rank="subgenus" authority="unknown" date="unknown">Amsonia</taxon_name>
    <taxon_hierarchy>family apocynaceae;genus amsonia;subgenus amsonia</taxon_hierarchy>
  </taxon_identification>
  <number>3a.</number>
  <description type="morphology">Stems usually branched only on distal portion.  Bracts on distal portions of inflorescences narrowly deltate to deltate or ovate,  to 2.5 mm.  Flowers: corolla tube (5–)6–9(–10) mm, widest at apex, not distally constricted; stigma depressed-capitate to truncate.  Follicles terete, not strongly constricted between seeds.  Seeds reddish brown, terete with usually diagonally truncate ends, with convoluted surface.</description>
  <discussion>Species 7 (5 in the flora).</discussion>
  <description type="distribution">United States, n Mexico, Europe (Greece, Turkey), e Asia.</description>
  <discussion>Delimitation of species and varieties in the southeastern subg. Amsonia is challenging.  The distinctions between the widespread and variable Amsonia tabernaemontana and the geograph­ically more restricted A. ludoviciana and A. rigida are small or inconsistent.  Problematic specimens identified as probable interspecific hybrids, including hybrids between A. ciliata and the A. tabernaemontana group, also exist.  Outside North America, subg. Amsonia includes A. orientalis Decaisne of Europe and A. elliptica (Thunberg) Roemer &amp; Schultes of Asia.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Abaxial leaf surfaces densely (to moderately) tomentose.</description>
      <determination>3. Amsonia ludoviciana</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Abaxial leaf surfaces glabrous or sparsely or moderately pubescent, not tomentose.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stem leaf blades linear.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stem leaf blades 2.5–4.6(–5.5) cm × 0.3–1(–1.6) mm (–2.4 mm if unrolled); North Carolina to Florida, Missouri, Oklahoma.</description>
      <determination>1. Amsonia ciliata</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Stem leaf blades (5–)7.5–11 cm × 2–3.2(–4.2) mm; Arkansas, Oklahoma.</description>
      <determination>2. Amsonia hubrichtii</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stem leaf blades narrowly elliptic, elliptic, narrowly oblong, ligulate, lanceolate, ovate, or obovate.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf blades at least slightly heteromorphic, surfaces glabrous except margins sometimes ciliate; stem leaf blades ligulate, very narrowly elliptic to elliptic or very narrowly lanceolate, 2.5–6.5 cm × 3–15 mm; branch leaf blades linear to ligulate or narrowly elliptic, 2.5–5.6 cm × 3–6 mm.</description>
      <determination>1. Amsonia ciliata</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf blades not heteromorphic, surfaces glabrous or pubescent; stem leaf blades lanceolate or narrowly elliptic to elliptic, narrowly oblong, narrowly lanceolate, ovate, or obovate, (2.5–)3.3–12.5(–14.4) cm × (4–)8–50(–65) mm; branch leaf blades similar to stem leaf blades.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Stem leaf blades elliptic, narrowly oblong, lanceolate, or obovate, (3.1–)3.9–8.4(–9.3) cm × (4–)8–18(–23) mm, margins sometimes irregularly revolute giving a dentate appearance; outer surface of corollas usually glabrous (rarely sparsely short-pubescent).</description>
      <determination>4. Amsonia rigida</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Stem leaf blades lanceolate to narrowly lanceolate, ovate, narrowly elliptic, or elliptic, (2.5–)3.3–12.5(–14.4) cm × (8–)11–50(–65) mm, margins not revolute or evenly revolute; outer surface of corollas normally with 5 patches of sparse to moderate, long pubescence at base of corolla lobes and distal part of tube (very rarely glabrate).</description>
      <determination>5. Amsonia tabernaemontana</determination>
    </key_statement>
  </key>
</bio:treatment>
