<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>Richard K. Brummitt†</author>
    </source>
    <other_info_on_meta type="volume">14</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">Convolvulaceae</taxon_name>
    <taxon_name rank="genus" authority="R. Brown" date="1810">CALYSTEGIA</taxon_name>
    <place_of_publication>
      <publication_title>Prodr.,</publication_title>
      <place_in_publication>483.  1810</place_in_publication>
      <other_info_on_pub>name conserved</other_info_on_pub>
    </place_of_publication>
    <taxon_hierarchy>family convolvulaceae;genus calystegia</taxon_hierarchy>
    <other_info_on_name type="etymology">Greek calyx and stegos, covering, alluding to two large bracts enclosing calyx</other_info_on_name>
  </taxon_identification>
  <number>13.</number>
  <other_name type="common_name">Morning glory</other_name>
  <description type="morphology">Perennials or subshrubs.  Stems usually twining-climbing, sometimes ascending, decumbent, erect, procumbent, prostrate, or trailing, rarely almost absent, usually hairy, hairs not branched, glandular, or stellate, sometimes glabrate, glabrescent, or glabrous.  Leaves petiolate; blade usually cordate, elliptic, linear, oblong, oblong-hastate, orbiculate, oval, ovate, reniform, tri­angular, or triangular-hastate, rarely palmately 7–9-lobed, (15–)20–130 mm, base usually lobed or truncate, sometimes cuneate, surfaces glabrate, glabrescent, glabrous, ± pilose, tomentose, tomentulose, or villous.  Inflorescences usually axillary, rarely terminal, compound cymes, bracteate; peduncles 1(–4)-flowered.  Flowers: sepals ± elliptic, lanceolate, lance-ovate, oblong, oblong-ovate, oval, or ovate, (5–)8–15(–25) mm; corolla usually white, sometimes cream, pink, purple, red, or yellow, campanulate to funnelform, (20–)25–60(–73)[–88] mm, limb entire or 5-lobed or -angled, rarely multilobed; ovary 1-locular, sometimes with partial septum; styles 1; stigma lobes 2, linear to oblong, apices blunt.  Fruits capsular, ± globose, dehiscence irregular.  Seeds (1–)2–4, pyramidal to subglobose or trigonous, glabrous, papillate, smooth, or reticulate.  x = 12.</description>
  <discussion>Species ca. 30 (20 in the flora).</discussion>
  <description type="distribution">North America, Mexico, South America, Eurasia, Africa, Atlantic Islands, Pacific Islands, Australia.</description>
  <discussion>H. Hallier (1893) and W. H. Lewis and R. L. Oliver (1965) summarized arguments for treat­ing Calystegia and Convolvulus as distinct genera.  In a molecular analysis, M. A. Carine et al. (2004) found Calystegia nested within Convolvulus.</discussion>
  <discussion>Species delimitation is problematic throughout Calystegia, with geographic and morphological intergradation between taxa, and often arbitrary limits have to be adopted to avoid impractically broad species.  Hybridization is common where species overlap geographically.  It is difficult to pinpoint any species which is not taxonomically subdivided and which does not intergrade or hybridize with others.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Bracts (1–)2–10(–50) mm distant from sepals, margins entire, lobed, or toothed.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems erect, sometimes intertwined; leaf blades usually linear to narrowly triangular, sometimes ovate, base not lobed or hastate-lobed and lobes ± linear, oblong, or trian­gular, 1-pointed.</description>
      <determination>1. Calystegia longipes</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Stems ascending, decumbent, procumbent, trailing, or twining-climbing; leaf blades oblong, oblong-ovate, orbiculate, ovate, broadly to narrowly triangular, triangular-hastate, or palmately 7–9-lobed, base usually lobed and lobes rounded or 1–3-pointed, base rarely cuneate or ± truncate.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blades oblong, oblong-ovate, orbiculate, or ovate, base lobed and lobes rounded or base cuneate to ± truncate.</description>
      <determination>2. Calystegia felix</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blades usually broadly to narrowly triangular, ovate-triangular, or triangular-hastate (daggerlike), or palmately 7–9-lobed, sometimes ± reniform, base usually lobed and lobes rounded or 1–3-pointed, base rarely cuneate or ± truncate.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Herbage glabrous.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaf blades ± triangular to ovate-triangular; bracts linear, margins entire or proximally lobed or toothed.</description>
      <determination>3. Calystegia purpurata</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Leaf blades narrowly triangular-hastate; bracts elliptic to broadly elliptic-oblong, margins entire.</description>
      <determination>4. Calystegia peirsonii</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Herbage usually hairy, at least near leaf blade sinus and/or tip of peduncle, sometimes glabrate or glabrescent.</description>
      <next_statement_id>6</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaf blades palmately 7–9-lobed.</description>
      <determination>5. Calystegia stebbinsii</determination>
    </key_statement>
    <key_statement>
      <statement_id>6</statement_id>
      <description type="morphology">Leaf blades not palmately lobed.</description>
      <next_statement_id>7</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Bract margins entire.</description>
      <next_statement_id>8</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaf blades ± triangular-hastate, middle lobe ± lance-linear; bracts 1–2 mm distant from sepals, linear, 5–16(–20) × 0.5–1.5 mm.</description>
      <determination>6. Calystegia vanzuukiae</determination>
    </key_statement>
    <key_statement>
      <statement_id>8</statement_id>
      <description type="morphology">Leaf blades ± triangular; bracts (1–)3–7 mm distant from sepals, linear to linear-oblong, 4–13(–18) × 1–4(–5) mm.</description>
      <determination>7. Calystegia occidentalis</determination>
    </key_statement>
    <key_statement>
      <statement_id>7</statement_id>
      <description type="morphology">Bract margins proximally lobed or toothed.</description>
      <next_statement_id>9</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Herbage tomentose to villous; leaf blades ± broadly to narrowly triangular, basal lobes 2(–3)-pointed.</description>
      <determination>8. Calystegia malacophylla</determination>
    </key_statement>
    <key_statement>
      <statement_id>9</statement_id>
      <description type="morphology">Herbage glabrate or hairy; leaf blades narrowly triangular or triangular-hastate, basal lobes rounded or 1–2-pointed.</description>
      <next_statement_id>10</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Bracts 1–2 mm distant from sepals, linear, 5–16(–20) × 0.5–1.5 mm.</description>
      <determination>6. Calystegia vanzuukiae</determination>
    </key_statement>
    <key_statement>
      <statement_id>10</statement_id>
      <description type="morphology">Bracts 2–12(–15) mm distant from sepals, lanceolate, linear, linear-oblong, oblanceolate, or narrowly to broadly triangular, 5–22(–30) × 2–4(–7) mm.</description>
      <determination>7. Calystegia occidentalis</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Bracts immediately subtending, less than 1 mm from, sepals, margins entire.</description>
      <next_statement_id>11</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Corollas horticultural doubles, limbs multilobed; stamens and ovaries absent.</description>
      <determination>9. Calystegia pubescens</determination>
    </key_statement>
    <key_statement>
      <statement_id>11</statement_id>
      <description type="morphology">Corollas not doubles, limbs weakly 5-lobed, 5-angled, or entire; stamens and ovaries present.</description>
      <next_statement_id>12</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Bracts 1.5–3.5(–4) mm wide.</description>
      <next_statement_id>13</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Herbage tomentellous, tomentose, or villous.</description>
      <determination>10. Calystegia collina</determination>
    </key_statement>
    <key_statement>
      <statement_id>13</statement_id>
      <description type="morphology">Herbage glabrous or ± hairy, not tomentellous, tomentose, or villous.</description>
      <next_statement_id>14</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Leaf blades narrowly triangular-hastate, base lobed, lobes ± oblong to rhombic, 1-pointed.</description>
      <determination>4. Calystegia peirsonii</determination>
    </key_statement>
    <key_statement>
      <statement_id>14</statement_id>
      <description type="morphology">Leaf blades rounded-deltate to triangular-hastate, base cuneate or lobed, lobes not oblong to rhombic and 1-pointed.</description>
      <next_statement_id>15</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Herbage glabrous; bract apices acute to obtuse.</description>
      <determination>11. Calystegia atriplicifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>15</statement_id>
      <description type="morphology">Herbage sparsely hairy; bract apices acute.</description>
      <determination>12. Calystegia subacaulis</determination>
    </key_statement>
    <key_statement>
      <statement_id>12</statement_id>
      <description type="morphology">Bracts 4–30 mm wide.</description>
      <next_statement_id>16</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Perennials or subshrubs, rootstock woody.</description>
      <determination>13. Calystegia macrostegia</determination>
    </key_statement>
    <key_statement>
      <statement_id>16</statement_id>
      <description type="morphology">Perennials, rhizomatous.</description>
      <next_statement_id>17</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Leaf blades ± reniform, ± fleshy; corollas pink.</description>
      <determination>14. Calystegia soldanella</determination>
    </key_statement>
    <key_statement>
      <statement_id>17</statement_id>
      <description type="morphology">Leaf blades not reniform, not fleshy; corollas usually pink, cream, or white, rarely purple.</description>
      <next_statement_id>18</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Stems usually erect, procumbent, or twining-climbing, sometimes trailing or proximally erect and distally twining-climbing; mostly c, e North America.</description>
      <next_statement_id>19</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Leaf blades triangular to triangular-hastate, base lobed, lobes 2(–3)-pointed; corollas usually pink, sometimes purple or white, 21–32(–35) mm.</description>
      <determination>15. Calystegia hederacea</determination>
    </key_statement>
    <key_statement>
      <statement_id>19</statement_id>
      <description type="morphology">Leaf blades ± cordate, elliptic, elliptic-ovate, linear, oblong, ovate, broadly to narrowly triangular, or triangular-hastate, base cuneate, rounded, or lobed, lobes obtuse, rounded, or 1–2-pointed; corollas pink or white, (20–)35–65(–70) mm.</description>
      <next_statement_id>20</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Stems erect.</description>
      <determination>16. Calystegia spithamaea</determination>
    </key_statement>
    <key_statement>
      <statement_id>20</statement_id>
      <description type="morphology">Stems usually twining-climbing, sometimes trailing or proxi­mally erect and distally twining-climbing.</description>
      <next_statement_id>21</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Leaf blades elliptic-ovate, basal lobes obtuse or rounded, surfaces some­times whitish tomentose.</description>
      <determination>17. Calystegia catesbeiana</determination>
    </key_statement>
    <key_statement>
      <statement_id>21</statement_id>
      <description type="morphology">Leaf blades ± cordate, linear, oblong, oblong-ovate, oval, ovate, triangular, or triangular-hastate, basal lobes usually 1–2-pointed, sometimes rounded or 1-pointed, surfaces not whitish.</description>
      <next_statement_id>22</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>22</statement_id>
      <description type="morphology">Leaf blade basal sinuses usually acute to rounded, sometimes quadrate, rarely closed; bracts proximally flat or keeled, not or scarcely saccate, margins not or scarcely enfolding sepals, apices acute to subobtuse or truncate.</description>
      <determination>18. Calystegia sepium</determination>
    </key_statement>
    <key_statement>
      <statement_id>22</statement_id>
      <description type="morphology">Leaf blade basal sinuses ± quadrate to rounded; bracts proximally saccate, margins enfolding sepals, apices obtuse to truncate.</description>
      <determination>19. Calystegia silvatica</determination>
    </key_statement>
    <key_statement>
      <statement_id>18</statement_id>
      <description type="morphology">Stems usually ascending-decumbent, sometimes procumbent, suberect, trailing, or proximally erect and distally weakly twining-climbing, or almost absent; mostly w North America (C. macounii plains and west).</description>
      <next_statement_id>23</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>23</statement_id>
      <description type="morphology">Herbage glabrous.</description>
      <determination>11. Calystegia atriplicifolia</determination>
    </key_statement>
    <key_statement>
      <statement_id>23</statement_id>
      <description type="morphology">Herbage moderately or sparsely hairy, puberulent, tomentellous, tomentose, or villous.</description>
      <next_statement_id>24</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>24</statement_id>
      <description type="morphology">Herbage sparsely hairy, hairs appressed.</description>
      <determination>12. Calystegia subacaulis</determination>
    </key_statement>
    <key_statement>
      <statement_id>24</statement_id>
      <description type="morphology">Herbage moderately or sparsely hairy, puberulent, tomentellous, tomentose, or villous, hairs not appressed.</description>
      <next_statement_id>25</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>25</statement_id>
      <description type="morphology">Leaf blade basal lobes ± rhombic, rounded; east of California.</description>
      <determination>20. Calystegia macounii</determination>
    </key_statement>
    <key_statement>
      <statement_id>25</statement_id>
      <description type="morphology">Leaf blade basal lobes 1–3-pointed; California.</description>
      <next_statement_id>26</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>26</statement_id>
      <description type="morphology">Stems mostly to 60(–100) cm; leaves not in basal rosettes.</description>
      <determination>8. Calystegia malacophylla</determination>
    </key_statement>
    <key_statement>
      <statement_id>26</statement_id>
      <description type="morphology">Stems to (2–)50 cm or almost absent; leaves usually in basal rosettes.</description>
      <next_statement_id>27</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>27</statement_id>
      <description type="morphology">Herbage tomentellous, tomentose, or villous; leaf blade margins ± undulate.</description>
      <determination>10. Calystegia collina</determination>
    </key_statement>
    <key_statement>
      <statement_id>27</statement_id>
      <description type="morphology">Herbage moderately to sparsely hairy; leaf blade margins not notably undulate.</description>
      <determination>12. Calystegia subacaulis</determination>
    </key_statement>
  </key>
</bio:treatment>
