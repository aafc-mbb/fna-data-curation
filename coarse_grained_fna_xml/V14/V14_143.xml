<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">14</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="unknown" date="unknown" rank="family">Apocynaceae</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">ASCLEPIAS</taxon_name>
    <taxon_name rank="species" authority="Elliott" date="1817">tomentosa</taxon_name>
    <place_of_publication>
      <publication_title>Sketch Bot. S. Carolina</publication_title>
      <place_in_publication>1: 320.  1817</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family apocynaceae;genus asclepias;species tomentosa</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <number>43.</number>
  <other_name type="common_name">Velvetleaf or tuba milkweed</other_name>
  <description type="morphology">Herbs.  Stems solitary (rarely 2), erect, unbranched (rarely branched), 25–150 cm, densely puberulent with curved tri­chomes, not glaucous, rhizomes absent.  Leaves opposite, petio­late, with 1 stipular colleter on each side of petiole; petiole 2–9 mm, densely puberulent with curved trichomes; blade lanceolate or ovate to oval or oblong or elliptic to oblanceolate or obovate, 3.5–10 × 1–5 cm, chartaceous, base obtuse to subcordate, margins crisped, apex acute or obtuse to truncate or emarginate, sometimes mucronate, venation eucamptodromous to brochidodromous, surfaces puberulent with curved trichomes to tomentulose, margins ciliate, 4–8 laminar colleters.  Inflorescences extra-axillary, sessile or pedun­culate, 5–37-flowered; peduncle 0–0.3 cm, densely puberulent with curved trichomes to tomentulose, with 1 caducous bract at the base of each pedicel.  Pedicels 12–19 mm, densely pilose to tomentulose.  Flowers erect; calyx lobes lanceolate, 3–4 mm, apex acute, pilose; corollas green, often tinged reddish or purplish, lobes reflexed with spreading tips, oval, 7–9 mm, apex acute, glabrous; gynostegial column 0.5–1 mm; fused anthers green, obconic, 3–4 mm, wings broadly right-triangular, closed, apical appendages broadly oval; corona segments green with cream apex, often tinged pink or purple, stipitate, conduplicate, dorsally flattened, 3–4 mm, exceeded by style apex, apex truncate, glabrous, internal appendage falcate, exserted, sharply inflexed towards style apex, papillose; style apex shallowly depressed, green, fading pink or red.  Follicles erect on upcurved pedicels, fusiform to narrowly lance-ovoid, 9–18 × 1–1.5 cm, apex long-acuminate, smooth, puberulent with curved trichomes to pilosulous or tomentulose.  Seeds ovate, 6.5–8 × 4.5–6 mm, margin winged, remotely erose, faces minutely and sparsely papillose and rugulose; coma 3–3.5 cm.</description>
  <discussion>Asclepias tomentosa is restricted largely to coastal and inland sandhills.  As described by B. A. Sorrie (2016), it exhibits a disjunct distribution, with gaps of unoccupied, but suitable, habitat in eastern Georgia and from the western Florida Panhandle to Louisiana.   Sorrie reports a specimen from Alabama, but this can­not be found.  When not in flower, A. tomentosa can be confused with A. curtissii in peninsular Florida, where they sometimes co-occur in close proximity, and with A. obovata on the Gulf Coastal Plain.  It can be distin­guished from A. curtissii by the usually larger and more densely vestitured leaf blades.  Both species may have purple stems.  Asclepias obovata can be dis­tinguished from A. tomentosa by the hirtellous to velu­tinous ves­ti­ture of the herbage.  Outside of Florida, pop­ulations of A. tomentosa are few, but it has not been considered to be of conservation concern; evaluation of its status in Texas and Georgia (known only from Coffee County) may be warranted.</discussion>
  <references>
    <reference>Sorrie, B. A.  2016.  The curious distribution of Asclepias tomentosa (Apocynaceae).  Phytoneuron 2016-68: 1–4.</reference>
  </references>
  <description type="phenology">Flowering May–Aug(–Oct); fruiting Jun–Oct.</description>
  <description type="habitat">Sand­hills, dunes, sandy and marl soils, pine, pine-palmetto, pine-oak, and oak scrubs, pine flatwoods.</description>
  <description type="elevation">0–200 m.</description>
  <description type="distribution">Fla., Ga., N.C., S.C., Tex.</description>
</bio:treatment>
