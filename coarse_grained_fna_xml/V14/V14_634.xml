<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">14</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name rank="family" authority="Jussieu" date="unknown">Convolvulaceae</taxon_name>
    <taxon_name rank="genus" authority="R. Brown" date="1810">CALYSTEGIA</taxon_name>
    <taxon_name rank="species" authority="(Linnaeus) R. Brown" date="1810">sepium</taxon_name>
    <place_of_publication>
      <publication_title>Prodr.,</publication_title>
      <place_in_publication>483.  1810</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family convolvulaceae;genus calystegia;species sepium</taxon_hierarchy>
    <other_info_on_name type="special_status">W</other_info_on_name>
    <other_info_on_name type="special_status">F</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="BASIONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Convolvulus</taxon_name>
    <taxon_name rank="species" authority="Linnaeus" date="1753">sepium</taxon_name>
    <place_of_publication>
      <publication_title>Sp. Pl.</publication_title>
      <place_in_publication>1: 153.  1753</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>genus convolvulus;species sepium</taxon_hierarchy>
  </taxon_identification>
  <number>18.</number>
  <other_name type="common_name">Hedge bindweed</other_name>
  <description type="morphology">Perennials, rhizomatous.  Herb­age glabrate, glabrous, or hairy.  Stems trailing or twining-climbing.  Leaves: blade linear, ovate, broadly to narrowly tri­angular, or triangular-hastate, (24–)36–50(–150) mm, base usu­ally lobed, sometimes nearly truncate, lobes usually 1- or 2-pointed, sometimes rounded, basal sinus usu­ally acute to rounded, rarely almost closed.  Bracts immediately subtending sepals, in subsp. erratica, intergrading with sepals, oblong, oval, or ovate, 12–34 × 5–26(–28) mm, proximally flat or keeled, not or scarcely saccate, margins not or scarcely enfolding sepals, apex acute to subobtuse or truncate.  Flowers: sepals lanceolate, 10–19(–25) mm; corolla pink or white, (28–)30–70(–80) mm.</description>
  <discussion>Subspecies 8 (6 in the flora).</discussion>
  <description type="distribution">North America, South America, Europe, Asia, Africa.</description>
  <discussion>Subspecies of Calystegia sepium mostly show strong geographic separation; morphologic intermediates between subspecies of C. sepium and between C. sepium and other species of Calystegia make taxonomy difficult.  Subspecies roseata Brummitt is known from Atlantic coasts of Europe, temperate coasts of South America, Easter Island, New Zealand, and Australia; subsp. spectabilis Brummitt is known from Europe and Asia.</discussion>
  <key>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Corollas white.</description>
      <next_statement_id>2</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blade basal sinuses acute; corollas 30–50 mm; stamens 17–24 mm.</description>
      <determination>18a. Calystegia sepium subsp. sepium</determination>
    </key_statement>
    <key_statement>
      <statement_id>2</statement_id>
      <description type="morphology">Leaf blade basal sinuses rounded; corollas (28–)44–65(–80) mm; stamens 19–32 mm.</description>
      <next_statement_id>3</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blades broadly triangular, basal lobes spreading, ± 2-pointed.</description>
      <determination>18b. Calystegia sepium subsp. angulata</determination>
    </key_statement>
    <key_statement>
      <statement_id>3</statement_id>
      <description type="morphology">Leaf blades linear to narrowly triangular, basal lobes slightly to abruptly spreading, 1-pointed or rounded.</description>
      <determination>18c. Calystegia sepium subsp. binghamiae</determination>
    </key_statement>
    <key_statement>
      <statement_id>1</statement_id>
      <description type="morphology">Corollas pink.</description>
      <next_statement_id>4</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf blade basal sinuses almost closed; bracts intergrading with sepals.</description>
      <determination>18d. Calystegia sepium subsp. erratica</determination>
    </key_statement>
    <key_statement>
      <statement_id>4</statement_id>
      <description type="morphology">Leaf blade sinuses acute or rounded; bracts not intergrading with sepals.</description>
      <next_statement_id>5</next_statement_id>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Herbage usually pubescent to tomentose, sometimes glabrate or glabrous; leaf blade sinuses acute.</description>
      <determination>18e. Calystegia sepium subsp. americana</determination>
    </key_statement>
    <key_statement>
      <statement_id>5</statement_id>
      <description type="morphology">Herbage glabrous; leaf blade sinuses rounded.</description>
      <determination>18f. Calystegia sepium subsp. appalachiana</determination>
    </key_statement>
  </key>
</bio:treatment>
