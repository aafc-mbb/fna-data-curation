<?xml version='1.0' encoding='UTF-8'?>
<bio:treatment xmlns:bio="http://www.github.com/biosemantics" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.github.com/biosemantics http://www.w3.org/2001/XMLSchema-instance">
  <meta>
    <source>
      <date>unknown</date>
      <author>unknown</author>
    </source>
    <other_info_on_meta type="volume">14</other_info_on_meta>
  </meta>
  <taxon_identification status="ACCEPTED">
    <taxon_name authority="unknown" date="unknown" rank="family">Apocynaceae</taxon_name>
    <taxon_name rank="genus" authority="Linnaeus" date="1754">ASCLEPIAS</taxon_name>
    <taxon_name rank="species" authority="Linnaeus" date="1756">exaltata</taxon_name>
    <place_of_publication>
      <publication_title>Amoen. Acad.</publication_title>
      <place_in_publication>3: 404.  1756</place_in_publication>
    </place_of_publication>
    <taxon_hierarchy>family apocynaceae;genus asclepias;species exaltata</taxon_hierarchy>
    <other_info_on_name type="special_status">E</other_info_on_name>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">Asclepias</taxon_name>
    <taxon_name rank="species" authority="Vail" date="unknown">bicknellii</taxon_name>
    <taxon_hierarchy>genus asclepias;species bicknellii</taxon_hierarchy>
  </taxon_identification>
  <taxon_identification status="SYNONYM">
    <taxon_name rank="genus" authority="unknown" date="unknown">A.</taxon_name>
    <taxon_name rank="species" authority="G. F. Lyon ex Pursh" date="unknown">phytolaccoides</taxon_name>
    <taxon_hierarchy>genus a.;species phytolaccoides</taxon_hierarchy>
  </taxon_identification>
  <number>74.</number>
  <other_name type="common_name">Poke or tall milkweed</other_name>
  <other_name type="common_name">asclépiade très grande</other_name>
  <description type="morphology">Herbs.  Stems 1–3+, erect, unbranched, 65–150 cm, sparsely pubescent to glabrate, not glaucous, rhizomes absent.  Leaves opposite (rarely whorled at 1 midstem node), petiolate, with 1 stipular colleter on each side of petiole; petiole 5–15 mm, minutely puberulent with curved trichomes to glabrate; blade broadly ovate to oblong or elliptic, 10–24 × 2–11 cm, membranous, base cuneate, margins entire, apex attenuate to acuminate, venation eucamptodromous to brochidodromous, surfaces pilosulous to glabrate abaxially, sparsely pilosulous to glabrate adaxially, densely so on veins, margins ciliate, 6–10 laminar colleters (often obscured in pressed specimens).  Inflorescences extra-axillary at upper nodes (terminal), pedunculate, 11–41-flowered; peduncle 0.5–8.5 cm, puberulent on 1 side with curved trichomes, with 1 caducous bract at the base of each pedicel.  Pedicels 25–45 mm, puberulent with curved trichomes to pilosulous on 1 side.  Flowers spreading to drooping; calyx lobes narrowly lanceolate, 3–4.5 mm, apex attenuate, pilosulous; corolla green (rarely pink-tinged), lobes reflexed, sometimes with spreading tips, elliptic, 6–12 mm, apex acute, glabrous; gynostegial column 1.5–2 mm; fused anthers green, columnar, 2.5–3.5 mm, wings right-triangular with rounded tip, apical appendages deltoid; corona segments white to pinkish, sometimes red-purple at base, stipitate, tubular, 3–5 mm, exceeding style apex, base saccate, apex truncate with 1–2 teeth on each side, glabrous, internal appendage falcate, exserted, arching above style apex, glabrous; style apex shallowly depressed, green or cream.  Follicles erect on upcurved pedicels, fusiform, 10–15 × 1.5–2 cm, apex long-acuminate, smooth, puberulent with curved trichomes.  Seeds lance-ovate, 8–10 × 4–6 mm, margin winged, faces minutely rugulose; coma 2.5–3 cm.  2n = 22.</description>
  <discussion>Unlike the other common deciduous forest understory milkweeds, the range of Asclepias exaltata does not extend to the Ozarks.  Compared to these other species, A. exaltata seems to prefer richer soils.  Non-flowering individuals are often confused with A. purpurascens, from which they are distinguished by leaves with thinner texture, sparser abaxial vestiture, and longer-tapered apices.  Hybrids with A. syriaca are well established at several disjunct locations (S. R. Kephart et al. 1988), and their genetics and pollination have been studied (S. B. Broyles 2002; T. M. Stoepler et al. 2012).  Hybrids with A. purpurascens and A. amplexicaulis are also known, but appear to be rare and local.  Putative hybrids exhibit intermediate floral and vegetative morphology.  Asclepias exaltata is rare at the margins of its range and is considered to be of conservation concern in Alabama (Lawrence, Madison, and Winston counties), Delaware (New Castle County), Rhode Island, and Quebec.  Recently, it has been documented at a single site in Missouri (Cape Girardeau County) and should be considered to be of conservation concern in that state, too.</discussion>
  <description type="phenology">Flowering May–Aug; fruiting May–Oct.</description>
  <description type="habitat">Bluffs, summits, hills, slopes, ravines, bottomlands, stream banks, lake shores, moraines, rock outcrops, limestone, alluvium, rich, thin, rocky, and sandy soils, oak, pine-oak, mixed-hardwood, riparian, and cove forests and edges, meadows.</description>
  <description type="elevation">0–1500 m.</description>
  <description type="distribution">Ont., Que.; Ala., Conn., Del., Ga., Ill., Ind., Iowa, Ky., Maine, Md., Mass., Mich., Minn., Mo., N.H., N.J., N.Y., N.C., Ohio, Pa., R.I., S.C., Tenn., Vt., Va., W.Va., Wis.</description>
</bio:treatment>
